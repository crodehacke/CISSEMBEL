#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 27 11:10:19 CEST 2021


-- Test and plots for mod_physic_column.F08

@author: Christian Rodehacke, DMI Copenhagen, Denmark

@copyright: Copyright (C) 2016-2025 Christian Rodehacke
"""
# -------------------------------------------------------------------
# This file is part of CISSEMBEL, which is the
# == Copenhagen Ice-Snow Surface Energy and Mass Balance modEL ==
#
# CISSEMBEL is free software; you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE  v.1.2
#
# Information about the EUROPEAN UNION PUBLIC LICENCE (*EUPL*) and
# translations into other European languages are available at
# https://eupl.eu (last access: 2023-09-18)](https://eupl.eu/)
#
# We hope that  this  distributed  CISSEMBEL  code, its  tools and
# documentations are usefull. Any improvements of the code, tools,
# and documentations tools are  very welcome.  Please, consider to
# share your thoughts and improvements with the community.
#
# The documentation,  tools,  and the code are provided as it are.
# NO WARRANTY  ABOUT FITNESS  AND USABILITY OF THE CODE, TOOLS, OR
# DOCUMENTATION  ARE  GIVEN.   NO  WARRANTY  IS  GIVEN  ABOUT  THE
# CORRECTNESS  OF COMPUTED RESULTS WITH THE  PROVIDED CODE, TOOLS,
# AND DOCUMENTATION. YOU USE THE CODE, DOCUMENTATION, AND TOOLS AT
# YOUR OWN RISK.           Removing  of  any  copyright  statement
# or  this  disclaimer  is considered  as a  attempt  to defraud.
#
# -------------------------------------------------------------------

# -------------------------------------------------------------------------
def plot_density_frozen(library_name, plot_suffix=None,
                        plotname='density_frozen'):
    '''
    Plot results of the via c-binding call Fortran function density_frozen

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'density_frozen'.

    Returns
    -------
    None.

    '''
    print('* Combined densities of snow and ice in a plot')
    snow_content = np.linspace(10, 100, 71)
    ice_content = np.linspace(10, 100, 47)
    snow_density = np.linspace(330, 910, 65)

    snow_content2 = np.linspace(10, 100, 91)
    ice_content2 = np.linspace(10, 100, 15)
    snow_density2 = np.linspace(330, 910, 4)

    # Loop of calculations for different combinations
    density_frozen_multi = np.zeros((len(snow_content), len(ice_content),
                                     len(snow_density)))
    for i_scont, scont in enumerate(snow_content):
        for i_icont, icont in enumerate(ice_content):
            density_frozen_multi[i_scont, i_icont, :] = \
            mod_physic_column.density_frozen(np.zeros_like(snow_density)+scont,
                                             np.zeros_like(snow_density)+icont,
                                             snow_density,
                                             np.zeros_like(snow_density)+RHO_ICE,
                                             library_name)

    density_frozen_multi2 = np.zeros((len(snow_content2), len(ice_content2),
                                      len(snow_density2)))
    for i_scont, scont in enumerate(snow_content2):
        for i_icont, icont in enumerate(ice_content2):
            density_frozen_multi2[i_scont, i_icont, :] = \
            mod_physic_column.density_frozen(np.zeros_like(snow_density2)+scont,
                                             np.zeros_like(snow_density2)+icont,
                                             snow_density2,
                                             np.zeros_like(snow_density2)+RHO_ICE,
                                             library_name)

    #
    # Plot 1
    #
    fig1 = plt.figure(dpi=300, facecolor='white')
    fig1.subplots_adjust(wspace=0.1)
    ax0, ax1 = fig1.subplots(1, 2, sharey=True)
    cmap = 'GnBu'

    indx_ax0 = -1
    indx_ax1 = int(len(snow_density)*0.25)

    field_ax0 = density_frozen_multi[:, indx_ax0, :].squeeze() #rho_snow:c_snow
    field_ax1 = density_frozen_multi[:, :, indx_ax1].squeeze() #   c_ice:c_snow

    vmin = np.ceil(min(field_ax0.min(), field_ax1.min()))
    vmax = np.floor(max(field_ax0.max(), field_ax1.max()))

    #
    # Subplot ax0
    #
    csf = ax0.contourf(snow_density, snow_content, field_ax0, vmin=vmin,
                       vmax=vmax, cmap=cmap)
    ax0.set_xlabel(r'$\rho_{snow}$ (kg m$^{-3}$)')
    ax0.set_ylabel(r'$c_{snow}$ (kg m$^{-2}$)')
    ax0.set_title(r'$c_{{ice}}$ = {:.4g} kg m$^{{-3}}$'.format(ice_content[indx_ax0]))

    #
    # Subplot ax1
    #
    ax1.contourf(ice_content, snow_content, field_ax1, vmin=vmin, vmax=vmax,
                 cmap=cmap)
    ax1.set_xlabel(r'$c_{ice}$ (kg m$^{-2}$)')
    ax1.set_title(r'$\rho_{{snow}}$ = {:.4g} kg m$^{{-3}}$'.format(snow_density[indx_ax1]))
    ax1.text(ice_content[2], snow_content[2],
             r'$\rho_{{ice}}$ = {:.4g} kg m$^{{-3}}$'.format(RHO_ICE))

    fig1.colorbar(csf, ax=[ax0, ax1],
                 label=r'combined density $\rho_{{frozen}}$ (kg m$^{-3}$)')

    if plot_suffix:
        module4plots.plotting(fig1, plotname+'_2D', plot_suffix)

    #
    # Plot 2
    #
    fig2 = plt.figure(dpi=300, facecolor='white')
    fig2.subplots_adjust(wspace=0.1)
    ax0, ax1 = fig2.subplots(1, 2, sharey=True)
    cmap = 'GnBu'

    indx_ax0 = -1
    indx_ax1 = int(len(snow_density2)*0.25)

    field_ax0 = density_frozen_multi2[:, indx_ax0, :].squeeze() # rho_snow: c_snow
    field_ax1 = density_frozen_multi2[:, :, indx_ax1].squeeze() # C_ice : c_snow

    #
    # Subplot ax0
    #
    csf0 = ax0.plot(snow_content2, field_ax0)
    ax0.set_xlabel(r'$c_{snow}$ (kg m$^{-2}$)')
    ax0.legend(csf0, np.round(snow_density2), title=r'$\rho_{{snow}}$ (kg m$^{-3}$)')
    ax0.set_ylabel(r'$\rho_{{{frozen}}}$ (kg m$^{-2}$)')
    ax0.set_title(r'$c_{{ice}}$ = {:.4g} kg m$^{{-3}}$'.format(ice_content[indx_ax0]))

    #
    # Subplot ax1
    #
    csf1 = ax1.plot(ice_content2, field_ax1[::20,: :].T)
    ax1.set_xlabel(r'$c_{ice}$ (kg m$^{-2}$)')
    ax1.legend(csf1, np.round(snow_content2[::20]),
               title=r'$c_{{snow}}$ (kg m$^{-2}$)')
    ax1.text(ice_content[1], 500,
             r'$\rho_{{ice}}$ = {:.4g} kg m$^{{-3}}$'.format(RHO_ICE))
    ax1.set_title(r'$\rho_{{snow}}$ = {:.3g} kg m$^{{-3}}$'.format(snow_density[indx_ax1]))

    if plot_suffix:
        module4plots.plotting(fig2, plotname+'_line', plot_suffix)


# -------------------------------------------------------------------------
def plot_water_saturation(library_name, plot_suffix=None,
                          plotname='water_saturation'):
    '''
    Plot results of the via c-binding call Fortran function water_saturation

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'water_saturation'.

    Returns
    -------
    None.

    '''
    print('* Water saturation fraction')

    snow_content = np.linspace(10, 100, 211)    # (10, 100, 71)
    water_content = np.linspace(10, 20, 61)     # (10, 20, 21)
    snow_density = np.linspace(330, 910, 159)   # (330, 910, 59)

    snow_content2 = np.linspace(10, 110, 101)
    water_content2 = np.linspace(10, 20, 5)
    snow_density2 = np.linspace(330, 910, 4)

    # Loop of calculations for different combinations
    water_sat_multi = np.zeros((len(snow_content), len(water_content),
                                len(snow_density)))
    for i_scont, scont in enumerate(snow_content):
        for i_wcont, wcont in enumerate(water_content):
            scont_array = np.zeros_like(snow_density)+scont
            wcont_array = np.zeros_like(snow_density)+wcont
            water_sat_multi[i_scont, i_wcont, :] = \
            mod_physic_column.water_saturation(scont_array, wcont_array,
                                               snow_density, library_name)

    water_sat_multi2 = np.zeros((len(snow_content2), len(water_content2),
                                 len(snow_density2)))
    for i_scont, scont in enumerate(snow_content2):
        for i_wcont, wcont in enumerate(water_content2):
            scont_array = np.zeros_like(snow_density2)+scont
            wcont_array = np.zeros_like(snow_density2)+wcont
            water_sat_multi2[i_scont, i_wcont, :] = \
            mod_physic_column.water_saturation(scont_array, wcont_array,
                                               snow_density2, library_name)

    #
    # Plot 1
    #
    fig1 = plt.figure(dpi=300, facecolor='white')
    fig1.subplots_adjust(wspace=0.1)
    ax0, ax1 = fig1.subplots(1, 2, sharey=True)
    cmap = 'Blues'

    indx_ax0 = int(len(water_content)*0.25) #-1
    indx_ax1 = int(len(snow_density)*0.40)

    field_ax0 = water_sat_multi[:, indx_ax0, :].squeeze()
    field_ax1 = water_sat_multi[:, :, indx_ax1].squeeze()

    vmin = 0.
    vmax = 1.

    #
    # Subplot ax0
    #
    csf0a = ax0.contourf(snow_density, snow_content, field_ax0, vmin=vmin,
                         vmax=vmax, cmap=cmap, extend='max')
    #ax0.contour(snow_density, snow_content, field_ax0, levels=[0.9999999999, 1.0])
    ax0.set_xlabel(r'$\rho_{snow}$ (kg m$^{-3}$)')
    ax0.set_ylabel(r'$c_{snow}$ (kg m$^{-2}$)')
    ax0.set_title(r'$c_{{water}}$ = {:.4g} kg m$^{{-2}}$'.format(water_content[indx_ax0]))

    #
    # Subplot ax1
    #
    ax1.contourf(water_content, snow_content, field_ax1, vmin=vmin, vmax=vmax,
                 cmap=cmap, extend='max')
    #ax1.contour(water_content, snow_content, field_ax1, levels=[0.9999999999, 1.0])
    ax1.set_xlabel(r'$c_{{water}}$ (kg m$^{-2}$)')
    ax1.set_title(r'$\rho_{{snow}}$ = {:.4g} kg m$^{{-3}}$'.format(snow_density[indx_ax1]))

    fig1.colorbar(csf0a, ax=[ax0, ax1],
                  label=r'water saturation fraction $\theta$ (1)')

    # Mark and label archived saturation
    ax0.contourf(snow_density, snow_content,
                 np.where(field_ax0>= 1., 1., np.NaN), cmap='Greys_r', alpha=0.55)
    ax1.contourf(water_content, snow_content,
                 np.where(field_ax1>= 1., 1., np.NaN), cmap='Greys_r', alpha=0.55)

    ax0.text(snow_density[0]+(snow_density[-1]-snow_density[0])*0.88,
             snow_content[0]+(snow_content[-1]-snow_content[0])*0.05,
             'Saturated', color='white', ha='right', va='bottom')
    ax1.text(water_content[0]+(water_content[-1]-water_content[0])*0.88,
             snow_content[0]+(snow_content[-1]-snow_content[0])*0.05,
             'Saturated', color='white', ha='right', va='bottom')

    if plot_suffix:
        module4plots.plotting(fig1, plotname+'_2D', plot_suffix)

    #
    # Plot 2
    #
    fig2 = plt.figure(dpi=300, facecolor='white')
    fig2.subplots_adjust(wspace=0.1)
    ax0, ax1 = fig2.subplots(1, 2, sharey=True)
    cmap = 'GnBu'
    #cmap = 'YlOrBr'

    indx_ax0 = -1
    indx_ax1 = int(len(snow_density2)*0.25)

    field_ax0 = water_sat_multi2[:, indx_ax0, :].squeeze()
    field_ax1 = water_sat_multi2[:, :, indx_ax1].squeeze()

    #
    # Subplot ax0
    #
    csf0 = ax0.plot(snow_content2, field_ax0)
    ax0.set_ylabel(r'water saturation fraction $\theta$ (1)')
    ax0.legend(csf0, np.round(snow_density2),
               title=r'$\rho_{{snow}}$ (kg m$^{-3}$)')
    ax0.set_xlabel(r'$c_{{{snow}}}$ (kg m$^{-2}$)')
    ax0.set_title(r'$c_{{water}}$ = {:.4g} kg m$^{{-2}}$'.format(water_content[indx_ax0]))

    #
    # Subplot ax1
    #
    csf1 = ax1.plot(snow_content2, field_ax1)
    ax1.set_xlabel(r'$c_{{{snow}}}$ (kg m$^{-2}$)')
    ax1.legend(csf1, np.round(water_content2),
               title=r'$c_{{water}}$ (kg m$^{-2}$)')
    ax1.set_title(r'$\rho_{{snow}}$ = {:.3g} kg m$^{{-3}}$'.format(snow_density[indx_ax1]))

    if plot_suffix:
        module4plots.plotting(fig2, plotname+'_line', plot_suffix)


# -------------------------------------------------------------------------
def plot_irreducible_water_saturation(library_name, plot_suffix=None,
                                      plotname='irreducible_water_saturation'):
    '''
    Plot results of the via c-binding call Fortran function
    irreducible_water_saturation

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'irreducible_water_saturation'.

    Returns
    -------
    None.

    '''
    print('* Irreducible water saturation')

    snow_density = np.linspace(100, 830, 62)  #830 : pore closure
    irreduc_wcont = mod_physic_column.irreducible_water_saturation(snow_density,
                                                                library_name)
    #
    # Plot 1
    #
    fig1 = plt.figure(dpi=300, facecolor='white')
    ax0 = fig1.subplots()

    ax0.plot(snow_density, irreduc_wcont, linewidth=2, color='indigo')
    ax0.set_xlabel(r'$\rho_{snow}$ (kg m$^{-3}$)')
    ax0.set_ylabel(r'irreducible water saturation')

    if plot_suffix:
        module4plots.plotting(fig1, plotname, plot_suffix)


# -------------------------------------------------------------------------
def plot_retention_potential(library_name, plot_suffix=None, plotname='retention_potential'):
    '''
    Plot results of the via c-binding call Fortran function retention_potential

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'retention_potential'.

    Returns
    -------
    None.

    '''
    print('* Retention potential')

    snow_content = np.linspace(10, 100, 71)
    snow_density = np.linspace(330, 910, 59)

    # Loop of calculations for different combinations
    retention_pot = np.zeros((len(snow_content), len(snow_density)))
    for i_scont, scont in enumerate(snow_content):
        scont_array = np.zeros_like(snow_density)+scont
        retention_pot[i_scont, :] = \
        mod_physic_column.retention_potential(scont_array, snow_density,
                                              library_name)

    #
    # Plot 1
    #
    fig1 = plt.figure(dpi=300, facecolor='white')
    fig1.subplots_adjust(wspace=0.1)
    ax0 = fig1.subplots()
    cmap = 'PuBuGn'

    csf0a = ax0.contourf(snow_density, snow_content, retention_pot, cmap=cmap)
    contour_levels = [1, 2, 5, 10]
    csf0b = ax0.contour(snow_density, snow_content, retention_pot,
                        levels=contour_levels, colors='dimgray',
                        linewidths=1.5)
    ax0.clabel(csf0b, contour_levels, fmt='%1.f', colors='k')
    ax0.set_xlabel(r'$\rho_{snow}$ (kg m$^{-3}$)')
    ax0.set_ylabel(r'$c_{snow}$ (kg m$^{-2}$)')

    cbar = fig1.colorbar(csf0a, ax=[ax0],
                          label=r'retention potential (kg m$^{-2}$)')
    cbar.add_lines(csf0b)

    if plot_suffix:
        module4plots.plotting(fig1, plotname, plot_suffix)


# -------------------------------------------------------------------------
def plot_calc_runoff(library_name, plot_suffix=None,
                          plotname='calc_runoff'):
    '''
    Plot results of the via c-binding call Fortran function calc_runoff

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'calc_runoff'.

    Returns
    -------
    None.

    '''
    print('* Runoff')

    snow_content = np.linspace(10, 100, 71)
    water_content = np.linspace(0, 10, 21)
    snow_density = np.linspace(330, 910, 59)
    waterflow = np.linspace(0,10,5)

    dtime = 3600.*3 # Seconds
    time_runoff = dtime*1

    # Preallocate of results
    runoff_multi = np.zeros((len(snow_content), len(water_content),
                             len(snow_density), len(waterflow)))

    # Loop of calculations for different combinations
    for i_scont, scont in enumerate(snow_content):
        for i_wcont, wcont in enumerate(water_content):
            for i_wflow, wflow in enumerate(waterflow):
                runoff_multi[i_scont, i_wcont, :, i_wflow] = \
                    mod_physic_column.calc_runoff(dtime,
                                                  np.zeros_like(snow_density)+scont,
                                                  np.zeros_like(snow_density)+wcont,
                                                  snow_density,
                                                  np.zeros_like(snow_density)+wflow,
                                                  np.zeros_like(snow_density)+time_runoff,
                                                  library_name)

    #
    # Plot 1
    #
    fig1 = plt.figure(dpi=300, facecolor='white')
    fig1.subplots_adjust(wspace=0.1)
    ax0, ax1 = fig1.subplots(1, 2, sharey=True)
    cmap = 'Blues'

    indx_ax0 = int(len(water_content)*0.66) #-1
    indx_ax1 = int(len(snow_density)*0.20)
    indx_wflow = 0

    field_ax0 = runoff_multi[:, indx_ax0, :, indx_wflow].squeeze()
    field_ax1 = runoff_multi[:, :, indx_ax1, indx_wflow].squeeze()

    # vmin = np.ceil(min(field_ax0.min(), field_ax1.min()))
    vmax = np.floor(max(field_ax0.max(), field_ax1.max()))

    contourf_levels = range(0,max(int(np.floor(vmax))-1, 2),1) #range(0,8,1)
    #
    # Subplot ax0
    #
    csf0a = ax0.contourf(snow_density, snow_content, field_ax0,
                         levels=contourf_levels, cmap=cmap, extend='max')
    ax0.set_xlabel(r'$\rho_{snow}$ (kg m$^{-3}$)')
    ax0.set_ylabel(r'$c_{snow}$ (kg m$^{-2}$)')
    ax0.set_title(r'$c_{{water}}$ = {:.4g} kg m$^{{-2}}$'.format(water_content[indx_ax0]))
    ax0.text(snow_density[1], snow_content[3],
             r'$\Delta t$ = {:.1g} h'.format(dtime/3600)+
             ', $\\tau_{{runoff}}$ = {:.1g} h'.format(time_runoff/3600))

    #
    # Subplot ax1
    #
    ax1.contourf(water_content, snow_content, field_ax1,
                 levels=contourf_levels, cmap=cmap, extend='max')
    ax1.set_xlabel(r'$c_{{water}}$ (kg m$^{-2}$)')
    ax1.set_title(r'$\rho_{{snow}}$ = {:.4g} kg m$^{{-3}}$'.format(snow_density[indx_ax1]))
    ax1.text(water_content[1], snow_content[3],
             'along with a downward flow: {:.2g} kg m$^{{-2}}$'.format(waterflow[indx_wflow]),
             rotation=90)
    #
    # All subfigures colorbar
    #
    fig1.colorbar(csf0a, ax=[ax0, ax1], label=r'runoff (kg m$^{{-2}}$)')
    if plot_suffix:
        module4plots.plotting(fig1, plotname+'_2D', plot_suffix)


# -------------------------------------------------------------------------
def plot_supericeform_potential(library_name, plot_suffix=None,
                                plotname='supericeform_pot'):
    '''
    Plot results of the via c-binding call Fortran function supericeform_potential

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'supericeform_pot'.

    Returns
    -------
    None.

    '''
    print('* Potential superimposed ice formation')

    snow_content = np.linspace(10, 100, 71)
    ice_content = np.linspace(0, 10, 21)
    snow_density = np.linspace(330, 910, 59)
    temperature = 273.15 - np.linspace(0,20,5)
    melt_temp = FREEZING_TEMP
    dtime = 3600.*24*90 # Seconds

    # Loop of calculations for different combinations
    superimposed_ice_form_multi = np.zeros((len(snow_content),
                                            len(ice_content),
                                            len(snow_density),
                                            len(temperature)))
    for i_scont, scont in enumerate(snow_content):
        for i_icont, icont in enumerate(ice_content):
            for i_temp, temp in enumerate(temperature):
                scont_array = np.zeros_like(snow_density)+scont
                icont_array = np.zeros_like(snow_density)+icont
                temp_array = np.zeros_like(snow_density)+temp
                melttemp_array = np.zeros_like(snow_density)+melt_temp
                superimposed_ice_form_multi[i_scont, i_icont, :, i_temp] = \
                    mod_physic_column.supericeform_potential(dtime,
                                                             scont_array,
                                                             icont_array,
                                                             snow_density,
                                                             temp_array,
                                                             melttemp_array,
                                                             library_name)

    #
    # Plot 1
    #
    fig1 = plt.figure(dpi=300, facecolor='white')
    fig1.subplots_adjust(wspace=0.1)
    ax0, ax1 = fig1.subplots(1, 2, sharey=True)
    cmap = 'ocean_r'

    indx_ax0 = int(len(ice_content)*0.33) #-1
    indx_ax1 = int(len(snow_density)*0.80)
    indx_temp = -1

    field_ax0 = superimposed_ice_form_multi[:, indx_ax0, :, indx_temp].squeeze()
    field_ax1 = superimposed_ice_form_multi[:, :, indx_ax1, indx_temp].squeeze()

    contourf_levels = np.linspace(0, 0.11, 12)
    #
    # Subplot ax0
    #
    csf0a = ax0.contourf(snow_density, snow_content, field_ax0,
                         levels=contourf_levels, cmap=cmap, extend='max')
    ax0.set_xlabel(r'$\rho_{snow}$ (kg m$^{-3}$)')
    ax0.set_ylabel(r'$c_{snow}$ (kg m$^{-2}$)')
    ax0.set_title(r'$c_{{ice}}$ = {:.4g} kg m$^{{-2}}$'.format(ice_content[indx_ax0]))


    if dtime/3600 < 24 :
        ax0.text(snow_density[1], snow_content[3],
                 r'$\Delta t$ = {:.1f} hour'.format(dtime/3600))
    else:  #if: dtime/86400 < 31:
        ax0.text(snow_density[1], snow_content[3],
                 r'$\Delta t$ = {:.1f} day'.format(dtime/86400))

    #
    # Subplot ax1
    #
    ax1.contourf(ice_content, snow_content, field_ax1,
                 levels=contourf_levels, cmap=cmap, extend='max')
    ax1.set_xlabel(r'$c_{{ice}}$ (kg m$^{-2}$)')
    ax1.set_title(r'$\rho_{{snow}}$ = {:.4g} kg m$^{{-3}}$'.format(snow_density[indx_ax1]))
    ax1.text(ice_content[1], snow_content[3],
             r'temperature {:.1f} $^{{\circ}}C$'.format(temperature[indx_temp]-FREEZING_TEMP),
             c='lightgray')

    #
    # All subfigures colorbar
    #
    fig1.colorbar(csf0a, ax=[ax0, ax1],
                  label=r'potential superimposed ice formation (kg m$^{{-2}}$)')
    if plot_suffix:
        module4plots.plotting(fig1, plotname+'_2D', plot_suffix)


# -------------------------------------------------------------------------
def plot_grain_growth(library_name, plot_suffix=None,
                          plotname='grain_growth'):
    '''
    Plot results of the via c-binding call Fortran function grain_growth

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'grain_growth'.

    Returns
    -------
    None.

    '''
    print('* Grain growth')

    snow_content = np.linspace(10, 100, 37) #[::2] 5-step, [::4] 10-steps
    water_content = np.linspace(0, 10, 61)  #[::6] all integer, [::12] even numbers

    dtime = 3600.*6 # Seconds
    number_of_timesteps = 365*86400/dtime
    time_array_day = np.arange(0, int(number_of_timesteps))*dtime/(86400.)

    # Preallocate of results
    graingrowth_multi = np.zeros((len(snow_content), len(water_content),
                                  int(number_of_timesteps)))
    graingrowth_multi = graingrowth_multi+DEFAULT_NEW_GRAIN_DIAMETER

    # Loop of calculations for different combinations
    #
    # Also a loop compute the temporal evolution of the grain diameter via a
    # loop to mimic the behavior of the Fortran code, where we loop too.
    #
    for istep in range(1, int(number_of_timesteps)):
        for i_scont, scont in enumerate(snow_content):
            scont_array = np.zeros_like(water_content)+scont
            grain_array = graingrowth_multi[i_scont, :, istep-1]
            graingrowth_multi[i_scont, :, istep] = \
                mod_physic_column.grain_growth(dtime, grain_array, scont_array,
                                               water_content, library_name)

    convert_m_to_mm = True
    if convert_m_to_mm:
        graingrowth_multi = graingrowth_multi * 1000.

    indx_ax0 = 9                    # water_content selector
    indx_ax1 = int(260*86400/dtime) # time_array-day selector

    field_ax0 = graingrowth_multi[:, indx_ax0, :].squeeze() # snow/time
    field_ax1 = graingrowth_multi[:, :, indx_ax1].squeeze() # snow/water

    #
    # Plot 1
    #
    fig1 = plt.figure(dpi=300, facecolor='white')
    fig1.subplots_adjust(wspace=0.1)
    ax0, ax1 = fig1.subplots(1, 2, sharey=True)
    cmap = 'Oranges'

    # vmin = np.ceil(min(field_ax0.min(), field_ax1.min()))
    # vmax = np.floor(max(field_ax0.max(), field_ax1.max()))
    contourf_levels = np.linspace(0.5, 1.8, 14) # np.linspace(vmin, vmax, 11)
    contour_levels = [1.0, 1.5]
    #
    # Subplot ax0
    #
    csf0a = ax0.contourf(time_array_day, snow_content, field_ax0,
                         levels=contourf_levels, cmap=cmap, extend='max')
    csf0b = ax0.contour(time_array_day, snow_content, field_ax0,
                        levels=contour_levels, colors='dimgray')
    ax0.set_xlabel(r'Time (day)')
    ax0.set_ylabel(r'$c_{snow}$ (kg m$^{-2}$)')
    ax0.set_title(r'$c_{{water}}$ = {:.4g} kg m$^{{-2}}$'.format(water_content[indx_ax0]))
    ax0.text(time_array_day[25], snow_content[1],
             r'$\Delta t$ = {:.1g} h'.format(dtime/3600))

    #
    # Subplot ax1
    #
    ax1.contourf(water_content, snow_content, field_ax1,
                 levels=contourf_levels, cmap=cmap, extend='max')
    ax1.contour(water_content, snow_content, field_ax1,
                levels=contour_levels, colors='dimgray')
    ax1.set_xlabel(r'$c_{{water}}$ (kg m$^{-2}$)')
    ax1.set_title(r'Day {:.4g}'.format(time_array_day[indx_ax1]))

    #
    # All subfigures colorbar
    #
    if convert_m_to_mm:
        cbar = fig1.colorbar(csf0a, ax=[ax0, ax1],
                             label=r'Snow grain diameter, $d_{grain}$ (mm)')
    else:
        cbar = fig1.colorbar(csf0a, ax=[ax0, ax1],
                             label=r'Snow grain diameter, $d_{grain}$ (m)')
    cbar.add_lines(csf0b)

    if plot_suffix:
        module4plots.plotting(fig1, plotname+'_2D', plot_suffix)

    #
    # Plot 2
    #
    fig2 = plt.figure(dpi=300, facecolor='white')
    fig2.subplots_adjust(wspace=0.1)
    ax0, ax1 = fig2.subplots(1, 2) #, sharey=True)

    #
    # Subplot ax0
    #
    plt0 = ax0.plot(time_array_day, field_ax0[::8].T, linewidth=2)
    ax0.set_xlabel(r'Time (day)')
    # Below `ax`1: ax0.legend(plt0, snow_content[::8], title=r'$c_{{snow}}$')
    if convert_m_to_mm:
        ax0.set_ylabel(r'Snow grain diameter, $d_{grain}$ (mm)')
    else:
        ax0.set_ylabel(r'Snow grain diameter, $d_{grain}$ (m)')
    ax0.set_title(r'$c_{{water}}$ = {:.4g} kg m$^{{-2}}$'.format(water_content[indx_ax0]))

    #
    # Subplot ax1
    #
    ax1.plot(water_content, field_ax1[::8].T, linewidth=2)
    ax1.set_xlabel(r'$c_{{water}}$ (kg m$^{-2}$)')
    ax1.legend(plt0, snow_content[::8], title=r'$c_{{snow}}$')
    if convert_m_to_mm:
        ax1.set_ylabel(r'Snow grain diameter, $d_{grain}$ (mm)')
    else:
        ax1.set_ylabel(r'Snow grain diameter, $d_{grain}$ (m)')
    ax1.yaxis.set_label_position('right')
    ax1.yaxis.tick_right()
    ax1.set_title(r'Day {:.4g}'.format(time_array_day[indx_ax1]))

    if plot_suffix:
        module4plots.plotting(fig2, plotname+'_line', plot_suffix)


# -------------------------------------------------------------------------
def plot_hydraulic_suction(library_name, plot_suffix=None,
                           plotname='hydraulic_suction'):
    '''
    Plot results of the via c-binding call Fortran function hydraulic_suction

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'hydraulic_suction'.

    Returns
    -------
    None.

    '''
    print('* Hydraulic suction')

    #theta = np.linspace(0., 0.1, 21) # Fraction from 0 to 1
    # In the `logspace` the second number gives the order by which we divide
    # the `logspace`-results to obtain a range from 1e-`order` to 1.
    # Do not forget to use for the plot: `ax0.set_yscale('log')`
    theta = np.logspace(0, 6, 250)/1e6
    graind = np.linspace(0.4, 2.2, 145)*0.001 # Meter !!

    # Loop of calculations for different combinations
    hydro_suction = np.zeros((len(theta), len(graind)))
    for i_theta, the in enumerate(theta):
        theta_array = np.zeros_like(graind)+the
        hydro_suction[i_theta, :] = \
                mod_physic_column.hydraulic_suction(theta_array, graind,
                                                    library_name)

    convert_m_to_mm = True
    if convert_m_to_mm:
        graind = graind * 1000.

    #
    # Plot 1
    #
    fig1 = plt.figure(dpi=300, facecolor='white')
    ax0 = fig1.subplots()
    cmap = 'magma_r'

    contourf_levels = np.linspace(0.0, 0.6, 7) ##13)
    csf0a = ax0.contourf(graind, theta, hydro_suction, cmap=cmap,
                         levels=contourf_levels, extend='max')
    if convert_m_to_mm:
        ax0.set_xlabel(r'grain diameter, $d_{grain}$ (mm)')
    else:
        ax0.set_xlabel(r'grain diameter, $d_{grain}$ (m)')
    ax0.set_ylabel(r'water saturation fraction, $\Theta$ (1)')
    ax0.set_yscale('log')

    fig1.colorbar(csf0a, ax=[ax0], label=r'hydraulic suction')

    if plot_suffix:
        module4plots.plotting(fig1, plotname+'_2D', plot_suffix)


# -------------------------------------------------------------------------
def plot_snow_permeability(library_name, plot_suffix=None,
                           plotname='snow_permeability'):
    '''
    Plot results of the via c-binding call Fortran function snow_permeability

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'snow_permeability'.

    Returns
    -------
    None.

    '''
    print('* Snow permeability')

    theta = np.linspace(0., 1., 11)  # Fraction from 0 to 1
    theta = theta[theta>0]  # For theta=0, we obtain snow_perm=0

    graind = np.linspace(0.4, 2.2, 37)*0.001 #73)*0.001 # Meter !!
    snow_density = np.linspace(330, 910, 11) #30)
    snow_content = np.linspace(10, 100, 19)  #31)
    ice_content = np.linspace(0, 10, 9)      #11)

    # Loop of calculations for different combinations
    snow_perm = np.zeros((len(snow_content), len(ice_content),
                          len(snow_density), len(theta),
                          len(graind)))
    for i_scont, scont in enumerate(snow_content):
        for i_icont, icont in enumerate(ice_content):
            for i_sdens, sdens in enumerate(snow_density):
                for i_the, thea in enumerate(theta):
                    scont_array = np.zeros_like(graind)+scont
                    icont_array = np.zeros_like(graind)+icont
                    sdens_array = np.zeros_like(graind)+sdens
                    theta_array = np.zeros_like(graind)+thea
                    snow_perm[i_scont, i_icont, i_sdens, i_the, :] = \
                        mod_physic_column.snow_permeability(theta_array,
                                                            graind,
                                                            sdens_array,
                                                            scont_array,
                                                            icont_array,
                                                            library_name)
    convert_m_to_mm = True
    if convert_m_to_mm:
        graind = graind * 1000.

    #
    # Selection for Plot 1
    # snow_perm[idx_scont, idx_icont, idx_dens, idx_theta, idx_graind]
    want_scont = 20.
    want_icont = 5.
    want_grain = 2.
    idx_scont = int(np.where(abs(snow_content-want_scont) == \
                             min(abs(snow_content-want_scont)))[0])
    idx_icont = int(np.where(abs(ice_content-want_icont) == \
                             min(abs(ice_content-want_icont)))[0])
    idx_dens = int(len(snow_density)/2)
    idx_theta = -1
    idx_graind = int(np.where(abs(graind-want_grain) == \
                              min(abs(graind-want_grain)))[0])

    #
    # Plot 1
    #
    fig1 = plt.figure(dpi=300, facecolor='white') #, figsize=(6.4, 5.2))
    fig1.subplots_adjust(wspace=0.5)
    ax0, ax1 = fig1.subplots(1, 2)
    cmap = 'twilight'

    cl1 = np.linspace(0.0, 170, 18)
    cl2 = np.linspace(100.0, 170, 8)
    contourf_levels = np.unique(np.sort(np.concatenate((cl1, cl2))))
    contourf_levels = np.linspace(0.0, 140, 15)
    #
    # Subplot ax0
    #
    ax0.contourf(graind, theta, snow_perm[idx_scont, idx_icont, idx_dens, :, :],
                         levels=contourf_levels, extend='max', cmap=cmap)
    if convert_m_to_mm:
        ax0.set_xlabel(r'grain diameter, $d_{grain}$ (mm)')
    else:
        ax0.set_xlabel(r'grain diameter, $d_{grain}$ (m)')
    ax0.set_ylabel(r'water saturation fraction, $\Theta$ (1)')
    ax0.set_title("$c_{{snow}}; c_{{ice}}=${:.4g}; ".format(snow_content[idx_scont])+
                  "{:.4g} kg m$^{{-2}}$\n".format(ice_content[idx_icont])+
                  "$\\rho_{{snow}}=${:.4g} kg m$^{{-3}}$".format(snow_density[idx_dens]))

    #
    # Subplot ax1
    #
    csf0b = ax1.contourf(ice_content, snow_content,
                         snow_perm[:, :, idx_dens, idx_theta, idx_graind],
                         levels=contourf_levels, extend='max', cmap=cmap)

    ax1.set_xlabel(r'$c_{ice}$ (kg m$^{-2}$)')
    ax1.set_ylabel(r'$c_{{snow}}$ (kg m$^{{-2}})$')
    ax1.set_title('$d_{{grain}}=${:.4g} mm'.format(graind[idx_graind])+
                  ', $\\theta=${:.2g}\n'.format(theta[idx_theta])+
                  '$\\rho_{{snow}}=${:.4g} kg m$^{{-3}}$'.format(snow_density[idx_dens]))

    fig1.colorbar(csf0b, ax=[ax0, ax1], label=r'snow permability') #' (kg m$^{{-2}}$)') TODO:Unit

    if plot_suffix:
        module4plots.plotting(fig1, plotname+'_2D', plot_suffix)

    #
    # Plot 2
    #
    fig2 = plt.figure(dpi=300, facecolor='white', figsize=(4.8, 8.4),
                      tight_layout=True)
    fig2.subplots_adjust(wspace=0.5)
    axs = fig2.subplots(3, 2)
    cmap = 'twilight'

    #
    # Axis[0, 0] : snow_perm[scont, icont, dens, theta, grain]
    plot00 = snow_perm[:, ::3, idx_dens, 0, 0]
    xvals00 = snow_content # [=1=,2,3,4,5]
    legstr00 = ['{:.2f}'.format(ss) for ss in ice_content[::3]] # [1,=2=,3,4,5]
    xlabel00 = '$c_{{snow}}$ (kg m$^{{-2}}$)'
    legtitle00 = '$c_{{ice}}$ (kg m$^{{-2}}$)'
    title00 = '$\\Theta$ = {:.1f}'.format(theta[0])+ \
        ', $d_{{grain}}$ = {:.1f}'.format(graind[0])

    # Axis[0, 1] : snow_perm[scont, icont, dens, theta, grain]
    plot01 = snow_perm[0, 0, idx_dens, ::3, :].T
    xvals01 = graind # [1,2,3,4,=5=]
    legstr01 = ['{:.1f}'.format(ss) for ss in theta[::3]] # [1,2,3,=4=,5]
    if convert_m_to_mm:
        xlabel01 = '$d_{{grain}}$ (mm)'
    else:
        xlabel01 = '$d_{{grain}}$ (m)'
    legtitle01 = '$\\Theta$ (1)'
    title01 = '$c_{{snow}}$ = {:.0f}'.format(snow_content[0])+ \
        ', $c_{{ice}}$ = {:.1f}'.format(ice_content[0])


    # Axis[1, 0] : snow_perm[scont, icont, dens, theta, grain]
    plot10 = snow_perm[0, 0, :, ::3, 0]
    xvals10 = snow_density # [1,2,=3=,4,5]
    legstr10 = ['{:.1f}'.format(ss) for ss in theta[::3]] # [1,2,3,=4=,5]
    xlabel10 = '$\\rho_{{snow}}$ (kg m$^{{-3}}$)'
    legtitle10 = '$\\Theta$ (1)'
    title10 = '$c_{{snow}}$ = {:.0f}'.format(snow_content[0])+ \
        ', $c_{{ice}}$ = {:.1f}'.format(ice_content[0])

    # Axis[1, 1] : snow_perm[scont, icont, dens, theta, grain]
    plot11 = snow_perm[0, 0, ::3, 0, :].T
    xvals11 = graind # [1,2,3,4,=5=]
    legstr11 = ['{:.0f}'.format(ss) for ss in snow_density[::3]] # [1,2,=3=,4,5]
    if convert_m_to_mm:
        xlabel11 = '$d_{{grain}}$ (mm)'
    else:
        xlabel11 = '$d_{{grain}}$ (m)'
    legtitle11 = '$\\rho_{{snow}}$ (kg m$^{{-3}}$)'
    title11 = '$c_{{snow}}$={:.0f}'.format(snow_content[0])+ \
        ', $c_{{ice}}$={:.0f}'.format(ice_content[0]) + \
        ', $\\Theta$={:.1f}'.format(theta[0])

    # Axis[2, 0] : snow_perm[scont, icont, dens, theta, grain]
    plot20 = snow_perm[0, ::2, 0, 0, :].T
    xvals20 = graind # [1,2,3,4,=5=]
    legstr20 = ['{:.1f}'.format(ss) for ss in ice_content[::2]] # [1,=2=,3,4,5]
    if convert_m_to_mm:
        xlabel20 = '$d_{{grain}}$ (mm)'
    else:
        xlabel20 = '$d_{{grain}}$ (m)'
    legtitle20 = '$c_{{ice}}$ (kg m$^{{-2}}$)'
    title20 = '$c_{{snow}}$ = {:.0f}'.format(snow_content[0])+ \
        ', $\\Theta$ = {:.1f}'.format(theta[0])

    # Axis[2, 1] : snow_perm[scont, icont, dens, theta, grain]
    plot21 = snow_perm[0, ::2, 0, :, 0].T
    xvals21 = theta # [1,2,3,=4=,5]
    legstr21 = ['{:.1f}'.format(ss) for ss in ice_content[::2]] # [1,=2=,3,4,5]
    xlabel21 = '$\\Theta$ (1)'
    legtitle21 = '$c_{{ice}}$ (kg m$^{{-2}}$)'
    title21 = '$c_{{snow}}$ = {:.0f}'.format(snow_content[0]) + \
        ', $d_{{grain}}$ = {:.1f}'.format(graind[0])


    pid00 = axs[0, 0].plot(xvals00, plot00, linewidth=2)
    pid01 = axs[0, 1].plot(xvals01, plot01, linewidth=2)
    pid10 = axs[1, 0].plot(xvals10, plot10, linewidth=2)
    pid11 = axs[1, 1].plot(xvals11, plot11, linewidth=2)
    pid20 = axs[2, 0].plot(xvals20, plot20, linewidth=2)
    pid21 = axs[2, 1].plot(xvals21, plot21, linewidth=2)

    for axx in axs[1, :]:
        axx.set_ylabel('snow permeability')

    for axx in axs[:, 1]:
        axx.yaxis.set_label_position('right')
        axx.yaxis.tick_right()

    axs[0, 0].set_xlabel(xlabel00)
    axs[0, 1].set_xlabel(xlabel01)
    axs[1, 0].set_xlabel(xlabel10)
    axs[1, 1].set_xlabel(xlabel11)
    axs[2, 0].set_xlabel(xlabel20)
    axs[2, 1].set_xlabel(xlabel21)

    axs[0, 0].legend(pid00, legstr00, title=legtitle00)
    axs[0, 1].legend(pid01, legstr01, title=legtitle01)
    axs[1, 0].legend(pid10, legstr10, title=legtitle10)
    axs[1, 1].legend(pid11, legstr11, title=legtitle11)
    axs[2, 0].legend(pid20, legstr20, title=legtitle20)
    axs[2, 1].legend(pid21, legstr21, title=legtitle21)

    axs[0, 0].set_title(title00, fontsize='medium')
    axs[0, 1].set_title(title01, fontsize='medium')
    axs[1, 0].set_title(title10, fontsize='medium')
    axs[1, 1].set_title(title11, fontsize='medium')
    axs[2, 0].set_title(title20, fontsize='medium')
    axs[2, 1].set_title(title21, fontsize='medium')

    fig2.suptitle('Snow permeability for different\nparameter combinations: '+
                  '$\\rho_{{snow}}$ = {:.0f} (kg m$^{{-3}}$)'.format(snow_density[idx_dens]))

    if plot_suffix:
        module4plots.plotting(fig2, plotname+'_line', plot_suffix)


# -------------------------------------------------------------------------
def plot_nfunc(library_name, plot_suffix=None, plotname='nfunc'):
    '''
    Plot results of the via c-binding call Fortran function nfunc

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'nfunc'.

    Returns
    -------
    None.

    '''
    print('* n-function')

    graind = np.linspace(0.45, 2.55, 43)*0.001 # Meter!!
    nfunc_array = mod_physic_column.nfunc(graind, library_name)

    convert_m_to_mm = True
    if convert_m_to_mm:
        graind = graind * 1000.

    #
    # Plot 1
    #
    fig1 = plt.figure(dpi=300, facecolor='white')
    ax0 = fig1.subplots()

    ax0.plot(graind, nfunc_array, linewidth=2, color='navy')
    if convert_m_to_mm:
        ax0.set_xlabel(r'grain diameter, $d_{grain}$ (mm)')
    else:
        ax0.set_xlabel(r'grain diameter, $d_{grain}$ (m)')
    ax0.set_ylabel(r'n-function, $n$ (1)')

    if plot_suffix:
        module4plots.plotting(fig1, plotname, plot_suffix)


# -------------------------------------------------------------------------
def plot_mfunc(library_name, plot_suffix=None, plotname='mfunc'):
    '''
    Plot results of the via c-binding call Fortran function mfunc

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'mfunc'.

    Returns
    -------
    None.

    '''
    print('* m-function')

    graind = np.linspace(0.45, 2.55, 43)*0.001  # Meter !
    nfunc_array = mod_physic_column.nfunc(graind, library_name)
    mfunc_array = mod_physic_column.mfunc(nfunc_array, library_name)

    convert_m_to_mm = True
    if convert_m_to_mm:
        graind = graind * 1000.

    #
    # Plot 1
    #
    fig1 = plt.figure(dpi=300, facecolor='white')
    ax0 = fig1.subplots()

    ax0.plot(nfunc_array, mfunc_array, linewidth=2, color='darkgreen')
    ax0.set_xlabel(r'n-function, $n$ (1)')
    ax0.set_ylabel(r'm-function, $m$ (1)')

    #
    # Parallel to the n-function values on the x-axis, we write the
    # corresponding grain diameter values
    #
    secax0 = ax0.secondary_xaxis('top')
    secax0.set_xticks(nfunc_array[::6])
    graind_str = ['{:.2f}'.format(grd) for grd in graind[::6]]
    secax0.set_xticklabels(graind_str)
    secax0.tick_params(axis='x', colors='dimgray')
    if convert_m_to_mm:
        secax0.set_xlabel(r'grain diameter, $d_{grain}$ (mm)', color='dimgray')
    else:
        secax0.set_xlabel(r'grain diameter, $d_{grain}$ (m)', color='dimgray')


    if plot_suffix:
        module4plots.plotting(fig1, plotname, plot_suffix)


# -------------------------------------------------------------------------
def plot_dgrain_dt(library_name, plot_suffix=None, plotname='dgrain_dt'):
    '''
    Plot results of the via c-binding call Fortran function dgrain_dt

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'dgrain_dt'.

    Returns
    -------
    None.

    '''
    print('* dgrain_dt')

    number_of_elements = 201
    graind = np.linspace(0.4, 2.1, number_of_elements)*0.001
    water_content = np.linspace(0, 10, number_of_elements)
    snow_content = np.zeros((number_of_elements)) + 100.

    ratio_water2snow = water_content/snow_content

    # Loop of calculations for different combinations
    dgraindt = np.zeros((len(graind), len(ratio_water2snow)))
    for i_gd, grd in enumerate(graind):
        grain_array = np.zeros_like(snow_content)+grd
        dgraindt[i_gd, :] = mod_physic_column.dgrain_dt(grain_array,
                                                        snow_content,
                                                        water_content,
                                                        library_name)
    #
    # Loop of a history
    #
    dtime = 3600*24
    number_of_timesteps = 365*86400/dtime*2
    time_array_day = np.arange(0, int(number_of_timesteps))*dtime/(86400.)
    ratio_water2snow_history = np.flipud(np.array([0.0, 0.02, 0.04, 0.06, 0.08]))
    snow_content2 = np.zeros_like(ratio_water2snow_history) + 100.
    water_content2 = snow_content2*ratio_water2snow_history

    dgraindt_hist = np.zeros((int(number_of_timesteps),
                              len(ratio_water2snow_history))) + \
        DEFAULT_NEW_GRAIN_DIAMETER
    for itime in range(1,int(number_of_timesteps)):
        change_dgraindt = mod_physic_column.dgrain_dt(dgraindt_hist[itime-1, :],
                                                      snow_content2,
                                                      water_content2)
        dgraindt_hist[itime, :] = dgraindt_hist[itime-1, :]+change_dgraindt*dtime

    convert_m_to_mm = True
    convert_m_sec_to_mm_week = True

    if convert_m_to_mm:
        graind = graind*1000.
        dgraindt_hist = dgraindt_hist*1000.
    if convert_m_sec_to_mm_week:
        dgraindt = dgraindt*(1000*86400*7)

    #
    # Plot 1
    #
    fig1 = plt.figure(dpi=300, facecolor='white')
    ax0 = fig1.subplots()

    # Adjust the colormap
    contourf_levels = [0., 0.01, 0.02, 0.03, 0.05, 0.07, 0.1, 0.15, 0.2, 0.25]
    if not convert_m_sec_to_mm_week:
        contourf_levels = np.array(contourf_levels)/(1000*86400*7)
    cmap = mpl.cm.YlOrRd   # = 'YlOrRd'
    norm_color = mpl.colors.BoundaryNorm(boundaries=contourf_levels, ncolors=cmap.N)

    csf0 = ax0.contourf(ratio_water2snow, graind, dgraindt, cmap=cmap,
                          levels=contourf_levels, extend='max', norm=norm_color)

    ax0.set_xlabel(r'Ratio of water to snow content $\left(\frac{c_{water}}{c_{snow}}\right)$')
    if convert_m_to_mm:
        ax0.set_ylabel(r'snow grain diameter, $d_{grain}$ (mm)')
    else:
        ax0.set_ylabel(r'snow grain diameter, $d_{grain}$ (m)')
    if convert_m_sec_to_mm_week:
        fig1.colorbar(csf0, ax=[ax0],
                      label=r'$\frac{\Delta\, d_{grain}}{\Delta\. t}$ (mm week$^{-1}$)')
    else:
        fig1.colorbar(csf0, ax=[ax0],
                      label=r'$\frac{\Delta\, d_{grain}}{\Delta\. t}$ (m s$^{-1}$)')

    #
    # 2nd axis
    #
    secax0 = ax0.secondary_xaxis('top')
    secax0.set_xticks(ratio_water2snow[::50])
    wcont_str = ['{:.5g}'.format(grd) for grd in water_content[::50]]
    secax0.set_xticklabels(wcont_str)
    secax0.tick_params(axis='x', colors='dimgray')
    secax0.set_xlabel(r'$c_{{water}}$ (kg m$^{-2}$)', color='dimgray')

    if plot_suffix:
        module4plots.plotting(fig1, plotname+'_2D', plot_suffix)

    #
    # Plot 2 (temporal evolution)
    #
    fig2 = plt.figure(dpi=300, facecolor='white')
    ax0 = fig2.subplots()

    idp = ax0.plot(time_array_day, dgraindt_hist, linewidth=2)

    ax0.legend(idp, ratio_water2snow_history,
               title=r'Ratio $\left(\frac{c_{water}}{c_{snow}}\right)$')

    ax0.set_xlabel('Time (day)')
    if convert_m_to_mm:
        ax0.set_ylabel(r'snow grain diameter, $d_{grain}$ (mm)')
        ax0.text(number_of_timesteps*0.05, dgraindt_hist[0,0],
                 r'$d_{{grain}}(t=0) =${:.1g} mm'.format(dgraindt_hist[0,0]))
    else:
        ax0.set_ylabel(r'snow grain diameter, $d_{grain}$ (m)')
        ax0.text(number_of_timesteps*0.05, dgraindt_hist[0,0],
                 r'$d_{{grain}}(t=0) =${:.1g} m'.format(dgraindt_hist[0,0]))


    ax0.set_title('Temporal evolution of the snow grain diameter $d_{grain}$')

    if plot_suffix:
        module4plots.plotting(fig2, plotname+'_line', plot_suffix)


# -------------------------------------------------------------------------
def plot_darcy_flow(library_name, plot_suffix=None, plotname='darcy_flow'):
    '''
    Plot results of the via c-binding call Fortran function darcy_flow

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'darcy_flow'.

    Returns
    -------
    None.

    '''
    print('* darcy_flow')

    #
    # Build a typical density profile with depth
    #
    depth = np.linspace(0,65,131)
    snow_density = mod_physic.densprofile(depth, RHO_SNOW, RHO_ICE, CDENSITY,
                                          library_name)

    dtime = 3600.*6 *0.001 # Seconds
    thickness = np.ones_like(depth)
    graind = np.array([0.0005, 0.001, 0.002]) # Meter
    snow_content = np.linspace(100, 10, 37)
    ice_content = snow_content.max()-snow_content
    water_content = snow_content.max()*np.array([0.005, 0.01, 0.02]) #, 0.05])

    snow_percent = 100.0*snow_content/(snow_content+ice_content)

    # Loop of calculations for different combinations
    # Preallocate of results
    darcyflow = np.zeros((len(graind), len(water_content), len(ice_content),
                          len(snow_density)))
    for i_grain, grain in enumerate(graind):
        grain_array = np.zeros_like(snow_density)+grain
        for i_wcont, wcont in enumerate(water_content):
            wcont_array = np.zeros_like(snow_density)+wcont
            for i_icont, icont in enumerate(ice_content):
                icont_array = np.zeros_like(snow_density)+icont
                scont_array = np.zeros_like(snow_density)+snow_content[i_icont]
                darcyflow[i_grain, i_wcont, :] = \
                    mod_physic_column.darcy_flow(dtime,
                                                 grain_array,
                                                 snow_density,
                                                 scont_array,
                                                 icont_array,
                                                 wcont_array,
                                                 thickness)

    convert_m_to_mm = True
    convert_m_sec_to_mm_week = False #  CURRENTLY default: False

    if convert_m_to_mm:
        graind = graind*1000.
    if convert_m_sec_to_mm_week:
        darcyflow = darcyflow*(1000*86400*7) #TODO: UNIT??

    #
    # Plot 1
    #
    fig1 = plt.figure(dpi=300, facecolor='white', figsize=(5., 8.))

    fig1.subplots_adjust(wspace=0.3, hspace=0.175)
    axs = fig1.subplots(len(water_content), len(graind), sharex=True, sharey=True)
    cmap = 'PuBu'

    #contourf_levels = np.linspace(darcyflow.min(), darcyflow.max(), 11)
    depth_min = depth.min()
    depth_max = depth.max()

    for i_wcont, wcont in enumerate(water_content):
        for i_grain, grain in enumerate(graind):
            ax0 = axs[i_wcont, i_grain]  # Current axis handle

            # darcyflow[grain, water_content, snow/ice_content, depth/snow_density]
            csf = ax0.contourf(snow_percent, depth,
                               darcyflow[i_grain, i_wcont,:,:].T, cmap=cmap) #,
                               # levels=contourf_levels)

            ax0.set_ylim(depth_max, depth_min) # High to low: inverts direction

            if i_wcont <= 0:
                if convert_m_to_mm:
                    ax0.set_title('$d_{{grain}}$\n{:.2g} mm'.format(grain))
                else:
                    ax0.set_title('$d_{{grain}}$\n{:.2g} m'.format(grain))#, ha='right')
            if i_grain == 0:
                ax0.set_ylabel('$c_{{water}}=${:.3g}'.format(wcont),
                               fontsize='x-large')
        #
        # Each row gets a y-axis label and its own independent colorbar (left)
        #
        fig1.colorbar(csf, ax=axs[i_wcont, :], shrink=0.95)
        axs[i_wcont, 1].set_ylabel('Depth (m)')

    axs[len(water_content)-1, 1].set_xlabel('Snow Percent (%):'+
                                          '$\\frac{c_{{snow}}}{c_{{ice}}+c_{{snow}}}$')
    axs[0, 0].set_xlabel('$d_{{gain}}(t=0)$', fontsize='small')


    #
    # The SUPerTITLE
    #
    # fig1.suptitle('Darcy flow; density profile increases with depth ($C_d=0.024$)\n'+
    #               r' $\rho(d) = \rho_{ice} - (\rho_{ice}-\rho_{snow}) \exp(-C_d \cdot d)$')
    fig1.suptitle('Darcy flow: For different grain sizes\n'+
                  ' $d_{grain}$ and water content $c_{water}$')

    #
    # Density profile as "on-the-top" plot
    #
    add_axes = inset_axes(axs[1, 1], width="100%", height="100%", loc=10)
    add_axes.patch.set_alpha(0.1)
    add_axes.plot(snow_density, depth, 'r--', linewidth=2)
    add_axes.set_ylim(depth_max, depth_min)
    add_axes.xaxis.set_label_position('bottom')
    add_axes.xaxis.tick_top()
    add_axes.yaxis.set_visible(False)
    add_axes.set_xticks([np.floor(snow_density.min()), np.ceil(snow_density.max())])
    add_axes.tick_params(axis='x', colors='r')
    add_axes.set_xlabel(r'$\rho_{snow}$ (kg m$^{-3}$)', color='r')

    if plot_suffix:
        module4plots.plotting(fig1, plotname, plot_suffix)


# -------------------------------------------------------------------------
def plot_calc_refreeze(library_name, plot_suffix=None, plotname='calc_refreeze'):
    '''
    Plot results of the via c-binding call Fortran function calc_refreeze

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'calc_refreeze'.

    Returns
    -------
    None.

    '''
    print('* Refreezing')

    ice_content = np.linspace(10, 100, 61)
    snow_content = ice_content.max()-ice_content
    water_content = np.linspace(0, 10, 101) #91)
    temperature = np.flipud(273.15-np.linspace(0,20,79))
    melt_temp = FREEZING_TEMP

    # Loop of calculations for different combinations
    refreeze_multi = np.zeros((len(snow_content),
                               len(water_content),
                               len(temperature)))
    for i_scont, scont in enumerate(snow_content):
        icont = ice_content[i_scont]
        for i_wcont, wcont in enumerate(water_content):
            scont_array = np.zeros_like(temperature)+scont
            icont_array = np.zeros_like(temperature)+icont
            wcont_array = np.zeros_like(temperature)+wcont
            melttemp_array = np.zeros_like(temperature)+melt_temp
            refreeze_multi[i_scont, i_wcont, :] = \
                mod_physic_column.calc_refreeze(scont_array, icont_array,
                                                wcont_array, temperature,
                                                melttemp_array, library_name)

    convert_Kelvin_to_relativ_to_freezing = True
    if convert_Kelvin_to_relativ_to_freezing:
        temperature = temperature - 273.15

    #
    # Plot 1
    #
    fig1 = plt.figure(dpi=300, facecolor='white')
    fig1.subplots_adjust(hspace=0.3)
    ax0, ax1 = fig1.subplots(2, 1, sharex=True)
    cmap = 'PuBu' #'Purples'
    cmap_lines = 'inferno' #'Reds'
    contourf_levels = np.linspace(0, 9, 10)
    contour_levels = [0., 33, 66]

    #
    # Computation of the field to be shown
    #
    indx_wcont = int(( np.where(abs(water_content-7.5) == min(abs(water_content-7.5))) )[0])
    indx_scont = int(( np.where(abs(snow_content-90.) == min(abs(snow_content-90.))) )[0])

    t2d, water_content_2d = np.meshgrid(temperature, water_content)#, sparse=True)
    del t2d

    # refreeze_multi[scont, wcont, temp]
    field_ax0 = refreeze_multi[:, indx_wcont, :].squeeze()
    cont_ax0 = 100.*(water_content[indx_wcont]-field_ax0)/water_content[indx_wcont]

    field_ax1 = refreeze_multi[indx_scont,:, :].squeeze()
    # Throws and: RuntimeWarning: invalid value encountered in true_divide
    #                     100.*(water_content_2d-field_ax1)/water_content_2d,
    cont_ax1 = np.where(water_content_2d > 0.001,
                        100.*(water_content_2d-field_ax1)/np.maximum(water_content_2d, 1.e-16),
                        0.0)


    #
    # Subplot ax0
    #
    ax0.contourf(temperature, snow_content, field_ax0, levels=contourf_levels,
                 extend='max', cmap=cmap)
    csf0b = ax0.contour(temperature, snow_content, cont_ax0,
                        levels=contour_levels, cmap=cmap_lines)
    ax0.clabel(csf0b, contour_levels, fmt='%1.f', colors='k')

    # if convert_Kelvin_to_relativ_to_freezing:
    #     ax0.set_xlabel(r'Temperature below freezing (K)')
    # else:
    #     ax0.set_xlabel(r'Temperature (K)')
    ax0.set_ylabel('$c_{{snow}}$ (kg m$^{{-2}}$)')
    ax0.set_title(r'$c_{{water}}$ = {:.3g} kg m$^{{-2}}$'.format(water_content[indx_wcont]))

    ax0.text(temperature[1], snow_content.mean(),
             '$\\Sigma c_{{snow}}+c_{{ice}}=100$', rotation=90, va='center')

    #
    # Subplot ax0
    #
    csf1a = ax1.contourf(temperature, water_content, field_ax1,
                         levels=contourf_levels, extend='max', cmap=cmap)
    csf1b = ax1.contour(temperature, water_content, cont_ax1,
                        levels=contour_levels, cmap=cmap_lines)
    ax1.clabel(csf1b, contour_levels, fmt='%1.f', colors='k')

    if convert_Kelvin_to_relativ_to_freezing:
        ax1.set_xlabel(r'Temperature below freezing (K)')
    else:
        ax1.set_xlabel(r'Temperature (K)')
    ax1.set_ylabel(r'$c_{water}$ (kg m$^{-2}$)')
    ax1.set_title(r'$c_{{ice}}$ = {:.3g} kg m$^{{-2}}$'.format(ice_content[indx_scont])+
                  ' and $c_{{snow}}$ = {:.3g} kg m$^{{-2}}$'.format(snow_content[indx_scont]))

    ax1.text(temperature[2], water_content.mean(),
             'contour line:\n percentage of\n remaining unfrozen water',
             ha='left', va='top')

    #
    # All subfigures colorbar
    #
    fig1.colorbar(csf1a, ax=[ax0, ax1],
                  label=r'Refreezing (kg m$^{{-2}}$)')
    if plot_suffix:
        module4plots.plotting(fig1, plotname+'_2D', plot_suffix)


# -------------------------------------------------------------------------
def plot_calc_waterflowdown(library_name, plot_suffix=None,
                            plotname='calc_waterflowdown'):
    '''
    Plot results of the via c-binding call Fortran function calc_waterflowdown

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'calc_waterflowdown'.

    Returns
    -------
    None.

    '''
    print('* calc_waterflowdown')

    snow_density0 = np.linspace(200, 900, 5) #np.linspace(250, 900, 6)
    snow_content0 = np.linspace(100, 25, 4) #/np.linspace(100, 0, 5) #(100, 25, 4) #(100, 20, 5)
    ice_content0 = snow_content0.max()-snow_content0
    water_content0 = np.array([0.01, 0.02, 0.05, 0.1, 0.2, 0.5, 1., 2., 5.])
    darcyflow0 = 0. # = np.zeros_like(snow_density)
    do_darcyflow0 = False ##True # np.ones_like(darcyflow, dtype=bool)

    def zero_array2full_array(array):
        rep = len(array)
        upper_branch = np.tile(array, rep)
        lower_branch = np.repeat(array, rep)
        return np.stack((upper_branch, lower_branch), axis=1)

    snow_content = zero_array2full_array(snow_content0)
    ice_content = zero_array2full_array(ice_content0)
    water_content = zero_array2full_array(water_content0)
    snow_density = zero_array2full_array(snow_density0)

    #
    # Loop of calculations for different combinations
    #
    DO_RETURN_THREE = True # TODO Must be identical here and in cbind_mod_physic_column.py
    #
    # Preallocate the output variables
    waterflow = np.zeros((len(ice_content), len(water_content), len(snow_density)))
    darcyflow = np.zeros_like(waterflow)
    do_runoff = np.zeros_like(waterflow, dtype=bool)
    #
    # The loop
    for i_scont, scont in enumerate(snow_content):
        i_icont = i_scont
        icont = ice_content[i_scont, :]
        for i_wcont, wcont in enumerate(water_content):
            for i_sdens, sdens in enumerate(snow_density):
                if DO_RETURN_THREE:
                    waterflow[i_icont, i_wcont, i_sdens], \
                    darcyflow[i_icont, i_wcont, i_sdens], \
                    do_runoff[i_icont, i_wcont, i_sdens] = \
                    mod_physic_column.calc_waterflowdown(scont, icont, wcont,
                                                         sdens, darcyflow0,
                                                         do_darcyflow0,
                                                         library_name)
                else:
                    waterflow[i_icont, i_wcont, i_sdens] = \
                    mod_physic_column.calc_waterflowdown(scont, icont, wcont,
                                                         sdens, darcyflow0,
                                                         do_darcyflow0,
                                                         library_name)

    #
    # Plot 1
    #
    fig1 = plt.figure(dpi=300, facecolor='white', figsize=(6.4, 10.2))
    fig1.subplots_adjust(hspace=0.3)  #(wspace=0.4)
    axss = fig1.subplots(4, 2) ##, sharex=True) # (right-left, top-bottom)

    for i_scont, scont in enumerate(snow_content0[snow_content0>0]):
        idx_snow_T =  np.where(snow_content[:, 0] == scont)[0]  # snow_content.shape: (16, 2)

        #
        # LEFT: x-axis = water_content
        #
        idx_dens_T = np.where(snow_density[:, 0] == 550 )[0]

        colors_density = ['blue', 'c', 'g', 'y', 'r']
        density_loop1 = enumerate(np.flipud(snow_density[idx_dens_T, 1].squeeze()))
        density_loop2 = enumerate(np.flipud(snow_density[idx_dens_T, 1].squeeze()))
        # NOTE: Using density_loop1 two-times does not work
        # NOTE: density_loop2 = density_loop1 does not work
        for i_dens_B, dens_B in density_loop1:
            #-----------------------------------
            #[i_icont, i_wcont, i_sdens]
            yval = waterflow[idx_snow_T, :, i_dens_B].ravel()
            imul = int(len(yval)/len(water_content[:,1]))
            xval = np.tile(water_content[:,1], imul) * ((np.random.random(1)*0.2)-0.1+1)
            axss[i_scont, 0].plot(xval, yval, '*', c=colors_density[i_dens_B])
            #-----------------------------------
            # for i_scont_B0, scont_B in enumerate(snow_content[idx_snow_T, 1].squeeze()):
            #     i_scont_B = int(np.where(snow_content[idx_snow_T, 1].squeeze() == scont_B)[0])
            #     yval = waterflow[idx_snow_T[i_scont_B], :, i_dens_B].ravel()
            #     imul = int(len(yval)/len(water_content[:,1]))
            #     xval = np.tile(water_content[:,1], imul) * ((np.random.random(1)*0.2)-0.1+1)
            #     axss[i_scont, 0].plot(xval, yval, linewidth=0,
            #                           marker=markers_scont[i_scont_B],
            #                           c=colors_density[i_dens_B])
            #-----------------------------------

        axss[i_scont, 0].set_xscale('log')
        axss[i_scont, 0].set_xlim(water_content.min()*0.85, water_content.max()*1.15)
        axss[i_scont, 0].set_title(r'$c_{{snow}}(top)=${:.0f}'.format(scont))

        if i_scont == 0:
            legend_str_density = []
            for i_dens_B, dens_B in density_loop2:
                legend_str_density.append('{:.0f}'.format(dens_B))
            axss[i_scont, 0].legend(legend_str_density, title=r'$\rho_{{snow}}(top)$')
        if i_scont == len(snow_content0)-1:
            axss[i_scont, 0].set_xlabel(r'$c_{{water}}$')
        axss[i_scont, 0].set_ylabel('Water flow')

        #
        # RIGHT: x-axis = snow_density
        #
        idx_water_T = np.where(abs(water_content0-1.) == min(abs(water_content0-1.)))[0]


        colors_density = ['C1', 'C2', 'C3', 'C4', 'C5', 'C6', 'C7', 'C8', 'C9']
        # markers_scont = ['<', '>', '^', 'v']
        wcont_loop1 = enumerate(np.flipud(water_content[idx_dens_T, 1].squeeze()))
        wcont_loop2 = enumerate(np.flipud(water_content[idx_dens_T, 1].squeeze()))
        # NOTE: Using wcont_loop1 two-times does not work
        # NOTE: wcont_loop2 = wcont_loop1 does not work
        for i_wcont_B, wcont_B in wcont_loop1:
            #-----------------------------------
            #[i_icont, i_wcont, i_sdens]
            yval = waterflow[idx_snow_T, i_wcont_B, :].ravel()
            imul = int(len(yval)/len(snow_density[:,1]))
            xval = np.tile(snow_density[:,1], imul) * ((np.random.random(1)*0.05)-0.025+1)
            axss[i_scont, 1].plot(xval, yval, '*', c=colors_density[i_wcont_B])

        axss[i_scont, 1].set_xlim(snow_density.min()*0.85, snow_density.max()*1.15)
        axss[i_scont, 1].set_title(r'$c_{{snow}}(top)=${:.0f}'.format(scont))

        if i_scont == 1:
            legend_str_wcont = []
            for i_wcont_B, wcont_B in wcont_loop2:
                legend_str_wcont.append('{:.3g}'.format(wcont_B))
            axss[i_scont, 1].legend(legend_str_wcont, title=r'$c_{{water}}(top)$')
        if i_scont == len(snow_content0)-1:
            axss[i_scont, 1].set_xlabel(r'$\rho_{{snow}}$')
        axss[i_scont, 1].set_ylabel('Water flow')
        axss[i_scont, 1].yaxis.set_label_position('right')
        axss[i_scont, 1].yaxis.tick_right()

    if plot_suffix:
        module4plots.plotting(fig1, plotname, plot_suffix)


# -------------------------------------------------------------------------
# @todo TODO:HD Check if Obsolet when calc_diffusion is replaced by an implicit scheme
def plot_heatflux_between_layers(library_name, plot_suffix=None,
                                 plotname='heatflux_between_layers'):
    '''
    Plot results of the via c-binding call Fortran function heatflux_between_layers

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'heatflux_between_layers'.

    Returns
    -------
    None.

    '''
    print('* heatflux_between_layers')



    thickness1 = np.array([0.1, 0.2, 0.4, 0.7, 1.0])
    temperature1 = np.linspace(-10, 0, 21)
    temperature2 = np.concatenate((np.linspace(-10, 0 ,11),
                                   np.linspace(0,-9,10)), axis=0)
    conductivity1 = np.array([0.2, 0.5, 1.0, 2.0])

    # Deduced variables
    thickness2 = np.ones_like(thickness1)
    conductivity2 = np.zeros_like(conductivity1) + 0.5
    heatflux = np.zeros((len(thickness1),
                         len(conductivity1),
                         len(temperature1)))

    #
    # Compute the heat flux via CISSEMBEL function/subroutine
    #
    for ithk, thk1  in enumerate(thickness1):
        for icond, cond1  in enumerate(conductivity1):
            for itemp, temp1 in enumerate(temperature1):
                thick = np.array([thk1, thickness2[ithk]])
                temp = np.array([temp1, temperature2[itemp]])
                cond = np.array([cond1, conductivity2[icond]])
                hflx2 = mod_physic_column.heatflux_between_layers(thick, temp, cond)
                heatflux[ithk, icond, itemp] = hflx2[0]


    #
    # Some values used for standard conditions.
    #
    itemp0 = 1
    ithk0 = -1
    icond0 = 0

    #
    # Plot 1
    #
    fig1 = plt.figure(dpi=300, facecolor='white', figsize=(6.2, 10.2))
    fig1.subplots_adjust(hspace=0.05, wspace=0.1)  #(wspace=0.4)

    # Specification about subplots
    heights = [0.2, 0.2, 0.3, 0.32]
    specs4x1 = fig1.add_gridspec(ncols=1, nrows=len(heights), height_ratios=heights)
    axss = []
    for spec in specs4x1: # for ispec, spec in enumerate(specs4x1):
        ax_ = fig1.add_subplot(spec)
        axss.append(ax_)

    # Subplot: Temperatures
    axss[0].plot(temperature1, '-', linewidth=3, label='Upper: $T_{up}$')
    axss[0].plot(temperature2, '--', linewidth=3, label='Lower: $T_{down}$')
    axss[0].set_ylabel('Layer temperature ($^{\\circ}C$)')
    axss[0].legend(title='Layer')
    axss[0].set_title('Resulting heat flux for different layer temperatures')

    # Subplot: Temperature difference
    axss[1].plot(temperature1-temperature2, linestyle='-.',
                 label='$\\Delta T$', linewidth=3)
    axss[1].plot(np.zeros_like(temperature2), ':', color='gray', linewidth=3,
                 label='zero')
    axss[1].set_ylabel('$T_{up} - T_{down}$\n($^{\\circ}C$)')
    axss[1].annotate('Temperature difference: $\\Delta T$', (1, 0.99),
                     xycoords='data', va='bottom', ha='left')


    # Subplot: Dependence on conductivity
    for icond, cont in enumerate(conductivity1):
        axss[2].plot(heatflux[ithk0, icond, :], linewidth=3,
                     label='{:3.1f}'.format(cont))
    axss[2].set_ylabel('Heat flux ($W\: m^{-2}$)')
    axss[2].legend(title='$\\kappa_{{up}}$ ($W\,m^{-2}\,s^{-1}$)',
                   loc='lower center')
    axss[2].plot(np.zeros_like(heatflux[ithk0, icond0, :]), ':', color='gray',
                 linewidth=3, label='zero')
    axss[2].annotate('$\\kappa_{{down}}$ = {:3.1f} $W\: m^{{-2}}\,s^{{-1}}$'.format(conductivity2[icond0])+
                     '\n $d_{{up}}$ = {:3.1f} $m$'.format(thickness1[ithk0])+
                     '\n $d_{{down}}$ = {:3.1f} $m$'.format(thickness2[ithk0]),
                     (len(temperature1)*0.92, 0.75), xycoords='data',
                     va='bottom', ha='right')
    axss[2].text(1,-2.5, 'Dependence on\n'+'conductivity: $\\kappa$')

    # Subplot: Dependence on conductivity
    for ithk, thk in enumerate(conductivity1):
        axss[3].plot(heatflux[ithk, icond0, :], linewidth=3,
                     label='{:3.1f}'.format(thk))
    axss[3].set_ylabel('Heat flux ($W\: m^{-2}$)')
    axss[3].legend(title='$d_{{up}}$ ($m$)', loc='lower center')
    axss[3].plot(np.zeros_like(heatflux[ithk, icond0, :]), ':', color='gray',
                 linewidth=3, label='zero')
    axss[3].annotate('$\\kappa_{{up}}$ = {:3.1f} $W\: m^{{-2}}\,s^{{-1}}$'.format(conductivity1[icond0])+
                     '\n $\\kappa_{{down}}$ = {:3.1f} $W\: m^{{-2}}\,s^{{-1}}$'.format(conductivity2[icond0])+
                     '\n $d_{{down}}$ = {:3.1f} $m$'.format(thickness2[0]),
                     (len(temperature1)*0.92, 0.75), xycoords='data',
                     va='bottom', ha='right')
    axss[3].text(1,-2.5, 'Dependence on\n'+'layer thickness: $d$', ha='left')

    # General adjustments
    axss[3].set_xlabel('Different temperature scenarios')
    for ax_ in axss:
        ax_.set_xlim([-0.05, len(temperature1)-0.95])
        ax_.axes.xaxis.set_visible(False)

    if plot_suffix:
        module4plots.plotting(fig1, plotname, plot_suffix)


#==========================================================================
# -------------------------------------------------------------------------
def plot_thermal_diffusion(library_name, plot_suffix=None,
                           plotname='thermal_diffusion'):
    '''
    Plot results of the via c-binding call Fortran function thermal_diffusion

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'thermal_diffusion'.

    Returns
    -------
    None.

    '''
    print('* thermal_diffusion')

    snow_density = 450.  #RHO_SNOW  # snow density (kg m-3)
    ice_density = RHO_ICE      # ice density (kg m-3)
    water_density = RHO_FWATER # water density (kg m-3)
    snow_fraction = 0.90  # snow fraction in each layer
    ice_fraction = 0.09   # ice fraction in each layer
    water_fraction = 0.01 # water fraction in each layer


    delta_time = 9*3600. # 6-hourly # time step (Seconds)
    ntimes = np.intp((86400.*100.)/delta_time)
    times = np.linspace(0.0, float(ntimes)*delta_time, ntimes)

    thickness1 = np.array([1.0, 2.0]) # thickness of upper layer (m)
    temperature1 = np.array([-1., -9.]) + FREEZING_TEMP # temperature of upper layer (degC)
    temperature2 = np.array([-6.5, -5.]) + FREEZING_TEMP # temperature of lower layer (degC)
    conductivity1 = np.array([0.5, 2.0])

    # Deduced variables
    snow_content1 = thickness1*(snow_fraction*snow_density)
    ice_content1 = thickness1*(ice_fraction*ice_density)
    water_content1 = thickness1*(water_fraction*water_density)

    thickness2 = np.ones_like(thickness1)
    snow_content2 = thickness2*(snow_fraction*snow_density)
    ice_content2 = thickness2*(ice_fraction*ice_density)
    water_content2 = thickness2*(water_fraction*water_density)
    conductivity2 = np.zeros_like(conductivity1) + 0.5

    temp1_time = np.zeros((len(thickness1),
                           len(conductivity1),
                           len(temperature1),
                           int(ntimes)))
    temp2_time = np.zeros((len(thickness2),
                           len(conductivity2),
                           len(temperature2),
                           int(ntimes)))

    temp1_time[:, :, :, 0] = temperature1 # upper layer temp. history (Kelvin)
    temp2_time[:, :, :, 0] = temperature2 # lower layer temp. history (Kelvin)

    ilayer = 2
    heatflux_top = np.zeros((len(thickness1), len(conductivity1),
                             len(temperature1), ilayer))
    heatflux_bottom = np.zeros((len(thickness1), len(conductivity1),
                                len(temperature1), ilayer))

    hflx2B = np.zeros(ilayer)
    hflx2T = np.zeros(ilayer)
    for itime in range(ntimes-1):
        for ithk in range(len(thickness1)):
            for icond in range(len(conductivity1)):
                for itemp in range(len(temperature1)):
                    #
                    # First: compute the heatflux between layers
                    #
                    thick = np.array([thickness1[ithk], thickness2[ithk]])
                    temp = np.array([temp1_time[ithk, icond, itemp, itime],
                                     temp2_time[ithk, icond, itemp, itime]])
                    cond = np.array([conductivity1[icond], conductivity2[icond],
                                     conductivity2[icond]])
                    hflx2B = mod_physic_column.heatflux_between_layers(thick,
                                                                       temp,
                                                                       cond)
                    hflx2T[1:] = -hflx2B[0:ilayer-1]

                    heatflux_bottom[ithk, icond, itemp, :] = hflx2B
                    heatflux_top[ithk, icond, itemp, :] = hflx2T

                    #
                    # Second: compute the temperature evolution driven by the
                    #         heatflux
                    #
                    scont = np.array([snow_content1, snow_content2])   #, snow_content2])
                    icont = np.array([ice_content1, ice_content2])     #, ice_content2])
                    wcont = np.array([water_content1, water_content2]) #, water_content2])

                    temp_time_next = \
                        mod_physic_column.thermal_diffusion(delta_time, thick, \
                                                            temp, hflx2B, \
                                                            hflx2T, scont,\
                                                            icont, wcont)
                    #
                    # Save the newly computed time step in the array holding
                    # the temporal evolution of temperature
                    #
                    temp1_time[ithk, icond, itemp, itime+1]  = temp_time_next[0]
                    temp2_time[ithk, icond, itemp, itime+1]  = temp_time_next[1]

    #
    # Plot 1
    #
    fig1 = plt.figure(dpi=300, facecolor='white', figsize=(4.5, 6.4))
    fig1.subplots_adjust(hspace=0.15)
    # Specification about subplots
    heights = [0.45, 0.55]
    specs2x1 = fig1.add_gridspec(ncols=1, nrows=len(heights), height_ratios=heights)
    axss = []
    for spec in specs2x1: # for ispec, spec in enumerate(specs2x1):
        ax_ = fig1.add_subplot(spec)
        axss.append(ax_)

    #ll
    # Subplot 1: Evolution of temperature in layers
    #
    icolor = 0
    for ithk in range(len(thickness1)):
        for icond in range(len(conductivity1)):
            for itemp in range(len(temperature1)):
                color = 'C'+str(icolor)
                label1 = None
                label2 = None
                if icolor == 0:
                    label1 = 'Upper: $T_{up}$'
                    label2 = 'Lower: $T_{down}$'
                linewidth = 2
                if itemp == 0:
                    linewidth = 2.5
                axss[0].plot(times/(86400.),
                             temp1_time[ithk, icond, itemp, :]-FREEZING_TEMP,
                             '-', color=color, linewidth=linewidth, label=label1)
                axss[0].plot(times/(86400.),
                             temp2_time[ithk, icond, itemp, :]-FREEZING_TEMP,
                             '--', color=color, linewidth=linewidth, label=label2)
                icolor += 1

    colors = ['black', 'dimgray']
    markers =['o', 's']
    for itemp in range(len(temperature1)):
        axss[0].plot(0, temperature1[itemp]-FREEZING_TEMP,
                     marker=markers[itemp], color=colors[itemp])
                     #, label='T(t=0) = {:.0f}K'.format(temperature1[itemp]))
        axss[0].text(2, temperature1[itemp]-FREEZING_TEMP,
                     '{:.0f}$^{{\circ}}\!C$'.format(temperature1[itemp]-FREEZING_TEMP),
                     va='center', ha='left')
        axss[0].plot(0, temperature2[itemp]-FREEZING_TEMP,
                     marker=markers[itemp], color=colors[itemp])
                     #, label='T(t=0) = {:.0f}K'.format(temperature2[itemp]))
        axss[0].text(2, temperature2[itemp]-FREEZING_TEMP,
                     '{:.0f}$^{{\circ}}\!C$'.format(temperature2[itemp]-FREEZING_TEMP),
                     va='center', ha='left')

    axss[0].set_ylabel('Layer temperature ($^{{\circ}}\!C$)')
    axss[0].legend()
    axss[0].set_title('Layer temperature evolution by thermal diffusion')
    axss[0].set_xlim([-2, 101])

    #
    # Subplot 1 : Evolution of the temperature difference
    #
    icolor = 0
    for ithk in range(len(thickness1)):
        for icond in range(len(conductivity1)):
            for itemp in range(len(temperature1)):
                color = 'C'+str(icolor)
                linestyle = '-.'
                linewidth = 2.2
                if icolor == 1 or icolor == 0:
                    linestyle = '-'
                    linewidth = 3.2
                label = '$h_{{up}}=${:.1f}'.format(thickness1[ithk])+ \
                    ', $\\kappa_{{up}}=${:.1f}'.format(conductivity1[icond])

                axss[1].plot(times/86400.,
                             temp1_time[ithk, icond, itemp, :]-
                             temp2_time[ithk, icond, itemp, :],
                             linestyle=linestyle,
                             linewidth=linewidth,
                             color=color,
                             label=label)
                icolor += 1

    axss[1].plot(times/86400., times*0.,':',color='gray')
    axss[1].set_xlabel('Time (day)')
    axss[1].set_ylabel('$\\Delta T = T_{up} - T_{down}$ ($^{{\circ}}\!C$)')
    axss[1].set_xlim([-2, 101])

    ithk0 = 0
    icond0 = 0
    axss[1].legend(loc='center', bbox_to_anchor=(0.85, 0.80),
                   title='Layers: $h_{{down}}=${:.1f} $m$'.format(thickness2[ithk0])+ \
                    ',\n$\\kappa_{{down}}=${:.1f} $W\: K^{{-1}}\, kg^{{-1}}$'.format(conductivity2[icond0]),
                    framealpha=0.92)

    axss[1].text(5, -4, 'Line colors indicate upper layer conditions')

    if plot_suffix:
        module4plots.plotting(fig1, plotname, plot_suffix)


# -------------------------------------------------------------------------
def plot_mfunc_inverted(library_name, plot_suffix=None,
                             plotname='mfunc_inverted'):
    '''
    Plot results of the via c-binding call Fortran function mfunc

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'mfunc'.

    Returns
    -------
    None.

    '''

    print('* inverted m-function')

    graind0 = np.linspace(0., 25, 101)*0.001  # Meter !
    graind1 = np.array([30, 40, 50, 75, 80, 85])*0.001
    graind = np.sort(np.concatenate((graind0, graind1)))

    nfunc_array = mod_physic_column.nfunc(graind, library_name)
    mfunc_array = mod_physic_column.mfunc(nfunc_array, library_name)
    inv_mfunc = 1.0/mfunc_array

    convert_m_to_mm = True # If False, adjust the text output below
    if convert_m_to_mm:
        graind = graind * 1000.

    #
    # Plot 1
    #
    fig1 = plt.figure(dpi=300, facecolor='white', figsize=(6.0, 5.2))
    ax0 = fig1.subplots()

    ax0.plot(nfunc_array, inv_mfunc, linewidth=2, color='darkgreen')
    ax0.set_xlabel(r'n-function, $n$ (1)', color='dimgray')
    ax0.set_ylabel(r'inverted m-function, $\frac{1}{m}$ (1)')

    ax0.tick_params(axis='x', colors='gray')
    ax0.set_yscale('log')

    ax0.set_title('Inverted m-function dependence on grain diameter size')

    #
    # Parallel to the n-function values on the x-axis, we write the
    # corresponding grain diameter values
    #
    secax0 = ax0.secondary_xaxis('top')
    if False:
        inc_2nd_xaxis = 6
        inc_2nd_xaxis = 20
        secax0.set_xticks(nfunc_array[::inc_2nd_xaxis])
        graind_str = ['{:.2f}'.format(grd) for grd in graind[::inc_2nd_xaxis]]
    else:
        indx_2nd_xaxis=[0, 1, 2, 3, 4, 6, 8, 12, 20, -1]
        secax0.set_xticks(nfunc_array[indx_2nd_xaxis])
        graind_str = ['{:.2f}'.format(grd) for grd in graind[indx_2nd_xaxis]]

    secax0.set_xticklabels(graind_str)
    secax0.tick_params(axis='x', colors='black')
    if convert_m_to_mm:
        secax0.set_xlabel(r'grain diameter, $d_{grain}$ (mm)', color='black')
    else:
        secax0.set_xlabel(r'grain diameter, $d_{grain}$ (m)', color='black')


    #
    # Highlight maximum grain diameter
    #
    graind_max_array = np.array([1.75, 12., 20., 30.0, 50.0, 80.0])*0.001 # Meter !
    graind_max_array = np.array([1.75, 10., 15., 20., 30.0, 50.0, 80.0])*0.001 # Meter !

    for graind_max in graind_max_array:
        if convert_m_to_mm:
            graind_max = graind_max * 1000.
        flag_graind_max = min(abs(graind-graind_max)) == abs(graind-graind_max)

        nfunc_graind_max = nfunc_array[flag_graind_max]
        mfunc_graind_max = mfunc_array[flag_graind_max]
        inv_mfunc_graind_max = inv_mfunc[flag_graind_max]

        ax0.plot(nfunc_graind_max, inv_mfunc_graind_max,
                 marker='x', markersize=5., color='r',
                 fillstyle='none', linewidth=3)
        ax0.plot(nfunc_graind_max, inv_mfunc_graind_max,
                 marker='.', markersize=5., color='r',
                 fillstyle='full', linewidth=3)

        str_graind_max  = '{:.2f}'.format(float(graind_max))
        str_inv_mfunc_graind_max ='{:.4g}'.format(float(inv_mfunc_graind_max))
        if convert_m_to_mm:
            ax0.text(nfunc_graind_max, inv_mfunc_graind_max,
                     r'  max $d_{{grain}}$= '+str_graind_max+' mm'+
                     '$\Rightarrow \\frac{{1}}{{m}}$='+str_inv_mfunc_graind_max,
                     ha='left', va='bottom')
        else:
            ax0.text(nfunc_graind_max, inv_mfunc_graind_max,
                     r'  max $d_{{grain}}$= '+str_graind_max+' m'+
                     '$\Rightarrow \\frac{{1}}{{m}}$='+str_inv_mfunc_graind_max,
                     ha='left', va='bottom')

    ax0.set_xlim(0.75, np.ceil(max(nfunc_array)))

    if plot_suffix:
        module4plots.plotting(fig1, plotname, plot_suffix)


# -------------------------------------------------------------------------
#
# Main program
#
if __name__ == '__main__':
    import values_mod_param as physical
    import cbind_mod_physic as mod_physic # Reference density profile
    import cbind_mod_physic_column as mod_physic_column
    import numpy as np
    import matplotlib.pyplot as plt
    from mpl_toolkits.axes_grid1.inset_locator import inset_axes
    import matplotlib as mpl
    import module4plots

    print('Load physical constant')
    physical.constant()
    # Values
    GRADLW = physical.constant.gradlw
    LAPSE_RATE = physical.constant.lapse_rate
    PATM = physical.constant.patm_surf
    RHO_ICE = physical.constant.rho_ice
    RHO_SNOW = physical.constant.rho_snow
    RHO_FWATER = physical.constant.rho_fwater
    RHO_SEAWATER = physical.constant.rho_seawater
    CDENSITY = physical.constant.Cdens
    FREEZING_TEMP = physical.constant.Tmelt_fw

    DEFAULT_NEW_GRAIN_DIAMETER = 0.0005 # Meter  !TODO: in a own module??

    #
    # Load the shared library
    #
    LIBRARY_NAME = '../src/CISSEMBEL_CBindings.so' # INCLUDING path, e.g., ./
    print('Shared library containing C-bindings of Fortran code "'
          +LIBRARY_NAME+'"')

    PLOT_SUFFIXES = module4plots.parse_arguments() # = None = ['png', 'pdf']

    #
    # Plots for each function
    #
    plot_density_frozen(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_water_saturation(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_irreducible_water_saturation(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_retention_potential(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_calc_runoff(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_supericeform_potential(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_grain_growth(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_hydraulic_suction(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_snow_permeability(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_nfunc(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_mfunc(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_dgrain_dt(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_darcy_flow(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_calc_refreeze(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_calc_waterflowdown(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_heatflux_between_layers(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_thermal_diffusion(LIBRARY_NAME, PLOT_SUFFIXES)
    #
    plot_mfunc_inverted(LIBRARY_NAME, PLOT_SUFFIXES)


# -- Last line

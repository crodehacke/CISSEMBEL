Change log of _CISSEMBEL_: Copenhagen Ice-Snow Surface Energy and Mass Balance modEL
====================================================================================

Details about _Copenhagen Ice-Snow Surface Energy and Mass Balance modEL_
(**CISSEMBEL**) are provided in [README.md](README.md).

This text reports ideas and (major) code changes as well as comments,
which can not be easy identified from the git history. It also serves
as a logging file reporting ongoing changes.

Open issues are reported in [TODO.md](TODO.md)

----------------


The logging information sorted by date
======================================

2025-Feb-04 : Christian Rodehacke
-----------------------------------

- New or improved features
  - New module [./src/mod\_solver.F08](./src/mod\_solver.F08) that
    comprises the solver of the surface mass balance (_seb_) and
    various auxiliary procedures. Additionally, they offer four
    iterative solver methods: _Newton-Raphson_, _Bisection_, _Secant_,
    and _Brent's_.
  - Surface energy balance (_seb_) as an output field.
  - Fewer variables and always defined variables for forcing output
    except you select `do_write_extended_forcing = .True.` in your
    controlling `namelist.ebm`.
  - Checks if one and only one configuration is selected in the
    Fortran driver [./src/EBMdriver.F08](./src/EBMdriver.F08).
  - The preprocessor variable `WRITE_FIELDS_IF_BAD` in
    [./src/cppdefs.h](./src/cppdefs.h) allows to activate the writing
    of an additional diagnostic file (_BAD_-file) that writes output
    for each record once values of fields are beyond a defined
    range. Currently, the code checks the layer temperature, the
    snowfall and rainfall rates, and the incoming solar radiation. For
    details see in [./src/modEBMworld.F08](./src/modEBMworld.F08) the
    called function `beyond_expected_range` and subroutine
    `report_beyond_setflag`. These can be called in `EBMworld` after
    all calculations and before potentially writing any record.
  - Improved setup for HIRHAM forced simulations including checks for
    invalid forcing field combinations.
  - More flexible use of the trigger to initiate writing.
- Fixes
  - Fix wrong calculation of `snowts` and its restriction to avoid a
    numerical overflow. The fix also solves a problem with the albedo
    scheme **5** (`ALBEDO_BROCK`).
  - Deactivation of _virtual layers_ because they are flawed.
  - Fixing a not initialized variable
  - Clean and with _ToDo_ marks (`[CHECK_and_ADJUST]`) enriched
    submission scripts.


2023-Dec-10 : Christian Rodehacke
-----------------------------------

Tagged version `v2303`: Publish corrections and imnproved documentation


2023-Dec-10 : Christian Rodehacke
-----------------------------------

Tagged version `v0.7.4`: _Height classes, correct turbulent fluxes in output_

- Correct output of turbulent heat and mass in output files.
- Pre-processor flag LATENT_HEAT_POSITIVE_DOWNWARD defines sign of
  latent heat flux
- Extended INSTALL.md
- Corrected enforced writing of not written files if the pre-processor
  `#define WRITE_LAST_TIME_STEP`, see [src/cppdefs.h](./src/cppdefs.h).
- Checked `vaporpress` against table values


2023-Nov-29 : Christian Rodehacke
-----------------------------------

Tagged version `v0.7.3.2`: _Clearer code_

- Indicate latent heat flux sign convention more clearly
- Updated Height class auxiliary script for downscaling simulations


2023-Nov-22 : Christian Rodehacke
-----------------------------------

Tagged version `v0.7.3.1`: _Latent heat flux CF-convention_

- Latent heat flux downward/upward separation clarified
- Height class auxilary script for downscaling simulations


2023-Nov-21 : Christian Rodehacke
-----------------------------------

Tagged version `v0.7.3`: _Sublimation flux_

- Improved batch scripts
- Fix issue #131: wrong sign of sublimation flux


2023-Nov-14 : Christian Rodehacke
-----------------------------------

Tagged version `v0.7.2.4`: _Publish code_

- Fix syntax issues with read time/date information


2023-Nov-01 : Christian Rodehacke
-----------------------------------

Tagged version `v2310`: _Publish code_

- Fix syntax issues in the documentation


2023-Oct-26 : Christian Rodehacke
-----------------------------------

Tagged version `v0.7.2.2`: _EraInterim simulation_

- Standard settings
- Fix misleading comments


2023-Oct-25 : Christian Rodehacke
-----------------------------------

Tagged version `v0.7.2.1`: _Known Issues_

- Extended [./KNOWN_ISSUES.md](/KNOWN_ISSUES.md)
- Cray Compiler information
- Safer memory access patterns


Tagged version `v0.7.2`: _absorbed LW and emissivity_

- `make proper` replaces `make propper`
- `nstep_* = 0` (in `namelist.ebm`) turns of writing except for the
   last forcing time step
- `emissivity` modifies longwave absorption
- expended support for further compilers
- Small fixes, todos


2023-Oct-09 : Christian Rodehacke
-----------------------------------

Tagged version `v0.7.1`: _EBMdriver (Python)_

- Rename generally Phyton driver `cissembel*.py` to `EBMdriver*.py`.


2023-Oct-08 : Christian Rodehacke
-----------------------------------

Tagged version `v0.7`: _EBMdriver_

- Rename generally `EBMoffline` to `EBMdriver`, so that the new
  Fortran driver is called
  [./src/EBMdriver.F08](./src/EBMdriver.F08) and the related
  namelist files are called `namelist.EBMdriver`.
- Fixes


2023-Sep-20 : Christian Rodehacke
-----------------------------------

Tagged version `v0.6.2`: _Clean up, restrict SMB calculations, OMP4future_


2023-Sep-20 : Christian Rodehacke
-----------------------------------

Tagged version `v0.6.1`: _RelHumid2DewTemp, Report missed `depth_subsurface_default`_

- Function `relhumid2dewpoint` computes dew-point temperature from
  relative humidity
- Report how often and where the requested temperature at the depth
 `depth_subsurface_default` has been taken from a shallower depth
  layer; write this diagnostic as field `shallow_counter` into the
  `DIAgnost`-file


2023-Sep-18 : Christian Rodehacke
-----------------------------------

Tagged version `v0.6`: _Towards publishing: Improvements, Clean up, TODOs_

- More modular Python code structure
- Write record for files that have not written any record on the
  last time step if `#define WRITE_LAST_TIME_STEP` in
  [./src/cppdefs.h](./src/cppdefs.h)
- Auxillary scripts to fetch and build dependent libraries
- Improve documentation
- Clean up code and solve various TODOs


2023-Sep-08 : Christian Rodehacke
-----------------------------------

Tagged version `v0.5`: _Better code, doc, namelists; New: cp\_ice, tpre2snow\_tanh, pretend\_use, trigger\_write; MPI subdomains; TODOs_

- Trigger daily and month writing by `build_trigger` as part of
  [./src/mod_aux4offdriver.F08](./src/mod_aux4offdriver.F08). Activated
  by setting `#define SET_FORCED_WRITE_MONTHLY`
  in [./src/EBMoffline.F08](./src/EBMoffline.F08)
- Distinguishing between snowfall and rainfall via a `tanh` function;
  selection of scheme via `iselect_precip2snow`
- Specific heat capacity of ice as a function of temperature in
  `calc_cp_ice` if `#define CP_ICE_TEMPERATURE_DEPEND` in
  [./src/cppdefs.h](./src/cppdefs.h)
- More favorable subdivision into MPI subdomains
- Improved internal code documentation
- Discard unused variables and files, which reduces warnings about
  unused variables
- Determination of computational efforts among subroutines and
  functions


2023-Jun-27 : Christian Rodehacke
-----------------------------------

Tagged version `v0.4`: _Diffusion via Crank-Nicolson and spare Jacobian_

- Heat diffusion by the implicit unconditional stable Crank-Nicolson
  approach
- Height class/height level support has been implemented for the
  serial code and OpenMP parallelized code


2023-Jun-27 : Christian Rodehacke
-----------------------------------

Tagged version `v0.3`: _Update code documentation, HIRHAM-data drivers_

- Improved time-series support
- Subdomain support by Fortran driver
- Date/time is in `wp_time => REAL64`
- Towards NUMA-aware variables if `#define OPENMP_ALLOCATE` in
  [./src/cppdefs.h](./src/cppdefs.h]).


2023-Jan-10 : Christian Rodehacke
-----------------------------------

Tagged version `v0.1`: _Initial productive version_

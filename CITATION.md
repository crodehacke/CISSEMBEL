Automatic conversion from BibTex to Endnote via: `bib2xml references.bib | xml2end > references.enw` and RIS `bib2xml references.bib | xml2ris > references.ris`


BibTeX
======

@article{Kapsch2021,
abstract = {A realistic simulation of the surface mass balance (SMB) is essential for simulating past and future ice-sheet changes. As most state-of-the-art Earth system models (ESMs) are not capable of realistically representing processes determining the SMB, most studies of the SMB are limited to observations and regional climate models and cover the last century and near future only. Using transient simulations with the Max Planck Institute ESM in combination with an energy balance model (EBM), we extend previous research and study changes in the SMB and equilibrium line altitude (ELA) for the Northern Hemisphere ice sheets throughout the last deglaciation. The EBM is used to calculate and downscale the SMB onto a higher spatial resolution than the native ESM grid and allows for the resolution of SMB variations due to topographic gradients not resolved by the ESM. An evaluation for historical climate conditions (1980–2010) shows that derived SMBs compare well with SMBs from regional modeling. Throughout the deglaciation, changes in insolation dominate the Greenland SMB. The increase in insolation and associated warming early in the deglaciation result in an ELA and SMB increase. The SMB increase is caused by compensating effects of melt and accumulation: the warming of the atmosphere leads to an increase in melt at low elevations along the ice-sheet margins, while it results in an increase in accumulation at higher levels as a warmer atmosphere precipitates more. After 13 ka, the increase in melt begins to dominate, and the SMB decreases. The decline in Northern Hemisphere summer insolation after 9 ka leads to an increasing SMB and decreasing ELA. Superimposed on these long-term changes are centennial-scale episodes of abrupt SMB and ELA decreases related to slowdowns of the Atlantic meridional overturning circulation (AMOC) that lead to a cooling over most of the Northern Hemisphere.},
author = {Kapsch, Marie-Luise and Mikolajewicz, Uwe and Ziemen, Florian Andreas and Rodehacke, Christian B. and Schannwell, Clemens},
doi = {10.5194/tc-15-1131-2021},
issn = {1994-0424},
journal = {The Cryosphere},
keywords = {Atmosphere,Climate model,Climatology,Deglaciation,Energy balance,Environmental science,Glacier mass balance,Greenland ice sheet,Ice sheet,Northern Hemisphere},
mendeley-groups = {2Read},
month = {mar},
number = {2},
pages = {1131--1156},
title = {{Analysis of the surface mass balance for deglacial climate simulations}},
url = {https://tc.copernicus.org/articles/15/1131/2021/},
volume = {15},
year = {2021}
}


@article{Langen2015,
abstract = {Freshwater runoff to fjords with marine-terminating glaciers along the Greenland Ice Sheet margin has an impact on fjord circulation and, potentially, ice sheet mass balance through increasing heat transport to the glacier front. Here we use the high resolution (5.5 km) HIRHAM5 regional climate model, allowing high detail in topography and surface types, to estimate freshwater input to Godth{\aa}bsfjord in Southwest Greenland. Model output is compared to hydro-meteorological observations, and while simulated daily variability in temperature and downwelling radiation shows high correlation with observations (typically {\textgreater}0.9), there are biases that impact our results. In particular, overestimated albedo leads to underestimation of melt and runoff at low-elevations. In the model simulation (1991-2012), the ice sheet experiences increasing energy input from the surface turbulent heat flux (up to elevations of 2000 m) and shortwave radiation (at all elevations). Southerly wind anomalies and declining cloudiness due to an increase in atmospheric pressure over North Greenland contribute to increased summer melt. This results in declining surface mass balance (SMB), increasing surface runoff and upward shift of the equilibrium line altitude. SMB is reconstructed back to 1890 though regression between simulated SMB and observed temperature and precipitation, with added uncertainty in the period 1890-1952 due to possible inhomogeneity in the precipitation record. SMB as low as in recent years appears to have occurred before, most notably around 1930, 1950 and 1960. While previous low SMBs were mainly caused by low accumulation, those around 1930 and in the 2000s are mainly due to warming},
author = {Langen, P. L. and Mottram, R. H. and Christensen, J. H. and Boberg, F. and Rodehacke, C. B. and Stendel, M. and van As, D. and Ahlstr{\o}m, A. P. and Mortensen, J. and Rysgaard, S. and Petersen, D. and Svendsen, K. H. and Aðalgeirsd{\'{o}}ttir, G. and Cappelen, J.},
doi = {10.1175/JCLI-D-14-00271.1},
issn = {0894-8755},
journal = {Journal of Climate},
keywords = {Glaciers,Godthabsfjord,Greenland ice sheet,HIRHAM,Heat budgets/fluxes,Ice sheets,Nuuk,Regional models,SMB,Surface fluxes,Water budget,surface mass balance},
number = {9},
pages = {3694--3713},
title = {{Quantifying energy and mass fluxes controlling Godth{\aa}bsfjord freshwater input in a 5 km simulation (1991-2012)}},
url = {http://journals.ametsoc.org/doi/abs/10.1175/JCLI-D-14-00271.1},
volume = {28},
year = {2015}
}


@Article{,
AUTHOR = {},
TITLE = {},
JOURNAL = {},
VOLUME = {},
YEAR = {},
PAGES = {},
URL = {https://},
DOI = {}
}


Endnote
=======

%0 Journal Article
%T Analysis of the surface mass balance for deglacial climate simulations
%A Kapsch, Marie-Luise
%A Mikolajewicz, Uwe
%A Ziemen, Florian Andreas
%A Rodehacke, Christian B.
%A Schannwell, Clemens
%J The Cryosphere
%D 2021
%8 mar
%V 15
%N 2
%@ 1994-0424
%F Kapsch2021
%X A realistic simulation of the surface mass balance (SMB) is essential for simulating past and future ice-sheet changes. As most state-of-the-art Earth system models (ESMs) are not capable of realistically representing processes determining the SMB, most studies of the SMB are limited to observations and regional climate models and cover the last century and near future only. Using transient simulations with the Max Planck Institute ESM in combination with an energy balance model (EBM), we extend previous research and study changes in the SMB and equilibrium line altitude (ELA) for the Northern Hemisphere ice sheets throughout the last deglaciation. The EBM is used to calculate and downscale the SMB onto a higher spatial resolution than the native ESM grid and allows for the resolution of SMB variations due to topographic gradients not resolved by the ESM. An evaluation for historical climate conditions (1980–2010) shows that derived SMBs compare well with SMBs from regional modeling. Throughout the deglaciation, changes in insolation dominate the Greenland SMB. The increase in insolation and associated warming early in the deglaciation result in an ELA and SMB increase. The SMB increase is caused by compensating effects of melt and accumulation: the warming of the atmosphere leads to an increase in melt at low elevations along the ice-sheet margins, while it results in an increase in accumulation at higher levels as a warmer atmosphere precipitates more. After 13 ka, the increase in melt begins to dominate, and the SMB decreases. The decline in Northern Hemisphere summer insolation after 9 ka leads to an increasing SMB and decreasing ELA. Superimposed on these long-term changes are centennial-scale episodes of abrupt SMB and ELA decreases related to slowdowns of the Atlantic meridional overturning circulation (AMOC) that lead to a cooling over most of the Northern Hemisphere.
%K Atmosphere,Climate model,Climatology,Deglaciation,Energy balance,Environmental science,Glacier mass balance,Greenland ice sheet,Ice sheet,Northern Hemisphere
%U https://tc.copernicus.org/articles/15/1131/2021/
%U http://dx.doi.org/10.5194/tc-15-1131-2021
%P 1131-1156

%0 Journal Article
%T Quantifying energy and mass fluxes controlling Godth�bsfjord freshwater input in a 5 km simulation (1991-2012)
%A Langen, P. L.
%A Mottram, R. H.
%A Christensen, J. H.
%A Boberg, F.
%A Rodehacke, C. B.
%A Stendel, M.
%A van As, D.
%A Ahlstr�m, A. P.
%A Mortensen, J.
%A Rysgaard, S.
%A Petersen, D.
%A Svendsen, K. H.
%A A?�algeirsd�ttir, G.
%A Cappelen, J.
%J Journal of Climate
%D 2015
%V 28
%N 9
%@ 0894-8755
%F Langen2015
%X Freshwater runoff to fjords with marine-terminating glaciers along the Greenland Ice Sheet margin has an impact on fjord circulation and, potentially, ice sheet mass balance through increasing heat transport to the glacier front. Here we use the high resolution (5.5 km) HIRHAM5 regional climate model, allowing high detail in topography and surface types, to estimate freshwater input to Godth�bsfjord in Southwest Greenland. Model output is compared to hydro-meteorological observations, and while simulated daily variability in temperature and downwelling radiation shows high correlation with observations (typically \textgreater0.9), there are biases that impact our results. In particular, overestimated albedo leads to underestimation of melt and runoff at low-elevations. In the model simulation (1991-2012), the ice sheet experiences increasing energy input from the surface turbulent heat flux (up to elevations of 2000 m) and shortwave radiation (at all elevations). Southerly wind anomalies and declining cloudiness due to an increase in atmospheric pressure over North Greenland contribute to increased summer melt. This results in declining surface mass balance (SMB), increasing surface runoff and upward shift of the equilibrium line altitude. SMB is reconstructed back to 1890 though regression between simulated SMB and observed temperature and precipitation, with added uncertainty in the period 1890-1952 due to possible inhomogeneity in the precipitation record. SMB as low as in recent years appears to have occurred before, most notably around 1930, 1950 and 1960. While previous low SMBs were mainly caused by low accumulation, those around 1930 and in the 2000s are mainly due to warming
%K Glaciers,Godthabsfjord,Greenland ice sheet,HIRHAM,Heat budgets/fluxes,Ice sheets,Nuuk,Regional models,SMB,Surface fluxes,Water budget,surface mass balance
%U http://journals.ametsoc.org/doi/abs/10.1175/JCLI-D-14-00271.1
%U http://dx.doi.org/10.1175/JCLI-D-14-00271.1
%P 3694-3713


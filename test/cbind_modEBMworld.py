#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Git version control id: $Id$

Created on Thu Nov 25 09:40:13 CET 2021

C-binding to run the Copenhagen Ice Snow Surface Energy and Mass Balance modEL
(CISSEMBEL) from a Python frontend. It is the calling the main Fortran
subroutines EBMinitialize, EBMworld, and EBMfinalize in the Fortran module
modEBMworld. The bridge between the Python and Fortran code is implemented
via C-bindings as part of the Fortran2008 standard.


Since the information is not preserved beetween the indiviuall calls of
indiviual c-wrapper calls (c_EBMinitialize, c_EBMworld, and C_EBMfinalize),
we have to have one entry point to CISSEMBEL. Therefore Python calls one
program handling the initialization initially (EBMinitialize), runs the actual
simulation (EBMworld), and finalize the simulations if requested (EBMfinalize).

Please note, that EBMworld offers many different (optional) combinations of
input forcing fields that are not handled by the C-wrappers.

@author: Christian Rodehacke, DMI Copenhagen, Denmark

@copyright: Copyright (C) 2016-2025 Christian Rodehacke
"""
# -------------------------------------------------------------------
# This file is part of CISSEMBEL, which is the
# == Copenhagen Ice-Snow Surface Energy and Mass Balance modEL ==
#
# CISSEMBEL is free software; you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE  v.1.2
#
# Information about the EUROPEAN UNION PUBLIC LICENCE (*EUPL*) and
# translations into other European languages are available at
# https://eupl.eu (last access: 2023-09-18)](https://eupl.eu/)
#
# We hope that  this  distributed  CISSEMBEL  code, its  tools and
# documentations are usefull. Any improvements of the code, tools,
# and documentations tools are  very welcome.  Please, consider to
# share your thoughts and improvements with the community.
#
# The documentation,  tools,  and the code are provided as it are.
# NO WARRANTY  ABOUT FITNESS  AND USABILITY OF THE CODE, TOOLS, OR
# DOCUMENTATION  ARE  GIVEN.   NO  WARRANTY  IS  GIVEN  ABOUT  THE
# CORRECTNESS  OF COMPUTED RESULTS WITH THE  PROVIDED CODE, TOOLS,
# AND DOCUMENTATION. YOU USE THE CODE, DOCUMENTATION, AND TOOLS AT
# YOUR OWN RISK.           Removing  of  any  copyright  statement
# or  this  disclaimer  is considered  as a  attempt  to defraud.
#
# -------------------------------------------------------------------

def CISSEMBEL(ilength, jlength, tlength, time, dtime, time_list, mask,
              zsurface, air_temperature, surface_short_radiation_down,
              surface_long_radiation_down, total_precipitation,
              dew_temperature, atmos_surface_pressure, wind_speed,
              cloudfraction, library='../src/CISSEMBEL_CBindings.so'):
    """
    C-binding to CISSEMBEL. It's a wrapper to the three main  EBMinitialize,
    EBMworld, and EBMfinalize in the Fortran module modEBMworld.


    Parameters
    ----------
    ilength : int
        Size of first dimension.
    jlength : int
        Size of second dimension.
    tlength : int
        Size of third dimension/time axis.
    time : numpy.ndarray in double precision
        Time axis.
    dtime : float
        Time step width.
    time_list : numpy.ndarray in integer
        Time axis of five elements for each time representing year, month, day, hour, minute.
    mask : numpy.ndarray in integer
        Mask defining each grid points characteristic.
    zsurface : numpy.ndarray in double precision
        Surface elevation/orography used by the atmospheric data source - reference topography.
    air_temperature : numpy.ndarray in double precision
        near surface air temperature. It is usually the 2m-air temperature.
    surface_short_radiation_down : numpy.ndarray in double precision
        Shortwave/solar dowward radiation at the surface/ground.
    surface_long_radiation_down : numpy.ndarray in double precision
        Longwave/thermal dowward radiation at the surface/ground.
    total_precipitation : numpy.ndarray in double precision
        Total precipitation, which is the sum of rain-, snow- and icefall.
    dew_temperature : numpy.ndarray in double precision
        Near surface dew point temperature, usually at 2m height.
    atmos_surface_pressure : numpy.ndarray in double precision
        Atmospheric air pressure at the surface; It is NOT the sea-level pressure.
    wind_speed : numpy.ndarray in double precision
        Absolute wind speed, usually at 10m height.
    cloudfraction : numpy.ndarray in double precision
        cloud cover fraction with values between 0.0 and 1.0.
    library : str or ctypes.CDLL, optional
        Reference to imported shared library containing the C-bindings.
        The default is '../src/CISSEMBEL_CBindings.so'.

    Returns
    -------
    None.

    """

    if isinstance(library, ctypes.CDLL):
        fortlibrary = library
    else:
        fortlibrary = ctypes.cdll.LoadLibrary(library)


    #
    # Checking input data size with requested dimension/axis lengths
    #
    all_length2D = (ilength, jlength)
    all_length3D = (ilength, jlength, tlength)

    if len(time) != tlength:
        print('cbind_modEBMworld:CISSEMBEL: Length of time array '+
              str(len(time))+' and time-dimension (tlength'+
              str(tlength)+') disagree')
        return
    # dtime == float or int
    # time_list

    flag_stop = False
    for icount, field in enumerate([zsurface]):
        if field.shape != all_length2D:
            print('cbind_modEBMworld:CISSEMBEL: ERROR: Shape of 2D-field [#'+
                  str(icount)+']: '+str(field.shape)+
                  ' and horizontal-dimensions '+str(all_length2D)+
                  ' disagree')
            flag_stop = True
        else:
            print('cbind_modEBMworld:CISSEMBEL:    OK: 2D-field [#'+
                  str(icount)+']: ',str(field.shape)+
                  ' == horizontal-dimensions '+str(all_length2D))

    for icount, field in enumerate([mask,
                                    air_temperature,
                                    surface_short_radiation_down,
                                    surface_long_radiation_down,
                                    total_precipitation,
                                    dew_temperature,
                                    atmos_surface_pressure,
                                    wind_speed,
                                    cloudfraction]):
        if field.shape != all_length3D:
            print('cbind_modEBMworld:CISSEMBEL: ERROR: Shape of 3D-field [#'+
                  str(icount)+']: '+str(field.shape)+
                  ' and dimensions '+str(all_length3D)+' disagree')
            flag_stop = True
        else:
            print('cbind_modEBMworld:CISSEMBEL:    OK: 3D-field [#'+
                  str(icount)+']: ',str(field.shape)+
                  ' == dimensions '+str(all_length3D))

    if flag_stop:
        print('cbind_modEBMworld:CISSEMBEL: ##########\n#  STOP  #\n##########')
        return

    #
    # C-Numver, Number conversion, Fortran-order
    #
    c_ilength = ctypes.c_int(ilength)
    c_jlength = ctypes.c_int(jlength)
    c_tlength = ctypes.c_int(tlength)
    c_dtime = ctypes.c_double(dtime)

    c_time = numpy.array(time, order='F')
    c_time_list = numpy.array(time_list, order='F')
    c_mask = numpy.array(mask, order='F')
    c_zsurface = numpy.array(zsurface, order='F')
    c_air_temperature = numpy.array(air_temperature, order='F')
    c_surface_short_radiation_down = numpy.array(surface_short_radiation_down,
                                                  order='F')
    c_surface_long_radiation_down = numpy.array(surface_long_radiation_down,
                                                order='F')
    c_total_precipitation = numpy.array(total_precipitation, order='F')
    c_dew_temperature = numpy.array(dew_temperature, order='F')
    c_atmos_surface_pressure = numpy.array(atmos_surface_pressure, order='F')
    c_wind_speed = numpy.array(wind_speed, order='F')
    c_cloudfraction = numpy.array(cloudfraction, order='F')

    #
    # c-pointer for fields
    #
    c_time_ptr = c_time.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    c_time_list_ptr = c_time_list.ctypes.data_as(ctypes.POINTER(ctypes.c_int))
    c_mask_ptr = c_mask.ctypes.data_as(ctypes.POINTER(ctypes.c_int))
    c_zsurface_ptr = \
        c_zsurface.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    c_air_temperature_ptr = \
        c_air_temperature.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    c_surface_short_radiation_down_ptr = \
        c_surface_short_radiation_down.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    c_surface_long_radiation_down_ptr = \
        c_surface_long_radiation_down.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    c_total_precipitation_ptr = \
        c_total_precipitation.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    c_dew_temperature_ptr = \
        c_dew_temperature.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    c_atmos_surface_pressure_ptr = \
        c_atmos_surface_pressure.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    c_wind_speed_ptr = c_wind_speed.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    c_cloudfraction_ptr = c_cloudfraction.ctypes.data_as(ctypes.POINTER(ctypes.c_double))

    #
    # Argument types's specification of the called Fortran c-wrapper subroutine
    #
    fortlibrary.c_CISSEMBEL.argtypes = [ctypes.c_int,
                                        ctypes.c_int,
                                        ctypes.c_int,
                                        ctypes.POINTER(ctypes.c_double),
                                        ctypes.c_double,
                                        ctypes.POINTER(ctypes.c_int),
                                        ctypes.POINTER(ctypes.c_int),
                                        ctypes.POINTER(ctypes.c_double),
                                        ctypes.POINTER(ctypes.c_double),
                                        ctypes.POINTER(ctypes.c_double),
                                        ctypes.POINTER(ctypes.c_double),
                                        ctypes.POINTER(ctypes.c_double),
                                        ctypes.POINTER(ctypes.c_double),
                                        ctypes.POINTER(ctypes.c_double),
                                        ctypes.POINTER(ctypes.c_double),
                                        ctypes.POINTER(ctypes.c_double)]
    #
    # Call of the wrapper
    #
    fortlibrary.c_CISSEMBEL(c_ilength,
                            c_jlength,
                            c_tlength,
                            c_time_ptr,
                            c_dtime,
                            c_time_list_ptr,
                            c_mask_ptr,
                            c_zsurface_ptr,
                            c_air_temperature_ptr,
                            c_surface_short_radiation_down_ptr,
                            c_surface_long_radiation_down_ptr,
                            c_total_precipitation_ptr,
                            c_dew_temperature_ptr,
                            c_atmos_surface_pressure_ptr,
                            c_wind_speed_ptr,
                            c_cloudfraction_ptr)
    return


def CISSEMBEL_HIRHAMrs(ilength, jlength, tlength, time, dtime,
                       time_list, mask, zsurface, air_temperature,
                       surface_short_radiation_down,
                       surface_long_radiation_down, rainfall,
                       snowfall, evaporation, latent_hflux,
                       sensible_hflux,
                       library='../src/CISSEMBEL_CBindings.so'):
    """
    C-binding to CISSEMBEL for the HIRHAM setup, where we provide turbulent
    heat fluxes (latent and sensible), and rain- and snowfall rates. This setup
    is not able to use height correction.
    It's a wrapper to the three main EBMinitialize, EBMworld, and EBMfinalize
    in the Fortran module modEBMworld.

    VARIANT: Here we provide both rainfall and snowfall

    Parameters
    ----------
    ilength : int
        Size of first dimension.
    jlength : int
        Size of second dimension.
    tlength : int
        Size of third dimension/time axis.
    time : numpy.ndarray in double precision
        Time axis.
    dtime : float
        Time step width.
    time_list : numpy.ndarray in integer
        Time axis of five elements for each time representing year, month, day, hour, minute.
    mask : numpy.ndarray in integer
        Mask defining each grid points characteristic.
    zsurface : numpy.ndarray in double precision
        Surface elevation/orography used by the atmospheric data source - reference topography.
    air_temperature : numpy.ndarray in double precision
        near surface air temperature. It is usually the 2m-air temperature.
    surface_short_radiation_down : numpy.ndarray in double precision
        Shortwave/solar dowward radiation at the surface/ground.
    surface_long_radiation_down : numpy.ndarray in double precision
        Longwave/thermal dowward radiation at the surface/ground.
    rainfall : numpy.ndarray in double precision
        Rainfall.
    snowfall : numpy.ndarray in double precision
        Snow- and icefall.
    evaporation : numpy.ndarray in double precision
        Evaporation/Sublimation.
    latent_hflux : numpy.ndarray in double precision
        Latent heat flux.
    sensible_hflux : numpy.ndarray in double precision
        Sensible heat flux.
    library : str or ctypes.CDLL, optional
        Reference to imported shared library containing the C-bindings.
        The default is '../src/CISSEMBEL_CBindings.so'.

    Returns
    -------
    None.

    """

    if isinstance(library, ctypes.CDLL):
        fortlibrary = library
    else:
        fortlibrary = ctypes.cdll.LoadLibrary(library)


    #
    # Checking input data size with requested dimension/axis lengths
    #
    all_length2D = (ilength, jlength)
    all_length3D = (ilength, jlength, tlength)

    if len(time) != tlength:
        print('cbind_modEBMworld:CISSEMBEL: Length of time array '+
              str(len(time))+' and time-dimension (tlength'+
              str(tlength)+') disagree')
        return
    # dtime == float or int
    # time_list

    flag_stop = False
    for icount, field in enumerate([zsurface]):
        if field.shape != all_length2D:
            print('cbind_modEBMworld:CISSEMBEL: ERROR: Shape of 2D-field [#'+
                  str(icount)+']: '+str(field.shape)+
                  ' and horizontal-dimensions '+str(all_length2D)+
                  ' disagree')
            flag_stop = True
        else:
            print('cbind_modEBMworld:CISSEMBEL:    OK: 2D-field [#'+
                  str(icount)+']: ',str(field.shape)+
                  ' == horizontal-dimensions '+str(all_length2D))

    for icount, field in enumerate([mask,
                                    air_temperature,
                                    surface_short_radiation_down,
                                    surface_long_radiation_down,
                                    rainfall,
                                    snowfall,
                                    evaporation,
                                    latent_hflux,
                                    sensible_hflux]):
        if field.shape != all_length3D:
            print('cbind_modEBMworld:CISSEMBEL: ERROR: Shape of 3D-field [#'+
                  str(icount)+']: '+str(field.shape)+
                  ' and dimensions '+str(all_length3D)+' disagree')
            flag_stop = True
        else:
            print('cbind_modEBMworld:CISSEMBEL:    OK: 3D-field [#'+
                  str(icount)+']: ',str(field.shape)+
                  ' == dimensions '+str(all_length3D))

    if flag_stop:
        print('cbind_modEBMworld:CISSEMBEL: ##########\n#  STOP  #\n##########')
        return

    #
    # C-Numver, Number conversion, Fortran-order
    #
    c_ilength = ctypes.c_int(ilength)
    c_jlength = ctypes.c_int(jlength)
    c_tlength = ctypes.c_int(tlength)
    c_dtime = ctypes.c_double(dtime)

    c_time = numpy.array(time, order='F')
    c_time_list = numpy.array(time_list, order='F')
    c_mask = numpy.array(mask, order='F')
    c_zsurface = numpy.array(zsurface, order='F')
    c_air_temperature = numpy.array(air_temperature, order='F')
    c_surface_short_radiation_down = numpy.array(surface_short_radiation_down,
                                                 order='F')
    c_surface_long_radiation_down = numpy.array(surface_long_radiation_down,
                                                order='F')
    c_rainfall = numpy.array(rainfall, order='F')
    c_snowfall = numpy.array(snowfall, order='F')
    c_evaporation = numpy.array(evaporation, order='F')
    c_latent_hflux = numpy.array(latent_hflux, order='F')
    c_sensible_hflux = numpy.array(sensible_hflux, order='F')

    #
    # c-pointer for fields
    #
    c_time_ptr = c_time.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    c_time_list_ptr = c_time_list.ctypes.data_as(ctypes.POINTER(ctypes.c_int))
    c_mask_ptr = c_mask.ctypes.data_as(ctypes.POINTER(ctypes.c_int))
    c_zsurface_ptr = \
        c_zsurface.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    c_air_temperature_ptr = \
        c_air_temperature.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    c_surface_short_radiation_down_ptr = \
        c_surface_short_radiation_down.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    c_surface_long_radiation_down_ptr = \
        c_surface_long_radiation_down.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    c_rainfall_ptr = \
        c_rainfall.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    c_snowfall_ptr = \
        c_snowfall.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    c_evaporation_ptr = \
        c_evaporation.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    c_latent_hflux_ptr = c_latent_hflux.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    c_sensible_hflux_ptr = c_sensible_hflux.ctypes.data_as(ctypes.POINTER(ctypes.c_double))

    #
    # Argument types's specification of the called Fortran c-wrapper subroutine
    #
    fortlibrary.c_CISSEMBEL_HIRHAMrs.argtypes = [ctypes.c_int,
                                                 ctypes.c_int,
                                                 ctypes.c_int,
                                                 ctypes.POINTER(ctypes.c_double),
                                                 ctypes.c_double,
                                                 ctypes.POINTER(ctypes.c_int),
                                                 ctypes.POINTER(ctypes.c_int),
                                                 ctypes.POINTER(ctypes.c_double),
                                                 ctypes.POINTER(ctypes.c_double),
                                                 ctypes.POINTER(ctypes.c_double),
                                                 ctypes.POINTER(ctypes.c_double),
                                                 ctypes.POINTER(ctypes.c_double),
                                                 ctypes.POINTER(ctypes.c_double),
                                                 ctypes.POINTER(ctypes.c_double),
                                                 ctypes.POINTER(ctypes.c_double),
                                                 ctypes.POINTER(ctypes.c_double)]
    #
    # Call of the wrapper
    #
    fortlibrary.c_CISSEMBEL_HIRHAMrs(c_ilength,
                                     c_jlength,
                                     c_tlength,
                                     c_time_ptr,
                                     c_dtime,
                                     c_time_list_ptr,
                                     c_mask_ptr,
                                     c_zsurface_ptr,
                                     c_air_temperature_ptr,
                                     c_surface_short_radiation_down_ptr,
                                     c_surface_long_radiation_down_ptr,
                                     c_rainfall_ptr,
                                     c_snowfall_ptr,
                                     c_evaporation_ptr,
                                     c_latent_hflux_ptr,
                                     c_sensible_hflux_ptr)
    return


def CISSEMBEL_HIRHAMp(ilength, jlength, tlength, time, dtime,
                       time_list, mask, zsurface, air_temperature,
                       surface_short_radiation_down,
                       surface_long_radiation_down,
                       precipitation, evaporation, latent_hflux,
                       sensible_hflux,
                       library='../src/CISSEMBEL_CBindings.so'):
    """
    C-binding to CISSEMBEL for the HIRHAM setup, where we provide turbulent
    heat fluxes (latent and sensible), and total precipitation
    rates. This setup is not able to use height correction.
    It's a wrapper to the three main EBMinitialize, EBMworld, and EBMfinalize
    in the Fortran module modEBMworld.

    VARIANT: Here we provide only total precipitation


    Parameters
    ----------
    ilength : int
        Size of first dimension.
    jlength : int
        Size of second dimension.
    tlength : int
        Size of third dimension/time axis.
    time : numpy.ndarray in double precision
        Time axis.
    dtime : float
        Time step width.
    time_list : numpy.ndarray in integer
        Time axis of five elements for each time representing year, month, day, hour, minute.
    mask : numpy.ndarray in integer
        Mask defining each grid points characteristic.
    zsurface : numpy.ndarray in double precision
        Surface elevation/orography used by the atmospheric data source - reference topography.
    air_temperature : numpy.ndarray in double precision
        near surface air temperature. It is usually the 2m-air temperature.
    surface_short_radiation_down : numpy.ndarray in double precision
        Shortwave/solar dowward radiation at the surface/ground.
    surface_long_radiation_down : numpy.ndarray in double precision
        Longwave/thermal dowward radiation at the surface/ground.
    precipitation : numpy.ndarray in double precision
        Total precipitation.
    evaporation : numpy.ndarray in double precision
        Evaporation/Sublimation.
    latent_hflux : numpy.ndarray in double precision
        Latent heat flux.
    sensible_hflux : numpy.ndarray in double precision
        Sensible heat flux.
    library : str or ctypes.CDLL, optional
        Reference to imported shared library containing the C-bindings.
        The default is '../src/CISSEMBEL_CBindings.so'.

    Returns
    -------
    None.

    """

    if isinstance(library, ctypes.CDLL):
        fortlibrary = library
    else:
        fortlibrary = ctypes.cdll.LoadLibrary(library)


    #
    # Checking input data size with requested dimension/axis lengths
    #
    all_length2D = (ilength, jlength)
    all_length3D = (ilength, jlength, tlength)

    if len(time) != tlength:
        print('cbind_modEBMworld:CISSEMBEL: Length of time array '+
              str(len(time))+' and time-dimension (tlength'+
              str(tlength)+') disagree')
        return
    # dtime == float or int
    # time_list

    flag_stop = False
    for icount, field in enumerate([zsurface]):
        if field.shape != all_length2D:
            print('cbind_modEBMworld:CISSEMBEL: ERROR: Shape of 2D-field [#'+
                  str(icount)+']: '+str(field.shape)+
                  ' and horizontal-dimensions '+str(all_length2D)+
                  ' disagree')
            flag_stop = True
        else:
            print('cbind_modEBMworld:CISSEMBEL:    OK: 2D-field [#'+
                  str(icount)+']: ',str(field.shape)+
                  ' == horizontal-dimensions '+str(all_length2D))

    for icount, field in enumerate([mask,
                                    air_temperature,
                                    surface_short_radiation_down,
                                    surface_long_radiation_down,
                                    precipitation,
                                    evaporation,
                                    latent_hflux,
                                    sensible_hflux]):
        if field.shape != all_length3D:
            print('cbind_modEBMworld:CISSEMBEL: ERROR: Shape of 3D-field [#'+
                  str(icount)+']: '+str(field.shape)+
                  ' and dimensions '+str(all_length3D)+' disagree')
            flag_stop = True
        else:
            print('cbind_modEBMworld:CISSEMBEL:    OK: 3D-field [#'+
                  str(icount)+']: ',str(field.shape)+
                  ' == dimensions '+str(all_length3D))

    if flag_stop:
        print('cbind_modEBMworld:CISSEMBEL: ##########\n#  STOP  #\n##########')
        return

    #
    # C-Numver, Number conversion, Fortran-order
    #
    c_ilength = ctypes.c_int(ilength)
    c_jlength = ctypes.c_int(jlength)
    c_tlength = ctypes.c_int(tlength)
    c_dtime = ctypes.c_double(dtime)

    c_time = numpy.array(time, order='F')
    c_time_list = numpy.array(time_list, order='F')
    c_mask = numpy.array(mask, order='F')
    c_zsurface = numpy.array(zsurface, order='F')
    c_air_temperature = numpy.array(air_temperature, order='F')
    c_surface_short_radiation_down = numpy.array(surface_short_radiation_down,
                                                 order='F')
    c_surface_long_radiation_down = numpy.array(surface_long_radiation_down,
                                                order='F')
    c_precipitation = numpy.array(precipitation, order='F')
    c_evaporation = numpy.array(evaporation, order='F')
    c_latent_hflux = numpy.array(latent_hflux, order='F')
    c_sensible_hflux = numpy.array(sensible_hflux, order='F')

    #
    # c-pointer for fields
    #
    c_time_ptr = c_time.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    c_time_list_ptr = c_time_list.ctypes.data_as(ctypes.POINTER(ctypes.c_int))
    c_mask_ptr = c_mask.ctypes.data_as(ctypes.POINTER(ctypes.c_int))
    c_zsurface_ptr = \
        c_zsurface.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    c_air_temperature_ptr = \
        c_air_temperature.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    c_surface_short_radiation_down_ptr = \
        c_surface_short_radiation_down.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    c_surface_long_radiation_down_ptr = \
        c_surface_long_radiation_down.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    c_precipitation_ptr = \
        c_precipitation.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    c_evaporation_ptr = \
        c_evaporation.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    c_latent_hflux_ptr = c_latent_hflux.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    c_sensible_hflux_ptr = c_sensible_hflux.ctypes.data_as(ctypes.POINTER(ctypes.c_double))

    #
    # Argument types's specification of the called Fortran c-wrapper subroutine
    #
    fortlibrary.c_CISSEMBEL_HIRHAMp.argtypes = [ctypes.c_int,
                                                ctypes.c_int,
                                                ctypes.c_int,
                                                ctypes.POINTER(ctypes.c_double),
                                                ctypes.c_double,
                                                ctypes.POINTER(ctypes.c_int),
                                                ctypes.POINTER(ctypes.c_int),
                                                ctypes.POINTER(ctypes.c_double),
                                                ctypes.POINTER(ctypes.c_double),
                                                ctypes.POINTER(ctypes.c_double),
                                                ctypes.POINTER(ctypes.c_double),
                                                ctypes.POINTER(ctypes.c_double),
                                                ctypes.POINTER(ctypes.c_double),
                                                ctypes.POINTER(ctypes.c_double),
                                                ctypes.POINTER(ctypes.c_double)]
    #
    # Call of the wrapper
    #
    fortlibrary.c_CISSEMBEL_HIRHAMp(c_ilength,
                                    c_jlength,
                                    c_tlength,
                                    c_time_ptr,
                                    c_dtime,
                                    c_time_list_ptr,
                                    c_mask_ptr,
                                    c_zsurface_ptr,
                                    c_air_temperature_ptr,
                                    c_surface_short_radiation_down_ptr,
                                    c_surface_long_radiation_down_ptr,
                                    c_precipitation_ptr,
                                    c_evaporation_ptr,
                                    c_latent_hflux_ptr,
                                    c_sensible_hflux_ptr)
    return


def __EBMinitilialize(library='../src/CISSEMBEL_CBindings.so'):
    """
    C-binding to Fortran subroutine EBMinitialize via the Fortran wrapper
    subroutine cwrapper_modEBMinitialize. It is here only to test if we can
    call it. I should not be used. Use instead CISSEMBEL.

    Parameters
    ----------.
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
        The default is '../src/CISSEMBEL_CBindings.so'.

    Returns
    -------
    None.

    """
    if isinstance(library, ctypes.CDLL):
        fortlibrary = library
    else:
        fortlibrary = ctypes.cdll.LoadLibrary(library)

    # if isinstance(depth, (float, int)):
    #     inum = 0
    #     # Single c-value
    #     c_depth = ctypes.c_double(depth)
    # else:
    #     inum = len(depth)
    #     # Number conversion, Fortran-order
    #     c_depth = numpy.array(depth, order="F", dtype=float)
    #     # c-pointer
    #     c_depth_ptr = c_depth.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    # c_rho_snow = ctypes.c_double(rho_snow)
    # c_rho_ice = ctypes.c_double(rho_ice)
    # c_cdens = ctypes.c_double(cdens)

    # if inum:
    #     # array
    #     density = numpy.zeros((inum), order="F", dtype=float)
    #     density_ptr = density.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    #     fortlibrary.c_densprofile.argtypes = [ctypes.c_int,
    #                                           ctypes.POINTER(ctypes.c_double),
    #                                           ctypes.c_double,
    #                                           ctypes.c_double,
    #                                           ctypes.c_double,
    #                                           ctypes.POINTER(ctypes.c_double)]
    #     fortlibrary.c_densprofile(ctypes.c_int(inum),
    #                               c_depth_ptr,
    #                               c_rho_snow,
    #                               c_rho_ice,
    #                               c_cdens,
    #                               density_ptr)
    # else:
    #     # skalar
    #     fortlibrary.densprofile.restype = ctypes.c_double
    #     density = fortlibrary.densprofile(ctypes.byref(c_depth),
    #                                       ctypes.byref(c_rho_ice),
    #                                       ctypes.byref(c_rho_snow),
    #                                       ctypes.byref(c_cdens))
    return


def __EBMworld(temp, pres, tmelt0, library='../src/CISSEMBEL_CBindings.so'):
    """
    C-binding to Fortran subroutine EBMworld via the Fortran wrapper
    subroutine cwrapper_modEBMinitialize. It is here only to test if we can
    call it. I should not be used. Use instead CISSEMBEL.

    Parameters
    ----------
    temp : float or numpy.ndarray
        Temperature.
    pres : float or numpy.ndarray
        Pressure of air.
    tmelt0 : float
        Melting temperature.
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings. The
        default is '../src/CISSEMBEL_CBindings.so'.

    Returns
    -------
    water_vapor_pressure : float or numpy.ndarray
        Water vapor pressure at given air pressure level(s).

    """
    if isinstance(library, ctypes.CDLL):
        fortlibrary = library
    else:
        fortlibrary = ctypes.cdll.LoadLibrary(library)

    # if isinstance(temp, (float, int)):
    #     inum = 0
    #     # Single c-value
    #     c_temp = ctypes.c_double(temp)
    #     c_pres = ctypes.c_double(pres)
    # else:
    #     inum = len(temp)
    #     # Number conversion, Fortran-order
    #     c_temp = numpy.array(temp, order="F", dtype=float)
    #     c_pres = numpy.array(pres, order="F", dtype=float)
    #     # c-pointer
    #     c_temp_ptr = c_temp.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    #     c_pres_ptr = c_pres.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    # c_tmelt0 = ctypes.c_double(tmelt0)

    # if inum:
    #     # array
    #     water_vapor_pressure = numpy.zeros((inum), order="F", dtype=float)
    #     water_vapor_pressure_ptr = \
    #         water_vapor_pressure.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    #     fortlibrary.c_vaporpress.argtypes = [ctypes.c_int,
    #                                          ctypes.POINTER(ctypes.c_double),
    #                                          ctypes.POINTER(ctypes.c_double),
    #                                          ctypes.c_double,
    #                                          ctypes.POINTER(ctypes.c_double)]
    #     fortlibrary.c_vaporpress(ctypes.c_int(inum),
    #                              c_temp_ptr,
    #                              c_pres_ptr,
    #                              c_tmelt0,
    #                              water_vapor_pressure_ptr)
    # else:
    #     # skalar
    #     fortlibrary.vaporpress.restype = ctypes.c_double
    #     water_vapor_pressure = fortlibrary.vaporpress(ctypes.byref(c_temp),
    #                                                   ctypes.byref(c_pres),
    #                                                   ctypes.byref(c_tmelt0))

    return


def __EBMfinalize(pres, library='../src/CISSEMBEL_CBindings.so'):
    """
    C-binding to Fortran subroutine EBMfinalize via the Fortran wrapper
    subroutine cwrapper_modEBMinitialize. It is here only to test if we can
    call it. I should not be used. Use instead CISSEMBEL.

    Parameters
    ----------
    pres : float or numpy.ndarray
        Pressure of air.
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings. The
        default is '../src/CISSEMBEL_CBindings.so'.

    Returns
    -------
    air_density : float or numpy.ndarray
        Density of air at given pressure level(s).

    """
    if isinstance(library, ctypes.CDLL):
        fortlibrary = library
    else:
        fortlibrary = ctypes.cdll.LoadLibrary(library)

    # if isinstance(pres, (float, int)):
    #     inum = 0
    #     # Single c-value
    #     c_pres = ctypes.c_double(pres)
    # else:
    #     inum = len(pres)
    #     # Number conversion, Fortran-order
    #     c_pres = numpy.array(pres, order="F", dtype=float)
    #     # c-pointer
    #     c_pres_ptr = c_pres.ctypes.data_as(ctypes.POINTER(ctypes.c_double))

    # if inum:
    #     # array
    #     air_density = numpy.zeros((inum), order="F", dtype=float)
    #     air_density_ptr = air_density.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    #     fortlibrary.c_rhoair.argtypes = [ctypes.c_int,
    #                                      ctypes.POINTER(ctypes.c_double),
    #                                      ctypes.POINTER(ctypes.c_double)]
    #     fortlibrary.c_rhoair(ctypes.c_int(inum), c_pres_ptr, air_density_ptr)
    # else:
    #     # skalar
    #     fortlibrary.rhoair.restype = ctypes.c_double
    #     air_density = fortlibrary.rhoair(ctypes.byref(c_pres))
    return

# -----------------------------------------------------------
#
# Main program
#
if __name__ == '__main__':
    import ctypes
    import numpy
    import xarray as xr

else:
    import ctypes
    import numpy

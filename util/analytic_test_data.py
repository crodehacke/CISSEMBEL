#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Git version control id: $Id$

Created on Mon 11 Sep 2023 07:34:59 AM CEST

-- Creation of analytic forcing and initial files.


Call example:

./analytic_test_data

@author: Christian Rodehacke, DMI Copenhagen, Denmark

@copyright: Copyright (C) 2023 Christian Rodehacke
"""
# -------------------------------------------------------------------
# This file is part of CISSEMBEL, which is the
# == Copenhagen Ice-Snow Surface Energy and Mass Balance modEL ==
#
# CISSEMBEL is free software; you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE  v.1.2
#
# Information about the EUROPEAN UNION PUBLIC LICENCE (*EUPL*) and
# translations into other European languages are available at
# https://eupl.eu (last access: 2023-09-18)](https://eupl.eu/)
#
# We hope that  this  distributed  CISSEMBEL  code, its  tools and
# documentations are usefull. Any improvements of the code, tools,
# and documentations tools are  very welcome.  Please, consider to
# share your thoughts and improvements with the community.
#
# The documentation,  tools,  and the code are provided as it are.
# NO WARRANTY  ABOUT FITNESS  AND USABILITY OF THE CODE, TOOLS, OR
# DOCUMENTATION  ARE  GIVEN.   NO  WARRANTY  IS  GIVEN  ABOUT  THE
# CORRECTNESS  OF COMPUTED RESULTS WITH THE  PROVIDED CODE, TOOLS,
# AND DOCUMENTATION. YOU USE THE CODE, DOCUMENTATION, AND TOOLS AT
# YOUR OWN RISK.           Removing  of  any  copyright  statement
# or  this  disclaimer  is considered  as a  attempt  to defraud.
#
# -------------------------------------------------------------------


# -------------------------------------------------------------------------
def parse_arguments():
    '''
    Parse the command line arguments

    Returns
    -------
    list
        parsed command line arguments.

    '''
    parser = argparse.ArgumentParser(
        description='Test data set for CISSEMBEL',
        add_help=False)



    parser.add_argument('--test_case',
                        nargs='?',
                        required=False,
                        default='melting',
                        choices=['melting', 'accumulation'],
                        help='String indicating the conditions represented by the test data set')


    parser.add_argument('--vname_lon',
                        nargs='?',
                        required=False,
                        default='lon',
                        help='variable name of the longitude')

    parser.add_argument('--vname_lat',
                        nargs='?',
                        required=False,
                        default='lat',
                        help='variable name of the latitude')

    parser.add_argument('--vname_surface_elevation',
                        nargs='?',
                        required=False,
                        default='surface_elevation',
                        help='variable name of the surface elevation')

    parser.add_argument('--output_format',
                        nargs='?',
                        required=False,
                        default='NETCDF3_CLASSIC',
                        choices=["NETCDF4", "NETCDF4_CLASSIC", "NETCDF3_64BIT", "NETCDF3_CLASSIC"],
                        help='Format of the netcdf output file')

    parser.add_argument('--output_path',
                        nargs='?',
                        required=False,
                        default='../data/functional/test',
                        help='Directory path for output file')


    # # Report used command line arguments
    print("Used settings: read command line arguments or default values")
    for single_arg in vars(parser.parse_args()): #sorted(vars(args)) :
        print('  - {:11s}'.format(single_arg)+' = '+
              str(getattr(parser.parse_args(), single_arg)) )
    print()

    return parser.parse_args()


def create_initial_file(_label, _filename, _fileformat,
                        _initial, _vname_lon, _vname_lat,
                        _iaxis, _jaxis, _kaxis, _haxis):

    #
    # INItial data set
    #
    out_initial = xr.Dataset(
        data_vars = {
            parsed_argv.vname_lon : (["jaxis", "iaxis"], _initial['longitude'],
                               {'long_name': 'longitude',
                                 'units': 'degrees',
                                 'standard_name': 'longitude'}
                              ),
            parsed_argv.vname_lat : (["jaxis", "iaxis"], _initial['latitude'],
                               {'long_name': 'latitude',
                                 'units': 'degrees',
                                 'standard_name': 'latitude'}
                              ),
            "height": (["haxis"], _initial['height'],
                       {'long_name': 'height',
                        'units': 'm',
                        'standard_name': 'height'}),
            "layer_thickness": (["kaxis"], _initial['layer_thickness'],
                                {'long_name': 'thickness of each snow layer',
                                  'units': 'meter',
                                  'standard_name': 'snow_layer_thickness'})
            },
        coords = {
            "iaxis": (["iaxis"], _iaxis,
                      {'long_name': 'Cartesian x-coordinate',
                       'units': 'count',
                       'standard_name': 'projectio_x_coordinate'}),
            "jaxis": (["jaxis"], _jaxis,
                      {'long_name': 'Cartesian y-coordinate',
                       'units': 'count',
                       'standard_name': 'projectio_y_coordinate'}),
            "kaxis": (["kaxis"], _kaxis,
                      {'long_name': 'z-layer of mesh grid',
                       'units': '1',
                       'positive': 'down',
                       'standard_name': 'model_layer_level'}),
            "haxis": (["haxis"], _haxis,
                      {'long_name': 'height class elevation levels',
                       'units': '1',
                       'standard_name': 'level_number'}),
            },
        attrs = {
            "title": "Initial data for CISSEMBEL, analytical case: "+str(_label),
            "creation_date": str(datetime.now())
            }
        )

    # Populate the remaining variables
    for var in _initial.keys():
        if var not in out_initial:
            out_initial[var] = (("jaxis", "iaxis"), _initial.get(var))

            # Add some attributes to added variables
    # #for var in out_initial.var():
            if var == 'longitude' or var == 'latitude':
                out_initial[var].attrs = [('units', 'degrees'),
                                          ('long_name', var)]
            if var == 'surface_elevation':
                out_initial[var].attrs = [('units', 'm'),
                                          ('long_name', 'surface elevation'),
                                          ('standard_name', 'surface_altitude'),]
            if var == 'surface_slope':
                out_initial[var].attrs = [('units', 'm m-1'),
                                          ('long_name', 'surface slope')]

    #
    # Write the data set
    #
    print("Written initial file '"+_filename+"' as "+_fileformat )
    out_initial.to_netcdf(_filename, format=_fileformat)

    return out_initial



def create_forcing_file(_label, _filename, _fileformat,
                        _initial, _forcing, _vname_lon, _vname_lat,
                        _iaxis, _jaxis, _number_of_time_step, _time_step):

    out_forcing = xr.Dataset(
        data_vars = {
            _vname_lon : (["jaxis", "iaxis"], _initial['longitude'],
                          {'long_name': 'longitude',
                           'units': 'degrees',
                           'standard_name': 'longitude'}
                              ),
            _vname_lat : (["jaxis", "iaxis"], _initial['latitude'],
                          {'long_name': 'latitude',
                           'units': 'degrees',
                           'standard_name': 'latitude'}
                          ),
            },
        coords = {
            "iaxis": (["iaxis"], _iaxis,
                      {'long_name': 'Cartesian x-coordinate',
                       'units': 'count',
                       'standard_name': 'projection_x_coordinate'}),
            "jaxis": (["jaxis"], _jaxis,
                      {'long_name': 'Cartesian y-coordinate',
                       'units': 'count',
                       'standard_name': 'projection_y_coordinate'}),
            # "kaxis": (["kaxis"], _kaxis,
            #           {'long_name': 'z-layer of mesh grid',
            #            'units': '1',
            #            'positive': 'down',
            #            'standard_name': 'model_layer_level'}),
            "time": (['time'], pd.date_range("2000-01-01",
                                             periods=_number_of_time_step,
                                             freq=str(_time_step)+'s'),
                    {'long_name' : 'time'})
            },
        attrs = {
            "title": "Forcing data for CISSEMBEL, analytical case: "+str(_label),
            "creation_date": str(datetime.now())
            }
        )

    for var in _forcing.keys():
        if var not in out_forcing:
            out_forcing[var] = (("time", "jaxis", "iaxis"), _forcing.get(var))

            # Add some attributes
            if var == 'longitude' or var == 'latitude':
                out_forcing[var].attrs = [('units', 'degrees'),
                                          ('long_name', var)]
            if var == 'air_pressure':
                out_forcing[var].attrs = [('units', 'Pa'),
                                          ('long_name', 'surface pressure'),]
            if var == 'cloud_cover':
                out_forcing[var].attrs = [('units', '1'),
                                          ('long_name', 'total cloud cover')]
            if var == 'dewpoint_temperature':
                out_forcing[var].attrs = [('units', 'K'),
                                          ('long_name', '2m-dew point temperature')]
            if var == 'solar_radiation':
                out_forcing[var].attrs = [('units', 'W m-2'),
                                          ('long_name', 'solar radiation, surface, downward')]
            if var == 'surface_elevation':
                out_forcing[var].attrs = [('units', 'm'),
                                          ('long_name', 'surface elevation')]
            if var == 'temperature':
                out_forcing[var].attrs = [('units', 'K'),
                                          ('long_name', '2m-air temperature')]
            if var == 'thermal_radiation':
                out_forcing[var].attrs = [('units', 'W m-2'),
                                          ('long_name', 'thermal radiation, surface, downward')]
            if var == 'total_precipitation':
                out_forcing[var].attrs = [('units', 'kg m-2 s-1'),
                                          ('long_name', 'total precipitation')]
            if var == 'wind_velocity':
                out_forcing[var].attrs = [('units', 'm s-1'),
                                          ('long_name', 'absolute wind velocity')]

    print("Written forcing file '"+_filename+"' as "+_fileformat)
    out_forcing.to_netcdf(_filename, format=_fileformat, unlimited_dims='time')

    return out_forcing

# -------------------------------------------------------------------------
# -------------------------------------------------------------------------

def build_elevation(iaxis, jaxis):

    # build a bell-shape mountain with an ...

    height_max = 4500.0
    sigma_x_fraction_axis = 0.33
    sigma_y_fraction_axis = 0.33

    iax = np.array(iaxis, dtype=float)
    jax = np.array(jaxis, dtype=float)

    xcenter = iax.mean()
    ycenter = jax.mean()

    #
    # Deduced variables
    #
    sigma_x = sigma_x_fraction_axis*len(iaxis)
    sigma_y = sigma_y_fraction_axis*len(jaxis)

    iax2d, jax2d = np.meshgrid(iax, jax)

    #
    # Central mountain
    #
    elevation0 = height_max * \
        np.exp( -((iax2d-xcenter)/sigma_x)**2 )* \
        np.exp( -((jax2d-ycenter)/sigma_y)**2 )

    #
    # Depression
    #
    xcenter_depression = xcenter
    ycenter_depression = jax.mean()*0.75

    sigma_x_depression = sigma_x*0.1
    sigma_y_depression = sigma_y*0.45

    depression = 1500.0*\
            np.exp(-((iax2d-xcenter_depression)/sigma_x_depression)**2)* \
            np.exp(-((jax2d-ycenter_depression)/sigma_y_depression)**2)

    elevation = np.maximum(0.0, elevation0 - depression)

    #
    # Domain edges are negative
    #
    elevation[0, :] = -500
    elevation[-1, :] = -500
    elevation[:, 0] = -500
    elevation[:, -1] = -500

    return elevation


def build_precipitation(iaxis, jaxis, taxis, zs):

    # build a bell-shape mountain with an ...
    # Assume a gamma-distribution in time

    iax = np.array(iaxis, dtype=float)
    jax = np.array(jaxis, dtype=float)

    rainband_elevation = 1500.
    rainband_elevation_sigma = 250.

    precip_west = 1.2
    precip_east = 0.8


    xcenter = iax.mean()
    xomega = iax.mean()*0.10

    #
    # Deduced variables
    #
    iax2d, jax2d = np.meshgrid(iax, jax)

    precip_height_factor = 1 + \
        0.33*np.exp(-((zs-rainband_elevation)/rainband_elevation_sigma)**2)

    precip_west_east_factor = \
        0.5*((precip_east+precip_west)+ \
             (precip_east-precip_west)*np.tanh((iax2d-xcenter)/xomega))


    precip_space_factor = np.random.uniform(low=0.5, high=1.0,
                                            size=precip_height_factor.shape)


    precip_time_factor = np.random.gamma(0.75, size=len(taxis))

    precip_space_factor = precip_height_factor*precip_west_east_factor*precip_space_factor
    del precip_height_factor, precip_west_east_factor
    precip_space_factor = precip_space_factor[:, :, np.newaxis]

    # precipitation = precip_space_factor*precip_time_factor

    precipitation = precip_time_factor*precip_space_factor

    return precipitation

def build_air_temperature(iaxis, jaxis, taxis, zs, lat, dtime=(6.*3600.)):

    # build a bell-shape mountain with an ...
    # Assume a gamma-distribution in time

    def calc_tma_tms(elevation, lat,
                     a_ma, gamma_ma_upper, gamma_ma_lower, gamma_ma_zs_thres,
                     b_ma, a_ms, gamma_ms, b_ms, Hinv=None):
        zs = elevation
        if Hinv is not None:
            zs = np.where(elevation>=Hinv, elevation, Hinv)
        tma = a_ma+zs*np.where(zs>gamma_ma_zs_thres,
                               gamma_ma_upper,
                               gamma_ma_lower)+np.abs(lat[:, np.newaxis])*b_ma
        tms = a_ms+gamma_ms*zs+np.abs(lat[:, np.newaxis])*b_ms

        return tma, tms

    def calc_TS(elevation, lat):
        # Martin (2011)
        return (273.15+30)-0.0075*elevation-0.6878*np.abs(lat[:, np.newaxis])


    iax = np.array(iaxis, dtype=float)
    jax = np.array(jaxis, dtype=float)
    tax = np.array(taxis, dtype=float)


    # Parameterization: Huybrechts and de Wolte (1999)
    region_tag_precipitation = 'greenland'
    #region_tag_precipitation = 'antarctica'

    # Temperature Mean Annual: Tma
    # Temperature Mean Summer: Tms
    if region_tag_precipitation == 'antarctica':
        gamma_ma_lower = -0.005102
        gamma_ma_upper = -0.014285
        gamma_ma_thres = 1500.

        a_ma = 34.46
        b_ma = -0.68775

        gamma_ms = -0.005102
        a_ms = 16.81
        b_ms = -0.27937

        Hinv = None

    elif region_tag_precipitation == 'greenland':
        gamma_ma_lower = -0.007992
        gamma_ma_upper = gamma_ma_lower
        gamma_ma_thres = 15000.

        a_ma = 49.13
        b_ma = -0.7576

        gamma_ms = -0.006277
        a_ms = 30.78
        b_ms = -0.3262

        Hinv = (20.0*(lat-65.0))[:, np.newaxis]

    Tma, Tms = calc_tma_tms(zs, lat,
                            a_ma, gamma_ma_upper, gamma_ma_lower, gamma_ma_thres,
                            b_ma, a_ms, gamma_ms, b_ms, Hinv)

    Tma = Tma[:, :, np.newaxis]
    Tms = Tms[:, :, np.newaxis]
    if region_tag_precipitation == 'antarctica':
        Tmonth = Tma-(Tms-Tma)*np.cos(2*np.pi*tax/taxis.max())
    else:
        Tmonth = Tma-(Tma-Tms)*np.cos(2*np.pi*tax/taxis.max())

    # We should have `(np.zeros_like(iaxis))` replace as a function of longitude
    time_offset = np.zeros_like(zs)+(np.zeros_like(iaxis))
    time_offset = time_offset[:, :, np.newaxis]
    Tdaily = np.cos(2*np.pi*(tax+time_offset)/86400.0)
    Tdaily = Tdaily+np.random.normal(0, scale=0.25, size=len(tax))

    temperature = Tmonth+Tdaily

    return temperature

def build_dew_temperature(airtemp):
    # The air temperature divided by dew-point temperature in the EraInterim
    # (year 1998) data set around Greenland looks like a gamma distribution.
    # It starts with a strong raise at 1.0 (no values below), reaches a maximum
    # at about 1.01, and has a longer tail towards higher values. These height
    # values fall quickly and diminish around 1.05/1.06.
    return airtemp/(1+np.random.gamma(2.0,
                                      scale=0.005,
                                      size=air_temperature.size).reshape(airtemp.shape))


def build_solar_radiation(iaxis, jaxis, taxis, lat):

    solar_radiation = build_solar_radiation_angstrom_prescott(iaxis, jaxis, taxis, lat)

    return solar_radiation

def build_solar_radiation_angstrom_prescott(iaxis, jaxis, taxis, lat):
    # Does not work as expected
    iax = np.array(iaxis, dtype=float)
    jax = np.array(jaxis, dtype=float)
    tax = np.array(taxis, dtype=float)

    seconds_per_day = 86400.0
    day_per_second = 1.0/seconds_per_day # 1/(86400.0)

    a = 0.2336
    b = 0.4987

    #distance_sun_earth = 149597870.*1.0e3 # m
    #omega = np.mod(timesteps[:8], 86400.0)/3600.0  # sunset hour-angle (hours) !
    #delta = 0.  # solar declination ange (rad)

    phi = lat*(2.0*np.pi/360.0) # latitude (rad)
    # phi = np.minimum(lat, 78.0)*(2.0*np.pi/360.0)
    phi = phi[:, :, np.newaxis]

    #todo:old:lon2d, phi = np.meshgrid(longitude, phi0)

    nday = np.floor(tax*day_per_second)+1 # number of day since 1. January
    # nday = (tax*day_per_second)+1 # number of day since 1. January
    nday_year = tax.max()*day_per_second

    d = 1+0.033*(2.0*np.pi*nday/nday_year)
    delta = 0.4093*np.sin(2.0*np.pi*nday*nday_year-1.39)


    omega_input_arccos = -np.tan(phi)*np.tan(delta)
    # Values outside of the 'acrccos' value range are filtered out. Those could
    # be found for some conditions at the high(est) latitude range.
    omega_input_arccos = np.maximum(omega_input_arccos, -1.0)
    omega_input_arccos = np.minimum(omega_input_arccos, 1.0)
    omega = np.arccos(omega_input_arccos)

    Ho = 24.0*omega/np.pi

    Ra = 37.6*d*(omega*np.sin(phi)*np.sin(delta)+
                np.cos(phi)*np.cos(delta)*np.sin(omega))

    # H : daily actual sunshine duration
    # Ho: dialy potential sunshine duration
    #  Rs/Ra = a*(H/Ho)+b
    #  => Rs = Ra*(a*H/Ho+b)

    H = Ho*0.95
    H = Ho*0.5

    # Rs = np.where(Ho == 0, Ra*b, Ra*(a*H/Ho+b)) # Rs = Ra*(a*H/Ho+b)
    eps = 1.0e-8
    Rs = Ra*(a*(H/(Ho+eps))+b)

    return Rs # Ra*(a*H/Ho+b)  # Rs = Ra*(a*H/Ho+b)



#def build_thermal_radiation():

#def build_windspeed():

#def build_cloudcover(precip):


# -------------------------------------------------------------------------
#
# Main program
#
if __name__ == '__main__':
    import argparse           # argparse : Parse command line arguments
    import numpy as np        # Numpy    : Array handling
    import pandas as pd       # Pandas   :
    import xarray as xr       # Xarrray  : Easy handling of netcdf files
    from datetime import  datetime # Current date/time
    # from pyproj import Proj   # PyProj   : Transformation between coordinates
    from pathlib import Path

    import matplotlib.pyplot as plt
    # Parse command arguments
    parsed_argv = parse_arguments()

    # Create the output directory if it is missing
    Path(parsed_argv.output_path).mkdir(parents=True, exist_ok=True)

    #
    # General settings
    #
    MELTING_TEMPERATURE = 273.15 # Kelvin
    ATMOSPHERIC_SEAL_LEVEL_PRESSURE = 99500. # Pascal

    HEIGHT_LEVELS = np.array([0., 200., 400., 700., 1000., 1300., 1600.,
                              2000., 2500., 3000., 10000.])
    LAYER_THICKNESS = np.array([1./3., 2./3., 1, 2, 2, 2.5, 3, 3.5, 4, 6.])


    longitude_range = [140.0, 170.0]
    latitude_range = [40.0, 85.0]

    time_step_width = 6.*3600.  # Seconds

    hours_per_day = 24.0 #86400.0
    seconds_per_day = 86400.0

    #
    # axis for output file
    #
    isize = 19
    jsize = 64
    timesteps = 360*seconds_per_day/time_step_width

    i_axis = [i for i in range(isize)]
    j_axis = [i for i in range(jsize)]
    k_axis = [i for i in range(LAYER_THICKNESS.size)]
    h_axis = [i for i in range(HEIGHT_LEVELS.size)]
    l_axis = [i for i in range(int(timesteps))]

    longitude = np.linspace(longitude_range[0], longitude_range[-1], len(i_axis))
    latitude = np.linspace(latitude_range[0], latitude_range[-1], len(j_axis))

    lon2d, lat2d = np.meshgrid(longitude, latitude)

    timesteps = np.array(l_axis, dtype=float)*time_step_width

    elevation = build_elevation(i_axis, j_axis)

    precipitation = build_precipitation(i_axis, j_axis, timesteps, elevation)

    air_temperature = build_air_temperature(i_axis, j_axis, timesteps,
                                            elevation, latitude,
                                            time_step_width)

    dew_temperature = build_dew_temperature(air_temperature)

    radiation = build_solar_radiation(i_axis, j_axis, timesteps, lat2d)
    seasonal_sin = np.sin(timesteps/timesteps.max()*np.pi)
    seasonal_sin_05 = np.maximum(seasonal_sin, 0.5)

    solar_radiation = radiation * 20.0 * seasonal_sin
    thermal_radiation = radiation * 15.0 * seasonal_sin_05



    # #
    # # Scenario dependend settings
    # #
    # if parsed_argv.test_case == 'melting':
    #     #
    #     # Melting only conditions
    #     #
    #     NUMBER_OF_GRID_POINTS = np.array([1, 1], dtype=int)
    #     NUMBER_OF_TIME_STEP = 120
    #     TIME_STEP = int(3*3600) # Seconds (Must be integer/'int')
    #     #
    #     # INItial values
    #     #
    #     initial_0 = {'basinid': 1,
    #                'layer_thickness': LAYER_THICKNESS,
    #                'height': HEIGHT_LEVELS,
    #                'latitude': 0.0,
    #                'longitude': 0.0,
    #                'surface_elevation': 0.0,
    #                'surface_slope': 0.01,
    #                }
    #     #
    #     # Atmospheric forcing constant values
    #     #
    #     forcing_0 = {'air_pressure': ATMOSPHERIC_SEAL_LEVEL_PRESSURE, # Pascal
    #                  'cloud_cover': 0.5, # fracton
    #                  'dewpoint_temperature':MELTING_TEMPERATURE, # Kelvin
    #                  'solar_radiation': 250.0, # W m-2
    #                  'surface_elevation': initial_0['surface_elevation'], # Meter
    #                  'temperature': MELTING_TEMPERATURE, # Kelvin
    #                  'thermal_radiation': 250.0, # W m-2
    #                  'total_precipitation': 0.0, # kg m-2 s-1
    #                  'wind_velocity': 0.0, # m s-2
    #                  }
    #     zero_field = np.zeros([NUMBER_OF_GRID_POINTS[0], NUMBER_OF_GRID_POINTS[1]])
    #     initial = {}
    #     initial = {'layer_thickness': initial_0.get('layer_thickness').squeeze(),
    #                'height': initial_0.get('height').squeeze()}
    #     for var in initial_0.keys():
    #         if var not in initial.keys():
    #             print(var)
    #             initial.update({var: zero_field+initial_0.get(var)})
    #     zero_field = np.zeros([NUMBER_OF_TIME_STEP, NUMBER_OF_GRID_POINTS[0], NUMBER_OF_GRID_POINTS[1]])
    #     forcing = dict()
    #     for var in forcing_0.keys():
    #         forcing.update({var: zero_field+forcing_0.get(var)})
    # elif parsed_argv.test_case == 'accumulation':
    #     #
    #     # Accumulation only conditions
    #     #
    #     NUMBER_OF_GRID_POINTS = np.array([1, 1], dtype=int)
    #     NUMBER_OF_TIME_STEP = 120
    #     TIME_STEP = int(3*3600) # Seconds (Must be integer/'int')
    #     #
    #     # INItial values
    #     #
    #     initial_0 = {'basinid': 1,
    #                'layer_thickness': LAYER_THICKNESS,
    #                'height': HEIGHT_LEVELS,
    #                'latitude': 0.0,
    #                'longitude': 0.0,
    #                'surface_elevation': 0.0,
    #                'surface_slope': 0.01,
    #                }
    #     #
    #     # Atmospheric forcing constant values
    #     #
    #     forcing_0 = {'air_pressure': ATMOSPHERIC_SEAL_LEVEL_PRESSURE, # Pascal
    #                  'cloud_cover': 0.5, # fracton
    #                  'dewpoint_temperature': MELTING_TEMPERATURE-0.01, # Kelvin
    #                  'solar_radiation': 0.0, # W m-2
    #                  'surface_elevation': initial_0['surface_elevation'], # Meter
    #                  'temperature': MELTING_TEMPERATURE-0.01, # Kelvin
    #                  'thermal_radiation': 0.0, # W m-2
    #                  'total_precipitation': 0.0001584, # kg m-2 s-1 #0.000158=5m(WE)/yr
    #                  'wind_velocity': 0.0, # m s-2
    #                  }
    #     zero_field = np.zeros([NUMBER_OF_GRID_POINTS[0], NUMBER_OF_GRID_POINTS[1]])
    #     initial = {}
    #     initial = {'layer_thickness': initial_0.get('layer_thickness').squeeze(),
    #                'height': initial_0.get('height').squeeze()}
    #     for var in initial_0.keys():
    #         if var not in initial.keys():
    #             print(var)
    #             initial.update({var: zero_field+initial_0.get(var)})
    #     zero_field = np.zeros([NUMBER_OF_TIME_STEP, NUMBER_OF_GRID_POINTS[0], NUMBER_OF_GRID_POINTS[1]])
    #     forcing = dict()
    #     for var in forcing_0.keys():
    #         forcing.update({var: zero_field+forcing_0.get(var)})

    #
    # create the data files
    #


    # #
    # # Output data set
    # #
    # #
    # # INItial data set
    # #
    # filename = parsed_argv.output_path+'/'+parsed_argv.test_case+'.initial.nc'
    # create_initial_file(parsed_argv.test_case, filename, parsed_argv.output_format,
    #                     initial, parsed_argv.vname_lon, parsed_argv.vname_lat,
    #                     i_axis, j_axis, k_axis, h_axis)
    # #
    # # FoRCing data set
    # #
    # filename = parsed_argv.output_path+'/'+parsed_argv.test_case+'.forcing.nc'
    # create_forcing_file(parsed_argv.test_case, filename, parsed_argv.output_format,
    #                     initial, forcing, parsed_argv.vname_lon, parsed_argv.vname_lat,
    #                     i_axis, j_axis, NUMBER_OF_TIME_STEP, TIME_STEP)


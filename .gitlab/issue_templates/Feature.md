/label ~feature

### Which features do you suggest?
<!-- Which process or problem do we address with the suggested feature? -->



### Intended users
<!-- Who will use this feature? -->



### Some details about the feature
<!-- Some more details
* For example, links to publications
* Code snippets about how it will look, if available (in any programming language).
-->



### Availability & Testing
<!-- Any idea how we could test a correct implementation? If so, please share your thoughts; otherwise, ignore them please -->



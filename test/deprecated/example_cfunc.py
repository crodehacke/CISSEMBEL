#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 27 11:10:19 CEST 2021


-- Test and plots for mod+physic.F08

@author: Christian Rodehacke (cr), DMI Copenhagen, Denmark

@copyright: Copyright (C) 2016-2025 Christian Rodehacke
"""
# -------------------------------------------------------------------
# This file is part of CISSEMBEL, which is the
# == Copenhagen Ice-Snow Surface Energy and Mass Balance modEL ==
#
# CISSEMBEL is free software; you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE  v.1.2
#
# Information about the EUROPEAN UNION PUBLIC LICENCE (*EUPL*) and
# translations into other European languages are available at
# https://eupl.eu (last access: 2021-10-01)](https://eupl.eu/)
#
# We hope that  this  distributed  CISSEMBEL  code, its  tools and
# documentations are usefull. Any improvements of the code, tools,
# and documentations tools are  very welcome.  Please, consider to
# share your thoughts and improvements with the community.
#
# The documentation,  tools,  and the code are provided as it are.
# NO WARRANTY  ABOUT FITNESS  AND USABILITY OF THE CODE, TOOLS, OR
# DOCUMENTATION  ARE  GIVEN.   NO  WARRANTY  IS  GIVEN  ABOUT  THE
# CORRECTNESS  OF COMPUTED RESULTS WITH THE  PROVIDED CODE, TOOLS,
# AND DOCUMENTATION. YOU USE THE CODE, DOCUMENTATION, AND TOOLS AT
# YOUR OWN RISK.           Removing  of  any  copyright  statement
# or  this  disclaimer  is considered  as a  attempt  to defraud.
#
# -------------------------------------------------------------------

# -----------------------------------------------------------
#
# Main program
#
if __name__ == '__main__':
    import ctypes as ct
    #import numpy as np

    salt = ct.c_double(34.)
    pres = ct.c_double(102400.)

    LIBRARY_NAME = './CISSEMBEL_CBindings.so' # INCLUDING path, e.g., ./

    fortlib = ct.cdll.LoadLibrary(LIBRARY_NAME)
    TfrezSeaWater = fortlib.TfrezSeaWater
    TfrezSeaWater.restype = ct.c_double

    TfrezSeaWater.restype = ct.c_double
    Tfrez0 = TfrezSeaWater(ct.byref(pres), ct.byref(salt))

    print(pres,salt)
    print(Tfrez0)

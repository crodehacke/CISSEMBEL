Open tasks for the _CISSEMBEL_ code
===================================

In addition to the issues at git-repository hosting service page, open
  tasks are listed below in different catagories. After solving the
  related tasks please report in [CHANGES.md](CHANGES.md).

Details about the _Copenhagen Ice-Snow Surface Energy and Mass Balance modEL_
  (**CISSEMBEL**) are provided in the [README.md](README.md).


Technique
---------

* Canonical variable names that follow the CMIP standard
  - More flexible code that lists all written NetCDF variable names in
    one single place.
* Diagnostic
  - Firn air content?
    ```
    rho_firn(rho_air=2kg/m3, rho_ice=910kg/m3) =
       fraction*rho_air + (fraction-1)*rho_ice
    ```
    and
    ```
    firn_air_thickness = /int^z(Bottom)_z(Top) fraction * dz
    ```
* Height corrections for provided turbulent sensible and latent heat
  flux. (Once implemented, adjust README.md)

* Define iteration controlling thresholds so that they depend on the
  used numerical precision defined in
  [./src/mod_numeric_kinds.F08](./src/mod_numeric_kinds.F08).

* Make ISO_FORTRAN_ENV as the unchangeable standard; see
  [DEPRECATED.md](DEPRECATED.md).

Physical Processes
------------------

* Tracer transport


Documentation
-------------

* Document required fields in the initial file (To adjust the filename
  and the variable names, modify the section `ini_file` in the
  namelist (`fname_nml4ini`):
  - `x-axis`: Increasing index, e.g., integer
     if `#define DIMENSION_AXES_INTEGER`
  - `y-axis`: Increasing index, e.g., integer
     if `#define DIMENSION_AXES_INTEGER`
  - `k-axis`: Increasing index, e.g., integer
     if `#define DIMENSION_AXES_INTEGER`
  - `h-axis`: Increasing index, e.g., integer
     if `#define DIMENSION_AXES_INTEGER`
  - `depth(k)`: Depth below the surface (Meter)
  - `height(h)`: Height level for height classes (Meter)
  - `Longitude(y, x)`: Longitude at each grid location
  - `Latitude(y, x)`: Latitude at each grid location
  - `zs_ref(y, x)`: Reference height (Meter) onto which the SMB shall
     be computed.
  - `reg_mask(y, x)`: Mask defining regions (used for time series
     across regions: **TSS** and **TSD** files).
  - `station_mask(y, x)`: Mask where stations are located allowing high
    frequent writing (**STA** file).
  - `station_id(y, x)`: Station-id at masked locations (**STA** file).
  - `cellarea(y, x)`: Grid cell area (m2).
  - Question: Do we need basal (geothermal) heat flux in the future?


Citation
--------

* Indicate citation(s) (`CITATION.md`) after publication(s) is(are)
  submitted/accepted.
* Integrate Bibtex citations in code documentation.


Miscellaneous
-------------

* Address `@todo`s in the code and its documentation
* Analyze for a _large_ real problem the performance of the code. In
  particular show the gain by using
  - Message Passing Interface (MPI)
    - It seems that OpenMPI outperforms MPI incl. Parallel IO
      (NetCDF4) on my laptop. Since, NetCDF4-IO seems to be slower, we
      have to perform checks for case, where both use
      NetCDF4-IO. Anyhow, for huge problems, it might be anyhow
      required.
    - Shall we improve the MPI code?
  - Does hybrid code (OpenMP+MPI) offers a benefit?
  - How good works the parallel NetCDF4-IO?
    - NetCDF4 seems to slower than Classical NetCDF, but compression
      capabilities may offset it for environments where IO or storage
      size can be a limiting factor.

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov  1 11:49:17 2022


Read monthly snow temperatures at 10m depth provided by Baptiste. This data set
is converted into a netcdf-file


@author: Christian Rodehacke, DMI Copenhagen, Denmark

@copyright: Copyright (C) 2022-2025 Christian Rodehacke
"""
# -------------------------------------------------------------------
# This file is part of CISSEMBEL, which is the
# == Copenhagen Ice-Snow Surface Energy and Mass Balance modEL ==
#
# CISSEMBEL is free software; you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE  v.1.2
#
# Information about the EUROPEAN UNION PUBLIC LICENCE (*EUPL*) and
# translations into other European languages are available at
# https://eupl.eu (last access: 2021-10-01)](https://eupl.eu/)
#
# We hope that  this  distributed  CISSEMBEL  code, its  tools and
# documentations are usefull. Any improvements of the code, tools,
# and documentations tools are  very welcome.  Please, consider to
# share your thoughts and improvements with the community.
#
# The documentation,  tools,  and the code are provided as it are.
# NO WARRANTY  ABOUT FITNESS  AND USABILITY OF THE CODE, TOOLS, OR
# DOCUMENTATION  ARE  GIVEN.   NO  WARRANTY  IS  GIVEN  ABOUT  THE
# CORRECTNESS  OF COMPUTED RESULTS WITH THE  PROVIDED CODE, TOOLS,
# AND DOCUMENTATION. YOU USE THE CODE, DOCUMENTATION, AND TOOLS AT
# YOUR OWN RISK.           Removing  of  any  copyright  statement
# or  this  disclaimer  is considered  as a  attempt  to defraud.
#
# -------------------------------------------------------------------

# -------------------------------------------------------------------------
#
# Main program
#
if __name__ == '__main__':
    # import argparse           # argparse : Parse command line arguments
    import numpy as np        # Numpy    : Array handling
    # import pandas as pd       # Pandas   :
    import xarray as xr       # Xarrray  : Easy handling of netcdf files
    from datetime import  datetime # Current date/time
    # from pyproj import Proj   # PyProj   : Transformation between coordinates
    # from pathlib import Path
    import csv
    # import os

    fname_input_csv = 'PROMICE_monthly_subsurface_temperature_10m.csv'
    #  tr , ';' < PROMICE_monthly_subsurface_temperature_10m.csv > PROMICE_monthly_subsurface_temperature_10m.2.csv
    # fname_input_csv = 'PROMICE_monthly_subsurface_temperature_10m.2.csv'

    fname_output_nc = 'PROMICE_monthly_subsurface_temperature_10m.nc'


    print('Open input file '+fname_input_csv)

    # Has problems with the file ....
    # data_in = pd.read_csv(fname_input_csv, sep=',')
    # data_in = pd.read_csv(fname_input_csv, sep=',', header=0)
    # # data_in = pd.read_csv(fname_input_csv, sep=';', header=0)
    #                       # names = ['date',
    #                       #          'site',
    #                       #          'latitude',
    #                       #          'longitude',
    #                       #          'elevation',
    #                       #          'depthOfTemperatureObservation',
    #                       #          'temperatureObserved',],)
    #                       # usecols=[0, 1, 2, 3, 4, 5, 6, 7],)
    #                       # dtype)

    date_str_csv = []
    site_csv = []
    lat_csv = []
    lon_csv= []
    elevation_csv = []
    depth_csv = []
    temp_csv = []
    #
    year_csv = []
    month_csv= []
    day_csv = []
    date_csv = []

    # with open(fname_input_csv, newline='') as csvfile:
    with open(fname_input_csv) as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',') #, quotechar='|')
        for irow, row in enumerate(spamreader):
            # print(irow,row[0])
            if irow == 0:
                print('Header :'+', '.join(row))
            else:
                date_str_csv.append(row[0])
                site_csv.append(row[1])
                lat_csv.append(float(row[2]))
                lon_csv.append(float(row[3]))
                elevation_csv.append(float(row[4]))
                depth_csv.append(float(row[5]))
                temp_csv.append(float(row[6]))

                date_split = row[0].split('-')
                yyyy = date_split[2]
                mm = date_split[1]
                dd = date_split[0]
                day_csv.append(dd)
                month_csv.append(mm)
                year_csv.append(dd)
                date_csv.append(yyyy+'-'+mm+'-'+dd)


        #     print(', '.join(row))
        # print(spamreader[0])

    site_list = []
    for item in site_csv:
        if item not in site_list:
            site_list.append(item)


    date_str_list = []
    # date_str_csv_copy_sort = date_str_csv.copy()
    # date_str_csv_copy_sort.sort()
    # for item in date_str_csv_copy_sort:
    #     if item not in date_str_list:
    #         date_str_list.append(item)
    date_csv_copy_sort = date_csv.copy()
    date_csv_copy_sort.sort()
    date_csv_copy_sort = np.unique(date_csv_copy_sort)
    for item in date_csv_copy_sort:
        if item not in date_str_list:
            yyyy = item.split('-')[0]
            mm = item.split('-')[1]
            dd = item.split('-')[2]
            # date_str_list.append(yyyy+'-'+mm+'-'+dd)
            date_str_list.append(dd+'-'+mm+'-'+yyyy)


    length_sites = len(site_list)
    length_date = len(date_str_list)

    #
    # Convert date/time information to numpy.datetime64
    #
    #date_list = np.zeros((length_date))
    date_list = np.full((length_date), np.datetime64('1900-01-01T00:00:00'))

    for ic, item in enumerate(date_str_list):
        item_split = item.split('-')
        yyyy = item_split[2]
        mm = item_split[1]
        dd = item_split[0]
        if type(date_list[0]) == np.datetime64:
            hour_str = '12'
            minute_str = '00'
            second_str = '00'
            date_list[ic] = np.datetime64(yyyy+'-'+mm+'-'+dd+
                                          'T'+hour_str+':'+
                                          minute_str+':'+second_str)
        else:
            date_list[ic] = np.datetime64(yyyy+'-'+mm+'-'+dd)

    iaxis = np.arange(0, length_sites)
    jaxis = np.array([0])


    #
    #  Initialize output fields
    #
    # temp10 = np.zeros((len(date_list), 1, len(site_list)))
    temp10 = np.full((len(date_list), 1, len(site_list)), np.NaN)
    depth10 = temp10.copy()  #np.zeros_like(temp10)
    longitude = temp10.copy()  #np.zeros_like(temp10)
    latitude = temp10.copy()  #np.zeros_like(temp10)
    elevation = temp10.copy()  #np.zeros_like(temp10)

    #
    # LOOP over all sites and all time steps
    #
    for isite, site_str in enumerate(site_list):
        #
        # Index of all read records belonging to the site 'site_str'
        #
        flags = [s in site_str for s in site_csv[:]]
        index = [ic for ic, ff in enumerate(flags) if ff]

        #
        # Information and skip (continue) if not data are present
        #
        if any(flags):
            print('  Work on  site "'+site_str+'" with records '+
                  str(len(index))+': ['+str(min(index))+', '+str(max(index))+
                  '] => isite='+str(isite))
        else:
            print('  Skip the site "'+site_str+'"')
            continue

        #
        # Loop all found data location for the current site 'site_str'
        # These locations are accessed by its 'index'
        #
        for idx in index:
            date_site = date_str_csv[idx]

            #
            # Loop all (sorted unique) time steps
            #
            for idate, date_tag in enumerate(date_str_list):
                if date_site == date_tag:
                    # print('+ '+str(idate)+'['+date_tag+'] = '+date_site+' :: '+str(idx))
                    temp10[idate, 0, isite] = temp_csv[idx]
                    depth10[idate, 0, isite] = depth_csv[idx]
                    longitude[idate, 0, isite] = lon_csv[idx]
                    latitude[idate, 0, isite] = lat_csv[idx]
                    elevation[idate, 0, isite] = elevation_csv[idx]



    #
    # Date/time array relative to a given reference time
    #
    REFERENCE_YEAR = 2007
    REFERENCE_MONTH = 9
    REFERENCE_DAY = 23
    REFERENCE_HOUR = 16
    REFERENCE_MINUTE = 0
    REFERENCE_SECOND = 0

    REFERENCE_DATETIME_STR= \
        '{:04d}-'.format(REFERENCE_YEAR)+ \
        '{:02d}-'.format(REFERENCE_MONTH)+ \
        '{:02d}T'.format(REFERENCE_DAY)+ \
        '{:02d}:'.format(REFERENCE_HOUR)+ \
        '{:02d}:'.format(REFERENCE_MINUTE)+ \
        '{:02d}'.format(REFERENCE_SECOND)

    print('Reference time '+REFERENCE_DATETIME_STR)

    REFERENCE_DATETIME = np.datetime64(REFERENCE_DATETIME_STR)

    #TODO ??
    seconds_year_ref = float(np.timedelta64(date_list[0]-REFERENCE_DATETIME)/
                              np.timedelta64(1, 's'))
    # datetime_list = date_list+seconds_year_ref

    datetime_list = np.zeros((len(date_list)))
    for ic, item in enumerate(date_list):
        datetime_list[ic] = float(np.timedelta64(item-REFERENCE_DATETIME)/
                                  np.timedelta64(1, 's'))


    # ----------------------------------------------------------------
    #
    # Create output file
    #
    #
    # Global attributes
    #
    global_attrs = {"title": "PROMICE monthly 10m-temperature data",
                    "data_source": fname_input_csv,
                    "creation_date": str(datetime.now()),
                    "Thanks": "Vandecrux, Baptiste"}



    # Output axis
    dname_taxis = 'time'
    dname_jaxis = 'jaxis'
    dname_iaxis = 'iaxis'

    out_datetime = xr.DataArray(datetime_list,
                                dims=[dname_taxis],
                                attrs={'long_name': 'time',
                                       'units': 'seconds since '+str(REFERENCE_DATETIME).replace('T', ''),
                                       'calendar': 'standard',
                                       'reference_time': str(REFERENCE_DATETIME),},
                                )
    #
    jaxis = xr.DataArray(jaxis, dims=[dname_jaxis],
                         attrs= {'long_name': 'Cartesian y-coordinate',
                                 'units': 'count',
                                 'standard_name': 'projection_y_coordinate',},
                         )
    iaxis = xr.DataArray(iaxis, dims=[dname_iaxis],
                         attrs= {'long_name': 'Cartesian x-coordinate',
                                 'units': 'count',
                                 'standard_name': 'projection_x_coordinate',},
                         )
    #
    # Coordinates
    #
    coordinates = {dname_taxis: (out_datetime),
                   dname_jaxis: (jaxis),
                   dname_iaxis: (iaxis),
                   }

    #
    # Output data
    #
    vname_temp10m = 'temp10m'
    vname_depth10m = 'depth'
    vname_longitude = 'lon'
    vname_latitude = 'lat'
    vname_elevation = 'surface_elevation'



    out_site_tag = xr.DataArray(np.expand_dims(site_list, axis=0),
                                dims=[dname_jaxis, dname_iaxis],
                                attrs={'long_name': 'Station label',
                                       },
                                )


    out_lon = xr.DataArray(np.nanmean(latitude[:,:,:], axis=0),
                           dims=[dname_jaxis, dname_iaxis],
                           attrs={'long_name': 'longitude',
                                  'units': 'degrees',},
                           )

    out_lat = xr.DataArray(np.nanmean(latitude[:,:,:], axis=0),
                           dims=[dname_jaxis, dname_iaxis],
                           attrs={'long_name': 'latitude',
                                  'units': 'degrees',},
                           )

    out_elevation = xr.DataArray(np.nanmean(elevation[:,:,:], axis=0),
                                 dims=[dname_jaxis, dname_iaxis],
                                 attrs={'long_name': 'surface elevation',
                                        'units': 'meter',
                                        'coordinates':
                                            vname_longitude+' '+vname_latitude,},
                                 )

    #
    # Temporal evoving fields
    #
    out_temp10m = xr.DataArray(temp10+273.15,
                               dims=[dname_taxis, dname_jaxis, dname_iaxis],
                               attrs={'long_name': 'temperature at 10m depth',
                                      'units': 'Kelvin',
                                      'coordinates':
                                          vname_longitude+' '+vname_latitude,},
                                   )
    out_depth10m = xr.DataArray(depth10,
                                dims=[dname_taxis, dname_jaxis, dname_iaxis],
                                attrs={'long_name': 'depth of temperature sensor',
                                      'units': 'meter',
                                      'coordinates':
                                          vname_longitude+' '+vname_latitude,},
                                   )

    data_out = xr.Dataset(coords = coordinates,
                          data_vars = {
                              'site': out_site_tag,
                              vname_longitude: out_lon,
                              vname_latitude : out_lat,
                              vname_elevation: out_elevation,
                              vname_temp10m: out_temp10m,
                              vname_depth10m: out_depth10m,
                              })

    print('Write output '+fname_output_nc)
    data_out.to_netcdf(fname_output_nc)



Compiler flags for different CPU architectures
==============================================



# Flags for EPYC processors from AMD

For the following compilers, a set of parameters have been suggested
which may lead to more performand code. To enable a good memory
bandwidth, you may use for OpenMP code the following environmental
variable: `OMP_PLACES=sockets` together with `OMP_PROC_BIND=true`

## GNU compiler `gfortran`
```
-O3 -march=znver1 -mtune=znver1 -mfma -mavx2 -m3dnow -fomit-frame-pointer
```
Depending on the AMD architecture and the compiler version you may
replace the flag `-mtune=znver1` which a more appropriate flag.


## AOOC compiler `flang`

```
-O3 -mavx -fplugin-arg-dragonegg-llvm-codegen-optimize=3
-fplugin-arg-dragonegg-llvm-ir-optimize=3
```

Since earlier version of AMD's Zen architecture does not support FMA4,
you may avoid the flag `-mfma4`.

## Intel compiler `ifort`
```
-O3 -march=core-avx2 -align array64byte -fma -ftz -fomit-frame-pointer
```

Here the flag `-march=core-avx2` forces the compiler to build _AVX2_
code using AVX2 instructions of EPYC CPUs. You may try the flag
`-xCORE-AVX2` but it may fail.


## PGI compiler
```
-O3 -tp zen -Mvect=simd -Mcache_align -Mprefetch -Munroll
```
You may replace `zen` by an updated flag if the compiler supports a
new version of the Zen architecture if applicable.
#!/bin/bash
# Build of parallel-IO ready netcdf libraries include building
# - OpenMPI
# - HDF5
# - NetCDF-C
# - NetCDF-Fortran
#
#   Please fetch the required OpenMPI, HDF5, NetCDF-C, and
# NetCDF-Fortran libraries from their dedicated download sites and
# place them in either `$HOME/Downloads` or in your preferred place
# and adjust afterwards the information about **Sources** (below). You
# may also adjust the **Installation directories** to your favor
# choices.
#
#   You may check in addition the build instructions for each
# libraries in the **Functions** section.
#
# Call example: ./Build_OpenMPI_HDF5_NetCDF.bash
#
# -------------------------------------------------------------------
#
# Copyright (C) 2016-2025 Christian Rodehacke
#
# This file is part of CISSEMBEL, which is the
# == Copenhagen Ice-Snow Surface Energy and Mass Balance modEL ==
#
# CISSEMBEL is free software; you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE  v.1.2
#
# Information about the EUROPEAN UNION PUBLIC LICENCE (*EUPL*) and
# translations into other European languages are available at
# https://eupl.eu (last access: 2023-09-18)](https://eupl.eu/)
#
# We hope that  this  distributed  CISSEMBEL  code, its  tools and
# documentations are usefull. Any improvements of the code, tools,
# and documentations tools are  very welcome.  Please, consider to
# share your thoughts and improvements with the community.
#
# The documentation,  tools,  and the code are provided as it are.
# NO WARRANTY  ABOUT FITNESS  AND USABILITY OF THE CODE, TOOLS, OR
# DOCUMENTATION  ARE  GIVEN.   NO  WARRANTY  IS  GIVEN  ABOUT  THE
# CORRECTNESS  OF COMPUTED RESULTS WITH THE  PROVIDED CODE, TOOLS,
# AND DOCUMENTATION. YOU USE THE CODE, DOCUMENTATION, AND TOOLS AT
# YOUR OWN RISK.           Removing  of  any  copyright  statement
# or  this  disclaimer  is considered  as a  attempt  to defraud.
#
# -------------------------------------------------------------------
echo " Start at $(date)"
echo " Start at $(pwd)"
set -ex

#
# Soures
#
SOURCE_OPENMPI=${HOME}/Downloads/openmpi-4.1.4.tar.gz
SOURCE_HPF5=${HOME}/Downloads/hdf5-1.12.2.tar
SOURCE_NETCDF_C=${HOME}/Downloads/netcdf-c-4.9.0.tar.gz
SOURCE_NETCDF_FORT=${HOME}/Downloads/netcdf-fortran-4.6.0.zip

#
# Compiler settings
#
#COMPILER_TAG=aocc  # AMD compiler suite
COMPILER_TAG=gcc   # GNU compiler suite
#COMPILER_TAG=intel # Intel compiler suite

case $COMPILER_TAG in
gcc)
    # Serial compilers
    C_COMPILER=gcc
    CXX_COMPILER=g++
    FORT_COMPILER=gfortran
    # MPI compilers
    C_MPI_COMPILER=mpicc
    CXX_MPI_COMPILER=mpic++
    FORT_MPI_COMPILER=mpifort
    ;;
aocc)
    # Serial compilers
    C_COMPILER=clang
    CXX_COMPILER=clang++
    FORT_COMPILER=fclang
    # MPI compilers
    C_MPI_COMPILER=mpicc
    CXX_MPI_COMPILER=mpic++
    FORT_MPI_COMPILER=mpifort
    ;;
intel)
    # Serial compilers
    C_COMPILER=icc
    CXX_COMPILER=icpc
    FORT_COMPILER=ifort
    # MPI compilers
    C_MPI_COMPILER=mpicc
    CXX_MPI_COMPILER=mpic++
    FORT_MPI_COMPILER=mpifort
    ;;
*)
    # Serial compilers
    C_COMPILER=icc
    CXX_COMPILER=icpc
    FORT_COMPILER=ifort
    # MPI compilers
    C_MPI_COMPILER=mpicc
    CXX_MPI_COMPILER=mpic++
    FORT_MPI_COMPILER=mpifort
    #
    #
    #
    echo " Unknown compiler $COMPILER_TAG"
    echo " Please construct a new section for the case-select-contruction in $0"
    echo " S T O P"
    exit 1
    ;;
esac

#
# Installation directories
#
INSTALL_DIR_OPENMPI=${HOME}/usr/local/openmpi-4.1.4-${COMPILER_TAG}
INSTALL_DIR_HDF5=${HOME}/usr/local/hdf5-1.12.2-${COMPILER_TAG}
INSTALL_DIR_NETCDF_C=${HOME}/usr/local/netcdf-4.9.0-${COMPILER_TAG}
INSTALL_DIR_NETCDF_FORT=${INSTALL_DIR_NETCDF_C}


set +x

NO_PARALLEL_MAKE=8

# --------------------------------------------------------
#
# Functions
#
build_openmpi() {
    echo " ****** Build OpenMPI ********"

    CC=$C_COMPILER CXX=$CCC_COMPILER FC=$FORT_COMPILER \
      ./configure --prefix=${INSTALL_DIR_OPENMPI}

    make -j $NO_PARALLEL_MAKE && make install

    echo "export PATH=${INSTALL_DIR_OPENMPI}:${PATH}"
    echo "export LD_LIBRARY_PATH=$OPENMPI/lib:${LD_LIBRARY_PATH}"
    export PATH=${INSTALL_DIR_OPENMPI}/bin:${PATH}
    export LD_LIBRARY_PATH=${INSTALL_DIR_OPENMPI}/lib:${LD_LIBRARY_PATH}
    }

build_hdf5() {
    echo " ****** Build HDF5 ***********"

    echo "export HDF5=${INSTALL_DIR_HDF5}"
    export HDF5=${INSTALL_DIR_HDF5}

    CC=$C_MPI_COMPILER CXX=$CCC_MPI_COMPILER FC=$FORT_MPI_COMPILER \
      CFLAGS=-fPIC ./configure --enable-shared --enable-parallel \
      --enable-fortran --enable-fortran2003 \
      --prefix=${INSTALL_DIR_HDF5}

    make -j $NO_PARALLEL_MAKE && make install
}

build_netcdf_c() {
    echo " ****** Build NetCDF-C *******"

    export NETCDF=${INSTALL_DIR_NETCDF_C}

    set -x
    CC=$C_MPI_COMPILER LDFLAGS=-L${HDF5}/lib \
      LIBS=-lhdf5 CPPFLAGS=-I${HDF5}/include \
      ./configure --prefix=${INSTALL_DIR_NETCDF_C}
    set +x

    make -j $NO_PARALLEL_MAKE && make install
}

build_netcdf_fort() {
    echo " ****** Build NetCDF-Fort ****"

    set -x
    CC=$C_MPI_COMPILER FC=$FORT_MPI_COMPILER \
      LDFLAGS=-L${INSTALL_DIR_NETCDF_FORT}/lib \
      CPPFLAGS=-I${INSTALL_DIR_NETCDF_FORT}/include LIBS=-lnetcdf \
      ./configure --prefix=${INSTALL_DIR_NETCDF_FORT} \
      --enable-parallel-tests
    set +x

    make -j $NO_PARALLEL_MAKE && make install
}


# --------------------------------------------------------
#
# Main program body
#

# --------------------------------------------------------
#
# Create temporary directory, where we build the from the
# sources the libraries
#
BuildDir=$( mktemp -d BuildLibs4ParallelNetCDFio.XXXXXXX.tmp -p . )
echo " Build libraries in BuildDir=$BuildDir"

#
# Copy code sources
#
ierror=0
for F in ${SOURCE_OPENMPI} ${SOURCE_HPF5} \
         ${SOURCE_NETCDF_C} ${SOURCE_NETCDF_FORT}
do
    echo " Check source $F"
    if [ -f $F ] ; then
	echo "  cp $F -> $BuildDir"
	cp  $F $BuildDir
    else
	echo "  MISS $F"
	ierror=$(( ierror+1 ))
    fi
done

if [ $ierror -gt 0 ] ; then
    echo " Missed $ierror sources"
    echo "S T O P"
    exit 1
fi


HERE=$(pwd)
echo " Jump into BuildDir=$BuildDir"
cd $BuildDir



#
# Unpack code sources
#
wdir='.'
for F in $wdir/*.gz ; do
    if [ -f $F ] ; then
       echo "  gunzip $F"
       gunzip $F
       [[ -f $F ]] && rm -f $F
    fi
done

for F in $wdir/*.zip ; do
    if [ -f $F ] ; then
       echo "  unzip $F"
       unzip -q $F
       [[ -f $F ]] && rm -f $F
    fi
done

for F in $wdir/*.tar ; do
    if [ -f $F ] ; then
       echo "  untar $F"
       tar xf $F
       [[ -f $F ]] && rm -f $F
    fi
done


# --------------------------------------------------------
#
# Call the build commands
#
cd openmpi*
build_openmpi

cd ..

cd hdf5*
build_hdf5

cd ..

cd netcdf-c*
build_netcdf_c

cd ..


cd netcdf-fortran*
build_netcdf_fort
cd ..

# --------------------------------------------------------
#
# Jump back
#
cd $HERE

echo "Clean up"
echo " Remove BuildDir=${BuildDir}"
[[ -d ${BuildDir} ]] && rm -f ${BuildDir}

#
# Information on standard out
#
echo
echo "========= INFORMATION ====[begin]====================="
echo "# -- COMPILER_TAG $COMPILER_TAG --"
echo "export HDF5=${INSTALL_DIR_HDF5}"
echo "export NETCDF=${INSTALL_DIR_NETCDF_C}"
echo "export PATH=${INSTALL_DIR_OPENMPI}/bin:${NETCDF}/bin:${HDF5}/bin:\$PATH"
echo "export LD_LIBRARY_PATH=${INSTALL_DIR_OPENMPI}/lib:${NETCDF}/lib:${HDF5}/lib:\${LD_LIBRARY_PATH:-.}"
echo "========= INFORMATION ====[end]======================="
echo

echo " Reach the last line ... Bye bye ($(date))."
exit 0

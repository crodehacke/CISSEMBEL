#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Git version control id: $Id$

Compute best guess (re)start conditions from mean climate conditions

Call example (1): with a time-mean climate forcing file and an initial
     file. The time mean could be constructed with the Climate Date
     Operator (cdo), e.g.,
       cdo -f nc2 timmean Forcing_With_Many_Records.nc Forcing_Mean.nc
     Note, we use the cdo switch '-f nc2' to write a classical netcdf
     file to avoid problems with xarray's netcdf engine.

    ./aveclim2start.py --input_climate Forcing_Mean.nc \
                       --input_initial Initial.nc \
                       --output $(pwd)/Restart_start.nc \
                       --vname_climate_airtemperature 2m_air_temperature \
                       --vname_climate_precipitation total_precipitation \
                       --vname_climate_surface_elevation orography \
                       --vname_initial_layer_thickness layer_thickness \
                       --vname_initial_axis_right x \
                       --vname_initial_axis_left y \
                       --vname_climate_longitude longitude \
                       --vname_climate_latitude latitude \
                       --vname_initial_surface_elevation topography \
                       --vname_initial_longitude long \
                       --vname_initial_latitude lati \
                       --lapse_rate 0.005

NOTE: The axis count is right aligned. It means if you have the following
      netcdf field, e.g. (obtained by "ncdump -h Forcing_Mean.nc"):

          float surface_altitude(time, y, x) ;

      You may use the following flags indicating the name of these
      axes in the call of "aveclim2start.py". In this example, please
      use
          --vname_initial_axis_left y
      and
          --vname_initial_axis_right x


Call example (2): One climate forcing file including many records/time steps.
     Here, the mean value is computed internally.

    ./aveclim2start.py --input_climate Forcing_Mean_2022.nc \
                       --input_initial Initial.nc \
                       --output Restart_start_mean2022.nc \


Call example (3): A consecutive bunch of forcing files including many records.
     The mean value is computed internally across all recognized forcing files.
     The layout amoung all files should be identical.

    ./aveclim2start.py --input_climate Forcing_Mean_19??.nc \
                       --input_initial Initial.nc \
                       --output Restart_start_mean19xx.nc


To activate the construction of a (re)start file including HEIGHT CLASSES
     levels, you initial file has to provide height levels. To
     activate the construction of height levels, you have to provide
     the corresponding variable name in the initial file, e.g.:
     "--vname_initial_height height".
     To activate the height class plus case, you have to provide the
     additional flag "--height_class_plus" too.


@author: Christian Rodehacke, DMI Copenhagen, Denmark

@copyright: Copyright (C) 2022-2025, Christian Rodehacke
"""
# -------------------------------------------------------------------
# This file is part of CISSEMBEL, which is the
# == Copenhagen Ice-Snow Surface Energy and Mass Balance modEL ==
#
# CISSEMBEL is free software; you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE  v.1.2
#
# Information about the EUROPEAN UNION PUBLIC LICENCE (*EUPL*) and
# translations into other European languages are available at
# https://eupl.eu (last access: 2021-10-01)](https://eupl.eu/)
#
# We hope that  this  distributed  CISSEMBEL  code, its  tools and
# documentations are usefull. Any improvements of the code, tools,
# and documentations tools are  very welcome.  Please, consider to
# share your thoughts and improvements with the community.
#
# The documentation,  tools,  and the code are provided as it are.
# NO WARRANTY  ABOUT FITNESS  AND USABILITY OF THE CODE, TOOLS, OR
# DOCUMENTATION  ARE  GIVEN.   NO  WARRANTY  IS  GIVEN  ABOUT  THE
# CORRECTNESS  OF COMPUTED RESULTS WITH THE  PROVIDED CODE, TOOLS,
# AND DOCUMENTATION. YOU USE THE CODE, DOCUMENTATION, AND TOOLS AT
# YOUR OWN RISK.           Removing  of  any  copyright  statement
# or  this  disclaimer  is considered  as a  attempt  to defraud.
#
# -------------------------------------------------------------------

# runfile('/home/cr/src/CISSEMBEL/util/aveclim2start.py',
# args='--input_climate ATMOS_Month00.1979.100km.fluxes.tm.nc
#    --input_initial Greenland_100km.kaxis10.nc',
# wdir='/home/cr/src/CISSEMBEL/util')

# runfile('/home/cr/src/CISSEMBEL/util/aveclim2start.py',
# args='--input_climate pism_ini.185?.nc
#   --vname_climate_airtemperature ice_surface_temp
#   --vname_climate_precipitation climatic_mass_balance
#   --vname_climate_surface_elevation topg
#   --input_initial CISSEMBEL_init.nc
#   --vname_initial_surface_elevation orog
#   --vname_initial_axis_right x
#   --vname_initial_axis_left y
#   --vname_initial_height height
#   --height_class_plus',
# wdir='/home/cr/src/CISSEMBEL/util')

# runfile('/home/cr/src/CISSEMBEL/util/aveclim2start.py',
# args='--input_climate simplegridfrompromice.nc.ATMOS_1979_2017.GrIS.ISMIP6.nc
#   --input_initial promice_initial.ATMOS_xxxx.GrIS.ISMIP6.nc
#   --output restart_time0_simplegridfrompromice.nc.ATMOS_1979_2017.GrIS.ISMIP6.nc
#   --vname_climate_airtemperature t2m
#   --vname_climate_precipitation pr
#   --vname_initial_axis_left y
#   --vname_initial_axis_right x',
# wdir='/home/cr/src/CISSEMBEL/util')

# debugfile('/home/cr/src/CISSEMBEL/util/aveclim2start.py', args='--input_climate DMI-HIRHAM5_CE2_1971_2000.tm.Greenland.xaxis_47_347_yaxis_81_519.nc --input_initial /home/cr/src/CISSEMBEL/data/HIRHAM.Greenland_5km/HIRHAM_initial.Greenland.xaxis_47_347_yaxis_81_519.nc --output output.nc --vname_climate_airtemperature tas --vname_climate_precipitation precip --vname_climate_surface_elevation orog --vname_climate_longitude lon --vname_climate_latitude lat --vname_initial_surface_elevation elevation --vname_initial_longitude lon --vname_initial_latitude lat --vname_initial_axis_right xaxis --vname_initial_axis_left yaxis --lapse_rate 0.005')



# -------------------------------------------------------------------------
def parse_arguments():
    '''
    Parse the command line arguments

    Returns
    -------
    list
        parsed command line arguments.

    '''
    parser = argparse.ArgumentParser(
        description='Creation of best guess (re)start conditions from average climate conditions',
        add_help=True)

    #
    # Input and output file names
    #
    parser.add_argument('--input_climate', '-c',
                        nargs='?',
                        required=True,
                        help='Input file name containing the forcing (climate) conditions; incl path')

    parser.add_argument('--input_initial', '-i',
                        nargs='?',
                        required=True,
                        help='Input file name containing the initial conditions; incl path')

    parser.add_argument('--output', '-o',
                        nargs='?',
                        required=False,
                        default='restart_start_time0.nc',
                        help='Output file name containing the best guess (re)start conditions; incl path')

    #
    # variable names in climate forcing file
    #
    parser.add_argument('--vname_climate_airtemperature',
                        nargs='?',
                        required=False,
                        default='temp2',
                        help='Variable name of the near-surface/2m-air temperature')

    parser.add_argument('--vname_climate_precipitation',
                        nargs='?',
                        required=False,
                        default='tprec',
                        help='Variable name of the total precipitation')

    parser.add_argument('--vname_climate_surface_elevation',
                        nargs='?',
                        required=False,
                        default='zs',
                        help='Variable name of the surface elevation in the climate file')

    parser.add_argument('--vname_climate_longitude',
                        nargs='?',
                        required=False,
                        default='lon',
                        help='Variable name of the longitude in the climate file')

    parser.add_argument('--vname_climate_latitude',
                        nargs='?',
                        required=False,
                        default='lat',
                        help='Variable name of the latitude in the climate file')

    #
    # variable names in initial file
    #
    parser.add_argument('--vname_initial_surface_elevation',
                        nargs='?',
                        required=False,
                        default='surface_elevation',
                        help='Variable name of the surface elevation in the initial file')

    parser.add_argument('--vname_initial_layer_thickness',
                        nargs='?',
                        required=False,
                        default='layer_thickness',
                        help='Variable name of the layer thickness in the initial file')

    parser.add_argument('--vname_initial_height',
                        nargs='?',
                        required=False,
                        default=None, #'height',
                        help='Variable name of the height in the initial file; Activates height class support if set')

    parser.add_argument('--vname_initial_axis_right',
                        nargs='?',
                        required=False,
                        default='iaxis',
                        help='Variable name of the right horizontal axis')

    parser.add_argument('--vname_initial_axis_left',
                        nargs='?',
                        required=False,
                        default='jaxis',
                        help='Variable name of the left horizontal axis')

    parser.add_argument('--vname_initial_longitude',
                        nargs='?',
                        required=False,
                        default='lon',
                        help='Variable name of the longitude in the initial file')

    parser.add_argument('--vname_initial_latitude',
                        nargs='?',
                        required=False,
                        default='lat',
                        help='Variable name of the latitude in the initial file')

    parser.add_argument('--dname_initial_height',
                        nargs='?',
                        required=False,
                        default='haxis',
                        help='DIMENSION name of the height axis in the initial file')

    #
    # Further arguments
    #
    parser.add_argument('--output_format',
                        nargs='?',
                        required=False,
                        default='NETCDF3_64BIT',
                        choices=['NETCDF4', 'NETCDF4_CLASSIC', 'NETCDF3_64BIT', 'NETCDF3_CLASSIC'],
                        help='Format of the netcdf output file')

    parser.add_argument('--lapse_rate', '-l',
                        type=float,
                        required=False,
                        default=0.006,
                        help='Atmospheric lapse rate (K m-1)')

    parser.add_argument('--height_class_plus', action='store_true',
                        required=False,
                        help='If set, activates height classes PLUS is; it requires "--vname_initial_height"')

    # # Report used command line arguments
    print('Used settings: read command line arguments or default values')
    for single_arg in vars(parser.parse_args()): #sorted(vars(args)) :
        print('  - {:32s}'.format(single_arg)+' = '+
              str(getattr(parser.parse_args(), single_arg)) )
    print()

    return parser.parse_args()

# -------------------------------------------------------------------------
def open_read_climate_data(filename,
                           vname_airtemp,
                           vname_precip,
                           vname_elevation,
                           vname_lon='longitude',
                           vname_lat='latitude'
                           ):
    '''
    Reads the input file

    Parameters
    ----------
    filename : str
        input file holding the surface elevation, distance in x- and y-direction.
    vname_airtemp : str
        Variable name of the near-surface/2m-air temperature field.
    vname_precip : str
        Variable name of the total precipitation field.
    vname_elevation : str
        Variable name of the elevation field.
    vname_lon : str, optional
        Variable name of the longitude field. default is 'longitude'.
    vname_lat : str, optional
        Variable name of the latitude field. The default is 'latitude'.

    Returns
    -------
    temp2m_ : numpy.ndarray
        Array of the near-surface/2m-air temperature elevation.
    precip_ : numpy.ndarray
        Array of the total precipitation elevation.
    surf_zs_ : numpy.ndarray
        Array of the surface elevation.
    lon_ : numpy.ndarray
        longitude field.
    lat_ : numpy.ndarray
        latitude field.
    # forc_file_ : xarray.core.dataset.Dataset
    #     Input data file object.
    '''

    # Open the data file
    if '?' in filename or '*' in filename:
        #
        # We may open many files with various time records/time steps
        #
        print('Open input climate files "'+filename+'"')
        forc_file_ = xr.open_mfdataset(filename,
                                       drop_variables=['rotated_pole', 'Other_Bad_Var'],
                                       decode_times=False).mean(dim='time', keepdims=True)
    else:
        #
        # We open one file that may contain many time records/time steps
        #
        print('Open input climate file  "'+filename+'"')
        forc_file_ = xr.open_dataset(filename,
                                     drop_variables=['rotated_pole', 'Other_Bad_Var'],
                                     decode_times=False).mean(dim='time', keepdims=True)


    #  Accessing data fields
    temp2m_ = forc_file_[vname_airtemp]  #.squeeze()
    precip_ = forc_file_[vname_precip]

    # Reduce to one time step and strip off 'time' dimension
    if 'time' in temp2m_.dims:
        temp2m_ = temp2m_.mean(dim='time')
    if 'time' in precip_.dims:
        precip_ = precip_.mean(dim='time')

    surf_zs_ = forc_file_[vname_elevation]  #.squeeze()
    # Reduce to one time step and strip off 'time' dimension
    if 'time' in surf_zs_.dims:
        surf_zs_ = surf_zs_.mean(dim='time')

    if vname_lon is not None and vname_lat is not None:
        lon_ = forc_file_[vname_lon]
        lat_ = forc_file_[vname_lat]
    else:
        lon_ = None
        lat_ = None


    return temp2m_, precip_, surf_zs_, lon_, lat_, forc_file_

# -------------------------------------------------------------------------
def open_read_initial_data(filename,
                           vname_elevation,
                           vname_thick,
                           vname_height=None,
                           vname_raxis=None,
                           vname_laxis=None,
                           vname_lon=None,
                           vname_lat=None
                           ):
    '''
    Reads the input file

    Parameters
    ----------
    filename : str
        input file holding the surface elevation, distance in x- and y-direction.
    vname_elevation : str
        Variable name of the elevation field.
    vname_thick : str
        Variable name of the layer thickness array.
    vname_height : str
        Variable name of the height array. default is None.
    vname_raxis : str
        Variable name of the right horizontal axis array. default is None.
    vname_laxis : str
        Variable name of the left horizontal axis array. default is None.
    vname_lon : str, optional
        Variable name of the longitude field. default is 'longitude'.
    vname_lat : str, optional
        Variable name of the latitude field. The default is 'latitude'.

    Returns
    -------
    temp2m_ : numpy.ndarray
        Array of the near-surface/2m-air temperature elevation.
    precip_ : numpy.ndarray
        Array of the total precipitation elevation.
    surf_zs_ : numpy.ndarray
        Array of the surface elevation.
    lon_ : numpy.ndarray
        longitude field.
    lat_ : numpy.ndarray
        latitude field.
    # initialfile_ : xarray.core.dataset.Dataset
    #     Input data file object.
    '''

    # Open the data file
    print('Open input initial file  "'+filename+'"')

    initialfile_ = xr.open_dataset(filename,
                                   decode_times=False,
                                   drop_variables=['bheatflux',
                                                   'bheatflx',
                                                   'Basins',
                                                   'basins',
                                                   'BasinsINTExtended',
                                                   'depth',
                                                   'landcover',
                                                   'station_id',
                                                   'station_mask'
                                                   ])

    #  Accessing data fields

    surf_zs_ = initialfile_[vname_elevation]  #.squeeze()
    thick_ = initialfile_[vname_thick]

    # Reduce to one time step and strip off 'time' dimension
    if 'time' in surf_zs_.dims:
        surf_zs_ = surf_zs_.mean(dim='time')
    if 'time' in thick_.dims:
        thick_ = thick_.mean(dim='time')

    if vname_height is not None:
        height_ = initialfile_[vname_height].squeeze()
        # Reduce to one time step and strip off 'time' dimension
        if 'time' in height_.dims:
            height_ = height_.mean(dim='time')
    else:
        height_ = None


    if vname_raxis is not None and vname_laxis is not None:
        ax_r_ = initialfile_[vname_raxis].squeeze()
        ax_l_ = initialfile_[vname_laxis].squeeze()
    else:
        ax_r_ = None
        ax_l_ = None


    if vname_lon is not None and vname_lat is not None:
        lon_ = initialfile_[vname_lon]
        lat_ = initialfile_[vname_lat]
    else:
        lon_ = None
        lat_ = None


    return surf_zs_, thick_, height_, ax_r_, ax_l_, lon_, lat_, initialfile_


# -------------------------------------------------------------------------
# -------------------------------------------------------------------------
def thickness2depth(thick):
    """
    Converge layer thickness into (centered) layer depth

    Parameters
    ----------
    thick : numpy.ndarray
        Array of layer thickness.

    Returns
    -------
    depth_ : numpy.ndarray
        Array of layer depths.

    """
    depth_ = np.zeros_like(thick)
    depth_[0] = thick[0]*0.5
    for ic_ in range(1, thick.size):
        depth_[ic_] = depth_[ic_-1]+0.5*(thick[ic_-1]+thick[ic_])

    return depth_

# -------------------------------------------------------------------------
def mtime_func(temp, temp_melt_, dn_tax, dn_jax, dn_iax, vn_lon, vn_lat, dn_hax=None):
    """
    Melting time: Elapsed time since last melt event

    Parameters
    ----------
    temp : numpy.ndarray
        Field of near-surface/2m-air temperature.
    temp_melt_ : float
        Melting point temperature.
    dn_tax : str
        Name of the time dimension/axis.
    dn_jax : str
        Name of the left horizontal dimension/axis.
    dn_iax : str
        Name of the right horizontal dimension/axis.
    vn_lon : str
        Variable name of the longitude field.
    vn_lat : str
        Variable name of the latitude field.
    dn_hax : str, optional
        Name of the height class dimension/axis. The default is None

    Returns
    -------
    mtime_ : numpy.ndarray
        Elapsed time since last melt event.

    """
    if dn_hax is not None:
        dims=[dn_tax, dn_hax, dn_jax, dn_iax]
    else:
        dims=[dn_tax, dn_jax, dn_iax]

    mtime_ = np.where(temp > temp_melt_-0.25, 0.,  SECPERDAY*10.)
    mtime_ = xr.DataArray(np.expand_dims(mtime_, axis=0),
                          dims=dims,
                          attrs={'long_name': 'time elapsed since last melt event',
                                 'units': 'seconds',
                                 ATTRS_COORDINATES: vn_lon+' '+vn_lat,
                                 'valid_min': 0.0,})
    return mtime_

# -------------------------------------------------------------------------
def stime_func(temp, temp_melt_, dn_tax, dn_jax, dn_iax, vn_lon, vn_lat, dn_hax=None):
    """
    Snow time: Elapsed time since last snowfall event

    Parameters
    ----------
    temp : numpy.ndarray
        Field of near-surface/2m-air temperature.
    temp_melt_ : float
        Melting point temperature.
    dn_tax : str
        Name of the time dimension/axis.
    dn_jax : str
        Name of the left horizontal dimension/axis.
    dn_iax : str
        Name of the right horizontal dimension/axis.
    vn_lon : str
        Variable name of the longitude field.
    vn_lat : str
        Variable name of the latitude field.
    dn_hax : str, optional
        Name of the height class dimension/axis. The default is None

    Returns
    -------
    stime_ : numpy.ndarray
        Elapsed time since last snowfall event.

    """
    if dn_hax is not None:
        dims=[dn_tax, dn_hax, dn_jax, dn_iax]
    else:
        dims=[dn_tax, dn_jax, dn_iax]

    stime_ = np.where(temp > temp_melt_-0.25, SECPERDAY*25.,  SECPERDAY*10.)
    #stime = xr.DataArray(np.zeros(dim2t_shape) + SECPERDAY, #  Snow has fallen 86400. (=1day) ago.
    stime_ = xr.DataArray(np.expand_dims(stime_, axis=0),
                         dims=dims,
                         attrs={'long_name': 'time elapsed since last snowfall',
                                'units': 'seconds',
                                ATTRS_COORDINATES: vn_lon+' '+vn_lat,
                                'valid_min': 0.0,})
    return stime_

# -------------------------------------------------------------------------
def xtime_func(dim2t, dn_tax, dn_jax, dn_iax, vn_lon, vn_lat, dn_hax=None):
    """
    Extra timer: Elapsed time since extra event

    Parameters
    ----------
    dim2t : list of numpy.ndarray
        Shape of the (horizontal) extra time field including the time axis.
    temp_melt_ : float
        Melting point temperature.
    dn_tax : str
        Name of the time dimension/axis.
    dn_jax : str
        Name of the left horizontal dimension/axis.
    dn_iax : str
        Name of the right horizontal dimension/axis.
    vn_lon : str
        Variable name of the longitude field.
    vn_lat : str
        Variable name of the latitude field.
    dn_hax : str, optional
        Name of the height class dimension/axis. The default is None

    Returns
    -------
    xtime_ : numpy.ndarray
        Elapsed time since last extra event.

    """
    if dn_hax is not None:
        dims=[dn_tax, dn_hax, dn_jax, dn_iax]
        zeros_ = np.zeros(dim2t)
    else:
        dims=[dn_tax, dn_jax, dn_iax]
        zeros_ = np.zeros(dim2t)

    xtime_ = xr.DataArray(zeros_,
                          dims=dims,
                          attrs={'long_name': 'extra event time',
                                 'units': 'seconds',
                                 ATTRS_COORDINATES: vn_lon+' '+vn_lat,
                                 'valid_min': 0.0,})
    return xtime_

# -------------------------------------------------------------------------
def snowdepth_func(temp, temp_melt_, snowd_thres,
                   dn_tax, dn_jax, dn_iax, vn_lon, vn_lat, dn_hax=None):
    """
    Melting time: Elapsed time since last melt event

    Parameters
    ----------
    temp : numpy.ndarray
        Field of near-surface/2m-air temperature.
    temp_melt_ : float
        Melting point temperature.
    snowd_thres : float
        Snow depth threshold.
    dn_tax : str
        Name of the time dimension/axis.
    dn_jax : str
        Name of the left horizontal dimension/axis.
    dn_iax : str
        Name of the right horizontal dimension/axis.
    vn_lon : str
        Variable name of the longitude field.
    vn_lat : str
        Variable name of the latitude field.
    dn_hax : str, optional
        Name of the height class dimension/axis. The default is None

    Returns
    -------
    mtime_ : numpy.ndarray
        Elapsed time since last melt event.

    """
    if dn_hax is not None:
        dims=[dn_tax, dn_hax, dn_jax, dn_iax]
    else:
        dims=[dn_tax, dn_jax, dn_iax]

    # Snow depth, where annual mean is less than TEMPERATURE_MELT we have
    # snow. With decreasing temperature we have more snow but

    snowd_ = np.maximum((temp_melt_-temp)/5., 0.)
    snowd_ = np.minimum( snowd_, snowd_thres)
    snowd_ = xr.DataArray(np.expand_dims(snowd_, axis=0),
                          dims=dims,
                          attrs={'long_name': 'snow depth',
                                 'units': 'm',
                                 ATTRS_COORDINATES: vn_lon+' '+vn_lat,
                                 'valid_min': 0.0,})
    return snowd_

# -------------------------------------------------------------------------
def snowts_func(temp, temp_melt_, dn_tax, dn_jax, dn_iax, vn_lon, vn_lat, dn_hax=None):
    """
    Temporal integrated temperature above melting

    Parameters
    ----------
    temp : numpy.ndarray
        Field of near-surface/2m-air temperature.
    temp_melt_ : float
        Melting point temperature.
    dn_tax : str
        Name of the time dimension/axis.
    dn_jax : str
        Name of the left horizontal dimension/axis.
    dn_iax : str
        Name of the right horizontal dimension/axis.
    vn_lon : str
        Variable name of the longitude field.
    vn_lat : str
        Variable name of the latitude field.
    dn_hax : str, optional
        Name of the height class dimension/axis. The default is None

    Returns
    -------
    snowts_ : numpy.ndarray
        Temporal integrated temperature above melting.

    """
    if dn_hax is not None:
        dims=[dn_tax, dn_hax, dn_jax, dn_iax]
    else:
        dims=[dn_tax, dn_jax, dn_iax]

    snowts_ = np.maximum((temp-temp_melt_-0.01)*SECPERDAY*10., 0.0)
    snowts_ = xr.DataArray(np.expand_dims(snowts_, axis=0),
                          dims=dims,
                          attrs={'long_name':
                                 'integrated temperature time above threshold='+str(temp_melt_),
                                 'units': 'degC second',
                                 ATTRS_COORDINATES:vn_lon+' '+vn_lat,
                                 'valid_min': 0.0,})
    return snowts_

# -------------------------------------------------------------------------
def meltmask_func(mtime_, dn_tax, dn_jax, dn_iax, vn_lon, vn_lat, dn_hax=None):
    """
    Diagnostic_ field indicating the melting of snow, ice or both.

    Parameters
    ----------
    mtime_ : numpy.ndarray
        Elapsed time since last melt event.
    dn_tax : str
        Name of the time dimension/axis.
    dn_jax : str
        Name of the left horizontal dimension/axis.
    dn_iax : str
        Name of the right horizontal dimension/axis.
    vn_lon : str
        Variable name of the longitude field.
    vn_lat : str
        Variable name of the latitude field.
    dn_hax : str, optional
        Name of the height class dimension/axis. The default is None

    Returns
    -------
    meltmsk_ : numpy.ndarray
        Melting mask.

    """
    if dn_hax is not None:
        dims=[dn_tax, dn_hax, dn_jax, dn_iax]
    else:
        dims=[dn_tax, dn_jax, dn_iax]

    meltmsk_ = np.array(np.where(mtime_ <= 0, 1, 0), dtype=int)
    meltmsk_ = xr.DataArray(meltmsk_,
                            dims=dims,
                            attrs={'long_name': 'mask of melting events',
                                   ATTRS_COORDINATES:vn_lon+' '+vn_lat})
    return meltmsk_

# -------------------------------------------------------------------------
# -------------------------------------------------------------------------
def snowtemp_func(airtemp_, temp_melt_, dim3_,
                  dn_tax, dn_kax, dn_jax, dn_iax, vn_lon, vn_lat, dn_hax=None):
    """
    Snow temperature distribution is set to the provided air long-term temperature.

    Parameters
    ----------
    airtemp_ : numpy.ndarray
        Annual or long-term near-surface/2m-air temperature.
    temp_melt_ : float
        Melting point temperature.
    dim3_ : list of numpy.ndarray
        Shape of the (2dim) snow temperature field including the time axis.
    dn_tax : str
        Name of the time dimension/axis.
    dn_kax : str
        Name of the depth dimension/axis.
    dn_jax : str
        Name of the left horizontal dimension/axis.
    dn_iax : str
        Name of the right horizontal dimension/axis.
    vn_lon : str
        Variable name of the longitude field.
    vn_lat : str
        Variable name of the latitude field.
    dn_hax : str, optional
        Name of the height class dimension/axis. The default is None

    Returns
    -------
    snowtemp_ : numpy.ndarray
        Snow temperature distribution.

    """
    if dn_hax is not None:
        dims=[dn_tax, dn_hax, dn_kax, dn_jax, dn_iax]
        snowtemp_ = np.zeros(dim3_)+273.0
        for k_ in range(dim3_[2]):
            snowtemp_[0:, :, k_, ::] = np.minimum(airtemp_, temp_melt_)
    else:
        dims=[dn_tax, dn_kax, dn_jax, dn_iax]
        snowtemp_ = np.zeros(dim3_)+273.0
        snowtemp_[0:,::] = np.minimum(airtemp_, temp_melt_)

    snowtemp_ = xr.DataArray(snowtemp_,
                             dims=dims,
                             attrs={'long_name': 'snow temperature',
                                    'units': 'K',
                                    ATTRS_COORDINATES: vn_lon+' '+vn_lat,
                                    'valid_max': temp_melt_},)
    return snowtemp_

# -------------------------------------------------------------------------
def rho_func(airtemp_, temp_melt_, snowtemp_, depth_,
             dn_tax, dn_kax, dn_jax, dn_iax, vn_lon, vn_lat, dn_hax=None,
             density_snow_=330., density_ice_=910.0, c_dens_=0.024):
    """
    Snow density distribution.

    Parameters
    ----------
    airtemp_ : numpy.ndarray
        Field of near-surface/2m-air temperature.
    temp_melt_ : float
        Melting point temperature.
    snowtemp_ : list of numpy.ndarray
        Snow temperature distribution.
    depth_ : numpy.ndarray
        Array of layer depth.
    dn_tax : str
        Name of the time dimension/axis.
    dn_kax : str
        Name of the depth dimension/axis.
    dn_jax : str
        Name of the left horizontal dimension/axis.
    dn_iax : str
        Name of the right horizontal dimension/axis.
    vn_lon : str
        Variable name of the longitude field.
    vn_lat : str
        Variable name of the latitude field.
    dn_hax : str, optional
        Name of the height class dimension/axis. The default is None
    density_snow_ : float
        Density of snow (minimum in profile). default is 330.0.
    density_ice_ : float
        Density of ice (maximum in profile). default is 910.0.
    c_dens_ : float
        C-value determining the exponential form of the density profile. The default is 0.024.


    Returns
    -------
    rho_ : numpy.ndarray
        Snow density distribution.

    """
    # One depth profile against depth
    rho_profile = xr.DataArray(depth2rho(depth_,
                                         density_snow_,
                                         density_ice_,
                                         c_dens_),
                               dims=[dn_kax])

    attrs = {'long_name': 'snow density',
             'units': 'kg m-3',
             ATTRS_COORDINATES: vn_lon+' '+vn_lat,
             'valid_range': [density_snow_, density_ice_]}

    # Define the field as DataArray (same size as the snow temperature)
    if dn_hax is not None:
        dims=[dn_tax, dn_hax, dn_kax, dn_jax, dn_iax]
    else:
        dims=[dn_tax, dn_kax, dn_jax, dn_iax]


    rho_ = xr.DataArray(np.zeros(snowtemp_.shape),
                        dims=dims,
                        attrs=attrs)

    # Add the profile to each (horizontal) grid location
    rho_ = rho_+rho_profile

    #
    # Modify/Increase the density in regions of high (long-term) air
    # temperature to reflect the existance of ice.
    #
    # -- Increase density where the snow temperature is at melting point
    threshold_fraction_ice_dens=0.95
    rho_0 = np.where(snowtemp_.data >= temp_melt_,
                   rho_*np.minimum(1+(snowtemp_.data-temp_melt_)*0.5,
                                  float(threshold_fraction_ice_dens
                                        *density_ice_/rho_profile.max())),
                   rho_)

    # -- At warm locations (Tair > Tmelt+5.0), the lowest layer has the density of ice
    if dn_hax is not None:
        rho_0[0, :, -1, ::] = np.where(airtemp_ > temp_melt_+5.0,
                                    density_ice_,
                                    rho_0[0, :, -1, ::])
    else:
        rho_0[0, -1, ::] = np.where(airtemp_ > temp_melt_+5.0,
                                    density_ice_,
                                    rho_0[0, -1, ::])
    rho_0 = np.minimum(rho_0, density_ice_)

    #
    # Replace the density data field by the additional constrains
    #
    rho_.data = rho_0
    rho_.attrs = attrs

    del rho_0

    return rho_


# -------------------------------------------------------------------------
def siw_cont_func(thick_, frac_snow_, frac_ice_, frac_water_,
                  dn_tax, dn_kax, dn_jax, dn_iax, vn_lon, vn_lat,
                  dn_hax=None,
                  snow_dens_=330., ice_dens_=910.0, water_dens_=1000.0):
    """
    Snow, ice, and water conent

    Parameters
    ----------
    thick_ : numpy.ndarray
        Array of layer thickness.
    frac_snow_ : float
        Fraction of snow in layers.
    frac_ice_ : float
        Fraction of ice in layers.
    frac_water_ : TYPE
        Fraction of water in layers.
    dn_tax : str
        Name of the time dimension/axis.
    dn_kax : str
        Name of the depth dimension/axis.
    dn_jax : str
        Name of the left horizontal dimension/axis.
    dn_iax : str
        Name of the right horizontal dimension/axis.
    vn_lon : str
        Variable name of the longitude field.
    vn_lat : str
        Variable name of the latitude field.
    dn_hax : str, optional
        Name of the height class dimension/axis. The default is None.
    snow_dens_ : numpy.ndarray, optional
        Array of snow densities. The default is 330.0.
    ice_dens_ : float, optional
        Density of ice. The default is 910.0.
    water_dens_ : float, optional
        Density of water. The default is 1000.0.

    Returns
    -------
    scont_ : numpy.ndarray
        Snow content field.
    icont_ : numpy.ndarray
        Ice content field.
    wcont_ : numpy.ndarray
        Water content field.

    """
    dens_aux0 = snow_dens_*0 # auxillary field

    #
    # Ensure that the sum of all fractions equals ONE.
    #
    frac_total_ = frac_snow_+frac_ice_+frac_water_
    factor = 1.0/frac_total_
    frac_snow_ = factor*frac_snow_
    frac_ice_ = factor*frac_ice_
    frac_water_ = factor*frac_water_

    if dn_hax is not None:
        dims=[dn_tax, dn_hax, dn_kax, dn_jax, dn_iax]
    else:
        dims=[dn_tax, dn_kax, dn_jax, dn_iax]


    scont_ = xr.DataArray(snow_dens_*thick_*frac_snow_,
                          dims=dims,
                          attrs={'long_name': 'snow content',
                                 'units': 'kg m-2',
                                 ATTRS_COORDINATES: vn_lon+' '+vn_lat,
                                 'valid_min': 0.0})

    icont_ = xr.DataArray(dens_aux0+thick_*(ice_dens_*frac_ice_),
                      dims=dims,
                      attrs={'long_name': 'ice content',
                             'units': 'kg m-2',
                             ATTRS_COORDINATES: vn_lon+' '+vn_lat,
                             'valid_min': 0.0})

    wcont_ = xr.DataArray(dens_aux0+thick_*(water_dens_*frac_water_),
                  dims=dims,
                  attrs={'long_name': 'water content',
                         'units': 'kg m-2',
                         ATTRS_COORDINATES: vn_lon+' '+vn_lat,
                         'valid_min': 0.0})
    del dens_aux0

    return scont_, icont_, wcont_

# -------------------------------------------------------------------------
def age_func(tprecip_, depth_, dn_tax, dn_kax, dn_jax, dn_iax, vn_lon, vn_lat,
             dn_hax=None,
             water_dens_=1000.0, time_damping_=1.0,
             threshold_height_per_sec=1.0e-12):
    """
    Computes the initial age as a function of the precipitation

    Parameters
    ----------
    tprecip_ : numpy.ndarry
        Total precipitation field.
    depth_ : numpy.ndarry
        Array of the layer depth.
    dn_tax : str
        Name of the time dimension/axis.
    dn_kax : str
        Name of the depth dimension/axis.
    dn_jax : str
        Name of the left horizontal dimension/axis.
    dn_iax : str
        Name of the right horizontal dimension/axis.
    vn_lon : str
        Variable name of the longitude field.
    vn_lat : str
        Variable name of the latitude field.
    dn_hax : str, optional
        Name of the height class dimension/axis. The default is None
    water_dens_ : float, optional
        Density of water. The default is 1000.0.
    time_damping_ : float, optional
        Factor used to increase the time. The default is 1.0.
    threshold_height_per_sec : float, optional
        Lowest precipitaition driven height changes; lower values equal this
        threshold. The default is 1.0e-12.

    Returns
    -------
    age_ : numpy.ndarray
        Estimated age.

    """
    if dn_hax is not None:
        dims=[dn_tax, dn_hax, dn_kax, dn_jax, dn_iax]
    else:
        dims=[dn_tax, dn_kax, dn_jax, dn_iax]

    #
    # Convert precipitation flux into a temporal height change
    # -- Ensure positive precipitation
    #
    precip_height_per_sec = xr.DataArray(abs(tprecip_.data)*(1.0/water_dens_),
                                         dims=[dn_jax, dn_iax])

    #
    # To prevent a division by zero or tinest values, we consider a threshold.
    # A `precip_height_per_sec = 1e-12` equals 333 miro meter of water
    # equivalent per year aka 1 mm of fresh snow per year
    #
    precip_height_per_sec.data = np.maximum(precip_height_per_sec.data,
                                            threshold_height_per_sec)

    age_ = ((depth_-depth_[0])/precip_height_per_sec)*time_damping_
    age_ = np.expand_dims(age_, axis=0)
    if dn_hax is not None:
        age_ = np.repeat(np.expand_dims(age_, axis=1),
                         height.size,
                         axis=1)

    age_ = xr.DataArray(age_, dims=dims,
                        attrs={'long_name': 'elapased time since snow has left the top layer',
                               'standard_name': 'age_of_subsurface_snow',
                               'units': 's',
                               ATTRS_COORDINATES: vn_lon+' '+vn_lat})
    return age_

# -------------------------------------------------------------------------
def graind_func(tprecip_, scont_, wcont_, depth_,
                dn_tax, dn_kax, dn_jax, dn_iax, vn_lon, vn_lat, dn_hax=None,
                grain_initial_=5.0e-5, grain_max_=0.05,
                water_dens_=1000.0, time_damping_=1.0,
                threshold_height_per_sec=1.0e-12):

    if dn_hax is not None:
        dims=[dn_tax, dn_hax, dn_kax, dn_jax, dn_iax]
        graind_ = np.zeros(dim3th_shape)+grain_initial_
    else:
        dims=[dn_tax, dn_kax, dn_jax, dn_iax]
        graind_ = np.zeros(dim3t_shape)+grain_initial_

    #
    # Convert precipitation flux into a temporal height change
    # -- Ensure positive precipitation
    #
    precip_height_per_sec = xr.DataArray(abs(tprecip_.data)*(1.0/water_dens_),
                                         dims=[dn_jax, dn_iax])
    #
    # -- Prevent division by zero or tiniest values, we consider a lower limit.
    #
    precip_height_per_sec.data = np.maximum(precip_height_per_sec.data,
                                            threshold_height_per_sec)
    #
    # Height changes as transfer time with depth
    #
    # time_precip2depth = depth_/precip_height_per_sec
    time_precip2depth = (depth_-depth_[0])/precip_height_per_sec
    time_precip2depth =  time_precip2depth*time_damping_

    # Add dim for time dimension
    time_precip2depth = np.expand_dims(time_precip2depth, axis=0)

    #
    # Grain size diameter
    #

    graind_ = xr.DataArray(graind_,
                           dims=dims,
                           attrs={'long_name': 'grain size diameter',
                                  'units': 'm',
                                  ATTRS_COORDINATES: vn_lon+' '+vn_lat})

    graind_.data = grain_growth(time_precip2depth,
                                graind_.data,
                                scont_.data,
                                wcont_.data,
                                grain_max_)

    return graind_

# -------------------------------------------------------------------------
# -------------------------------------------------------------------------
def depth2rho(depth_, ref_snow_dens=330., ref_ice_dens=910., ref_c_dens=0.024, dn_hax=None):
    """
    Snow density profile from depth

    Parameters
    ----------
    depth_ : numpy.ndarray
        Array of layer depth.
    ref_snow_dens : float, optional
        Density of snow. The default is 330..
    ref_ice_dens : float, optional
        Density of ice. The default is 910..
    ref_c_dens : float, optional
        C-value determining the exponential form of the density profile. The default is 0.024.

    Returns
    -------
    density_ : numpy.ndarray
        Density profile.

    """
    density_ = ref_ice_dens - (ref_ice_dens-ref_snow_dens) \
            *np.exp(-ref_c_dens*depth_)

    return density_



def grain_growth(dtime, graind_, scont_, wcont_, graind_upper_threshold = 0.05, dn_hax=None):
    """
    Grain size diameter growth

    Parameters
    ----------
    dtime : float, numpy.ndarray
        Time step.
    graind_ : numpy.ndarray
        Initial grain size diameter.
    scont_ : numpy.ndarray
        Snow content in layer.
    wcont_ : numpy.ndarray
        Water content in layer.
    graind_upper_threshold : float, optional
        Upper threshold of grain size. The default is 0.05.

    Returns
    -------
    grain_growth_ : numpy.ndarray
        Updated grain size diameter after the time step.

    """
    grain_growth_ = graind_+dtime*dgrain_dt(graind_, scont_, wcont_)
    grain_growth_ = np.minimum(grain_growth_, graind_upper_threshold)

    return grain_growth_


def dgrain_dt(graind_, scont_, wcont_):
    """
    Equation determing the grain size diameter growth velocity

    Parameters
    ----------
    graind_ : numpy.ndarray
        Initial grain size diameter.
    scont_ : numpy.ndarray
        Snow content in layer.
    wcont_ : numpy.ndarray
        Water content in layer.

    Returns
    -------
    dgrain_dt_ : numpy.ndarray
        Grain size diameter growth velocity.

    """

    tiny_real = 1.0e-6
    liquid_percent = np.where(scont_>tiny_real,
                              100.0*wcont_/(scont_+tiny_real),
                              0.0)

    #tiny_real = 1.0e-12
    #liquid_percent = 100.0*wcont_/(scont_+tiny_real)
    # # To avoid numerical underflow/underrun below in `brun = ..`
    # liquid_percent = max(liquid_percent, tiny_real)

    GRAIND_INTERMEDIATE_MILLIMETER = True

    if GRAIND_INTERMEDIATE_MILLIMETER:
        # UNITS: mm, mm s-1, mm-3 s-1
        brun = (2.0/np.pi)*(1.28e-8+4.22e-10*np.power(liquid_percent, 3.0)) # Unit: mm3 s-1
        tusima = 6.94e-8 # =2.4e-4/3600, Unit: mm3 s-1 ; equation (17) in Katsushima (2009).
        dgrain_dt_ = 1.0e-9*np.minimum(brun, tusima)/np.square(graind_)
    else:
        # UNITS: Meter = m, m s-1, m-3 s-1
        brun = (2.0/np.pi)*(1.28e-17+4.22e-19*np.power(liquid_percent, 3.0)) # Unit: m3 s-1
        tusima = 6.94e-17  # Unit: m3 s-1
        dgrain_dt_ = np.minimum(brun, tusima)/np.square(graind_)
    return dgrain_dt_


# -------------------------------------------------------------------------
#
# Main program
#
if __name__ == '__main__':
    import argparse           # argparse : Parse command line arguments
    import numpy as np        # Numpy    : Array handling
    # import pandas as pd       # Pandas   :
    import xarray as xr       # Xarrray  : Easy handling of netcdf files
    from datetime import  datetime # Current date/time
    # from pyproj import Proj   # PyProj   : Transformation between coordinates
    # from pathlib import Path
    import os


    print('Start    the job ('+str(datetime.now())[:19]+')')
    # Parse command arguments
    parsed_argv = parse_arguments()

    #
    # Reference variable names
    #
    ATTRS_COORDINATES = 'coordinates'
    VNAME_BASAL_HEAT_FLUX_MAP_REF = None
    VNAME_SURFACE_ELEVATION_REF = 'surface_elevation'
    VNAME_SURFACE_SLOPE_REF = 'surface_slope'
    # VNAME_BASINS_REF = 'Basins'
    # VNAME_STATION_MASK = 'station_mask'
    # VNAME_STATION_ID = 'station_id'

    #
    # Dimension names
    #
    DNAME_IAX = 'iaxis'
    DNAME_JAX = 'jaxis'
    DNAME_KAX = 'kaxis'
    DNAME_HAX = 'haxis'
    DNAME_SAX = 'saxis'
    DNAME_CAX = 'nv'
    DNAME_TBX = 'tbounds'
    DNAME_TAX = 'time'

    #
    # Parameter
    #
    TEMPERATURE_MELT = 273.15
    SNOW_THICKNESS_THRESHOLD = 2.0

    GRAIN_SIZE_INITIAL = 100.0*1.0e-6 #  0.0001 # 100 um in Meter
    GRAIN_SIZE_INITIAL =  50.0*1.0e-6 #  0.0001 #  50 um in Meter
    # GRAIN_SIZE_MAX_THRESHOLD = 2000.0*1.0e-6 #
    GRAIN_SIZE_MAX_THRESHOLD = 1300.0*1.0e-6 # 1.3 mm in Meter

    #GRAIN_SIZE_MAX_THRESHOLD = 0.015   # Meter
    #GRAIN_SIZE_MAX_THRESHOLD = 0.05    # Meter

    REFERENCE_SNOW_DENSITY=330.
    REFERENCE_ICE_DENSITY=910.
    REFERENCE_WATER_DENSITY=1000.
    REFERENCE_CDENSITY=0.024

    SECPERDAY = 86400. # seconds per day
    SECPERYEAR = SECPERDAY*365.25 # seconds per year

    #
    # Read the necassary data fields
    #
    # -- From the climate forcing file
    #
    temperature, precipitation, surface_elevation_climate, lon, lat, force_data = \
        open_read_climate_data(parsed_argv.input_climate,
                               parsed_argv.vname_climate_airtemperature,
                               parsed_argv.vname_climate_precipitation,
                               parsed_argv.vname_climate_surface_elevation,
                               parsed_argv.vname_climate_longitude,
                               parsed_argv.vname_climate_latitude
                               )
    #
    # -- From the initial condition file
    #
    surface_elevation_initial, layer_thickness, height, xaxis, yaxis, \
        lon_ini, lat_ini, init_data = \
        open_read_initial_data(parsed_argv.input_initial,
                               parsed_argv.vname_initial_surface_elevation,
                               parsed_argv.vname_initial_layer_thickness,
                               parsed_argv.vname_initial_height,
                               parsed_argv.vname_initial_axis_right,
                               parsed_argv.vname_initial_axis_left,
                               )

    #
    # Simple test if the fields from both input files have the same shape
    #
    if surface_elevation_climate.shape == surface_elevation_initial.shape:
        print(' The surface elevation in both input files have the same shape of'+
              str(surface_elevation_climate.shape))
    else:
        print('**************************************************************')
        print('****  Shape of the elevation in both input files differ  *****')
        print('****  Climate data has an elevation shape of '+
              str(surface_elevation_climate.shape)+'  *****')
        print('****  Initial data has an elevation shape of '+
              str(surface_elevation_initial.shape)+'  *****')
        print('**************************************************************')
        raise SystemExit(0)
        exit()
        # sys.exit('Error, field sizes differ')


    #
    # Axis for output file
    #
    # from right to left
    i_axis = list(range(surface_elevation_initial.shape[-1]))
    j_axis = list(range(surface_elevation_initial.shape[-2]))
    k_axis = list(range(layer_thickness.size))
    if parsed_argv.vname_initial_height is not None:
        if parsed_argv.height_class_plus:
            # Remark: The index '0' (zero) indicates in Fortran code CISSEMBEL
            #         that we have the height_class_plus case
            h_axis = list(range(0, height.size+1))
        else:
            # Remark: The standard height classes indexes in the Fortran code
            #         of CISSEMBEL starts at '1' (one).
            h_axis = list(range(1, height.size+1))

    s_axis = [1, 2, 3, 4, 5, 6, 7]
    c_axis = [1, 2, 3, 4]
    t_bounds_axis = np.array([0, 1], dtype=int)
    t_axis = np.array([0], dtype=float)

    #
    # Copy initial data to restart data
    #
    # - rename dimensions
    #
    restart_data_tmp = init_data.copy()

    if DNAME_IAX not in restart_data_tmp.dims:
        restart_data = restart_data_tmp.copy(). \
            rename_dims({parsed_argv.vname_initial_axis_right: DNAME_IAX})
        restart_data_tmp = restart_data.copy()

    if DNAME_JAX not in restart_data_tmp.dims:
        restart_data = restart_data_tmp.copy(). \
            rename_dims({parsed_argv.vname_initial_axis_left: DNAME_JAX})
        restart_data_tmp = restart_data.copy()

    #
    # - rename variables
    #
    if VNAME_SURFACE_ELEVATION_REF not in restart_data_tmp.var():
        restart_data = restart_data_tmp.copy(). \
            rename({parsed_argv.vname_initial_surface_elevation:
                    VNAME_SURFACE_ELEVATION_REF})
        restart_data_tmp = restart_data.copy()

    if parsed_argv.vname_initial_axis_right not in restart_data_tmp.var():
        restart_data = restart_data_tmp.set_index(iaxis=parsed_argv.vname_initial_axis_right).copy()
        restart_data_tmp = restart_data.copy()
        restart_data_tmp['iaxis'].attrs = {'long_name': 'i-index of mesh grid',
                                           'standard_name': 'projection_x_coordinate',
                                           'axis': 'X'}

    if parsed_argv.vname_initial_axis_left not in restart_data_tmp.var():
        restart_data = restart_data_tmp.set_index(jaxis=parsed_argv.vname_initial_axis_left).copy()
        restart_data_tmp = restart_data.copy()
        restart_data_tmp['jaxis'].attrs = {'long_name': 'j-index of mesh grid',
                                           'standard_name': 'projection_y_coordinate',
                                           'axis': 'Y'}

    #
    # - add dimensions
    #
    iaxis = xr.DataArray(i_axis, coords=[(DNAME_IAX, i_axis)],
                         attrs={'long_name': 'i-index of mesh grid',
                                'standard_name': 'projection_x_coordinate',
                                'axis': 'X'})
    jaxis = xr.DataArray(j_axis, coords=[(DNAME_JAX, j_axis)],
                         attrs={'long_name': 'j-index of mesh grid',
                                'standard_name': 'projection_y_coordinate',
                                'axis': 'Y'})
    kaxis = xr.DataArray(k_axis, coords=[(DNAME_KAX, k_axis)],
                         attrs={'long_name': 'z-layer of mesh grid',
                                'units': 'layer',
                                'axis': 'Z',
                                'coordinates': 'depth',
                                'positive': 'down',
                                'bounds': 'time_bounds'})
    if parsed_argv.vname_initial_height is not None:
        haxis = xr.DataArray(h_axis, coords=[(DNAME_HAX, h_axis)],
                             attrs={'long_name': 'height level',
                                    'coordinates': parsed_argv.vname_initial_height,
                                    'kind': 'axis along the height levels'})
        if parsed_argv.height_class_plus:
            # To expand the haxis and the corresponding height array, we have
            # to rename the corresponding dimension and variable in the file.
            # Afterward, we create a new height axis and add the above expanded
            # height dimension and the height variable to the output/restart
            # file. Afterward, we can savely delete original/rename dimension
            # and variable.

            dname_new = parsed_argv.dname_initial_height+'_original'
            vname_new = parsed_argv.vname_initial_height+'_original'

            restart_data = restart_data_tmp.copy(). \
                rename({parsed_argv.vname_initial_height: vname_new}). \
                    rename_dims({parsed_argv.dname_initial_height: dname_new})

            height2 = [hh for hh in height.data]
            height2.append(-9999.0)
            height2.sort()
            height = xr.DataArray(height2, coords=[(DNAME_HAX, h_axis)],
                                   attrs={'long_name': 'height class elevation levels',
                                          'standard_name': 'height',
                                          'units': 'meter',
                                          'positive': 'up',})
            restart_data[DNAME_HAX] = haxis
            restart_data[parsed_argv.vname_initial_height] = height

            restart_data.drop(vname_new)
            restart_data.drop_dims(dname_new)
            restart_data_tmp = restart_data.copy()
            del height2


    saxis = xr.DataArray(s_axis, coords=[(DNAME_SAX, s_axis)],
                         attrs={'long_name': 'stations',
                                'axis': 's'})
    caxis = xr.DataArray(c_axis, coords=[(DNAME_CAX, c_axis)],
                         attrs={'long_name': 'grid corners',
                                'standard_name': 'time',
                                'axis': 'c',
                                'kind': 'axis along the grid corners'})
    tb_axis = xr.DataArray(t_bounds_axis, coords=[(DNAME_TBX, t_bounds_axis)],
                           attrs={'long_name': 'time bounds axis',
                                  'axis': 'b',
                                  'kind': 'axis of time bounds with length two'})
    taxis = xr.DataArray(t_axis, coords=[(DNAME_TAX, t_axis)])

    #fails sometimes: restart_data[DNAME_IAX] = iaxis
    #fails sometimes: restart_data[DNAME_JAX] = jaxis
    restart_data[DNAME_SAX] = saxis
    restart_data[DNAME_CAX] = caxis

    # Add time axis only if is does NOT exist already
    # Fails: restart_data[DNAME_TAX] = taxis
    if DNAME_TAX is not restart_data.dims:
        restart_data = restart_data.expand_dims({'time': taxis}). \
            mean(dim=DNAME_TAX, keepdims=True)

    restart_data[DNAME_TBX] = tb_axis

    #
    # Delete not necassary fields
    #
    # List of needed variable which shall also have the 'coordinates' attribute
    var_needed_add_coordinate_attrs = [VNAME_SURFACE_SLOPE_REF,
                                       VNAME_SURFACE_ELEVATION_REF]
                                       # VNAME_BASAL_HEAT_FLUX_MAP_REF]
    # List of variables without the 'coordinates' attribute
    var_needed = [parsed_argv.vname_initial_height,
                  parsed_argv.vname_initial_layer_thickness,
                  parsed_argv.vname_initial_axis_left,
                  parsed_argv.vname_initial_axis_right,
                  parsed_argv.vname_initial_longitude,
                  parsed_argv.vname_initial_latitude]
    for var in var_needed_add_coordinate_attrs:
        var_needed.append(var)

    #
    # 1) Delete all not needed variable: var_needed
    # 2) Add the 'coordinates' attribute to the variables: var_needed_add_coordinate_attrs
    #
    for var in restart_data.var().copy():
        if var not in var_needed:
            #
            # Delete not needed
            #
            print('  Del '+var)
            del restart_data[var]
        elif var in var_needed_add_coordinate_attrs and var is not None:
            #
            # Add 'coordinates' attribute
            #
            print('  Add "coordinates" '+var)
            restart_data[var].attrs[ATTRS_COORDINATES] = \
                parsed_argv.vname_initial_longitude+' '+parsed_argv.vname_initial_latitude

    #
    # Delete old global attributes. Here we COPY the dict to allow that the
    # applied changes ('del') are allowed within the loop below.
    #
    for attr in restart_data.attrs.copy():
        if attr in ['CDI' , 'CDO', 'Comments', ATTRS_COORDINATES, 'Creator',
                    'Creators', 'History', 'history', 'history_of_appended_files',
                    'NCO', 'nco_openmp_thread_number', 'Title',]:
            del restart_data.attrs[attr]



    #
    # Create new fields
    #
    print(' +-----------------------------------+')
    print(' |                                   |')
    if parsed_argv.vname_initial_height is not None:
        do_height_classes = True
        if parsed_argv.height_class_plus:
            do_height_classes_plus = True
            print(' |       Height classes PLUS         |')
        else:
            do_height_classes_plus = False
            print(' |      Height classes (standard)    |')
    else:
        do_height_classes = False
        print(' | Standard case (No height classes) |')
        h_axis = [0]
    print(' |                                   |')
    print(' +-----------------------------------+')
    # dim2_shape = [yaxis.size, xaxis.size]
    # dim3_shape = [layer_thickness.size, yaxis.size, xaxis.size]
    dim2t_shape = [1, yaxis.size, xaxis.size]
    dim3t_shape = [1, layer_thickness.size, yaxis.size, xaxis.size]
    dim3t_shape = [1, layer_thickness.size, yaxis.size, xaxis.size]
    if do_height_classes:
        dim2h_shape = [height.size, yaxis.size, xaxis.size]
        dim2th_shape = [1, height.size, yaxis.size, xaxis.size]
        dim3th_shape = [1, height.size, layer_thickness.size, yaxis.size, xaxis.size]
        dim4th_shape = [1, height.size, layer_thickness.size, yaxis.size, xaxis.size]


    #
    # time and related variables
    #
    time = xr.DataArray(np.array([0.0]), dims=[DNAME_TAX],
                         attrs={'long_name': 'model time',
                                'standard_name': 'time',
                                'units': 'seconds since 1990-01-01 0:0:0',
                                'axis': 'T',
                                'calendar': 'standard',
                                'bounds': 'time_bounds'})

    time_bounds = xr.DataArray(np.array([0, 1], dtype=float), dims=[DNAME_TBX],
                          attrs={'long_name': 'time bounds',})

    w_step = xr.DataArray(np.array([0], dtype=int), dims=[DNAME_TAX],
                          attrs={'long_name': 'time steps covered by the current record',
                                 'units': '1',
                                 'valid_min': 0})

    w_dt = xr.DataArray(np.array([0.001]), dims=[DNAME_TAX],
                          attrs={'long_name': 'time period of the written record',
                                 'units': 'seconds',
                                 'valid_min': 0.0})

    istep = xr.DataArray(np.array([0], dtype=int), dims=[DNAME_TAX],
                          attrs={'long_name': 'time step of numerical simulation',
                                 'units': '1',
                                 'valid_min': 0})

    #
    # deduced variables
    #
    depth = xr.DataArray(thickness2depth(layer_thickness),
                         dims=[DNAME_KAX],
                         attrs={'long_name': 'depth below surface' ,
                                'units': 'm' ,
                                'code': 10 ,
                                'standard_name': 'depth' ,
                                'valid_min': 0.0})

    zs_atm = xr.DataArray(surface_elevation_climate.data,
                          dims=[DNAME_JAX, DNAME_IAX],
                          attrs={'long_name':
                                 'orography atmosphere forcing',
                                 'units': 'm',
                                 ATTRS_COORDINATES:
                                     parsed_argv.vname_initial_longitude+
                                     ' '+parsed_argv.vname_initial_latitude})

    h_axis2 = h_axis.copy()
    if do_height_classes:
        # If we reverse h_axis2, we keep as last the results of the the lowest
        # index, which is Fortran-index 0 in case, we have activated
        # "--height_class_plus". In this case we have correct `delta_zs` and
        # `temperature_height_correction` in the output file without the need
        # to copy the fields
        h_axis2.sort(reverse=True)
        temp_final = np.zeros(dim2h_shape)


    for hax in h_axis2:
        #
        # Height correction of the temperature
        #
        if hax <= 0:
            # Normal case, where the height difference is computed relative to
            # a given surface_elevation obtain from the initial file.
            # No height classes or plus case for activated height classes
            delta_zs = \
                (surface_elevation_climate.data-surface_elevation_initial.data)
        else:
            # Height classes: height difference between given height level and
            # the surface elevation of the forcing data.
            delta_zs = (surface_elevation_climate.data-height.data[hax-1])

        delta_zs = xr.DataArray(delta_zs,
                                dims=[DNAME_JAX, DNAME_IAX],
                                attrs={'long_name':
                                       'elevation difference: forcing-initial',
                                       'units': 'm',
                                       ATTRS_COORDINATES:
                                           parsed_argv.vname_initial_longitude+
                                           ' '+parsed_argv.vname_initial_latitude})

        temperature_height_correction = \
            xr.DataArray(delta_zs*abs(parsed_argv.lapse_rate) ,
                         dims=[DNAME_JAX, DNAME_IAX],
                         attrs={'long_name':
                                'temperature offset due to height correction, lapse rate='+str(abs(parsed_argv.lapse_rate))+' K m-1',
                                'units': 'K',
                                ATTRS_COORDINATES:
                                    parsed_argv.vname_initial_longitude+
                                    ' '+parsed_argv.vname_initial_latitude})
        if do_height_classes:
            temp_final[hax-1, :, :] = temperature.data+temperature_height_correction
        else:
            temp_final = temperature.data+temperature_height_correction

    del h_axis2 # Clean-up and discard the (reversed) copy of h_axis

    #
    # 2dim fields
    #
    if do_height_classes:
        #
        # Considering Height/elevation classes
        #
        stime = stime_func(temp_final, TEMPERATURE_MELT,
                           DNAME_TAX, DNAME_JAX, DNAME_IAX,
                           parsed_argv.vname_initial_longitude,
                           parsed_argv.vname_initial_latitude,
                           dn_hax=DNAME_HAX)

        mtime = mtime_func(temp_final, TEMPERATURE_MELT,
                           DNAME_TAX, DNAME_JAX, DNAME_IAX,
                           parsed_argv.vname_initial_longitude,
                           parsed_argv.vname_initial_latitude,
                           dn_hax=DNAME_HAX)

        xtime = xtime_func(dim2th_shape,
                           DNAME_TAX, DNAME_JAX, DNAME_IAX,
                           parsed_argv.vname_initial_longitude,
                           parsed_argv.vname_initial_latitude,
                           dn_hax=DNAME_HAX)


        snowdepth = snowdepth_func(temp_final, TEMPERATURE_MELT,
                                   SNOW_THICKNESS_THRESHOLD,
                                   DNAME_TAX, DNAME_JAX, DNAME_IAX,
                                   parsed_argv.vname_initial_longitude,
                                   parsed_argv.vname_initial_latitude,
                                   dn_hax=DNAME_HAX)

        snowts = snowts_func(temp_final, TEMPERATURE_MELT,
                             DNAME_TAX, DNAME_JAX, DNAME_IAX,
                             parsed_argv.vname_initial_longitude,
                             parsed_argv.vname_initial_latitude,
                             dn_hax=DNAME_HAX)

        meltmask = meltmask_func(mtime,
                                 DNAME_TAX, DNAME_JAX, DNAME_IAX,
                                 parsed_argv.vname_initial_longitude,
                                 parsed_argv.vname_initial_latitude,
                                 dn_hax=DNAME_HAX)

        #
        # 3dim fields
        #
        #
        # snow temperature distribution
        #
        temp_snow = snowtemp_func(temp_final, TEMPERATURE_MELT, dim3th_shape,
                                  DNAME_TAX, DNAME_KAX, DNAME_JAX, DNAME_IAX,
                                  parsed_argv.vname_initial_longitude,
                                  parsed_argv.vname_initial_latitude,
                                  dn_hax=DNAME_HAX)

        #
        # Density with depth
        #
        rho = rho_func(temp_final, TEMPERATURE_MELT, temp_snow, depth,
                       DNAME_TAX, DNAME_KAX, DNAME_JAX, DNAME_IAX,
                       parsed_argv.vname_initial_longitude,
                       parsed_argv.vname_initial_latitude,
                       DNAME_HAX,
                       density_snow_=REFERENCE_SNOW_DENSITY,
                       density_ice_=REFERENCE_ICE_DENSITY,
                       c_dens_=REFERENCE_CDENSITY)

        #
        # Snow, Ice, and Water content
        #
        zeros_field = np.zeros(dim3th_shape)

        delta_temperature = xr.DataArray(np.expand_dims((temp_final-TEMPERATURE_MELT).data, axis=0),
                                         dims=[DNAME_TAX, DNAME_HAX, DNAME_JAX, DNAME_IAX],
                                         attrs={'long_name':
                                                'temperature above melting '+str(TEMPERATURE_MELT),
                                                'units': 'K',
                                                ATTRS_COORDINATES:
                                                    parsed_argv.vname_initial_longitude+
                                                    ' '+parsed_argv.vname_initial_latitude,
                                                'meaning': 'above 0 means above melting'})

        # TODO:RM:
        # fract_water = xr.DataArray(np.where(delta_temperature>=5.,
        #                                     0.9,
        #                                     np.where(delta_temperature<=-5.0, 0.0, 0.02)),
        #                                dims=[DNAME_TAX, DNAME_HAX, DNAME_JAX, DNAME_IAX])
        fract_water = np.where(delta_temperature>=5.,
                               0.9,
                               np.where(delta_temperature<=-5.0, 0.0, 0.02))

        field = np.zeros_like(zeros_field)
        for k in range(layer_thickness.size):
            field[:, :, k, ::] = fract_water
        fraction_water = xr.DataArray(field,
                                      dims=[DNAME_TAX, DNAME_HAX, DNAME_KAX, DNAME_JAX, DNAME_IAX],
                                      attrs={'long_name': 'mass fraction of water',
                                             'units': '1',
                                             ATTRS_COORDINATES:
                                                 parsed_argv.vname_initial_longitude+
                                                 ' '+parsed_argv.vname_initial_latitude,})
        # No water in the top layer and the last two layers
        fraction_water[:, :, 0,::] = fraction_water[:, :, 0,::]*0.1
        fraction_water[:, :, -2:,::] = 0.0

        fraction_ice = xr.DataArray(zeros_field+0.12,
                                    dims=[DNAME_TAX, DNAME_HAX, DNAME_KAX, DNAME_JAX, DNAME_IAX],
                                    attrs={'long_name': 'mass fraction of ice',
                                           'units': '1',
                                           ATTRS_COORDINATES:
                                               parsed_argv.vname_initial_longitude+
                                               ' '+parsed_argv.vname_initial_latitude,})

        fraction_snow = xr.DataArray(np.maximum(0.0, 1.0-(fraction_ice+fraction_water)),
                                     dims=[DNAME_TAX, DNAME_HAX, DNAME_KAX, DNAME_JAX, DNAME_IAX],
                                     attrs={'long_name': 'mass fraction of snow',
                                            'units': '1',
                                            ATTRS_COORDINATES:
                                                parsed_argv.vname_initial_longitude+
                                                ' '+parsed_argv.vname_initial_latitude,})

        cont_snow, cont_ice, cont_water = siw_cont_func(layer_thickness,
                                                        fraction_snow,
                                                        fraction_ice,
                                                        fraction_water,
                                                        DNAME_TAX, DNAME_KAX,
                                                        DNAME_JAX, DNAME_IAX,
                                                        parsed_argv.vname_initial_longitude,
                                                        parsed_argv.vname_initial_latitude,
                                                        DNAME_HAX,
                                                        snow_dens_=rho,
                                                        ice_dens_=REFERENCE_ICE_DENSITY,
                                                        water_dens_=REFERENCE_WATER_DENSITY)

        #
        # Grain size diameter
        #
        TIME_DAMPING_FACTOR = 0.001
        graind = graind_func(precipitation, cont_snow, cont_water, depth,
                       DNAME_TAX, DNAME_KAX, DNAME_JAX, DNAME_IAX,
                       parsed_argv.vname_initial_longitude,
                       parsed_argv.vname_initial_latitude,
                       dn_hax=DNAME_HAX,
                       grain_initial_=GRAIN_SIZE_INITIAL,
                       grain_max_=GRAIN_SIZE_MAX_THRESHOLD,
                       water_dens_=REFERENCE_WATER_DENSITY,
                       time_damping_=TIME_DAMPING_FACTOR)

        #
        # Age since snow has left the surface layer
        #
        age = age_func(precipitation, depth,
                       DNAME_TAX, DNAME_KAX, DNAME_JAX, DNAME_IAX,
                       parsed_argv.vname_initial_longitude,
                       parsed_argv.vname_initial_latitude,
                       dn_hax=DNAME_HAX,
                       water_dens_=REFERENCE_WATER_DENSITY,)

    else:
        stime = stime_func(temp_final, TEMPERATURE_MELT,
                           DNAME_TAX, DNAME_JAX, DNAME_IAX,
                           parsed_argv.vname_initial_longitude,
                           parsed_argv.vname_initial_latitude)

        mtime = mtime_func(temp_final, TEMPERATURE_MELT,
                           DNAME_TAX, DNAME_JAX, DNAME_IAX,
                           parsed_argv.vname_initial_longitude,
                           parsed_argv.vname_initial_latitude)

        xtime = xtime_func(dim2t_shape,
                           DNAME_TAX, DNAME_JAX, DNAME_IAX,
                           parsed_argv.vname_initial_longitude,
                           parsed_argv.vname_initial_latitude)


        snowdepth = snowdepth_func(temp_final, TEMPERATURE_MELT,
                                   SNOW_THICKNESS_THRESHOLD,
                                   DNAME_TAX, DNAME_JAX, DNAME_IAX,
                                   parsed_argv.vname_initial_longitude,
                                   parsed_argv.vname_initial_latitude)

        snowts = snowts_func(temp_final, TEMPERATURE_MELT,
                             DNAME_TAX, DNAME_JAX, DNAME_IAX,
                             parsed_argv.vname_initial_longitude,
                             parsed_argv.vname_initial_latitude)

        meltmask = meltmask_func(mtime,
                                 DNAME_TAX, DNAME_JAX, DNAME_IAX,
                                 parsed_argv.vname_initial_longitude,
                                 parsed_argv.vname_initial_latitude)

        #
        # 3dim fields
        #
        #
        # snow temperature distribution
        #
        temp_snow = snowtemp_func(temp_final, TEMPERATURE_MELT, dim3t_shape,
                                  DNAME_TAX, DNAME_KAX, DNAME_JAX, DNAME_IAX,
                                  parsed_argv.vname_initial_longitude,
                                  parsed_argv.vname_initial_latitude)

        #
        # Density with depth
        #
        rho = rho_func(temp_final, TEMPERATURE_MELT, temp_snow, depth,
                       DNAME_TAX, DNAME_KAX, DNAME_JAX, DNAME_IAX,
                       parsed_argv.vname_initial_longitude,
                       parsed_argv.vname_initial_latitude,
                       density_snow_=REFERENCE_SNOW_DENSITY,
                       density_ice_=REFERENCE_ICE_DENSITY,
                       c_dens_=REFERENCE_CDENSITY)

        #
        # Snow, Ice, and Water content
        #
        delta_temperature = xr.DataArray(np.expand_dims((temp_final-TEMPERATURE_MELT).data, axis=0),
                                         dims=[DNAME_TAX, DNAME_JAX, DNAME_IAX],
                                         attrs={'long_name':
                                                'temperature above melting '+str(TEMPERATURE_MELT),
                                                'units': 'K',
                                                ATTRS_COORDINATES:
                                                    parsed_argv.vname_initial_longitude+
                                                    ' '+parsed_argv.vname_initial_latitude,
                                                'meaning': 'above 0 means above melting'})

        fract_water = np.where(delta_temperature>=5.,
                               0.9,
                               np.where(delta_temperature<=-5.0, 0.0, 0.02))
        fraction_water = xr.DataArray(np.zeros(dim3t_shape)+fract_water,
                                      dims=[DNAME_TAX, DNAME_KAX, DNAME_JAX, DNAME_IAX],
                                      attrs={'long_name': 'mass fraction of water',
                                             'units': '1',
                                             ATTRS_COORDINATES:
                                                 parsed_argv.vname_initial_longitude+
                                                 ' '+parsed_argv.vname_initial_latitude,})
        # No water in the top layer and the last two layers
        fraction_water[:, 0,::] = fraction_water[:, 0,::]*0.1
        fraction_water[:, -2:,::] = np.zeros(dim2t_shape)

        fraction_ice = xr.DataArray(np.zeros(dim3t_shape)+0.12,
                                    dims=[DNAME_TAX, DNAME_KAX, DNAME_JAX, DNAME_IAX],
                                    attrs={'long_name': 'mass fraction of ice',
                                           'units': '1',
                                           ATTRS_COORDINATES:
                                               parsed_argv.vname_initial_longitude+
                                               ' '+parsed_argv.vname_initial_latitude,})

        fraction_snow = xr.DataArray(np.maximum(0.0, 1.0-(fraction_ice+fraction_water)),
                                     dims=[DNAME_TAX, DNAME_KAX, DNAME_JAX, DNAME_IAX],
                                     attrs={'long_name': 'mass fraction of snow',
                                            'units': '1',
                                            ATTRS_COORDINATES:
                                                parsed_argv.vname_initial_longitude+
                                                ' '+parsed_argv.vname_initial_latitude,})

        cont_snow, cont_ice, cont_water = siw_cont_func(layer_thickness,
                                                        fraction_snow,
                                                        fraction_ice,
                                                        fraction_water,
                                                        DNAME_TAX, DNAME_KAX,
                                                        DNAME_JAX, DNAME_IAX,
                                                        parsed_argv.vname_initial_longitude,
                                                        parsed_argv.vname_initial_latitude,
                                                        snow_dens_=rho,
                                                        ice_dens_=REFERENCE_ICE_DENSITY,
                                                        water_dens_=REFERENCE_WATER_DENSITY)

        #
        # Grain size diameter
        #
        TIME_DAMPING_FACTOR = 0.001
        graind = graind_func(precipitation, cont_snow, cont_water, depth,
                       DNAME_TAX, DNAME_KAX, DNAME_JAX, DNAME_IAX,
                       parsed_argv.vname_initial_longitude,
                       parsed_argv.vname_initial_latitude,
                       grain_initial_=GRAIN_SIZE_INITIAL,
                       grain_max_=GRAIN_SIZE_MAX_THRESHOLD,
                       water_dens_=REFERENCE_WATER_DENSITY,
                       time_damping_=TIME_DAMPING_FACTOR)

        #
        # Age since snow has left the surface layer
        #
        age = age_func(precipitation, depth,
                       DNAME_TAX, DNAME_KAX, DNAME_JAX, DNAME_IAX,
                       parsed_argv.vname_initial_longitude,
                       parsed_argv.vname_initial_latitude,
                       water_dens_=REFERENCE_WATER_DENSITY,)

    #
    # Add computed fields to the final DataSet
    #
    # Required fieds (as of 2022-09-06):
    #  - layer_thickness
    #  - istep
    #  - w_step
    #  - w_dt
    #  - time_bounds
    #  - time_bounds
    #  - stime
    #  - mtime
    #  - xtime
    #  - snowdepth
    #  - snowts
    #  - meltmask
    #  - snowtemp
    #  - cont_snow
    #  - cont_ice
    #  - cont_water
    #  - rho
    #  - graind
    #
    # Nice to have
    #  - depth
    #
    # For the future we already add
    #  - age
    #

    restart_data['depth'] = depth

    restart_data['time'] = time
    restart_data['time_bounds'] = time_bounds
    restart_data['w_step'] = w_step
    restart_data['w_dt'] = w_dt
    restart_data['istep'] = istep


    restart_data['mtime'] = mtime
    restart_data['stime'] = stime
    restart_data['xtime'] = xtime

    restart_data['snowdepth'] = snowdepth
    restart_data['snowts'] = snowts
    restart_data['meltmask'] = meltmask

    restart_data['snowtemp'] = temp_snow
    restart_data['rho'] = rho

    restart_data['cont_snow'] = cont_snow
    restart_data['cont_ice'] = cont_ice
    restart_data['cont_water'] = cont_water
    restart_data['graind'] = graind

    restart_data['age'] = age

    #
    # Diagnostic fields (not required)
    #
    restart_data['fraction_snow'] = fraction_snow
    restart_data['fraction_ice'] = fraction_ice
    restart_data['fraction_water'] = fraction_water
    if not do_height_classes or parsed_argv.height_class_plus:
        restart_data['zs_atm'] = zs_atm
        restart_data['height_difference'] = delta_zs
        restart_data['temp_height_correction'] = temperature_height_correction

    #
    # Global attributes
    #
    # Here we determine your user name, which is below turned into your
    # real name in the if-then-elseif-elseif-construction.
    # NOTE: If you want set your full Name and insitution settings, you may
    #       expand or change below the if-then-elseif-elseif-construction below.
    #
    creator = os.getlogin() # You user name reported by the operating system

    #
    # Templates of labels: Shall be modified in the
    # if-then-elseif-elseif-construction below.
    #
    institution_labels = {'insitution': None,
                          'institution_id': None,
                          'department': None,
                          'address': None,
                          'contact': None,
                          'web_page': None}
    experiment_labels = {'model_domain': 'unknown',
                         'experiment': 'unknown',
                         'experiment_id': 'unknown',
                         'parent_experiment_id': 'unknown',
                         'project': 'PROTECT (European Union), NCKF (National DK)'}

    #
    # Special settings for individuals (know by your user name, e.g. $USER on *nix)
    #
    if creator == 'cr':
        creator = 'Christian Rodehacke'
        institution_labels = {'insitution': 'Danish Meteorological Institute',
                              'institution_id': 'DMI',
                              'department': 'Rigsfaellesskabets Klima',
                              'address': 'Lyngbyvej 100, DK-2100 Copenhagen, Denmark',
                              'contact': creator+', http://research.dmi.dk/staff/all-staff/cr',
                              'web_page': 'https://www.dmi.dk'}
    elif creator == 'msm':
        creator = 'Marianne S Madsen'
        institution_labels = {'insitution': 'Danish Meteorological Institute',
                              'institution_id': 'DMI',
                              'department': 'Rigsfaellesskabets Klima',
                              'address': 'Lyngbyvej 100, DK-2100 Copenhagen, Denmark',
                              'contact': creator,
                              'web_page': 'https://www.dmi.dk'}
    elif creator == 'uffebu':
        creator = 'Uffe Bundesen'
        institution_labels = {'insitution': 'Danish Meteorological Institute',
                              'institution_id': 'DMI',
                              'department': 'Rigsfaellesskabets Klima',
                              'address': 'Lyngbyvej 100, DK-2100 Copenhagen, Denmark',
                              'contact': creator,
                              'web_page': 'https://www.dmi.dk'}

    #
    # Write the attributes
    #
    # Common CISSSEMBEL related attributes
    #
    restart_data.attrs['title'] = 'Estimated starting conditions for CISSEMBEL'
    restart_data.attrs['type'] = 'Numerical simulations of the Surface Mass Balance (SMB)'
    restart_data.attrs['model'] = \
        'Copenhagen Ice-Snow Surface Energy and Mass Balance modEL (CISSEMBEL)'
    restart_data.attrs['source'] = \
        'Copenhagen Ice-Snow Surface Energy and Mass Balance modEL (CISSEMBEL)'
    restart_data.attrs['source_id'] = 'CISSEMBEL'
    restart_data.attrs['Conventions'] = 'CF-1.6'
    restart_data.attrs['realm'] = 'cryosphere, land'
    restart_data.attrs['keywords'] = \
        'Surface Mass Balance, numerical simulation, cryosphere, CISSEMBEL'
    restart_data.attrs['license'] = \
        'Creative Commons Attribution 4.0 International (CC BY 4.0)'

    #
    # Institution, user, address related attributes
    #
    for key in institution_labels:
        if institution_labels.get(key) is not None:
            restart_data.attrs[key] = institution_labels.get(key)

    #
    # Domain and experiment id related attributes
    #
    for key in experiment_labels:
        if experiment_labels.get(key) is not None:
            restart_data.attrs[key] = experiment_labels.get(key)

    #
    # Specific and automatically deduced attributes
    #
    restart_data.attrs['creation_date'] = str(datetime.now())
    restart_data.attrs['creator'] = creator
    restart_data.attrs['initial_data_source'] = parsed_argv.input_initial
    restart_data.attrs['forcing_data_source'] = parsed_argv.input_climate

    restart_data.attrs[ATTRS_COORDINATES] = \
        parsed_argv.vname_initial_longitude+ \
        ' '+parsed_argv.vname_initial_latitude

    #
    # Write the data
    #
    print('Write (re)start file "'+parsed_argv.output
          +'" as '+parsed_argv.output_format)
    restart_data.to_netcdf(parsed_argv.output,
                           format=parsed_argv.output_format,
                           unlimited_dims='time')

    #
    # Some information for namelist files
    #
    print()
    print('Pre-processor variable in ./src/cppdefs.h')
    print("==== /src/cppdefs.h ======== [begin] ==")
    if do_height_classes:
        print('#define ALLOW_HEIGHT_CLASSES')
    else:
        print('#define ALLOW_HEIGHT_CLASSES')
    print("==== ./src/cppdefs.h ======= [end] ====")

    print()
    print('Namelist fragments')
    print("==== namelist.EBMdriver === [begin] ==")
    print("&offline_domain")
    print("  xsize = "+str(xaxis.size)+" ,")
    print("  ysize = "+str(yaxis.size)+" ,")
    print("  ! *** Keep or adjust the rest of this section ***")
    print("/")
    print("==== namelist.EBMdriver === [end] ====")

    print("==== namelist.ebm ========== [begin] ==")
    print("&run_ctrl")
    print("  do_restart = T ,")
    if do_height_classes:
        print("  do_height_classes = F,")
        if parsed_argv.height_class_plus:
            print("  do_height_classes_plus = T,")
        else:
            print("  do_height_classes_plus = F,")
    print("  ! *** Keep or adjust the rest of this section ***")
    print("/")
    print()
    print("&ini_file")
    print("  fname_ini_data = '<Adjust the path to your needs/>"+parsed_argv.input_initial+"',")
    print("  dimname_iaxis = '"+DNAME_IAX+"',")
    print("  dimname_jaxis = '"+DNAME_JAX+"',")
    print("  dimname_kaxis = '"+DNAME_KAX+"',")
    print("  dimname_haxis = '"+DNAME_HAX+"',")
    print("  dimname_raxis = 'raxis',")
    print("  dimname_saxis = '"+DNAME_SAX+"',")
    print("  dimname_caxis = '"+DNAME_CAX+"',")
    print("  dimname_1axis = 'one' ,")
    print("  dimname_taxis = '"+DNAME_TAX+"',")
    print("  varname_lon = '"+parsed_argv.vname_initial_longitude+"',")
    print("  varname_lat = '"+parsed_argv.vname_initial_latitude+"',")
    print("  varname_target_elevation = '"+parsed_argv.vname_initial_surface_elevation+"',")
    print("  varname_region_mask = 'Basins',")
    print("  varname_station_mask = 'station_mask', ")
    print("  varname_station_number ='station_id', ")
    print("  varname_depth = 'depth', ")
    print("  varname_layer_thickness = '"+parsed_argv.vname_initial_layer_thickness+"',")
    print("  varname_height ='height', ")
    print("  varname_surface_slope = '"+VNAME_SURFACE_SLOPE_REF+"' ,")
    print("/")
    print()
    print("&restart_file")
    print("  fname_rst_input = '<Adjust the path to your needs/>"+parsed_argv.output+"',")
    print("  fname_rst_output = '<Adjust the path to your needs/>restart_output.nc' ,")
    print("/")
    print("==== namelist.ebm ========== [end] ====")

    print()
    print('Complete the job ('+str(datetime.now())[:19]+')')

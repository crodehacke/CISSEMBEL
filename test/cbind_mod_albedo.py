#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  8 22:39:16 2021

C-binding to Fortran functions and subroutines in mod_albedo as part of the
Copenhagen Ice Snow Surface Energy and Mass Balance modEL (CISSEMBEL)

@author: Christian Rodehacke, DMI Copenhagen, Denmark

@copyright: Copyright (C) 2016-2025 Christian Rodehacke
"""
# -------------------------------------------------------------------
# This file is part of CISSEMBEL, which is the
# == Copenhagen Ice-Snow Surface Energy and Mass Balance modEL ==
#
# CISSEMBEL is free software; you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE  v.1.2
#
# Information about the EUROPEAN UNION PUBLIC LICENCE (*EUPL*) and
# translations into other European languages are available at
# https://eupl.eu (last access: 2023-09-18)](https://eupl.eu/)
#
# We hope that  this  distributed  CISSEMBEL  code, its  tools and
# documentations are usefull. Any improvements of the code, tools,
# and documentations tools are  very welcome.  Please, consider to
# share your thoughts and improvements with the community.
#
# The documentation,  tools,  and the code are provided as it are.
# NO WARRANTY  ABOUT FITNESS  AND USABILITY OF THE CODE, TOOLS, OR
# DOCUMENTATION  ARE  GIVEN.   NO  WARRANTY  IS  GIVEN  ABOUT  THE
# CORRECTNESS  OF COMPUTED RESULTS WITH THE  PROVIDED CODE, TOOLS,
# AND DOCUMENTATION. YOU USE THE CODE, DOCUMENTATION, AND TOOLS AT
# YOUR OWN RISK.           Removing  of  any  copyright  statement
# or  this  disclaimer  is considered  as a  attempt  to defraud.
#
# -------------------------------------------------------------------

def albedo_temp(surface_temperature, melt_temperature, trans_temperature,
                library='../src/CISSEMBEL_CBindings.so'):
    """
    C-binding to Fortran function albedo_temp in mod_albedo in CISSEMBEL
    Snow albedo based on the surface temperature.

    Parameters
    ----------
    surface_temperature : float or numpy.ndarray
        Temperature of the snow/ice column top.
    melt_temperature : float or numpy.ndarray
        Melting temperature of the snow/ice column top.
    trans_temperature : float
        Transiiton temperature of the snow/ice.
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings. The
        default is '../src/CISSEMBEL_CBindings.so'.

    Returns
    -------
    albedo : float or numpy.ndarray
        Albedo of the snow/ice column top.

    """
    if isinstance(library, ctypes.CDLL):
        fortlibrary = library
    else:
        fortlibrary = ctypes.cdll.LoadLibrary(library)

    if isinstance(surface_temperature, (float, int)):
        inum = 0
        # Single c-value
        c_surface_temperature = ctypes.c_double(surface_temperature)
        c_melt_temperature = ctypes.c_double(melt_temperature)
        c_trans_temperature = ctypes.c_double(trans_temperature)
    else:
        inum = len(surface_temperature)
        # Number conversion, Fortran-order
        c_surface_temperature = numpy.array(surface_temperature, order="F", dtype=float)
        c_melt_temperature = numpy.array(melt_temperature, order="F", dtype=float)
        # c-pointer
        c_surface_temperature_ptr = c_surface_temperature.ctypes.data_as( \
            ctypes.POINTER(ctypes.c_double))
        c_melt_temperature_ptr = c_melt_temperature.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        # Field sizes
        m4c.compare_field2shapes(surface_temperature.shape, melt_temperature.shape,
                                 'surface_temperature', 'melt_temperature')


    c_trans_temperature = ctypes.c_double(trans_temperature)

    if inum:
        # array
        albedo = numpy.zeros((inum), order="F", dtype=float)
        albedo_ptr = albedo.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        fortlibrary.c_albedo_temp.argtypes = [ctypes.c_int,
                                              ctypes.POINTER(ctypes.c_double),
                                              ctypes.POINTER(ctypes.c_double),
                                              ctypes.c_double,
                                              ctypes.POINTER(ctypes.c_double)]
        fortlibrary.c_albedo_temp(ctypes.c_int(inum),
                                  c_surface_temperature_ptr,
                                  c_melt_temperature_ptr,
                                  c_trans_temperature,
                                  albedo_ptr)
    else:
        # skalar
        fortlibrary.albedo_temp.restype = ctypes.c_double
        albedo = fortlibrary.albedo_temp(ctypes.byref(c_surface_temperature),
                                         ctypes.byref(c_melt_temperature),
                                         ctypes.byref(c_trans_temperature))
    return albedo


def albedo_temp_snowdepth(surface_temperature, snow_depth, melt_temperature,
                          trans_temperature, snow_depth_threshold,
                          library='../src/CISSEMBEL_CBindings.so'):
    """
    C-binding to Fortran function albedo_temp_snowdepth in mod_albedo in CISSEMBEL
    Snow albedo based on the surface temperature and snow depth.

    Parameters
    ----------
    surface_temperature : float or numpy.ndarray
        Temperature of the snow/ice column top.
    snow_depth : float or numpy.ndarray
        Snow depth.
    melt_temperature : float or numpy.ndarray
        Melting temperature of the snow/ice column top.
    trans_temperature : float
        Transiiton temperature of the snow/ice.
    snow_depth_threshold : float
        Snow depth threshold.
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings. The
        default is '../src/CISSEMBEL_CBindings.so'.

    Returns
    -------
    albedo : float or numpy.ndarray
        Albedo of the snow/ice column top.

    """
    if isinstance(library, ctypes.CDLL):
        fortlibrary = library
    else:
        fortlibrary = ctypes.cdll.LoadLibrary(library)

    if isinstance(surface_temperature, (float, int)):
        inum = 0
        # Single c-value
        c_surface_temperature = ctypes.c_double(surface_temperature)
        c_snow_depth = ctypes.c_double(snow_depth)
        c_melt_temperature = ctypes.c_double(melt_temperature)
        c_trans_temperature = ctypes.c_double(trans_temperature)
    else:
        inum = len(surface_temperature)
        # Number conversion, Fortran-order
        c_surface_temperature = numpy.array(surface_temperature, order="F", dtype=float)
        c_snow_depth = numpy.array(snow_depth, order="F", dtype=float)
        c_melt_temperature = numpy.array(melt_temperature, order="F", dtype=float)
        # c-pointer
        c_surface_temperature_ptr = c_surface_temperature.ctypes.data_as( \
            ctypes.POINTER(ctypes.c_double))
        c_snow_depth_ptr = c_snow_depth.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_melt_temperature_ptr = c_melt_temperature.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        # Field sizes
        m4c.compare_field2shapes(surface_temperature.shape, snow_depth.shape,
                                 'surface_temperature', 'snow_depth')
        m4c.compare_field2shapes(surface_temperature.shape, melt_temperature.shape,
                                 'surface_temperature', 'melt_temperature')


    c_trans_temperature = ctypes.c_double(trans_temperature)
    c_snow_depth_threshold = ctypes.c_double(snow_depth_threshold)

    if inum:
        # array
        albedo = numpy.zeros((inum), order="F", dtype=float)
        albedo_ptr = albedo.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        fortlibrary.c_albedo_temp_snowdepth.argtypes = [ctypes.c_int,
                                              ctypes.POINTER(ctypes.c_double),
                                              ctypes.POINTER(ctypes.c_double),
                                              ctypes.POINTER(ctypes.c_double),
                                              ctypes.c_double,
                                              ctypes.c_double,
                                              ctypes.POINTER(ctypes.c_double)]
        fortlibrary.c_albedo_temp_snowdepth(ctypes.c_int(inum),
                                  c_surface_temperature_ptr,
                                  c_snow_depth_ptr,
                                  c_melt_temperature_ptr,
                                  c_trans_temperature,
                                  c_snow_depth_threshold,
                                  albedo_ptr)
    else:
        # skalar
        fortlibrary.albedo_temp_snowdepth.restype = ctypes.c_double
        albedo = fortlibrary.albedo_temp_snowdepth( \
            ctypes.byref(c_surface_temperature),
            ctypes.byref(c_snow_depth),
            ctypes.byref(c_melt_temperature),
            ctypes.byref(c_trans_temperature),
            ctypes.byref(c_snow_depth_threshold))
    return albedo


def albedo_time(snow_age, albedo_bright, albedo_dark, reciprocal_decaytime,
                library='../src/CISSEMBEL_CBindings.so'):
    """
    C-binding to Fortran function albedo_time in mod_albedo in CISSEMBEL
    Snow albedo based on elapsed time since last snow event.

    Parameters
    ----------
    snow_age : float or numpy.ndarray
        Elapsed time since the last snow event.
    albedo_bright : float
        Bright/high albedo of fresh snow.
    albedo_dark : float
        Dark/low albedo of aged snow.
    reciprocal_decaytime : float
        Reciprocal decay time.
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings. The
        default is '../src/CISSEMBEL_CBindings.so'.

    Returns
    -------
    albedo : float or numpy.ndarray
        Albedo of the snow/ice column top.

    """
    if isinstance(library, ctypes.CDLL):
        fortlibrary = library
    else:
        fortlibrary = ctypes.cdll.LoadLibrary(library)

    if isinstance(snow_age, (float, int)):
        inum = 0
        # Single c-value
        c_snow_age = ctypes.c_double(snow_age)
    else:
        inum = len(snow_age)
        # Number conversion, Fortran-order
        c_snow_age = numpy.array(snow_age, order="F", dtype=float)
        # c-pointer
        c_snow_age_ptr = c_snow_age.ctypes.data_as(ctypes.POINTER(ctypes.c_double))

    c_albedo_bright = ctypes.c_double(albedo_bright)
    c_albedo_dark = ctypes.c_double(albedo_dark)
    c_reciprocal_decaytime = ctypes.c_double(reciprocal_decaytime)

    if inum:
        # array
        albedo = numpy.zeros((inum), order="F", dtype=float)
        albedo_ptr = albedo.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        fortlibrary.c_albedo_time.argtypes = [ctypes.c_int,
                                              ctypes.POINTER(ctypes.c_double),
                                              ctypes.c_double,
                                              ctypes.c_double,
                                              ctypes.c_double,
                                              ctypes.POINTER(ctypes.c_double)]
        fortlibrary.c_albedo_time(ctypes.c_int(inum),
                                  c_snow_age_ptr,
                                  c_albedo_bright,
                                  c_albedo_dark,
                                  c_reciprocal_decaytime,
                                  albedo_ptr)
    else:
        # skalar
        fortlibrary.albedo_time.restype = ctypes.c_double
        albedo = fortlibrary.albedo_time(ctypes.byref(c_snow_age),
                                         ctypes.byref(c_albedo_bright),
                                         ctypes.byref(c_albedo_dark),
                                         ctypes.byref(c_reciprocal_decaytime))
    return albedo


def albedo_snowd_exp(snow_depth, albedo_top, albedo_below,
                     reciprocal_snow_decaylength,
                     library='../src/CISSEMBEL_CBindings.so'):
    """
    C-binding to Fortran function albedo_snowd_exp in mod_albedo in CISSEMBEL
    Snow albedo based on elapsed time since last snow event and snow depth.

    Parameters
    ----------
    snow_depth : float or numpy.ndarray
        Snow depth.
    albedo_top : float
        Albedo of top snow layer.
    albedo_below : float
        Albedo of snow layer below.
    reciprocal_snow_decaylength : float
        Reciprocal decay length.
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings. The
        default is '../src/CISSEMBEL_CBindings.so'.

    Returns
    -------
    albedo : float or numpy.ndarray
        Albedo of the snow/ice column top.

    """
    if isinstance(library, ctypes.CDLL):
        fortlibrary = library
    else:
        fortlibrary = ctypes.cdll.LoadLibrary(library)

    if isinstance(snow_depth, (float, int)):
        inum = 0
        # Single c-value
        c_snow_depth = ctypes.c_double(snow_depth)
        c_albedo_top = ctypes.c_double(albedo_top)
        c_albedo_below = ctypes.c_double(albedo_below)
    else:
        inum = len(snow_depth)
        # Number conversion, Fortran-order
        c_snow_depth = numpy.array(snow_depth, order="F", dtype=float)
        c_albedo_top = numpy.array(albedo_top, order="F", dtype=float)
        c_albedo_below = numpy.array(albedo_below, order="F", dtype=float)
        # c-pointer
        c_snow_depth_ptr = c_snow_depth.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_albedo_top_ptr = c_albedo_top.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_albedo_below_ptr = c_albedo_below.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        # Field sizes
        m4c.compare_field2shapes(snow_depth.shape, albedo_top.shape,
                                 'snow_depth', 'albedo_top')
        m4c.compare_field2shapes(albedo_top.shape, albedo_below.shape,
                                 'albedo_top', 'albedo_below')

    c_reciprocal_snow_decaylength = ctypes.c_double(reciprocal_snow_decaylength)

    if inum:
        # array
        albedo = numpy.zeros((inum), order="F", dtype=float)
        albedo_ptr = albedo.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        fortlibrary.c_albedo_snowd_exp.argtypes = [ctypes.c_int,
                                                   ctypes.POINTER(ctypes.c_double),
                                                   ctypes.POINTER(ctypes.c_double),
                                                   ctypes.POINTER(ctypes.c_double),
                                                   ctypes.c_double,
                                                   ctypes.POINTER(ctypes.c_double)]
        fortlibrary.c_albedo_snowd_exp(ctypes.c_int(inum),
                                       c_snow_depth_ptr,
                                       c_albedo_top_ptr,
                                       c_albedo_below_ptr,
                                       c_reciprocal_snow_decaylength,
                                       albedo_ptr)
    else:
        # skalar
        fortlibrary.albedo_snowd_exp.restype = ctypes.c_double
        albedo = fortlibrary.albedo_snowd_exp(ctypes.byref(c_snow_depth),
                                              ctypes.byref(c_albedo_top),
                                              ctypes.byref(c_albedo_below),
                                              ctypes.byref(c_reciprocal_snow_decaylength))
    return albedo


def albedo_time_snowdepth(snow_age, snow_depth, albedo_freshsnow, albedo_firn,
                          albedo_ice, reciprocal_snow_decaytime,
                          library='../src/CISSEMBEL_CBindings.so'):
    """
    C-binding to Fortran function albedo_time_snowdepth in mod_albedo in CISSEMBEL
    Snow albedo based on elapsed time since last snow event and snow depth.

    Parameters
    ----------
    snow_age : float or numpy.ndarray
        Elapsed time since the last snow event.
    snow_depth : float or numpy.ndarray
        Snow depth.
    albedo_freshsnow : float
        Albedo of fresh snow.
    albedo_firn : float
        Albedo of firn.
    albedo_ice : float
        Albedo of ice.
    reciprocal_snow_decaytime : float
        Reciprocal decay time.
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings. The
        default is '../src/CISSEMBEL_CBindings.so'.

    Returns
    -------
    albedo : float or numpy.ndarray
        Albedo of the snow/ice column top.

    """
    if isinstance(library, ctypes.CDLL):
        fortlibrary = library
    else:
        fortlibrary = ctypes.cdll.LoadLibrary(library)

    if isinstance(snow_age, (float, int)):
        inum = 0
        # Single c-value
        c_snow_age = ctypes.c_double(snow_age)
        c_snow_depth = ctypes.c_double(snow_depth)
    else:
        inum = len(snow_age)
        # Number conversion, Fortran-order
        c_snow_age = numpy.array(snow_age, order="F", dtype=float)
        c_snow_depth = numpy.array(snow_depth, order="F", dtype=float)
        # c-pointer
        c_snow_age_ptr = c_snow_age.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_snow_depth_ptr = c_snow_depth.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        # Field sizes
        m4c.compare_field2shapes(snow_age.shape, snow_depth.shape,
                                 'snow_age', 'snow_depth')

    c_albedo_freshsnow = ctypes.c_double(albedo_freshsnow)
    c_albedo_firn = ctypes.c_double(albedo_firn)
    c_albedo_ice = ctypes.c_double(albedo_ice)
    c_reciprocal_snow_decaytime = ctypes.c_double(reciprocal_snow_decaytime)

    if inum:
        # array
        albedo = numpy.zeros((inum), order="F", dtype=float)
        albedo_ptr = albedo.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        fortlibrary.c_albedo_time_snowdepth.argtypes = [ctypes.c_int,
                                              ctypes.POINTER(ctypes.c_double),
                                              ctypes.POINTER(ctypes.c_double),
                                              ctypes.c_double,
                                              ctypes.c_double,
                                              ctypes.c_double,
                                              ctypes.c_double,
                                              ctypes.POINTER(ctypes.c_double)]
        fortlibrary.c_albedo_time_snowdepth(ctypes.c_int(inum),
                                            c_snow_age_ptr,
                                            c_snow_depth_ptr,
                                            c_albedo_freshsnow,
                                            c_albedo_firn,
                                            c_albedo_ice,
                                            c_reciprocal_snow_decaytime,
                                            albedo_ptr)
    else:
        # skalar
        fortlibrary.albedo_time_snowdepth.restype = ctypes.c_double
        albedo = fortlibrary.albedo_time_snowdepth(ctypes.byref(c_snow_age),
                                                   ctypes.byref(c_snow_depth),
                                                   ctypes.byref(c_albedo_freshsnow),
                                                   ctypes.byref(c_albedo_firn),
                                                   ctypes.byref(c_albedo_ice),
                                                   ctypes.byref(c_reciprocal_snow_decaytime))
    return albedo


def albedo_inttime_warm(snowts, snow_depth,
                        library='../src/CISSEMBEL_CBindings.so'):
    """
    C-binding to Fortran function albedo_inttime_warm in mod_albedo in CISSEMBEL
    Snow albedo based on temperature above threshold times time since last
    snow event and snow depth.

    Parameters
    ----------
    snowts : float or numpy.ndarray
        Elapsed time since the last snow event.
    snow_depth : float or numpy.ndarray
        Snow depth.
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings. The
        default is '../src/CISSEMBEL_CBindings.so'.

    Returns
    -------
    albedo : float or numpy.ndarray
        Albedo of the snow/ice column top.

    """
    if isinstance(library, ctypes.CDLL):
        fortlibrary = library
    else:
        fortlibrary = ctypes.cdll.LoadLibrary(library)

    if isinstance(snowts, (float, int)):
        inum = 0
        # Single c-value
        c_snowts = ctypes.c_double(snowts)
        c_snow_depth = ctypes.c_double(snow_depth)
    else:
        inum = len(snowts)
        # Number conversion, Fortran-order
        c_snowts = numpy.array(snowts, order="F", dtype=float)
        c_snow_depth = numpy.array(snow_depth, order="F", dtype=float)
        # c-pointer
        c_snowts_ptr = c_snowts.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_snow_depth_ptr = c_snow_depth.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        # Field sizes
        m4c.compare_field2shapes(snowts.shape, snow_depth.shape,
                                 'snowts', 'snow_depth')

    if inum:
        # array
        albedo = numpy.zeros((inum), order="F", dtype=float)
        albedo_ptr = albedo.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        fortlibrary.c_albedo_inttime_warm.argtypes = [ctypes.c_int,
                                              ctypes.POINTER(ctypes.c_double),
                                              ctypes.POINTER(ctypes.c_double),
                                              ctypes.POINTER(ctypes.c_double)]
        fortlibrary.c_albedo_inttime_warm(ctypes.c_int(inum),
                                          c_snowts_ptr,
                                          c_snow_depth_ptr,
                                          albedo_ptr)
    else:
        # skalar
        fortlibrary.albedo_inttime_warm.restype = ctypes.c_double
        albedo = fortlibrary.albedo_inttime_warm(ctypes.byref(c_snowts),
                                                 ctypes.byref(c_snow_depth))
    return albedo


def albedo_rodehacke1(snow_age, melt_age, snowdepth, surface_rho,
                      surface_temperature, melt_mask, melt_temperature,
                      snowdepth_threshold, library='../src/CISSEMBEL_CBindings.so'):
    """
    C-binding to Fortran function albedo_rodehacke1 in mod_albedo in CISSEMBEL
    Snow albedo based on elapsed time since last snow event.

    Parameters
    ----------
    snow_age : float or numpy.ndarray
        Elapsed time since the last snow event.
    melt_age : float or numpy.ndarray
        Elapsed time since the last melt event.
    snowdepth : float or numpy.ndarray
        Snow depth.
    surface_rho : float or numpy.ndarray
        Surface density.
    surface_temperature : float or numpy.ndarray
        Surface temperature.
    melt_mask : int or numpy.ndarray
        Melting mask.
    melt_temperature : float or numpy.ndarray
        Melting temperature.
    snowdepth_threshold : float
        Snow depth threshold.
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings. The
        default is '../src/CISSEMBEL_CBindings.so'.

    Returns
    -------
    results : float or numpy.ndarray
        Albedo of the snow/ice column top.

    """
    # Field sizes
    m4c.compare_field2shapes(snow_age.shape, melt_age.shape,
                             'snow_age', 'melt_age')
    m4c.compare_field2shapes(snow_age.shape, snowdepth.shape,
                             'snow_age', 'snowdepth_age')
    m4c.compare_field2shapes(snow_age.shape, surface_rho.shape,
                             'snow_age', 'surface_rho')
    m4c.compare_field2shapes(snow_age.shape, surface_temperature.shape,
                             'snow_age', 'surface_temperature')
    m4c.compare_field2shapes(snow_age.shape, melt_mask.shape,
                             'snow_age', 'melt_mask')
    m4c.compare_field2shapes(snow_age.shape, melt_temperature.shape,
                             'snow_age', 'melt_temperature')

    results = __albedo_rodehacke123(snow_age, melt_age, snowdepth, surface_rho,
                                    surface_temperature, melt_mask,
                                    melt_temperature, snowdepth_threshold,
                                    library, 'albedo_rodehacke1')
    return results


def albedo_rodehacke2(snow_age, melt_age, snowdepth, surface_rho,
                      surface_temperature, melt_mask, melt_temperature,
                      snowdepth_threshold, library='../src/CISSEMBEL_CBindings.so'):
    """
    C-binding to Fortran function albedo_rodehacke2 in mod_albedo in CISSEMBEL
    Snow albedo based on elapsed time since last snow event and snow depth.

    Parameters
    ----------
    snow_age : float or numpy.ndarray
        Elapsed time since the last snow event.
    melt_age : float or numpy.ndarray
        Elapsed time since the last melt event.
    snowdepth : float or numpy.ndarray
        Snow depth.
    surface_rho : float or numpy.ndarray
        Surface density.
    surface_temperature : float or numpy.ndarray
        Surface temperature.
    melt_mask : int or numpy.ndarray
        Melting mask.
    melt_temperature : float or numpy.ndarray
        Melting temperature.
    snowdepth_threshold : float
        Snow depth threshold.
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings. The
        default is '../src/CISSEMBEL_CBindings.so'.

    Returns
    -------
    results : float or numpy.ndarray
        Albedo of the snow/ice column top.

    """
    # Field sizes
    m4c.compare_field2shapes(snow_age.shape, melt_age.shape,
                             'snow_age', 'melt_age')
    m4c.compare_field2shapes(snow_age.shape, snowdepth.shape,
                             'snow_age', 'snowdepth_age')
    m4c.compare_field2shapes(snow_age.shape, surface_rho.shape,
                             'snow_age', 'surface_rho')
    m4c.compare_field2shapes(snow_age.shape, surface_temperature.shape,
                             'snow_age', 'surface_temperature')
    m4c.compare_field2shapes(snow_age.shape, melt_mask.shape,
                             'snow_age', 'melt_mask')
    m4c.compare_field2shapes(snow_age.shape, melt_temperature.shape,
                             'snow_age', 'melt_temperature')

    results = __albedo_rodehacke123(snow_age, melt_age, snowdepth, surface_rho,
                                    surface_temperature, melt_mask,
                                    melt_temperature, snowdepth_threshold,
                                    library, 'albedo_rodehacke2')
    return results


def albedo_rodehacke3(snow_age, melt_age, snowdepth, surface_rho,
                      surface_temperature, melt_mask, melt_temperature,
                      snowdepth_threshold, library='../src/CISSEMBEL_CBindings.so'):
    """
    C-binding to Fortran function albedo_rodehacke3 in mod_albedo in CISSEMBEL
    Snow albedo based on elapsed time since last snow event and snow depth.

    Parameters
    ----------
    snow_age : float or numpy.ndarray
        Elapsed time since the last snow event.
    melt_age : float or numpy.ndarray
        Elapsed time since the last melt event.
    snowdepth : float or numpy.ndarray
        Snow depth.
    surface_rho : float or numpy.ndarray
        Surface density.
    surface_temperature : float or numpy.ndarray
        Surface temperature.
    melt_mask : int or numpy.ndarray
        Melting mask.
    melt_temperature : float or numpy.ndarray
        Melting temperature.
    snowdepth_threshold : float
        Snow depth threshold.
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings. The
        default is '../src/CISSEMBEL_CBindings.so'.

    Returns
    -------
    results : float or numpy.ndarray
        Albedo of the snow/ice column top.

    """
    # Field sizes
    m4c.compare_field2shapes(snow_age.shape, melt_age.shape,
                             'snow_age', 'melt_age')
    m4c.compare_field2shapes(snow_age.shape, snowdepth.shape,
                             'snow_age', 'snowdepth_age')
    m4c.compare_field2shapes(snow_age.shape, surface_rho.shape,
                             'snow_age', 'surface_rho')
    m4c.compare_field2shapes(snow_age.shape, surface_temperature.shape,
                             'snow_age', 'surface_temperature')
    m4c.compare_field2shapes(snow_age.shape, melt_mask.shape,
                             'snow_age', 'melt_mask')
    m4c.compare_field2shapes(snow_age.shape, melt_temperature.shape,
                             'snow_age', 'melt_temperature')

    results = __albedo_rodehacke123(snow_age, melt_age, snowdepth, surface_rho,
                                    surface_temperature, melt_mask,
                                    melt_temperature, snowdepth_threshold,
                                    library, 'albedo_rodehacke3')
    return results


def __albedo_rodehacke123(snow_age, melt_age, snowdepth, surface_rho,
                          surface_temperature, melt_mask, melt_temperature,
                          snowdepth_threshold,
                          library='../src/CISSEMBEL_CBindings.so',
                          albedo_scheme='albedo_rodehacke1'):
    """
    C-binding to Fortran function albedo_rodehacke1, albedo_rodehacke2, and
    albedo_rodehacke3 in mod_albedo in CISSEMBEL
    Snow albedo based on elapsed time since last snow event and snow depth.

    Parameters
    ----------
    snow_age : float or numpy.ndarray
        Elapsed time since the last snow event.
    melt_age : float or numpy.ndarray
        Elapsed time since the last melt event.
    snowdepth : float or numpy.ndarray
        Snow depth.
    surface_rho : float or numpy.ndarray
        Surface density.
    surface_temperature : float or numpy.ndarray
        Surface temperature.
    melt_mask : int or numpy.ndarray
        Melting mask.
    melt_temperature : float or numpy.ndarray
        Melting temperature.
    snowdepth_threshold : float
        Snow depth threshold.
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings. The
        default is '../src/CISSEMBEL_CBindings.so'.
    albedo_scheme : str
        Name of the requested

    Returns
    -------
    albedo_rodehacke : float or numpy.ndarray
        Albedo of the snow/ice column top.

    """
    if isinstance(library, ctypes.CDLL):
        fortlibrary = library
    else:
        fortlibrary = ctypes.cdll.LoadLibrary(library)

    if isinstance(snow_age, (float, int)):
        inum = 0
        # Single c-value
        c_snow_age = ctypes.c_double(snow_age)
        c_melt_age = ctypes.c_double(melt_age)
        c_snowdepth = ctypes.c_double(snowdepth)
        c_surface_rho = ctypes.c_double(surface_rho)
        c_surface_temperature = ctypes.c_double(surface_temperature)
        c_melt_mask = ctypes.c_int(melt_mask)
        c_melt_temperature = ctypes.c_double(melt_temperature)
    else:
        inum = len(snow_age)
        # Number conversion, Fortran-order
        c_snow_age = numpy.array(snow_age, order="F", dtype=float)
        c_melt_age = numpy.array(melt_age, order="F", dtype=float)
        c_snowdepth = numpy.array(snowdepth, order="F", dtype=float)
        c_surface_rho = numpy.array(surface_rho, order="F", dtype=float)
        c_surface_temperature = numpy.array(surface_temperature, order="F", dtype=float)
        c_melt_mask = numpy.array(melt_mask, order="F", dtype=int)
        c_melt_temperature = numpy.array(melt_temperature, order="F", dtype=float)
        # c-pointer
        c_snow_age_ptr = c_snow_age.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_melt_age_ptr = c_melt_age.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_snowdepth_ptr = c_snowdepth.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_surface_rho_ptr = c_surface_rho.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_surface_temperature_ptr = c_surface_temperature.ctypes.data_as( \
            ctypes.POINTER(ctypes.c_double))
        c_melt_mask_ptr = c_melt_mask.ctypes.data_as(ctypes.POINTER(ctypes.c_int))
        c_melt_temperature_ptr = c_melt_temperature.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    c_snowdepth_threshold = ctypes.c_double(snowdepth_threshold)

    if inum:
        # array
        albedo_rodehacke = numpy.zeros((inum), order="F", dtype=float)
        albedo_rodehacke_ptr = albedo_rodehacke.ctypes.data_as(ctypes.POINTER(ctypes.c_double))

        if albedo_scheme == 'albedo_rodehacke1':
            fortlibrary.c_albedo_rodehacke1.argtypes = [ctypes.c_int,
                                                  ctypes.POINTER(ctypes.c_double),
                                                  ctypes.POINTER(ctypes.c_double),
                                                  ctypes.POINTER(ctypes.c_double),
                                                  ctypes.POINTER(ctypes.c_double),
                                                  ctypes.POINTER(ctypes.c_double),
                                                  ctypes.POINTER(ctypes.c_int),
                                                  ctypes.POINTER(ctypes.c_double),
                                                  ctypes.c_double,
                                                  ctypes.POINTER(ctypes.c_double)]
            fortlibrary.c_albedo_rodehacke1(ctypes.c_int(inum),
                                            c_snow_age_ptr,
                                            c_melt_age_ptr,
                                            c_snowdepth_ptr,
                                            c_surface_rho_ptr,
                                            c_surface_temperature_ptr,
                                            c_melt_mask_ptr,
                                            c_melt_temperature_ptr,
                                            c_snowdepth_threshold,
                                            albedo_rodehacke_ptr)
        elif albedo_scheme == 'albedo_rodehacke2':
            fortlibrary.c_albedo_rodehacke2.argtypes = [ctypes.c_int,
                                                  ctypes.POINTER(ctypes.c_double),
                                                  ctypes.POINTER(ctypes.c_double),
                                                  ctypes.POINTER(ctypes.c_double),
                                                  ctypes.POINTER(ctypes.c_double),
                                                  ctypes.POINTER(ctypes.c_double),
                                                  ctypes.POINTER(ctypes.c_int),
                                                  ctypes.POINTER(ctypes.c_double),
                                                  ctypes.c_double,
                                                  ctypes.POINTER(ctypes.c_double)]
            fortlibrary.c_albedo_rodehacke2(ctypes.c_int(inum),
                                            c_snow_age_ptr,
                                            c_melt_age_ptr,
                                            c_snowdepth_ptr,
                                            c_surface_rho_ptr,
                                            c_surface_temperature_ptr,
                                            c_melt_mask_ptr,
                                            c_melt_temperature_ptr,
                                            c_snowdepth_threshold,
                                            albedo_rodehacke_ptr)
        elif albedo_scheme == 'albedo_rodehacke3':
            fortlibrary.c_albedo_rodehacke3.argtypes = [ctypes.c_int,
                                                  ctypes.POINTER(ctypes.c_double),
                                                  ctypes.POINTER(ctypes.c_double),
                                                  ctypes.POINTER(ctypes.c_double),
                                                  ctypes.POINTER(ctypes.c_double),
                                                  ctypes.POINTER(ctypes.c_double),
                                                  ctypes.POINTER(ctypes.c_int),
                                                  ctypes.POINTER(ctypes.c_double),
                                                  ctypes.c_double,
                                                  ctypes.POINTER(ctypes.c_double)]
            fortlibrary.c_albedo_rodehacke3(ctypes.c_int(inum),
                                            c_snow_age_ptr,
                                            c_melt_age_ptr,
                                            c_snowdepth_ptr,
                                            c_surface_rho_ptr,
                                            c_surface_temperature_ptr,
                                            c_melt_mask_ptr,
                                            c_melt_temperature_ptr,
                                            c_snowdepth_threshold,
                                            albedo_rodehacke_ptr)
    else:
        # skalar
        if albedo_scheme == 'albedo_rodehacke1':
            fortlibrary.albedo_rodehacke1.restype = ctypes.c_double
            albedo_rodehacke = fortlibrary.albedo_rodehacke1(
                ctypes.byref(c_snow_age),
                ctypes.byref(c_melt_age),
                ctypes.byref(c_snowdepth),
                ctypes.byref(c_surface_rho),
                ctypes.byref(c_surface_temperature),
                ctypes.byref(c_melt_mask),
                ctypes.byref(c_melt_temperature),
                ctypes.byref(c_snowdepth_threshold))
        elif albedo_scheme == 'albedo_rodehacke2':
            fortlibrary.albedo_rodehacke2.restype = ctypes.c_double
            albedo_rodehacke = fortlibrary.albedo_rodehacke2(
                ctypes.byref(c_snow_age),
                ctypes.byref(c_melt_age),
                ctypes.byref(c_snowdepth),
                ctypes.byref(c_surface_rho),
                ctypes.byref(c_surface_temperature),
                ctypes.byref(c_melt_mask),
                ctypes.byref(c_melt_temperature),
                ctypes.byref(c_snowdepth_threshold))
        elif albedo_scheme == 'albedo_rodehacke3':
            fortlibrary.albedo_rodehacke3.restype = ctypes.c_double
            albedo_rodehacke = fortlibrary.albedo_rodehacke3(
                ctypes.byref(c_snow_age),
                ctypes.byref(c_melt_age),
                ctypes.byref(c_snowdepth),
                ctypes.byref(c_surface_rho),
                ctypes.byref(c_surface_temperature),
                ctypes.byref(c_melt_mask),
                ctypes.byref(c_melt_temperature),
                ctypes.byref(c_snowdepth_threshold))
    return albedo_rodehacke

def albedo_ecearth1(surface_temperature, melt_mask, snowfall, melt_temperature,
                    recip_decaytime, dtime, albedo_old, trans_temperature,
                    library='../src/CISSEMBEL_CBindings.so'):
    """
    C-binding to Fortran function albedo_ecearth1 in mod_albedo in CISSEMBEL
    Snow albedo based on elapsed time since last snow event, and albedo for
    melting and refreezing.

    Parameters
    ----------
    surface_temperature : float or numpy.ndarray
        Surface temperature.
    melt_mask : int or numpy.ndarray
        Melting mask.
    snowfall : float or numpy.ndarray
        Elapsed time since the last snow event.
    melt_temperature : float or numpy.ndarray
        Melting temperature.
    recip_decaytime : float
        Reciprocal decay time.
    dtime : float or numpy.ndarray
        Time step width
    albedo_old : float
        Light albedo, e.g., for fresh snow.
    trans_temperature : float or numpy.ndarray
        Transiiton temperature of the snow/ice.
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings. The
        default is '../src/CISSEMBEL_CBindings.so'.

    Returns
    -------
    albedo : float or numpy.ndarray
        Albedo of the snow/ice column top.

    """
    if isinstance(library, ctypes.CDLL):
        fortlibrary = library
    else:
        fortlibrary = ctypes.cdll.LoadLibrary(library)

    if isinstance(surface_temperature, (float, int)):
        inum = 0
        # Single c-value
        c_surface_temperature = ctypes.c_double(surface_temperature)
        c_melt_mask = ctypes.c_int(melt_mask)
        c_melt_temperature = ctypes.c_double(melt_temperature)
        c_snowfall = ctypes.c_double(snowfall)
        c_recip_decaytime = ctypes.c_double(recip_decaytime)
        c_dtime = ctypes.c_double(dtime)
        c_albedo_old = ctypes.c_double(albedo_old)
        c_trans_temperature = ctypes.c_double(trans_temperature)
    else:
        inum = int(len(surface_temperature))
        c_surface_temperature = numpy.array(surface_temperature, order="F",
                                            dtype=float)# Number conversion, Fortran-order
        c_melt_mask = numpy.array(melt_mask, order="F", dtype=int)
        c_melt_temperature = numpy.array(melt_temperature, order="F",
                                         dtype=float)
        c_snowfall = numpy.array(snowfall, order="F", dtype=float)
        c_recip_decaytime = numpy.array(recip_decaytime,
                                        order="F", dtype=float)
        c_dtime = numpy.array(dtime, order="F", dtype=float)
        c_albedo_old = numpy.array(albedo_old, order="F", dtype=float)
        c_trans_temperature = numpy.array(trans_temperature, order="F", dtype=float)
        # c-pointer
        c_surface_temperature_ptr = c_surface_temperature.ctypes.data_as( \
            ctypes.POINTER(ctypes.c_double))
        c_melt_mask_ptr = c_melt_mask.ctypes.data_as(ctypes.POINTER(ctypes.c_int))
        c_melt_temperature_ptr = c_melt_temperature.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_snowfall_ptr = c_snowfall.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_recip_decaytime_ptr = c_recip_decaytime.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_dtime_ptr = c_dtime.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_albedo_old_ptr = c_albedo_old.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_trans_temperature_ptr = c_trans_temperature.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        # Field sizes
        m4c.compare_field2shapes(snowfall.shape, surface_temperature.shape,
                                 'snowfall', 'surface_temperature')
        m4c.compare_field2shapes(snowfall.shape, melt_mask.shape,
                                 'snowfall', 'melt_mask')
        m4c.compare_field2shapes(snowfall.shape, melt_temperature.shape,
                                 'snowfall', 'melt_temperature')
        m4c.compare_field2shapes(snowfall.shape, surface_temperature.shape,
                                 'snowfall', 'recip_decaytime')
        m4c.compare_field2shapes(snowfall.shape, surface_temperature.shape,
                                 'snowfall', 'dtime')
        m4c.compare_field2shapes(snowfall.shape, surface_temperature.shape,
                                 'snowfall', 'albedo_old')
        m4c.compare_field2shapes(snowfall.shape, surface_temperature.shape,
                                 'snowfall', 'trans_temperature')
    if inum:
        # array
        albedo = numpy.zeros((inum), order="F", dtype=float)
        albedo_ptr = albedo.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        fortlibrary.c_albedo_ecearth1.argtypes = [
            ctypes.c_int,
            ctypes.POINTER(ctypes.c_double),
            ctypes.POINTER(ctypes.c_int),
            ctypes.POINTER(ctypes.c_double),
            ctypes.POINTER(ctypes.c_double),
            ctypes.POINTER(ctypes.c_double),
            ctypes.POINTER(ctypes.c_double),
            ctypes.POINTER(ctypes.c_double),
            ctypes.POINTER(ctypes.c_double),
            ctypes.POINTER(ctypes.c_double),

            ctypes.POINTER(ctypes.c_double)]
        fortlibrary.c_albedo_ecearth1(ctypes.c_int(inum),
                                      c_surface_temperature_ptr,
                                      c_melt_mask_ptr,
                                      c_snowfall_ptr,
                                      c_melt_temperature_ptr,
                                      c_recip_decaytime_ptr,
                                      c_dtime_ptr,
                                      c_albedo_old_ptr,
                                      c_trans_temperature_ptr,
                                      albedo_ptr)
    else:
        # skalar
        fortlibrary.albedo_ecearth1.restype = ctypes.c_double
        albedo = \
            fortlibrary.albedo_ecearth1(ctypes.byref(c_surface_temperature),
                                        ctypes.byref(c_melt_mask),
                                        ctypes.byref(c_snowfall),
                                        ctypes.byref(c_melt_temperature),
                                        ctypes.byref(c_recip_decaytime),
                                        ctypes.byref(c_dtime),
                                        ctypes.byref(c_albedo_old),
                                        ctypes.byref(c_trans_temperature))
    return albedo

def albedo_time_melt_freeze(snow_age, albedo_light, albedo_dark,
                            albedo_refrz, albedo_melt,
                            reciprocal_snow_decaytime, surface_temperature,
                            melt_temperature, melt_mask,
                            library='../src/CISSEMBEL_CBindings.so'):
    """
    C-binding to Fortran function albedo_time_melt_freeze in mod_albedo in CISSEMBEL
    Snow albedo based on elapsed time since last snow event, and albedo for
    melting and refreezing.

    Parameters
    ----------
    snow_age : float or numpy.ndarray
        Elapsed time since the last snow event.
    albedo_light : float
        Light albedo, e.g., for fresh snow.
    albedo_dark : float
        Dark albedo, e.g., for firn.
    albedo_refrz : float
        Albedo of refrozen snow and ice.
    albedo_melt : float
        Albedo of melting snow and ice.
    reciprocal_snow_decaytime : float
        Reciprocal decay time.
    surface_temperature : float or numpy.ndarray
        Surface temperature.
    melt_temperature : float or numpy.ndarray
        Melting temperature.
    melt_mask : int or numpy.ndarray
        Melting mask.
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings. The
        default is '../src/CISSEMBEL_CBindings.so'.

    Returns
    -------
    albedo : float or numpy.ndarray
        Albedo of the snow/ice column top.

    """
    if isinstance(library, ctypes.CDLL):
        fortlibrary = library
    else:
        fortlibrary = ctypes.cdll.LoadLibrary(library)

    if isinstance(snow_age, (float, int)):
        inum = 0
        # Single c-value
        c_snow_age = ctypes.c_double(snow_age)
        c_albedo_light = ctypes.c_double(albedo_light)
        c_albedo_dark = ctypes.c_double(albedo_dark)
        c_albedo_refrz = ctypes.c_double(albedo_refrz)
        c_albedo_melt = ctypes.c_double(albedo_melt)
        c_reciprocal_snow_decaytime = ctypes.c_double(reciprocal_snow_decaytime)
        c_surface_temperature = ctypes.c_double(surface_temperature)
        c_melt_temperature = ctypes.c_double(melt_temperature)
        c_melt_mask = ctypes.c_int(melt_mask)
    else:
        inum = int(len(snow_age))
        # Number conversion, Fortran-order
        c_snow_age = numpy.array(snow_age, order="F", dtype=float)
        c_albedo_light = numpy.array(albedo_light, order="F", dtype=float)
        c_albedo_dark = numpy.array(albedo_dark, order="F", dtype=float)
        c_albedo_refrz = numpy.array(albedo_refrz, order="F", dtype=float)
        c_albedo_melt = numpy.array(albedo_melt, order="F", dtype=float)
        c_reciprocal_snow_decaytime = numpy.array(reciprocal_snow_decaytime,
                                                  order="F", dtype=float)
        c_surface_temperature = numpy.array(surface_temperature, order="F",
                                            dtype=float)
        c_melt_temperature = numpy.array(melt_temperature, order="F",
                                         dtype=float)
        c_melt_mask = numpy.array(melt_mask, order="F", dtype=int)
        # c-pointer
        c_snow_age_ptr = c_snow_age.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_albedo_light_ptr = c_albedo_light.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_albedo_dark_ptr = c_albedo_dark.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_albedo_refrz_ptr = c_albedo_refrz.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_albedo_melt_ptr = c_albedo_melt.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_reciprocal_snow_decaytime_ptr = c_reciprocal_snow_decaytime.ctypes.data_as( \
            ctypes.POINTER(ctypes.c_double))
        c_surface_temperature_ptr = c_surface_temperature.ctypes.data_as( \
            ctypes.POINTER(ctypes.c_double))
        c_melt_temperature_ptr = c_melt_temperature.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_melt_mask_ptr = c_melt_mask.ctypes.data_as(ctypes.POINTER(ctypes.c_int))
        # Field sizes
        m4c.compare_field2shapes(snow_age.shape, surface_temperature.shape,
                                 'snow_age', 'surface_temperature')
        m4c.compare_field2shapes(snow_age.shape, melt_temperature.shape,
                                 'snow_age', 'melt_temperature')
        m4c.compare_field2shapes(snow_age.shape, melt_mask.shape,
                                 'snow_age', 'melt_mask')
    if inum:
        # array
        albedo = numpy.zeros((inum), order="F", dtype=float)
        albedo_ptr = albedo.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        fortlibrary.c_albedo_time_melt_freeze.argtypes = [
            ctypes.c_int,
            ctypes.POINTER(ctypes.c_double),
            ctypes.POINTER(ctypes.c_double),
            ctypes.POINTER(ctypes.c_double),
            ctypes.POINTER(ctypes.c_double),
            ctypes.POINTER(ctypes.c_double),
            ctypes.POINTER(ctypes.c_double),
            ctypes.POINTER(ctypes.c_double),
            ctypes.POINTER(ctypes.c_double),
            ctypes.POINTER(ctypes.c_int),
            ctypes.POINTER(ctypes.c_double)]
        fortlibrary.c_albedo_time_melt_freeze(ctypes.c_int(inum),
                                              c_snow_age_ptr,
                                              c_albedo_light_ptr,
                                              c_albedo_dark_ptr,
                                              c_albedo_refrz_ptr,
                                              c_albedo_melt_ptr,
                                              c_reciprocal_snow_decaytime_ptr,
                                              c_surface_temperature_ptr,
                                              c_melt_temperature_ptr,
                                              c_melt_mask_ptr,
                                              albedo_ptr)
    else:
        # skalar
        fortlibrary.albedo_time_melt_freeze.restype = ctypes.c_double
        albedo = \
            fortlibrary.albedo_time_melt_freeze(ctypes.byref(c_snow_age),
                                                ctypes.byref(c_albedo_light),
                                                ctypes.byref(c_albedo_dark),
                                                ctypes.byref(c_albedo_refrz),
                                                ctypes.byref(c_albedo_melt),
                                                ctypes.byref(c_reciprocal_snow_decaytime),
                                                ctypes.byref(c_surface_temperature),
                                                ctypes.byref(c_melt_temperature),
                                                ctypes.byref(c_melt_mask),
                                                ctypes.byref(c_reciprocal_snow_decaytime))
    return albedo


def albedo_density(snow_rho, albedo_upper_threshold, q1, q2,
                   library='../src/CISSEMBEL_CBindings.so'):
    """
    C-binding to Fortran function albedo_density in mod_albedo in CISSEMBEL
    Snow albedo dependent on snow density

    Parameters
    ----------
    snow_rho : float or numpy.ndarray
        Snow density (kg m-3).
    albedo_upper_threshold : float or numpy.ndarray
        Upper albedo threshold (1).
    q1 : float or numpy.ndarray
        axis inclination (m3 kg-1).
    q2 : float or numpy.ndarray
        axis interception (1).

    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings. The
        default is '../src/CISSEMBEL_CBindings.so'.

    Returns
    -------
    albedo : float or numpy.ndarray
        Snow-density dependent albedo.

    """
    if isinstance(library, ctypes.CDLL):
        fortlibrary = library
    else:
        fortlibrary = ctypes.cdll.LoadLibrary(library)

    if isinstance(snow_rho, (float, int)):
        inum = 0
        # Single c-value
        c_snow_rho = ctypes.c_double(snow_rho)
        c_albedo_upper_threshold = ctypes.c_double(albedo_upper_threshold)
        c_q1 = ctypes.c_double(q1)
        c_q2 = ctypes.c_double(q2)
    else:
        inum = len(snow_rho)
        # Number conversion, Fortran-order
        c_snow_rho = numpy.array(snow_rho, order="F", dtype=float)
        c_albedo_upper_threshold = numpy.array(albedo_upper_threshold, order="F", dtype=float)
        c_q1 = numpy.array(q1, order="F", dtype=float)
        c_q2 = numpy.array(q2, order="F", dtype=float)
        # c-pointer
        c_snow_rho_ptr = c_snow_rho.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_albedo_upper_threshold_ptr = \
            c_albedo_upper_threshold.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_q1_ptr = c_q1.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_q2_ptr = c_q2.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    if inum:
        # array
        albedo = numpy.zeros((inum), order="F", dtype=float)
        albedo_ptr = albedo.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        fortlibrary.c_albedo_density.argtypes = \
            [ctypes.c_int,
             ctypes.POINTER(ctypes.c_double),
             ctypes.POINTER(ctypes.c_double),
             ctypes.POINTER(ctypes.c_double),
             ctypes.POINTER(ctypes.c_double),
             ctypes.POINTER(ctypes.c_double)]
        fortlibrary.c_albedo_density(ctypes.c_int(inum),
                                     c_snow_rho_ptr,
                                     c_albedo_upper_threshold_ptr,
                                     c_q1_ptr,
                                     c_q2_ptr,
                                     albedo_ptr)
    else:
        # skalar
        fortlibrary.albedo_density.restype = ctypes.c_double
        albedo = fortlibrary.albedo_density(ctypes.byref(c_snow_rho),
                                            ctypes.byref(c_albedo_upper_threshold),
                                            ctypes.byref(c_q1),
                                            ctypes.byref(c_q2))
    return albedo


def albedo_cloud_shift(abledo_in, cloudcover,
                       library='../src/CISSEMBEL_CBindings.so'):
    """
    C-binding to Fortran function albedo_cloud_shift in mod_albedo in CISSEMBEL
    Cloud cover related shift of the broad-band albedo.

    Parameters
    ----------
    abledo_in : float or numpy.ndarray
        Albedo before the cloud cover shift is applied.
    cloudcover : float or numpy.ndarray
        Cloud cover fraction.
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings. The
        default is '../src/CISSEMBEL_CBindings.so'.

    Returns
    -------
    albedo : float or numpy.ndarray
        Albedo of the snow/ice column top.

    """
    if isinstance(library, ctypes.CDLL):
        fortlibrary = library
    else:
        fortlibrary = ctypes.cdll.LoadLibrary(library)

    if isinstance(abledo_in, (float, int)):
        inum = 0
        # Single c-value
        c_abledo_in = ctypes.c_double(abledo_in)
        c_cloudcover = ctypes.c_double(cloudcover)
    else:
        inum = len(abledo_in)
        # Number conversion, Fortran-order
        c_abledo_in = numpy.array(abledo_in, order="F", dtype=float)
        c_cloudcover = numpy.array(cloudcover, order="F", dtype=float)
        # c-pointer
        c_abledo_in_ptr = c_abledo_in.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_cloudcover_ptr = c_cloudcover.ctypes.data_as(ctypes.POINTER(ctypes.c_double))

    if inum:
        # array
        albedo = numpy.zeros((inum), order="F", dtype=float)
        albedo_ptr = albedo.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        fortlibrary.c_albedo_cloud_shift.argtypes = \
            [ctypes.c_int,
             ctypes.POINTER(ctypes.c_double),
             ctypes.POINTER(ctypes.c_double),
             ctypes.POINTER(ctypes.c_double)]
        fortlibrary.c_albedo_cloud_shift(ctypes.c_int(inum),
                                         c_abledo_in_ptr,
                                         c_cloudcover_ptr,
                                         albedo_ptr)
    else:
        # skalar
        fortlibrary.albedo_cloud_shift.restype = ctypes.c_double
        albedo = fortlibrary.albedo_cloud_shift(ctypes.byref(c_abledo_in),
                                                ctypes.byref(c_cloudcover))
    return albedo



# -----------------------------------------------------------
#
# Main program
#
if __name__ == '__main__':
    import ctypes
    import numpy
    import values_mod_param as physical
    import module4cbind as m4c

    #
    # Example code
    #
    print('Load physical constant')
    physical.constant()
    RHO_ICE = physical.constant.rho_ice
    RHO_SNOW = physical.constant.rho_snow
    CDENSITY = physical.constant.Cdens

    #
    # Load the shared library
    #
    LIBRARY_NAME = '../src/CISSEMBEL_CBindings.so' # INCLUDING path, e.g., ./

    print('Load shared library containing C-bindings of Fortran code "'
          +LIBRARY_NAME+'"')
    FORTLIBRARY = ctypes.cdll.LoadLibrary(LIBRARY_NAME)
else:
    import ctypes
    import numpy
    import module4cbind as m4c

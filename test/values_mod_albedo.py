#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov  9 13:43:17 2021

-- Some variables and parameters of mod_albedo.F08

@author: Christian Rodehacke, DMI Copenhagen, Denmark

@copyright: Copyright (C) 2016-2025 Christian Rodehacke
"""
# -------------------------------------------------------------------
# This file is part of CISSEMBEL, which is the
# == Copenhagen Ice-Snow Surface Energy and Mass Balance modEL ==
#
# CISSEMBEL is free software; you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE  v.1.2
#
# Information about the EUROPEAN UNION PUBLIC LICENCE (*EUPL*) and
# translations into other European languages are available at
# https://eupl.eu (last access: 2023-09-18)](https://eupl.eu/)
#
# We hope that  this  distributed  CISSEMBEL  code, its  tools and
# documentations are usefull. Any improvements of the code, tools,
# and documentations tools are  very welcome.  Please, consider to
# share your thoughts and improvements with the community.
#
# The documentation,  tools,  and the code are provided as it are.
# NO WARRANTY  ABOUT FITNESS  AND USABILITY OF THE CODE, TOOLS, OR
# DOCUMENTATION  ARE  GIVEN.   NO  WARRANTY  IS  GIVEN  ABOUT  THE
# CORRECTNESS  OF COMPUTED RESULTS WITH THE  PROVIDED CODE, TOOLS,
# AND DOCUMENTATION. YOU USE THE CODE, DOCUMENTATION, AND TOOLS AT
# YOUR OWN RISK.           Removing  of  any  copyright  statement
# or  this  disclaimer  is considered  as a  attempt  to defraud.
#
# -------------------------------------------------------------------

def add_this_arg(func):
    """
    Functions are objects in Python and can have arbitrary attributes assigned to them.

    A generic decorator that adds a this argument to each call to the decorated
    function. This additional argument will give functions a way to reference
    themselves without needing to explicitly embed (hardcode) their name into
    the rest of the definition and is similar to the instance argument that
    class methods automatically receive as their first argument which is
    usually named `self`. You may a different one to avoid confusion, such as
    `this`.

    Parameters
    ----------
    func : function
        function call.

    Returns
    -------
    function
        decorated function call.

    """
    def wrapped(*args, **kwargs):
        return func(wrapped, *args, **kwargs)
    return wrapped

@add_this_arg
def constant(this=None):
    '''
    Define various physical constants

    Parameters
    ----------
    this : TYPE
        DESCRIPTION.

    Returns
    -------
    None.

    '''

    this.albedo_allowed_minimum = 0.10
    this.albedo_allowed_maximum = 0.95

    this.default_snow_depth_threshold = 0.05  # (m)
    this.default_snowfall_threshold = 7.23e-7 # (kg m-2 s-1)

    this.albedo_q1 = -4.e-4
    this.albedo_q2 = 0.97

    this.reciprocal_snow_decaytime = 3.858e-7    # (s-1) # 1/(30days) = 3.858e-7
    this.reciprocal_refrozen_decaytime = 2.572-7 # (s-1) # 1/(45days) = 2.572e-7

    this.snow_depth_decaylength = 0.0024 # (m)
    this.reciprocal_snow_depth_decaylength = 1.0/this.snow_depth_decaylength # (m)

    this.albedo_freshsnow = 0.85
    this.albedo_melting = 0.55
    this.trans_temperature = 2.0 # (K) transition temperature in the albedo scheme ALBEDO_TEMP
    this.albedo_firn = 0.70
    this.albedo_refrozen = 0.65
    this.albedo_ice = 0.65
    this.albedo_snow = 0.76
    this.albedo_snow_melting = 0.50
    this.albedo_snow_refrozen = 0.65
    this.albedo_ice_melting = 0.40
    this.albedo_ice_refrozen = 0.45

    this.default_temperature_transitional = 2.0

# -----------------------------------------------------------
#
# Main program
#
if __name__ == '__main__':
    constant()

#! Git version control id: $Id$

#
# -------------------------------------------------------------------
#
# Copyright (C) 2016-2025 Christian Rodehacke
#
# This file is part of CISSEMBEL, which is the
# == Copenhagen Ice-Snow Surface Energy and Mass Balance modEL ==
#
# CISSEMBEL is free software; you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE  v.1.2
#
# Information about the EUROPEAN UNION PUBLIC LICENCE (*EUPL*) and
# translations into other European languages are available at
# https://eupl.eu (last access: 2023-09-18)](https://eupl.eu/)
#
# We hope that  this  distributed  CISSEMBEL  code, its  tools and
# documentations are usefull. Any improvements of the code, tools,
# and documentations tools are  very welcome.  Please, consider to
# share your thoughts and improvements with the community.
#
# The documentation,  tools,  and the code are provided as it are.
# NO WARRANTY  ABOUT FITNESS  AND USABILITY OF THE CODE, TOOLS, OR
# DOCUMENTATION  ARE  GIVEN.   NO  WARRANTY  IS  GIVEN  ABOUT  THE
# CORRECTNESS  OF COMPUTED RESULTS WITH THE  PROVIDED CODE, TOOLS,
# AND DOCUMENTATION. YOU USE THE CODE, DOCUMENTATION, AND TOOLS AT
# YOUR OWN RISK.           Removing  of  any  copyright  statement
# or  this  disclaimer  is considered  as a  attempt  to defraud.
#
# -------------------------------------------------------------------

## @file Linux-gfortran-static.mk
#  @brief Include makefile that contain specific information for the
#    environment following the filename scheme: $(OS)-$(FORT).mk ,
#    where the compiler environment is storage in $(COMPILER_ENV). To
#    check its value on different computers run the following command:
#    make print-COMPILER_ENV
#    All different compiler specific files are located in the
#    sub-directory $(Compilers), so to ask to the final filename check
#    make print-COMPILER_INCLUDE_FILE
#
#    We set in this file
#    - Program locations for various Unix programs
#    - Path of required libraries
#    - Compiler specific flags
##

#---------------------------------------
#
# Unix programs (compiler or machine specific commands)
#
# XXX   :=

#---------------------------------------
#  SPECIAL CASE for building static executable
$(warning Environment $(COMPILER_ENV) building static executable not tested for MPI_PARALLEL)
#
# Flags
#
# flag for free format of long lines: LINE_LENGTH
LINE_LENGTH := -ffree-line-length-512
FFLAGS := -cpp -static -fPIC
FFLAGS += -march=generic # architecture

ifdef DEBUG_FLAGS
  FFLAGS += -g -O0
  FFLAGS += -Wall -fbacktrace -fcheck=all -pedantic
  FFLAGS += -ffpe-trap=invalid,zero,overflow,underflow
  FFLAGS += -finit-real=snan
  FFLAGS += -DDEBUG
else
  FFLAGS += -O2 -mtune=generic -mfpmath=sse
  ifdef PARALLEL_AUTO
    FFLAGS += -floop-parallelize-all
    $(warning "Turn on autoparallelization : set OMP_NUM_THREADS")
  endif
  FFLAGS += -DNDEBUG
endif

ifdef LARGE_MEM_FLAG
  FFLAGS += -mcmodel=medium # for data exceeding 2GB
endif
ifdef GPROFILE_FLAG
  $(warning "***** Add profiling support -pg ************")
  FFLAGS += -pg
  ifdef DEBUG_FLAGS
    FFLAGS += -fcheck-array-temporaries
  endif
endif

ifdef POS_INDEPEN_CODE
  $(warning "***** PIC: partition independent code ******")
  FFLAGS += -fPIC
endif

#
# Libaries
#
CPPFLAGS := -static-libgfortran -static-libgcc # -fintrinsic-modules-path -I/usr/include
ifdef NETCDF_SUPPORT
  #CPPFLAGS += $(shell nc-config --cflags --libs) -fintrinsic-modules-path $(shell nc-config --includedir) # plus ....
  ##CPPFLAGS += -I/usr/include -I/usr/include/hdf5/serial -L/usr/lib/x86_64-linux-gnu -L/usr/lib/x86_64-linux-gnu/hdf5/serial -lnetcdf -lnetcdff -lhdf5_hl -lhdf5 -lpthread -lsz -lz -ldl -lm -lcurl ##-fintrinsic-modules-path /usr/include #

  CPPFLAGS += -I$(HOME)/usr/local/include/ -L$(HOME)/usr/local/lib -lnetcdff -lnetcdf -lhdf5_hl -lhdf5 -lz -lsz -L/lib/x86_64-linux-gnu -ldl
endif
ifdef OPENMP_PARALLEL
  FFLAGS += -fopenmp
  # To include also the OpemMP thread library in the static code
  CPPFLAGS += -Wl,--whole-archive -lpthread -Wl,--no-whole-archive
endif
ifdef MPI_PARALLEL
  FFLAGS   += -DMPI_PARALLEL
  CPPFLAGS += -I/usr/lib/openmpi/lib
  FORT     := mpifort
  ifdef MPI_PARALLEL_OUT
    FFLAGS   += -DPARALLEL_IO
  endif
  ifdef MPI_PARALLEL
    $(error MPI_PARALLEL: Environment $(COMPILER_ENV) does not support STATIC executable)
  endif
endif

#
# -- Last lines
#

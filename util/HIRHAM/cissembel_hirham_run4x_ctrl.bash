#!/bin/bash
# Master CISSEMBEL run script: HIRHAM SMB-type simulations via binary/Fortran driver using preprocessed HIRHAM data
#
# To run CISSEMBEL simulations driven by preprocessed HIRHAM data via
# the Fortran driver a python drive/binary executable. The
# preprocessing has been done by the bash script
# `cissembel_hirham_data_preprocess.bash`. The compilation
# of the code occurs via `make` for serial code and via
# `make OPENMP_PARALLEL` for OpenMP parallelized code, for instance.

# Usage example:
# SCENARIO="CE2";YEAR0=1971;YEAR1=2100 ; nohup ./cissembel_hirham_run4x_ctrl.bash $SCENARIO $YEAR0 $YEAR1 > hirham2cissemble.simulation.$SCENARIO.log 2>&1 &
#
# (c) Christian Rodehacke, 2023, DMI Copenhagen, Denmark
#
# -------------------------------------------------------------------
# This file is part of CISSEMBEL, which is the
# == Copenhagen Ice-Snow Surface Energy and Mass Balance modEL ==
#
# CISSEMBEL is free software; you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE  v.1.2
#
# Information about the EUROPEAN UNION PUBLIC LICENCE (*EUPL*) and
# translations into other European languages are available at
# https://eupl.eu (last access: 2021-10-01)](https://eupl.eu/)
#
# We hope that  this  distributed  CISSEMBEL  code, its  tools and
# documentations are usefull. Any improvements of the code, tools,
# and documentations tools are  very welcome.  Please, consider to
# share your thoughts and improvements with the community.
#
# The documentation,  tools,  and the code are provided as it are.
# NO WARRANTY  ABOUT FITNESS  AND USABILITY OF THE CODE, TOOLS, OR
# DOCUMENTATION  ARE  GIVEN.   NO  WARRANTY  IS  GIVEN  ABOUT  THE
# CORRECTNESS  OF COMPUTED RESULTS WITH THE  PROVIDED CODE, TOOLS,
# AND DOCUMENTATION. YOU USE THE CODE, DOCUMENTATION, AND TOOLS AT
# YOUR OWN RISK.           Removing  of  any  copyright  statement
# or  this  disclaimer  is considered  as a  attempt  to defraud.
#
# -------------------------------------------------------------------
set -e

#
# Activate/Uncomment HDF5_USE_FILE_LOCKING, if you have file system issues
#
export HDF5_USE_FILE_LOCKING=FALSE

#
# OpenMP settings
#
export OMP_NUM_THREADS=24
#export OMP_SCHEDULE=dynamic # Recommanded for dynamic z-axis
export OMP_WAIT_POLICY=active # Active: Encourages idle threads to spin rather than sleep
export OMP_DYNAMIC=false     # False: Don’t let the runtime deliver fewer threads than you asked for
export OMP_PROC_BIND=true    # True: Prevents threads migrating between cores


#
# Main parameters, also controlled via command line arguments
#
SCENARIO="${1:-CE2}"
YEAR_SCENRIO_START=${2:-1971}
YEAR_SCENRIO_END=${3:-2100}

#
# Directories of the data and the running conditions
#
# The WORKING_ROOT_DIR needs enough space
#
WORKING_ROOT_DIR=${HOME}/work/HIRHAM_CISSEMBEL

CISSEMBEL_SUBDIR=cissembel
DATA_SUBDIR=data

#
# Invariant surface elevation file
#
#DIR=$HOME/work/Data/data4Models/HIRHAM/
SURFACE_ELEVATION_FILE="${WORKING_ROOT_DIR}/${SCENARIO}/${DATA_SUBDIR}/topo_geog.clean.nc"
VARNAME_elevation="orog"

GLACIER_MASK_FILE="${WORKING_ROOT_DIR}/${SCENARIO}/${DATA_SUBDIR}/glmask_geog.clean.nc"
VARNAME_GlacierMask="glacmask"

LAND_MASK_FILE="${WORKING_ROOT_DIR}/${SCENARIO}/${DATA_SUBDIR}/landsea_geog.clean.nc"
VARNAME_LandMask="landmask"

#
# CISSEMBEL specific files
#
CISSEMBEL_SOURCE_ROOT_DIR="${HOME}/src/CISSEMBEL"

#
# CISSEMBEL executable
#
#CISSEMBEL_EXE="${CISSEMBEL_SOURCE_ROOT_DIR}/src/CISSEMBEL.xSG" # Serial incl debug
#CISSEMBEL_EXE="${CISSEMBEL_SOURCE_ROOT_DIR}/src/CISSEMBEL.xOG" # OpenMP incl debug
#CISSEMBEL_EXE="${CISSEMBEL_SOURCE_ROOT_DIR}/src/CISSEMBEL.xS" # Serial
CISSEMBEL_EXE="${CISSEMBEL_SOURCE_ROOT_DIR}/src/CISSEMBEL.xO" # OpenMP

#
# CISSEMBEL input file names
INITIAL_FILE_CISSEMBEL="${CISSEMBEL_SOURCE_ROOT_DIR}/data/HIRHAM.Greenland_5km/HIRHAM_initial.Greenland.xaxis_47_347_yaxis_81_519.kaxis32.nc"
NAMELIST_FILE_CISSEMBEL="${CISSEMBEL_SOURCE_ROOT_DIR}/examples/namelists/HIRHAM.Greenland_5km/namelist.ebm.HIRHAM.Greenland"
NAMELIST_FILE_DRIVER_FOR="${CISSEMBEL_SOURCE_ROOT_DIR}/examples/namelists/HIRHAM.Greenland_5km/namelist.EBMdriver.HIRHAM.Greenland"

START_FILE_CISSEMBEL="${WORKING_ROOT_DIR}/${SCENARIO}/${DATA_SUBDIR}/HIRHAM_boot.${SCENARIO}.Greenland.xaxis_47_347_yaxis_81_519.kaxis32.nc"

#
# CISSEMBEL output file names
#
fname_suffix='nc'
fname_rst_input="restart_input.${fname_suffix}"
fname_rst_output="restart_output.${fname_suffix}"
fname_out_data="output.${fname_suffix}"
fname_dia_data="diagnost.${fname_suffix}"
fname_frc_data="forcing_out.${fname_suffix}"
fname_sta_data="station.${fname_suffix}"
fname_cpl_data="coupling4ism.${fname_suffix}"

#
# Commands
#
CDO=cdo
NCKS=ncks
NCRCAT=ncrcat
NCATTED=ncatted

# ========================================================================
#
# Subroutines, functions
#

#
# General information
#
information(){
    echo "General information"

    #
    # Reports
    #
    echo "Report"
    echo "  hostname   ${HOST:-$(hostname)}"
    echo "  date-time  $(date)"
    echo "  pwd        $(pwd)"
    echo "  User       ${USER:-Unknown}"
    echo "  SCENARIO           ${SCENARIO}"
    echo "  YEAR_SCENRIO_START ${YEAR_SCENRIO_START}"
    echo "  YEAR_SCENRIO_END   ${YEAR_SCENRIO_END}"
}

#
# Reports and checks of required input
#
checks(){
    echo "Check if the input data are available for HIRHAM simulations"

    #
    # Forcing files
    #
    echo "  Forcing files"
    istop=0
    igood=0
    imiss=0
    for year in $(seq $YEAR_SCENRIO_START $YEAR_SCENRIO_END) ; do
	# Merged data
	#todo:rm:zip_rate_merged_file=1
	MERGED_TARGET_FILE="${DATADIR}/${TARGET_DATA_FILE_HEAD}_${year}_MERGED${TARGET_DATA_FILE_TAIL}"

	echo -n "    year $year"
	if [ -f $MERGED_TARGET_FILE ] ; then
	    echo " +Good $MERGED_TARGET_FILE"
	    igood=$(( igood+1 ))
	else
	    echo " -MISS $MERGED_TARGET_FILE"
	    imiss=$(( imiss+1 ))
	fi
    done

    #
    # Statistic and report
    #
    if [ $imiss -le 0 ] ; then
	echo "  * Found all$(printf '%4d' $igood) atmospheric forcing files"
    else
	echo "  + Found    $(printf '%4d' $igood) atmospheric forcing files"
	echo "  - MISSING  $(printf '%4d' $imiss) atmospheric forcing files"
	#istop=$(( istop+1 ))
    fi
    echo

    # ------------------------------------------------------------
    #
    # Time invariant file
    #
    echo "  Time invariant files"
    igood=0
    imiss=0
    if [ -f $SURFACE_ELEVATION_FILE ] ; then
	echo "    + OK   topography file '${SURFACE_ELEVATION_FILE}'"
	igood=$(( igood+1 ))
    else
	echo "    - MISS topography file '${SURFACE_ELEVATION_FILE}'"
	imiss=$(( imiss+1 ))
    fi

    if [ -f $GLACIER_MASK_FILE ] ; then
	echo "    + OK   glac. mask file '${GLACIER_MASK_FILE}'"
	igood=$(( igood+1 ))
    else
	echo "    - MISS glac. mask file '${GLACIER_MASK_FILE}'"
	imiss=$(( imiss+1 ))
    fi

    if [ -f $LAND_MASK_FILE ] ; then
	echo "    + OK   land mask  file '${LAND_MASK_FILE}'"
	igood=$(( igood+1 ))
    else
	echo "    - MISS land mask  file '${LAND_MASK_FILE}'"
	imiss=$(( imiss+1 ))
    fi

    #
    # Statistic and report
    #
    if [ $imiss -le 0 ] ; then
	echo "  * Found all$(printf '%4d' $igood) temporal invariant files"
    else
	echo "  + Found    $(printf '%4d' $igood) temporal invariant files"
	echo "  - MISSING  $(printf '%4d' $imiss) temporal invariant files"
	#istop=$(( istop+1 ))
    fi
    echo

    # ------------------------------------------------------------
    #
    # CISSEMBEL driver script
    # CISSEMBEL input files
    #
    echo "  Executable"
    igood=0
    imiss=0

    for file in $CISSEMBEL_EXE ; do
	if [ -f $file ] ; then
	    echo "    + OK   executable file '${file}'"
	    igood=$(( igood+1 ))
	else
	    echo "    - MISS executable file '${file}'"
	    imiss=$(( imiss+1 ))
	fi
    done

    echo "  CISSEMBEL input files"
    for file in $INITIAL_FILE_CISSEMBEL  $START_FILE_CISSEMBEL \
		$NAMELIST_FILE_CISSEMBEL $NAMELIST_FILE_DRIVER_FOR
    do
	if [ -f $file ] ; then
	    echo "    + OK   CISSEMBEL input '${file}'"
	    igood=$(( igood+1 ))
	else
	    echo "    - MISS CISSEMBEL input '${file}'"
	    imiss=$(( imiss+1 ))
	fi
    done

    #
    # Statistic and report
    #
    if [ $imiss -le 0 ] ; then
	echo "  * Found all$(printf '%4d' $igood) CISSEMBEL input files"
    else
	echo "  + Found    $(printf '%4d' $igood) CISSEMBEL input files"
	echo "  - MISSING  $(printf '%4d' $imiss) CISSEMBEL input files"
	istop=$(( istop+1 ))
    fi
    echo

    if [ ${istop:-1} -gt 0 ] ; then
	echo "MISSING required input. See above"
	echo "S T O P"
	exit 1
    fi
}

namelist_footer(){
    _namelist=${1:?Missing namelist file in namelist_footer}

    echo ""  >> $_namelist
    echo "!" >> $_namelist
    echo "! === Modified ===" >> $_namelist
    echo "! - Simulation scenario ${SCENARIO:-unknown}" >> $_namelist
    echo "! - Simulation year ${year:-unknown}" >> $_namelist
    echo "! - Date: ${date}" >> $_namelist
    echo "! - PWD :$(pwd)" >> $_namelist
    echo "! - HOST: ${HOSTNAME:-Unknown}" >> $_namelist
    echo "! - For user ${USER:-Unknown}" >> $_namelist
}

# ========================================================================
#
# Main
#
# Deactivate HDF5_USE_FILE_LOCKING, if you have file system issues
export HDF5_USE_FILE_LOCKING=FALSE

DATADIR=${WORKING_ROOT_DIR}/${SCENARIO}/${DATA_SUBDIR}
RUNDIR=${WORKING_ROOT_DIR}/${SCENARIO}/${CISSEMBEL_SUBDIR}

TARGET_DATA_FILE_HEAD="DMI-HIRHAM5_${SCENARIO}" # also ${SOURCE_DATA_FILE_HEAD}
TARGET_DATA_FILE_TAIL=".nc4"

# ------------------------------------------------------------------------
#
# Information
#
information

# ------------------------------------------------------------------------
#
# Check and report which files are available
#
checks

# ------------------------------------------------------------------------
#
# Prepare and populate the running directory
#
echo " Populate RUNDIR=${RUNDIR}"

if [ -d ${RUNDIR} ] ; then
    echo "   o Reuse existing RUNDIR"
elif [ -f ${RUNDIR} ] ; then
    echo "   * Found RUNDIR as a file and NOT as a directory"
    echo "S T O P"
    exit 2
else
    echo "   + Create new RUNDIR"
    mkdir -p $RUNDIR

    if [ ! -d $RUNDIR ] ; then
	echo "FAILED to create RUNDIR"
	echo "S T O P"
	exit 3
    fi
fi

echo "   + executable/binary: $CISSEMBEL_EXE"
[ -f $RUNDIR/$CISSEMBEL_EXE ] && echo "     = Overwrite old version"
cp $CISSEMBEL_EXE ${RUNDIR}

echo "   + Namelist file CISSEMBEL"
namelist_template_ebm=${RUNDIR}/namelist.ebm.template
[ -f $namelist_template_ebm ] && echo "     = Overwrite old version"
cp $NAMELIST_FILE_CISSEMBEL ${namelist_template_ebm}

echo "   + Namelist file Fortran driver"
namelist_template_drv=${RUNDIR}/namelist.EBMdriver.template
[ -f $namelist_template_drv ] && echo "     = Overwrite old version"
cp $NAMELIST_FILE_DRIVER_FOR ${namelist_template_drv}

echo "   + Initial file"
[ -f $RUNDIR/$INITIAL_FILE_CISSEMBEL ] && echo "     = Overwrite old version"
cp $INITIAL_FILE_CISSEMBEL ${RUNDIR}

echo "   + Restart/warm-start file"
[ -f $RUNDIR/$START_FILE_CISSEMBEL ] && echo "     = Overwrite old version"
cp $START_FILE_CISSEMBEL ${RUNDIR}

echo "   = Jump into RUNDIR"
cd $RUNDIR
echo "   - Content of $RUNDIR"
for file in * ; do
    if [ -d $file ] ; then
	echo "     - [d] $file"
    elif [ -f $file ] ; then
	echo "     - [f] $file"
    else
	echo "     - [x] $file"
    fi
done

#
# Last adjustments
#
# The leading './' in $cissemble_executable ensures proper execusion.
#
cissemble_executable="./$(basename $CISSEMBEL_EXE)"
[ -f $cissemble_executable ] && chmod u+x $cissemble_executable

#export PATH=$(pwd):${PATH}

echo
# ------------------------------------------------------------------------
#
# LOOP of all forcing files to run CISSEMBEL simulations
#
echo " ===== THE LOOP ============================= = =  =  -  -   .   .    ."

missing_year=""
for year in $(seq $YEAR_SCENRIO_START $YEAR_SCENRIO_END) ; do
    echo " SIMULATION year ${year}"
    # ------------------------------------------------------------
    #
    # Variables for each loop item
    #
    MERGED_TARGET_FILE="${DATADIR}/${TARGET_DATA_FILE_HEAD}_${year}_MERGED${TARGET_DATA_FILE_TAIL}"

    output_data_directory=${WORKING_ROOT_DIR}/${SCENARIO}/${CISSEMBEL_SUBDIR}/${year}
    log_cissemble_file=${output_data_directory}/cissemble.${SCENARIO}.${year}.log

    echo "  - Save directory ${output_data_directory}"
    if [ -d ${output_data_directory} ] ; then
	echo "    + Reuse and clean ${output_data_directory}"
	rm ${output_data_directory}/*
    else
	echo "    + Create ${output_data_directory}"
	mkdir -p ${output_data_directory}
    fi

    # ------------------------------------------------------------
    #
    # Check for restart. If missing and first year, use prepared start file
    #
    if [ -f $fname_rst_input ] ; then
	echo "  Use existing input restart file $fname_rst_input"
    else
	if [ $year -eq $YEAR_SCENRIO_START ] ; then
	    echo "  First year use as restart START_FILE_CISSEMBEL=$START_FILE_CISSEMBEL"
	    cp $START_FILE_CISSEMBEL $fname_rst_input
	else
	    echo "  Missing input restart file $fname_rst_input"
	    echo " S T O P"
	    exit 4
	fi
    fi

    # ------------------------------------------------------------
    #
    # Create final namelist.ebm file from template
    #
    namelist_final_ebm="namelist.ebm"
    echo "  Create namelist $namelist_final_ebm"

    if [ ! -f ${namelist_template_ebm} ] ; then
	echo "Missing namelist_template_ebm=${namelist_template_ebm}"
	echo "S T O P"
	exit 4
    fi

    sed 's/do_restart = F/do_restart = T/g' ${namelist_template_ebm} \
	| sed "/forcing_data_source/s/unknown/DMI:$(basename $MERGED_TARGET_FILE)/g" \
	| sed "/experiment_id/s/unknown/${SCENARIO}/g" \
	      > $namelist_final_ebm

    namelist_footer $namelist_final_ebm

    # ------------------------------------------------------------
    #
    # Create final namelist.EBMdriver file from template
    #
    namelist_final_drv="namelist.EBMdriver"
    echo "  Create namelist $namelist_final_drv"

    if [ ! -f ${namelist_template_drv} ] ; then
	echo "Missing namelist_template_drv=${namelist_template_drv}"
	echo "S T O P"
	exit 4
    fi

    sed_file="sed_file4$(basename ${namelist_template_drv})"

    cat > $sed_file <<EOF
/topo_info%filename/s|DMI-HIRHAM5_scenario_year_merged.nc4|${MERGED_TARGET_FILE}|g
/temp2m_info%filename/s|DMI-HIRHAM5_scenario_year_merged.nc4|${MERGED_TARGET_FILE}|g
/tprecip_info%filename/s|DMI-HIRHAM5_scenario_year_merged.nc4|${MERGED_TARGET_FILE}|g
/snowfall_info%filename/s|DMI-HIRHAM5_scenario_year_merged.nc4|${MERGED_TARGET_FILE}|g
/rainfall_info%filename/s|DMI-HIRHAM5_scenario_year_merged.nc4|${MERGED_TARGET_FILE}|g
/solardown_info%filename/s|DMI-HIRHAM5_scenario_year_merged.nc4|${MERGED_TARGET_FILE}|g
/thermaldown_info%filename/s|DMI-HIRHAM5_scenario_year_merged.nc4|${MERGED_TARGET_FILE}|g
/latentheat_info%filename/s|DMI-HIRHAM5_scenario_year_merged.nc4|${MERGED_TARGET_FILE}|g
/sensible_info%filename/s|DMI-HIRHAM5_scenario_year_merged.nc4|${MERGED_TARGET_FILE}|g
/evaporat_info%filename/s|DMI-HIRHAM5_scenario_year_merged.nc4|${MERGED_TARGET_FILE}|g
/cloudcover_info%filename/s|DMI-HIRHAM5_scenario_year_merged.nc4|${MERGED_TARGET_FILE}|g
/icefrac_info%filename/s|DMI-HIRHAM5_scenario_year_merged.nc4|${MERGED_TARGET_FILE}|g
/lat_info%filename/s|DMI-HIRHAM5_scenario_year_merged.nc4|${MERGED_TARGET_FILE}|g
/ton_info%filename/s|DMI-HIRHAM5_scenario_year_merged.nc4|${MERGED_TARGET_FILE}|g
/mask_info%filename/s|DMI-HIRHAM5_scenario_year_merged.nc4|${MERGED_TARGET_FILE}|g
/VARIABLE_info%filename/s|DMI-HIRHAM5_scenario_year_merged.nc4|${MERGED_TARGET_FILE}|g
EOF

    sed -f $sed_file ${namelist_template_drv} > $namelist_final_drv && rm $sed_file

    namelist_footer $namelist_final_drv

    # ------------------------------------------------------------
    #
    # Run the simulation
    #
    echo "  == Starting $cissemble_executable ======================================"
    if [ ! -f $MERGED_TARGET_FILE ] ; then
	echo "   **** SKIP, missing forcing file $MERGED_TARGET_FILE"
	missing_year="${missing_year} ${year}"
	continue
    fi
    $cissemble_executable > ${log_cissemble_file} 2>&1

    echo "  == Returning from $cissemble_executable ================================"

    # ------------------------------------------------------------
    #
    # Post-processing
    #
    echo " Post-processing"

    #
    # Current output restart file is the next input restart file
    #
    echo "  - Next restart file"
    if [ -f $fname_rst_output ] ; then
	if [ -f $fname_rst_input ] ; then
	    echo "    + Overwrite current $fname_rst_input with $fname_rst_output"
	else
	    echo "    - Copy newly $fname_rst_input to $fname_rst_output"
	fi
	cp -pf $fname_rst_output $fname_rst_input
    else
	echo "MISSING output restart file $fname_rst_output"
	echo "S T O P"
	exit 4
    fi

    #
    # Save current output file and configuration in data sub-directories
    #
    echo "  - Save output in ${output_data_directory}"
    for file in $fname_dia_data $fname_cpl_data \
		$fname_frc_data $fname_sta_data \
		$fname_out_data $fname_rst_output
    do
	target_file=${output_data_directory}/${file%%.${fname_suffix}}.${SCENARIO}.${year}.${fname_suffix}
	#echo " *** $(basename $file) -> $target_file"
	if [ -f $file ] ; then
	    echo "    + $file -> $target_file"
	    mv -f $file $target_file
	else
	    echo "    - Skip MISSING $file"
	fi
    done

    echo "  - Save configuration in ${output_data_directory}"
    for file in $namelist_final_ebm \
		$namelist_final_drv
    do
	target_file=${output_data_directory}/${file}.${SCENARIO}.${year}
	if [ -f $file ] ; then
	    echo "    + $file -> $target_file"
	    mv -f $file $target_file
	else
	    echo "    - Skip MISSING $file"
	fi
    done

    # Empty lines before the next years starts
    echo ; echo
done


# ------------------------------------------------------------
#
# Clean up
#
if [ "x${missing_year}" != "x" ] ; then
    echo "Missing forcing years"
    for year in ${missing_year} ; do
	echo "  - $year"
    done
fi

exit 0
# -- last linex

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 31 23:28:54 CET 2022

C-binding to Fortran functions and subroutines in mod_physic_column_column as part of
the Copenhagen Ice Snow Surface Energy and Mass Balance modEL (CISSEMBEL)

@author: Christian Rodehacke, DMI Copenhagen, Denmark

@copyright: Copyright (C) 2016-2025 Christian Rodehacke
"""
# -------------------------------------------------------------------
# This file is part of CISSEMBEL, which is the
# == Copenhagen Ice-Snow Surface Energy and Mass Balance modEL ==
#
# CISSEMBEL is free software; you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE  v.1.2
#
# Information about the EUROPEAN UNION PUBLIC LICENCE (*EUPL*) and
# translations into other European languages are available at
# https://eupl.eu (last access: 2023-09-18)](https://eupl.eu/)
#
# We hope that  this  distributed  CISSEMBEL  code, its  tools and
# documentations are usefull. Any improvements of the code, tools,
# and documentations tools are  very welcome.  Please, consider to
# share your thoughts and improvements with the community.
#
# The documentation,  tools,  and the code are provided as it are.
# NO WARRANTY  ABOUT FITNESS  AND USABILITY OF THE CODE, TOOLS, OR
# DOCUMENTATION  ARE  GIVEN.   NO  WARRANTY  IS  GIVEN  ABOUT  THE
# CORRECTNESS  OF COMPUTED RESULTS WITH THE  PROVIDED CODE, TOOLS,
# AND DOCUMENTATION. YOU USE THE CODE, DOCUMENTATION, AND TOOLS AT
# YOUR OWN RISK.           Removing  of  any  copyright  statement
# or  this  disclaimer  is considered  as a  attempt  to defraud.
#
# -------------------------------------------------------------------

def density_frozen(snow_content, ice_content, snow_density, ice_density,
                   library='../src/CISSEMBEL_CBindings.so'):
    """
    C-binding to Fortran function density_frozen in mod_physic_column in CISSEMBEL
    Combined density / density profile with depth.

    Parameters
    ----------
    snow_content : float or numpy.ndarray
        Snow content.
    ice_content : float or numpy.ndarray
        Ice content.
    snow_density : float or numpy.ndarray
        Density of snow.
    ice_density : float or numpy.ndarray
        Density of ice.
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings. The
        default is '../src/CISSEMBEL_CBindings.so'.

    Returns
    -------
    density : float or numpy.ndarray
        Snow density at given depth(s).

    """
    if isinstance(library, ctypes.CDLL):
        fortlibrary = library
    else:
        fortlibrary = ctypes.cdll.LoadLibrary(library)

    if isinstance(snow_content, (float, int)):
        inum = 0
        # Single c-value
        c_snow_content = ctypes.c_double(snow_content)
        c_ice_content = ctypes.c_double(ice_content)
        c_snow_density = ctypes.c_double(snow_density)
        c_ice_density = ctypes.c_double(ice_density)
    else:
        inum = len(snow_content)
        # Number conversion, Fortran-order
        c_snow_content = numpy.array(snow_content, order="F", dtype=float)
        c_ice_content = numpy.array(ice_content, order="F", dtype=float)
        c_snow_density = numpy.array(snow_density, order="F", dtype=float)
        c_ice_density = numpy.array(ice_density, order="F", dtype=float)
        # c-pointer
        c_snow_content_ptr = c_snow_content.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_ice_content_ptr = c_ice_content.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_snow_density_ptr = c_snow_density.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_ice_density_ptr = c_ice_density.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        # Field sizes
        m4c.compare_field2shapes(snow_content.shape, ice_content.shape,
                             'snow_content', 'ice_content')
        m4c.compare_field2shapes(snow_content.shape, snow_density.shape,
                             'snow_content', 'snow_density')
        m4c.compare_field2shapes(snow_content.shape, ice_density.shape,
                             'snow_content', 'ice_density')

    if inum:
        # array
        density = numpy.zeros((inum), order="F", dtype=float)
        density_ptr = density.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        fortlibrary.c_density_frozen.argtypes = [ctypes.c_int,
                                                 ctypes.POINTER(ctypes.c_double),
                                                 ctypes.POINTER(ctypes.c_double),
                                                 ctypes.POINTER(ctypes.c_double),
                                                 ctypes.POINTER(ctypes.c_double),
                                                 ctypes.POINTER(ctypes.c_double)]
        fortlibrary.c_density_frozen(ctypes.c_int(inum),
                                     c_snow_content_ptr,
                                     c_ice_content_ptr,
                                     c_snow_density_ptr,
                                     c_ice_density_ptr,
                                     density_ptr)
    else:
        # skalar
        fortlibrary.density_frozen.restype = ctypes.c_double
        density = fortlibrary.density_frozen(ctypes.byref(c_snow_content),
                                             ctypes.byref(c_ice_content),
                                             ctypes.byref(c_snow_density),
                                             ctypes.byref(c_ice_density))
    return density


def water_saturation(snow_content, water_content, snow_density,
                     library='../src/CISSEMBEL_CBindings.so'):
    """
    C-binding to Fortran function water_saturation in mod_physic_column in CISSEMBEL
    Combined density / density profile with depth.

    Parameters
    ----------
    snow_content : float or numpy.ndarray
        Snow content.
    water_content : float or numpy.ndarray
        Water content.
    snow_density : float or numpy.ndarray
        Density of snow.
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings. The
        default is '../src/CISSEMBEL_CBindings.so'.

    Returns
    -------
    water_sat : float or numpy.ndarray
        Water saturation fraction.

    """
    if isinstance(library, ctypes.CDLL):
        fortlibrary = library
    else:
        fortlibrary = ctypes.cdll.LoadLibrary(library)

    if isinstance(snow_content, (float, int)):
        inum = 0
        # Single c-value
        c_snow_content = ctypes.c_double(snow_content)
        c_water_content = ctypes.c_double(water_content)
        c_snow_density = ctypes.c_double(snow_density)
    else:
        inum = len(snow_content)
        # Number conversion, Fortran-order
        c_snow_content = numpy.array(snow_content, order="F", dtype=float)
        c_water_content = numpy.array(water_content, order="F", dtype=float)
        c_snow_density = numpy.array(snow_density, order="F", dtype=float)
        # c-pointer
        c_snow_content_ptr = c_snow_content.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_water_content_ptr = c_water_content.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_snow_density_ptr = c_snow_density.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        # Field sizes
        m4c.compare_field2shapes(snow_content.shape, water_content.shape,
                             'snow_content', 'water_content')
        m4c.compare_field2shapes(snow_content.shape, snow_density.shape,
                             'snow_content', 'snow_density')

    if inum:
        # array
        water_sat = numpy.zeros((inum), order="F", dtype=float)
        water_sat_ptr = water_sat.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        fortlibrary.c_water_saturation.argtypes = [ctypes.c_int,
                                                   ctypes.POINTER(ctypes.c_double),
                                                   ctypes.POINTER(ctypes.c_double),
                                                   ctypes.POINTER(ctypes.c_double),
                                                   ctypes.POINTER(ctypes.c_double)]
        fortlibrary.c_water_saturation(ctypes.c_int(inum),
                                       c_snow_content_ptr,
                                       c_water_content_ptr,
                                       c_snow_density_ptr,
                                       water_sat_ptr)
    else:
        # skalar
        fortlibrary.water_saturation.restype = ctypes.c_double
        water_sat = fortlibrary.water_saturation(ctypes.byref(c_snow_content),
                                                 ctypes.byref(c_water_content),
                                                 ctypes.byref(c_snow_density))
    return water_sat


def irreducible_water_saturation(snow_density,
                                 library='../src/CISSEMBEL_CBindings.so'):
    """
    C-binding to Fortran function irreducible_water_saturation in mod_physic_column
    in CISSEMBEL
    Irreducible water saturation / irreducible water saturation depth profile.

    Parameters
    ----------
    snow_density : float or numpy.ndarray
        Density of snow.
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings. The
        default is '../src/CISSEMBEL_CBindings.so'.

    Returns
    -------
    irreduc_wcont : float or numpy.ndarray
        Irreducible water saturation at given depth(s).

    """
    if isinstance(library, ctypes.CDLL):
        fortlibrary = library
    else:
        fortlibrary = ctypes.cdll.LoadLibrary(library)

    if isinstance(snow_density, (float, int)):
        inum = 0
        # Single c-value
        c_snow_density = ctypes.c_double(snow_density)
    else:
        inum = len(snow_density)
        # Number conversion, Fortran-order
        c_snow_density = numpy.array(snow_density, order="F", dtype=float)
        # c-pointer
        c_snow_density_ptr = \
            c_snow_density.ctypes.data_as(ctypes.POINTER(ctypes.c_double))


    if inum:
        # array
        irreduc_wcont = numpy.zeros((inum), order="F", dtype=float)
        irreduc_wcont_ptr = \
            irreduc_wcont.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        fortlibrary.c_irreducible_water_saturation.argtypes = \
            [ctypes.c_int,
             ctypes.POINTER(ctypes.c_double),
             ctypes.POINTER(ctypes.c_double)]
        fortlibrary.c_irreducible_water_saturation(ctypes.c_int(inum),
                                                   c_snow_density_ptr,
                                                   irreduc_wcont_ptr)
    else:
        # skalar
        fortlibrary.irreducible_water_saturation.restype = ctypes.c_double
        irreduc_wcont = \
            fortlibrary.irreducible_water_saturation(ctypes.byref(c_snow_density))
    return irreduc_wcont


def retention_potential(snow_content, snow_density,
                        library='../src/CISSEMBEL_CBindings.so'):
    """
    C-binding to Fortran function retention_potential in mod_physic_column in CISSEMBEL
    Potential retention / Potential retention profile with depth.

    Parameters
    ----------
    snow_content : float or numpy.ndarray
        Snow content.
    snow_density : float or numpy.ndarray
        Density of snow.
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings. The
        default is '../src/CISSEMBEL_CBindings.so'.

    Returns
    -------
    retention_pot : float or numpy.ndarray
        Retention potential of liquid water at given depth(s).

    """
    if isinstance(library, ctypes.CDLL):
        fortlibrary = library
    else:
        fortlibrary = ctypes.cdll.LoadLibrary(library)

    if isinstance(snow_content, (float, int)):
        inum = 0
        # Single c-value
        c_snow_content = ctypes.c_double(snow_content)
        c_snow_density = ctypes.c_double(snow_density)
    else:
        inum = len(snow_content)
        # Number conversion, Fortran-order
        c_snow_content = numpy.array(snow_content, order="F", dtype=float)
        c_snow_density = numpy.array(snow_density, order="F", dtype=float)
        # c-pointer
        c_snow_content_ptr = c_snow_content.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_snow_density_ptr = c_snow_density.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        # Field sizes
        m4c.compare_field2shapes(snow_content.shape, snow_density.shape,
                             'snow_content', 'snow_density')

    if inum:
        # array
        retention_pot = numpy.zeros((inum), order="F", dtype=float)
        retention_pot_ptr = \
            retention_pot.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        fortlibrary.c_retention_potential.argtypes = \
            [ctypes.c_int,
             ctypes.POINTER(ctypes.c_double),
             ctypes.POINTER(ctypes.c_double),
             ctypes.POINTER(ctypes.c_double)]
        fortlibrary.c_retention_potential(ctypes.c_int(inum),
                                          c_snow_content_ptr,
                                          c_snow_density_ptr,
                                          retention_pot_ptr)
    else:
        # skalar
        fortlibrary.retention_potential.restype = ctypes.c_double
        retention_pot = \
            fortlibrary.retention_potential(ctypes.byref(c_snow_content),
                                            ctypes.byref(c_snow_density))
    return retention_pot


def calc_runoff(dtime, snow_content, water_content, snow_density, waterflow,
                time_runoff, library='../src/CISSEMBEL_CBindings.so'):
    """
    C-binding to Fortran function calc_runoff in mod_physic_column in CISSEMBEL
    Combined runoff / runoff profile with depth.

    Parameters
    ----------
    dtime : float
        Time step width.
    snow_content : float or numpy.ndarray
        Snow content.
    water_content : float or numpy.ndarray
        Water content.
    snow_density : float or numpy.ndarray
        Density of snow.
    waterflow : float or numpy.ndarray
        flow of liquid water.
    time_runoff : float
        Runoff time scale.
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings. The
        default is '../src/CISSEMBEL_CBindings.so'.

    Returns
    -------
    runoff : float or numpy.ndarray
        Runoff at given depth(s).

    """
    if isinstance(library, ctypes.CDLL):
        fortlibrary = library
    else:
        fortlibrary = ctypes.cdll.LoadLibrary(library)


    if isinstance(snow_content, (float, int)):
        inum = 0
        # Single c-value
        c_snow_content = ctypes.c_double(snow_content)
        c_water_content = ctypes.c_double(water_content)
        c_snow_density = ctypes.c_double(snow_density)
        c_waterflow = ctypes.c_double(waterflow)
        c_time_runoff = ctypes.c_double(time_runoff)
    else:
        inum = len(snow_content)
        # Number conversion, Fortran-order
        c_snow_content = numpy.array(snow_content, order="F", dtype=float)
        c_water_content = numpy.array(water_content, order="F", dtype=float)
        c_snow_density = numpy.array(snow_density, order="F", dtype=float)
        c_waterflow = numpy.array(waterflow, order="F", dtype=float)
        c_time_runoff = numpy.array(time_runoff, order="F", dtype=float)
        # c-pointer
        c_snow_content_ptr = c_snow_content.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_water_content_ptr = c_water_content.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_snow_density_ptr = c_snow_density.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_waterflow_ptr = c_waterflow.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_time_runoff_ptr = c_time_runoff.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        # Field sizes
        m4c.compare_field2shapes(snow_content.shape, water_content.shape,
                             'snow_content', 'water_content')
        m4c.compare_field2shapes(snow_content.shape, snow_density.shape,
                             'snow_content', 'snow_density')
        m4c.compare_field2shapes(snow_content.shape, waterflow.shape,
                             'snow_content', 'waterflow')
        m4c.compare_field2shapes(snow_content.shape, time_runoff.shape,
                             'snow_content', 'time_runoff')
    c_dtime = ctypes.c_double(dtime)


    if inum:
        # array
        runoff = numpy.zeros((inum), order="F", dtype=float)
        runoff_ptr = runoff.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        fortlibrary.c_calc_runoff.argtypes = [ctypes.c_int,
                                              ctypes.c_double,
                                              ctypes.POINTER(ctypes.c_double),
                                              ctypes.POINTER(ctypes.c_double),
                                              ctypes.POINTER(ctypes.c_double),
                                              ctypes.POINTER(ctypes.c_double),
                                              ctypes.POINTER(ctypes.c_double),
                                              ctypes.POINTER(ctypes.c_double)]
        fortlibrary.c_calc_runoff(ctypes.c_int(inum),
                                  c_dtime,
                                  c_snow_content_ptr,
                                  c_water_content_ptr,
                                  c_snow_density_ptr,
                                  c_waterflow_ptr,
                                  c_time_runoff_ptr,
                                  runoff_ptr)
    else:
        # skalar
        fortlibrary.calc_runoff.restype = ctypes.c_double
        runoff = fortlibrary.calc_runoff(ctypes.byref(c_dtime),
                                         ctypes.byref(c_snow_content),
                                         ctypes.byref(c_water_content),
                                         ctypes.byref(c_snow_density),
                                         ctypes.byref(c_waterflow),
                                         ctypes.byref(c_time_runoff))
    return runoff


def supericeform_potential(dtime, snow_content, ice_content, snow_density,
                           temperature, melt_temperature,
                           library='../src/CISSEMBEL_CBindings.so'):
    """
    C-binding to Fortran function supericeform_potential in mod_physic_column in CISSEMBEL
    Superimposed ice formation / superimposed ice formation profile with depth.

    Parameters
    ----------
    dtime : float
        Time step width.
    snow_content : float or numpy.ndarray
        Snow content.
    ice_content : float or numpy.ndarray
        Ice content.
    snow_density : float or numpy.ndarray
        Density of snow.
    temperature : float or numpy.ndarray
        Temperature.
    melt_temperature : float or numpy.ndarray
        Melting temperature.
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings. The
        default is '../src/CISSEMBEL_CBindings.so'.

    Returns
    -------
    supericeform_pot : float or numpy.ndarray
        Superimposed ice formation at given depth(s).

    """
    if isinstance(library, ctypes.CDLL):
        fortlibrary = library
    else:
        fortlibrary = ctypes.cdll.LoadLibrary(library)


    if isinstance(snow_content, (float, int)):
        inum = 0
        # Single c-value
        c_snow_content = ctypes.c_double(snow_content)
        c_ice_content = ctypes.c_double(ice_content)
        c_snow_density = ctypes.c_double(snow_density)
        c_temperature = ctypes.c_double(temperature)
        c_melt_temperature = ctypes.c_double(melt_temperature)
    else:
        inum = len(snow_content)
        # Number conversion, Fortran-order
        c_snow_content = numpy.array(snow_content, order="F", dtype=float)
        c_ice_content = numpy.array(ice_content, order="F", dtype=float)
        c_snow_density = numpy.array(snow_density, order="F", dtype=float)
        c_temperature = numpy.array(temperature, order="F", dtype=float)
        c_melt_temperature = numpy.array(melt_temperature, order="F", dtype=float)
        # c-pointer
        c_snow_content_ptr = c_snow_content.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_ice_content_ptr = c_ice_content.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_snow_density_ptr = c_snow_density.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_temperature_ptr = c_temperature.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_melt_temperature_ptr = \
            c_melt_temperature.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        # Field sizes
        m4c.compare_field2shapes(snow_content.shape, ice_content.shape,
                             'snow_content', 'ice_content')
        m4c.compare_field2shapes(snow_content.shape, snow_density.shape,
                             'snow_content', 'snow_density')
        m4c.compare_field2shapes(snow_content.shape, snow_density.shape,
                             'snow_content', 'snow_density')
        m4c.compare_field2shapes(snow_content.shape, temperature.shape,
                             'snow_content', 'temperature')
        m4c.compare_field2shapes(snow_content.shape, melt_temperature.shape,
                             'snow_content', 'melt_temperature')

    c_dtime = ctypes.c_double(dtime)


    if inum:
        # array
        supericeform_pot = numpy.zeros((inum), order="F", dtype=float)
        supericeform_pot_ptr = supericeform_pot.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        fortlibrary.c_supericeform_potential.argtypes = [ctypes.c_int,
                                              ctypes.c_double,
                                              ctypes.POINTER(ctypes.c_double),
                                              ctypes.POINTER(ctypes.c_double),
                                              ctypes.POINTER(ctypes.c_double),
                                              ctypes.POINTER(ctypes.c_double),
                                              ctypes.POINTER(ctypes.c_double),
                                              ctypes.POINTER(ctypes.c_double)]
        fortlibrary.c_supericeform_potential(ctypes.c_int(inum),
                                  c_dtime,
                                  c_snow_content_ptr,
                                  c_ice_content_ptr,
                                  c_snow_density_ptr,
                                  c_temperature_ptr,
                                  c_melt_temperature_ptr,
                                  supericeform_pot_ptr)
    else:
        # skalar
        fortlibrary.supericeform_potential.restype = ctypes.c_double
        supericeform_pot = fortlibrary.supericeform_potential(ctypes.byref(c_dtime),
                                         ctypes.byref(c_snow_content),
                                         ctypes.byref(c_ice_content),
                                         ctypes.byref(c_snow_density),
                                         ctypes.byref(c_temperature),
                                         ctypes.byref(c_melt_temperature))
    return supericeform_pot


def grain_growth(dtime, grain_diameter, snow_content, water_content,
                 library='../src/CISSEMBEL_CBindings.so'):
    """
    C-binding to Fortran function grain_growth in mod_physic_column in CISSEMBEL
    Growth of snow grains / growth of snow grains profile with depth.

    Parameters
    ----------
    dtime : float
        Time step width.
    grain_diameter : float or numpy.ndarray
        Snow grain diameter.
    snow_content : float or numpy.ndarray
        Snow content.
    water_content : float or numpy.ndarray
        Water content.
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings. The
        default is '../src/CISSEMBEL_CBindings.so'.

    Returns
    -------
    graingrowth : float or numpy.ndarray
        Growth of snow grain size at given depth(s).

    """
    if isinstance(library, ctypes.CDLL):
        fortlibrary = library
    else:
        fortlibrary = ctypes.cdll.LoadLibrary(library)


    if isinstance(snow_content, (float, int)):
        inum = 0
        # Single c-value
        c_grain_diameter = ctypes.c_double(grain_diameter)
        c_snow_content = ctypes.c_double(snow_content)
        c_water_content = ctypes.c_double(water_content)
    else:
        inum = len(snow_content)
        # Number conversion, Fortran-order
        c_grain_diameter = numpy.array(grain_diameter, order="F", dtype=float)
        c_snow_content = numpy.array(snow_content, order="F", dtype=float)
        c_water_content = numpy.array(water_content, order="F", dtype=float)
        # c-pointer
        c_grain_diameter_ptr = c_grain_diameter.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_snow_content_ptr = c_snow_content.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_water_content_ptr = c_water_content.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        # Field sizes
        m4c.compare_field2shapes(snow_content.shape, grain_diameter.shape,
                             'snow_content', 'grain_diameter')
        m4c.compare_field2shapes(snow_content.shape, water_content.shape,
                             'snow_content', 'water_content')

    c_dtime = ctypes.c_double(dtime)


    if inum:
        # array
        graingrowth = numpy.zeros((inum), order="F", dtype=float)
        graingrowth_ptr = graingrowth.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        fortlibrary.c_grain_growth.argtypes = [ctypes.c_int,
                                              ctypes.c_double,
                                              ctypes.POINTER(ctypes.c_double),
                                              ctypes.POINTER(ctypes.c_double),
                                              ctypes.POINTER(ctypes.c_double),
                                              ctypes.POINTER(ctypes.c_double)]
        fortlibrary.c_grain_growth(ctypes.c_int(inum),
                                   c_dtime,
                                   c_grain_diameter_ptr,
                                   c_snow_content_ptr,
                                   c_water_content_ptr,
                                   graingrowth_ptr)
    else:
        # skalar
        fortlibrary.grain_growth.restype = ctypes.c_double
        graingrowth = fortlibrary.grain_growth(ctypes.byref(c_dtime),
                                               ctypes.byref(c_grain_diameter),
                                               ctypes.byref(c_snow_content),
                                               ctypes.byref(c_water_content))
    return graingrowth


def hydraulic_suction(theta, grain_diameter,
                      library='../src/CISSEMBEL_CBindings.so'):
    """
    C-binding to Fortran function hydraulic_suction in mod_physic_column in CISSEMBEL
    Growth of snow grains / growth of snow grains profile with depth.

    Parameters
    ----------
    theta : float or numpy.ndarray
        Water saturation fraction.
    grain_diameter : float or numpy.ndarray
        Snow grain diameter.
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings. The
        default is '../src/CISSEMBEL_CBindings.so'.

    Returns
    -------
    hydrau_suction : float or numpy.ndarray
        Hydraulic suction at given depth(s).

    """
    if isinstance(library, ctypes.CDLL):
        fortlibrary = library
    else:
        fortlibrary = ctypes.cdll.LoadLibrary(library)


    if isinstance(theta, (float, int)):
        inum = 0
        # Single c-value
        c_theta = ctypes.c_double(theta)
        c_grain_diameter = ctypes.c_double(grain_diameter)
    else:
        inum = len(theta)
        # Number conversion, Fortran-order
        c_theta = numpy.array(theta, order="F", dtype=float)
        c_grain_diameter = numpy.array(grain_diameter, order="F", dtype=float)
        # c-pointer
        c_theta_ptr = c_theta.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_grain_diameter_ptr = c_grain_diameter.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        # Field sizes
        m4c.compare_field2shapes(theta.shape, grain_diameter.shape,
                             'theta', 'grain_diameter')


    if inum:
        # array
        hydrau_suction = numpy.zeros((inum), order="F", dtype=float)
        hydrau_suction_ptr = hydrau_suction.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        fortlibrary.c_hydraulic_suction.argtypes = [ctypes.c_int,
                                                    ctypes.POINTER(ctypes.c_double),
                                                    ctypes.POINTER(ctypes.c_double),
                                                    ctypes.POINTER(ctypes.c_double)]
        fortlibrary.c_hydraulic_suction(ctypes.c_int(inum),
                                        c_theta_ptr,
                                        c_grain_diameter_ptr,
                                        hydrau_suction_ptr)
    else:
        # skalar
        fortlibrary.hydraulic_suction.restype = ctypes.c_double
        hydrau_suction = fortlibrary.hydraulic_suction(ctypes.byref(c_theta),
                                                       ctypes.byref(c_grain_diameter))
    return hydrau_suction


def snow_permeability(theta, grain_diameter, snow_density, snow_content,
                      ice_content, library='../src/CISSEMBEL_CBindings.so'):
    """
    C-binding to Fortran function snow_permeability, k-function in mod_physic_column
    in CISSEMBEL
    Snow permeability (k-function) / snow permeability profile with depth.

    Parameters
    ----------
    theta : float or numpy.ndarray
        Water saturation fraction.
    grain_diameter : float or numpy.ndarray
        Snow grain diameter.
    snow_density : float or numpy.ndarray
        Density of snow.
    snow_content : float or numpy.ndarray
        Snow content.
    ice_content : float or numpy.ndarray
        Water content.
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings. The
        default is '../src/CISSEMBEL_CBindings.so'.

    Returns
    -------
    snow_perm : float or numpy.ndarray
        Snow permeability (k-function) at given depth(s).

    """
    if isinstance(library, ctypes.CDLL):
        fortlibrary = library
    else:
        fortlibrary = ctypes.cdll.LoadLibrary(library)


    if isinstance(theta, (float, int)):
        inum = 0
        # Single c-value
        c_theta = ctypes.c_double(theta)
        c_grain_diameter = ctypes.c_double(grain_diameter)
        c_snow_density = ctypes.c_double(snow_density)
        c_snow_content = ctypes.c_double(snow_content)
        c_ice_content = ctypes.c_double(ice_content)
    else:
        inum = len(theta)
        # Number conversion, Fortran-order
        c_theta = numpy.array(theta, order="F", dtype=float)
        c_grain_diameter = numpy.array(grain_diameter, order="F", dtype=float)
        c_snow_density = numpy.array(snow_density, order="F", dtype=float)
        c_snow_content = numpy.array(snow_content, order="F", dtype=float)
        c_ice_content = numpy.array(ice_content, order="F", dtype=float)
        # c-pointer
        c_theta_ptr = c_theta.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_grain_diameter_ptr = c_grain_diameter.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_snow_density_ptr = c_snow_density.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_snow_content_ptr = c_snow_content.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_ice_content_ptr = c_ice_content.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        # Field sizes
        m4c.compare_field2shapes(snow_content.shape, ice_content.shape,
                             'snow_content', 'ice_content')
        m4c.compare_field2shapes(snow_content.shape, snow_density.shape,
                             'snow_content', 'snow_density')
        m4c.compare_field2shapes(snow_content.shape, grain_diameter.shape,
                             'snow_content', 'grain_diameter')
        m4c.compare_field2shapes(snow_content.shape, theta.shape,
                             'snow_content', 'theta')

    if inum:
        # array
        snow_perm = numpy.zeros((inum), order="F", dtype=float)
        snow_perm_ptr = snow_perm.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        fortlibrary.c_snow_permeability.argtypes = [ctypes.c_int,
                                                    ctypes.POINTER(ctypes.c_double),
                                                    ctypes.POINTER(ctypes.c_double),
                                                    ctypes.POINTER(ctypes.c_double),
                                                    ctypes.POINTER(ctypes.c_double),
                                                    ctypes.POINTER(ctypes.c_double),
                                                    ctypes.POINTER(ctypes.c_double)]
        fortlibrary.c_snow_permeability(ctypes.c_int(inum),
                                        c_theta_ptr,
                                        c_grain_diameter_ptr,
                                        c_snow_density_ptr,
                                        c_snow_content_ptr,
                                        c_ice_content_ptr,
                                        snow_perm_ptr)
    else:
        # skalar
        fortlibrary.snow_permeability.restype = ctypes.c_double
        snow_perm = fortlibrary.snow_permeability(ctypes.byref(theta),
                                                  ctypes.byref(c_grain_diameter),
                                                  ctypes.byref(c_snow_density),
                                                  ctypes.byref(c_snow_content),
                                                  ctypes.byref(c_ice_content))
    return snow_perm


def mfunc(n_value, library='../src/CISSEMBEL_CBindings.so'):
    """
    C-binding to Fortran function mfunc in mod_physic_column in CISSEMBEL
    n-function / n-function profile with depth.

    Parameters
    ----------
    n_value : float or numpy.ndarray
        n-function at given depth(s).
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings. The
        default is '../src/CISSEMBEL_CBindings.so'.

    Returns
    -------
    m_value : float or numpy.ndarray
        m-function at given depth(s).

    """
    if isinstance(library, ctypes.CDLL):
        fortlibrary = library
    else:
        fortlibrary = ctypes.cdll.LoadLibrary(library)


    if isinstance(n_value, (float, int)):
        inum = 0
        # Single c-value
        c_n_value = ctypes.c_double(n_value)
    else:
        inum = len(n_value)
        # Number conversion, Fortran-order
        c_n_value = numpy.array(n_value, order="F", dtype=float)
        # c-pointer
        c_n_value_ptr = c_n_value.ctypes.data_as(ctypes.POINTER(ctypes.c_double))


    if inum:
        # array
        m_value = numpy.zeros((inum), order="F", dtype=float)
        m_value_ptr = m_value.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        fortlibrary.c_mfunc.argtypes = [ctypes.c_int,
                                        ctypes.POINTER(ctypes.c_double),
                                        ctypes.POINTER(ctypes.c_double)]
        fortlibrary.c_mfunc(ctypes.c_int(inum),
                            c_n_value_ptr,
                            m_value_ptr)
    else:
        # skalar
        fortlibrary.mfunc.restype = ctypes.c_double
        m_value = fortlibrary.mfunc(ctypes.byref(c_n_value))
    return m_value


def nfunc(grain_diameter, library='../src/CISSEMBEL_CBindings.so'):
    """
    C-binding to Fortran function nfunc in mod_physic_column in CISSEMBEL
    n-function / n-function profile with depth.

    Parameters
    ----------
    grain_diameter : float or numpy.ndarray
        Snow grain diameter.
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings. The
        default is '../src/CISSEMBEL_CBindings.so'.

    Returns
    -------
    n_value : float or numpy.ndarray
        n-function at given depth(s).

    """
    if isinstance(library, ctypes.CDLL):
        fortlibrary = library
    else:
        fortlibrary = ctypes.cdll.LoadLibrary(library)


    if isinstance(grain_diameter, (float, int)):
        inum = 0
        # Single c-value
        c_grain_diameter = ctypes.c_double(grain_diameter)
    else:
        inum = len(grain_diameter)
        # Number conversion, Fortran-order
        c_grain_diameter = numpy.array(grain_diameter, order="F", dtype=float)
        # c-pointer
        c_grain_diameter_ptr = c_grain_diameter.ctypes.data_as(ctypes.POINTER(ctypes.c_double))


    if inum:
        # array
        n_value = numpy.zeros((inum), order="F", dtype=float)
        n_value_ptr = n_value.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        fortlibrary.c_nfunc.argtypes = [ctypes.c_int,
                                        ctypes.POINTER(ctypes.c_double),
                                        ctypes.POINTER(ctypes.c_double)]
        fortlibrary.c_nfunc(ctypes.c_int(inum),
                            c_grain_diameter_ptr,
                            n_value_ptr)
    else:
        # skalar
        fortlibrary.nfunc.restype = ctypes.c_double
        n_value = fortlibrary.nfunc(ctypes.byref(c_grain_diameter))
    return n_value


def dgrain_dt(grain_diameter, snow_content, water_content,
              library='../src/CISSEMBEL_CBindings.so'):
    """
    C-binding to Fortran function dgrain_dt in mod_physic_column in CISSEMBEL
    Snow grain evolution slope / snow grain evolution slope profile with depth.

    Parameters
    ----------
    grain_diameter : float or numpy.ndarray
        Snow grain diameter.
    snow_content : float or numpy.ndarray
        Snow content.
    water_content : float or numpy.ndarray
        Water content.
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings. The
        default is '../src/CISSEMBEL_CBindings.so'.

    Returns
    -------
    grain_dt_slope : float or numpy.ndarray
        Snow grain evolution slope at given depth(s).

    """
    if isinstance(library, ctypes.CDLL):
        fortlibrary = library
    else:
        fortlibrary = ctypes.cdll.LoadLibrary(library)


    if isinstance(grain_diameter, (float, int)):
        inum = 0
        # Single c-value
        c_grain_diameter = ctypes.c_double(grain_diameter)
        c_snow_content = ctypes.c_double(snow_content)
        c_water_content = ctypes.c_double(water_content)
    else:
        inum = len(snow_content)
        # Number conversion, Fortran-order
        c_grain_diameter = numpy.array(grain_diameter, order="F", dtype=float)
        c_snow_content = numpy.array(snow_content, order="F", dtype=float)
        c_water_content = numpy.array(water_content, order="F", dtype=float)
        # c-pointer
        c_grain_diameter_ptr = c_grain_diameter.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_snow_content_ptr = c_snow_content.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_water_content_ptr = c_water_content.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        # Field sizes
        m4c.compare_field2shapes(snow_content.shape, water_content.shape,
                             'snow_content', 'water_content')
        m4c.compare_field2shapes(snow_content.shape, grain_diameter.shape,
                             'snow_content', 'grain_diameter')

    if inum:
        # array
        grain_dt_slope = numpy.zeros((inum), order="F", dtype=float)
        grain_dt_slope_ptr = grain_dt_slope.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        fortlibrary.c_dgrain_dt.argtypes = [ctypes.c_int,
                                            ctypes.POINTER(ctypes.c_double),
                                            ctypes.POINTER(ctypes.c_double),
                                            ctypes.POINTER(ctypes.c_double),
                                            ctypes.POINTER(ctypes.c_double)]
        fortlibrary.c_dgrain_dt(ctypes.c_int(inum),
                                c_grain_diameter_ptr,
                                c_snow_content_ptr,
                                c_water_content_ptr,
                                grain_dt_slope_ptr)
    else:
        # skalar
        fortlibrary.dgrain_dt.restype = ctypes.c_double
        grain_dt_slope = fortlibrary.dgrain_dt(ctypes.byref(c_grain_diameter),
                                            ctypes.byref(c_snow_content),
                                            ctypes.byref(c_water_content))
    return grain_dt_slope


def darcy_flow(dtime, grain_diameter, snow_density, snow_content, ice_content,
               water_content, thickness,
               library='../src/CISSEMBEL_CBindings.so'):
    """
    C-binding to Fortran function darcy_flow in mod_physic_column in CISSEMBEL
    Growth of snow grains / growth of snow grains profile with depth.

    Parameters
    ----------
    dtime : float
        Time step width.
    grain_diameter : float or numpy.ndarray
        Snow grain diameter.
    snow_density : float or numpy.ndarray
        Snow density.
    snow_content : float or numpy.ndarray
        Snow content.
    ice_content : float or numpy.ndarray
        Ice content.
    water_content : float or numpy.ndarray
        Water content.
    thickness : float or numpy.ndarray
        Layer thickness.
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings. The
        default is '../src/CISSEMBEL_CBindings.so'.

    Returns
    -------
    darcyflow : float or numpy.ndarray
        Darcy flow at given depth(s).

    """
    if isinstance(library, ctypes.CDLL):
        fortlibrary = library
    else:
        fortlibrary = ctypes.cdll.LoadLibrary(library)


    if isinstance(grain_diameter, (float, int)):
        inum = 0
        # Single c-value
        c_grain_diameter = ctypes.c_double(grain_diameter)
        c_snow_density = ctypes.c_double(snow_density)
        c_snow_content = ctypes.c_double(snow_content)
        c_ice_content = ctypes.c_double(ice_content)
        c_water_content = ctypes.c_double(water_content)
        c_thickness = ctypes.c_double(thickness)
    else:
        inum = len(grain_diameter)
        # Number conversion, Fortran-order
        c_grain_diameter = numpy.array(grain_diameter, order="F", dtype=float)
        c_snow_density = numpy.array(snow_density, order="F", dtype=float)
        c_snow_content = numpy.array(snow_content, order="F", dtype=float)
        c_ice_content = numpy.array(ice_content, order="F", dtype=float)
        c_water_content = numpy.array(water_content, order="F", dtype=float)
        c_thickness = numpy.array(thickness, order="F", dtype=float)
        # c-pointer
        c_grain_diameter_ptr = c_grain_diameter.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_snow_density_ptr = c_snow_density.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_snow_content_ptr = c_snow_content.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_ice_content_ptr = c_ice_content.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_water_content_ptr = c_water_content.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_thickness_ptr = c_thickness.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        # Field sizes
        m4c.compare_field2shapes(snow_content.shape, ice_content.shape,
                             'snow_content', 'ice_content')
        m4c.compare_field2shapes(snow_content.shape, water_content.shape,
                             'snow_content', 'water_content')
        m4c.compare_field2shapes(snow_content.shape, snow_density.shape,
                             'snow_content', 'snow_density')
        m4c.compare_field2shapes(snow_content.shape, grain_diameter.shape,
                             'snow_content', 'grain_diameter')
        m4c.compare_field2shapes(snow_content.shape, thickness.shape,
                             'snow_content', 'thickness')

    c_dtime = ctypes.c_double(dtime)

    if inum:
        # array
        darcyflow = numpy.zeros((inum), order="F", dtype=float)
        darcyflow_ptr = darcyflow.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        fortlibrary.c_darcy_flow.argtypes = [ctypes.c_int,
                                             ctypes.c_double,
                                             ctypes.POINTER(ctypes.c_double),
                                             ctypes.POINTER(ctypes.c_double),
                                             ctypes.POINTER(ctypes.c_double),
                                             ctypes.POINTER(ctypes.c_double),
                                             ctypes.POINTER(ctypes.c_double),
                                             ctypes.POINTER(ctypes.c_double),
                                             ctypes.POINTER(ctypes.c_double)]
        fortlibrary.c_darcy_flow(ctypes.c_int(inum),
                                 c_dtime,
                                 c_grain_diameter_ptr,
                                 c_snow_density_ptr,
                                 c_snow_content_ptr,
                                 c_ice_content_ptr,
                                 c_water_content_ptr,
                                 c_thickness_ptr,
                                 darcyflow_ptr)
    else:
        # skalar
        fortlibrary.darcy_flow.restype = ctypes.c_double
        darcyflow = fortlibrary.darcy_flow(ctypes.byref(c_dtime),
                                             ctypes.byref(c_grain_diameter),
                                             ctypes.byref(c_snow_density),
                                             ctypes.byref(c_snow_content),
                                             ctypes.byref(c_ice_content),
                                             ctypes.byref(c_water_content),
                                             ctypes.byref(c_thickness))
    return darcyflow


def calc_refreeze(snow_content, ice_content, water_content, temperature,
                  melt_temperature,
                  library='../src/CISSEMBEL_CBindings.so'):
    """
    C-binding to Fortran function calc_refreeze in mod_physic_column in CISSEMBEL
    Refreezing / refreezing profile with depth.

    Parameters
    ----------
    snow_content : float or numpy.ndarray
        Snow content.
    ice_content : float or numpy.ndarray
        Ice content.
    water_content : float or numpy.ndarray
        Water content.
    temperature : float or numpy.ndarray
        Temperature.
    melt_temperature : float
        Melting temperature.
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings. The
        default is '../src/CISSEMBEL_CBindings.so'.

    Returns
    -------
    refreezing : float or numpy.ndarray
        Refreezing at given depth(s).

    """
    if isinstance(library, ctypes.CDLL):
        fortlibrary = library
    else:
        fortlibrary = ctypes.cdll.LoadLibrary(library)


    if isinstance(snow_content, (float, int)):
        inum = 0
        # Single c-value
        c_snow_content = ctypes.c_double(snow_content)
        c_ice_content = ctypes.c_double(ice_content)
        c_water_content = ctypes.c_double(water_content)
        c_temperature = ctypes.c_double(temperature)
        c_melt_temperature = ctypes.c_double(melt_temperature)
    else:
        inum = len(snow_content)
        # Number conversion, Fortran-order
        c_snow_content = numpy.array(snow_content, order="F", dtype=float)
        c_ice_content = numpy.array(ice_content, order="F", dtype=float)
        c_water_content = numpy.array(water_content, order="F", dtype=float)
        c_temperature = numpy.array(temperature, order="F", dtype=float)
        c_melt_temperature = numpy.array(melt_temperature, order="F", dtype=float)
        # c-pointer
        c_snow_content_ptr = c_snow_content.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_ice_content_ptr = c_ice_content.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_water_content_ptr = c_water_content.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_temperature_ptr = c_temperature.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_melt_temperature_ptr = \
            c_melt_temperature.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        # Field sizes
        m4c.compare_field2shapes(snow_content.shape, ice_content.shape,
                             'snow_content', 'ice_content')
        m4c.compare_field2shapes(snow_content.shape, water_content.shape,
                             'snow_content', 'water_content')
        m4c.compare_field2shapes(snow_content.shape, temperature.shape,
                             'snow_content', 'temperature')
        m4c.compare_field2shapes(snow_content.shape, melt_temperature.shape,
                             'snow_content', 'melt_temperature')

    #c_delta_time = ctypes.c_double(delta_time)

    if inum:
        # array
        refreezing = numpy.zeros((inum), order="F", dtype=float)
        refreezing_ptr = refreezing.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        fortlibrary.c_calc_refreeze.argtypes = [ctypes.c_int,
                                                ctypes.POINTER(ctypes.c_double),
                                                ctypes.POINTER(ctypes.c_double),
                                                ctypes.POINTER(ctypes.c_double),
                                                ctypes.POINTER(ctypes.c_double),
                                                ctypes.POINTER(ctypes.c_double),
                                                ctypes.POINTER(ctypes.c_double)]
        fortlibrary.c_calc_refreeze(ctypes.c_int(inum),
                                    c_snow_content_ptr,
                                    c_ice_content_ptr,
                                    c_water_content_ptr,
                                    c_temperature_ptr,
                                    c_melt_temperature_ptr,
                                    refreezing_ptr)
    else:
        # skalar
        fortlibrary.calc_refreeze.restype = ctypes.c_double
        refreezing = fortlibrary.calc_refreeze(ctypes.byref(c_snow_content),
                                               ctypes.byref(c_ice_content),
                                               ctypes.byref(c_water_content),
                                               ctypes.byref(c_temperature),
                                               ctypes.byref(c_melt_temperature))
    return refreezing


def calc_waterflowdown(snow_content2, ice_content2, water_content2,
                       snow_density2, darcyflow_input=0.0, do_darcy_flow=True,
                       library='../src/CISSEMBEL_CBindings.so'):
    """
    C-binding to Fortran function calc_waterflowdown in mod_physic_column in CISSEMBEL
    Downward flow between two layers.

    Parameters
    ----------
    snow_content2 : numpy.ndarray
        Snow content.
    ice_content2 : numpy.ndarray
        Ice content.
    water_content2 : numpy.ndarray
        Water content.
    snow_density2 : numpy.ndarray
        Snow density.
    darcyflow_in : float
        Darcy flow input. The default is 0.0.
    do_darcy_flow : bool
        Darcy flow input. The default is True.
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings. The
        default is '../src/CISSEMBEL_CBindings.so'.

    Returns
    -------
    flows(darcyflow, waterflow, flag_runoff) : list
        Flows between two layers.

    """
    if isinstance(library, ctypes.CDLL):
        fortlibrary = library
    else:
        fortlibrary = ctypes.cdll.LoadLibrary(library)


    inum = len(snow_content2)
    if inum != 2 :
        print('Length of arrays have have a length of TWO (2)')

    #
    # Input
    #
    # Skalars
    c_darcyflow_input = ctypes.c_double(darcyflow_input)
    c_do_darcy_flow = ctypes.c_bool(do_darcy_flow)

    # Number conversion, Fortran-order
    c_snow_content2 = numpy.array(snow_content2, order="F", dtype=float)
    c_ice_content2 = numpy.array(ice_content2, order="F", dtype=float)
    c_water_content2 = numpy.array(water_content2, order="F", dtype=float)
    c_snow_density2 = numpy.array(snow_density2, order="F", dtype=float)
    # c-pointer
    c_snow_content2_ptr = c_snow_content2.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    c_ice_content2_ptr = c_ice_content2.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    c_water_content2_ptr = c_water_content2.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    c_snow_density2_ptr = c_snow_density2.ctypes.data_as(ctypes.POINTER(ctypes.c_double))

    #
    # Return values
    #  Selection of `DO_RETURN_THREE`:
    #     True  : return_list = (waterflow, darcyflow, flag_runoff)
    #     False : return_list = (waterflow)
    #
    DO_RETURN_THREE = True # TODO Must be identical here and in cbind_mod_physic_column.py
    #
    # Priliminary return values defined as dictionary
    #
    return_dict = {'waterflow':0.0, 'darcyflow':0.0, 'flag_runoff': False}

    if DO_RETURN_THREE:
        return_list = [return_dict['waterflow'],
                       return_dict['darcyflow'],
                       return_dict['flag_runoff']]
        return_array3 = [0., 0., 0.]
        c_return_array3 = numpy.array(return_array3, order="F", dtype=float)
        c_return_array3_ptr = \
            c_return_array3.ctypes.data_as(ctypes.POINTER(ctypes.c_double))

    else:
        return_list = [return_dict['waterflow']]
        return_array1 = [10.]
        c_return_array1 = numpy.array(return_array1, order="F", dtype=float)
        c_return_array1_ptr = \
            c_return_array1.ctypes.data_as(ctypes.POINTER(ctypes.c_double))

    if inum:
        # array
        if DO_RETURN_THREE:
            fortlibrary.c_calc_waterflowdown_return.argtypes = [ctypes.POINTER(ctypes.c_double),
                                                     ctypes.POINTER(ctypes.c_double),
                                                     ctypes.POINTER(ctypes.c_double),
                                                     ctypes.POINTER(ctypes.c_double),
                                                     ctypes.c_double,
                                                     ctypes.POINTER(ctypes.c_double),
                                                     ctypes.c_bool]

            fortlibrary.c_calc_waterflowdown_return(c_snow_content2_ptr,
                                                    c_ice_content2_ptr,
                                                    c_water_content2_ptr,
                                                    c_snow_density2_ptr,
                                                    c_darcyflow_input,
                                                    c_return_array3_ptr,
                                                    c_do_darcy_flow)

            return_list[:2] = c_return_array3[:2]
            #
            # Transform the third (index 2) floating value into a bool value
            #
            return_list[2] = False
            if c_return_array3[2] != 0. :
                return_list[2] = True
        else:
            fortlibrary.c_calc_waterflowdown.argtypes = [ctypes.POINTER(ctypes.c_double),
                                                         ctypes.POINTER(ctypes.c_double),
                                                         ctypes.POINTER(ctypes.c_double),
                                                         ctypes.POINTER(ctypes.c_double),
                                                         ctypes.c_double,
                                                         ctypes.POINTER(ctypes.c_double),
                                                         ctypes.c_bool]

            fortlibrary.c_calc_waterflowdown(c_snow_content2_ptr,
                                             c_ice_content2_ptr,
                                             c_water_content2_ptr,
                                             c_snow_density2_ptr,
                                             c_darcyflow_input,
                                             c_return_array1_ptr,
                                             c_do_darcy_flow)
            return_list = c_return_array1
    else:
        return_list = None

    return return_list
#====================================================================================
def heatflux_between_layers(thickness, temperature, conductivity,
                            library='../src/CISSEMBEL_CBindings.so'):
    """
    C-binding to Fortran function heatflux_between_layers in mod_physic_column in CISSEMBEL
    Heatflux between layers.

    Parameters
    ----------
    thickness : float or numpy.ndarray
        Layer thickness.
    temperature : float or numpy.ndarray
        Temperature.
    conductivity : float or numpy.ndarray
        Thermal heat conductivity.
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings. The
        default is '../src/CISSEMBEL_CBindings.so'.

    Returns
    -------
    heatflux_layers : float or numpy.ndarray
        heatflux between layers.

    """
    if isinstance(library, ctypes.CDLL):
        fortlibrary = library
    else:
        fortlibrary = ctypes.cdll.LoadLibrary(library)


    if isinstance(thickness, (float, int)):
        inum = 0
        # Single c-value
        c_thickness = ctypes.c_double(thickness)
        c_temperature = ctypes.c_double(temperature)
        c_conductivity = ctypes.c_double(conductivity)
    else:
        inum = len(thickness)
        # Number conversion, Fortran-order
        c_thickness = numpy.array(thickness, order="F", dtype=float)
        c_temperature = numpy.array(temperature, order="F", dtype=float)
        c_conductivity = numpy.array(conductivity, order="F", dtype=float)
        # c-pointer
        c_thickness_ptr = c_thickness.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_temperature_ptr = c_temperature.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_conductivity_ptr = c_conductivity.ctypes.data_as(ctypes.POINTER(ctypes.c_double))

    if inum:
        # array
        heatflux_layers = numpy.zeros((inum), order="F", dtype=float)
        heatflux_layers_ptr = heatflux_layers.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        fortlibrary.c_heatflux_between_layers.argtypes = [ctypes.c_int,
                                                ctypes.POINTER(ctypes.c_double),
                                                ctypes.POINTER(ctypes.c_double),
                                                ctypes.POINTER(ctypes.c_double),
                                                ctypes.POINTER(ctypes.c_double)]
        fortlibrary.c_heatflux_between_layers(ctypes.c_int(inum),
                                    c_thickness_ptr,
                                    c_temperature_ptr,
                                    c_conductivity_ptr,
                                    heatflux_layers_ptr)
    else:
        # skalar
        fortlibrary.heatflux_between_layers.restype = ctypes.c_double
        heatflux_layers = fortlibrary.heatflux_between_layers(ctypes.byref(c_thickness),
                                               ctypes.byref(c_temperature),
                                               ctypes.byref(c_conductivity))
    return heatflux_layers

#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
def thermal_diffusion(dtime, thickness, temperature, heatflux_top,
                      heatflux_bottom, snow_content, ice_content,
                      water_content, library='../src/CISSEMBEL_CBindings.so'):
    """
    C-binding to Fortran function calc_thermal_diffusion in mod_physic_column in CISSEMBEL
    Heatflux between layers.

    Parameters
    ----------
    dtime : float
        Time step width.
    thickness : float or numpy.ndarray
        Layer thickness.
    temperature : float or numpy.ndarray
        Temperature.
    heatflux_top : float or numpy.ndarray
        Thermal heat through the layer top (upper interface).
    heatflux_bottom : float or numpy.ndarray
        Thermal heat through the layer bottom (lower interface).
    snow_content : float or numpy.ndarray
        Snow content.
    ice_content : float or numpy.ndarray
        Ice content.
    water_content : float or numpy.ndarray
        Water content.
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings. The
        default is '../src/CISSEMBEL_CBindings.so'.

    Returns
    -------
    temperature_new : float or numpy.ndarray
        updated temperature due to heatflux between layers.

    """
    if isinstance(library, ctypes.CDLL):
        fortlibrary = library
    else:
        fortlibrary = ctypes.cdll.LoadLibrary(library)


    if isinstance(thickness, (float, int)):
        inum = 0
        # Single c-value
        c_thickness = ctypes.c_double(thickness)
        c_temperature = ctypes.c_double(temperature)
        c_heatflux_top = ctypes.c_double(heatflux_top)
        c_heatflux_bottom = ctypes.c_double(heatflux_bottom)
        c_snow_content = ctypes.c_double(snow_content)
        c_ice_content = ctypes.c_double(ice_content)
        c_water_content = ctypes.c_double(water_content)
    else:
        inum = len(thickness)
        # Number conversion, Fortran-order
        c_thickness = numpy.array(thickness, order="F", dtype=float)
        c_temperature = numpy.array(temperature, order="F", dtype=float)
        c_heatflux_top = numpy.array(heatflux_top, order="F", dtype=float)
        c_heatflux_bottom = numpy.array(heatflux_bottom, order="F", dtype=float)
        c_snow_content = numpy.array(snow_content, order='F', dtype=float)
        c_ice_content = numpy.array(ice_content, order='F', dtype=float)
        c_water_content = numpy.array(water_content, order='F', dtype=float)
        # c-pointer
        c_thickness_ptr = c_thickness.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_temperature_ptr = c_temperature.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_heatflux_top_ptr = c_heatflux_top.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_heatflux_bottom_ptr = c_heatflux_bottom.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_snow_content_ptr = c_snow_content.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_ice_content_ptr = c_ice_content.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_water_content_ptr = c_water_content.ctypes.data_as(ctypes.POINTER(ctypes.c_double))

    c_dtime = ctypes.c_double(dtime)

    if inum:
        # array
        temperature_new = numpy.zeros((inum), order="F", dtype=float)
        temperature_new_ptr = temperature_new.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        fortlibrary.c_calc_thermal_diffusion.argtypes = [ctypes.c_int,
                                                ctypes.c_double,
                                                ctypes.POINTER(ctypes.c_double),
                                                ctypes.POINTER(ctypes.c_double),
                                                ctypes.POINTER(ctypes.c_double),
                                                ctypes.POINTER(ctypes.c_double),
                                                ctypes.POINTER(ctypes.c_double),
                                                ctypes.POINTER(ctypes.c_double),
                                                ctypes.POINTER(ctypes.c_double),
                                                ctypes.POINTER(ctypes.c_double)]
        fortlibrary.c_calc_thermal_diffusion(ctypes.c_int(inum),
                                    c_dtime,
                                    c_thickness_ptr,
                                    c_temperature_ptr,
                                    c_heatflux_top_ptr,
                                    c_heatflux_bottom_ptr,
                                    c_snow_content_ptr,
                                    c_ice_content_ptr,
                                    c_water_content_ptr,
                                    temperature_new_ptr)
    else:
        # skalar
        fortlibrary.calc_thermal_diffusion.restype = ctypes.c_double
        temperature_new = fortlibrary.calc_thermal_diffusion(ctypes.byref(c_dtime),
                                                             ctypes.byref(c_thickness),
                                                             ctypes.byref(c_temperature),
                                                             ctypes.byref(c_heatflux_top),
                                                             ctypes.byref(c_heatflux_bottom),
                                                             ctypes.byref(c_snow_content),
                                                             ctypes.byref(c_ice_content),
                                                             ctypes.byref(c_water_content))
    return temperature_new

#====================================================================================
# -----------------------------------------------------------
#
# Main program
#
if __name__ == '__main__':
    import ctypes
    import numpy
    import values_mod_param as physical
    import module4cbind as m4c

    #
    # Example code
    #
    print('Load physical constant')
    physical.constant()
    RHO_ICE = physical.constant.rho_ice
    RHO_SNOW = physical.constant.rho_snow
    CDENSITY = physical.constant.Cdens

    #
    # Load the shared library
    #
    LIBRARY_NAME = '../src/CISSEMBEL_CBindings.so' # INCLUDING path, e.g., ./

    print('= Load shared library containing C-bindings of Fortran code "'
          +LIBRARY_NAME+'"')
    FORTLIBRARY = ctypes.cdll.LoadLibrary(LIBRARY_NAME)
else:
    import ctypes
    import numpy
    import module4cbind as m4c

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Git version control id: $Id$

Created on Sat Nov  5 10:46:27 2022


Call example

(1) Command line use: Same period of monthly values but both keep their time
                      axis

# First: cdo monmean -shifttime,-1s output.nc output_mm.nc
python cutaligntaxis.py \
    --input1 ../src/output_mm.nc \
    --input2 ../data/PROMICE.Baptiste.02.7sites/ORG/PROMICE_monthly_subsurface_temperature_10m.07sites.nc \
    --index_low1 41 --index_high1 131 \
    --index_low2  0 --index_high2 90 \
    --output1 output_mm_trim.nc \
    --output2 surface_temp_trim.nc


(2) Command line use: Same period of monthly values and use the time axis from
                      first file

# First: cdo monmean -shifttime,-1s output.nc output_mm.nc
python cutaligntaxis.py \
    --input1 ../src/output_mm.nc \
    --input2 ../data/PROMICE.Baptiste.02.7sites/ORG/PROMICE_monthly_subsurface_temperature_10m.07sites.nc \
    --index_low1 41 --index_high1 131 \
    --index_low2  0 --index_high2 90 \
    --output1 output_mm_trim.nc \
    --output2 surface_temp_trim.nc \
    --ref_time_file 1 \

(3) Integration into a python framework: Report trimmed files and do not write
                                         files

import xarray
from cutaligntaxis import cutfiles

filename1 = 'output_mm.nc'
filename2 = '../data/PROMICE.Baptiste.02.7sites/ORG/PROMICE_monthly_subsurface_temperature_10m.07sites.nc'
index1 = [41, 131]
index2 = [0, 90]

trimmed_file1, trimmed_file2 = cutfiles(filename1, filename2, index1, index2)


(4) Integration into a python framework: Report trimmed files and write files

import xarray
from cutaligntaxis import cutfiles

filename1 = 'output_mm.nc'
filename2 = '../data/PROMICE.Baptiste.02.7sites/ORG/PROMICE_monthly_subsurface_temperature_10m.07sites.nc'
index1 = [41, 131]
index2 = [0, 90]
filename_out1 = 'output_mm.trimmed.nc'
filename_out2 = 'PROMICE_sub_temp.trimmed.nc'

trimmed_file1, trimmed_file2 = cutfiles(filename1, filename2, index1, index2,
                                        fname_out1=filename_out1,
                                        fname_out2=filename_out2)


(5) Integration into a python framework: Use time axis from first file, report
                                         trimmed files and write files

import xarray
from cutaligntaxis import cutfiles

filename1 = 'output_mm.nc'
filename2 = '../data/PROMICE.Baptiste.02.7sites/ORG/PROMICE_monthly_subsurface_temperature_10m.07sites.nc'
index1 = [41, 131]
index2 = [0, 90]
filename_out1 = 'output_mm.trimmed.nc'
filename_out2 = 'PROMICE_sub_temp.trimmed.nc'
take_taxis_from_file = 1

trimmed_file1, trimmed_file2 = cutfiles(filename1, filename2, index1, index2,
                                        take_taxis_from_file,
                                        fname_out1=filename_out1,
                                        fname_out2=filename_out2)


Dependence : argparse, xarray

@author: Christian Rodehacke, DMI Copenhagen, Denmark

@copyright: Copyright (C) 2022-2025 Christian Rodehacke
"""
# -------------------------------------------------------------------
# This file is part of CISSEMBEL, which is the
# == Copenhagen Ice-Snow Surface Energy and Mass Balance modEL ==
#
# CISSEMBEL is free software; you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE  v.1.2
#
# Information about the EUROPEAN UNION PUBLIC LICENCE (*EUPL*) and
# translations into other European languages are available at
# https://eupl.eu (last access: 2023-09-18)](https://eupl.eu/)
#
# We hope that  this  distributed  CISSEMBEL  code, its  tools and
# documentations are usefull. Any improvements of the code, tools,
# and documentations tools are  very welcome.  Please, consider to
# share your thoughts and improvements with the community.
#
# The documentation,  tools,  and the code are provided as it are.
# NO WARRANTY  ABOUT FITNESS  AND USABILITY OF THE CODE, TOOLS, OR
# DOCUMENTATION  ARE  GIVEN.   NO  WARRANTY  IS  GIVEN  ABOUT  THE
# CORRECTNESS  OF COMPUTED RESULTS WITH THE  PROVIDED CODE, TOOLS,
# AND DOCUMENTATION. YOU USE THE CODE, DOCUMENTATION, AND TOOLS AT
# YOUR OWN RISK.           Removing  of  any  copyright  statement
# or  this  disclaimer  is considered  as a  attempt  to defraud.
#
# -------------------------------------------------------------------

# -------------------------------------------------------------------------
def parse_arguments():
    '''
    Parse the command line arguments

    Returns
    -------
    dict
        parsed command line arguments.

    '''
    parser = argparse.ArgumentParser(
        description='Cut two files and create a common time axis',
        add_help=False)


    parser.add_argument('--input1',
                        nargs='?',
                        required=True,
                        default=None,
                        help='Filename of the first input file')

    parser.add_argument('--input2',
                        nargs='?',
                        required=True,
                        default=None,
                        help='Filename of the second input file')

    parser.add_argument('--index_low1',
                        type=int,
                        nargs='?',
                        required=False,
                        default=0,
                        help='lower time index of first file (default 0)')

    parser.add_argument('--index_high1',
                        type=int,
                        nargs='?',
                        required=False,
                        default=-1,
                        help='higher time index of first file (default -1)')

    parser.add_argument('--index_low2',
                        type=int,
                        nargs='?',
                        required=False,
                        default=0,
                        help='lower time index of second file (default 0)')

    parser.add_argument('--index_high2',
                        type=int,
                        nargs='?',
                        required=False,
                        default=-1,
                        help='higher time index of second file (default -1)')

    parser.add_argument('--ref_time_file',
                        type=int,
                        nargs='?',
                        required=False,
                        default=None,
                        choices=[1, 2, 'None', None],
                        help='Index of the reference time axis: 1st=1 or 2nd=2 file?')

    parser.add_argument('--vname_time1',
                        nargs='?',
                        required=False,
                        default='time',
                        help='variable name of the time in the first file (default: time)')

    parser.add_argument('--vname_time2',
                        nargs='?',
                        required=False,
                        default='time',
                        help='variable name of the time in the second file (default: time)')

    parser.add_argument('--output1',
                        nargs='?',
                        required=False,
                        default=None,
                        help='Filename of the first output file '+ \
                            '(default: None = Do not write the file)')

    parser.add_argument('--output2',
                        nargs='?',
                        required=False,
                        default=None,
                        help='Filename of the second output file '+ \
                            '(default: None = Do not write the file)')

    parser.add_argument('--decode_timeaxis1',
                        action='store_true',
                        required=False,
                        help='Shall be decode the timeaxis of the first file')

    parser.add_argument('--decode_timeaxis2',
                        action='store_true',
                        required=False,
                        help='Shall be decode the timeaxis of the second file')



    if parser.parse_args().input1 == 'None':
        parser.parse_args().input1 = None
    if parser.parse_args().input2 == 'None':
        parser.parse_args().input2 = None

    # # Report used command line arguments
    print("Used settings: read command line arguments or default values")
    for single_arg in vars(parser.parse_args()): #sorted(vars(args)) :
        print('  - {:11s}'.format(single_arg)+' = '+
              str(getattr(parser.parse_args(), single_arg)) )
    print()

    # return parser.parse_args()
    _values = {'input1': parser.parse_args().input1,
               'input2': parser.parse_args().input2,
               'index_low1': parser.parse_args().index_low1,
               'index_high1': parser.parse_args().index_high1,
               'index_low2': parser.parse_args().index_low2,
               'index_high2': parser.parse_args().index_high2,
               'ref_time_file': parser.parse_args().ref_time_file,
               'vname_time1': parser.parse_args().vname_time1,
               'vname_time2': parser.parse_args().vname_time2,
               'output1': parser.parse_args().output1,
               'output2': parser.parse_args().output2,
               'decode_time1': parser.parse_args().decode_timeaxis1,
               'decode_time2': parser.parse_args().decode_timeaxis2,
               }

    return _values


def init_standard_value():
    '''
    Intial standard values of various settings

    Returns
    -------
    _values : dict
        Dictionary of initial/standard values.

    '''
    _values = {'input1': None,
               'input2': None,
               'index_low1': 0,
               'index_high1': 0,
               'index_low2': -1,
               'index_high2': -1,
               'ref_time_file': None,
               'vname_time1': 'time',
               'vname_time2': 'time',
               'output1': 'outputfile_1.nc',
               'output2': 'outputfile_2.nc',
               'decode_time1': False,
               'decode_time2': False,}

    return _values

def cutfiles(fname_in1, fname_in2,
             index1=[0, -1], index2=[0, -1],
             ref_time_file_id=None,
             vname_time1='time', vname_time2='time',
             fname_out1=None, fname_out2=None,
             decode_time1=False, decode_time2=False):
    '''
    Trim two files, use an identical time axis/array if requested, and write
    these out if requested.

    Parameters
    ----------
    fname_in1 : str
        full file name of the first input file.
    fname_in2 : str
        full file name of the second input file.
    index1 : list of int, optional
        DESCRIPTION. The default is [0, -1].
    index2 : list of int, optional
        DESCRIPTION. The default is [0, -1].
    ref_time_file_id : int, optional
        Number of the file having the reference time axis; if None skip it. The default is None.
    vname_time1 : str, optional
        variable name of the time axis in the first input file. The default is None. The default is 'time'.
    vname_time2 : str, optional
        variable name of the time axis in the second input file. The default is None. The default is 'time'.
    fname_out1 : str, optional
        file name of the trimmed first output file; if None skip writing. The default is None.
    fname_out2 : str, optional
        file name of the trimmed second output file; if None skip writing. The default is None.
    decode_time1 : bool, optional
        Shall the read time axis from the first file be decoded? The default is False.
    decode_time2 : bool, optional
        Shall the read time axis from the second file be decoded? The default is False.

    Returns
    -------
    xarray.core.dataset.Dataset
        Trimmed dataset from file1 and file2.

    '''

    def cut_task(fname, index, tname='time', decode_time='False'):
        '''
        Open, trim, and add global attributes to an input file

        Parameters
        ----------
        fname : str
            full file name.
        index : list of int
            Index of the lowest and highest considered record (zero-based).
        tname : str
            variable name of the time axis.

        Returns
        -------
        file_cut : xarray.core.dataset.Dataset
            Trimmed dataset.

        '''
        file_cut = None
        if fname is not None and fname != 'None':
            file_full = None
            try:
                file_full = xr.open_dataset(fname, decode_times=decode_time)
            except NameError:
                print('Failed using "xr", try "xarray"')
                try:
                    file_full = xarray.open_dataset(fname,
                                                    decode_times=decode_time)
                except FileNotFoundError:
                    print('Do not find the file "'+fname+'"')
                except NameError:
                    print('Failed using "xarray" too; SKIP file "'+fname+'"')
            except FileNotFoundError:
                print('Do not find the file "'+fname+'"')

            if file_full is not None:
                if tname == 'time':
                    file_cut = file_full.isel(time=slice(index[0], index[1])).copy()
                else:
                    file_cut = eval('file_full.isel('+tname+
                                 '=slice('+str(index[0])+', '+str(index[1])+')).copy()')
                file_cut.attrs = {'source': fname, 'index_0based':str(index)}

        return file_cut

    #
    # Trim the input files
    #
    file1_cut = cut_task(fname_in1, index1, vname_time1, decode_time1)
    file2_cut = cut_task(fname_in2, index2, vname_time2, decode_time2)

    #
    # If requested, use a common time axis
    #
    if ref_time_file_id == 1 and file1_cut is not None:
        # Reference time axis is found in the first file
        print('Time axis comes from '+fname_in1)
        if file2_cut is not None:
            file2_cut[vname_time2] = file1_cut.get(vname_time1)
    elif ref_time_file_id == 2 and file2_cut is not None:
        # Reference time axis is found in the second file
        print('Time axis comes from '+fname_in2)
        if file1_cut is not None:
            file1_cut[vname_time1] = file2_cut.get(vname_time2)

    #
    # If requested, write out trimmed files
    #
    if fname_out1 is not None and file1_cut is not None:
        print('Create "'+fname_out1+'" from "'+fname_in1+'"')
        file1_cut.to_netcdf(fname_out1)

    if fname_out2 is not None and file2_cut is not None:
        print('Create "'+fname_out2+'" from "'+fname_in2+'"')
        file2_cut.to_netcdf(fname_out2)

    return file1_cut, file2_cut


# -------------------------------------------------------------------------
#
# Main program
#
if __name__ == '__main__':
    import argparse      # argparse : Parse command line arguments
    import xarray as xr  # Xarrray  : Easy handling of netcdf files
    # from datetime import  datetime # Current date/time
    # from pathlib import Path

    _vals_cataxis = init_standard_value()
    _vals_cataxis = parse_arguments()

    #
    # Write the data fields only, if output1 and output2 are both not None
    #
    # cutfiles(_vals_cataxis['input1'],
    #          _vals_cataxis['input2'],
    #          [_vals_cataxis['index_low1'], _vals_cataxis['index_high1']],
    #          [_vals_cataxis['index_low2'], _vals_cataxis['index_high2']],
    #          _vals_cataxis['ref_time_file'],
    #          _vals_cataxis['vname_time1'],
    #          _vals_cataxis['vname_time2'],
    #          _vals_cataxis['output1'],
    #          _vals_cataxis['output2'],)

    #
    # Read the data files and write the the output if output1 and output2 is
    # not None. In addition, report the cut/trimmed files back
    #
    file_trim1 = None
    file_trim2 = None
    file_trim1, file_trim2 = cutfiles(_vals_cataxis['input1'],
                                      _vals_cataxis['input2'],
                                      [_vals_cataxis['index_low1'], _vals_cataxis['index_high1']],
                                      [_vals_cataxis['index_low2'], _vals_cataxis['index_high2']],
                                      _vals_cataxis['ref_time_file'],
                                      _vals_cataxis['vname_time1'],
                                      _vals_cataxis['vname_time2'],
                                      _vals_cataxis['output1'],
                                      _vals_cataxis['output2'],)
    if file_trim1 is not None:
        print('File 1, time period ['+
              str(file_trim1.time.data.min())+' , '+
              str(file_trim1.time.data.max())+']')
    if file_trim2 is not None:
        print('File 2, time period ['+
              str(file_trim2.time.data.min())+' , '+
              str(file_trim2.time.data.max())+']')

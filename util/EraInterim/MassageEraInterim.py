#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 20 09:05:17 2023

@author: Uffe Bundersen, DMI Copenhagen, Denmark

@copyright: Copyright (C) 2022-2023, Uffe Bundersen, Christian Rodehacke
"""
# -------------------------------------------------------------------
# This file is part of CISSEMBEL, which is the
# == Copenhagen Ice-Snow Surface Energy and Mass Balance modEL ==
#
# CISSEMBEL is free software; you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE  v.1.2
#
# Information about the EUROPEAN UNION PUBLIC LICENCE (*EUPL*) and
# translations into other European languages are available at
# https://eupl.eu (last access: 2021-10-01)](https://eupl.eu/)
#
# We hope that  this  distributed  CISSEMBEL  code, its  tools and
# documentations are usefull. Any improvements of the code, tools,
# and documentations tools are  very welcome.  Please, consider to
# share your thoughts and improvements with the community.
#
# The documentation,  tools,  and the code are provided as it are.
# NO WARRANTY  ABOUT FITNESS  AND USABILITY OF THE CODE, TOOLS, OR
# DOCUMENTATION  ARE  GIVEN.   NO  WARRANTY  IS  GIVEN  ABOUT  THE
# CORRECTNESS  OF COMPUTED RESULTS WITH THE  PROVIDED CODE, TOOLS,
# AND DOCUMENTATION. YOU USE THE CODE, DOCUMENTATION, AND TOOLS AT
# YOUR OWN RISK.           Removing  of  any  copyright  statement
# or  this  disclaimer  is considered  as a  attempt  to defraud.
#
# -------------------------------------------------------------------

import xarray as xr
import netCDF4 as nc
import numpy as np
import os
import gc
from cdo import Cdo
import zlib
cdo=Cdo()


#Specifying last YYYYMM
#Note this is important to specify since the 'step0p' files use different
# Indexing than the 'extra' and 'stepX' files.
last_timestep = str(201812)
#%%


def create_massaged_netcdf(nc_file, grid_file, output_folder_regridded = 'test', output_folder_global = 'massaged_global'):
    """
    This function takes care of the 'massaging' of the netcdf files.
    The 'massaging' is neccessary since the fluxes are integrated over
    2 timesteps, before starting from zero.
    For the 'step0p' files we add a new variable 'windspeed10', calculating
    the windspeed at 10m from the velocities given in the file.
    """
    org_filename = nc_file[17:]
    ds = xr.open_dataset(nc_file, decode_times=False).astype('float64')

    # Massaging the 'extra' files.
    if 'extra' in nc_file:
        sshf = ds.variables['sshf']
        sf = ds.variables['sf']
        slhf = ds.variables['slhf']
        e = ds.variables['e']
        for i in range(0, np.shape(sshf)[0], 2):
            sshf[i+1] -= sshf[i]
            sf[i+1] -= sf[i]
            slhf[i+1] -= slhf[i]
            e[i+1] -= e[i]

        #Skips the very last timestep, since no corresponding point exists for
        # the 'extra' files.
        if last_timestep in nc_file:
            ds = ds.isel(time= np.arange(0, len(ds['time'])-1))

    # Massaging the 'stp0p' files.
    # There's some rearraringing of the files, since the 'step0p' files starts
    # at 00:00:00 in new month and ends at 18:00:00 at the end, whereas the
    # 'extra' and 'stepX' files starts at 18:00:00 and ends at 00:00:00
    # Therefore we have to skip the very first datapoint inthe 'step0p' files
    # and append the first point from the following month to every file,
    # except the last.
    if 'step0p' in nc_file:
        ds['wind10'] = np.sqrt((ds.variables['u10'] * ds.variables['u10']) + ds.variables['v10'] * ds.variables['v10']) #(ds.variables['u10']**2 + ds.variables['v10']**2)^(1/2)
        ds = ds.isel(time= np.arange(1, len(ds['time'])))

        if last_timestep in nc_file:
            pass

        else:
            if nc_file[-5:-3] == '12':
                pass
                #ds = ds.isel(time= np.arange(1, len(ds['time'])))
                new_year = str(int(nc_file[-9:-5]) + 1)
                new_month = '01'
                new_nc_file = nc_file[:-9] +new_year + new_month + '.nc'
                ds2 = xr.open_dataset(new_nc_file, decode_times=False).astype('float64')
                ds2 = ds2.isel(time= np.arange(1))
                ds = ds.merge(ds2)
            else:
                new_month = str( int(nc_file[-9:-3]) + 1)
                new_nc_file = nc_file[:-9] + new_month + '.nc'
                ds2 = xr.open_dataset(new_nc_file, decode_times=False).astype('float64')
                ds2 = ds2.isel(time= np.arange(1))
                ds = ds.merge(ds2)


    # 'massaging' the
    if 'stepX' in nc_file:
        ssrd = ds.variables['ssrd']
        strd = ds.variables['strd']
        tp = ds.variables['tp']
        for i in range(0, np.shape(ssrd)[0], 2):
            ssrd[i+1] -= ssrd[i]
            strd[i+1] -= strd[i]
            tp[i+1] -= tp[i]

        #Skips the very last timestep, since no corresponding point exists for
        # the 'extra' files.
        if last_timestep in nc_file:
            ds = ds.isel(time= np.arange(0, len(ds['time'])-1))

    ds_kitchenSink = xr.open_dataset('Invariant/FinalFile.nc', decode_times=False).astype('float64')

    #######################################
    ds = ds.combine_first(ds_kitchenSink)
    ds.to_netcdf(output_folder_regridded + '/' +'global'+ org_filename)
    cdo.remapbil(grid_file ,input= output_folder_regridded + '/' +'global'+ org_filename, output= output_folder_regridded + '/' +'remapped_'+ org_filename)
    os.rename(output_folder_regridded + '/' +'global'+ org_filename, output_folder_global + '/' +'global'+ org_filename)
    ds.close()



def merge_netcdfs_monthly(input_foldername = 'massaged_regridded', output_foldername = 'merged_monthly_regridded', last_year = int(last_timestep[:-2])):
    """
    This function merges the three types of files 'extra', 'step0p' and 'stepX'
    files. Note files are merged from the given timerange, which should be specified
    Standard is the last_timestep as specified at the beginning of this script.

    """
    filenames = sorted(os.listdir(input_foldername))
    merge_month = []
    for year in range(1979, last_year + 1):
        for month in range(1, 12+1):
            for file in filenames:
                if (str(year)+str(month) in file and month>=10) or (str(year)+'0'+str(month) in file and month<=10):
                    merge_month.append(file)
                    if len(merge_month) == 3:
                        ds0 = xr.open_dataset(input_foldername +'/' + merge_month[0])
                        ds1 = xr.open_dataset(input_foldername +'/' + merge_month[1])
                        ds2 = xr.open_dataset(input_foldername +'/' + merge_month[2])
                        ds0 = ds0.merge(ds1)
                        ds0 = ds0.merge(ds2)
                        ds0.to_netcdf(output_foldername +'/'+'merged_monthly_Interim'+str(year)+str(month).zfill(2) + '.nc')

                        #Zipping the outputfile
                        cdo.copy(input=output_foldername +'/'+'merged_monthly_Interim'+str(year)+str(month).zfill(2) + '.nc',
                                 output=output_foldername +'/'+'merged_monthly_Interim'+str(year)+str(month).zfill(2) + '_zipped.nc',
                                 options='-f nc4 -z zip_3')
                        os.remove(output_foldername +'/'+'merged_monthly_Interim'+str(year)+str(month).zfill(2) + '.nc')
                        os.rename(output_foldername +'/'+'merged_monthly_Interim'+str(year)+str(month).zfill(2) + '_zipped.nc',
                                  output_foldername +'/'+'merged_monthly_Interim'+str(year)+str(month).zfill(2) + '.nc')

                        #empty merge_month list
                        merge_month = []

                        # Close datasets
                        ds1.close()
                        ds2.close()
                        ds0.close()




def merge_netcdfs_yearly(input_foldername, output_foldername):
    """
    Merge the monthly Netcdf files into yearly files.

    Parameters
    ----------
    input_foldername : Folder
        Folder containing monthly netcdfs.
    output_foldername : Folder
        Folder containing the output netcdfs.

    Returns
    -------
    None.

    """
    filenames = sorted(os.listdir(input_foldername))
    merge_yearly = []
    for year in range(1979, int(last_timestep[:-2])+1):
        for month in range(1, 12+1):
                if month == 1:
                    ds = xr.open_dataset(input_foldername + '/'+'merged_monthly_Interim'+str(year)+str(month).zfill(2) + '.nc')

                elif month < 12:
                    ds_month = xr.open_dataset(input_foldername + '/'+'merged_monthly_Interim'+str(year)+str(month).zfill(2) + '.nc')
                    ds = xr.merge([ds, ds_month])

                elif month == 12:
                    ds_month = xr.open_dataset(input_foldername + '/'+'merged_monthly_Interim'+str(year)+str(month).zfill(2) + '.nc')
                    ds = xr.merge([ds, ds_month])
                    ds.to_netcdf(output_foldername + '/' + 'merged_yearly_Interim' + str(year) + '.nc')

                    #Zipping the outputfile
                    cdo.copy(input= output_foldername + '/' + 'merged_yearly_Interim' + str(year) + '.nc',
                             output=output_foldername + '/' + 'merged_yearly_Interim' + str(year) + '_zipped.nc',
                             options='-f nc4 -z zip_3')
                    os.remove(output_foldername + '/' + 'merged_yearly_Interim' + str(year) + '.nc')
                    os.rename(output_foldername + '/' + 'merged_yearly_Interim' + str(year) + '_zipped.nc',
                              output_foldername + '/' + 'merged_yearly_Interim' + str(year) + '.nc')

for file in os.listdir('EraInterim.fetch'):
    if 'extra' in file or 'step0' in file or 'stepX' in file:
        create_massaged_netcdf('EraInterim.fetch/'+ file, "CISSEMBEL_init.25km.nc4",
                                output_folder_regridded = 'massaged_regridded_25km', output_folder_global = 'massaged_global')

merge_netcdfs_monthly(input_foldername = 'massaged_regridded_25km', output_foldername = 'merged_monthly_regridded_25km')
merge_netcdfs_monthly(input_foldername = 'massaged_global', output_foldername = 'merged_monthly_global')
merge_netcdfs_yearly(input_foldername= 'merged_monthly_regridded_25km', output_foldername = 'merged_yearly_25km')

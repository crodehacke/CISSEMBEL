#! Git version control id: $Id$

#
# -------------------------------------------------------------------
#
# Copyright (C) 2016-2025 Christian Rodehacke
#
# This file is part of CISSEMBEL, which is the
# == Copenhagen Ice-Snow Surface Energy and Mass Balance modEL ==
#
# CISSEMBEL is free software; you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE  v.1.2
#
# Information about the EUROPEAN UNION PUBLIC LICENCE (*EUPL*) and
# translations into other European languages are available at
# https://eupl.eu (last access: 2023-09-18)](https://eupl.eu/)
#
# We hope that  this  distributed  CISSEMBEL  code, its  tools and
# documentations are usefull. Any improvements of the code, tools,
# and documentations tools are  very welcome.  Please, consider to
# share your thoughts and improvements with the community.
#
# The documentation,  tools,  and the code are provided as it are.
# NO WARRANTY  ABOUT FITNESS  AND USABILITY OF THE CODE, TOOLS, OR
# DOCUMENTATION  ARE  GIVEN.   NO  WARRANTY  IS  GIVEN  ABOUT  THE
# CORRECTNESS  OF COMPUTED RESULTS WITH THE  PROVIDED CODE, TOOLS,
# AND DOCUMENTATION. YOU USE THE CODE, DOCUMENTATION, AND TOOLS AT
# YOUR OWN RISK.           Removing  of  any  copyright  statement
# or  this  disclaimer  is considered  as a  attempt  to defraud.
#
# -------------------------------------------------------------------


## @file Linux-pgf90.mk
#  @brief Include makefile that contain specific information for the
#    environment following the filename scheme: $(OS)-$(FORT).mk ,
#    where the compiler environment is storage in $(COMPILER_ENV). To
#    check its value on different computers run the following command:
#    make print-COMPILER_ENV
#    All different compiler specific files are located in the
#    sub-directory $(Compilers), so to ask to the final filename check
#    make print-COMPILER_INCLUDE_FILE
#
#    We set in this file
#    - Program locations for various Unix programs
#    - Path of required libraries
#    - Compiler specific flags
##

#---------------------------------------
#
# Unix programs (compiler or machine specific commands)
#
# XXX   :=

#---------------------------------------
#
# Flags
#
LINE_LENGTH  := -Mextend -Mfree # flag for free format of long lines
FFLAGS := -Mpreprocess
ifdef DEBUG_FLAGS
	FFLAGS += -g -O0
	FFLAGS += -Mbounds -traceback -Ktrap=fp
else
	FFLAGS += -fast -fastsse -Minline -Mipa=fast
      ifdef PARALLEL_AUTO
		FFLAGS += -Mconcur
		$(warning "Turn on autoparallelization : set OMP_NUM_THREADS")
      endif
endif

ifdef LARGE_MEM_FLAG
	FFLAGS += -mcmodel=medium # for very large code/data exceeding 2GB
endif
ifdef GPROFILE_FLAG
	$(warning "***** Add profiling support -pg ************")
	FFLAGS += -pg
      #GPROF-Options: -Mprof=..., see documentation
endif

ifdef POS_INDEPEN_CODE
  $(warning "***** PIC: partition independent code ******")
  FFLAGS += -fPIC
endif

#
# Libaries
#
CPPFLAGS :=
ifdef NETCDF_SUPPORT
    CPPFLAGS += -I/usr/include/ -L/usr/lib/ -lnetcdff -lnetcdf
    $(warning "Are the CPPFLAGS for NETCD_SUPPORT correctly set for compiler $(FORT) ??")
endif
ifdef OPENMP_PARALLEL
  FFLAGS += -fopenmp
  $(error Please set for compiler pgf90 the missing FFLAGS allowing OpenMP code)
endif
ifdef MPI_PARALLEL
  FFLAGS   += -DMPI_PARALLEL
  CPPFLAGS +=
  FORT     := mpif90
  $(error Please set for compiler pgf90 the missing CPPFLAGS allowing MPI code)
  ifdef MPI_PARALLEL_OUT
    FFLAGS   += -DPARALLEL_IO
  endif
endif

#
# -- Last lines
#

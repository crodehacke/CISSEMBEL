! Git version control id: $Id$
!
! -------------------------------------------------------------------
!
! Copyright (C) 2016-2025 Christian Rodehacke
!
! This file is part of CISSEMBEL, which is the
! == Copenhagen Ice-Snow Surface Energy and Mass Balance modEL ==
!
! CISSEMBEL is free software; you can redistribute it and/or modify
! it under the terms of the EUROPEAN UNION PUBLIC LICENCE  v.1.2
!
! Information about the EUROPEAN UNION PUBLIC LICENCE (*EUPL*) and
! translations into other European languages are available at
! https://eupl.eu (last access: 2023-09-18)](https://eupl.eu/)
!
! We hope that  this  distributed  CISSEMBEL  code, its  tools and
! documentations are usefull. Any improvements of the code, tools,
! and documentations tools are  very welcome.  Please, consider to
! share your thoughts and improvements with the community.
!
! The documentation,  tools,  and the code are provided as it are.
! NO WARRANTY  ABOUT FITNESS  AND USABILITY OF THE CODE, TOOLS, OR
! DOCUMENTATION  ARE  GIVEN.   NO  WARRANTY  IS  GIVEN  ABOUT  THE
! CORRECTNESS  OF COMPUTED RESULTS WITH THE  PROVIDED CODE, TOOLS,
! AND DOCUMENTATION. YOU USE THE CODE, DOCUMENTATION, AND TOOLS AT
! YOUR OWN RISK.           Removing  of  any  copyright  statement
! or  this  disclaimer  is considered  as a  attempt  to defraud.
!
! -------------------------------------------------------------------
!
! Test module to highlight a potential Cray compiler (V15.0.0) bug.
!
! Use call and its output.
!
! > ftn --version && ftn -c -g -G0 -ecinq mod_ftnbug.F08
! Cray Fortran : Version 15.0.0
!
! module mod_ftnbug
!        ^
! ftn-855 ftn: ERROR MOD_FTNBUG, File = mod_ftnbug.F08, Line = 7, Column = 8
!   The compiler has detected errors in module "MOD_FTNBUG".  No module information file will be created for this module.
!
!   real(wp) elemental function theta(rho)
!                               ^
! ftn-1272 ftn: ERROR THETA, File = mod_ftnbug.F08, Line = 92, Column = 31
!   "THETA" is an internal subprogram to ELEMENTAL subprogram "THERMAL_COND_SNOWFIRN".  It must be given the PURE or ELEMENTAL prefix-spec.
!
!   real(wp) elemental function k_ref_firn(rho)
!                               ^
! ftn-1272 ftn: ERROR K_REF_FIRN, File = mod_ftnbug.F08, Line = 113, Column = 31
!   "K_REF_FIRN" is an internal subprogram to ELEMENTAL subprogram "THERMAL_COND_SNOWFIRN".  It must be given the PURE or ELEMENTAL prefix-spec.
!
!   real(wp) elemental function k_ref_snow(rho) result(ksref)
!                               ^
! ftn-1272 ftn: ERROR K_REF_SNOW, File = mod_ftnbug.F08, Line = 133, Column = 31
!   "K_REF_SNOW" is an internal subprogram to ELEMENTAL subprogram "THERMAL_COND_SNOWFIRN".  It must be given the PURE or ELEMENTAL prefix-spec.
!
! Cray Fortran : Version 15.0.0 (20221026200610_324a8e7de6a18594c06a0ee5d8c0eda2109c6ac6)
! Cray Fortran : Compile time:  0.0101 seconds
! Cray Fortran : 144 source lines
! Cray Fortran : 4 errors, 0 warnings, 0 other messages, 0 ansi
! Cray Fortran : "explain ftn-message number" gives more information about each message.
!
! -------------------------------------------------------------------

module mod_ftnbug

  use, intrinsic :: iso_fortran_env, &
       ip => INT32, &
       wp => REAL64

  implicit none

contains

! ------------------------------------------------------------
!
! thermal conductivity of ice: thermal_cond_snowfirn
!
! ----------------------------------------------------
!>
!! @brief Thermal conductivity of combined snow and firn (elemental)
!! @details The diffusion of heat in combined snow and firn depends
!!   on the thermal conductivity of snow and firn
!!   (`thermal_cond_snowfirn`, \f$ k_{sf} \f$). The thermal
!!   conductivity of snow and firn depends predominately on the
!!   density (`firn_density`, \f$\rho \f$) and temperature
!!   (`firn_temp`, \f$T\f$). We use the equation (5) in
!!   Calonne et al. (2019):
!!   \f[ k_{snow\oplus firn}{\rho, T} = (1-\theta)\,
!!            \frac{k_i(T)\,k_a(T)}{k_i^{ref}\,k_a^{ref}}\,
!!            k^{ref}_{snow}(\rho) +
!!          \theta\,\frac{k_i(T)}{k_i^{ref}}\, k^{ref}_{firn}(\rho) \f]
!!
!! @return    thermal_cond_snowfirn ice thermal conductivity (W m-1 K-1)
!!
real(wp) elemental function thermal_cond_snowfirn(firn_temp, &
     & firn_density) result(cond_firn)
  implicit none
  ! Interface
  real(wp), intent(in) :: firn_temp !< firn temperature (Kelvin)
  real(wp), intent(in) :: firn_density !< firn density (kg m-3)
  ! Internal
  real(wp), parameter :: rho_transition = 450.0_wp !< transition density (kg m-3)
  real(wp), parameter :: a_value = 0.02_wp !< (m3 kg-1)
  real(wp), parameter :: k_ref_ice = 2.107_wp !< ice conductivity at T_ref=-3degC (W m-1 K-1)
  real(wp), parameter :: k_ref_air = 0.024_wp !< air conductivity at T_ref=-3degC (W m-1 K-1)
  real(wp), parameter :: rho_ice =  910.0_wp !< Density of ice/glaciers (kg m-3)
  real(wp) :: k_ice_T
  real(wp) :: k_air_T


  k_ice_T = 2.50_wp !Original:removed: k_ice_T = thermal_cond_ice(firn_temp)
  k_air_t = 0.02_wp !Original:removed: k_air_T = thermal_cond_air(firn_temp)

  cond_firn = (1.0_wp-theta(firn_density))* &
       & (k_ice_T*k_air_T)/(k_ref_ice*k_ref_air)* &
       & k_ref_snow(firn_density) &
       & + theta(firn_density)* &
       & (k_ice_T/k_ref_ice)*k_ref_snow(firn_density)

contains
  !>
  !! @brief Theta function describing the weighting of two regressions (elemental)
  !! @details The combination of a linear formulation describing the
  !!   density-conductivity relationship of firn and a quadratic
  !!   regression considering the density-conductivity relationship
  !!   of snow is determined by the weighting factor \f$ \theta\f$.
  !!   This factor depends on density (`rho`, \f$ \rho \f$) in
  !!   respect to the transition density
  !!   (`rho_transistion`, \f$ \rho_{\mathrm{trans}} \f$) at which
  !!   the influence of snow or firn (and ice) dominates. It is part
  !!   of the equation (5) in Calonne et al. (2019):
  !!   \f[ \theta = \left[
  !!      1+\exp\left(-2 a\, (\rho-\rho_{\mathrm{trans}}) \right)
  !!                \right]^{-1} . \f]
  !!
  real(wp) elemental function theta(rho)
    implicit none
    real(wp), intent(in) :: rho
    !
    theta = 1.0_wp/(1.0_wp+exp(-2.0_wp*a_value*(rho-rho_transition)))
  end function theta

  !>
  !! @brief Reference thermal conductivity of firn (elemental)
  !! @details The reference conductivity of firn as a function of its
  !!   density (`rho`, \f$\rho\f$). It is part of the equation (5) in
  !!   Calonne et al. (2019)
  !!   \f[ k_{\mathrm{firn}}^{\mathrm{ref}} = 2.107+
  !!     0.003618\, (\rho-\rho_{\mathrm{ice}}) \f]
  !!   where \f$ \rho_{\mathrm{ice}} \f$ is the density of ice.
  !!
  !!
  real(wp) elemental function k_ref_firn(rho)
    implicit none
    real(wp), intent(in) :: rho
    !
    k_ref_firn = 2.107_wp+0.003618_wp*(rho-rho_ice)
  end function k_ref_firn

  !>
  !! @brief Reference thermal conductivity of snow (elemental)
  !! @details The reference conductivity of snow as a function of its
  !!   density (`rho`, \f$\rho\f$). It is part of the equation (5) in
  !!   Calonne et al. (2019)
  !!   \f[ k_{\mathrm{firn}}^{\mathrm{ref}} = 0.024
  !!     -1.23\cdot 10^{-4}\, \rho  +2.5\cdot 10^{-6}\, \rho^{2}. \f]
  !!
  real(wp) elemental function k_ref_snow(rho) result(ksref)
    implicit none
    real(wp), intent(in) :: rho
    !
    ksref = 0.024_wp-1.23e-4_wp*rho+2.5e-6_wp*rho**2
  end function k_ref_snow
end function thermal_cond_snowfirn

end module mod_ftnbug
!
! --- end of file
!

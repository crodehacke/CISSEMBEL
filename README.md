Copenhagen Ice-Snow Surface Energy and Mass Balance modEL
======================================================================

The **C**openhagen **I**ce-**S**now **S**urface **E**nergy and
  **M**ass **B**alance mod**EL** (**CISSEMBEL**) is a general purpose
  code to determine the Surface Mass and Energy Balance of snow and
  ice-covered regions.


Purpose
=======

**CISSEMBEL** is a simulation code describing the interaction between
  atmosphere and snow/ice. It considers the essential physical
  processes controlling snow evolution. We use this code to simulate
  the growth of glaciers and ice sheets. An ice sheet grows where more
  snow falls as snow melts throughout the year. This area is called
  the accumulation zone because snow is accumulated. In contrast, the
  ice sheet loses mass in places where snow melts more than snow
  falls. This loss is called ablation, which gives this zone the name
  ablation zone.


Main features
=============

### Physical processes

- Radiation and heat fluxes drives the energy balance
  - Internally computed turbulent sensible and latent heat fluxes
  - Provide turbulent sensible and latent heat fluxes (_no_ height
    corrections)
- Internal heat diffusion via implicit Crank-Nicolson scheme
- Refreezing/Percolation
  - liquid water table
- Pore close
- Rain-induced melt
- Vertical advection
- Density evolution
  - Pore close: discharges runoff
- Height corrections
  - Temperature
  - Precipitation (desertification)
  - Long-wave radiation
  - Turbulent heat fluxed if internally computed
- Various albedo schemes
  - Ageing = darkening
  - Snow depth influence
  - Cloud cover
- Static and dynamic depth-axis
  - Static: fast and simple
  - Dynamic: Adaptive grid resolution that follows features
    in the snow/ice column
- Height classes: Builds a _lockup table_ for the surface mass balance
  for changing ice sheet geometries

### Technical capabilities

- Parallelization
  - OpenMP (optional NUMA aware)
  - MPI (Message Passing Interface)
- Timeseries for defined basins
- Driver
  - Fortran
  - Python (restriction on-the-fly)
  - EC-Earth via OASIS, prototype
- On-the-fly computation of mean, maximums, and minimums beside the
  shapshot (point in time) fields for any variable.
- NetCDF output files
  - Compression on-the-fly (requires NetCDF-4 and zlib support)
  - Parallel IO (requires NetCDF-4 and MPI support)
- Numerical precision selection via [src/mod\_numeric\_kinds.F08](src/mod_numeric_kinds.F08).
- Code documentation: doxygen


Requirements
============

**CISSEMBEL** has the following essential requirements

- Fortran compiler following the Fortran 2008 standard
- The NetCDF library for reading and writing data files

For further details, see [INSTALL.md](./INSTALL.md).


Contribution
============

Any contribution is very welcome and you can contribute in many
  ways. The code is hosted at
  [gitlab.com/crodehacke/CISSEMBEL](https://gitlab.com/crodehacke/CISSEMBEL).

You are free to submit feature requests but please keep in mind that
  the development of requested features is funding dependent. Of
  course you can implement any feature and share it with the entire
  community. Nevertheless, report features becasue we may find way to
  get it implemented (:smiley:).

- Bug report at
  [https://gitlab.com/crodehacke/CISSEMBEL/-/issues/new?0issuable_template=Bug](https://gitlab.com/crodehacke/CISSEMBEL/-/issues/new?issuable_template=Bug)
- An issue with the documentation of CISSEMBEL
  [https://gitlab.com/crodehacke/CISSEMBEL/-/issues/new?issuable_template=Documentation](https://gitlab.com/crodehacke/CISSEMBEL/-/issues/new?issuable_template=Documentation)
- An feature request at
  [https://gitlab.com/crodehacke/CISSEMBEL/-/issues/new?issuable_template=Feature](https://gitlab.com/crodehacke/CISSEMBEL/-/issues/new?issuable_template=Feature)
- Any other request, suggestion, ideas: Please report it at
  [https://gitlab.com/crodehacke/CISSEMBEL/-/issues/new?issuable_template=Misc](https://gitlab.com/crodehacke/CISSEMBEL/-/issues/new?issuable_template=Misc)


Abstract
========

**CISSEMBEL** computes the **S**urface **M**ass (and energy)
  **B**alance (_SMB_). The accumulation of snowfall increases the
  surface mass balance. On the other side, various energy fluxes
  warm snow and ice and drive the melting of snow and ice. The
  not-refrozen and not-retained part of the melted snow and ice runs
  off. This runoff decreases the surface mass balance (ablation).

The energy fluxes are incoming and outgoing energy fluxes of
  shortwave/solar radiation, longwave/thermal radiation, turbulent
  fluxes of sensible and latent heat. The energy budget is also
  modified by internal heat diffusion and energy transformation due
  to the changing phase of water, such as refreezing.

![Schematic figure of the Surface Mass Balance](./Documentation/Figure/html/EBM_skizze.png "Sketch of the Surface Mass Balance")

#### CISSEMBEL
At [DMI](http://research.dmi.dk/research), we have developed CISSEMBEL
  to analyze and diagnose the surface mass balance and compute surface
  conditions for ice sheet models. CISSEMBEL's flexible code structure
  also facilitates the coupling with atmospheric models.

Therefore, CISSEMBEL simplifies the integration of ice sheets in
  climate and earth system models, where the atmospheric model drives
  CISSEMBEL and provides thereby boundary conditions for subsequent
  ice sheet model simulations. Central are the downscaling
  capabilities and the optionally available height classes procedure.

![Code structure of CISSEMBEL](./Documentation/Figure/html/CISSEMBEL_struc.png "Code structure.")

The Copenhagen Ice-Snow Surface Energy and Mass Balance modEL
  (CISSEMBEL) is mainly developed by the
  [Danish Meteorological Institute (DMI)](http://www.dmi.dk) in the
  [research and development department](http://research.dmi.dk/research)
  in collaboration with the
  [Alfred Wegener Institute, Helmholtz Centre for Polar and Marine Research](https://www.awi.de/en).


General information
===================

The default branch of CISSEMBEL has the name "__main__".


Citation
========
If you use the code that leads to published scientific papers,
  reports, and monograms, please cite the references listed in
  [CITATION.md](./CITATION.md). Also your publication shall be added
  to the list; Please report it at
  [https://gitlab.com/crodehacke/CISSEMBEL/-/issues/new?issuable_template=Misc](https://gitlab.com/crodehacke/CISSEMBEL/-/issues/new?issuable_template=Misc).


Documentation of the code
=========================

Complete documentation of the code can be obtained by calling:

```
make documentation
```

in the subdirectory [`./src`](./src).

The documentation is available afterward as a PDF file (*refman.pdf*)
  or as a set of HTML web pages (*index.html*) in the build directory.
  The latter includes the search capability for the code and
  documentation. The building of the documentation requires `doxygen`
  and dependent packages such as _latex_ and _graphviz_.

If your browser has problems with the link *index.html*, please go
  into the directory where the link points to (`cd ./doxygen/html/`)
  and open the corresponding *index.html* directly.

Description of specific topics, technical reports, and manuals are
  collected in the directory
  [`./Documentation/topic`](./Documentation/topic).


Build the libraries and binaries
================================

The installation of CISSEMBEL requires building libraries or binaries
  depending on the wanted mode of operation. In the _online_ mode,
  CISSEMBEL is coupled and driven by an atmospheric model. In
  contrast, CISSEMBEL is driven by atmospheric forcing files in the
  _offline_ mode. For further details, inspect the installation
  procedure in [INSTALL.md](./INSTALL.md).


Known issues
============

In case you have problems with the code, please inspect the
  [KNOWN_ISSUES.md](KNOWN_ISSUES.md). It lists known issues, and
  provide solution for some cases.


Change log and ToDo list
========================

The change log and todo list are given in the local file
  [CHANGES.md](CHANGES.md) and [TODO.md](TODO.md),
  respectively. The general code documentation is available via
  doxygen as described above in the section _Documentation of the
  code_.


License
=======
This code is licensed under the EUROPEAN UNION PUBLIC LICENCE
  v. 1.2. See [LICENSE.md](./LICENSE.md). It is an OSI-approved
  license
  [https://opensource.org/license](https://opensource.org/license).

Information about the
  [EUROPEAN UNION PUBLIC LICENCE (*EUPL*)](https://eupl.eu/) and
  translations into other European languages are available at
  [**https://eupl.eu**](https://eupl.eu/) (last access: 2023-09-18).


Contact
=======
Christian Rodehacke
Danish Meteorological Institute (DMI)
Copenhagen
Denmark
<script type="text/javascript"><!--
var bpdxcbs = ['"','m','a','k','d','r','m','<','l',' ','.','t','"','i','o','e','c','k','s','d','d','c','/','.',' ','a','<','i','r','a','l','m','i',':','l','f','"','r','e','a','m','s','"','=','d','i','>','a','>','@','=','@','c','h'];var frkzdzk = [33,35,10,49,19,42,9,50,12,26,22,13,25,37,14,5,16,24,31,23,48,41,51,47,2,36,0,11,4,29,28,20,46,15,38,6,8,17,34,52,45,30,39,32,44,21,53,1,40,18,7,43,27,3];var tozaeof= new Array();for(var i=0;i<frkzdzk.length;i++){tozaeof[frkzdzk[i]] = bpdxcbs[i]; }for(var i=0;i<tozaeof.length;i++){document.write(tozaeof[i]);}
// --></script>
<noscript>cr [at] dmi.dk</noscript>




Appendix
========

## Acknowledgment

Please see [./Documentation/funding.csv](./Documentation/funding.csv)
  for a list of grants supporting the use or development of
  CISSEMBEL.

For the initial developement, the
  [Danish Meteorological Institute (DMI)](http://www.dmi.dk) and
  [Helmholtz centre for polar and marine research the Alfred Wegener
  Institute (AWI)](https://www.awi.de/en/)
  have received funding from the European Union’s Horizon 2020
  research and innovation program under grant agreement 25 no. 869304,
  [PROTECT](https://protect-slr.eu/) (last access: 2023-09-22).

_Thanks to many colleagues_ who have helped to make this program
  ready for use. We would like to thank our various Danish
  collaborators at the
  [Technical University of Denmark (DTU)](https://www.dtu.dk/english/),
  [Aarhus University (UNI-AU)](https://international.au.dk/),
  [Danish Geological Survey (GEUS)](https://eng.geus.dk/),
  and at
  the [University of Copenhagen(KU)](http://www.ku.dk/english) in
  the [Niels Bohr Institute](http://www.nbi.ku.dk/english).

## Disclaimer

**The documentation and code are provided as it is. NO WARRANTY ABOUT
  FITNESS AND USABILITY OF THE CODE, DOCUMENTATION, OR TOOLS ARE
  GIVEN. NO WARRANTY IS GIVEN ABOUT THE CORRECTNESS OF COMPUTED
  RESULTS WITH THE PROVIDED CODE, DOCUMENTATION OR TOOLS. YOU USE THE
  CODE, DOCUMENTATION AND TOOLS AT YOUR OWN RISK. Removing any
  copyright statement or this disclaimer is considered an attempt
  to defraud.**

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 21 20:00:17 CET 2022

C-binding to Fortran functions and subroutines in mod_auxfunc as part of the
Copenhagen Ice Snow Surface Energy and Mass Balance modEL (CISSEMBEL)

@author: Christian Rodehacke, DMI Copenhagen, Denmark

@copyright: Copyright (C) 2016-2025 Christian Rodehacke
"""
# -------------------------------------------------------------------
# This file is part of CISSEMBEL, which is the
# == Copenhagen Ice-Snow Surface Energy and Mass Balance modEL ==
#
# CISSEMBEL is free software; you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE  v.1.2
#
# Information about the EUROPEAN UNION PUBLIC LICENCE (*EUPL*) and
# translations into other European languages are available at
# https://eupl.eu (last access: 2023-09-18)](https://eupl.eu/)
#
# We hope that  this  distributed  CISSEMBEL  code, its  tools and
# documentations are usefull. Any improvements of the code, tools,
# and documentations tools are  very welcome.  Please, consider to
# share your thoughts and improvements with the community.
#
# The documentation,  tools,  and the code are provided as it are.
# NO WARRANTY  ABOUT FITNESS  AND USABILITY OF THE CODE, TOOLS, OR
# DOCUMENTATION  ARE  GIVEN.   NO  WARRANTY  IS  GIVEN  ABOUT  THE
# CORRECTNESS  OF COMPUTED RESULTS WITH THE  PROVIDED CODE, TOOLS,
# AND DOCUMENTATION. YOU USE THE CODE, DOCUMENTATION, AND TOOLS AT
# YOUR OWN RISK.           Removing  of  any  copyright  statement
# or  this  disclaimer  is considered  as a  attempt  to defraud.
#
# -------------------------------------------------------------------

def diffusion_implicit_central(dtime, profile, distance, alpha, source, bottom,
                               library='../src/CISSEMBEL_CBindings.so'):
    """
    C-binding to Fortran subroutine diffusion_implicit_central in mod_auxfunc.F08

    Parameters
    ----------
    dtime : float
        time step width (Second).
    profile : numpy.ndarray
        Profile of a quantity, e.g. temperature, at layers centers (UNIT).
    distance : numpy.ndarray
        Distances between layers centers.
    alpha : numpy.ndarray
        Numerical diffusivity between layers centers (m2 s-1).
    source : numpy.ndarray
        Source term of quantity at layers centers (UNIT).
    bottom : float
        Value of the property below the lowest layer (UNIT).
    library : TYPE, optional
        DESCRIPTION. The default is '../src/CISSEMBEL_CBindings.so'.

    Returns
    -------
    profile_new : numpy.ndarray
        Updated profile of the quantity, e.g. temperature, at layers centers (UNIT).

    """
    if isinstance(library, ctypes.CDLL):
        fortlibrary = library
    else:
        fortlibrary = ctypes.cdll.LoadLibrary(library)

    inum = len(profile)
    if inum <=1:
        print('** cbind_mod_auxfunc:diffusion_implicit_central'+
              ' Length of profile too short: '+str(inum)+'; need at least 2')
        return -9999999999999.9

    # Number conversion, Fortran-order
    c_profile = numpy.array(profile, order="F", dtype=float)
    c_distance = numpy.array(distance, order="F", dtype=float)
    c_alpha = numpy.array(alpha, order="F", dtype=float)
    c_source = numpy.array(source, order="F", dtype=float)
    # c-pointer
    c_profile_ptr = c_profile.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    c_distance_ptr = c_distance.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    c_alpha_ptr = c_alpha.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    c_source_ptr = c_source.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    # Field sizes
    m4c.compare_field2shapes(profile.shape, source.shape, 'profile', 'source')
    m4c.compare_field2shapes(distance.shape, alpha.shape, 'distance', 'alpha')
    m4c.compare_field2shapes(distance.shape, source.shape, 'distance', 'source')
    #
    c_dtime = ctypes.c_double(dtime)
    c_bottom = ctypes.c_double(bottom)

    if inum:
        # array
        profile_new = numpy.zeros((inum), order="F", dtype=float)
        c_profile_new_ptr = profile_new.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        fortlibrary.c_diffusion_implicit_central.argtypes = \
            [ctypes.c_int,
             ctypes.c_double,
             ctypes.POINTER(ctypes.c_double),
             ctypes.POINTER(ctypes.c_double),
             ctypes.POINTER(ctypes.c_double),
             ctypes.POINTER(ctypes.c_double),
             ctypes.c_double,
             ctypes.POINTER(ctypes.c_double)]
        fortlibrary.c_diffusion_implicit_central(ctypes.c_int(inum),
                                                 c_dtime,
                                                 c_profile_ptr,
                                                 c_distance_ptr,
                                                 c_alpha_ptr,
                                                 c_source_ptr,
                                                 c_bottom,
                                                 c_profile_new_ptr)
    return profile_new

def adiff_0to1_cto0(absdiff, diff_critical,
                    library='../src/CISSEMBEL_CBindings.so'):
    """
    C-binding to Fortran function adiff_0to1 _cto0 in

    Parameters
    ----------
    absdiff : numpy.ndarray
        field of (absolute) difference.
    diff_critical : float
        critical difference.
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings. The
        default is '../src/CISSEMBEL_CBindings.so'.

    Returns
    -------
    critierion : numpy.ndarray
        One, where the differnce is zero, and zero, where the difference is
        (beyond) diff_critical, and a linear interpolation in-between.

    """
    if isinstance(library, ctypes.CDLL):
        fortlibrary = library
    else:
        fortlibrary = ctypes.cdll.LoadLibrary(library)

    inum = len(absdiff)
    if inum <=1:
        print('** cbind_mod_auxfunc:adiff_0to1_cto0'+
              ' Length of profile too short: '+str(inum)+'; need at least 2')
        return -9999999999999.9

    # Number conversion, Fortran-order
    c_absdiff = numpy.array(absdiff, order="F", dtype=float)
    c_diff_critical = numpy.array(diff_critical, order="F", dtype=float)

    # c-pointer
    c_absdiff_ptr = c_absdiff.ctypes.data_as(ctypes.POINTER(ctypes.c_double))

    if inum:
        # array
        criterion = numpy.zeros((inum), order="F", dtype=float)
        c_criterion_ptr = criterion.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        fortlibrary.c_adiff_0to1_cto0.argtypes = [ctypes.c_int,
                                                  ctypes.POINTER(ctypes.c_double),
                                                  ctypes.c_double,
                                                  ctypes.POINTER(ctypes.c_double)]
        fortlibrary.c_adiff_0to1_cto0(ctypes.c_int(inum),
                                      c_absdiff_ptr,
                                      c_diff_critical,
                                      c_criterion_ptr)
    return criterion


def rescale_field(reference, field2rescale, mask,
                  library='../src/CISSEMBEL_CBindings.so'):
    """
    C-binding to Fortran function rescale_field in mod_auxfunc in CISSEMBEL
    Factor to rescale a field

    Parameters
    ----------
    reference : numpy.ndarray
        Reference field, source field.
    field2rescale : numpy.ndarray
        DField that shall be rescaled.
    mask : numpy.ndarray of bool/logical
        Mask which points shall be included in the rescaling.
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings. The
        default is '../src/CISSEMBEL_CBindings.so'.

    Returns
    -------
    rescale_factor rescaled_field : float
        Rescaling factor.

    """
    if isinstance(library, ctypes.CDLL):
        fortlibrary = library
    else:
        fortlibrary = ctypes.cdll.LoadLibrary(library)

    inum = len(reference)
    if inum <=1:
        print('** cbind_mod_auxfunc:rescale_field'+
              ' Length of profile too short: '+str(inum)+'; need at least 2')
        return -9999999999999.9

    # Convert NaN to zeros
    reference = numpy.where(numpy.isnan(reference), 0., reference)
    field2rescale = numpy.where(numpy.isnan(field2rescale), 0., field2rescale)
    mask = numpy.where(numpy.isnan(mask), False, mask)

    # Number conversion, Fortran-order
    c_reference = numpy.array(reference, order="F", dtype=float)
    c_field2rescale = numpy.array(field2rescale, order="F", dtype=float)
    c_mask = numpy.array(mask, order="F", dtype=bool)
    # c-pointer
    c_reference_ptr = c_reference.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    c_field2rescale_ptr = c_field2rescale.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    c_mask_ptr = c_mask.ctypes.data_as(ctypes.POINTER(ctypes.c_bool))
    # Field sizes
    m4c.compare_field2shapes(reference.shape, field2rescale.shape,
                             'reference', 'field2rescale')
    m4c.compare_field2shapes(reference.shape, mask.shape, 'reference', 'mask')

    # array function to skalar
    if reference.squeeze().ndim == 2:
        inum = reference.shape[0]
        jnum = reference.shape[1]
        fortlibrary.rescale_field2D.argtypes = [ctypes.c_int,
                                                ctypes.c_int,
                                                ctypes.POINTER(ctypes.c_double),
                                                ctypes.POINTER(ctypes.c_double),
                                                ctypes.POINTER(ctypes.c_bool)]
        fortlibrary.rescale_field2D.restype = ctypes.c_double
        rescale_factor = fortlibrary.rescale_field2D(ctypes.c_int(inum),
                                                     ctypes.c_int(jnum),
                                                     c_reference_ptr,
                                                     c_field2rescale_ptr,
                                                     c_mask_ptr)
    else:
        fortlibrary.rescale_field.argtypes = [ctypes.c_int,
                                              ctypes.POINTER(ctypes.c_double),
                                              ctypes.POINTER(ctypes.c_double),
                                              ctypes.POINTER(ctypes.c_bool)]
        fortlibrary.rescale_field.restype = ctypes.c_double
        rescale_factor = fortlibrary.rescale_field(ctypes.c_int(inum),
                                                   c_reference_ptr,
                                                   c_field2rescale_ptr,
                                                   c_mask_ptr)
    return rescale_factor


def values_at_depth_level(field, depth, depth_subsurface=10.,
                                 library='../src/CISSEMBEL_CBindings.so'):
    """
    C-binding to Fortran function values_at_depth_level in mod_auxfunc in CISSEMBEL
    Interpolation of property field profile on depth_subsurface.

    Parameters
    ----------
    field : numpy.ndarray
        propertiy profile.
    depth : numpy.ndarray
        depth profile.
    depth_subsurface : float
        Target depth. The default is 10..
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings. The
        default is '../src/CISSEMBEL_CBindings.so'.

    Returns
    -------
    value_subsurface : float
        Property value at required depth.

    """
    if isinstance(library, ctypes.CDLL):
        fortlibrary = library
    else:
        fortlibrary = ctypes.cdll.LoadLibrary(library)

    inum = len(field)
    if inum <=1:
        print('** cbind_mod_auxfunc:values_at_depth_level'+
              ' Length of profile too short: '+str(inum)+'; need at least 2')

    # Number conversion, Fortran-order
    c_field = numpy.array(field, order="F", dtype=float)
    c_depth = numpy.array(depth, order="F", dtype=float)
    # c-pointer
    c_field_ptr = c_field.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    c_depth_ptr = c_depth.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    # Field sizes
    m4c.compare_field2shapes(field.shape, depth.shape, 'field', 'depth')
    # Single c-value
    c_depth_subsurface = ctypes.c_double(depth_subsurface)
    # array function to skalar
    fortlibrary.values_at_depth_level.argtypes = [ctypes.c_int,
                                                  ctypes.POINTER(ctypes.c_double),
                                                  ctypes.POINTER(ctypes.c_double),
                                                  ctypes.c_double]
    fortlibrary.values_at_depth_level.restype = ctypes.c_double
    value_subsurface = fortlibrary.values_at_depth_level(ctypes.c_int(inum),
                                                         c_field_ptr,
                                                         c_depth_ptr,
                                                         c_depth_subsurface)
    return value_subsurface

# -----------------------------------------------------------
#
# Main program
#
if __name__ == '__main__':
    import ctypes
    import numpy
    import values_mod_param as physical
    import module4cbind as m4c

    #
    # Example code
    #
    print('Load physical constant')
    physical.constant()
    RHO_ICE = physical.constant.rho_ice
    RHO_SNOW = physical.constant.rho_snow
    CDENSITY = physical.constant.Cdens

    #
    # Load the shared library
    #
    LIBRARY_NAME = '../src/CISSEMBEL_CBindings.so' # INCLUDING path, e.g., ./

    print('= Load shared library containing C-bindings of Fortran code "'
          +LIBRARY_NAME+'"')
    FORTLIBRARY = ctypes.cdll.LoadLibrary(LIBRARY_NAME)
else:
    import ctypes
    import numpy
    import module4cbind as m4c

This script helps prepare the EraInterim data for the CISSEMBEL model.
Note that in the beginning the last timestep should be specified.
This is to setup the functions correctly, as special consideration to the last
timestep is needed for the funcitons


The script consists of three functions:

1. create_massaged_netcdf() adjust the fluxes since in the original EraInterim
data, every other datapoint contains the fluxes of the previous datapoint.
The function also calculates the windspeed at 10 from the veclocities and adds
that the to the final netcdf file.
The files are combined with the file containing static data: 'interim_invariant.nc'.
Finally the function regrids the netcdf to a predetermined grid, and save the
regridded files in one folder and the global files in another.
The function also does some reordering of the datapoints since the ordering is
different in the 'step0p' files than the 'extra' and 'stepX' files. Note that
due to this reordering the final dataset does not include a datapoint for
00:00:00 of the first day of the first month and similarly the last datapoint
is at 18:00:00 on the last day of the last month.

2. The second function, merge_netcdfs_monthly(), merge the three different types of files 'step0',
'extra' and 'stepX', from a given folder, into monthly data. The files should
contain YYYYmm then the function will take care of merging the right files together
THerefore the folder containing the files shouldn't contain more than one type of dataset.

3. The third function, merge_netcdfs_yearly(), merge monthly data into yearly data. This function also
use the naming of the files, so the input folder should only contain the files
that we want to be merged.



Finally there's a script running the functions such to create, a folder containing
the massaged data, both global and regridded, merge the massaged data into monthly
data both global and regridded, and finally merge the regridded data into yearly
data. Since the global files tend to become somewhat large, they are not merged
into yearly data.

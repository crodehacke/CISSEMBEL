#! Git version control id: $Id$

#
# -------------------------------------------------------------------
#
# Copyright (C) 2016-2025 Christian Rodehacke
#
# This file is part of CISSEMBEL, which is the
# == Copenhagen Ice-Snow Surface Energy and Mass Balance modEL ==
#
# CISSEMBEL is free software; you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE  v.1.2
#
# Information about the EUROPEAN UNION PUBLIC LICENCE (*EUPL*) and
# translations into other European languages are available at
# https://eupl.eu (last access: 2023-09-18)](https://eupl.eu/)
#
# We hope that  this  distributed  CISSEMBEL  code, its  tools and
# documentations are usefull. Any improvements of the code, tools,
# and documentations tools are  very welcome.  Please, consider to
# share your thoughts and improvements with the community.
#
# The documentation,  tools,  and the code are provided as it are.
# NO WARRANTY  ABOUT FITNESS  AND USABILITY OF THE CODE, TOOLS, OR
# DOCUMENTATION  ARE  GIVEN.   NO  WARRANTY  IS  GIVEN  ABOUT  THE
# CORRECTNESS  OF COMPUTED RESULTS WITH THE  PROVIDED CODE, TOOLS,
# AND DOCUMENTATION. YOU USE THE CODE, DOCUMENTATION, AND TOOLS AT
# YOUR OWN RISK.           Removing  of  any  copyright  statement
# or  this  disclaimer  is considered  as a  attempt  to defraud.
#
# -------------------------------------------------------------------

## @file Linux-crayftn.16.mk
#  @brief Include makefile that contain specific information for the
#    environment following the filename scheme: $(OS)-$(FORT).mk ,
#    where the compiler environment is storage in $(COMPILER_ENV). To
#    check its value on different computers run the following command:
#    make print-COMPILER_ENV
#    All different compiler specific files are located in the
#    sub-directory $(Compilers), so to ask to the final filename check
#    make print-COMPILER_INCLUDE_FILE
#
#    We set in this file
#    - Program locations for various Unix programs
#    - Path of required libraries
#    - Compiler specific flags
##

#---------------------------------------
#
# Unix programs (compiler or machine specific commands)
#
# XXX   :=

#---------------------------------------
#
# Flags
#
# flag for free format of long lines: LINE_LENGTH
LINE_LENGTH :=
FFLAGS      := -eF
ifdef DEBUG_FLAGS
    # debug level of G:
    # 0   : Full DWARF information; slow and large executable,
    #       most optimizations are disabled incl floating point
    # 1   : Most DWARF information; faster than -G0
    # 2   : Partial DWARF information; most optimizations and tracebacks,
    #       limited brackpoints
    # fast: fast-track, useful in combination with debugger knowning
    #       fast-track debugging.
    FFLAGS += -g -G0
    #FFLAGS += -eacinq
    FFLAGS += -ecinq
    FFLAGS += -craype-verbose  # Only Cray; all settings: lib path, flags, ...
else
    # -O ipa3 -O vector2
    FFLAGS += -O 2
    ifdef PARALLEL_AUTO
        FFLAGS += -O autothread
        $(warning "Turn on autoparallelization : set OMP_NUM_THREADS")
    endif
    FFLAGS += -DNDEBUG
endif

ifdef LARGE_MEM_FLAG
  FFLAGS +=
endif
ifdef GPROFILE_FLAG
    $(warning "*********************************************")
    $(warning "*                                           *")
    $(warning "*   For profiling load the corresponding    *")
    $(warning "*    modules BEFORE compiling the code      *")
    $(warning "*                                           *")
    $(warning "*>     module load perftools-lite       OR  *")
    $(warning "*>     module load perftools            OR  *")
    $(warning "*>    (module load perftools-base)          *")
    $(warning "*                                           *")
    $(warning "*                                           *")
    $(warning "*********************************************")
    $(warning "*                                           *")
    $(warning "*       NOT ADDING profiling support        *")
    $(warning "*    EXCEPT the above modules are loaded    *")
    $(warning "*    during the compilation of the code     *")
    $(warning "*                                           *")
    $(warning "*********************************************")
    #FFLAGS += -pg
endif

ifdef POS_INDEPEN_CODE
  $(warning "***** PIC: partition independent code ******")
  FFLAGS += -fPIC
endif

#
# Libaries
#
CPPFLAGS :=
ifdef NETCDF_SUPPORT
    CPPFLAGS +=
endif
ifdef OPENMP_PARALLEL
  FFLAGS += -homp
else
  FFLAGS += -hnoomp
endif
ifdef MPI_PARALLEL
  FFLAGS   += -DMPI_PARALLEL
  CPPFLAGS +=
  ifdef MPI_PARALLEL_OUT
    FFLAGS   += -DPARALLEL_IO
  endif
endif

$(warning "*********************************************")
$(warning "*                                           *")
$(warning "*  Cray Fortran compiler CEE (Version 16)   *")
$(warning "*  may fail due to a compiler error         *")
$(warning "*                                           *")
$(warning "*********************************************")

#
# -- Last lines
#

# Known Issues


This document lists known issues with CISSEMBEL and possible workarounds.

## Dependency issues


### Wrong dependence of `netcdf.mod`

You may see during the first compilation the following error message:

```
make: *** No rule to make target '/usr/include/netcdf.mod', needed by 'mod_io.o'.  Stop.
```

In this case, the included file `./src/depend.mk` does not fit your
 computer environment. Try to rebuild it by calling `make depend`. If this
 fails, you can delete the file `./src/depend.mk`. Successive calls of
 `make` will create an empty `depend.mk` file, which allows compiling
 the code. However, any attempt to run a parallel building process may
 fail, e.g., `make -j 4`.

#### `make depend` on Cray systems for a _gcc_ environment (`PrgEnv-gnu`)

On some Cray systems, the selected compiler is called via the generic
 compiler command `ftn`. Even for a selected _gcc_ program
 environment (`PrgEnv-gnu`), the recreation of `./src/depend.mk`
 fails. In this case, you replace in the displayed command `ftn` by
 `gfortran` to build the `depend.mk` by hand. For example, you see in
 the call of `make depend` the following output:

```
ftn -cpp -O2 -march=znver1 -mfpmath=sse -DNDEBUG -M mod_numeric_kinds.F08 mod_ebmmpi.F08 mod_general.F08 mod_param.F08 mod_fields.F08 mod_attr_param.F08 mod_io.F08 mod_auxfunc.F08 mod_physic.F08 mod_albedo.F08 mod_physic_column.F08 mod_dynamic_column.F08 mod_static_column.F08 mod_varllst.F08 mod_aux4offdriver.F08 modEBMcolumn.F08 mod__env.F08 modEBMworld.F08 > depend.mk
gfortran: error: unrecognized command line option ‘-Mmod_numeric_kinds.F08’
```

Just copy the line, replace `ftn` with `gfortran` and call the
 modified command manually on the command line, e.g.:

```
gfortran -cpp -O2 -march=znver1 -mfpmath=sse -DNDEBUG -M mod_numeric_kinds.F08 mod_ebmmpi.F08 mod_general.F08 mod_param.F08 mod_fields.F08 mod_attr_param.F08 mod_io.F08 mod_auxfunc.F08 mod_physic.F08 mod_albedo.F08 mod_physic_column.F08 mod_dynamic_column.F08 mod_static_column.F08 mod_varllst.F08 mod_aux4offdriver.F08 modEBMcolumn.F08 mod__env.F08 modEBMworld.F08 > depend.mk
```

In any case, the safest procedure is to delete the file
 `./src/depend.mk`.


## Operation system-related issues


### Creation of the documentation via `make documentation` fails

For Ubuntu 20.04 (LTS), the installed Doxygen version 1.8.17 and gs
 version 9.50 are not compatible. See the issue at
 [bugs.launchpad.net/ubuntu/+source/doxygen/+bug/1926353](https://bugs.launchpad.net/ubuntu/+source/doxygen/+bug/1926353).

It has been suggested to install the updated Doxygen version 1.9.1
 which is available at
 [github.com/doxygen/doxygen](https://github.com/doxygen/doxygen)
 [date: 2023-09-06].

**Current fix**: Please adjust the the setting of `USE_MATHJA` in the
 file [./src/Doxyfile](./src/Doxyfile). Use `USE_MATHJAX = YES`,
 which allows to generate the documentation as a PDF-file and as
 HTML-pages. However, the equations are not rendered correctly in the
 HTML documents.

The problem disappeared in Ubuntu 24.04 (LTS).


### Message Passing Interface (MPI)

The standard Message Passing Interface (MPI) libraries and their
 dependencies, such as HDF5, provided as Ubuntu packages from the
 standard Ubuntu repository, do not support parallel writing. It leads
 to a failure if you compile CISSEMBEL with MPI support.

To fix the problem, you have to build your own OpenMP, HDF, and NetCDF
 (supporting C and Fortran) libraries. See, for example,
 [gist.github.com/milancurcic/3a6c1a97a99d291f88cc61dae6621bdf](https://gist.github.com/milancurcic/3a6c1a97a99d291f88cc61dae6621bdf)
 [date: 2022-11-24].



## Know compiler issues

### The Cray Fortran compiler : Version 15.0.0 and 16.0.0

The Cray Fortran compiler (Version 15.0.0 and 16.0.0) fails to compile
 the module `mod_physic.F08` and, in paricular, the embedded elemental
 functions `theta`, `k_ref_firn` and `k_ref_snow` of the elemental
 function `thermal_cond_snowfirn`. The compiler does not recognize
 that the embedded functions are elemental functions and reports
 erroneously:

```
"THETA" is an internal subprogram to ELEMENTAL subprogram "THERMAL_COND_SNOWFIRN".  It must be given the PURE or ELEMENTAL prefix-spec.
"K_REF_FIRN" is an internal subprogram to ELEMENTAL subprogram "THERMAL_COND_SNOWFIRN".  It must be given the PURE or ELEMENTAL prefix-spec.
"K_REF_SNOW" is an internal subprogram to ELEMENTAL subprogram "THERMAL_COND_SNOWFIRN".  It must be given the PURE or ELEMENTAL prefix-spec.
```

To reproduce this error, you may compile the test module code
 `./test/mod_ftnbug.F08` as described in its file header.



## Doxygen

The used documentation system _doxygen_ fails during the first call of
 `make documentation`
 with the error messages like following one:
```
[ ... shortend output ... ]
Preprocessing /home/USER/src/CISSEMBEL/src/mod_auxfunc.F08...
Parsing file /home/USER/src/CISSEMBEL/src/mod_auxfunc.F08...
********************************************************************
Error in file /home/USER/src/CISSEMBEL/src/mod_auxfunc.F08 line: 2655, state: 4
********************************************************************
makefile:817: recipe for target 'documentation' failed
make: *** [documentation] Segmentation fault (core dumped)
```

Often, long or more complex format constructions and related write
 statements leads to this failure. On Ubuntu 18.04.6 LTS, a second
 call of doxygen (version 1.8.13) passes these bad constructs and,
 ultimately, build a the documentations in some cases.

Alternatively, you may try make a rigorous clean to have a fresh
 start. Therefore, you call `make proper` followed the generation of
 the documentation: `make documentation`.


## Python

Python can be used to drive CISSEMBEL or to test the behavior of
 various parameterizations, functions, and subroutines via Fortran's
 standardized C-interface (requires Fortran 2008 or newer). You may
 experience a mismatch between libraries used to compile the code and
 in the python environment that runs. For example, you may face the
 following error under Ubuntu focal (20.04 LTS) while running it under
 your onw (Ana)Conda environment:
`
OSError: /lib/x86_64-linux-gnu/libp11-kit.so.0: undefined symbol: ffi_type_pointer, version LIBFFI_BASE_7.0
`
In this case you may use the provided YAML files:
 [./examples/environment/cissembel.yaml](./examples/environment/cissembel.yaml). Use
 `conda env create cissembel -f ./examples/environment/cissembel.yaml`
 to create a working environment for the report OS environment.


## Message Passing Interface (MPI)

### High number of MPI processes for low number of grid points

In the case of a high fraction of MPI processes to grid points, e.g.,
 four (**4**) MPI processes for seven (**7**) grid points in total,
 starting _CISSEMBEL_ may fail with the following error message, even
 if the "not found" variable exists in the read file:

```
NetCDF library message [-49] : NetCDF: Variable not found
                     + [-49] : ncid=    65536
```

Current fix: Try to restart the run, which may require several
 attempts.


### Parallel reading and writing require NetCDF4-aware files

NetCDF files following the NetCDF4 standard are required to support
 parallel reading and writing files in CISSEMBEL. It means either the
 pure NetCDF4 format (`NF90_NETCDF4`) or the mixed classical NetCDF3
 plus NetCDF4 format (`ior(NF90_NETCDF4, NF90_CLASSIC_MODEL)` =
 `NF90_NETCDF4_CLASSIC`). Please set the namelist parameter
 `netcdf_create_mode` in the section `&write2files` accordingly.

For IO-heavy simulations with a high output frequency of big files,
 using NetCDF4 output files potentially leads to a longer model run
 time. Under such circumstances, activating compressed output via
 setting the namelist parameter `do_ncvar_compression = T` may reduce
 the run time and required storage space. You can combine compression
 and parallel IO if the linked libraries support it.


### Single Station support in the MPI mode

The parallel use of the MPI mode and the support for writing data at
 defined grid locations, called stations, may not work. Therefore, the
 code stops with an error message, even if it passed the vulnerable
 code part.

The problem raises a
 `SIGSEGV: Segmentation fault - invalid memory reference` during the
 generation of a variable by the NetCDF library.

The station support is controlled by the namelist parameter
 `do_stations` in the namelist section `run_ctrl`. Disable it by
 setting `do_stations = F,`.

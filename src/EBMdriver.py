#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Git version control id: $Id$

Created on Fri Nov 26 14:28:07 CET 2021
Modified: 03 Nov 2022 : Modularization by Christian Rodehacke, DMI


The Copenhagen Ice Snow Surface Energy and Mass Balance modEL (CISSEMBEL)
Python frontend. It calls via a Fortran wrapper CISSEMBEL's main subroutines
EBMinitialize, EBMworld, and EBMfinalize located in the Fortran module
modEBMworld. The bridge between the Python and Fortran code is implemented
via C-bindings as part of the Fortran2008 standard. For details inspect the
subroutine cwrapper_CISSEMBEL_standard in modEBMworld.F08. The related
c-binding occurs via the symbol named c_CISSEMBEL in a shared library defined
by LIBRARY_NAME.

Since this code loads all the data files at once into the main memory, it has
a larger main memory footprint than the corresponding Fortran driver
(EBMdriver.F08). Since EBMdriver.F08 reads the atmospheric forcing fields in
sequential order, its main memory footprint is lower but sequential reading
may require a longer execution time.

NOTE: To run CISSEMBEL via this python frontend, it is nessary to compile the
      code with the C-bindings. Please use the following make command on the
      command line in the ./src directory:

      make CISSEMBEL4py

      Before running CISSEMBEL via this Python frontend, please check
      - the settings in the namelist namelist.ebm, which control different
        parts of CISSEMBEL
      - the flags and settings in cEBMdriver.py. Here, in particular, the
        data source (filename) and the variable names.
      - if the path to the Python module cbind_modEBMworld is correctly via
        the variable PYTHONPATH_TO_cbind_modEBMworld. Otherwise it would not
        work and you see the following error message:
        ModuleNotFoundError: No module named 'cbind_modEBMworld'.

      To run CISSEMBEL via Python you may use

      python EBMdriver.py


NOTE: Since this Python frontend gives the control to the CISSEMBEL (Fortran),
      just killing the python program may not immediately kill CISSEMBEL too.
      It may take a little moment.


Dependence : numpy, xarray, ctypes, os, sys
Optional : f90nml

@author: Christian Rodehacke, DMI Copenhagen, Denmark
"""
# -------------------------------------------------------------------
# This file is part of CISSEMBEL, which is the
# == Copenhagen Ice-Snow Surface Energy and Mass Balance modEL ==
#
# CISSEMBEL is free software; you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE  v.1.2
#
# Information about the EUROPEAN UNION PUBLIC LICENCE (*EUPL*) and
# translations into other European languages are available at
# https://eupl.eu (last access: 2023-09-18)](https://eupl.eu/)
#
# We hope that  this  distributed  CISSEMBEL  code, its  tools and
# documentations are usefull. Any improvements of the code, tools,
# and documentations tools are  very welcome.  Please, consider to
# share your thoughts and improvements with the community.
#
# The documentation,  tools,  and the code are provided as it are.
# NO WARRANTY  ABOUT FITNESS  AND USABILITY OF THE CODE, TOOLS, OR
# DOCUMENTATION  ARE  GIVEN.   NO  WARRANTY  IS  GIVEN  ABOUT  THE
# CORRECTNESS  OF COMPUTED RESULTS WITH THE  PROVIDED CODE, TOOLS,
# AND DOCUMENTATION. YOU USE THE CODE, DOCUMENTATION, AND TOOLS AT
# YOUR OWN RISK.           Removing  of  any  copyright  statement
# or  this  disclaimer  is considered  as a  attempt  to defraud.
#
# -------------------------------------------------------------------



# ---------------------------------------------------------------------------
#
# Subroutines and functions
#
def definition_settings():
    '''
    Definition of general settings

    Returns
    -------
    None.

    '''
    # ---------------------------------------------------------------------
    #
    # Settings
    #
    global CHECK_FIELD_SHAPES_WITH_INITIAL_FILE, \
        USE_DUMMY_MASK, \
        LIBRARY_NAME, \
        CHECK_FIELD_SHAPES_WITH_INITIAL_FILE, \
        USE_ABSOLUTE_TIME_AXIS, \
        NAMELIST_FILE_CISSEMBEL

    #
    # Switches/Flags
    #
    USE_DUMMY_MASK = True # If no glacier mask is available, activate it

    #
    # Library including path holding the c-bindings for CISSEMBEL
    #
    LIBRARY_NAME = '../src/CISSEMBEL_CBindings.so' # INCLUDING path, e.g., ./

    #
    # Initial file (shall be identical to the one given in namelist.ebm)
    #
    CHECK_FIELD_SHAPES_WITH_INITIAL_FILE = True

    #
    # Shall we create an absolute time arrays in seconds
    #
    USE_ABSOLUTE_TIME_AXIS = True

    #
    # Namelist file
    #
    NAMELIST_FILE_CISSEMBEL = 'namelist.ebm'

def read_forcing_data(_setup, _dummy_mask=True):
    '''
    Read the atmospheric forcing data at once

    Parameters
    ----------
    _setup : str
        Setup tag used to select a bunch of setup definitions.
    _dummy_mask : bool
        Shall we consider all grid points glacieried? The default is True,
        otherwise read a glacier mask

    Returns
    -------
    time_array : numpy.ndarray
        Time information array.
    time_list : numpy.ndarray
        List of date/time information, with the fields: year, month, day, hour,
        minute, and second.
    zsurface : numpy.ndarray
        Surface elevation of the force data orography; unit: meter.
    air_temperature : numpy.ndarray
        Near-surface air temperature, typically at 2 meter; unit: Kelvin.
    surface_short_radiation_down : numpy.ndarray
        Downward directed shortwave radiation at the surface; unit: W m-2.
    surface_long_radiation_down : numpy.ndarray
        Downward directed longwave radiation at the surface; unit: W m-2
    total_precipitation : numpy.ndarray
        Total precipitation (rain, snow, and ice fall) at the surface; unit: kg s-1 m-2.
    dew_temperature : numpy.ndarray
        Near-surface dew point temperature, typically at 2 meter; unit: Kelvin.
    atmos_surface_pressure : numpy.ndarray
        Near-surface atmospheric pressure, not sea-level pressure; unit: Pascal.
    wind_speed : numpy.ndarray
        Near-surface wind velocity, typicaly at 10m height; unit: m s-1.
    cloudfraction : numpy.ndarray
        Total cloud fraction.
    mask : numpy.ndarray
        Mask indicating glaciated regions: True=glaciated.

    '''

    global FORCING_FILE
    global NAMELIST_FILE_RESTART0
    global NAMELIST_FILE_CONTINUE
    global TIME_STEP_SECONDS
    global INITIAL_FILE_CISSEMBEL, VNAME_2D_INITIAL_FIELD

    #
    # For each read variable we solve the equation:
    #   var = factor*var + offsett
    # which allows to simply rescale fields.
    #
    factor_time = 1.0
    factor_zsurface = 1.0
    factor_air_temperature = 1.0
    factor_surface_short_radiation_down = 1.0
    factor_surface_long_radiation_down = 1.0
    factor_total_precipitation = 1.0
    factor_dew_temperature = 1.0
    factor_atmos_surface_pressure = 1.0
    factor_wind_speed = 1.0
    factor_cloudfraction = 1.0

    offset_time = 0.0
    offset_zsurface = 0.0
    offset_air_temperature = 0.0
    offset_surface_short_radiation_down = 0.0
    offset_surface_long_radiation_down = 0.0
    offset_total_precipitation = 0.0
    offset_dew_temperature = 0.0
    offset_atmos_surface_pressure = 0.0
    offset_wind_speed = 0.0
    offset_cloudfraction = 0.0

    #
    # SECTION of file setting for a given `_setup`.
    #
    if _setup == 'GrIS_EraInterim_100km':
        if CHECK_FIELD_SHAPES_WITH_INITIAL_FILE:
            # Name including path of the initial file
            INITIAL_FILE_CISSEMBEL = \
                '../examples/data.initial.greenland/Greenland_100km.kaxis10.nc'
            # Variable name used to check the dimensions between forcing and
            # initial file
            VNAME_2D_INITIAL_FIELD = 'usrf' # e.g., target topograpy, basins, ...

        #
        # Atmospheric forcing file(s)
        #
        TIME_STEP_SECONDS = 6.0*3600. # We set it because the time axis might be corrupt
        FORCING_FILE = 'ATMOS_Month00.1979.100km.nc'

        # Variable names of the forcing fields found in the FORCING_FILE
        vname_time = 'time'
        vname_zsurface = 'zs'
        vname_air_temperature = 'temp2'
        vname_surface_short_radiation_down = 'sradsd'
        vname_surface_long_radiation_down = 'tradsd'
        vname_total_precipitation = 'tprec'
        vname_dew_temperature = 'dew2'
        vname_atmos_surface_pressure = 'aps'
        vname_wind_speed = 'wind10'
        vname_cloudfraction = 'aclcov'
        vname_mask = 'mask'

    elif _setup == 'PROMICE_EraInterim':
        if CHECK_FIELD_SHAPES_WITH_INITIAL_FILE:
            # Name including path of the initial file
            INITIAL_FILE_CISSEMBEL = \
                '../data/PROMICE/promice_initial.ATMOS_xxxx.GrIS.ISMIP6.nc'
            # Variable name used to check the dimensions between forcing and
            # initial file
            VNAME_2D_INITIAL_FIELD = 'surface_elevation' # e.g., target topograpy, basins, ...

        #
        # Atmospheric forcing file(s)
        #
        TIME_STEP_SECONDS = 6.0*3600. # We set it because the time axis might be corrupt
        FORCING_FILE = '../data/PROMICE/simplegridfrompromice.nc.ATMOS_1979_2017.GrIS.ISMIP6.nc'

        # Variable names of the forcing fields found in the FORCING_FILE
        vname_time = 'time'
        vname_zsurface = 'zs'
        vname_air_temperature = 't2m'
        vname_surface_short_radiation_down = 'ssrd'
        vname_surface_long_radiation_down = 'strd'
        vname_total_precipitation = 'pr'
        vname_dew_temperature = 'd2m'
        vname_atmos_surface_pressure = 'sp'
        vname_wind_speed = 'wind10'
        vname_cloudfraction = 'tcc'
        vname_mask = 'mask'

    elif _setup == 'PROMICE_EraInterim2014':
        if CHECK_FIELD_SHAPES_WITH_INITIAL_FILE:
            # Name including path of the initial file
            INITIAL_FILE_CISSEMBEL = \
                '../data/PROMICE/promice_initial.ATMOS_xxxx.GrIS.ISMIP6.nc'
            # Variable name used to check the dimensions between forcing and
            # initial file
            VNAME_2D_INITIAL_FIELD = 'surface_elevation' # e.g., target topograpy, basins, ...

        #
        # Atmospheric forcing file(s)
        #
        TIME_STEP_SECONDS = 6.0*3600. # We set it because the time axis might be corrupt
        FORCING_FILE = '../data/PROMICE/simplegridfrompromice.nc.ATMOS_2014.GrIS.ISMIP6.nc'

        # Variable names of the forcing fields found in the FORCING_FILE
        vname_time = 'time'
        vname_zsurface = 'zs'
        vname_air_temperature = 't2m'
        vname_surface_short_radiation_down = 'ssrd'
        vname_surface_long_radiation_down = 'strd'
        vname_total_precipitation = 'pr'
        vname_dew_temperature = 'd2m'
        vname_atmos_surface_pressure = 'sp'
        vname_wind_speed = 'wind10'
        vname_cloudfraction = 'tcc'
        vname_mask = 'mask'

    elif _setup == 'PURE_promice_B_EraInterim2007_2017.ver02.7sites':
        TIME_STEP_SECONDS = 6.0*3600. # We set it because the time axis might be corrupt
        VNAME_2D_INITIAL_FIELD = 'surface_elevation' # e.g., target topograpy, basins, ...

        # Variable names of the forcing fields found in the FORCING_FILE
        vname_time = 'time'
        vname_zsurface = 'zs'
        vname_air_temperature = 't2m'
        vname_surface_short_radiation_down = 'ssrd'
        vname_surface_long_radiation_down = 'strd'
        vname_total_precipitation = 'pr'
        vname_dew_temperature = 'd2m'
        vname_atmos_surface_pressure = 'sp'
        vname_wind_speed = 'wind10'
        vname_cloudfraction = 'tcc'
        vname_mask = 'mask'

        #
        # Atmospheric forcing file(s)
        #
        FORCING_FILE = '../data/PROMICE.Baptiste.02.7sites/Forcing.PROMICE_Baptiste.nc.ATMOS_2007_2017.GrIS.ISMIP6.nc'
        # Name including path of the initial file
        INITIAL_FILE_CISSEMBEL = \
            '../data/PROMICE.Baptiste.02.7sites/PROMICE_Baptiste_initial.ATMOS_xxxx.GrIS.ISMIP6.nc'



    elif 'PROMICE_B_EraInterim2007_2017' in _setup:
        TIME_STEP_SECONDS = 6.0*3600. # We set it because the time axis might be corrupt
        VNAME_2D_INITIAL_FIELD = 'surface_elevation' # e.g., target topograpy, basins, ...

        # Variable names of the forcing fields found in the FORCING_FILE
        vname_time = 'time'
        vname_zsurface = 'zs'
        vname_air_temperature = 't2m'
        vname_surface_short_radiation_down = 'ssrd'
        vname_surface_long_radiation_down = 'strd'
        vname_total_precipitation = 'pr'
        vname_dew_temperature = 'd2m'
        vname_atmos_surface_pressure = 'sp'
        vname_wind_speed = 'wind10'
        vname_cloudfraction = 'tcc'
        vname_mask = 'mask'

        # Fallback values, which may be overwritten below.
        # Please inspect the following if-elif construct analysing _setup
        NAMELIST_FILE_RESTART0 = 'namelist.ebm.PROMICE_B.restart0'
        NAMELIST_FILE_CONTINUE = 'namelist.ebm.PROMICE_B.continue'

        if _setup == 'PROMICE_B_EraInterim2007_2017':
            # Atmospheric forcing file(s)
            #
            FORCING_FILE = \
                '../data/PROMICE.Baptiste.01/PROMICE_Baptiste_initial.nc.ATMOS_2007_2017.GrIS.ISMIP6.nc'
            NAMELIST_FILE_RESTART0 = \
                '../examples/namelists/PROMICE.Baptiste.01/namelist.ebm.PROMICE_B.restart0'
            NAMELIST_FILE_CONTINUE = \
                '../examples/namelists/PROMICE.Baptiste.01/namelist.ebm.PROMICE_B.continue'
            # Name including path of the initial file
            INITIAL_FILE_CISSEMBEL = \
                '../data/PROMICE.Baptiste.01/PROMICE_Baptiste_initial.ATMOS_xxxx.GrIS.ISMIP6.nc'

        elif _setup == 'PROMICE_B_EraInterim2007_2017.ver02':
            # Atmospheric forcing file(s)
            #
            FORCING_FILE = '../data/PROMICE.Baptiste.02/Forcing.PROMICE_Baptiste.nc.ATMOS_2007_2017.GrIS.ISMIP6.nc'
            NAMELIST_FILE_RESTART0 = \
                '../examples/namelists/PROMICE.Baptiste.02/namelist.ebm.PROMICE_B.restart0'
            NAMELIST_FILE_CONTINUE = \
                '../examples/namelists/PROMICE.Baptiste.02/namelist.ebm.PROMICE_B.continue'
            # Name including path of the initial file
            INITIAL_FILE_CISSEMBEL = \
                '../data/PROMICE.Baptiste.02/PROMICE_Baptiste_initial.ATMOS_xxxx.GrIS.ISMIP6.nc'

        elif _setup == 'PROMICE_B_EraInterim2007_2017.ver02.8sites':
            # Atmospheric forcing file(s)
            #
            FORCING_FILE = '../data/PROMICE.Baptiste.02.8sites/Forcing.PROMICE_Baptiste.nc.ATMOS_2007_2017.GrIS.ISMIP6.nc'
            NAMELIST_FILE_RESTART0 = \
                '../examples/namelists/PROMICE.Baptiste.02.8sites/namelist.ebm.PROMICE_B.restart0'
            NAMELIST_FILE_CONTINUE = \
                '../examples/namelists/PROMICE.Baptiste.02.8sites/namelist.ebm.PROMICE_B.continue'
            # Name including path of the initial file
            INITIAL_FILE_CISSEMBEL = \
                '../data/PROMICE.Baptiste.02.8sites/PROMICE_Baptiste_initial.ATMOS_xxxx.GrIS.ISMIP6.nc'

        elif _setup == 'PROMICE_B_EraInterim2007_2017.ver02.7sites':
            # Atmospheric forcing file(s)
            #
            FORCING_FILE = '../data/PROMICE.Baptiste.02.7sites/Forcing.PROMICE_Baptiste.nc.ATMOS_2007_2017.GrIS.ISMIP6.nc'
            NAMELIST_FILE_RESTART0 = \
                '../examples/namelists/PROMICE.Baptiste.02.7sites/namelist.ebm.PROMICE_B.restart0'
            NAMELIST_FILE_CONTINUE = \
                '../examples/namelists/PROMICE.Baptiste.02.7sites/namelist.ebm.PROMICE_B.continue'
            # Name including path of the initial file
            INITIAL_FILE_CISSEMBEL = \
                '../data/PROMICE.Baptiste.02.7sites/PROMICE_Baptiste_initial.ATMOS_xxxx.GrIS.ISMIP6.nc'

    print('PYTHON-driver: Open forcing data file '+FORCING_FILE)
    forcing_fields = xr.open_dataset(FORCING_FILE) #, decode_times=True)

    # SHORTEN the record for testing
    #TODO:rm: forcing_fields = forcing_fields.isel(time=slice(0, 15))

    #
    # Time axis
    #
    print('PYTHON-driver:   Read time "'+vname_time+'"')
    _time_raw = forcing_fields.get(vname_time).data
    _time_array, _time_list = \
        compute_yyyymmdd_HHMMSS(forcing_fields.get(vname_time),
                                factor_time, offset_time)

    #
    # Surface elevation of atmospheric forcing fields
    #
    print('PYTHON-driver:   Read surface elevation "'+vname_zsurface+'"')
    _zsurface = numpy.array(forcing_fields.get(vname_zsurface).\
                            data.transpose(index4trans2D), dtype=numpy.double)\
        * factor_zsurface + offset_zsurface
    #
    # Near surface air temperature (e.g., 2m-air temperature)
    #
    print('PYTHON-driver:   Read air temperature "'+ \
          vname_air_temperature+'"')
    _air_temperature = numpy.array(forcing_fields.get(vname_air_temperature).\
                                   data.transpose(index4trans3D), dtype=numpy.double)\
        * factor_air_temperature + offset_air_temperature
    #
    # Shortwave radiation
    #
    print('PYTHON-driver:   Read shortwave radiation (downward, surface) "'+ \
          vname_surface_short_radiation_down+'"')
    _surface_short_radiation_down = \
        numpy.array(forcing_fields.get(vname_surface_short_radiation_down).\
                    data.transpose(index4trans3D), dtype=numpy.double)\
            * factor_surface_short_radiation_down + offset_surface_short_radiation_down
    #
    # Longwave radiation
    #
    print('PYTHON-driver:   Read longtwave radiation (downward, surface) "'+ \
          vname_surface_long_radiation_down+'"')
    _surface_long_radiation_down = \
        numpy.array(forcing_fields.get(vname_surface_long_radiation_down).\
                    data.transpose(index4trans3D), dtype=numpy.double)\
            * factor_surface_long_radiation_down + offset_surface_long_radiation_down
    #
    # Total precipitation = rainfall + snowfall + icefall (if applicable)
    #
    print('PYTHON-driver:   Read total precipitation "'+ \
          vname_total_precipitation+'"')
    _total_precipitation = \
        numpy.array(forcing_fields.get(vname_total_precipitation).\
                    data.transpose(index4trans3D), dtype=numpy.double)\
            * factor_total_precipitation + offset_total_precipitation
    #
    # # Near surface dew point temperature (e.g., 2m-dew point temperature)
    #
    print('PYTHON-driver:   Read dew point temperature "'+ \
          vname_dew_temperature+'"')
    _dew_temperature = \
        numpy.array(forcing_fields.get(vname_dew_temperature).\
                    data.transpose(index4trans3D), dtype=numpy.double)\
            * factor_dew_temperature + offset_dew_temperature
    #
    # Atmospheric surface pressure (NOT sea-level pressure)
    #
    print('PYTHON-driver:   Read surface air pressure "'+ \
          vname_atmos_surface_pressure+'"')
    _atmos_surface_pressure = \
        numpy.array(forcing_fields.get(vname_atmos_surface_pressure).\
                    data.transpose(index4trans3D), dtype=numpy.double)\
            * factor_atmos_surface_pressure + offset_atmos_surface_pressure
    #
    # Absolute wind speed (commonly at 10m height)
    #
    print('PYTHON-driver:   Read wind velocity "'+vname_wind_speed+'"')
    _wind_speed = \
        numpy.array(forcing_fields.get(vname_wind_speed).\
                    data.transpose(index4trans3D), dtype=numpy.double)\
            * factor_wind_speed + offset_wind_speed
    #
    # Total cloud fraction (values between 0 and 1)
    #
    print('PYTHON-driver:   Read cloud fraction "'+vname_cloudfraction+'"')
    _cloudfraction = \
        numpy.array(forcing_fields.get(vname_cloudfraction).\
                    data.transpose(index4trans3D), dtype=numpy.double)\
            * factor_cloudfraction + offset_cloudfraction
    #
    # Mask of surface / ice conditions (see mod_param.F08)
    #
    # Dummy value: everywhere one (1) = glacier point
    if _dummy_mask:
        _mask = numpy.ones_like(_air_temperature, dtype=int)
    else:
        vname_mask = 'mask'
        print('PYTHON-driver:   Read mask "'+vname_mask+'"')
        _mask = numpy.array(forcing_fields.get(vname_mask).\
                            data.transpose(index4trans3D), dtype=numpy.int)
            #* FACTOR_MASK + OFFSET_MASK

    return _time_raw, _time_array, _time_list, _zsurface, _air_temperature, \
        _surface_short_radiation_down, _surface_long_radiation_down, \
        _total_precipitation, _dew_temperature, _atmos_surface_pressure, \
        _wind_speed, _cloudfraction, _mask


def compute_yyyymmdd_HHMMSS(_time_array_in,
                            _factor_time=1.0, _offset_time=0.0):
    '''
    Determines from a given time axis/array the intermediate time axis/array
    and the time-list, for separate entries for the date/time elements: year,
    month, day, hour, minute, and second.

    Parameters
    ----------
    _time_array_in : numpy.ndarray
        Initial time array.
    _factor_time : float, optional
        The factor in the equation: time=time*factor+offset. The default is 1.0.
    _offset_time : float, optional
        The offset in the equation: time=time*factor+offset. The default is 0.0.

    Returns
    -------
    _time_array_out : numpy.ndarray
        date/time array.
    _time_list : numpy.ndarray
        list of time information for separate fields for year, month, day,
        hours, minute (, and seconds).

    '''
    years = _time_array_in.dt.year
    months = _time_array_in.dt.month
    days = _time_array_in.dt.day
    hours = _time_array_in.dt.hour
    minutes= _time_array_in.dt.minute
    #seconds = _time_array_in.dt.second

    # Time array passed to CISSEMBEL (Fortran code)
    _time_array_out = numpy.array(_time_array_in, dtype=numpy.double) \
        *_factor_time+_offset_time

    #
    # Time axis with five entries: Year, month, day, hour, minute
    #
    _time_list = numpy.zeros((5, len(_time_array_out)), dtype=int)
    _time_list[0, :] = years
    _time_list[1, :] = months
    _time_list[2, :] = days
    _time_list[3, :] = hours
    _time_list[4, :] = minutes
    #_time_list[5, :] = seconds

    return _time_array_out, _time_list



def compute_abs_timeaxis(_time_array_in, _time_forcing, _flag_abs_time=False,
                         _setup='unknown', _namelist='namelist.ebm'):
    '''
    Recompute the time array relative to a given reference time.

    Parameters
    ----------
    _time_array_in : numpy.ndarray
        initial time array.
    _time_forcing : numpy.ndarray
        DESCRIPTION.
    _flag_abs_time : bool, optional
        Shall we compute a absolute time axis. The default is False.
    _setup : string, optional
        Setup tag used to determine special cases. The default is 'unknown'.
    _namelist : string, optional
        Full path of the namelist file containing the reference data information.
        The default is 'namelist.ebm'.

    Returns
    -------
    _time_array_out : numpy.ndarray
        date/time array.

    '''

    _time_array_out = _time_array_in

    if _flag_abs_time:
        print('PYTHON-driver: Absolute time axis construction')
        print('PYTHON-driver: Open namelist file '+_namelist)
        with open(_namelist) as nml_file:
            try:
                nml = f90nml.read(nml_file)
                tunits = nml['write2files']['units_taxis']
                reference_year = int(tunits.split()[2].split('-')[0])
                reference_month = int(tunits.split()[2].split('-')[1])
                reference_day = int(tunits.split()[2].split('-')[2])
                reference_hour = int(tunits.split()[3].split(':')[0])
                reference_minute = int(tunits.split()[3].split(':')[1])
                reference_second = int(tunits.split()[3].split(':')[2])
                message_string = 'reference [namelist] = '
            except NameError:
                if 'PROMICE_B_EraInterim2007_2017' in _setup:
                    reference_year = 2007
                    reference_month = 9
                    reference_day = 23
                    reference_hour = 16
                    reference_minute = 0
                    reference_second = 0
                    message_string = 'reference [FALLBACK, hardcoded PROMICE_B] = '
                else:
                    reference_year = 1990
                    reference_month = 1
                    reference_day = 1
                    reference_hour = 0
                    reference_minute = 0
                    reference_second = 0
                    message_string = 'reference [FALLBACK, hardcoded] = '

        reference_datetime_str = \
            '{:04d}-'.format(reference_year)+ \
            '{:02d}-'.format(reference_month)+ \
            '{:02d}T'.format(reference_day)+ \
            '{:02d}:'.format(reference_hour)+ \
            '{:02d}:'.format(reference_minute)+ \
            '{:02d}'.format(reference_second)

        print('PYTHON-driver: Build absolute time axis, '+
              message_string+reference_datetime_str)

        #
        # Very first time
        #
        _time_forcing0 = _time_forcing.min()

        # taxis_day = numpy.zeros(_time_forcing.shape)   # Day-axis
        # taxis_hour = numpy.zeros(_time_forcing.shape)  # Hour-axis
        taxis_sec = numpy.zeros(_time_forcing.shape)    # Second-axis Default
        for ic, tt in enumerate(_time_forcing):
            if ic != 0:
                # taxis_day[ic] = numpy.timedelta64(_time_forcing[ic]-_time_forcing[ic-1], 'D')
                # taxis_hour[ic] = numpy.timedelta64(_time_forcing[ic]-_time_forcing[ic-1], 'h')
                taxis_sec[ic] = numpy.timedelta64(_time_forcing[ic]-_time_forcing[ic-1], 's')

        #
        # Offset in seconds between reference year and first time stamp in
        # forcing data record
        #
        reference_datetime = numpy.datetime64(reference_datetime_str)

        # days_year_ref = float(numpy.timedelta64(_time_forcing0-reference_datetime)/
        #                       numpy.timedelta64(1, 'D'))
        # taxis2ref_days = taxis_day.cumsum()+days_year_ref
        # hours_year_ref = float(numpy.timedelta64(_time_forcing0-reference_datetime)/
        #                        numpy.timedelta64(1, 'h'))
        # taxis2ref_hours = taxis_hour.cumsum()+hours_year_ref
        seconds_year_ref = float(numpy.timedelta64(_time_forcing0-reference_datetime)/
                                 numpy.timedelta64(1, 's'))
        taxis2ref_seconds = taxis_sec.cumsum()+seconds_year_ref

        #
        # Replace the _time_array_out with the newly computed time array in
        # seconds relative to the given reference_year. You may provide
        # the related offset via the variable `constant_time_offset`.
        #
        constant_time_offset = 0

        # _time_array_out = taxis2ref_days+constant_time_offset   # Days axis
        # _time_array_out = taxis2ref_hours+constant_time_offset  # Hours axis
        _time_array_out = taxis2ref_seconds+constant_time_offset # Second axis (default)

    return _time_array_out


def check_against_initial_file(flag_inspect_initial, fname_initial,
                               ilen_forcing, jlen_forcing):
    '''
    If requested (flag_inspect_initial=True), check whether the field's size in
    the forcing file(s) and the initial data file is identical. If they are not
    identical set the return flag to False.

    Parameters
    ----------
    flag_inspect_initial : bool
        Shall we check if the field sizes of forcing and initial data is identical?
    fname_initial : str
        full file name of the initial file.
    ilen_forcing : int
        Number of grid box in i-/x-direction.
    jlen_forcing : int
        Number of grid box in j-/y-direction.

    Returns
    -------
    _flag_skip : bool
        Are the fields identical. If not check, we report True.

    '''
    _flag_skip = False
    if flag_inspect_initial:
        print('PYTHON-driver: Open initial data file '+fname_initial)
        initial_fields = xr.open_dataset(fname_initial,
                                         decode_times=False, decode_cf=False,
                                         drop_variables=('station_id',
                                                         'station_mask'))

        reference_initial_field = \
            numpy.array(initial_fields.get(VNAME_2D_INITIAL_FIELD).data.transpose(index4trans2D),
                        dtype=numpy.double)
        ilen_initial = reference_initial_field.shape[0]
        jlen_initial = reference_initial_field.shape[1]

        if ilen_forcing == ilen_initial:
            print('PYTHON-driver:    OK: Length of 1. horizontal dimension '+
                  str(ilen_forcing))
        else:
            print('PYTHON-driver: ERROR: Length of 1. horizontal dimension differ:'+
                  ' Forcing field '+str(ilen_forcing)+
                  ' != '+str(ilen_initial)+' initial')
            _flag_skip = True

        if jlen_forcing == jlen_initial:
            print('PYTHON-driver:    OK: Length of 2. horizontal dimension '+
                  str(jlen_forcing))
        else:
            print('PYTHON-driver: ERROR: Length of 2. horizontal dimension differ:'+
                  ' Forcing field '+str(jlen_forcing)+
                  ' != '+str(jlen_initial)+' initial')
            _flag_skip = True

    return _flag_skip

# ---------------------------------------------------------------------------
#
# Main
#

if __name__ == '__main__':
    PYTHONPATH_TO_cbind_modEBMworld = '../test' # The path to the location of 'cbind_modEBMworld.py'
    import sys
    sys.path.append(PYTHONPATH_TO_cbind_modEBMworld)

    #
    # Required python packages
    #
    import numpy
    import xarray as xr
    import os

    #
    # Load the C-bindings to access the CISSEMBEL's Fortran code.
    # To create the related shared object file (so-file) go into the source
    # directory and call for serial code
    #   make CISSEMBEL4py
    # and for OpenMP parallel code
    #   make OPENMP_PARALLEL=on CISSEMBEL4py
    #
    # Please note: You may need to adjust the FORT variable in the `makefile`.
    #
    import cbind_modEBMworld as EBMworld

    from importlib import reload  # Needed to reload the C-bindings of
                                  # CISSEMBEL if you want to perform several
                                  # calls of `EBMworld` in a row.

    #
    # If requested, read the absolute time axis information from a namelist
    # file
    #
    try:
        # Installation:
        # > conda install -c conda-forge f90nml
        # > pip install f90nml
        import f90nml
    except ImportError:
        pass

    # ---------------------------------------------------------------------
    #
    # Settings
    #
    definition_settings()

    #
    # The `setup_tag` selects below predefined setups. Please select
    #
    # setup_tag = 'GrIS_EraInterim_100km'
    # setup_tag = 'PROMICE_EraInterim'
    # setup_tag = 'PROMICE_EraInterim2014'
    # setup_tag = 'PROMICE_B_EraInterim2007_2017'
    # setup_tag = 'PROMICE_B_EraInterim2007_2017.ver02'
    # setup_tag = 'PROMICE_B_EraInterim2007_2017.ver02.8sites'
    # setup_tag = 'PROMICE_B_EraInterim2007_2017.ver02.7sites'

    setup_tag = 'PROMICE_B_EraInterim2007_2017.ver02.7sites'
    #setup_tag = 'PURE_promice_B_EraInterim2007_2017.ver02.7sites'

    #
    # Report some settings
    #
    print('PYTHON-driver:')
    print('PYTHON-driver: Configuration:   setup_tag = '+setup_tag)
    print('PYTHON-driver: LIBRARY_NAME = '+LIBRARY_NAME)
    print('PYTHON-driver: NAMELIST_FILE_CISSEMBEL = '+NAMELIST_FILE_CISSEMBEL)
    print('PYTHON-driver: USE_ABSOLUTE_TIME_AXIS: '+str(USE_ABSOLUTE_TIME_AXIS))
    print('PYTHON-driver: USE_DUMMY_MASK        : '+str(USE_DUMMY_MASK))
    print('PYTHON-driver: CHECK_FIELD_SHAPES_WITH_INITIAL_FILE: '+str(CHECK_FIELD_SHAPES_WITH_INITIAL_FILE))
    print('PYTHON-driver:')

    #
    # New index order of 2-dimensional (x- and y-axis, static in time) and
    # 3-dimensional fields (temporal evolving fields with an x- and y-axis)
    # are defined by the following two lists (see command below):
    #
    global index4trans2D, index4trans3D
    index4trans2D = [1, 0]
    index4trans3D = [2, 1, 0]

    #
    # Reading the forcing data fields and the initial file
    #
    # Since the data layout in NetCDF files, C-/Python-code differs from
    # Fortran (see command above), we have to perform in most cases here
    # 1) Transpose/swap/permute the axis order to support Fortran ordering
    # 2) dtype=numpy.double to ensure the correct number precision supporting
    #    the following code (otherwise "strange" values are obtained). If you
    #    prefer another precision (e.g., float32), change the Python wrapper
    #    in cbind_modEBMworld.py (CISSEMBEL) and the numerical precision of
    #    c-type numbers of the subroutine cwrapper_CISSEMBEL_standard in
    #    modEBMworld.F08.
    #
    # WARNING A wrong order of the date files dimensions/axes, and here in
    #    particular the time axis, can cause utterly wrong results. In case
    #    you recognize unexpected results, please check the axis order. The
    #    size ofeach axis is report, please check these values.
    #
    #  NOTE The order axis order in NetCDF files, which follow the
    #    CF-convention, is typically: time, horizontal-dimension-2,
    #    horizontal-dimension-1, depth-dimension (see example below). In
    #    Fortran the order of these dimensions is transposed/permuted/different.
    #    Therefore CISSEMBEL, which is a Fortran code, the expected dimension
    #    order is horizontal-dimension-1, horizontal-dimension-2,
    #    depth-dimension, ..., time. Therfore, you may have to TRANSPOSE
    #    the axis order, if you use Python code to drive CISSEMBEL.
    #    Please see Python code examples.
    #
    # netcdf AirTemperatureForcing {
    # dimensions:
    # 	time = UNLIMITED ; // (1460 currently)
    # 	x1 = 16 ;
    # 	y1 = 29 ;
    # variables:
    # 	double time(time) ;
    # 		time:standard_name = "time" ;
    # 		time:units = "hours since 1979-01-01 03:00:00" ;
    # 		time:calendar = "proleptic_gregorian" ;
    # 		time:axis = "T" ;
    # 	float x1(x1) ;
    # 		x1:standard_name = "projection_x_coordinate" ;
    # 		x1:long_name = "Cartesian x-coordinate" ;
    # 		x1:units = "meters" ;
    # 		x1:axis = "X" ;
    # 	float y1(y1) ;
    # 		y1:standard_name = "projection_y_coordinate" ;
    # 		y1:long_name = "Cartesian y-coordinate" ;
    # 		y1:units = "meters" ;
    # 		y1:axis = "Y" ;
    # 	float temp2(time, y1, x1) ;
    # 		temp2:units = "K" ;
    #       temp2:long_name = "2m-air temperature" ;
    # 		temp2:code = 167 ;
    # 		temp2:table = 128 ;
    # 		temp2:coordinates = "lat lon" ;
    # 	float lon(y1, x1) ;
    # 		lon:standard_name = "longitude" ;
    # 		lon:long_name = "Longitude" ;
    # 		lon:units = "degreeE" ;
    # 		lon:_CoordinateAxisType = "Lon" ;
    # 	float lat(y1, x1) ;
    # 		lat:standard_name = "latitude" ;
    # 		lat:long_name = "Latitude" ;
    # 		lat:units = "degreeN" ;
    # 		lat:_CoordinateAxisType = "Lat" ;
    #

    # ---------------------------------------------------------------------
    #
    # Open files
    #
    print('PYTHON-driver: Open forcing data file')
    time_raw, \
        time_array, \
        time_list, \
        zsurface, \
        air_temperature, \
        surface_short_radiation_down, \
        surface_long_radiation_down, \
        total_precipitation, \
        dew_temperature, \
        atmos_surface_pressure, \
        wind_speed, \
        cloudfraction, \
        mask = read_forcing_data(setup_tag, USE_DUMMY_MASK)

    #
    # Special tasks for certain configurations
    #
    if 'PROMICE_B_EraInterim2007_2017' in setup_tag:
        print('PYTHON-driver: Copy '+NAMELIST_FILE_RESTART0+' '+NAMELIST_FILE_CISSEMBEL)
        os.system('cp -f '+NAMELIST_FILE_RESTART0+' '+NAMELIST_FILE_CISSEMBEL)
    else:
        print('PYTHON-driver: Keep '+NAMELIST_FILE_CISSEMBEL)


    #
    # We may construct an absolute time array from scratch
    #
    time_array = compute_abs_timeaxis(time_array,
                                      time_raw,
                                      USE_ABSOLUTE_TIME_AXIS,
                                      setup_tag,
                                      NAMELIST_FILE_CISSEMBEL)

    #
    # Determine the size of the fields. These are needed by the Fortran code
    # via the c-binding interface because fields are conveyed as pointers (see
    # function CISSEMBEL in cbind_modEBMworld.py).
    #
    ilen = air_temperature.shape[0]
    jlen = air_temperature.shape[1]
    tlen = air_temperature.shape[2]

    #
    # Initial field for comparing the field shape
    #
    FLAG_SKIP = check_against_initial_file(CHECK_FIELD_SHAPES_WITH_INITIAL_FILE,
                                           INITIAL_FILE_CISSEMBEL, ilen, jlen)

    #
    # Calling CISSEMBEL via the Python-wrapper "CISSEMBEL" calling the
    # Fortran-wrapper "cwrapper_CISSEMBEL_standard in modEBMworld.F08.
    # The related symbol is named "c_CISSEMBEL".
    #
    if FLAG_SKIP:
        print('PYTHON-driver: ++++++++++')
        print('PYTHON-driver: +  SKIP  +')
        print('PYTHON-driver: ++++++++++')
    else:
        if 'PROMICE_B_EraInterim2007_2017' in setup_tag:
            namelist_filename_src = NAMELIST_FILE_RESTART0
            files2delete = ['restart_out',
                            'restart_in',
                            'restart_output_final.nc',
                            'restart_output_spinup.nc' ]
            # files2delete = ['restart_out', 'restart_in']

            print('PYTHON-driver: Remove old files')
            for file in files2delete:
                if os.path.isfile(file):
                    print('PYTHON-driver:   rm '+file)
                    os.remove(file)

            print('PYTHON-driver: Copy '+namelist_filename_src+
                  ' '+NAMELIST_FILE_CISSEMBEL)
            os.system('cp -f '+namelist_filename_src+' '+NAMELIST_FILE_CISSEMBEL)
            print('PYTHON-driver: ---- SPIN UP -----')

        # print('PYTHON-driver: Reload/Reimport EBMworld')
        # reload(EBMworld)
        EBMworld.CISSEMBEL(ilen,
                           jlen,
                           tlen,
                           time_array,
                           TIME_STEP_SECONDS,
                           time_list,
                           mask,
                           zsurface,
                           air_temperature,
                           surface_short_radiation_down,
                           surface_long_radiation_down,
                           total_precipitation,
                           dew_temperature,
                           atmos_surface_pressure,
                           wind_speed,
                           cloudfraction,
                           LIBRARY_NAME)

        if 'PROMICE_B_EraInterim2007_2017' in setup_tag:
            namelist_filename_src = NAMELIST_FILE_CONTINUE

            print('PYTHON-driver: Remove old files')
            files2delete = ['coupling4ism.nc',
                            'diagnost.nc',
                            'forcing_out.nc',
                            'output.nc']
            for file in files2delete:
                if os.path.isfile(file):
                    print('PYTHON-driver:   rm '+file)
                    os.remove(file)

            print('PYTHON-driver: Copy '+namelist_filename_src+
                  ' '+NAMELIST_FILE_CISSEMBEL)
            os.system('cp -f '+namelist_filename_src+' '+NAMELIST_FILE_CISSEMBEL)
            print('PYTHON-driver: ---- FINAL SIMULATION -----')

            print('PYTHON-driver: ****************************************')
            print('PYTHON-driver: ****************************************')
            print('PYTHON-driver: ***                                  ***')
            print('PYTHON-driver: ***     Reload/Reimport EBMworld     ***')
            print('PYTHON-driver: ***                                  ***')
            print('PYTHON-driver: ****************************************')
            print('PYTHON-driver: ****************************************')
            reload(EBMworld)

            EBMworld.CISSEMBEL(ilen,
                               jlen,
                               tlen,
                               time_array,
                               TIME_STEP_SECONDS,
                               time_list,
                               mask,
                               zsurface,
                               air_temperature,
                               surface_short_radiation_down,
                               surface_long_radiation_down,
                               total_precipitation,
                               dew_temperature,
                               atmos_surface_pressure,
                               wind_speed,
                               cloudfraction,
                               LIBRARY_NAME)


    print('PYTHON-driver: ')
    print('PYTHON-driver: Complete simulation     setup_tag = '+setup_tag)
    print('PYTHON-driver: Forcing              FORCING_FILE = '+FORCING_FILE)
    if CHECK_FIELD_SHAPES_WITH_INITIAL_FILE:
        print('PYTHON-driver: Initial    INITIAL_FILE_CISSEMBEL = '+
              INITIAL_FILE_CISSEMBEL)
    print('PYTHON-driver:  Bye bye ....')

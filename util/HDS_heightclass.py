#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Git version control id: $Id$

Downscales by height classes computed CISSEMBEL results to a heighter horizontal resolution
HeightClass_class
Created on Mon Jul 31 14:21:32 2023

@author: Uffe Bundesen, DMI Copenhagen, Denmark

@copyright: Copyright (C) 2022-2025, Christian Rodehacke
"""
# -------------------------------------------------------------------
# This file is part of CISSEMBEL, which is the
# == Copenhagen Ice-Snow Surface Energy and Mass Balance modEL ==
#
# CISSEMBEL is free software; you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE  v.1.2
#
# Information about the EUROPEAN UNION PUBLIC LICENCE (*EUPL*) and
# translations into other European languages are available at
# https://eupl.eu (last access: 2021-10-01)](https://eupl.eu/)
#
# We hope that  this  distributed  CISSEMBEL  code, its  tools and
# documentations are usefull. Any improvements of the code, tools,
# and documentations tools are  very welcome.  Please, consider to
# share your thoughts and improvements with the community.
#
# The documentation,  tools,  and the code are provided as it are.
# NO WARRANTY  ABOUT FITNESS  AND USABILITY OF THE CODE, TOOLS, OR
# DOCUMENTATION  ARE  GIVEN.   NO  WARRANTY  IS  GIVEN  ABOUT  THE
# CORRECTNESS  OF COMPUTED RESULTS WITH THE  PROVIDED CODE, TOOLS,
# AND DOCUMENTATION. YOU USE THE CODE, DOCUMENTATION, AND TOOLS AT
# YOUR OWN RISK.           Removing  of  any  copyright  statement
# or  this  disclaimer  is considered  as a  attempt  to defraud.
#
# -------------------------------------------------------------------

import numpy as np
import xarray as xr
import netCDF4 as nc
from cdo import Cdo
import os
# import plotting_for_cissembel as pcis
cdo=Cdo()



class HeightClass_class:
    """
    This class takes as input an initial file, and define different datasets
    which can be used for the creating an output from a finer grid, using height
    classes.
    """

    def __init__(self, initial_file = 'Initial_file.nc'):
        self.initial_file = initial_file
        self.data = nc.Dataset(initial_file, mode='r')
        self.ds = xr.open_dataset(initial_file, decode_times=False, engine = 'netcdf4').astype('float64')
        self.height = self.ds.variables['height']
        self.orog = self.ds.variables['orog']

        # We define two arrays, containing the heights above and below the
        # reference heights respectively.
        self.height_below_alldim = self.height.where(self.height <= self.orog, other=np.nan)
        self.height_above_alldim = self.height.where(self.height > self.orog, other=np.nan)

        # We define the minimum of the heights above reference, and the maximum
        # of the heights below, ie. we find the heights closes to the reference height.
        self.height_below = self.height_below_alldim.max(dim= 'haxis')
        self.height_above = self.height_above_alldim.min(dim= 'haxis')

        # And similar for the arrays, with the height dimension
        self.height_below_alldim = self.height_below_alldim.where(self.height_below_alldim == self.height_below, other=np.nan)
        self.height_above_alldim = self.height_above_alldim.where(self.height_above_alldim == self.height_above, other=np.nan)

        # And here we define the factor-array. Called 'g' in the CISSEMBEL manual
        self.G = -(self.height_below - self.orog)/(self.height_above - self.height_below)


    def file_from_hcc(self, initial_fine_grid, coarse_grid_output,
     fine_grid_output = 'fine_grid_output.nc', interpolation = 'Bilinear'):
        """
        This function creates the netcdf file from a cissembel output with
        HeightClasses turned on. The created file contains the same variables
        but interpolated from the heightclass output, using the heights given in
        the higher resolution file fine_grid_output, and thus the 'height'
        dimension collapsed.


        Parameters
        ----------
        initial_fine_grid : NC-file
            This refers to the initial that we want to interpolate onto.
        coarse_grid_output : NC-file
            Output file, containing the variables with the height-dimension, ie.
            before we have done any interpolation.
        fine_grid_output : NC-file, optional
            DESCRIPTION. The default is 'fine_grid_output.nc'.
            This is the name of the output file containing the interpolated data.
        interpolation : TYPE, optional
            DESCRIPTION. The default is 'Bilinear'.

        Returns
        -------
        None.

        """
        # We create the downscaled NC-file
        if interpolation == 'Bilinear':
            cdo.remapbil(initial_fine_grid, input= coarse_grid_output, output= fine_grid_output)

        fine_output_data = nc.Dataset(fine_grid_output, mode='r')
        fine_output_ds = xr.open_dataset(fine_grid_output, decode_times=False).astype('float64')

        #
        fine_output_smb = fine_output_ds.variables['smb']
        fine_output_snowf = fine_output_ds.variables['snowf']
        fine_output_runoff = fine_output_ds.variables['runoff']
        fine_output_acc = fine_output_ds.variables['acc']
        fine_output_evap = fine_output_ds.variables['evap']
        fine_output_abl = fine_output_ds.variables['abl']
        # print(np.max(fine_output_smb))

        # We create an instance of the HeightClass_class for the downscaled grid
        fine_grid_hcc = HeightClass_class(initial_fine_grid)


        # Define arrays for placeholders for the variables above and below then
        # the fine grid heights.
        fine_output_ds['smb_above'] = fine_output_smb * np.nan
        fine_output_ds['smb_below'] = fine_output_smb * np.nan
        fine_output_ds.smb_above.attrs['units'] = 'kg s-1 m-2'
        fine_output_ds.smb_below.attrs['units'] = 'kg s-1 m-2'

        fine_output_ds['snowf_above'] = fine_output_snowf * np.nan
        fine_output_ds['snowf_below'] = fine_output_snowf * np.nan
        fine_output_ds.snowf_above.attrs['units'] = 'kg s-1 m-2'
        fine_output_ds.snowf_below.attrs['units'] = 'kg s-1 m-2'

        fine_output_ds['runoff_above'] = fine_output_runoff * np.nan
        fine_output_ds['runoff_below'] = fine_output_runoff * np.nan
        fine_output_ds.runoff_above.attrs['units'] = 'kg s-1 m-2'
        fine_output_ds.runoff_below.attrs['units'] = 'kg s-1 m-2'

        fine_output_ds['acc_above'] = fine_output_acc * np.nan
        fine_output_ds['acc_below'] = fine_output_acc * np.nan
        fine_output_ds.acc_above.attrs['units'] = 'kg s-1 m-2'
        fine_output_ds.acc_below.attrs['units'] = 'kg s-1 m-2'

        fine_output_ds['evap_above'] = fine_output_evap * np.nan
        fine_output_ds['evap_below'] = fine_output_evap * np.nan
        fine_output_ds.evap_above.attrs['units'] = 'kg s-1 m-2'
        fine_output_ds.evap_below.attrs['units'] = 'kg s-1 m-2'

        fine_output_ds['abl_above'] = fine_output_abl * np.nan
        fine_output_ds['abl_below'] = fine_output_abl * np.nan
        fine_output_ds.abl_above.attrs['units'] = 'kg s-1 m-2'
        fine_output_ds.abl_below.attrs['units'] = 'kg s-1 m-2'

        # Since height_above_alldim contain Values above correspondingly and nans
        # for other values, by dividing it by itself and multiplying with fine_output_smb
        # we select smbs above the reference height. Likewise for values below.
        # We can use this selection for other variables we are interested in.
        # Note
        fine_output_ds['smb_above'][:, 1:, :, :] = (fine_grid_hcc.height_above_alldim / fine_grid_hcc.height_above_alldim) * fine_output_smb[:, 1:, :, :]
        fine_output_ds['smb_below'][:, 1:, :, :] = (fine_grid_hcc.height_below_alldim / fine_grid_hcc.height_below_alldim) * fine_output_smb[:, 1:, :, :]

        fine_output_ds['snowf_above'][:, 1:, :, :] = (fine_grid_hcc.height_above_alldim / fine_grid_hcc.height_above_alldim) * fine_output_snowf[:, 1:, :, :]
        fine_output_ds['snowf_below'][:, 1:, :, :] = (fine_grid_hcc.height_below_alldim / fine_grid_hcc.height_below_alldim) * fine_output_snowf[:, 1:, :, :]

        fine_output_ds['runoff_above'][:, 1:, :, :] = (fine_grid_hcc.height_above_alldim / fine_grid_hcc.height_above_alldim) * fine_output_runoff[:, 1:, :, :]
        fine_output_ds['runoff_below'][:, 1:, :, :] = (fine_grid_hcc.height_below_alldim / fine_grid_hcc.height_below_alldim) * fine_output_runoff[:, 1:, :, :]

        fine_output_ds['acc_above'][:, 1:, :, :] = (fine_grid_hcc.height_above_alldim / fine_grid_hcc.height_above_alldim) * fine_output_acc[:, 1:, :, :]
        fine_output_ds['acc_below'][:, 1:, :, :] = (fine_grid_hcc.height_below_alldim / fine_grid_hcc.height_below_alldim) * fine_output_acc[:, 1:, :, :]

        fine_output_ds['evap_above'][:, 1:, :, :] = (fine_grid_hcc.height_above_alldim / fine_grid_hcc.height_above_alldim) * fine_output_evap[:, 1:, :, :]
        fine_output_ds['evap_below'][:, 1:, :, :] = (fine_grid_hcc.height_below_alldim / fine_grid_hcc.height_below_alldim) * fine_output_evap[:, 1:, :, :]

        fine_output_ds['abl_above'][:, 1:, :, :] = (fine_grid_hcc.height_above_alldim / fine_grid_hcc.height_above_alldim) * fine_output_abl[:, 1:, :, :]
        fine_output_ds['abl_below'][:, 1:, :, :] = (fine_grid_hcc.height_below_alldim / fine_grid_hcc.height_below_alldim) * fine_output_abl[:, 1:, :, :]

        # Here we multiply by the factor G as decribed in the Cissembel manual
        # Collapsing the array in the 'height' dimension, describing the variable
        # as a linear combination of the values right above and below the given
        # grid point.
        fine_output_ds['smb_FromHeightClass'] = fine_grid_hcc.G * fine_output_ds.variables['smb_above'].min(dim = 'haxis') + (1 - fine_grid_hcc.G) * fine_output_ds.variables['smb_below'].max(dim = 'haxis')
        fine_output_ds.smb_FromHeightClass.attrs['units'] = 'kg s-1 m-2'

        fine_output_ds['snowf_FromHeightClass'] = fine_grid_hcc.G * fine_output_ds.variables['snowf_above'].min(dim = 'haxis') + (1 - fine_grid_hcc.G) * fine_output_ds.variables['snowf_below'].max(dim = 'haxis')
        fine_output_ds.smb_FromHeightClass.attrs['units'] = 'kg s-1 m-2'

        fine_output_ds['runoff_FromHeightClass'] = fine_grid_hcc.G * fine_output_ds.variables['runoff_above'].min(dim = 'haxis') + (1 - fine_grid_hcc.G) * fine_output_ds.variables['runoff_below'].max(dim = 'haxis')
        fine_output_ds.smb_FromHeightClass.attrs['units'] = 'kg s-1 m-2'

        fine_output_ds['acc_FromHeightClass'] = fine_grid_hcc.G * fine_output_ds.variables['acc_above'].min(dim = 'haxis') + (1 - fine_grid_hcc.G) * fine_output_ds.variables['acc_below'].max(dim = 'haxis')
        fine_output_ds.smb_FromHeightClass.attrs['units'] = 'kg s-1 m-2'

        fine_output_ds['sub_FromHeightClass'] = fine_grid_hcc.G * fine_output_ds.variables['evap_above'].min(dim = 'haxis') + (1 - fine_grid_hcc.G) * fine_output_ds.variables['evap_below'].max(dim = 'haxis')
        fine_output_ds.smb_FromHeightClass.attrs['units'] = 'kg s-1 m-2'

        fine_output_ds['abl_FromHeightClass'] = fine_grid_hcc.G * fine_output_ds.variables['abl_above'].min(dim = 'haxis') + (1 - fine_grid_hcc.G) * fine_output_ds.variables['abl_below'].max(dim = 'haxis')
        fine_output_ds.smb_FromHeightClass.attrs['units'] = 'kg s-1 m-2'

        fine_output_ds['smb_FromHeightClass'] = fine_output_ds['smb_FromHeightClass'].transpose('time', 'y', 'x')
        fine_output_ds['snowf_FromHeightClass'] = fine_output_ds['snowf_FromHeightClass'].transpose('time', 'y', 'x')
        fine_output_ds['runoff_FromHeightClass'] = fine_output_ds['runoff_FromHeightClass'].transpose('time', 'y', 'x')
        fine_output_ds['acc_FromHeightClass'] = fine_output_ds['acc_FromHeightClass'].transpose('time', 'y', 'x')
        fine_output_ds['sub_FromHeightClass'] = fine_output_ds['sub_FromHeightClass'].transpose('time', 'y', 'x')
        fine_output_ds['abl_FromHeightClass'] = fine_output_ds['abl_FromHeightClass'].transpose('time', 'y', 'x')

        fine_output_ds.to_netcdf(fine_grid_output, mode='a')


# J = HeightClass_class('Initial_file_RoughGrid')
# J.file_from_hcc('Initial_file_FineGrid', 'output.nc', 'FineGrid_output.nc')


# save_folder = "path/to/save_directory"

# starting_year = 1980
# ending_year = 1991

# for folder in sorted(os.listdir(save_folder))[(ending_year - starting_year):]:
#     print(folder)
#     print(str(int(folder)%(ending_year - starting_year)+ starting_year))
#     J.file_from_hcc('fine_grid_initial_file', save_folder + folder + '/' + str(int(folder)%(ending_year - starting_year)+ starting_year)+ '/output.nc', save_folder + folder + '/' + str(int(folder)%(ending_year - starting_year)+ starting_year) + '/HDS_output.nc', 'Bilinear')

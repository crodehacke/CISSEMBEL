#! Git version control id: $Id$

#
# -------------------------------------------------------------------
#
# Copyright (C) 2016-2025 Christian Rodehacke
#
# This file is part of CISSEMBEL, which is the
# == Copenhagen Ice-Snow Surface Energy and Mass Balance modEL ==
#
# CISSEMBEL is free software; you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE  v.1.2
#
# Information about the EUROPEAN UNION PUBLIC LICENCE (*EUPL*) and
# translations into other European languages are available at
# https://eupl.eu (last access: 2023-09-18)](https://eupl.eu/)
#
# We hope that  this  distributed  CISSEMBEL  code, its  tools and
# documentations are usefull. Any improvements of the code, tools,
# and documentations tools are  very welcome.  Please, consider to
# share your thoughts and improvements with the community.
#
# The documentation,  tools,  and the code are provided as it are.
# NO WARRANTY  ABOUT FITNESS  AND USABILITY OF THE CODE, TOOLS, OR
# DOCUMENTATION  ARE  GIVEN.   NO  WARRANTY  IS  GIVEN  ABOUT  THE
# CORRECTNESS  OF COMPUTED RESULTS WITH THE  PROVIDED CODE, TOOLS,
# AND DOCUMENTATION. YOU USE THE CODE, DOCUMENTATION, AND TOOLS AT
# YOUR OWN RISK.           Removing  of  any  copyright  statement
# or  this  disclaimer  is considered  as a  attempt  to defraud.
#
# -------------------------------------------------------------------

## @file OS-FORT-template.mk
#  @brief Include makefile that contain specific information for the
#    environment following the filename scheme: $(OS)-$(FORT).mk ,
#    where the compiler environment is storage in $(COMPILER_ENV). To
#    check its value on different computers run the following command:
#    make print-COMPILER_ENV
#    All different compiler specific files are located in the
#    sub-directory $(Compilers), so to ask to the final filename check
#    make print-COMPILER_INCLUDE_FILE
#
#    We set in this file
#    - Program locations for various Unix programs
#    - Path of required libraries
#    - Compiler specific flags
##

#---------------------------------------
#
# Unix programs (compiler or machine specific commands)
#
# XXX   :=

#---------------------------------------
#
# Flags
#
# flag for free format of long lines: LINE_LENGTH
LINE_LENGTH :=
FFLAGS      := -cpp
ifdef DEBUG_FLAGS
    FFLAGS += -g -O0
else
  FFLAGS += -O
  ifdef PARALLEL_AUTO
    FFLAGS +=
    $(warning Turn on autoparallelization : set OMP_NUM_THREADS)
  endif
  FFLAGS += -DNDEBUG
endif

ifdef LARGE_MEM_FLAG
  FFLAGS +=
endif
ifdef GPROFILE_FLAG
  $(warning ***** Add profiling support -pg ************)
  FFLAGS += -pg
endif

ifdef POS_INDEPEN_CODE
  $(warning "***** PIC: partition independent code ******")
  FFLAGS += -fPIC
endif

#
# Libaries
#
CPPFLAGS :=
ifdef NETCDF_SUPPORT
  # Automatic, it may fail
  CPPFLAGS += $(shell nc-config --cflags --libs)

  # Manual settings of the path
  NETCDF_DIR=/usr/share/netcdf
  CPPFLAGS += -I$(NETCDF_DIR)/include -L$(NETCDF_DIR)/lib -lnetcdff -lnetcdf

  $(warning You might have to set LD_LIBRARY_PATH $(NETCDF_DIR))
  #MESSAGE += "export/setenv LD_LIBRARY_PATH $(NETCDF_DIR)/lib"
endif
ifdef OPENMP_PARALLEL
  FFLAGS +=
endif
ifdef MPI_PARALLEL
  FFLAGS   += -DMPI_PARALLEL
  CPPFLAGS += -I/usr/lib
  FORT     := mpifort
  ifdef MPI_PARALLEL_OUT
    FFLAGS   += -DPARALLEL_IO
  endif
endif

#
# -- Last lines
#

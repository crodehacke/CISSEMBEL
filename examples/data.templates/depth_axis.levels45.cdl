// Git version control id: $Id$
/// @file
/// @brief CDL file containing an examplarily depth array along the kaxis
/// @detail The INItial file of the CISSEMBEL model needs for the
///   very first run information about the depth/layer axis `kaxis`
///   and initial depth values. This Common Date Language (CDL) file
///   contains this information, which can be transform via the
///   `ncgen` command into a netCDF file, for example:
///   ```
///       ncgen -o depth_axis.nc depth_axis.cdl
///   ```
///

netcdf depth_axis {
dimensions:
	kaxis = 45 ;
variables:
	int kaxis(kaxis) ;
		kaxis:standard_name = "model_level_number" ;
		kaxis:positive = "down" ;
		kaxis:long_name = "z-layer of mesh grid" ;
		kaxis:units = "1" ;
		kaxis:axis = "Z" ;
	double depth(kaxis) ;
		depth:standard_name = "depth" ;
		depth:long_name = "depth below surface" ;
		depth:units = "meter" ;
		depth:positive = "down" ;
		depth:note = "depth of the layer center (vertical mean)" ;
		depth:comment = "computed from 'layer_thickness': depth(1)=0.5*layer_thickness(1);for k=2,size(kaxis); depth(k)=depth(k-1)+0.5*(layer_thickness(k-1)+layer_thickness(k)); end" ;
	double layer_thickness(kaxis) ;
		layer_thickness:standard_name = "snow_layer_thickness" ;
		layer_thickness:long_name = "thickness of each snow layer" ;
		layer_thickness:units = "meter" ;


// global attributes:
		:Conventions = "CF-1.3" ;
		:history = "Example for a depth axis in CISSEMBEL" ;
		:Creator = "Christian Rodehacke - Danish Meteorological Institute, Copenhagen, Denmark" ;
		:Title = "Example Depth axis for CISSEMBEL" ;
data:

// 45 Levels, maximum depth: 27.7m
kaxis =  1,  2,  3,  4,  5,  6,  7,  8,  9, 10,
        11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
	21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
	31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
	41, 42, 43, 44, 45 ;

depth = 0.0325, 0.0975, 0.165, 0.235, 0.31, 0.4, 0.5, 0.6, 0.705,
    0.82, 0.94, 1.0625, 1.2, 1.35, 1.51, 1.695, 1.895, 2.12, 2.37,
    2.6225, 2.9, 3.2, 3.525, 3.9, 4.3, 4.725, 5.2, 5.7,
    6.225, 6.8, 7.4, 8.05, 8.75, 9.5, 10.4, 11.45, 12.6, 13.85, 15.2,
    16.65, 18.4, 20.45, 22.6, 24.95, 27.7 ;

layer_thickness = 0.065, 0.065, 0.07, 0.07, 0.08, 0.1, 0.1, 0.1, 0.11,
    0.12, 0.12, 0.125, 0.15, 0.15, 0.17, 0.2, 0.2, 0.25, 0.25, 0.255,
    0.3, 0.3, 0.35, 0.4, 0.4, 0.45, 0.5, 0.5, 0.55, 0.6, 0.6, 0.7,
    0.7, 0.8, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 2.0, 2.1, 2.2, 2.5, 3.0 ;

}

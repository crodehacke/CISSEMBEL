Deprecated Open tasks for the _CISSEMBEL_ code
==============================================

This document lists **deprecated** features, files and tools of
  CISSEMBEL. You may find related information in [TODO.md](TODO.md).

Details about the _Copenhagen Ice-Snow Surface Energy and Mass Balance modEL_
  (**CISSEMBEL**) are provided in the [README.md](README.md).


Technique
---------

* UNDEFine `ISO_FORTRAN_ENV` in [./src/cppdefs.h](./src/cppdefs.h) is a
  deprecated feature. Instead we consider that Fortran compilers
  support all used _Fortran 2008_ features [2025-01-31].

* Former Python code to test and develop the Fortran-C-Python binding
  chain. It is locate in [./test/deprecated](./test/deprecated) and
  comprises the following three files:
  - [./test/deprecated/example_cbind1.py](./test/deprecated/example_cbind1.py)
  - [./test/deprecated/example_cbind2.py](./test/deprecated/example_cbind2.py)
  - [./test/deprecated/example_cfunc.py](./test/deprecated/example_cfunc.py)
  [2023-09-18]
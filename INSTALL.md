﻿Copenhagen Ice-Snow Surface Energy and Mass Balance modEL
======================================================================

The **C**openhagen **I**ce-**S**now **S**urface **E**nergy and
 **M**ass **B**alance mod**EL** (**CISSEMBEL**) is a general purpose
 code to determine the Surface Mass and Energy Balance of snow and
 ice-covered regions.


Here, you find information on how to build binaries and
 libraries. CISSEMBEL is coupled and directly driven by an atmospheric
 model in the _online_ mode. In the _offline_ mode, CISSEMBEL reads
 atmospheric forcing files to perform simulations.

Depending on the settings influencing the building of libraries and
 binaries, the following configurations are available: serial, OpenMP
 parallel, MPI (Message Passing Interface) parallel, and hybrid MPI
 and OpenMP parallel libraries and binaries.

For those running CISSEMBEL under standard Linux systems, you may find
  information about installing required dependencies in the section
  _Installation of the required dependencies_.

Please note known issues compiled in
  [KNOWN_ISSUES.md](KNOWN_ISSUES.md).

The code is hosted at
  [gitlab.com/crodehacke/CISSEMBEL](https://gitlab.com/crodehacke/CISSEMBEL).
  You can obtain the code by running the following `git` command, for
  instance:

  ```
  git clone https://gitlab.com/crodehacke/CISSEMBEL.git
  ```

General commands
================
Before compiling the code to build binaries or libraries, please
 check the switches in [`./src/cppdefs.h`](./src/cppdefs.h). Usually,
 you do not need to change the standard settings if you start to
 explore CISSEMBEL for the first time.

Please select one of the most appropriate compilers at the beginning of
 the *makefile* [./src/makefile](./src/makefile) by uncommenting the
 most appropriate `FORT` switch. Please be aware that the NetCDF
 library is needed in all cases since input and output files are
 NetCDF files. If you ask for MPI support (see below), an appropriate
 library has to exist to compile and link the binary successfully. For
 parallel Input/Output (IO) the NetCDF library has to support
 NetCDF-4, which includes HDF5 support. Reading and writing of
 compressed files requires NetCDF-4 and z-lib support.

If one of the offered settings does not fit your computer environment
 or you want to change settings (e.g., adjusting LIBRARY paths) please
 inspect the sub-directory *Compilers*:
 [`./src/Compilers`](./src/Compilers). For an existing
 environment of the operation system and compiler, select the file
 that fits your environment (use the comment `make print-COMPILER_ENV`
 to find the detected environment or call `make
 print-COMPILER_INCLUDE_FILE` to get the name of the
 corresponding file for the selected compiler and detected operating
 system).

According to your needs, you can build the code with OpenMP support
 (set `OPENMP_PARALLEL ?= on` in the
 [`./src/makefile`](./src/makefile)) and Message Passing Interface
 (MPI) support (set `MPI_PARALLEL ?= on` in the
 [`./src/makefile`](./src/makefile)) including parallel writing under
 MPI. For huge simulations with many data points and/or depth layers,
 you may have to switch on massive file data size support
 (`LARGE_MEM_FLAG := on`).

At the beginning, we recommend OpenMP (Open Multi-Processing)
 parallelization; please see below the section
 _OpenMP Parallelization_ for further details.

The MPI use is not recommended for simulations on a single
 node/computer. Although, it is available for simulations across
 various compute nodes, its performance is not optimized. Furthermore,
 some features are unavailable in the MPI mode, such as the support
 for time series of defined basins.

During the compilation, the code version number is obtained via a
 `git` command call, this may lead to problems on computers where GIT
 is not installed. If you experience this problem, please change
 either the `GIT := git` in your environment and compiler-specific
 file in the sub-directory *Compilers* (see also above) or modify the
 *makefile*. For example, please place a new line with "`GIT :=`"
 directly in front of the `ifdef GIT`-statement . This command
 deactivates the call of GIT. Consequently, the GIT information in the
 model logging output and the various output files contain only dummy
 information and not the actual code version information.

In the following, we discuss the code compilation for three different
 kinds of configurations:

- Online coupled simulations, where CISSEMBEL is driven directly by an
  atmosphere model
- Offline simulations, where CISSEMBEL is driven by atmospheric
  forcing fields read by a program that is either the Fortran driver
  [`./src/EBMdriver.F08`](./src/EBMdriver.F08) or
- a Python program reading atmospheric forcing fields, e.g.,
  [`./src/cissembel.py`](./src/cissembel.py).

In the following, all `make` command are called in the directory
 [`./src`](./src). These create binaries, libraries, and any other
 produces. For instance, the make command `make documentation` creates
 detailed code documentation via _doxygen _, while `make tests` starts
 various tests that lead to figures being part of the
 documentation. These tests call Python code found in
 [`./test`](./test).

To test the various functions and subroutines which describe a
 variety of different physical processes, these functions and
 subroutines can be accessed via C-bindings from Python. This
 capability allows us to visualize the functional form of these
 functions and to driver CISSEMBEL by Python, C, or C++.

Build the libraries for coupling to an atmosphere model
=======================================================
The comment `make coupling` compiles the code and builds the libraries
 for the coupling between an atmosphere model and CISSEMBEL. The names
 of the build libraries are printed at the end of a successful
 compilation.

Build the binary for offline simulations by a Fortran driver
============================================================
Before compiling the code, activate one preprocessor switch in the
 Fortran code [`./src/EBMdriver.F08`](./src/EBMdriver.F08) that
 controls which forcing fields CISSEMBEL shall expect. CISSEMBEL can
 compute turbulent fluxes internally from provided atmospheric
 fields. In this case, the full set of height corrections can be
 considered. Select this setup by activating the preprocessor
 `#define SETUP_CISSEMBEL_HC`. Other cases are described in
 [`./src/EBMdriver.F08`](./src/EBMdriver.F08). The source code should
 be ready for Surface Mass Balance (_SMB_) simulations. Each
 preprocessor switch has specific code parts in
 [`./src/EBMdriver.F08`](./src/EBMdriver.F08).

Depending on the configuration, the binary file name is different:
 *CISSEMBEL.xS* for serial code, *CISSEMBEL.xO* for OpenMP-enabled
 code, *CISSEMBEL.xM* for code that supports MPI, and
 *CISSEMBEL.xMO* for hybrid code. If you select the
 debugging flag (`DEBUG_FLAGS ?= on`) the file name ends with *G*. The
 following lists build binaries; the first ones are optimized, and the
 latter have debug statements and stronger checking included.

- Serial code: `CISSEMBEL.xS`,`CISSEMBEL.xSG`
- Parallel OpenMP code: `CISSEMBEL.xO`, `CISSEMBEL.xOG`
- Parallel MPI (Message Passing Interface) code: `CISSEMBEL.xM`,
`CISSEMBEL.xMG`
- Hybrid code considering MPI and OpenMP and MPI:
`CISSEMBEL.xMO`, `CISSEMBEL.xMOG`


Start the comment `make` to compile the code. The name of the build
 executable is printed at the end of a successful compilation. The
 pure `make` command builds a serial binary. Supposed you want to
 build an binary supporting OpenMP and do not want to modify
 [`./src/makefile`](./src/makefile), you run the following command to
 build an OpenMP binary:

 ```
 make OPENMP_PARALLEL=on
 ```

For the Fortran driver [`./src/EBMdriver.F08`](./src/EBMdriver.F08),
 you can choose if all atmospheric data is read once and computation
 starts afterward or if sequential data reading and computation is
 used for each forcing data record. In the first case, the memory
 demand is higher, but the computation time is potentially lower. In
 contrast, sequential data access might be required for a huge forcing
 data volume exceeding the available main memory. Although the
 sequential data access is standard, you can request the reading of
 atmospheric forcing data at once by setting the variable
 `flag_read_input_once=T`. This variable is part of the namelist
 section `&offline_driver_ctrl` in the namelist file
 `namelist.EBMdriver`.

Build the dynamic library for offline simulations by a Python driver
====================================================================
To use any other program language such as C, C++, or Python, we have
 to compile the dynamic library called `CISSEMBEL_CBindings.so`. To
 use the Python driver ([`./src/cissembel.py`](./src/cissembel.py))
 you build this required dynamic library via `make
 CISSEMBEL4py`. Afterward, you run the Python code driver via `python
 cissembel.py` or `python cissembel.py > display_information.log` to
 save the logging information in the file
 `display_information.log`. Since this Python driver loads all
 atmospheric data into the main memory; you will recognize a higher
 main memory demand compared to the sequential data reading by the
 Fortran driver [`./src/EBMdriver.F08`](./src/EBMdriver.F08). For
 details, see above.

The compilation of the dynamic library `CISSEMBEL_CBindings.so` also
 builds the binary, including the Fortran driver. The name of this
 build binary (see above) indicates the capabilities of the compiled
 dynamic library. For instance, OpenMP parallelization is also used by
 code driven by a Python driver if the code has been compiled accordingly.

Tests of the code
=================
To allow testing the code, some functions and subroutines are
 accessible via corresponding wrapper functions and subroutines which
 offers c-binding support. These c-bindings are utilized by Python
 test scripts to analyze these functions and subroutines.

Some tests are started by calling `make tests` in the source code
 directory [`./src`](./src), which will produce plots of numerous used
 parameterizations in a subdirectory
 [`./src/Figure/PNG`](./src/Figure/PNG) - this subdirectory is being
 created during the run of `make tests` and does not exist otherwise.


OpenMP Parallelization
======================
Supposed you use OpenMP enabled code, the environmental variable
 `OMP_NUM_THREADS` defines the number of threads used in OpenMP
 parallel sections of the code; if undefined, all available cores are
 usually utilized. The second environmental variable `OMP_SCHEDULE`
 sets the scheduling for the big work-intensive loop, where
 `modEBMcolumn` is called. Since adaptive time steps are used during
 the diffusion calculation, a load unbalancing may occur if the
 standard scheduling named `static` is used. In case of a significant
 load imbalance, the scheduling type `dynamic` may yield a better
 performance. You may define, in addition, the so-called chunk number
 size for large problems. Please inspect the documentation of the
 OpenMP consortium [https://www.openmp.org](https://www.openmp.org).


Installation of the required dependencies
=========================================
Certain operation systems allow to install the CISSEMBEL's
  dependencies without much effort. CISSEMBEL has the following
  dependencies

  - Fortran compiler, e.g., `gfortran` (required)
  - NetCDF library including Fortran support (required)
  - `make` (required)
  - `doxygen` (optional; creates code documentation)
  - Python including packages (optional)

The input and output of files is performed via the NetCDF (Network
  Common Data Form) library; see
  [www.unidata.ucar.edu/software/netcdf](https://www.unidata.ucar.edu/software/netcdf/)
  (last access: 2023-12-05) for details. On certain system, it is not
  required to build this library from scratch, instead you can install
  it via an integrated repository as part of your operating system.

Here, we restrict us to CISSEMBEL configurations that are parallelized
  via OpenMP (Open Multi-Processing)
  [www.openmp.org](https://www.openmp.org/) (last access:
  2023-12-05). In addition, we do not address Python here because the
  installation of Python may differ depending on your own preferred
  choices. This restriction includes Python drivers and the test of
  CISSEMBEL via `make test`. Nevertheless, you find the Anaconda
  environment file
  [`./examples/environment/cissembel.yaml`](./examples/environment/cissembel.yaml)
  allowing to install a python environment that shall work together
  with the current CISSEMBEL code.

Furthermore, CISSEMBEL is primarily supported on Unix operation
  systems, such as Linux. If you prefer to run it under a MS Windows
  Operation System, you may try the _cygwin_ environment or the
  _Windows subsystem for Linux_ (WSL). Please share your experience
  because we would like to make it available to the community.

Please note known issues reported in
  [KNOWN_ISSUES.md](KNOWN_ISSUES.md).

## Debian/Ubuntu Linux systems
To install the dependencies on Debian/Ubuntu Linux systems you need
  sufficient administrative right -- if you do not have those rights,
  ask your system administrator please. The installation of the
  general requirements (git, Fortran compiler, doxygen, ...) and the
  NetCDF library may be performed via the command below. It shall
  automatically resolve and install further depending programs.

  ```
  sudo apt-get git gfortran make libnetcdf-dev libnetcdff-dev netcdf-bin doxygen
  ```

As auxiliary programs, you may install the Climate Data Operators
  (_CDO_)
  [code.mpimet.mpg.de/projects/cdo](https://code.mpimet.mpg.de/projects/cdo)
  (last access: 2023-12-05), the netCDF Operators (_NCO_)
  [nco.sourceforge.net](https://nco.sourceforge.net/) (last
  access: 2023-12-05), and ncview
  [cirrus.ucsd.edu/ncview](https://cirrus.ucsd.edu/ncview/) (last
  access: 2023-12-05). You can install those via

  ```
  sudo apt-get install cdo nco ncview
  ```

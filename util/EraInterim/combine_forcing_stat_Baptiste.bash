#!/usr/bin/env bash
set -e

# (c) Christian Rodehacke, 2023, DMI Copenhagen, Denmark
#
# -------------------------------------------------------------------
# This file is part of CISSEMBEL, which is the
# == Copenhagen Ice-Snow Surface Energy and Mass Balance modEL ==
#
# CISSEMBEL is free software; you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE  v.1.2
#
# Information about the EUROPEAN UNION PUBLIC LICENCE (*EUPL*) and
# translations into other European languages are available at
# https://eupl.eu (last access: 2021-10-01)](https://eupl.eu/)
#
# We hope that  this  distributed  CISSEMBEL  code, its  tools and
# documentations are usefull. Any improvements of the code, tools,
# and documentations tools are  very welcome.  Please, consider to
# share your thoughts and improvements with the community.
#
# The documentation,  tools,  and the code are provided as it are.
# NO WARRANTY  ABOUT FITNESS  AND USABILITY OF THE CODE, TOOLS, OR
# DOCUMENTATION  ARE  GIVEN.   NO  WARRANTY  IS  GIVEN  ABOUT  THE
# CORRECTNESS  OF COMPUTED RESULTS WITH THE  PROVIDED CODE, TOOLS,
# AND DOCUMENTATION. YOU USE THE CODE, DOCUMENTATION, AND TOOLS AT
# YOUR OWN RISK.           Removing  of  any  copyright  statement
# or  this  disclaimer  is considered  as a  attempt  to defraud.
#
# -------------------------------------------------------------------


python PROMICE_baptiste2initial.py \
       -i ./ORG/PROMICE_subsurface_temperature_all_depths_hourly.nc \
       -o PROMICE_Baptiste_initial.nc \
       --vname_output_xaxis x --vname_output_yaxis y \
       --vname_output_surface_elevation usrf \
       --vname_output_longitude lon --vname_output_latitude lat \
       --dname_output_xaxis x1 --dname_output_yaxis y1

ncatted -h \
    -a coordinates,lon,d,, \
    -a coordinates,lat,d,, \
    -a coordinates,site_label,d,, \
    -a coordinates,usrf,o,c,'lon lat' \
    -a coordinates,surface_slope,o,c,'lon lat' \
    -a coordinates,lon_promice_means,o,c,'lon lat' \
    -a coordinates,lat_promice_means,o,c,'lon lat' \
    PROMICE_Baptiste_initial.nc

echo "============================================================="
echo "#                                                           #"
echo "#        Transform EraInterim to PROMICE forcing            #"
echo "#          Create initial file(s) for CISSEMBEL             #"
echo "#                                                           #"
echo "============================================================="
if [[ 1 -eq 1 ]] ; then
    for F in ~/work/Data/EraInterim/EraInterim1979_2017.GrIS/ATMOS_????.GrIS.ISMIP6.nc4
    do
	~/src/CISSEMBEL/util/EraInterim/EraInterim2PROMICE_Baptiste.bash $F
    done
fi

echo "============================================================="
echo "#                                                           #"
echo "#             Combine the forcing files                     #"
echo "#                 Create statistics                         #"
echo "#                                                           #"
echo "============================================================="

if [[ 1 -eq 0 ]] ; then
    echo " Fetch the data from ./ORG"
    ln -sf ORG/forcingfrompromice.nc
    cp ORG/simplegridfrompromice.nc .

    echo " Adjust the surface elevation 'usrf' field"
    [ ! -f usrf_slope.nc ] && ncgen -o usrf_slope.nc usrf_slope.cdl
    ncks -Av usrf usrf_slope.nc simplegridfrompromice.nc
fi
echo "============================================================="
#
# Initial file
#
FINAL_INITIAL="PROMICE_Baptiste_initial.ATMOS_xxxx.GrIS.ISMIP6.nc"
DIR_NCinitial="NC.initial"

echo " Create one initial file"
START_INITIAL="PROMICE_Baptiste_initial.ATMOS_1979.GrIS.ISMIP6.nc"
[ -f $START_INITIAL ] && ncrename -Oh -v usrf,surface_elevation $START_INITIAL $FINAL_INITIAL
[ ! -f $FINAL_INITIAL ] && cp -fp $START_INITIAL $FINAL_INITIAL


echo " - Move initial file duplicates to $DIR_NCinitial"
[ -d $DIR_NCinitial ] || mkdir -p $DIR_NCinitial
for FILE in PROMICE_Baptiste_initial.ATMOS_1[9][0-9][0-9].GrIS.ISMIP6.nc \
	    PROMICE_Baptiste_initial.ATMOS_2[0][0-9][0-9].GrIS.ISMIP6.nc
do
    if [ -f $FILE ] ; then
	echo "   $FILE"
	mv $FILE $DIR_NCinitial
    fi
done

#
# Forcing file: 2007-2017
#
FINAL_FORCING="Forcing.PROMICE_Baptiste.nc.ATMOS_2007_2017.GrIS.ISMIP6.nc"
echo " Merge annual forcing in one file  $FINAL_FORCING"
cdo -O mergetime \
    Forcing.PROMICE_Baptiste.nc.ATMOS_2[0][0][7-9].GrIS.ISMIP6.nc \
    Forcing.PROMICE_Baptiste.nc.ATMOS_2[0][1][0-7].GrIS.ISMIP6.nc $FINAL_FORCING

echo " Compute statistics"
INPUT=$FINAL_FORCING
for OPR in timmean daymean ymonmean monmean ; do
    echo " - $OPR"
    OUTPUT=${INPUT%%.nc}.${OPR}.nc
    cdo -s ${OPR} -shifttime,-1sec $INPUT $OUTPUT
done


#
# Forcing file: 1979-2017
#
FINAL_FORCING="Forcing.PROMICE_Baptiste.nc.ATMOS_1979_2017.GrIS.ISMIP6.nc"
echo " Merge annual forcing in one file  $FINAL_FORCING"
cdo -O mergetime Forcing.PROMICE_Baptiste.nc.ATMOS_[1,2][0-9][0-9][0-9].GrIS.ISMIP6.nc $FINAL_FORCING
#ncrcat PROMICE_Baptiste_initial.nc.ATMOS_[1,2][0-9][0-9][0-9].GrIS.ISMIP6.nc $FINAL_FORCING

echo " Compute statistics"
INPUT=$FINAL_FORCING
for OPR in timmean daymean ymonmean monmean ; do
    echo " - $OPR"
    OUTPUT=${INPUT%%.nc}.${OPR}.nc
    cdo -s ${OPR} -shifttime,-1sec $INPUT $OUTPUT
done


echo "============================================================="
echo "#                                                           #"
echo "#           Create restart file for CISSEMBEL               #"
echo "#                                                           #"
echo "============================================================="
~/src/CISSEMBEL/util/aveclim2start.py \
    --input_climate $FINAL_FORCING \
    --input_initial $FINAL_INITIAL \
    --output restart_time0_PROMICE_Baptiste_initial.nc.ATMOS_1979_2017.GrIS.ISMIP6.nc \
    --vname_climate_airtemperature t2m \
    --vname_climate_precipitation pr \
    --vname_initial_surface_elevation surface_elevation \
    --vname_initial_axis_left y \
    --vname_initial_axis_right x

echo "============================================================="
echo "#                                                           #"
echo "#       Extend initial file with SeaRISE data fields        #"
echo "#                                                           #"
echo "============================================================="
FNAME_SeaRise=/home/cr/src/CISSEMBEL/examples/data.initial.greenland/Greenland_100km.kaxis10.nc
FNAME_SeaRise_PROMICE="Greenland_SeaRise_PROMICE.nc"
FNAME_griddes="griddes.promice.txt.dat"
cdo griddes -selvar,surface_elevation $FINAL_INITIAL  > $FNAME_griddes

cdo remapnn,${FNAME_griddes} \
    -selvar,Basins,BasinsINTExtended,surface_slope \
    ${FNAME_SeaRise} ${FNAME_SeaRise_PROMICE}

ncrename -v surface_slope,surface_slope_SeaRise \
	 -v Basins,Basins_ORG \
	 -v BasinsINTExtended,Basins \
	 ${FNAME_SeaRise_PROMICE}


exit 0

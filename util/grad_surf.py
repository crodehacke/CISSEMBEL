#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Git version control id: $Id$

Created on Wed Apr 27 19:01:36 UTC 2022

-- Computation of the surface slope (absolute surface gradient)

Call example (1): with equi-distant x- and y-axis as arrays
    ./grad_surf.py --input surface_elevation_input.nc \
                   --output slope.nc \
                   --vname_input_surface usurf \
                   --vname_input_axis0 x \
                   --vname_input_axis1 y

NOTE: The axis count is right aligned. It means if you have the following
      netcdf field, e.g., "float surface_altitude(time, y, x) ;" , you should
      use the following vname_input_axis calls
      "--vname_input_axis0 x --vname_input_axis1 y"

Call example (2): with equi-distant x- and y-axis as arrays
    ./grad_surf.py --input Greenland_5km_v1.1.nc \
                   --output Greenland_5km_v1.1.slope.nc \
                   --vname_input_surface usrf \
                   --vname_input_axis1 y1 \
                   --vname_input_axis0 x1

Call example (3): with fields of x- and y-grid-sizes
    ./grad_surf.py --input lonlat2dxdy.nc \
                   --output lonlat2dxdy.slope.nc \
                   --vname_input_surface surface_elevation \
                   --vname_input_axis0 x_axis \
                   --vname_input_axis1 y_axis \
                   --vname_input_field_dx delta_x \
                   --vname_input_field_dy delta_y \
                   --vname_input_lon longitude \
                   --vname_input_lat latitude


@author: Christian Rodehacke, DMI Copenhagen, Denmark

@copyright: Copyright (C) 2016-2025 Christian Rodehacke
"""
# -------------------------------------------------------------------
# This file is part of CISSEMBEL, which is the
# == Copenhagen Ice-Snow Surface Energy and Mass Balance modEL ==
#
# CISSEMBEL is free software; you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE  v.1.2
#
# Information about the EUROPEAN UNION PUBLIC LICENCE (*EUPL*) and
# translations into other European languages are available at
# https://eupl.eu (last access: 2023-09-18)](https://eupl.eu/)
#
# We hope that  this  distributed  CISSEMBEL  code, its  tools and
# documentations are usefull. Any improvements of the code, tools,
# and documentations tools are  very welcome.  Please, consider to
# share your thoughts and improvements with the community.
#
# The documentation,  tools,  and the code are provided as it are.
# NO WARRANTY  ABOUT FITNESS  AND USABILITY OF THE CODE, TOOLS, OR
# DOCUMENTATION  ARE  GIVEN.   NO  WARRANTY  IS  GIVEN  ABOUT  THE
# CORRECTNESS  OF COMPUTED RESULTS WITH THE  PROVIDED CODE, TOOLS,
# AND DOCUMENTATION. YOU USE THE CODE, DOCUMENTATION, AND TOOLS AT
# YOUR OWN RISK.           Removing  of  any  copyright  statement
# or  this  disclaimer  is considered  as a  attempt  to defraud.
#
# -------------------------------------------------------------------

# -------------------------------------------------------------------------
def parse_arguments():
    '''
    Parse the command line arguments

    Returns
    -------
    list
        List of strings indicating plot formats: ['png', 'pdf'].

    '''
    parser = argparse.ArgumentParser(
        description='Convert lon and lat into dx, dy',
        add_help=False)

    parser.add_argument('--input', '-i',
                        nargs='?',
                        required=True,
                        help='Input file name containing the surface elevation')

    parser.add_argument('--output', '-o',
                        nargs='?',
                        required=False,
                        default='surface_slope.nc',
                        help='Output file name containing the surface slope')

    parser.add_argument('--vname_input_surface',
                        nargs='?',
                        required=False,
                        default='usrf',
                        help='variable name representing the surface elevation')

    parser.add_argument('--vname_input_axis0',
                        nargs='?',
                        required=False,
                        default='x',
                        help='variable name of the first horizontal axis (right-most)')

    parser.add_argument('--vname_input_axis1',
                        nargs='?',
                        required=False,
                        default='y',
                        help='variable name of the second horizontal axis (2nd right-most)')

    parser.add_argument('--vname_input_lon',
                        nargs='?',
                        required=False,
                        default='lon',
                        help='variable name of the longitude')

    parser.add_argument('--vname_input_lat',
                        nargs='?',
                        required=False,
                        default='lat',
                        help='variable name of the latitude')

    parser.add_argument('--vname_input_field_dx',
                        nargs='?',
                        required=False,
                        default=None,
                        help='variable name of the delta_x field')

    parser.add_argument('--vname_input_field_dy',
                        nargs='?',
                        required=False,
                        default=None,
                        help='variable name of the delta_y field')

    parser.add_argument('--output_format',
                        nargs='?',
                        required=False,
                        default='NETCDF3_CLASSIC',
                        choices=["NETCDF4", "NETCDF4_CLASSIC", "NETCDF3_64BIT", "NETCDF3_CLASSIC"],
                        help='Format of the netcdf output file')
    #
    # Plot
    #
    parser.add_argument('--plot', action='store_true',
                        required=False,
                        help='Create a plot')

    parser.add_argument('--plot_format', nargs='?',
                        required=False,
                        default='png',
                        choices=['png', 'pdf', 'svg', 'svgz', 'eps', 'ps', 'jpg', 'tif'],
                        help='File format of plot image')


    if not parser.parse_args().plot:
        parser.parse_args().plot_format = None

    # # Report used command line arguments
    # for single_arg in vars(parser.parse_args()): #sorted(vars(args)) :
    #         print('  - {:21s}'.format(single_arg)+' = '+
    #               str(getattr(parser.parse_args(), single_arg)) )

    #If you want to convey all switches outside, use:  return parser.parse_args()
    return parser.parse_args()

# -------------------------------------------------------------------------
def print_plot(fig_id, plot_name, suffix='png', flag_close=True):
    '''
    Generate plot

    Parameters
    ----------
    fig_id : matplotlib.figure.Figure
        Figure handle.
    plot_name : str
        Prefix of the plot's filename.
    suffix : str, optional
        Filename suffix defining also the figure file format. The default is 'png'.
    flag_close : bool, optional
        Close figure after print, which save memory. The default is True.

    Returns
    -------
    None.

    '''
    print('    Create plot "'+plot_name+'"')

    filename = plot_name+'.'+suffix
    print('      - Print '+filename)
    fig_id.savefig(filename, orientation='landscape', \
                       transparent='false', facecolor='white') #papertype='a4', \
    if flag_close:
        print('      => Close figure')
        plt.close(fig_id)


def open_read_input(filename, vname_elevation,
                    vname_longitude='longitude',
                    vname_latitude='latitude',
                    vname_axis0st='x',
                    vname_axis1nd='y',
                    vname_fld_dx=None,
                    vname_fld_dy=None):
    '''
    Reads the input file

    Parameters
    ----------
    filename : str
        input file holding the surface elevation, distance in x- and y-direction.
    vname_elevation : str
        Variable name of the elevation field.
    vname_longitude : str, optional
        Variable name of the longitude field. default is 'longitude'.
    vname_latitude : str, optional
        Variable name of the latitude field. The default is 'latitude'.
    vname_axis0st : str, optional
        Variable name of the first/right-most horizontal array (dimension). The default is 'x'.
    vname_axis0nd : str, optional
        Variable name of the second/2nd right-most horizontal array (dimension). The default is 'y'.
    vname_fld_dx : str, optional
        Variable name of the first/right-most grid size horizontal field (dimension). The default is 'None'.
    vname_fld_dy : str, optional
        Variable name of the second right most grid size horizontal field (dimension). The default is 'None'.

    Returns
    -------
    surf_eleva : numpy.ndarray
        Array of the surface elevation.
    axis0st : numpy.ndarray
        First (right-most) horizontal axis-array.
    axis1nd : numpy.ndarray
        Second (right-most) horizontal axis-array.
    lon_ : numpy.ndarray
        longitude field.
    lat_ : numpy.ndarray
        latitude field.
    dxdy_ : numpy.ndarray
        grid size fields.
    topofile_ : xarray.core.dataset.Dataset
        Input data file object.

    '''

    # Open the data file
    print("Open input file '"+filename+"'")

    topofile_ = xr.open_dataset(filename, decode_times=False)

    #  Accessing data fields
    surf_eleva = topofile_[vname_elevation].squeeze()
    axis0st = topofile_[vname_axis0st].squeeze()
    axis1nd = topofile_[vname_axis1nd].squeeze()

    if vname_longitude is not None and vname_latitude is not None:
        lon_ = topofile_[vname_longitude].squeeze()
        lat_ = topofile_[vname_latitude].squeeze()
    else:
        lon_ = None
        lat_ = None

    if vname_fld_dx is not None and vname_fld_dy is not None:
        dxf = topofile_[vname_fld_dx]
        dyf = topofile_[vname_fld_dy]
        dxdy_ = np.stack((dxf, dyf), axis=0)
        del dxf, dyf
    else:
        dxdy_ = None

    return surf_eleva, axis0st, axis1nd, lon_, lat_, dxdy_, topofile_


def compute_grid_distance(axis_0, axis_1):
    '''
    Computes from the distance axis the grid size

    Parameters
    ----------
    axis_0 : numpy.ndarray
        First horizontal axis-array.
    axis_1 : numpy.ndarray
        Second horizontal axis-array.

    Returns
    -------
    delta_x : float or numpy.ndarray
        Grid distance in x-direction.
    delta_y : float or numpy.ndarray
        Grid distance in y-direction.
    delta_xy : numpy.ndarray
        Grid distance (2d) in x- and y-direction.
    string_grid_distance : str
        String describing the grid distances as metadata for the output.

    '''
    delta_x_ = np.gradient(axis_0)
    delta_y_ = np.gradient(axis_1)

    delta_x = delta_x_
    if all(delta_x_ == delta_x_[0]):
        delta_x = delta_x_[0]
    delta_y = delta_y_
    if all(delta_y == delta_y[0]):
        delta_y = delta_y[0]

    if delta_x.ndim or delta_y.ndim:
        print("  Construct stacked grid distance field")
        delta_x2, delta_y2 = np.meshgrid(delta_y_, delta_x_)
        delta_xy = np.stack((delta_x2, delta_y2))
            # ^ -- If changed, adjust below: dx2=dxy[1], dy2=dxy[0]

        string_grid_distance = "dx = [{:.2f},".format(delta_x_.min())+ \
            " {:.2f}] ".format(delta_x_.max())
        if 'units' in axis_0.attrs:
            string_grid_distance += axis_0.attrs['units']

        string_grid_distance += ", dy = [{:.2f},".format(delta_y_.min())+ \
            " {:.2f}] ".format(delta_y_.max())
        if 'units' in axis_1.attrs:
            string_grid_distance += axis_1.attrs['units']
    else:
        if 'units' in axis_0.attrs and 'units' in axis_1.attrs:
            print("  Equal distance along 1st axis {:.2f} ".format(delta_x)
                  +axis_0.attrs['units']+
                  ", and along 2nd axis {:.2f} ".format(delta_y)
                  +axis_1.attrs['units'])
        else:
            print("  Equal distance along 1st axis {:.2f} step(s)".format(delta_x)+
                  ", and along 2nd axis {:.2f} step(s)".format(delta_y))
        delta_xy = None

        string_grid_distance  = "dx = {:.2f} ".format(delta_x)
        if 'units' in axis_0.attrs:
            string_grid_distance += axis_0.attrs['units']
        string_grid_distance += ", dy = {:.2f} ".format(delta_y)
        if 'units' in axis_1.attrs:
            string_grid_distance += axis_1.attrs['units']

    return delta_x, delta_y, delta_xy, string_grid_distance


def compute_surface_slope(usurf, delta_x, delta_y, delta_xy):
    '''
    Computes the surface slope (absolute surface elevation gradient)

    Parameters
    ----------
    usurf : numpy.ndarray
        Array of the surface elevation.
    delta_x : float or numpy.ndarray
        Grid distance in x-direction.
    delta_y : float or numpy.ndarray
        Grid distance in y-direction.
    delta_xy : numpy.ndarray
        Grid distance (2d) in x- and y-direction.

    Returns
    -------
    surf_slope : numpy.ndarray
        Slope of the surface elevation.

    '''

    delta_x = np.array(delta_x)
    delta_y = np.array(delta_y)
    if delta_x.size < 1 or delta_y.size < 1:
        g_usurf_vect0 = np.array(np.gradient(usurf.data))
        g_usurf_vect = np.array(np.abs(g_usurf_vect0)/delta_xy)
    else:
        g_usurf_vect = np.gradient(usurf.data, delta_y, delta_x)

    surf_slope = np.sqrt(np.max(np.square(g_usurf_vect), axis=0))

    return surf_slope

def create_plot(slope, elev, axis_0, axis_1, plot_suffix):
    '''
    Plot of the computed surface slope field

    Parameters
    ----------
    slope : numpy.ndarray
        Slope of the surface elevation (m m-1).
    elev : numpy.ndarray
        Surface elevation (Meter)
    axis_0 : numpy.ndarray
        First horizontal axis-array.
    axis_1 : numpy.ndarray
        Second horizontal axis-array.
    plot_suffix : str
        Filename suffix defining also the figure file format.

    Returns
    -------
    None.

    '''
    #
    print('')
    fig0 = plt.figure(dpi=300, facecolor='white')
    axes0 = fig0.subplots()

    pcmid = axes0.pcolormesh(axis_1, axis_0, slope,
                             shading='nearest',
                             cmap='plasma', vmin=0., vmax=slope.max())
    axes0.contour(axis_1, axis_0, elev, colors='k', alpha=0.25,
                  linestyles='dashed', linewidths=1,
                  levels=[0, 1000, 2000, 3000, 4000])
    axes0.set_xlabel(axis_1.attrs['long_name']+" ("+axis_1.attrs['units']+")")
    axes0.set_ylabel(axis_0.attrs['long_name']+" ("+axis_0.attrs['units']+")")
    cbar = fig0.colorbar(pcmid, ax=axes0)
    cbar.set_label('Slope (m m-1)')

    plt.draw()

    if plot_suffix:
        print_plot(fig0, 'surface_slope', plot_suffix)

def write_slope_file(topofile_obj, slope, axis_0, axis_1, dax_1, dax_2,
                     vn_lon, vn_lat, vn_ax0, vn_ax1, vn_surf,
                     file_input, file_output, str_grid_dxdy,
                     netcdf_format='NETCDF3_CLASSIC',
                     parse_arguments='Unknown'):
    '''
    Write the output file containing the slope field

    Parameters
    ----------
    topofile_obj : xarray.core.dataset.Dataset
        Input data file object.
    slope : numpy.ndarray
        Slope of the surface elevation.
    axis_0 : numpy.ndarray
        Right-most horizontal axis-array.
    axis_1 : numpy.ndarray
        Second right-most horizontal axis-array.
    dax_1 : numpy.ndarray
        Grid distance of first horizontal axis-array.
    dax_2 : numpy.ndarray
        Grid distance of second horizontal axis-array.
    vn_lon : str
         Longitude variable name in topofile_obj.
    vn_lat : str
         Latitude variable name in topofile_obj.
    vn_ax0 : str
         Variable name of the right-most horizontal axis-array.
    vn_ax1 : str
         Variable name of the 2nd right-most horizontal axis-array.
    vn_surf : str
         Variable name of the surface elevation field.
    file_input : str
         Input filename holding the elevation field.
    file_output : str
         Output filename.
    str_grid_dxdy : str
        String describing the grid distances as metadata for the output.
    netcdf_format : str
        String describing the netcdf format of the output, The default is NETCDF3_CLASSIC.
    parse_arguments : Namespace
        Parsed input arguments or its standard values, The default is None.

    Returns
    -------
    None.

    '''

    # Create the surface elevation output field and file
    if vn_lon is not None and vn_lat is not None:
        slope_dat = xr.Dataset(
            data_vars=dict(surface_slope=([vn_ax1, vn_ax0], slope),
                           delta_x=([vn_ax1, vn_ax0], dax_1),
                           delta_y=([vn_ax1, vn_ax0], dax_2),
                           surface=topofile_obj[vn_surf],
                           lon=topofile_obj[vn_lon],
                           lat=topofile_obj[vn_lat]
                          ),
            coords={vn_ax1: ([vn_ax1], axis_1.data),
                    vn_ax0: ([vn_ax0], axis_0.data),
                },
            attrs=[('title', 'surface slope data'),
                   ('Input_file', file_input),
                   ('grid_resolution', str_grid_dxdy),
                   ('applied_settings', str(parse_arguments)),
                   ]
            )
    else:
        slope_dat = xr.Dataset(
            data_vars=dict(surface_slope=([vn_ax1, vn_ax0], slope),
                           delta_x=([vn_ax1, vn_ax0], dax_1),
                           delta_y=([vn_ax1, vn_ax0], dax_2),
                           surface=topofile_obj[vn_surf],
                          ),
            coords=dict([(vn_ax1, axis_0), (vn_ax0, axis_1)]),
            attrs=[('title', 'surface slope data'),
                   ('Input_file', file_input),
                   ('grid_resolution', str_grid_dxdy),
                   ('applied_settings', str(parse_arguments)),
                  ])

    #            coords=dict([(vn_ax1, axis_0), (vn_ax0, axis_1)]),

    slope_dat['surface_slope'].attrs=[
        ('longname', 'surface elevation slope'),
        ('units', 'm m-1'),
        ]
    slope_dat['delta_x'].attrs=[
        ('longname', 'grid distance x-coordinate'),
        ('units', 'meter'),
        ]
    slope_dat['delta_y'].attrs=[
        ('longname', 'grid distance y-coordinate'),
        ('units', 'meter'),
        ]

    print("Write output file '"+file_output+"' as "+netcdf_format)
    slope_dat.to_netcdf(file_output, format=netcdf_format)

# -------------------------------------------------------------------------
#
# Main program
#
if __name__ == '__main__':
    import argparse
    import numpy as np
    import xarray as xr
    import matplotlib.pyplot as plt

    parsed_argv = parse_arguments()

    #
    # Read the input data
    #
    surface_elevation, axis0, axis1, lon, lat, delta_dxy_fields, topofile = \
        open_read_input(parsed_argv.input,
                        parsed_argv.vname_input_surface,
                        parsed_argv.vname_input_lon,
                        parsed_argv.vname_input_lat,
                        parsed_argv.vname_input_axis0,
                        parsed_argv.vname_input_axis1,
                        parsed_argv.vname_input_field_dx,
                        parsed_argv.vname_input_field_dy)


    if parsed_argv.vname_input_field_dx is None and parsed_argv.vname_input_field_dy is None :
        print(' Compute distances as 2dim fields')
        #
        # Compute the grid distance
        #
        dx, dy, dxy, string_grid = compute_grid_distance(axis0, axis1)

        #
        # Compute 2dim grid distance field
        #
        if dxy is None:
            delta_x_field, delta_y_field = \
                np.meshgrid(np.zeros_like(axis0)+dy, np.zeros_like(axis1)+dx)
        else:
            delta_x_field = dxy[1]
            delta_y_field = dxy[0]
    else:
        print(' Use existing distances as 2dim fields')
        dx = []
        dy = []
        delta_x_field = delta_dxy_fields[0,::]
        delta_y_field = delta_dxy_fields[1,::]
        dxy = delta_dxy_fields
        string_grid = "Grid distance individually defined at each grid point"

    #
    # Compute the surface slope (absolute surface elevation gradient)
    #
    surface_slope = compute_surface_slope(surface_elevation, dx, dy, dxy)

    #
    #  Plot?
    #
    if parsed_argv.plot:
        create_plot(surface_slope, surface_elevation, axis0, axis1,
                    parsed_argv.plot_format)

    #
    # Write the surface slope, grid distance, ...
    #
    write_slope_file(topofile, surface_slope, axis0, axis1,
                     delta_x_field, delta_y_field,
                     parsed_argv.vname_input_lon,
                     parsed_argv.vname_input_lat,
                     parsed_argv.vname_input_axis0,
                     parsed_argv.vname_input_axis1,
                     parsed_argv.vname_input_surface,
                     parsed_argv.input,
                     parsed_argv.output,
                     string_grid,
                     parsed_argv.output_format,
                     parsed_argv)

# -- last line

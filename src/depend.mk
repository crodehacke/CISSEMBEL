# Git version control id: $Id: 8fbee3e341b7d3d9f4db67af6a6b851addb07e22 $
#
# === Empty `depend.mk` as standard case ===
#
# You may create a `depend.mk` file describing the dependencies
# adequately by running: `make depend`. However, this command may
# fail. Therefore, an empty `depend.mk` is the best fallback solution,
# even if it does not unnecessarily allow to run successfully parallel
# compilation tasks via the `make -j` command.
#

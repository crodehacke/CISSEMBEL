#!/usr/bin/env bash
# Git version control id: $Id$
#
# This script converts a PISM restart/state file into CISSEMBEL
# initial file.
#
# Set the variables in the top section labeled "Settings" and start
# the script by the command "./pism2cissembel_init.bash".
#
# (c) Christian Rodehacke, 2023, DMI Copenhagen, Denmark
#
# -------------------------------------------------------------------
# This file is part of CISSEMBEL, which is the
# == Copenhagen Ice-Snow Surface Energy and Mass Balance modEL ==
#
# CISSEMBEL is free software; you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE  v.1.2
#
# Information about the EUROPEAN UNION PUBLIC LICENCE (*EUPL*) and
# translations into other European languages are available at
# https://eupl.eu (last access: 2023-09-18)](https://eupl.eu/)
#
# We hope that  this  distributed  CISSEMBEL  code, its  tools and
# documentations are usefull. Any improvements of the code, tools,
# and documentations tools are  very welcome.  Please, consider to
# share your thoughts and improvements with the community.
#
# The documentation,  tools,  and the code are provided as it are.
# NO WARRANTY  ABOUT FITNESS  AND USABILITY OF THE CODE, TOOLS, OR
# DOCUMENTATION  ARE  GIVEN.   NO  WARRANTY  IS  GIVEN  ABOUT  THE
# CORRECTNESS  OF COMPUTED RESULTS WITH THE  PROVIDED CODE, TOOLS,
# AND DOCUMENTATION. YOU USE THE CODE, DOCUMENTATION, AND TOOLS AT
# YOUR OWN RISK.           Removing  of  any  copyright  statement
# or  this  disclaimer  is considered  as a  attempt  to defraud.
#
# -------------------------------------------------------------------
set -e
#set -vx


# ===================================================
#
# Settings
#
# Input files
PISM_FNAME=pism_ini.1851.nc              # Source filename
TOPO_SURFACE_FNAME=${PISM_FNAME}         # Topography surface (for slope calculation)
DOMAIN_LABEL="Greenland Ice Sheet, 5km"


# Output file
CISSEMBEL_INI_FNAME=${1:-"CISSEMBEL_init.nc"}  # Target filename (Output

#
# Select from PISM file wanted fields
#
VAR_NAME_TOPO="orog"
VAR_LIST_PISM="${VAR_NAME_TOPO},lon,lat"     # ESSENTIAL list
VAR_LIST_PISM="${VAR_NAME_TOPO},lon,lat,mask"
#VAR_LIST_PISM="${VAR_NAME_TOPO},lon,lat,mask,bheatflx,thk"

#
# CISSEMBEL Target file settings
#
DO_HORIZONTAL_INTEGER_AXIS=0
x_dimension_name='iaxis'
y_dimension_name='jaxis'

#
# Layer thickness of vertical layers
#
layer_thickness=(0.1666666666
		 0.6666666666
		 1.5
		 3.0
		 5.0
		 7.25
		 10.0
		 13.25
		 17.0
		 22.0)
#
# Height levels (if applicable)
#
height=(0.
	100.
	250.
	500.
	750.
	1000.
	1250.
	1500.
	2000.
	2400.
	2800.
	3500.
	10000.)

# ---------------------------------------------------
#
# Internal settings
#
fname_levels_cdl="${TMPDIR:-.}/levels.tmp.cdl"
fname_height_cdl="${TMPDIR:-.}/height.tmp.cdl"


# CDO="cdo"
# NCKS="ncks"
# NCWA="ncwa"
# NCATTED="ncatted"

CDO="cdo -s --history"
NCKS="ncks -h"
NCWA="ncwa -h"
NCATTED="ncatted -h"
NCRENAME="ncrename -h"

SLOPE_SCRIPT="./grad_surf.py"

# ===================================================
#
# Functions
#

function information {
    echo " PISM   PISM_FNAME: ${PISM_FNAME}"
    echo " Output CISSEMBEL_INI_FNAME: ${CISSEMBEL_INI_FNAME}"
    echo " Number of layers: ${#layer_thickness[@]}"
    echo "    Thickness"
    for VAL in ${layer_thickness[@]} ; do echo "        $VAL"; done
    echo " Number of height levels ${#height[@]}"
    echo "    Heights"
    for VAL in ${height[@]} ; do echo "        $VAL"; done
}


function checks {
    if [ ! -f ${PISM_FNAME} ] ; then
	echo " Missing PISM_FNAME $PISM_FNAME"
	echo " S T O P"
	exit 1
    fi

    if [ -f ${CISSEMBEL_INI_FNAME} ] ; then
	echo " Found output file CISSEMBEL_INI_FNAME ${CISSEMBEL_INI_FNAME}"
	echo " S T O P"
	exit 2
    fi
}


function create_thickness_axis {
    fname_=${1:?Unknow filename}
    NO_ELEMENTS=${#layer_thickness[@]}

    cat > $fname_ <<EOF_thickness
netcdf levels {
dimensions:
	kaxis = ${NO_ELEMENTS} ;
variables:
	int kaxis(kaxis) ;
		kaxis:standard_name = "model_level_number" ;
		kaxis:positive = "down" ;
		kaxis:long_name = "z-layer of mesh grid" ;
		kaxis:units = "1" ;
	double layer_thickness(kaxis) ;
		layer_thickness:standard_name = "snow_layer_thickness" ;
		layer_thickness:long_name = "thickness of each snow layer" ;
		layer_thickness:units = "meter" ;
	double depth(kaxis) ;
		depth:standard_name = "depth" ;
		depth:long_name = "depth below surface" ;
		depth:units = "meter" ;
		depth:positive = "down" ;
		depth:note = "depth of the layer center (vertical mean)" ;
		depth:comment = "computed from 'layer_thickness': depth(1)=0.5*layer_thickness(1);for k=2,size(kaxis); depth(k)=depth(k-1)+0.5*(layer_thickness(k-1)+layer_thickness(k)); end" ;


// global attributes:
		:Conventions = "CF-1.3" ;
		:history = "Layer thicknesses for CISSEMBEL ($(date))" ;
		:Creator = "${USER:-Unknown}" ;
		:Title = "Layer thickness" ;
data:

EOF_thickness

    #
    # Data section
    # - Coordinate variable (kaxis)
    # - Levels
    echo "kaxis = $(echo $(seq 1 ${NO_ELEMENTS})  | tr -s ' ' ',') ;  " >> $fname_
    echo "layer_thickness = $(echo ${layer_thickness[@]:0:NO_ELEMENTS} | tr -s ' ' ',') ;  " >> $fname_

    # - Depth
    #   Compute the depth
    depth=( $(echo "${layer_thickness[0]}*0.5 " | bc -l) ) # First depth
    for idx in $(seq 1 $(( NO_ELEMENTS - 1 )) )
    do
	depth+=($( echo "${depth[-1]}+0.5*(${layer_thickness[idx-1]}+${layer_thickness[idx]})" | bc -l))
    done
    #   Write the depth
    echo "depth = $(echo ${depth[@]:0:NO_ELEMENTS} | tr -s ' ' ',') ;  " >> $fname_


    # Close the definiton of the CDL (common data form) file
    echo "}" >> $fname_

    #
    # Create the netcdf file containing the layer thicknesses
    #
    ncgen -o ${fname_}.nc $fname_
}


function create_height_axis {
    fname_=${1:?Unknow filename}

    NO_ELEMENTS=${#height[@]}

    cat > $fname_ << EOF_height

netcdf height_axis {
dimensions:
	haxis = ${NO_ELEMENTS} ;
variables:
	int haxis(haxis) ;
		haxis:standard_name = "level_number" ;
		haxis:positive = "up" ;
		haxis:long_name = "height class elevation levels" ;
		haxis:units = "1" ;
	float height(haxis) ;
		height:standard_name = "height" ;
		height:long_name = "height class elevation levels" ;
		height:units = "meter" ;
		height:positive = "up" ;

// global attributes:
		:Conventions = "CF-1.3" ;
		:history = "Example for a height axis in CISSEMBEL" ;
		:Creator = "Christian Rodehacke - Danish Meteorological Institute, Copenhagen, Denmark" ;
		:Title = "Example Height axis for CISSEMBEL" ;
data:
EOF_height

    #
    # Data section
    # - Coordinate variable (haxis)
    # - Heights
    echo "haxis = $(echo $(seq 1 ${NO_ELEMENTS})  | tr -s ' ' ',') ;  " >> $fname_
    echo "height = $(echo ${height[@]:0:NO_ELEMENTS} | tr -s ' ' ',') ;" >> $fname_

    # Close the definitona of the CDL (common data form) file
    echo "}" >> $fname_

    #
    # Create the netcdf file containing the height levels
    #
    ncgen -o ${fname_}.nc $fname_
}



function rename_dim_axes  {
    fname_=${1}
    _xdim=${2:-'x'}
    _ydim=${3:-'y'}

    echo " Rename dimension 'x' to ${x_dimension_name} and  'y' to ${y_dimension_name}: $fname"
    $NCRENAME -d ${_xdim},${x_dimension_name} \
	      -d ${_ydim},${y_dimension_name} $fname_
    $NCATTED -a axis,${_xdim},d,, -a axis,${_ydim},d,, $fname_
}



function horizontal_int_axis {
    fname_=${1}
    _xdim=${2:-'x'}
    _ydim=${3:-'y'}
#    _xvar=${4:-'x'}
#    _yvar=${5:-'y'}

    x_length=$(ncdump -h $fname_ | grep -A 3 dimensions: | grep "${_xdim} =" | cut -d= -f2 | tr -d '[:punct:]')
    #x_length_minus=$(( x_length-1 ))
    x_array="$(seq -s, 1 ${x_length})"

    y_length=$(ncdump -h $fname_ | grep -A 3 dimensions: | grep "${_ydim} =" | cut -d= -f2 | tr -d '[:punct:]')
    #y_length_minus=$(( y_length-1 ))
    y_array="$(seq -s, 1 ${y_length})"

    echo " Size x_length  $x_length "
    echo " Size y_length  $y_length "

    cat > xx.cdl << EOF_xx
netcdf ${x_dimension_name} {
dimensions:
	${x_dimension_name} = ${x_length} ;
variables:
	int ${x_dimension_name}(${x_dimension_name}) ;
		${x_dimension_name}:long_name = "${x_dimension_name}-index of mesh grid" ;
		${x_dimension_name}:axis = "x";

// global attributes:
	:title = "generic coordinate variables for x-axis";
data:
${x_dimension_name} = ${x_array} ;
}
EOF_xx

    cat > yy.cdl << EOF_yy
netcdf ${y_dimension_name} {
dimensions:
	${y_dimension_name} = ${y_length} ;
variables:
	int ${y_dimension_name}(${y_dimension_name}) ;
		${y_dimension_name}:long_name = "${y_dimension_name}-index of mesh grid" ;
		${y_dimension_name}:axis = "y";
// global attributes:
	:title = "generic coordinate variables for y-axis";
data:
${y_dimension_name} = ${y_array} ;
}
EOF_yy

    for FILE in xx.cdl yy.cdl ; do
	ncgen -o ${FILE}.nc ${FILE} && rm ${FILE}
	$NCKS -A ${FILE}.nc ${fname_} && rm ${FILE}.nc
    done

}


# ===================================================
#
# Main
#

information
checks


$NCKS -v ${VAR_LIST_PISM} ${PISM_FNAME} ${CISSEMBEL_INI_FNAME}
$NCWA -a time ${CISSEMBEL_INI_FNAME} ${CISSEMBEL_INI_FNAME}.tmp \
    && mv ${CISSEMBEL_INI_FNAME}.tmp ${CISSEMBEL_INI_FNAME}

#
# Create and add needed layer thickness and height level axis
#
create_thickness_axis ${fname_levels_cdl}
create_height_axis ${fname_height_cdl}

$NCKS -A ${fname_levels_cdl}.nc ${CISSEMBEL_INI_FNAME}
$NCKS -A ${fname_height_cdl}.nc ${CISSEMBEL_INI_FNAME}

#
# Add Basins
#
if [ 1 -eq 1 ] ; then
    griddes_fname=${CISSEMBEL_INI_FNAME}.griddes
    basin_fname=BasinsGreenland.nc
    basin4pism_fname=${basin_fname}.4PISM.nc


    VAR_LIST="Basins,BasinsRestricted,BasinsExtended" #,glacmask_HIRHAM"

    #if [ -1 -eq 0 ] ; then
    #	ncrename -h -v maskbas,BasinsRestricted \
    #		 -v BasinsINTExtended,Basins \
    #		 -v glacmask,glacmask_HIRHAM \
    #		 ZwallyBasinsGreenlandExtended.nc \
    #		 ${basin_fname}
    #
    #	ncatted -h -a coordinates,BasinsRestricted,c,c,'lon lat' \
    #		-a coordinates,Basins,c,c,'lon lat' \
    #		-a coordinates,BasinsExtended,c,c,'lon lat' \
    #		-a coordinates,glacmask_HIRHAM,c,c,'lon lat' \
    #		${basin_fname}
    #fi

    $CDO griddes -selvar,${VAR_NAME_TOPO} ${CISSEMBEL_INI_FNAME} \
	> ${griddes_fname}

    $CDO remapnn,${griddes_fname} \
	-selvar,${VAR_LIST} \
	${basin_fname} ${basin4pism_fname}

    # $NCKS -A -v x,y ${CISSEMBEL_INI_FNAME} basin4pism_fname
    $NCKS -A -v ${VAR_LIST} \
	 ${basin4pism_fname} ${CISSEMBEL_INI_FNAME}
fi


#
# Rename dimensions
#
if [ ${DO_HORIZONTAL_INTEGER_AXIS} != 0 ] ; then
    rename_dim_axes ${CISSEMBEL_INI_FNAME}

    horizontal_int_axis ${CISSEMBEL_INI_FNAME} \
			${x_dimension_name} \
			${y_dimension_name}
fi

#
# Add surface slope
#
slope_fname=slope_${CISSEMBEL_INI_FNAME}
$SLOPE_SCRIPT --input ${TOPO_SURFACE_FNAME} \
	      --output ${slope_fname} \
	      --vname_surface ${VAR_NAME_TOPO} \
	      --vname_axis2 x --vname_axis1 y
#
# NOTE: If this python script reports a missmatch between in the
#       size/length of 'surface_slope' and the provided axis lengths,
#       you may exchange this axis names between 'vname_axis1' and
#       'vname_axis2'.
#
if [ ${DO_HORIZONTAL_INTEGER_AXIS} != 0 ] ; then
    rename_dim_axes ${slope_fname}
fi

${NCKS} -A -v surface_slope ${slope_fname} ${CISSEMBEL_INI_FNAME}


#
# Clean up meta data
#
creator=${USER}
if [ $creator == 'cr' ] ; then
    creator="Christian Rodehacke"
elif [ $creator == 'msm' ] ; then
    creator="Marianne S Madsen"
fi

ncatted -h \
	-a pism_intent,,d,, \
	-a command,global,d,, \
	-a CDI,global,d,, \
	-a CDO,global,d,, \
	-a NCO,global,d,, \
	-a Title,global,d,, \
	-a source,global,o,c,${PISM_FNAME} \
	-a Creator,global,o,c,"${creator}" \
	-a title,global,o,c,'Initial data for CISSEMBEL' \
	-a model_domain,global,o,c,"${DOMAIN_LABEL}" \
	-a history,global,o,c,"Created $(date)" \
	${CISSEMBEL_INI_FNAME}


#
# Clean up
#
for file in ${fname_levels_cdl} ${fname_height_cdl} \
	    ${fname_levels_cdl}.nc ${fname_height_cdl}.nc \
	    ${griddes_fname} ${basin4pism_fname} \
	    ${slope_fname} \
	    ${CISSEMBEL_INI_FNAME}.tmp xx.nc yy.nc ; do
    if [ -f $file ] ; then
	echo " rm $file" && rm $file
    fi
done

echo
echo " Created ${CISSEMBEL_INI_FNAME}"
echo

exit 0

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 27 11:10:19 CEST 2021


-- Test and plots for mod+physic.F08

@author: Christian Rodehacke (cr), DMI Copenhagen, Denmark

@copyright: Copyright (C) 2016-2025 Christian Rodehacke
"""
# -------------------------------------------------------------------
# This file is part of CISSEMBEL, which is the
# == Copenhagen Ice-Snow Surface Energy and Mass Balance modEL ==
#
# CISSEMBEL is free software; you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE  v.1.2
#
# Information about the EUROPEAN UNION PUBLIC LICENCE (*EUPL*) and
# translations into other European languages are available at
# https://eupl.eu (last access: 2021-10-01)](https://eupl.eu/)
#
# We hope that  this  distributed  CISSEMBEL  code, its  tools and
# documentations are usefull. Any improvements of the code, tools,
# and documentations tools are  very welcome.  Please, consider to
# share your thoughts and improvements with the community.
#
# The documentation,  tools,  and the code are provided as it are.
# NO WARRANTY  ABOUT FITNESS  AND USABILITY OF THE CODE, TOOLS, OR
# DOCUMENTATION  ARE  GIVEN.   NO  WARRANTY  IS  GIVEN  ABOUT  THE
# CORRECTNESS  OF COMPUTED RESULTS WITH THE  PROVIDED CODE, TOOLS,
# AND DOCUMENTATION. YOU USE THE CODE, DOCUMENTATION, AND TOOLS AT
# YOUR OWN RISK.           Removing  of  any  copyright  statement
# or  this  disclaimer  is considered  as a  attempt  to defraud.
#
# -------------------------------------------------------------------

# -----------------------------------------------------------
#
# Main program
#
if __name__ == '__main__':
    import ctypes as ct
    import numpy as np
    fortlib3 = ct.cdll.LoadLibrary('../src/CISSEMBEL_CBindings.so')

    print("Input")
    ilevelsX = 5
    ilevelsY = 2
    ilevX = ct.c_int(ilevelsX)
    ilevY = ct.c_int(ilevelsY)

    Temp1D = np.zeros((ilevelsX), order="F") + 272.
    Pres1D = np.zeros((ilevelsX), order="F") + 102400.
    Tmelt1D = np.zeros((ilevelsX), order="F") + 273.15
    Vaporpress1D = np.zeros((ilevelsX), order="F")

    for idx, ival in np.ndenumerate(Pres1D):
        if idx[0] == 0:
            continue
        Pres1D[idx] = Pres1D[idx] + (np.power(idx[0], 1.2)*9.81*650.)
        Temp1D[idx] = Temp1D[idx] - idx[0]/2.
        #array1D[idx[0]] = float(idx[0])
        print(idx[0],Pres1D[idx], Temp1D[idx])

    Temp1D_ptr = Temp1D.ctypes.data_as(ct.POINTER(ct.c_double))
    Pres1D_ptr = Pres1D.ctypes.data_as(ct.POINTER(ct.c_double))
    Tmelt1D_ptr = Tmelt1D.ctypes.data_as(ct.POINTER(ct.c_double))
    Vaporpress1D_ptr = Vaporpress1D.ctypes.data_as(ct.POINTER(ct.c_double))

    vaporpress = fortlib3.c_vaporpress
    fortlib3.c_vaporpress.argtypes = [ct.c_int, ct.POINTER(ct.c_double), \
                           ct.POINTER(ct.c_double), ct.POINTER(ct.c_double), \
                           ct.POINTER(ct.c_double)]

    rint = vaporpress(ilevX, Temp1D_ptr, Pres1D_ptr, \
                      Tmelt1D_ptr, Vaporpress1D_ptr)

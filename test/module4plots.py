#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep 10 16:56:09 2023


-- Test and plots for mod_physic.F08

@author: Christian Rodehacke, DMI Copenhagen, Denmark

@copyright: Copyright (C) 2016-2025 Christian Rodehacke
"""
# -------------------------------------------------------------------
# This file is part of CISSEMBEL, which is the
# == Copenhagen Ice-Snow Surface Energy and Mass Balance modEL ==
#
# CISSEMBEL is free software; you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE  v.1.2
#
# Information about the EUROPEAN UNION PUBLIC LICENCE (*EUPL*) and
# translations into other European languages are available at
# https://eupl.eu (last access: 2023-09-18)](https://eupl.eu/)
#
# We hope that  this  distributed  CISSEMBEL  code, its  tools and
# documentations are usefull. Any improvements of the code, tools,
# and documentations tools are  very welcome.  Please, consider to
# share your thoughts and improvements with the community.
#
# The documentation,  tools,  and the code are provided as it are.
# NO WARRANTY  ABOUT FITNESS  AND USABILITY OF THE CODE, TOOLS, OR
# DOCUMENTATION  ARE  GIVEN.   NO  WARRANTY  IS  GIVEN  ABOUT  THE
# CORRECTNESS  OF COMPUTED RESULTS WITH THE  PROVIDED CODE, TOOLS,
# AND DOCUMENTATION. YOU USE THE CODE, DOCUMENTATION, AND TOOLS AT
# YOUR OWN RISK.           Removing  of  any  copyright  statement
# or  this  disclaimer  is considered  as a  attempt  to defraud.
#
# -------------------------------------------------------------------

import argparse
import matplotlib.pyplot as plt
import os


# -------------------------------------------------------------------------
def parse_arguments():
    '''
    Parse the command line arguments

    Returns
    -------
    list
        List of strings indicating plot formats: ['png', 'pdf'].

    '''
    parser = argparse.ArgumentParser(
        description='Testing Fortran of CISSEMBEL via c-bindings',
        add_help=False)

    parser.add_argument('--skip_plots', '-s',
                        action='store_false',
                        help='Do not create plots (Default)')

    parser.add_argument('--plot_format', '-f', nargs='?', required=False,
                        default=['png', 'pdf'],
                        choices=['png', 'pdf', 'svg', 'svgz', 'eps', 'ps', 'jpg', 'tif'],
                        help='File format of image output')


    # If --skip_plots or -s is set, set --plot_format=None
    plot_formats = None
    if parser.parse_args().skip_plots:
        plot_formats = parser.parse_args().plot_format

    # # Report used command line arguments
    # for single_arg in vars(parser.parse_args()): #sorted(vars(args)) :
    #     print('  - %-21s =  '% (single_arg)+str(getattr(parser.parse_args(), single_arg)) )

    #If you want to convey all switches outside, use:  return parser.parse_args()
    return plot_formats

# -------------------------------------------------------------------------
def plotting(fig_id, plot_name, suffixes='png', subdirectory='./Figure',
             flag_close=True):
    '''
    Generate plot

    Parameters
    ----------
    fig_id : matplotlib.figure.Figure
        Figure handle.
    plot_name : str
        Prefix of the plot's filename.
    suffixes : str, optional
        Filename suffix defining also the figure file format. The default is 'png'.
    subdirectory : str, optional
        Name of the subdirectory holding the figures. The default is './Figure'.
    flag_close : bool, optional
        Close figure after print, which save memory. The default is True.

    Returns
    -------
    None.

    '''
    print('    Create plot "'+plot_name+'"')

    if not isinstance(suffixes, list):
        suffixes = [suffixes]
    for suffix in suffixes :
        subdir = subdirectory + '/' + suffix.upper()
        if not os.path.exists(subdir) :
            print('      + Create directory '+subdir)
            os.makedirs(subdir)
        else :
            print('      o Reuse directory '+subdir)

        filename = subdir+'/'+plot_name+'.'+suffix
        print('      - Print '+filename)
        fig_id.savefig(filename, orientation='landscape', \
                       transparent='false', facecolor='white') #papertype='a4', \
    if flag_close:
        print('      => Close figure')
        plt.close(fig_id)

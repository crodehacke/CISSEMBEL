#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun 10 Sep 2023 05:30:54 PM CEST

Auxillary functions for C-binding to Fortran functions and subroutines as part
of the Copenhagen Ice Snow Surface Energy and Mass Balance modEL (CISSEMBEL)

@author: Christian Rodehacke, DMI Copenhagen, Denmark

@copyright: Copyright (C) 2016-2025 Christian Rodehacke
"""
# -------------------------------------------------------------------
# This file is part of CISSEMBEL, which is the
# == Copenhagen Ice-Snow Surface Energy and Mass Balance modEL ==
#
# CISSEMBEL is free software; you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE  v.1.2
#
# Information about the EUROPEAN UNION PUBLIC LICENCE (*EUPL*) and
# translations into other European languages are available at
# https://eupl.eu (last access: 2023-09-18)](https://eupl.eu/)
#
# We hope that  this  distributed  CISSEMBEL  code, its  tools and
# documentations are usefull. Any improvements of the code, tools,
# and documentations tools are  very welcome.  Please, consider to
# share your thoughts and improvements with the community.
#
# The documentation,  tools,  and the code are provided as it are.
# NO WARRANTY  ABOUT FITNESS  AND USABILITY OF THE CODE, TOOLS, OR
# DOCUMENTATION  ARE  GIVEN.   NO  WARRANTY  IS  GIVEN  ABOUT  THE
# CORRECTNESS  OF COMPUTED RESULTS WITH THE  PROVIDED CODE, TOOLS,
# AND DOCUMENTATION. YOU USE THE CODE, DOCUMENTATION, AND TOOLS AT
# YOUR OWN RISK.           Removing  of  any  copyright  statement
# or  this  disclaimer  is considered  as a  attempt  to defraud.
#
# -------------------------------------------------------------------

def compare_field2shapes(shape_info0, shape_info1, varn0='variable_1st',
                        varn1='variable_2nd'):
    """
    Compares the shape of fields

    Parameters
    ----------
    shape_info0 : tuple
        Shape information of first variable, e.g., var1.shape.
    shape_info1 : tuple
        Shape information of second variable, e.g., var2.shape.
    varn0 : str, optional
        Variable name of the first variable. The default is 'variable_1st'.
    varn1 : str, optional
        Variable of the second variable. The default is 'variable_2nd'.

    Returns
    -------
    None.

    """
    if shape_info0 != shape_info1:
        print('Shape of "'+varn0+'"['+str(shape_info0)+'] differs from'+
              'shape of "'+varn1+'"['+str(shape_info1)+']')
    # else:
    #     print('OK! Shape ['+str(shape_info0)+'] of "'+varn0+'" and "'+varn1+'"')


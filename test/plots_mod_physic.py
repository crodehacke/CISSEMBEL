#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 27 11:10:19 CEST 2021


-- Test and plots for mod_physic.F08

@author: Christian Rodehacke, DMI Copenhagen, Denmark

@copyright: Copyright (C) 2016-2025 Christian Rodehacke
"""
# -------------------------------------------------------------------
# This file is part of CISSEMBEL, which is the
# == Copenhagen Ice-Snow Surface Energy and Mass Balance modEL ==
#
# CISSEMBEL is free software; you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE  v.1.2
#
# Information about the EUROPEAN UNION PUBLIC LICENCE (*EUPL*) and
# translations into other European languages are available at
# https://eupl.eu (last access: 2023-09-18)](https://eupl.eu/)
#
# We hope that  this  distributed  CISSEMBEL  code, its  tools and
# documentations are usefull. Any improvements of the code, tools,
# and documentations tools are  very welcome.  Please, consider to
# share your thoughts and improvements with the community.
#
# The documentation,  tools,  and the code are provided as it are.
# NO WARRANTY  ABOUT FITNESS  AND USABILITY OF THE CODE, TOOLS, OR
# DOCUMENTATION  ARE  GIVEN.   NO  WARRANTY  IS  GIVEN  ABOUT  THE
# CORRECTNESS  OF COMPUTED RESULTS WITH THE  PROVIDED CODE, TOOLS,
# AND DOCUMENTATION. YOU USE THE CODE, DOCUMENTATION, AND TOOLS AT
# YOUR OWN RISK.           Removing  of  any  copyright  statement
# or  this  disclaimer  is considered  as a  attempt  to defraud.
#
# -------------------------------------------------------------------


# -------------------------------------------------------------------------
def plot_relhumid2dewpoint(library_name, plot_suffix=None,
                           plotname='relhumid2dewpoint'):
    '''
    Plot results of the via c-binding call Fortran function relhumid2dewpoint

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'relhumid2dewpoint'.

    Returns
    -------
    None.

    '''
    print('* Dew-point temperature from relative humidity')
    rel_humid = np.linspace(45, 110, 99)
    air_temp = np.linspace(-75., 30, 106)+FREEZING_TEMP

    dewpt_temp = np.zeros([rel_humid.size,
                           air_temp.size])
    dewpt_temp_ano = np.zeros_like(dewpt_temp)

    for irh, rhumid0 in enumerate(rel_humid):
        rhumid_array = np.zeros_like(air_temp)+rhumid0
        dewpt_temp[irh, :] = \
            mod_physic.relhumid2dewpoint(rhumid_array,
                                         air_temp,
                                         library_name)
        dewpt_temp_ano[irh, :] = air_temp-dewpt_temp[irh, :]

    #
    # Figure
    #
    fig = plt.figure(dpi=300, facecolor='white')
    ax0 = fig.subplots(1, 2, sharey=True)
    fig.subplots_adjust(wspace=0.14)

    #
    # Dew-point temperature
    #
    iap = 0
    levels_dewtemp = np.linspace(170, 300, 14)
    csf0 = ax0[iap].contourf(air_temp, rel_humid, dewpt_temp,
                             levels=levels_dewtemp,
                             cmap='coolwarm',
                             extend='both')

    ax0[iap].plot([FREEZING_TEMP, FREEZING_TEMP],
                  [rel_humid.min(), rel_humid.max()], '--',
                  color='gray', label='$T_{{\\rm freeze}}$')
    ax0[iap].plot([air_temp.min(), air_temp.max()], [100., 100.], ':',
                  color='dimgray', label='$RH = 100 \%$')

    ax0[iap].set_xlabel('air temperature, $T_{\mathrm{air}}$ ($K$)')
    ax0[iap].set_ylabel('relative humidity, $RH$ ($\%$)')
    ax0[iap].legend(loc='lower left')

    fig.colorbar(csf0, ax=ax0[iap],
                 label='dew-point temperature, $T_{\mathrm{dew}}$ ($K$)',
                 location='top')

    #
    # Difference air temperature minus dew-point temperature
    #
    iap = 1
    levels_difftemp = np.linspace(-5, 35, 9)
    csf1 = ax0[iap].contourf(air_temp, rel_humid, dewpt_temp_ano,
                            levels=levels_difftemp,
                            cmap='Blues_r',
                            extend='max')
    ax0[iap].contour(air_temp, rel_humid, dewpt_temp_ano,
                     levels=[0], colors='white')
    ax0[iap].plot([FREEZING_TEMP, FREEZING_TEMP],
                  [rel_humid.min(), rel_humid.max()], '--',
                  color='gray', label='$T_{{\\rm freeze}}$')
    ax0[iap].plot([air_temp.min(), air_temp.max()], [100., 100.], ':',
                  color='dimgray', label='$RH = 100 \%$')

    ax0[iap].set_xlabel('air temperature, $T_{\mathrm{air}}$ ($K$)')

    fig.colorbar(csf1, ax=ax0[iap],
                 label=r'$T_{\mathrm{air}} - T_{\mathrm{dew}} (K)$',
                 location='top')


    if plot_suffix:
        module4plots.plotting(fig, plotname, plot_suffix)

# -------------------------------------------------------------------------
def plot_calc_cp_ice(library_name, plot_suffix=None,
                          plotname='calc_cp_ice'):
    '''
    Plot results of the via c-binding call Fortran function calc_cp_ice

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'calc_cp_ice'.

    Returns
    -------
    None.

    '''
    print('* Specific heat capacity of ice')
    temperature = np.linspace(82, 282, 109, dtype=float)
    calc_cp_ice = mod_physic.calc_cp_ice(temperature, library_name)
    calc_cp_ice_restricted = \
        mod_physic.calc_cp_ice(np.minimum(temperature, 273.15), library_name)
    step = 6
    #
    # Figure
    #
    fig = plt.figure(dpi=300, facecolor='white')
    ax0 = fig.subplots()

    ax0.plot(temperature, calc_cp_ice, linewidth=2.5,
             linestyle='--', color='orangered',
             label='unrestricted temperatures range')
    ax0.plot(temperature, calc_cp_ice_restricted, linewidth=2.5,
             linestyle='-', color='mediumblue',
             label='restricted to valid temperature')

    if step > 0:
        ax0.plot(temperature[::step], calc_cp_ice_restricted[::step], 'o',
                  color='midnightblue', alpha=0.75)
    ax0.set_xlabel('temperature (Kelvin)')
    ax0.set_ylabel('specific heat heat capacity of ice ($J\, kg^{{-1}}\, K^{{-1}}$)')
    ax0.legend()


    ax0.axvspan(FREEZING_TEMP, temperature.max(), color='crimson', alpha=0.15)
    ax0.axvspan(temperature.min(), FREEZING_TEMP, color='cyan', alpha=0.15)

    ax0.set_xlim([temperature.min(), temperature.max()])

    if plot_suffix:
        module4plots.plotting(fig, plotname, plot_suffix)

# -------------------------------------------------------------------------
def plot_thermal_cond_air(library_name, plot_suffix=None,
                          plotname='thermal_cond_air'):
    '''
    Plot results of the via c-binding call Fortran function thermal_cond_air

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'thermal_cond_air'.

    Returns
    -------
    None.

    '''
    print('* Thermal conductivity of air plot')
    air_temp = np.linspace(170, 280, 101, dtype=float)
    thermal_cond_air = mod_physic.thermal_cond_air(air_temp, library_name)
    step = 5

    #
    # Figure
    #
    fig = plt.figure(dpi=300, facecolor='white')
    ax0 = fig.subplots()

    ax0.plot(air_temp, thermal_cond_air, linewidth=2,
             linestyle='-', color='goldenrod')
    if step > 0:
        ax0.plot(air_temp[::step], thermal_cond_air[::step], 'o',
                 color='darkgoldenrod', alpha=0.75)
    ax0.set_xlabel('Air temperature (Kelvin)')
    ax0.set_ylabel('Air thermal conductivity (W m$^{-1}$ K$^{-1}$)')

    ax0.axvspan(FREEZING_TEMP, air_temp.max(), color='crimson', alpha=0.15)
    ax0.axvspan(air_temp.min(), FREEZING_TEMP, color='cyan', alpha=0.15)

    ax0.set_xlim([air_temp.min(), air_temp.max()])

    if plot_suffix:
        module4plots.plotting(fig, plotname, plot_suffix)

# -------------------------------------------------------------------------
def plot_thermal_cond_snow(library_name, plot_suffix=None,
                          plotname='thermal_cond_snow'):
    '''
    Plot results of the via c-binding call Fortran function thermal_cond_snow

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'thermal_cond_snow'.

    Returns
    -------
    None.

    '''
    print('* Thermal conductivity of snow plot')
    snow_temp = np.linspace(170, 280, 101, dtype=float)
    snow_densities = np.array([300, 500, 700, 800, 900])

    thermal_cond_snow = np.zeros([snow_temp.size, snow_densities.size])

    for idx, sdens0 in enumerate(snow_densities):
        sdens = np.zeros_like(snow_temp)+sdens0
        thermal_cond_snow[:, idx] = mod_physic.thermal_cond_snow(snow_temp,
                                                                 sdens,
                                                                 library_name)
    #
    # Figure
    #
    fig = plt.figure(dpi=300, facecolor='white')
    ax0 = fig.subplots()

    lineid = ax0.plot(snow_temp, thermal_cond_snow, linewidth=2,
                      linestyle='-')

    lineid[-1].set_linestyle(':')
    lineid[-2].set_linestyle('--')

    ax0.legend(snow_densities, title='Snow density\n($kg\; m^{-3}$)')
    ax0.set_xlabel('Snow temperature (Kelvin)')
    ax0.set_ylabel('Snow thermal conductivity (W m$^{-1}$ K$^{-1}$)')

    ax0.axvspan(FREEZING_TEMP, snow_temp.max(), color='crimson', alpha=0.15)
    ax0.axvspan(snow_temp.min(), FREEZING_TEMP, color='cyan', alpha=0.15)

    ax0.set_xlim([snow_temp.min(), snow_temp.max()])

    if plot_suffix:
        module4plots.plotting(fig, plotname, plot_suffix)

# -------------------------------------------------------------------------
def plot_thermal_cond_ice(library_name, plot_suffix=None,
                          plotname='thermal_cond_ice'):
    '''
    Plot results of the via c-binding call Fortran function thermal_cond_ice

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'thermal_cond_ice'.

    Returns
    -------
    None.

    '''
    print('* Thermal conductivity of ice plot')
    ice_temp = np.linspace(170, 280, 101, dtype=float)
    thermal_cond_ice = mod_physic.thermal_cond_ice(ice_temp, library_name)
    step = 5

    #
    # Figure
    #
    fig = plt.figure(dpi=300, facecolor='white')
    ax0 = fig.subplots()

    ax0.plot(ice_temp, thermal_cond_ice, linewidth=2, linestyle='-')
    if step > 0:
        ax0.plot(ice_temp[::step], thermal_cond_ice[::step], 'o',
                 color='darkblue', alpha=0.75)
    ax0.set_xlabel('Ice temperature (Kelvin)')
    ax0.set_ylabel('Ice thermal conductivity (W m$^{-1}$ K$^{-1}$)')

    ax0.axvspan(FREEZING_TEMP, ice_temp.max(), color='crimson', alpha=0.25)
    ax0.axvspan(ice_temp.min(), FREEZING_TEMP, color='cyan', alpha=0.15)

    ax0.set_xlim([ice_temp.min(), ice_temp.max()])

    if plot_suffix:
        module4plots.plotting(fig, plotname, plot_suffix)

# -------------------------------------------------------------------------
def plot_thermal_cond_snowfirn(library_name, plot_suffix=None,
                               plotname='thermal_cond_snowfirn'):
    '''
    Plot results of the via c-binding call Fortran function thermal_cond_snowfirn

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'thermal_cond_snowfirn'.

    Returns
    -------
    None.

    '''
    print('* Thermal conductivity of snow-firn plot')
    firn_temp = np.array([-80, -60, -40, -20, 0])+FREEZING_TEMP
    firn_densities = np.linspace(50, 925, 176, dtype=float)
    firn_closure = 830. # pore closure density

    thermal_cond_firn = np.zeros([firn_temp.size, firn_densities.size])
    for idx, ftemp0 in enumerate(firn_temp):
        ftemp = np.zeros_like(firn_densities)+ftemp0
        thermal_cond_firn[idx, :] = mod_physic.thermal_cond_snowfirn(ftemp,
                                                                     firn_densities,
                                                                     library_name)
    #
    # Figure
    #
    fig = plt.figure(dpi=300, facecolor='white')
    ax0 = fig.subplots()

    for idx, ftemp0 in enumerate(firn_temp):
        label = '{:3.0f}'.format(ftemp0-FREEZING_TEMP)
        linewidth=2
        if idx is firn_temp.size-1:
            linewidth=3
        ax0.plot(firn_densities, thermal_cond_firn[idx, :],
                 linewidth=linewidth, linestyle='-', label=label)
    ax0.legend(loc='upper left', title='Temperature ($^{\circ}C$)')
    ax0.set_xlabel('Snow/firn/ice density (kg m$^{-3}$)')
    ax0.set_ylabel('Snow/firn thermal conductivity (W m$^{-1}$ K$^{-1}$)')

    ax0.axvspan(firn_closure, firn_densities.max(), color='gainsboro', alpha=0.7)
    ax0.text(firn_closure, 0, 'Pore closure', rotation=90)

    ax0.set_xlim([firn_densities.min(), firn_densities.max()])

    if plot_suffix:
        module4plots.plotting(fig, plotname, plot_suffix)

# -------------------------------------------------------------------------
def plot_thermal_cond_water(library_name, plot_suffix=None,
                            plotname='thermal_cond_water'):
    '''
    Plot results of the via c-binding call Fortran function thermal_cond_water

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'thermal_cond_water'.

    Returns
    -------
    None.

    '''
    print('* Thermal conductivity of water plot')
    water_temp = np.linspace(-1.5, 2.5, 11)+FREEZING_TEMP
    water_salt = np.array([0, 1, 2, 5, 10])
    water_pressure = np.array([1, 100])*PATM

    thermal_cond_water = np.zeros([water_temp.size,
                                   water_salt.size,
                                   water_pressure.size])

    for idp, wpres0 in enumerate(water_pressure):
        for ids, wsalt0 in enumerate(water_salt):
            wpres = np.zeros_like(water_temp)+wpres0
            wsalt = np.zeros_like(water_temp)+wsalt0
            thermal_cond_water[:, ids, idp] = \
                mod_physic.thermal_cond_water(water_temp, wsalt, wpres,
                                              library_name)

    #
    # Figure
    #
    fig = plt.figure(dpi=300, facecolor='white')
    ax0 = fig.subplots(1, 2)
    fig.subplots_adjust(wspace=0.14)

    for idp, wpres0 in enumerate(water_pressure):
        for ids, wsalt0 in enumerate(water_salt):
            # Pressure -> linestyle
            if idp == 0:
                linestyle = '-'
            elif idp == 1:
                linestyle = '--'
            elif idp == 2:
                linestyle = ':'
            # Salt -> color
            linewidth = 2
            if ids == 0:
                linewidth = 3
                color = 'k'
            elif ids == 1:
                color = 'b'
            elif ids == 2:
                color = 'g'
            elif ids == 3:
                color = 'orange'
            elif ids == 4:
                color = 'r'
            else:
                color = 'k'

            label = '{:-3d}'.format(wsalt0)
            ax0[idp].plot(water_temp, thermal_cond_water[:, ids, idp],
                          color=color, linewidth=linewidth,
                          linestyle=linestyle, label=label)

            if idp == 1:
                ax0[idp].legend(loc='upper left',
                                title='Salt\n($g\; kg^{-1}$)')
            if idp == 1:
                ax0[idp].yaxis.tick_right()
                ax0[idp].set_ylabel('Water thermal conductivity (W m$^{-1}$ K$^{-1}$)')

            ax0[idp].set_xlabel('Water temperature (K)')

            ax0[idp].set_title('$p_{{water}}$ = {:1.1f} $\cdot\; p_{{atm}}$'.format(wpres0/PATM))

            ax0[idp].axvspan(FREEZING_TEMP, water_temp.max(),
                             color='crimson', alpha=0.05)
            ax0[idp].axvspan(water_temp.min(), FREEZING_TEMP,
                             color='cyan', alpha=0.05)

            ax0[idp].set_xlim([water_temp.min(), water_temp.max()])

    if plot_suffix:
        module4plots.plotting(fig, plotname, plot_suffix)


    # #
    # # Special: Not used anymore
    # #
    # fig2 = plt.figure(dpi=300, facecolor='white')
    # ax0 = fig2.subplots(1)
    # ids = 0
    # idp = 0
    # ax0.plot(water_temp, thermal_cond_water[:, ids, idp],
    #          color='k', linewidth=3, linestyle='--', label='valid')
    # flag_valid = water_temp<=FREEZING_TEMP
    # ax0.plot(water_temp[flag_valid], thermal_cond_water[flag_valid, ids, idp],
    #          color='crimson', linewidth=3, linestyle ='-', label='not valid')
    # ax0.set_xlabel('Water temperature, $T_w$ (K)')
    # ax0.set_ylabel('Water thermal conductivity (W m$^{-1}$ K$^{-1}$)')
    # ax0.set_title(r'Freshwater thermal conductivity: $\sum_{i=1}^4 a_i\, \left( \frac{T_w}{300} \right)^{b_i}$')
    # ax0.axvspan(FREEZING_TEMP, water_temp.max(), color='springgreen', alpha=0.25)
    # ax0.axvspan(water_temp.min(), FREEZING_TEMP, color='gold', alpha=0.66)
    # ax0.set_xlim([water_temp.min(), water_temp.max()])
    # ax0.legend(loc='upper right', title='Temperature range')
    # if plot_suffix:
    #     module4plots.plotting(fig2, plotname+'_FreshWater', plot_suffix)


# -------------------------------------------------------------------------
def plot_fresh_snow_dens_ant(library_name, plot_suffix=None,
                             plotname='fresh_snow_dens_ant'):
    '''
    Plot results of the via c-binding call Fortran function fresh_snow_dens_ant

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'fresh_snow_dens_ant'.

    Returns
    -------
    None.

    '''
    print('* Various freshly snow density in Antarctica')

    air_temp = np.linspace(-40, 5, num=91) + 273.15 # Kelvin
    wind_speed = np.linspace(00, 40, num=81) # m s-1 (0-144 km hours-1)

    density = np.zeros((air_temp.size, wind_speed.size))

    for iwind, wind in enumerate(wind_speed):
        wind_array = np.zeros_like(air_temp)+wind
        density[:, iwind] = mod_physic.fresh_snow_dens_ant(air_temp,
                                                           wind_array,
                                                           library_name)

    #
    # Figure
    #
    contour_levels = np.arange(250,525,25)
    fig = plt.figure(dpi=300, facecolor='white')
    axes0 = fig.subplots()
    csf = axes0.contourf(wind_speed, air_temp, density,
                         levels=contour_levels,
                         cmap='GnBu') #, extend='both')

    axes0.plot(wind_speed, wind_speed*0+FREEZING_TEMP,
               '--r', linewidth=2, label='Freezing point temperature')

    axes0.set_ylabel('air temperature ($K$)')
    axes0.set_xlabel('wind speed ($m \; s^{-1}$)')
    axes0.set_title('Antarctica')
    axes0.legend(loc='lower right')

    fig.colorbar(csf, label='freshly fallen snow density ($kg \; m^{-3}$)')

    if plot_suffix:
        module4plots.plotting(fig, plotname, plot_suffix)

# -------------------------------------------------------------------------
def plot_fresh_snow_dens_midlat(library_name, plot_suffix=None,
                                plotname='fresh_snow_dens_midlat'):
    '''
    Plot results of the via c-binding call Fortran function fresh_snow_dens_midlat

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'fresh_snow_dens_midlat'.

    Returns
    -------
    None.

    '''
    print('* Various freshly snow density in the boreal Canadian Arctic')

    air_temp = np.linspace(-40, 5, num=91) + 273.15 # Kelvin
    density = mod_physic.fresh_snow_dens_midlat(air_temp, library_name)

    #
    # Figure
    #
    fig = plt.figure(dpi=300, facecolor='white')
    axes0 = fig.subplots()

    axes0.plot(air_temp, density, linewidth=3)


    axes0.axvspan(FREEZING_TEMP, air_temp.max(), color='crimson', alpha=0.25)
    axes0.axvspan(air_temp.min(), FREEZING_TEMP, color='cyan', alpha=0.15)

    axes0.set_xlabel('air temperature ($K$)')
    axes0.set_ylabel('freshly fallen snow density ($kg \; m^{-3}$)')
    axes0.set_title('Boreal Canadian Arctic')

    axes0.set_xlim([air_temp.min(), air_temp.max()])

    if plot_suffix:
        module4plots.plotting(fig, plotname, plot_suffix)


##########################
# -------------------------------------------------------------------------
def plot_active_layer_depth(library_name, plot_suffix=None,
                            plotname='active_layer_depth'):
    '''
    Plot results of the via c-binding call Fortran function active_layer_depth

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'active_layer_depth'.

    Returns
    -------
    None.

    '''
    print('* Thermal active layer depth')

    delta_time = np.linspace(1, 24, num=24)*3600.0 # Seconds
    spec_heat_capacity = np.linspace(2000, 4000, num=5) # J K-1 kg-1

    snow_temperature = FREEZING_TEMP-5.0  # Kelvin
    density = np.arange(300, 800+100, 100) # kg m-3
    snow_temp_array = np.zeros_like(density)+snow_temperature
    conductivity = mod_physic.thermal_cond_snow(snow_temp_array,
                                                density,
                                                library_name)


    layer_depth = np.zeros((spec_heat_capacity.size,
                            density.size,
                            delta_time.size))

    for ish_cap, sh_cap in enumerate(spec_heat_capacity):
        for idens, dens in enumerate(density):
            spec_heat_cap_array = np.zeros_like(delta_time)+sh_cap
            density_array = np.zeros_like(delta_time)+dens
            conduct_array = np.zeros_like(delta_time)+conductivity[idens]
            layer_depth[ish_cap, idens, :] = \
                mod_physic.active_layer_depth(delta_time,
                                              conduct_array,
                                              density_array,
                                              spec_heat_cap_array,
                                              library_name)

    #
    # Figure
    #
    delta_time_hour = delta_time/3600
    contour_levels = np.linspace(0.0, 0.4, num=9) #11 )
    contour_levels2 = [0.1, 0.2, 0.3]
    fig = plt.figure(dpi=300, facecolor='white')
    axes = fig.subplots(3, 1, sharex=True)

    iax = 0
    idat = 0
    csf = axes[iax].contourf(delta_time_hour, density, layer_depth[idat, ::],
                           levels=contour_levels,
                           cmap='GnBu') #, extend='both')
    csf2 = axes[iax].contour(delta_time_hour, density, layer_depth[idat, ::],
                             levels=contour_levels2, cmap='ocean_r')
    axes[iax].annotate('$cp$ = {:.0f} $J\, K^{{-1}}\, kg^{{-1}}$'.format(spec_heat_capacity[idat]),
              (0.05, 0.05), xycoords='axes fraction', ha='left', va='bottom')
    if True:
        axes[iax].set_title('$cp$ via thermal_cond_snow '+
                            'for $T$ = {:.2f} K'.format(snow_temperature))
    else:
        axes[iax].set_title('active layer depth \n$cp$ via thermal_cond_snow '+
                            'for $T$ = {:.2f} K'.format(snow_temperature))

    iax = iax+1
    idat = 2
    csf = axes[iax].contourf(delta_time_hour, density, layer_depth[idat, ::],
                           levels=contour_levels,
                           cmap='GnBu') #, extend='both')
    csf2 = axes[iax].contour(delta_time_hour, density, layer_depth[idat, ::],
                             levels=contour_levels2, cmap='ocean_r')
    axes[iax].set_ylabel('density ($kg\; m^{-3}$)')
    axes[iax].annotate('$cp$ = {:.0f} $J\, K^{{-1}}\, kg^{{-1}}$'.format(spec_heat_capacity[idat]),
              (0.05, 0.05), xycoords='axes fraction', ha='left', va='bottom')

    iax = iax+1
    idat = 4
    csf = axes[iax].contourf(delta_time_hour, density, layer_depth[idat, ::],
                           levels=contour_levels,
                           cmap='GnBu') #, extend='both')
    csf2 = axes[iax].contour(delta_time_hour, density, layer_depth[idat, ::],
                             levels=contour_levels2, cmap='ocean_r')
    axes[iax].annotate('$cp$ = {:.0f} $J\, K^{{-1}}\, kg^{{-1}}$'.format(spec_heat_capacity[idat]),
              (0.05, 0.05), xycoords='axes fraction', ha='left', va='bottom')

    axes[iax].set_xlabel('time ($day$)')

    cbar = fig.colorbar(csf, ax=axes, label='active layer depth ($m$)')
    cbar.add_lines(csf2)


    if plot_suffix:
        module4plots.plotting(fig, plotname, plot_suffix)


# -------------------------------------------------------------------------
def plot_densprofile(library_name, plot_suffix=None, plotname='densprofile'):
    '''
    Plot results of the via c-binding call Fortran function densprofile

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'densprofile'.

    Returns
    -------
    None.

    '''
    print('* Various density profiles in a plot')
    depth = np.linspace(0, 200, 201, dtype=float)
    cdens_factors = [0.5, 0.75, 1, 1.25, 2.]
    # Preallocate of results
    densityprofile_multi = np.zeros((len(cdens_factors), len(depth)))

    # Loop of calculations for different "cdensity"
    for icount, factor in enumerate(cdens_factors):
        cdensity = CDENSITY * factor
        densityprofile_multi[icount, :] = \
            mod_physic.densprofile(depth, RHO_SNOW, RHO_ICE, cdensity,
                                   library_name)

    #
    # Plot
    #
    depth_min = 0
    depth_split = 25
    depth_max = max(depth)

    fig = plt.figure(dpi=300, facecolor='white')
    axu, axl = fig.subplots(2,1, sharex=True)
    fig.subplots_adjust(hspace=0.0005)
    for ax0 in [axu, axl]:
        for icount, factor in enumerate(cdens_factors):
            linewidth = 1.5
            linestyle = '--'
            if factor == 1:
                # Different line style for reference case: factor == 1
                linewidth = 3
                linestyle = '-'

            cdensity = CDENSITY * factor
            label = r'{:.4g}'.format(cdensity)

            # Plot
            ax0.plot(densityprofile_multi[icount, :], depth,
                     label=label, linewidth=linewidth, linestyle=linestyle)

        # Reference densities (upper, lower boundaries)
        ax0.plot([RHO_SNOW, RHO_SNOW],[depth[0], depth[-1]], '--', c='gray')
        ax0.plot([RHO_ICE, RHO_ICE],[depth[0], depth[-1]], '--', c='gray')

        #
        # Handling of the splitting between higher resolved upper column
        # and coarser resolved lower column
        #
        if ax0 == axu:
            #ax0.text(RHO_SNOW, (depth[-1]-depth[0]) * 0.1,
            ax0.text(RHO_SNOW, (depth_split-depth_min)*0.75,
                     r'  $\rho_{snow}$', ha='left', c='dimgray')
            ax0.text(RHO_ICE, (depth_split-depth_min)*0.75,
                     r'$\rho_{ice}$  ', ha='right', c='dimgray')

            ax0.set_ylim([depth_min, depth_split])
            ax0.set_ylabel('Depth (m)', horizontalalignment='right')
            ax0.set_title('Density profiles (cold start)')
        elif ax0 == axl:
            ax0.set_ylim([depth_split+0.001, depth_max])
            ax0.set_xlabel('Density (kg m-3)')
            ax0.legend(loc='lower left', title=r'C$_\rho$ ($m^{-1}$)')
        ax0.invert_yaxis()

    if plot_suffix:
        module4plots.plotting(fig, plotname, plot_suffix)

# -------------------------------------------------------------------------
def plot_vaporpress(library_name, plot_suffix=None, plotname='vaporpress'):
    '''
    Plot results of the via c-binding call Fortran function vaporpress

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'vaporpress'.

    Returns
    -------
    None.

    '''
    print('* Vapor pressure')
    tmelt0 = FREEZING_TEMP
    temperature = np.linspace(-30, 10, 163, dtype=float) +tmelt0
    pressure = np.linspace(650e2, 1050e2, 101, dtype=float)
    vaporpress_multi = np.zeros((len(temperature), len(pressure))) # Preallocate

    # Loop of calculations for different "cdensity"
    for itemp, temp in enumerate(temperature):
        tarray = np.ones(len(pressure), dtype='float', order='F') * temp
        vaporpress_multi[itemp, :] = mod_physic.vaporpress(tarray, pressure,
                                                           tmelt0, library_name)
    pressure_factor = 0.01

    #
    # Figure 1
    #
    print('  - Vapor pressure as contour plot: air pressure and temperature')
    fig0 = plt.figure(dpi=300, facecolor='white')
    ax0 = fig0.subplots()

    csf = ax0.contourf(pressure*pressure_factor, temperature, vaporpress_multi,
                       levels=[0, 100, 200, 500, 750, 1000, 1250, 1500, 1750],
                       extend='max', cmap='YlGnBu')
    if pressure_factor == 1:
        ax0.set_xlabel('Air pressure (Pa)')
    elif pressure_factor == 0.01:
        ax0.set_xlabel('Air pressure (hPa)')
    elif pressure_factor == 0.001:
        ax0.set_xlabel('Air pressure (kPa)')
    elif pressure_factor == 0.000001:
        ax0.set_xlabel('Air pressure (MPa)')
    else:
        ax0.set_xlabel('Air pressure ({:g}'.format(pressure_factor)+' Pa)')
    ax0.set_ylabel('Air temperature (K)')

    ax0.plot([pressure[0]*pressure_factor, pressure[-1]*pressure_factor],
            [tmelt0, tmelt0],'--', color='red')
    ax0.text((pressure[0]+(pressure[-1]-pressure[0])*0.25 )*pressure_factor,
            tmelt0+0.01*(temperature[-1]-temperature[0]),
            r'$T_{freeze}$', va='bottom', color='darkred')

    plt.colorbar(csf, label='Water vapor pressure (Pa)')

    if plot_suffix:
        module4plots.plotting(fig0, plotname+'_2D', plot_suffix)

    #
    # Figure 2
    #
    print('  - Vapor pressure as line plots: x-axis: air pressure or temperature')
    fig2 = plt.figure(dpi=300, facecolor='white')
    ax0, ax1 = fig2.subplots(1,2, sharey=True)
    fig2.subplots_adjust(wspace=0.26)

    #
    # Left : x-axis = air temperature, y-axis = water vapor
    #
    step = 50
    ax0.plot([tmelt0, tmelt0], [0, np.max(vaporpress_multi)],'--', c='cyan')
    line0 = ax0.plot(temperature, vaporpress_multi[:,::step], linewidth=2)
    ax0.text(tmelt0, 10., r'$T_{freeze}$', color='darkcyan')
    ax0.set_xlabel('Air temperature (K)')
    ax0.set_ylabel('Water vapor pressure (Pa)')
    ax0.yaxis.tick_right()

    label = [] #[r'$T_{frez}$']
    for prs in pressure[::step]:
        label.append('{:5.1f}'.format(prs*pressure_factor))
    ax0.legend(line0, label, title=r'p$_{air}$ (hPa)')

    #
    # Right : x-axis = air pressure, y-axis = water vapor
    #
    step = 18
    line1 = ax1.plot(pressure*pressure_factor, vaporpress_multi[::step,:].T,
                     linewidth=2)
    # Dashed lines for temperature above freezing
    for temp, lin in zip(temperature[::step], line1):
        if temp > tmelt0:
            plt.setp(lin, linestyle='--')
    if pressure_factor == 1:
        ax1.set_xlabel('Air pressure (Pa)')
    elif pressure_factor == 0.01:
        ax1.set_xlabel('Air pressure (hPa)')
    elif pressure_factor == 0.001:
        ax1.set_xlabel('Air pressure (kPa)')
    elif pressure_factor == 0.000001:
        ax1.set_xlabel('Air pressure (MPa)')
    else:
        ax1.set_xlabel('Air pressure ({:g}'.format(pressure_factor)+' Pa)')
    ax1.set_ylabel('Water vapor pressure (Pa)')
    ax1.yaxis.set_label_position("right")

    label = []
    for temp in temperature[::step]:
        label.append('{:6.2f}'.format(temp))
    ax1.legend(line1, label, title=r'T$_{air}$ (K)', loc='lower left')

    if plot_suffix:
        module4plots.plotting(fig2, plotname+'_lines', plot_suffix)

# -------------------------------------------------------------------------
def plot_rhoair(library_name, plot_suffix=None, plotname='rhoair'):
    '''
    Plot results of the via c-binding call Fortran function rhoair

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'rhoair'.

    Returns
    -------
    None.

    '''
    print('* Air density plot')
    pres = np.linspace(650e2, 1050e2, 106, dtype=float)
    rhoair = mod_physic.rhoair(pres, library_name)
    pressure_factor = 0.01
    step = -5

    #
    # Figure
    #
    fig = plt.figure(dpi=300, facecolor='white')
    ax0 = fig.subplots()

    ax0.plot(pres*pressure_factor, rhoair, linewidth=2, linestyle='-')
    if step > 0:
        ax0.plot(pres[::step]*pressure_factor, rhoair[::step], 'o',
                 color='darkblue', alpha=0.75)
    ax0.set_xlabel('Air pressure (hPa)')
    ax0.set_ylabel('Air density (kg m$^{-3}$)')

    if plot_suffix:
        module4plots.plotting(fig, plotname, plot_suffix)

# -------------------------------------------------------------------------
def plot_sensheatflux(library_name, plot_suffix=None, plotname='sensheatflux'):
    '''
    Plot results of the via c-binding call Fortran function sensheatflux

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'sensheatflux'.

    Returns
    -------
    None.

    '''
    print('* Sensible heat flux')
    rho_air = 1.0 #kg m-30
    tmelt = FREEZING_TEMP
    wind =  np.array([2.0, 10., 20., 30.])# m/2
    tair = np.linspace(-25, 10, 36, dtype=float) + tmelt
    tsuf = np.linspace(-25,  5, 31, dtype=float) + tmelt
    sensheatflux_multi = np.zeros((len(wind), len(tair), len(tsuf)))

    for ita, ta in enumerate(tair):
        for its, ts in enumerate(tsuf):
            rho_air_array = np.zeros_like(wind) + rho_air
            tair_array = np.zeros_like(wind) + ta
            tsuf_array = np.zeros_like(wind) + ts
            sensheatflux_multi[:, ita, its] = \
                mod_physic.sensheatflux(rho_air_array, wind,
                                        tair_array, tsuf_array, library_name)

    #
    # Figure 1
    #
    fig = plt.figure(dpi=300, facecolor='white')
    ax0 = fig.subplots(2, 2)
    levels = np.array([-1000, -500, -200, -100, -50, -20, -10, 10, 20, 50, 100, 200, 500, 1000], dtype=float)
    cm_run = mpl.cm.seismic

    norm_color = mpl.colors.BoundaryNorm(boundaries=levels, ncolors=cm_run.N)

    for icount, ax in enumerate([ax0[0, 0], ax0[0, 1], ax0[1, 0], ax0[1, 1]]):
        csf = ax.contourf(tsuf, tair, sensheatflux_multi[icount, :, :],
                          levels=levels, extend='both', norm=norm_color, cmap=cm_run)

        ax.plot([max(min(tair),min(tsuf)), min(max(tair),max(tsuf))],
                [max(min(tair),min(tsuf)), min(max(tair),max(tsuf))],
                color='black', linestyle='--', label='1:1')

        ax.plot([tmelt, tmelt], [min(tair), max(tair)], color='gold',
                linestyle=':', label='$T_{freeze}$')

        # x-/y-labels
        if ax in ax0[1, :]:
            # lower row
            ax.set_xlabel('$T_{surface}$ (K)')
        if ax in ax0[:, 0]:
            # left column
            ax.set_ylabel('$T_{air}$ (K)')

        # ticks
        if ax in ax0[0, :]:
            ax.set_xticklabels([])
        if ax in ax0[:, 1]:
            ax.set_yticklabels([])
        if ax == ax0[1, 1]:
            ax.legend()

        title = r'wind = {:2.0f} m s$^{{-1}}$'.format(wind[icount])
        ax.text(((tsuf[-1]-tsuf[0])*0.25+tsuf[0]),
                ((tair[-1]-tair[0])*0.05+tair[0]), title)

    fig.colorbar(csf, ax=ax0, label='Sensible heat flux (W m$^{-2}$)')

    if plot_suffix:
        module4plots.plotting(fig, plotname+'_2D', plot_suffix)

# -------------------------------------------------------------------------
def plot_temp_excess2melt(library_name, plot_suffix=None,
                          plotname='temp_excess2melt'):
    '''
    Plot results of the via c-binding call Fortran function texcess2melt

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'temp_excess2melt'.

    Returns
    -------
    None.

    '''
    print('* Melting by temperature excess')

    delta_temp = np.linspace(-0.5, 3.5, 280, dtype=float) #Kelvin Diff to T_freeze
    density_snow = 350.0 # kg m-3
    density_ice = 910.0  # kg m-3
    layer_thickness = 0.1 # Meter

    snow_content = np.zeros_like(delta_temp)
    ice_content = np.zeros_like(delta_temp)
    fraction_snow = 0.95
    snow_content95 = snow_content + layer_thickness*density_snow*fraction_snow
    ice_content95 = ice_content + layer_thickness*density_ice*(1.0-fraction_snow)
    fraction_snow = 0.50
    snow_content50 = snow_content + layer_thickness*density_snow*fraction_snow
    ice_content50 = ice_content + layer_thickness*density_ice*(1.0-fraction_snow)
    fraction_snow = 0.05
    snow_content05 = snow_content + layer_thickness*density_snow*fraction_snow
    ice_content05 = ice_content + layer_thickness*density_ice*(1.0-fraction_snow)



    melting95snow = mod_physic.temp_excess2melt(delta_temp, snow_content95,
                                                ice_content95, library_name)
    melting50snow = mod_physic.temp_excess2melt(delta_temp, snow_content50,
                                                ice_content50, library_name)
    melting05snow = mod_physic.temp_excess2melt(delta_temp, snow_content05,
                                                ice_content05, library_name)

    #
    # Figure 1
    #
    fig = plt.figure(dpi=300, facecolor='white')
    ax = fig.subplots()

    ax.plot(delta_temp, melting95snow,'--g', linewidth=2, label='95% snow')
    ax.plot(delta_temp, melting50snow,'-.c', linewidth=2, label='50% snow')
    ax.plot(delta_temp, melting05snow,'-b', linewidth=2, label=' 5% snow')

    ax.set_xlabel('Temperature above freezing point temperaure (Kelvin)')
    ax.set_ylabel('Melting (kg m$^{{-2}}$)')



    ax.set_title(r'Melting of snow and ice in a layer of '
                 +str(layer_thickness)+' m thickness' )
    # ax.text(delta_temp[0]+(delta_temp[-1]-delta_temp[0])*0.65,
    #         melting95snow[0]+(melting95snow[-1]-melting95snow[0])*0.4,
    #         '$\\Delta$h = '+str(layer_thickness)+' m')
    ax.text(delta_temp[0]+(delta_temp[-1]-delta_temp[0])*0.65,
            melting95snow[0]+(melting95snow[-1]-melting95snow[0])*0.3,
            '$\\rho_{{snow}}$ = '+str(density_snow)+' kg m$^{{-3}}$')

    ax.legend(loc='upper left', title='Snow fraction in layer')

    if plot_suffix:
        module4plots.plotting(fig, plotname, plot_suffix)


# -------------------------------------------------------------------------
def plot_latheatflux(library_name, plot_suffix=None,
                         plotname='latheatflux'):
    '''
    Plot results of the via c-binding call Fortran function latheatflux

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'latheatflux'.

    Returns
    -------
    None.

    '''
    print('* Latent heat flux')
    wind =  np.array([2.0, 10., 20., 30.])# m/2
    vair = np.linspace(0, 1500, 36, dtype=float)
    vsuf = np.linspace(0, 1200, 31, dtype=float)
    latheatflux_multi = np.zeros((len(wind), len(vair), len(vsuf)))

    for iva, va in enumerate(vair):
        for ivs, vs in enumerate(vsuf):
            vair_array = np.zeros_like(wind) + va
            tsuf_array = np.zeros_like(wind) + vs
            latheatflux_multi[:, iva, ivs] = \
                mod_physic.latheatflux(wind,
                                        vair_array, tsuf_array, library_name)

    #
    # Figure 1
    #
    fig = plt.figure(dpi=300, facecolor='white')
    ax0 = fig.subplots(2, 2)

    levels = np.array([-1000, -500, -200, -100, -50, -20, -10, -5, 5, 10, 20, 50, 100, 200, 500, 1000], dtype=float)
    cm_run = mpl.cm.PiYG
    norm_color = mpl.colors.BoundaryNorm(boundaries=levels, ncolors=cm_run.N)

    for icount, ax in enumerate([ax0[0, 0], ax0[0, 1], ax0[1, 0], ax0[1, 1]]):
        csf = ax.contourf(vsuf, vair, latheatflux_multi[icount, :, :],
                          levels=levels, extend='both', norm=norm_color, cmap=cm_run)

        ax.plot([max(min(vair),min(vsuf)), min(max(vair),max(vsuf))],
                [max(min(vair),min(vsuf)), min(max(vair),max(vsuf))],
                color='salmon', linestyle='--', label='1:1')

        # x-/y-labels
        if ax in ax0[1, :]:
            # lower row
            ax.set_xlabel('$p^{vapor}_{surface}$ (Pa)')
        if ax in ax0[:, 0]:
            # left column
            ax.set_ylabel('$p^{vapor}_{air}$ (Pa)')

        # ticks
        if ax in ax0[0, :]:
            ax.set_xticklabels([])
        if ax in ax0[:, 1]:
            ax.set_yticklabels([])

        if ax == ax0[1, 1]:
            ax.legend(loc='upper left')

        title = r'wind = {:2.0f} m s$^{{-1}}$'.format(wind[icount])
        ax.text(((vsuf[-1]-vsuf[0])*0.25+vsuf[0]),
                ((vair[-1]-vair[0])*0.05+vair[0]), title)

    fig.colorbar(csf, ax=ax0, label='Latent heat flux (W m$^{-2}$)')

    if plot_suffix:
        module4plots.plotting(fig, plotname+'_2D', plot_suffix)

# -------------------------------------------------------------------------
def plot_sublimation(library_name, plot_suffix=None, plotname='sublimation'):
    '''
    Plot results of the via c-binding call Fortran function sublimation

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'sublimation'.

    Returns
    -------
    None.

    '''
    print('* Sublimation')
    tmelt = FREEZING_TEMP
    stemp = np.linspace(-5, 5, 701) + tmelt
    lhflux = np.array([-100, -25, -10, 0, 10, 25, 100])

    mtemp =  np.zeros_like(stemp) + tmelt
    sublimation_multi = np.zeros((len(stemp), len(lhflux)))

    for ilhfx, lhfx in enumerate(lhflux):
        lhfx_array = np.zeros_like(stemp) + lhfx
        sublimation_multi[:, ilhfx] = \
                mod_physic.sublimation(lhfx_array, stemp, mtemp, library_name)

    #
    # Figure
    #
    fig0 = plt.figure(dpi=300, facecolor='white')
    ax0 = fig0.subplots()

    line0 = ax0.plot(stemp, sublimation_multi, '-', linewidth=2)
    # Different line styples for each line
    for iline, line in enumerate(line0):
        if iline == 0 or iline == 1 or iline == 2:
            line.set_linestyle(':')
        if iline == 3:
            line.set_linestyle('--')

    #
    # T_Freeze line
    #
    ymax = np.max(sublimation_multi) + \
        (np.max(sublimation_multi)-np.min(sublimation_multi)) * 0.05
    ymin = np.min(sublimation_multi) - \
        (np.max(sublimation_multi)-np.min(sublimation_multi)) * 0.05

    ax0.plot([tmelt, tmelt], [ymin, ymax], '-', color='white', linewidth=3)
    ax0.plot([tmelt, tmelt], [ymin, ymax], '--', color='darkcyan', linewidth=2)
    ax0.text(tmelt+0.02*(stemp[-1]-stemp[0]), np.max(sublimation_multi),
             r'$T_{freeze}$', color='darkcyan')

    ax0.set_xlabel('Surface temperature (K)')
    ax0.set_ylabel('Sublimation (kg m$^{{-2}}$ s$^{{-1}}$)')

    #
    # legend
    #
    label = []
    for lhfx in lhflux:
        label.append('{:g}'.format(lhfx))
    ax0.legend(line0, label, title='Latent heat flux\n(W m$^{-2}$)',
               loc='upper right')

    if plot_suffix:
        module4plots.plotting(fig0, plotname, plot_suffix)

# -------------------------------------------------------------------------
def plot_evaporation(library_name, plot_suffix=None, plotname='evaporation'):
    '''
    Plot results of the via c-binding call Fortran function evoporation

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'evoporation'.

    Returns
    -------
    None.

    '''
    print('* Evaporation')
    tmelt = FREEZING_TEMP
    stemp = np.linspace(-5, 5, 701) + tmelt
    shflux = np.array([-100, -25, -10, 0, 10, 25, 100])

    mtemp =  np.zeros_like(stemp) + tmelt
    evaporation_multi = np.zeros((len(stemp), len(shflux)))

    for ishfx, shfx in enumerate(shflux):
        shfx_array = np.zeros_like(stemp) + shfx
        evaporation_multi[:, ishfx] = \
                mod_physic.evaporation(shfx_array, stemp, mtemp, library_name)

    #
    # Figure
    #
    fig0 = plt.figure(dpi=300, facecolor='white')
    ax0 = fig0.subplots()

    line0 = ax0.plot(stemp, evaporation_multi, '-', linewidth=2)
    # Different line styples for each line
    for iline, line in enumerate(line0):
        if iline == 0 or iline == 1 or iline == 2:
            line.set_linestyle(':')
        if iline == 3:
            line.set_linestyle('--')

    #
    # T_Freeze line
    #
    ymax = np.max(evaporation_multi) + \
        (np.max(evaporation_multi)-np.min(evaporation_multi)) * 0.05
    ymin = np.min(evaporation_multi) - \
        (np.max(evaporation_multi)-np.min(evaporation_multi)) * 0.05

    ax0.plot([tmelt, tmelt], [ymin, ymax], '-', color='white', linewidth=3)
    ax0.plot([tmelt, tmelt], [ymin, ymax], '--', color='darkcyan', linewidth=2)
    ax0.text(tmelt-0.02*(stemp[-1]-stemp[0]), np.max(evaporation_multi),
             r'$T_{freeze}$', color='darkcyan', horizontalalignment='right')

    ax0.set_xlabel('Surface temperature (K)')
    ax0.set_ylabel('Evaporation (kg m$^{{-2}}$ s$^{{-1}}$)')

    #
    # legend
    #
    label = []
    for shfx in shflux:
        label.append('{:g}'.format(shfx))
    ax0.legend(line0, label, title='Sensible heat flux\n(W m$^{-2}$)',
               loc='lower left')

    fig0.tight_layout()

    if plot_suffix:
        module4plots.plotting(fig0, plotname, plot_suffix)

# -------------------------------------------------------------------------
def plot_tpre2snowf_cut(library_name, plot_suffix=None,
                         plotname='tpre2snowf_cut'):
    '''
    Plot results of the via c-binding call Fortran function tpre2snowf_cut

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'tpre2snowf_cut'.

    Returns
    -------
    None.

    '''
    print('* Snowfall for temperature threshold based function')
    tmelt0 = FREEZING_TEMP
    tair = np.linspace(-10, 10, 210) + tmelt0
    tprec = np.ones_like(tair)

    snowf = mod_physic.tpre2snowf_cut(tprec, tair, tmelt0, library_name)

    #
    # Figure
    #
    fig0 = plt.figure(dpi=300, facecolor='white')
    ax0 = fig0.subplots()

    ax0.plot(tair, snowf, '.', color='darkblue', markersize=2) #lw=2
    ax0.plot([tmelt0, tmelt0], [-0.02, np.max(tprec)*1.02], '--', c='cyan', lw=2)
    ax0.text(tmelt0+0.02*(tair[-1]-tair[0]), 1. , r'$T_{freeze}$', c='darkcyan')

    ax0.set_xlabel('Air temperature (K)')
    ax0.set_ylabel('Snowfall fraction')

    if plot_suffix:
        module4plots.plotting(fig0, plotname, plot_suffix)

# -------------------------------------------------------------------------
def plot_tpre2snowf_lin(library_name, plot_suffix=None,
                         plotname='tpre2snowf_lin'):
    '''
    Plot results of the via c-binding call Fortran function tpre2snowf_lin

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'tpre2snowf_lin'.

    Returns
    -------
    None.

    '''
    print('* Snowfall for temperature range based function')
    tmelt0 = FREEZING_TEMP
    thigh = tmelt0 + 1.
    tlow = tmelt0 - 1.
    tair = np.linspace(-10, 10, 210) + tmelt0
    tprec = np.ones_like(tair)

    snowf = mod_physic.tpre2snowf_lin(tprec, tair, thigh, tlow, library_name)

    #
    # Figure
    #
    fig0 = plt.figure(dpi=300, facecolor='white')
    ax0 = fig0.subplots()

    ax0.plot([tlow], [1.0], 's', color='darkred', markersize=8,
             label='reversal point '+str(tlow)+' K')
    ax0.plot([thigh], [0.0], 'D', color='darkorange', markersize=8,
             label='reversal point '+str(thigh)+' K')

    ax0.plot(tair, snowf, '.', color='darkblue', markersize=2)
    ax0.plot(tair, snowf, linewidth=2, color='darkblue')
    ax0.plot([tmelt0, tmelt0], [-.02, np.max(tprec)*1.02], '--', c='cyan', lw=2)
    ax0.text(tmelt0+0.02*(tair[-1]-tair[0]), 1., r'$T_{freeze}$', c='darkcyan')

    ax0.set_xlabel('Air temperature (K)')
    ax0.set_ylabel('Snowfall fraction')
    ax0.legend(loc='lower left')

    if plot_suffix:
        module4plots.plotting(fig0, plotname, plot_suffix)

# -------------------------------------------------------------------------
def plot_tpre2snowf_tanh(library_name, plot_suffix=None,
                         plotname='tpre2snowf_tanh'):
    '''
    Plot results of the via c-binding call Fortran function tpre2snowf_tanh

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'tpre2snowf_tanh'.

    Returns
    -------
    None.

    '''
    print('* Snowfall for hyperbolic tangent air temperature range')
    tmelt0 = FREEZING_TEMP
    omega = np.array([0.2, 0.5, 0.8, 1.2, 2.0, 3.0])
    #omega = omega[::-1]
    tair = np.linspace(-10, 10, 210) + tmelt0
    tair = np.linspace(265, 285, 210)
    treverse = np.zeros_like(tair)+tmelt0
    tprec = np.ones_like(tair)

    snowf = np.zeros((len(tair), len(omega)))

    for iomega, omega_value in enumerate(omega):
        omega_array = np.zeros_like(tair)+omega_value
        snowf[:, iomega] = mod_physic.tpre2snowf_tanh(tprec,
                                                      tair,
                                                      treverse,
                                                      omega_array,
                                                      library_name)

    #
    # Figure
    #
    fig0 = plt.figure(dpi=300, facecolor='white')
    ax0 = fig0.subplots()

    for iomega, omega_value in enumerate(omega):
        if omega_value >= 2 :
            line_stype='--'
        else:
            line_stype='-'
        ax0.plot(tair, snowf[:, iomega], linewidth=2, linestyle=line_stype,
                 label=r'$\Omega=${:.1f} K'.format(omega_value))

    ax0.plot([tmelt0, tmelt0], [-0.05, np.max(tprec)*1.05], ':',
         c='gray', lw=2) #, label=r'$T_{{freeze}}$')
    ax0.text(tmelt0+0.02*(tair[-1]-tair[0]), 1.,
             r'$T_{{reverse}} = T_{{freeze}}=${:.2f} K'.format(tmelt0), c='gray')

    ax0.set_ylim(-0.05, 1.05)

    ax0.set_xlabel('Air temperature (K)')
    ax0.set_ylabel('Snowfall fraction')
    ax0.legend(loc='lower left')

    if plot_suffix:
        module4plots.plotting(fig0, plotname, plot_suffix)

# -------------------------------------------------------------------------
def plot_hctemp(library_name, plot_suffix=None, plotname='hctemp'):
    '''
    Plot results of the via c-binding call Fortran function hctemp

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'hctemp'.

    Returns
    -------
    None.

    '''
    print('* Height correction air temperature')
    delta_z = np.linspace(-1500, 1500, 301, dtype=float)
    temp = np.zeros_like(delta_z) #+10
    lapse_rate = LAPSE_RATE

    hctemp = mod_physic.hctemp(temp, delta_z, lapse_rate, library_name)

    #
    # Figure
    #
    fig0 = plt.figure(dpi=300, facecolor='white')
    ax0 = fig0.subplots()

    ax0.plot(delta_z, hctemp, lw=2, linestyle='-', label='Height-corrected')
    ax0.plot(delta_z, temp, lw=2, linestyle='--', label='Original')

    ax0.text(delta_z[5], hctemp[-1],'Lapse rate = '+str(lapse_rate*1000)+' K km$^{-1}$')
    ax0.set_xlabel('Height difference (m)')
    ax0.set_ylabel('Air temperature (K)')
    ax0.legend(title='Temperature')

    if plot_suffix:
        module4plots.plotting(fig0, plotname, plot_suffix)

# -------------------------------------------------------------------------
def plot_hclongwave(library_name, plot_suffix=None, plotname='hclongwave'):
    '''
    Plot results of the via c-binding call Fortran function hclongwave

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'hclongwave'.

    Returns
    -------
    None.

    '''
    print('* Height correction longwave radtiation')
    delta_z = np.linspace(-2000, 2000, 301, dtype=float)
    lwrad = np.zeros_like(delta_z) + 50.
    gradlw = GRADLW

    hclongwave = mod_physic.hclongwave(lwrad, delta_z, library_name)

    #
    # Figure
    #
    fig0 = plt.figure(dpi=300, facecolor='white')
    ax0 = fig0.subplots()

    ax0.plot(delta_z, hclongwave, lw=2, linestyle='-', label='Height-corrected')
    ax0.plot(delta_z, lwrad, lw=2, linestyle='--', label='Original')

    ax0.text(delta_z[5], hclongwave[-1],'Rate = {:7.3g}'.format(gradlw)+' W m$^{-2}$ m$^{-1}$')
    ax0.set_xlabel('Height difference (m)')
    ax0.set_ylabel('Longwave radiation (K)')
    ax0.legend(title='Radiation')

    if plot_suffix:
        module4plots.plotting(fig0, plotname, plot_suffix)

# -------------------------------------------------------------------------
def plot_hcpressure(library_name, plot_suffix=None, plotname='hcpressure'):
    '''
    Plot results of the via c-binding call Fortran function hcpressure

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'hcpressure'.

    Returns
    -------
    None.

    '''
    print('* Height correction of atmospheric pressure')
    press = np.linspace(650e2, 1050e2, 101, dtype=float)
    zs_ref = np.linspace(0, 3000, 4, dtype=float)
    zs_target = np.linspace(0, 6000, 9)
    hcpressure_multi = np.zeros((len(press), len(zs_target), len(zs_ref)))

    for iref, zref in enumerate(zs_ref):
        zstgt2d, press2d = np.meshgrid(zs_target, press)
        zsref2d = np.zeros_like(press2d) + zref
        hcpressure_multi[:, :, iref] = \
                mod_physic.hcpressure(press2d, zsref2d, zstgt2d, library_name)
    #
    # Alternative formulation using a 1dim arrays and two nested loops
    #
    # for itgt, ztgt in enumerate(zs_target):
    #     for iref, zref in enumerate(zs_ref):
    #         zref_array = np.zeros_like(press) + zref #zs_ref[iref]
    #         ztgt_array = np.zeros_like(press) + ztgt #zs_target[itgt]
    #         hcpressure_multi[:, itgt, iref] = \
    #             mod_physic.hcpressure(press, zref_array, ztgt_array,
    #                                   library_name)

    pressure_factor = 0.01

    #
    # Figure 1
    #
    fig = plt.figure(dpi=300, facecolor='white')
    ax0 = fig.subplots(2, 2)

    levels = np.linspace(32000, 110000, 14) #(30000, 110000, 9) #(30000, 120000, 11)
    for icount, ax in enumerate([ax0[0, 0], ax0[0, 1], ax0[1, 0], ax0[1, 1]]):
        csf = ax.contourf(zs_target, press*pressure_factor,
                          hcpressure_multi[:, :, 0]*pressure_factor,
                          levels=levels*pressure_factor, extend='both',
                          cmap='plasma_r')

        # x-/y-labels
        if ax in ax0[1, :]:
            # lower row
            ax.set_xlabel('$zs_{target}$ (m)')
        if ax in ax0[:, 0]:
            # left column
            ax.set_ylabel('Pressure (hPa)')

        # ticks
        if ax in ax0[0, :]:
            ax.set_xticklabels([])
        if ax in ax0[:, 1]:
            ax.set_yticklabels([])

        title = r'$z_0$ = {:4.0f} m'.format(zs_ref[icount])
        ax.text(((zs_target[-1]-zs_target[0])*0.05+zs_target[0]),
                ((press[-1]-press[0])*0.05+press[0])*pressure_factor, title)

    fig.colorbar(csf, ax=ax0, label='Corrected air pressure (hPa)')

    if plot_suffix:
        module4plots.plotting(fig, plotname+'_2D', plot_suffix)

    #
    # Figure 2
    #
    fig = plt.figure(dpi=300, facecolor='white')
    ax0, ax1 = fig.subplots(1, 2, sharey=True)
    fig.subplots_adjust(wspace=0.05)

    iref = 1

    # x-axis: air pressure
    step = 2
    line0 = ax0.plot(press*pressure_factor,
                     hcpressure_multi[:, ::step, iref]*pressure_factor, lw=2)
    ax0.set_title(r'Topopgraphy $z_0$={:4.0f} m'.format(zs_ref[iref]), ha='left')
    ax0.set_xlabel('Air pressure (hPa)')
    ax0.set_ylabel('Corrected air pressure (hPa)')
    label = []
    for ztgt in zs_target[::step]:
        label.append('{:4.0f}'.format(ztgt))
    ax0.legend(line0, label, title=r'$zs_{target}$ (m)', loc='upper left')


    # x-axis: zs_target
    step = 20
    line0 = ax1.plot(zs_target,
                     hcpressure_multi[::step, :, iref].T*pressure_factor, lw=2)
    ax1.set_xlabel(r'$zs_{target}$ (m)')
    #ax1.set_ylabel('Corrected pressure (hPa)')
    label = []
    for prs in press[::step]:
        label.append('{:4.0f}'.format(prs*pressure_factor))
    ax1.legend(line0, label, title='Air pressure\n(hPa)', loc='upper right')

    if plot_suffix:
        module4plots.plotting(fig, plotname+'_line', plot_suffix)

# -------------------------------------------------------------------------
def plot_hcprecip_high_desert(library_name, plot_suffix=None,
                         plotname='hcprecip_high_desert'):
    '''
    Plot results of the via c-binding call Fortran function hcprecip_high_desert

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'hcprecip_high_desert'.

    Returns
    -------
    None.

    '''
    print('* Height correction of atmospheric precipure')
    precip = 1.
    zs_ref = np.linspace(0, 5000, 101, dtype=float)
    zs_target = np.linspace(0, 6000, 121)

    zsref2d, zstgt2d = np.meshgrid(zs_ref, zs_target)
    precip2d = np.zeros_like(zsref2d) + precip
    hcprecip_desert_multi = \
            mod_physic.hcprecip_high_desert(precip2d, zsref2d, zstgt2d,
                                            library_name)
    #
    # Alternative formulation using a 1dim arrays and a loop
    #
    # hcprecip_desert_multi = np.zeros((len(zs_target), len(zs_ref)))
    # for itgt, ztgt in enumerate(zs_target):
    #     precip_array = np.zeros_like(zs_ref) + precip
    #     ztgt_array = np.zeros_like(zs_ref) + ztgt #zs_target[itgt]
    #     hcprecip_desert_multi[itgt, :] = \
    #         mod_physic.hcprecip_high_desert(precip_array, zs_ref,
    #                                         ztgt_array, library_name)


    #
    # Figure 1
    #
    fig0 = plt.figure(dpi=300, facecolor='white')
    ax0 = fig0.subplots()
    ax0.set_aspect(zs_ref[-1]/zs_target[-1])

    levels = np.linspace(0, 9, 91)*precip
    levels = np.linspace(0, 2, 41)*precip
    csf = ax0.contourf(zs_ref, zs_target, hcprecip_desert_multi, cmap='BrBG',
                      levels=levels, extend='max')

    flag_contourlines = True
    if flag_contourlines:
        # Offset of -1 creates dashed lines for values below 1 and
        # solid lines otherwise (values>=1)
        offset = -1
        levels = np.array([1./7, 0.25, 0.5, 2., 4., 7.]) + offset
        ax0.contour(zs_ref, zs_target, hcprecip_desert_multi.squeeze()+offset,
                   levels=levels, colors='k')

        ax0.text(3000, 10, '2', color='w')
        ax0.text(4000, 10, '4', color='w')
        ax0.text(4800, 10, '7', color='w')

        ax0.text(10, 3000, '1/2', color='w')
        ax0.text(10, 4000, '1/4', color='w')
        ax0.text(10, 4800, '1/7', color='w')

    ax0.set_ylabel('Target elevation: $zs_{target}$ (m)')
    ax0.set_xlabel('Original elevation: $z_0$ (m)')
    ax0.set_title('hcprecip_high_desert')

    fig0.colorbar(csf, ax=ax0, label='Precipitation correction factor')

    if plot_suffix:
        module4plots.plotting(fig0, plotname+'_2D', plot_suffix)

# -------------------------------------------------------------------------
def plot_snowlayers2pressure(library_name, plot_suffix=None,
                             plotname='snowlayers2pressure'):
    '''
    Plot results of the via c-binding call Fortran function snowlayers2pressure

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'snowlayers2pressure'.

    Returns
    -------
    None.

    '''
    print('* Snow layers to pressure (snowlayers2pressure)')
    patm = PATM # Pa
    thickness = np.ones(5000) * 0.1
    cdensity = CDENSITY
    depth_profile = np.cumsum(thickness)
    # "real" density profile
    # Applied density profile, where the density increases with height
    rho_profile = mod_physic.densprofile(depth_profile, RHO_SNOW, RHO_ICE,
                                         cdensity, library_name)
    snowlayer_pres = \
            mod_physic.snowlayers2pressure(thickness, rho_profile, patm,
                                           library_name)

    #
    # Figure 1
    #
    pressure_factor = 1e-6
    depth_profile_min = 0
    depth_profile_split = 50
    depth_profile_max = max(depth_profile)

    fig = plt.figure(dpi=300, facecolor='white')
    [ax0d, ax0p], [ax1d, ax1p] = fig.subplots(2, 2,
                            gridspec_kw={'height_ratios':[50, 50],
                                         'width_ratios':[45, 55]})
    fig.subplots_adjust(hspace=0.0025, wspace=0.1)

    for ax0 in [ax0p, ax1p, ax0d, ax1d]:
        # Plot
        if ax0 in (ax0p, ax1p):
            ax0.plot(snowlayer_pres*pressure_factor, depth_profile,
                    linewidth=2, color='darkslateblue')
            ax0.set_xlim(0.001, max(snowlayer_pres*pressure_factor))
        else:
            ax0.plot(rho_profile, depth_profile, lw=2, c='darkgoldenrod')
            ax0.set_xlim(RHO_SNOW, 950) #RHO_ICE)

        #
        # Handling of the splitting between higher resolved upper column
        # and coarser resolved lower column
        #
        if ax0 in (ax0p, ax0d):
            ax0.set_ylim(depth_profile_min, depth_profile_split)
        if ax0 in (ax1p, ax1d):
            ax0.set_ylim(depth_profile_split+0.001, depth_profile_max)

        if ax0 == ax0p:
            ax0.set_title('Snow/ice column pressure') #, horizontalalignment='left')
        if ax0 == ax0d:
            ax0.set_title('Density profile')
            ax0.set_ylabel('Depth (m)', horizontalalignment='right')

        if ax0 == ax1p:
            ax0.set_ylim(depth_profile_split+0.001, depth_profile_max)
            if pressure_factor == 1:
                ax0.set_xlabel('Pressure (Pa)')
            elif pressure_factor == 0.01:
                ax0.set_xlabel('Pressure (hPa)')
            elif pressure_factor == 0.001:
                ax0.set_xlabel('Pressure (kPa)')
            elif pressure_factor == 0.000001:
                ax0.set_xlabel('Pressure (MPa)')
            else:
                ax0.set_xlabel('Pressure ({:g}'.format(pressure_factor)+' Pa)')
        if ax0 == ax1d:
            ax0.set_xlabel('Density (kg m$^{{-3}}$)')
        if ax0 in (ax0d, ax1d):
            ax0.yaxis.tick_left()
        elif ax0 in(ax0p, ax1p):
            ax0.yaxis.tick_right()
        ax0.invert_yaxis()

    if plot_suffix:
        module4plots.plotting(fig, plotname, plot_suffix)

# -------------------------------------------------------------------------
def plot_densification_pressure(library_name, plot_suffix=None,
                                plotname='densification_pressure'):
    '''
    Plot results of the via c-binding call Fortran function densifiction_pressure

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'densifiction_pressure'.

    Returns
    -------
    None.

    '''
    print('* Densification of snow layers (densification_pressure)')
    dtime = 3*3600. # seconds
    number_of_time_steps = int(400*86400/dtime) #int(365*86400/dtime) #2000
    patm = PATM # Pa
    tmelt = FREEZING_TEMP
    thickness = np.concatenate((np.ones(500)*0.1, np.ones(301)*0.5,
                               np.ones(100)*2))
    temp0 = tmelt-21.5
    temperature = np.zeros_like(thickness) + temp0
    scont_frac = 0.87
    icont_frac = 0.10
    wcont_frac = 1. - scont_frac - icont_frac
    if ( wcont_frac < 0 or wcont_frac > 1 ):
        print('====> Unrealistic wcont_frac =', min(wcont_frac),
              '/', max(wcont_frac), ' outside valid range [0, 1]')

    scont = np.zeros_like(thickness) + scont_frac*thickness
    icont = np.zeros_like(thickness) + icont_frac*thickness
    wcont = np.zeros_like(thickness) + wcont_frac*thickness
    cdensity = CDENSITY

    depth_profile = np.cumsum(thickness)
    # "real" density profile
    # Applied density profile, where the density increases with height
    rho_profile0 = mod_physic.densprofile(depth_profile, RHO_SNOW, RHO_ICE,
                                          cdensity, library_name)
    rho_profile0 = np.zeros_like(depth_profile) + RHO_SNOW
    rho_profile = scont*rho_profile0 + icont*RHO_ICE + wcont*RHO_FWATER

    snowlayer_pres = \
            mod_physic.snowlayers2pressure(thickness, rho_profile, patm,
                                           library_name)

    densified = np.zeros((len(thickness), number_of_time_steps))
    densified[:, 0] = rho_profile0

    tmelt_array = np.zeros_like(thickness)+tmelt
    for its in range(1, number_of_time_steps):
        snowlayer_pres = \
            mod_physic.snowlayers2pressure(thickness, densified[:, its-1],
                                           patm, library_name)
        densified[:, its] = mod_physic.densification_pressure(dtime,
                                                              snowlayer_pres,
                                                              temperature,
                                                              densified[:, its-1],
                                                              scont,
                                                              wcont,
                                                              tmelt_array,
                                                              library_name)

    date_array = np.arange(0, number_of_time_steps, 1) * (dtime/3600/24) # days

    #
    # Figure 1
    #
    depth_profile_min = 0
    depth_profile_split = 50
    depth_profile_max = max(depth_profile)

    fig1 = plt.figure(dpi=300, facecolor='white')
    axu, axl = fig1.subplots(2, 1, gridspec_kw={'height_ratios':[50, 50]})
    fig1.subplots_adjust(hspace=0.0005)

    for ax0 in [axu, axl]:
        csf = ax0.contourf(date_array, depth_profile, densified, cmap='cividis_r')
        # Note: pcolormesh shows the grid structure but it creates a huge pdf file
        # csf = ax0.pcolormesh(date_array, depth_profile, densified,
        #                    cmap='cividis_r', shading='auto') #,vmin=RHO_SNOW, vmax=RHO_FWATER*1)
        if ax0 == axu:
            ax0.set_ylim([depth_profile_min, depth_profile_split])
            ax0.set_ylabel('Depth (m)', horizontalalignment='right')
            ax0.set_title('Densification')
            ax0.set_xticklabels([])
        elif ax0 == axl:
            ax0.set_ylim([depth_profile_split+0.001, depth_profile_max])
            ax0.set_xlabel('Time axis (day)')
        ax0.invert_yaxis()

    axl.text(date_array[0]+(date_array[-1]-date_array[0])*0.9,
             depth_profile_split+(depth_profile[-1]-depth_profile_split)*0.33,
             "$T(t_0)$ = {:.0f} K\n".format(temp0)+
             "$p_{{s}}$ = {:3.0f} %\n".format(scont_frac*100.)+
             "$p_{{i}}$ = {:3.0f} %\n".format(icont_frac*100.)+
             "$p_{{w}}$ = {:3.0f} %".format(wcont_frac*100.),
             ha="right", va="top",bbox=dict(boxstyle="square",
                                            ec='darkgray', fc='lightgray',
                                            alpha=0.5))

    fig1.colorbar(csf, ax=[axu, axl], label='Density (kg m$^{{-3}}$)')

    if plot_suffix:
        module4plots.plotting(fig1, plotname+'_2D', plot_suffix)

    #
    # Figure 2
    #
    profile_depths_check = np.array([0.1, 2, 5, 10, 20, 50, 100])
    index = np.zeros(len(profile_depths_check), dtype=int)
    label = []
    for idx, depth in enumerate(profile_depths_check):
        index[idx] = np.where( abs(depth_profile-depth) == min(abs(depth_profile-depth)) )[0][0]
        if depth < 1.:
            label.append('{:.1f}'.format(depth))
        else:
            label.append('{:.0f}'.format(depth))

    fig2 = plt.figure(dpi=300, facecolor='white')
    ax2 = fig2.subplots()
    line0 = ax2.plot(date_array, densified[index, :].T)
    for iline, line in enumerate(line0):
        if iline == 0:
            line.set_linestyle(':')
        if iline in [2, 4, 6]:
            line.set_linestyle('--')

    ax2.set_xlabel('Time axis (day)')
    ax2.set_ylabel('Density (kg m$^{{-3}}$)')
    ax2.legend(line0, label, title='Depth (m)')

    if plot_suffix:
        module4plots.plotting(fig2, plotname+'_lines', plot_suffix)

# -------------------------------------------------------------------------
def plot_waterdepth2pressure(library_name, plot_suffix=None,
                             plotname='waterdepth2pressure'):
    '''
    Plot results of the via c-binding call Fortran function waterdepth2pressure

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'waterdepth2pressure'.

    Returns
    -------
    None.

    '''
    print('* Presure at different water depths')
    depth = np.linspace(0, 6000, 6001, dtype=float)
    rho_seawater = RHO_SEAWATER
    patm = PATM

    wdepth2pres = mod_physic.waterdepth2pressure(depth, rho_seawater,
                                                 patm, library_name)
    #
    # nearly identical lines, hence deactivated
    #
    FLAG_MULTI_DENSITIES = False
    if FLAG_MULTI_DENSITIES:
        rhos = np.array([900, 1028, 1040])
        wdepth2pres_multi = np.zeros((len(rhos), len(depth)))
        for icount, rho in enumerate(rhos):
            wdepth2pres_multi[icount, :] = \
                mod_physic.waterdepth2pressure(depth, rho, patm, library_name)

    #
    # Figure
    #
    pressure_factor = 1e-6
    depth_min = 0
    depth_split = 2000
    depth_max = max(depth)

    fig = plt.figure(dpi=300, facecolor='white')
    axu, axl = fig.subplots(2, 1, sharex=True,
                            gridspec_kw={'height_ratios':[50, 50]})
    fig.subplots_adjust(hspace=0.025)
    for ax0 in [axu, axl]:
        # Plot
        if FLAG_MULTI_DENSITIES:
            line = ax0.plot(wdepth2pres_multi.T*pressure_factor, depth, lw=2)
        else:
            ax0.plot(wdepth2pres*pressure_factor, depth, linewidth=2)

        #
        # Handling of the splitting between higher resolved upper column
        # and coarser resolved lower column
        #
        if ax0 == axu:
            ax0.set_ylim([depth_min, depth_split])
            ax0.set_ylabel('Depth (m)', horizontalalignment='right')
            ax0.set_title('Water pressure')
        elif ax0 == axl:
            ax0.set_ylim([depth_split+0.001, depth_max])
            if pressure_factor == 1:
                ax0.set_xlabel('Pressure (Pa)')
            elif pressure_factor == 0.01:
                ax0.set_xlabel('Pressure (hPa)')
            elif pressure_factor == 0.001:
                ax0.set_xlabel('Pressure (kPa)')
            elif pressure_factor == 0.000001:
                ax0.set_xlabel('Pressure (MPa)')
            else:
                ax0.set_xlabel('Pressure ({:g}'.format(pressure_factor)+' Pa)')
        ax0.invert_yaxis()

    if FLAG_MULTI_DENSITIES:
        label = [] #[r'$T_{frez}$']
        for rho in rhos:
            label.append('{:4.0f}'.format(rho))
        ax0.legend(line, label, title=r'$\rho$ (kg m$^{-1}$)')

    if plot_suffix:
        module4plots.plotting(fig, plotname, plot_suffix)

# -------------------------------------------------------------------------
def plot_height2pressure(library_name, plot_suffix=None,
                     plotname='height2pressure'):
    '''
    Plot results of the via c-binding call Fortran function height2pressure

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'height2pressure'.

    Returns
    -------
    None.

    '''
    print('* Presure at different column thicknesses')
    patm = PATM # Pa
    height = np.linspace(0, 6000, 6001, dtype=float)
    cdensity = CDENSITY*0.05
    flag_use_constant_profile = False
    if flag_use_constant_profile:
        # Constant profile
        rho_profile = np.ones_like(height) * 910. # Column of constant density
    else:
        # "real" density profile
        # Applied density profile, where the density increases with height
        rho_profile0 = mod_physic.densprofile(height, RHO_SNOW, RHO_ICE,
                                              cdensity, library_name)
        # * The increasing density with height could be missunderstood since
        # * we would get the pressure at depth `d` assuming that the entire
        # * column above have the density at this depth.
        # hence we have to compute the mean density above
        counts = np.linspace(1, len(height), len(height))
        rho_profile = np.cumsum(rho_profile0)/counts

    height2pres = mod_physic.height2pressure(height, rho_profile, patm,
                                             library_name)

    #
    # Figure
    #
    pressure_factor = 1e-6

    fig = plt.figure(dpi=300, facecolor='white')
    axl, axr = fig.subplots(1, 2, sharey=True)
    fig.subplots_adjust(wspace=0.025)

    axl.plot(rho_profile, height, linewidth=2, color='darkgreen',
                     label=r'$C_\rho$ = {:g}'.format(cdensity))
    axl.set_xlabel('Density (kg mr$^{-1}$)')
    axl.set_ylabel('Column height (m)')
    axl.set_title('Applied density profile')
    if min(rho_profile) != max(rho_profile):
        axl.legend()
    axl.invert_yaxis()

    axr.plot(height2pres*pressure_factor, height, linewidth=2,
             color='darkblue', label='Pressure')

    if pressure_factor == 1:
        axr.set_xlabel('Pressure (Pa)')
    elif pressure_factor == 0.01:
        axr.set_xlabel('Pressure (hPa)')
    elif pressure_factor == 0.001:
        axr.set_xlabel('Pressure (kPa)')
    elif pressure_factor == 0.000001:
        axr.set_xlabel('Pressure (MPa)')
    else:
        axr.set_xlabel('Pressure ({:g}'.format(pressure_factor)+' Pa)')
    axr.set_title('Resulting column pressure')

    axr.legend()

    if plot_suffix:
        module4plots.plotting(fig, plotname, plot_suffix)

# -------------------------------------------------------------------------
def plot_TfrezSeaWater(library_name, plot_suffix=None, plotname='TfrezSeaWater'):
    '''
    Plot results of the via c-binding call Fortran function TfrezSeaWater

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'TfrezSeaWater'.

    Returns
    -------
    None.

    '''
    print('* Sea water freezing temperature')
    tmelt0 = FREEZING_TEMP
    salt = np.linspace(0, 42, 85, dtype=float)
    # Get pressure from depths
    depth = np.linspace(0, 1500, 3001, dtype=float) #(0, 6000, 3001, dtype=float)
    rho_seawater = RHO_SEAWATER
    patm = PATM
    pres = mod_physic.waterdepth2pressure(depth, rho_seawater, patm, library_name)
    TfrezSeaWater_multi = np.zeros((len(pres), len(salt))) # Preallocate
    for iprs, prs in enumerate(pres):
        parray = np.ones_like(salt) * prs
        TfrezSeaWater_multi[iprs, :] = mod_physic.TfrezSeaWater(parray, salt,
                                                                library_name)
    pressure_factor=1.0e-6

    #
    # Figure 1
    #
    print('  - Freezing temperature of sea water: air pressure and temperature')
    fig1 = plt.figure(dpi=300, facecolor='white')
    ax1 = fig1.subplots()

    csf = ax1.contourf(salt, pres*pressure_factor, TfrezSeaWater_multi,
                      cmap='YlGnBu')
    ax1.contour(salt, pres*pressure_factor, TfrezSeaWater_multi,
                     colors='r', levels=[tmelt0-0.0001, tmelt0+0.0001])

    if pressure_factor == 0.001:
        ax1.set_ylabel('Water pressure (kPa)')
    elif pressure_factor == 1.0e-6:
        ax1.set_ylabel('Water pressure (MPa)')
    else:
        ax1.set_ylabel('Water pressure ({:g}'.format(pressure_factor)+' Pa)')
    ax1.set_xlabel('Salinity (kg m$^{-3}$)')

    plt.colorbar(csf, label='Sea water freezing point (K)')

    if plot_suffix:
        module4plots.plotting(fig1, plotname+'_2D', plot_suffix)

    #
    # Figure 2
    #
    print('  - Freezing temperature of sea water plots: x-axis: pressure or salt')
    fig2 = plt.figure(dpi=300, facecolor='white')
    ax0, ax1 = fig2.subplots(1,2, sharey=True)
    fig2.subplots_adjust(wspace=0.3)

    #
    # Left : x-axis = air temperature, y-axis = water vapor
    #
    step = 500
    ax0.plot([0, np.max(salt)], [tmelt0, tmelt0], '--', color='cyan')
    ax0.text(10., tmelt0+0.1, r'$T_{freeze}$ (fresh water)', color='darkcyan')
    line0 = ax0.plot(salt, TfrezSeaWater_multi[::step,:].T, linewidth=2)
    ax0.set_xlabel('Salinity (g kg$^{-1}$)')
    ax0.set_ylabel('Freezing temperature (K)')
    ax0.yaxis.tick_right()

    label = [] #[r'$T_{frez}$']
    for prs in pres[::step]:
        label.append('{:5.1f}'.format(prs*pressure_factor))
    ax0.legend(line0, label, title=r'p$_{sea}$ (MPa)')

    #
    # Right : x-axis = air pressure, y-axis = water vapor
    #
    step = 20
    ax1.plot([0, pres[-1]*pressure_factor], [tmelt0, tmelt0], '--', c='cyan')
    ax1.text(3., tmelt0+0.1, r'$T_{freeze}$ (fresh water)', color='darkcyan')
    line1 = ax1.plot(pres*pressure_factor, TfrezSeaWater_multi[:, ::step], lw=2)
    # Dashed lines for temperature above freezing
    for slt, lin in zip(salt[::step], line1):
        if slt > tmelt0:
            plt.setp(lin, linestyle='--')
    if pressure_factor == 0.001:
        ax1.set_xlabel('Water pressure (kPa)')
    elif pressure_factor == 1.0e-6:
        ax1.set_xlabel('Water pressure (MPa)')
    else:
        ax1.set_xlabel('Water pressure ({:g}'.format(pressure_factor)+' Pa)')
    ax1.set_ylabel('Freezing temperature (K)')
    ax1.yaxis.set_label_position("right")

    label = []
    for slt in salt[::step]:
        label.append('{:6.1f}'.format(slt))
    ax1.legend(line1, label, title=r'S (g kg$^{-1}$)', loc='lower left')

    if plot_suffix:
        module4plots.plotting(fig2, plotname+'_lines', plot_suffix)

# -------------------------------------------------------------------------
def plot_TfrezSoilWater(library_name, plot_suffix=None, plotname='TfrezSoilWater'):
    '''
    Plot results of the via c-binding call Fortran function TfrezSoilWater

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'TfrezSoilWater'.

    Returns
    -------
    None.

    '''
    print('* Freezing temperature of soil water')
    salt = np.linspace(0, 150, 151, dtype=float)
    TfrezSoil = mod_physic.TfrezSoilWater(salt, library_name)

    #
    # Figure
    #
    salt_min = 0
    salt_split = 38
    salt_max = max(salt)

    fig = plt.figure(dpi=300, facecolor='white')
    axl, axr = fig.subplots(1,2, sharey=True,
                            gridspec_kw={'width_ratios':[75, 25]})
    fig.subplots_adjust(wspace=0.025)
    for ax0 in [axl, axr]:
        ax0.plot(salt, TfrezSoil, linewidth=2)

        #
        # Handling of the splitting between lower (common) and higher
        # (uncommon) salinities
        #
        if ax0 == axl:
            ax0.set_xlim([salt_min, salt_split])
            ax0.set_xlabel('Salinity (g kg$^{-1}$)', horizontalalignment='left')
            ax0.set_ylabel('Freezing temperature (K)')
            ax0.set_title('Soil water freezing temperature')
        elif ax0 == axr:
            ax0.set_xlim([salt_split+0.001, salt_max])

    if plot_suffix:
        module4plots.plotting(fig, plotname, plot_suffix)

# -------------------------------------------------------------------------
def plot_runoff_time(library_name, plot_suffix=None, plotname='runoff_time'):
    '''
    Plot results of the via c-binding call Fortran function compute_runoff_time

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'runoff_time'.

    Returns
    -------
    None.

    '''
    print('* Runoff time-scale')
    elevation_grad = np.linspace(0, 0.15, 151, dtype=float)
    runofftime = mod_physic.compute_runoff_time(elevation_grad, library_name)

    #
    # Figure
    #
    elevation_grad_min = 0
    elevation_grad_split = 0.049
    elevation_grad_max = max(elevation_grad)

    fig = plt.figure(dpi=300, facecolor='white')
    axl, axr = fig.subplots(1,2, sharey=True,
                            gridspec_kw={'width_ratios':[75, 25]})
    fig.subplots_adjust(wspace=0.025)
    for ax0 in [axl, axr]:
        ax0.plot(elevation_grad, runofftime/86400., linewidth=2)
        ax0.plot(elevation_grad, runofftime*0., linewidth=2, color='gray',
                 linestyle='--') # Zero-line

        #
        # Handling of the splitting between lower (common) and higher
        # (uncommon) salinities
        #
        if ax0 == axl:
            ax0.set_xlim([elevation_grad_min, elevation_grad_split])
            ax0.set_xlabel('Surface elevation gradient (m m$^{-1}$)', horizontalalignment='left')
            #ax0.set_ylabel('runoff time-scale (s)')
            ax0.set_ylabel('runoff time-scale (day)')
            ax0.set_title('Runoff time-scale')
        elif ax0 == axr:
            ax0.set_xlim([elevation_grad_split+0.001, elevation_grad_max])

    if plot_suffix:
        module4plots.plotting(fig, plotname, plot_suffix)

# -------------------------------------------------------------------------
#
# Main program
#
if __name__ == '__main__':
    import values_mod_param as physical
    import cbind_mod_physic as mod_physic
    import numpy as np
    import matplotlib.pyplot as plt
    import matplotlib as mpl
    import module4plots

    print('Load physical constant')
    physical.constant()
    # Values
    GRADLW = physical.constant.gradlw
    LAPSE_RATE = physical.constant.lapse_rate
    PATM = physical.constant.patm_surf
    RHO_ICE = physical.constant.rho_ice
    RHO_SNOW = physical.constant.rho_snow
    RHO_FWATER = physical.constant.rho_fwater
    RHO_SEAWATER = physical.constant.rho_seawater
    CDENSITY = physical.constant.Cdens
    FREEZING_TEMP = physical.constant.Tmelt_fw

    #
    # Load the shared library
    #
    LIBRARY_NAME = '../src/CISSEMBEL_CBindings.so' # INCLUDING path, e.g., ./
    print('Shared library containing C-bindings of Fortran code "'
          +LIBRARY_NAME+'"')

    PLOT_SUFFIXES = module4plots.parse_arguments() # = None = ['png', 'pdf']

    #
    # Plots for each function
    #
    plot_relhumid2dewpoint(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_calc_cp_ice(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_thermal_cond_air(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_thermal_cond_snow(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_thermal_cond_ice(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_thermal_cond_snowfirn(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_thermal_cond_water(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_fresh_snow_dens_ant(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_fresh_snow_dens_midlat(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_active_layer_depth(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_densprofile(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_vaporpress(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_rhoair(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_temp_excess2melt(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_sensheatflux(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_latheatflux(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_tpre2snowf_cut(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_tpre2snowf_lin(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_tpre2snowf_tanh(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_sublimation(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_evaporation(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_hctemp(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_hclongwave(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_hcpressure(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_hcprecip_high_desert(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_snowlayers2pressure(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_densification_pressure(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_waterdepth2pressure(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_height2pressure(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_TfrezSeaWater(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_TfrezSoilWater(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_runoff_time(LIBRARY_NAME, PLOT_SUFFIXES)
# -- Last line

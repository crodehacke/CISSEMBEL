#!/usr/bin/env bash
# This routine fetches external source code.
#
# The CISSEMBEL model requires the NetCDF library for writing and
#   reading files. This script aims primarily fetching any additional
#   source code required to use enhanced capabilities.
#
# This script fetches also the required NetCDF libraries; please note
#   that NetCDF4-support requires in addition HDF5 and zlib, while
#   parallel IO needs an Message Passing Library, such as OpenMP or
#   MPICH. Those are not fetch here.
#
# Dependence: git, curl
#
# (c) Christian Rodehacke, 2023, DMI Copenhagen, Denmark
#
# -------------------------------------------------------------------
#
# Copyright (C) 2016-2023 Christian Rodehacke
#
# This file is part of CISSEMBEL, which is the
# == Copenhagen Ice-Snow Surface Energy and Mass Balance modEL ==
#
# CISSEMBEL is free software; you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE  v.1.2
#
# Information about the EUROPEAN UNION PUBLIC LICENCE (*EUPL*) and
# translations into other European languages are available at
# https://eupl.eu (last access: 2021-10-01)](https://eupl.eu/)
#
# We hope that  this  distributed  CISSEMBEL  code, its  tools and
# documentations are usefull. Any improvements of the code, tools,
# and documentations tools are  very welcome.  Please, consider to
# share your thoughts and improvements with the community.
#
# The documentation,  tools,  and the code are provided as it are.
# NO WARRANTY  ABOUT FITNESS  AND USABILITY OF THE CODE, TOOLS, OR
# DOCUMENTATION  ARE  GIVEN.   NO  WARRANTY  IS  GIVEN  ABOUT  THE
# CORRECTNESS  OF COMPUTED RESULTS WITH THE  PROVIDED CODE, TOOLS,
# AND DOCUMENTATION. YOU USE THE CODE, DOCUMENTATION, AND TOOLS AT
# YOUR OWN RISK.           Removing  of  any  copyright  statement
# or  this  disclaimer  is considered  as a  attempt  to defraud.
#
# -------------------------------------------------------------------
set -e

fetch_scrip(){
    libname="SCRIP"
    echo
    echo "Fetching $libname"
    echo "- - - - - - - - - - - - - - - - - - - - - - - - - - -"
    git clone https://github.com/SCRIP-Project/${libname}.git ${libname}
    echo "- - - - - - - - - - - - - - - - - - - - - - - - - - -"
    echo "Done $libname"
    echo
    }


fetch_netcdf_c(){
    libname="netcdf-c"
    echo
    echo "Fetching $libname"
    echo "- - - - - - - - - - - - - - - - - - - - - - - - - - -"
    git clone https://github.com/Unidata/${$libname}.git netcdf
    echo "- - - - - - - - - - - - - - - - - - - - - - - - - - -"
    echo "Done $libname"
    echo
    }

fetch_netcdf_fortran(){
    libname="netcdf-fortran"
    echo
    echo "Fetching $libname"
    echo "- - - - - - - - - - - - - - - - - - - - - - - - - - -"
    git clone https://github.com/Unidata/${libname}.git netcdf
    echo "- - - - - - - - - - - - - - - - - - - - - - - - - - -"
    echo "Done $libname"
    echo
    }


fetch_zlib(){
    libname="zlib"
    echo
    echo "Fetching $libname"
    echo "- - - - - - - - - - - - - - - - - - - - - - - - - - -"
    [ -d $libname ] || mkdir $libname
    source_archive="zlib.tar.gz"
    curl https://zlib.net/current/${source_archive} --output $libname/${source_archive}

    # Unpack
    here=$(pwd)
    cd $libname
    gunzip $source_archive && rm $source_archive
    tar xvf ${source_archive%%.gz} && rm ${source_archive%%.gz}

    # Modify directory structure
    dirname=$(ls -rt1 | tail -n 1)
    if [ 1 -eq 1 ] ; then
	# Have a directory structure like
	#  zlib/zlib -> zlib-1.2
	#  zlib/zlib-1.2
	ln -s $dirname $libname
	cd $here
    else
	# Have a directory structure like:
	#  zlib -> zlib-1.2
	#  zlib-1.2
	mv * ..
	cd $here
	rmdir $libname
	ln -s $dirname $libname
    fi
    echo "- - - - - - - - - - - - - - - - - - - - - - - - - - -"
    echo "Done $libname"
    echo
    }


fetch_hdf5(){
    libname="hdf5"
    echo
    echo "Fetching $libname"
    echo "- - - - - - - - - - - - - - - - - - - - - - - - - - -"
    echo "To obtain the $libname library go to the webpage"
    echo
    echo " https://www.hdfgroup.org/downloads/hdf5/source-code/"
    echo
    echo "and download the tar.gz-version, for instance. Please"
    echo "save the downloaded file in the directory"
    echo "\$CISSEMBEL_ROOT/external/hdf5, which might be"
    echo "$(pwd)/hdf5"
    echo "- - - - - - - - - - - - - - - - - - - - - - - - - - -"
    echo "Done $libname"
    echo
    }



# ----------------------------------------------
echo " This program fetches external sources"
echo " Please consider licences of external sources"


fetch_scrip

fetch_netcdf_c
fetch_netcdf_fortran

fetch_zlib

fetch_hdf5

exit 0

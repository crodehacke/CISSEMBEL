/* Git version control id: $Id$*/

/*
 *
 * -------------------------------------------------------------------
 *
 * Copyright (C) 2016-2025 Christian Rodehacke
 *
 * This file is part of CISSEMBEL, which is the
 * == Copenhagen Ice-Snow Surface Energy and Mass Balance modEL ==
 *
 * CISSEMBEL is free software; you can redistribute it and/or modify
 * it under the terms of the EUROPEAN UNION PUBLIC LICENCE  v.1.2
 *
 * Information about the EUROPEAN UNION PUBLIC LICENCE (*EUPL*) and
 * translations into other European languages are available at
 * https://eupl.eu (last access: 2023-09-18)](https://eupl.eu/)
 *
 * We hope that  this  distributed  CISSEMBEL  code, its  tools and
 * documentations are usefull. Any improvements of the code, tools,
 * and documentations tools are  very welcome.  Please, consider to
 * share your thoughts and improvements with the community.
 *
 * The documentation,  tools,  and the code are provided as it are.
 * NO WARRANTY  ABOUT FITNESS  AND USABILITY OF THE CODE, TOOLS, OR
 * DOCUMENTATION  ARE  GIVEN.   NO  WARRANTY  IS  GIVEN  ABOUT  THE
 * CORRECTNESS  OF COMPUTED RESULTS WITH THE  PROVIDED CODE, TOOLS,
 * AND DOCUMENTATION. YOU USE THE CODE, DOCUMENTATION, AND TOOLS AT
 * YOUR OWN RISK.           Removing  of  any  copyright  statement
 * or  this  disclaimer  is considered  as a  attempt  to defraud.
 *
 * -------------------------------------------------------------------
 */

/*! \file    cppdefs.h
 *  \brief   Preprocessor variables file
 *  \details All pre-processor definitions used across all files are
 *           compiled here.
 *  \warning All Fortran files shall include #include "cppdefs.h" to
 *     ensure that the preprocessor definitions are use
 *     everywhere. The automatically created file mod__env.F08 is an
 *     exception. This file mod__env.F08 do not need to include
 *     cppdefs.h
 */

/* -- General settings controlling various aspects of CISSEMBEL */
#undef  ALLOW_HEIGHT_CLASSES      /* Allow height classes; default: UNDEF */
#undef  DYNAMIC_ZAXIS             /* each grid point has its own depth and vertical size; default: UNDEF */
#define DIMENSION_AXES_INTEGER    /* The arrays along i-, j-, k-, h-axes are integers; default: DEFINE */
#define REPORT_PWD                /* Report current working directory (reads environmental variable PWD) */
#undef  RESTRICT_SMB2ICE_POINTS   /* Restrict SMB calculation to glacier/ice points, where gfraction>0 */
#define WRITE_LAST_TIME_STEP      /* Shall we face to write into output files at the last time step; default: DEFINE */
/* Below: Not yet implemented features (TODO: Future work) */
#define WRITE_MELTING_PROFILE     /* Write melting throughout the entire column (Future); default: UNDEF */
#undef  RUNOFF_PROFILE            /* Report runoff as a profile (Future); default: UNDEF */
#undef  BIOLOGY                   /* Biology on and in the snow (Future); default: UNDEF */
#undef  HORZ_SURFACE_FLOW         /* Topography horizontal flow water of surface water (Future); default: UNDEF */
#undef  WIND_UPLIFT_SNOW          /* Uplift of (dry) snow by wind (Future); default: UNDEF */

/* -- Numerics */
#define PREVENT_DIV0              /* Prevent devision by zero by additional checks; default: DEFINE */
#define PURE_FUNCTIONS            /* Use PURE; requires no print statements in subsequently called functions/subroutines */

/* -- NetCDF related settings */
#undef  NETCDF_NO_PREFILL_MODE    /* Do not prefill newly created files, default: UNDEF */
#undef  COMPRESSION_HORIZON_NCVAR /* If variable compression is requested, compress also horizontal fields */

/* -- OpenMP related settings (multithreding parallelism within a node) */
#undef  OPENMP_ALLOCATE           /* Allocate variables with a horizontal component in an OpenMP environment */
                                  /* for static z-axis=DEFINE or UNDEF; for dynamic z-axis=UNDEF */
#undef  LIMIT_OMP_NUM_THREADS_NML /* Number of OMP threads via namelist instead of env variable; default: undef */

/* -- Message Passing Interface (MPI) settings (across nodes parallelism via MPI library) */
#define MPI_MODULE                /* Load mpi module (use mpi) rather than include mpi.h; default: DEFINE */
#define MPI_IO_PARALLEL           /* Parallel writing in the MPI mode (currently REQUIRED; default: DEFINE) */
#define MPI_WRITE_PAR_COLLECTIVE  /* Parallel collective MPI writing; for performance reasons: default: DEFINE  */
#define MPI_READ_PAR_COLLECTIVE   /* Parallel collective MPI reading; for performance reasons: default: DEFINE  */


/*
 * Settings for specific parts of the code
 */

/* -- primarily in mod_physic.F08 */
#undef  VAPORPRESS_SMOOTH         /* Smooth water vapor pressure between different states; default: UNDEF, DEFINE=unrealistc */
#define CP_ICE_TEMPERATURE_DEPEND /* Specific heat capacity of ice is temperature dependent (TODO:namelist) */
#define CP_ICE_SMOOTH             /* Ensures a smooth water specific heat capacity of ice; default: DEFINE */
#define CP_ICE_ENFORCE_TEMP_RANGE /* If define CP_ICE_TEMPERATURE_DEPEND enforce valid temperature range; default: DEFINE */
#define LATENT_HEAT_POSITIVE_DOWNWARD /* Latent heat flux is positive for downward directed flux; default: DEFINE */

/* -- primarily in mod_physic_column.F08 */
#define CALC_THERMAL_CONDUCT_SNOWICE_FIRN /* Compute the thermal conductivity by complexer form; default: DEFINE */
#define CALC_THERMAL_CONDUCT_WATER /* Compute the conductivity of water; default: DEFINE */
#define USE_N_FUNC_THRESHOLD      /* Restrict the n-value based on grain size threshold; default: DEFINE */
#define USE_M_FUNC_THRESHOLD      /* Restrict the m-value based on grain size threshold; default: DEFINE */
#define USE_BOTTOM_GRAD_AGE       /* For the age inflow at the bottom, apply an age gradient */
#undef  USE_BOTTOM_GRAD_GRAIND    /* For the grain size diameter inflow at the bottom, apply a grain size gradient */
#undef  USE_BOTTOM_GRAD_TEMP      /* For the temperature inflow at the bottom, apply a temperature gradient */
#undef  MAX_FRACTION_MELT2TOTALC  /* Restrict melting to a fraction of the layers content; default: UNDEF */


/* -- primarily in modEBMworld.F08 */
#define USE_XTIME                 /* Use additional event driven timer */
#define REPORT_ALBEDO_IF_UNLIMIT  /* Report unrealistic albedo value if do_restrict_albedo_allowed_min_max=F */

/* -- primarily in modEBMcolumn.F08 */
#undef  ACCUMULATION_CONTAINS_RAINFALL /* The accumulation contains the contribution from rainfall; default: UNDEF */
#define PERCOLATION_INCLUDES_RAINFALL  /* Rainfall contributes to percolation; default: DEFINE; Recommended */
#define PRESS4DENSIFY_SNOW_ICE_WATER   /* Pressure driving densification considers the load of snow, ice, and water; default: DEFINE */

/* -- primarily in mod_static_column.F08 */
#define FIND4XCONT_ALL_SIGN /* If defined, the subroutine find_del4xcont works for all sign conventions; default: UNDEF */


/*
 * Additional settings, which should not be changed in general.
 */
#define USE_FORTRAN_BINDINGS      /* Module "use::iso_fortran_env"; default: DEFINE; UNDEF is deprecated */
#define USE_ISO_FORTRAN_ENV       /* Use intrinsic module iso_fortran_env, requires F2003 or higher; default: DEFINE */
#undef  WRITE_FIELDS_IF_BAD       /* Write special diagnostics if some checks are bad; default: UNDEF */
#define WRITE_FIELDS_ONCE_BAD     /* If DEFINE WRITE_FIELDS_IF_BAD, write continous fiels once bad, default: DEFINE */

/*
 * TODO?: Shall we discard 'PURE_FUNCTIONS' and have
 * only 'USE_PURE_FUNCTIONS', which is set UNDEF if debugging?
 */



/*
 * Perform test if switch combinations consider dependencies
 * =========================================================
 * Usually you do NOT need to change setting below this line
 *
 */

/*
 * Since some future features are not coded yet, we check for
 * currently valid options and raise an error message for not
 * supported options
 */
#ifndef DIMENSION_AXES_INTEGER
**** Only integer axes are currently supported: define DIMENSION_AXES_INTEGER in cppdefs.h ****
STOP
#endif

#ifdef WIND_UPLIFT_SNOW
**** Uplift of (dry) snow by wind is NOT supported yet: undef WIND_UPLIFT_SNOW in cppdefs.h ****
STOP
#endif

#ifdef  HORZ_SURFACE_FLOW
**** Topography horizontal flow water of surface water is NOT supported yet: undef HORZ_SURFACE_FLOW in cppdefs.h ****
STOP
#endif

#ifdef BIOLOGY
**** Biology on and in the snow is NOT supported yet: undef BIOLOGY in cppdefs.h ****
STOP
#endif

#ifndef USE_FORTRAN_BINDINGS
**** It is required as standard: define USE_FORTRAN_BINDINGS in cppdefs.h ****
#define USE_FORTRAN_BINDINGS
#endif

#ifdef USE_C_BINDINGS
/* Use as C-signatures (iso_c_bindings), default UNDEF, python-binding: DEFINE*/
#else
/* Do not use C-signatures , DEFAULT */
#endif

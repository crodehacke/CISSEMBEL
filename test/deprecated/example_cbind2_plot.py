#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 27 11:10:19 CEST 2021


-- Test and plots for mod+physic.F08

@author: Christian Rodehacke (cr), DMI Copenhagen, Denmark

@copyright: Copyright (C) 2016-2025 Christian Rodehacke
"""
# -------------------------------------------------------------------
# This file is part of CISSEMBEL, which is the
# == Copenhagen Ice-Snow Surface Energy and Mass Balance modEL ==
#
# CISSEMBEL is free software; you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE  v.1.2
#
# Information about the EUROPEAN UNION PUBLIC LICENCE (*EUPL*) and
# translations into other European languages are available at
# https://eupl.eu (last access: 2021-10-01)](https://eupl.eu/)
#
# We hope that  this  distributed  CISSEMBEL  code, its  tools and
# documentations are usefull. Any improvements of the code, tools,
# and documentations tools are  very welcome.  Please, consider to
# share your thoughts and improvements with the community.
#
# The documentation,  tools,  and the code are provided as it are.
# NO WARRANTY  ABOUT FITNESS  AND USABILITY OF THE CODE, TOOLS, OR
# DOCUMENTATION  ARE  GIVEN.   NO  WARRANTY  IS  GIVEN  ABOUT  THE
# CORRECTNESS  OF COMPUTED RESULTS WITH THE  PROVIDED CODE, TOOLS,
# AND DOCUMENTATION. YOU USE THE CODE, DOCUMENTATION, AND TOOLS AT
# YOUR OWN RISK.           Removing  of  any  copyright  statement
# or  this  disclaimer  is considered  as a  attempt  to defraud.
#
# -------------------------------------------------------------------

# -----------------------------------------------------------
#
# Main program
#
if __name__ == '__main__':
    import ctypes as ct
    import numpy as np

    print('*** 4 ***')
    fortlib4 = ct.cdll.LoadLibrary('../src/CISSEMBEL_CBindings.so')

    print("Input")
    ilevelsX = 5
    ilevX = ct.c_int(ilevelsX)

    patm = 102400.
    patm_c = ct.c_double(patm)

    Thick1D = np.ones((ilevelsX), order="F")
    Dens1D = np.zeros((ilevelsX), order="F") + 102400.
    Press1D = np.zeros((ilevelsX), order="F")

     # Not necessary but GOOD stype: Help the IDE to identify undefined variables
    Thick1D_ptr = None
    Dens1D_ptr = None
    Press1D_ptr = None
    for var in ['Thick1D', 'Dens1D', 'Press1D']:
        #locals()[var+'_ptr'] = eval(var+' * 100.')
        locals()[var+'_ptr'] = eval(var+'.ctypes.data_as(ct.POINTER(ct.c_double))')
        eval('print(var, '+var+')')

    if False:
        # Not necessary but GOOD stype
        fortlib4.c_snowlayers2pressure.argtypes = \
            [ct.c_int, \
             ct.POINTER(ct.c_double), ct.POINTER(ct.c_double), \
             ct.POINTER(ct.c_double), ct.c_double]

    print('Computation')
    #rint = fortlib4.c_snowlayers2pressure(ilevX, Thick1D_ptr, Dens1D_ptr, Press1D_ptr, ct.c_double(patm))
    rint = fortlib4.c_snowlayers2pressure(ilevX, Thick1D_ptr, Dens1D_ptr, Press1D_ptr, patm_c)

    for var in ['Thick1D', 'Dens1D', 'Press1D']:
        eval('print(var, '+var+')')

    if True:
        import matplotlib.pyplot as plt
        plt.plot(np.cumsum(Thick1D), Press1D)
        plt.xlabel('depth (m)')
        plt.ylabel('Pressure (Pa)')
    print()

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Git version control id: $Id$

Created on Thu Sep 15 15:41:07 2022

-- Compute horizontal grid sizes from longitude and latitude fields

Call example (1):

    ./lonlat2dxdy..py --input  File_with_LonLat_Fields.nc \
                      --output lonlatdxdy.nc \
                      --vname_surface_elevation orography \
                      --vname_lon longitude \
                      --vname_lat latitude


@author: Christian Rodehacke, DMI Copenhagen, Denmark

@copyright: Copyright (C) 2022-2025 Christian Rodehacke

"""
# -------------------------------------------------------------------
# This file is part of CISSEMBEL, which is the
# == Copenhagen Ice-Snow Surface Energy and Mass Balance modEL ==
#
# CISSEMBEL is free software; you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE  v.1.2
#
# Information about the EUROPEAN UNION PUBLIC LICENCE (*EUPL*) and
# translations into other European languages are available at
# https://eupl.eu (last access: 2023-09-18)](https://eupl.eu/)
#
# We hope that  this  distributed  CISSEMBEL  code, its  tools and
# documentations are usefull. Any improvements of the code, tools,
# and documentations tools are  very welcome.  Please, consider to
# share your thoughts and improvements with the community.
#
# The documentation,  tools,  and the code are provided as it are.
# NO WARRANTY  ABOUT FITNESS  AND USABILITY OF THE CODE, TOOLS, OR
# DOCUMENTATION  ARE  GIVEN.   NO  WARRANTY  IS  GIVEN  ABOUT  THE
# CORRECTNESS  OF COMPUTED RESULTS WITH THE  PROVIDED CODE, TOOLS,
# AND DOCUMENTATION. YOU USE THE CODE, DOCUMENTATION, AND TOOLS AT
# YOUR OWN RISK.           Removing  of  any  copyright  statement
# or  this  disclaimer  is considered  as a  attempt  to defraud.
#
# -------------------------------------------------------------------

# -------------------------------------------------------------------------
def parse_arguments():
    '''
    Parse the command line arguments

    Returns
    -------
    list
        List of strings indicating plot formats: ['png', 'pdf'].

    '''
    parser = argparse.ArgumentParser(
        description='Testing Fortran of CISSEMBEL via c-bindings',
        add_help=False)

    parser.add_argument('--input', '-i',
                        nargs='?',
                        required=True,
                        type=str,
                        help='Input file name containing the lon/lat fields')

    parser.add_argument('--output', '-o',
                        nargs='?',
                        required=False,
                        default='lonlat2dxdy.nc',
                        help='Input file name containing the surface elevation')

    parser.add_argument('--vname_lon',
                        nargs='?',
                        required=False,
                        default='lon',
                        help='variable name of the longitude')

    parser.add_argument('--vname_lat',
                        nargs='?',
                        required=False,
                        default='lat',
                        help='variable name of the latitude')

    parser.add_argument('--vname_surface_elevation',
                        nargs='?',
                        required=False,
                        default='surface_elevation',
                        help='variable name of the surface elevation')


    parser.add_argument('--index_lon',
                        required=False,
                        type=int,
                        default=1,
                        help='index of the longitude')

    parser.add_argument('--index_lat',
                        required=False,
                        type=int,
                        default=0,
                        help='index of the latitude')

    parser.add_argument('--vname_dx_field',
                        nargs='?',
                        required=False,
                        default='delta_x',
                        help='OUTPUT variable name of distance along grid dimension "0"')

    parser.add_argument('--vname_dy_field',
                        nargs='?',
                        required=False,
                        default='delta_y',
                        help='OUTPUT variable name of distance along the grid dimension "1"')

    parser.add_argument('--output_format',
                        nargs='?',
                        required=False,
                        default='NETCDF3_CLASSIC',
                        choices=["NETCDF4", "NETCDF4_CLASSIC", "NETCDF3_64BIT", "NETCDF3_CLASSIC"],
                        help='Format of the netcdf output file')


    # # Report used command line arguments
    print("Used settings: read command line arguments or default values")
    for single_arg in vars(parser.parse_args()): #sorted(vars(args)) :
        print('  - {:11s}'.format(single_arg)+' = '+
              str(getattr(parser.parse_args(), single_arg)) )
    print()

    return parser.parse_args()


def open_read_input(filename,
                    vname_longitude='longitude',
                    vname_latitude='latitude',
                    vname_surface_elevation='surface_elevation'):
    '''
    Reads the input file

    Parameters
    ----------
    filename : str
        input file holding the surface elevation, distance in x- and y-direction.
    vname_longitude : str, optional
        Variable name of the longitude field. default is 'longitude'.
    vname_latitude : str, optional
        Variable name of the latitude field. The default is 'latitude'.
    vname_surface_elevation : str, optional
        Variable name of the surface elevation field. The default is 'surface_elevation'.

    Returns
    -------
    lon_ : numpy.ndarray
        longitude field.
    lat_ : numpy.ndarray
        latitude field.
    zs_ : numpy.ndarray
        surface elevation field.
    '''

    # Open the data file
    print("Open input file   '"+filename+"'")

    lonlatfile_ = xr.open_dataset(filename, decode_times=False)

    #  Accessing data fields
    lon_ = lonlatfile_[vname_longitude].squeeze()
    lat_ = lonlatfile_[vname_latitude].squeeze()

    if vname_surface_elevation is None:
        zs_ = np.zeros([lat_.data.size, lon_.data.size])
    else:
        zs_ = lonlatfile_[vname_surface_elevation].squeeze()

    return lon_, lat_, zs_


# -------------------------------------------------------------------------
#
# Main program
#
if __name__ == '__main__':
    import argparse           # argparse : Parse command line arguments
    import numpy as np        # Numpy    : Array handling
    import xarray as xr       # Xarrray  : Easy handling of netcdf files
    from datetime import  datetime # Current date/time
    # from pyproj import Proj   # PyProj   : Transformation between coordinates


    parsed_argv = parse_arguments()

    if parsed_argv.index_lon == parsed_argv.index_lat:
        print("Index of latitude and longitude are identical: FAILURE?")
    else:
        print("Index of longitude direction: "+str(parsed_argv.index_lon))
        print("Index of latitude  direction: "+str(parsed_argv.index_lat))

    lon, lat, zs = open_read_input(parsed_argv.input,
                                   parsed_argv.vname_lon,
                                   parsed_argv.vname_lat,
                                   parsed_argv.vname_surface_elevation)

    #
    # Create 2 dimensional longitude and latitude fields
    #
    if lon.ndim == 1 and lat.ndim == 1:
        # Create 2dim longitude and latitude fields
        lon2d, lat2d = np.meshgrid(lon, lat)
    elif lon.ndim == 2 and lat.ndim == 2:
        # Use existing 2dim longitude and latitude fields
        lon2d = lon
        lat2d = lat

    elif lon.ndim == 2 and lat.ndim == 1:
        # Use 2dim longitude and expand 2dim latitude
        lon2d = lon
        lat2d = np.tile(lat, (lon2d.shape[1],1)).T
    elif lon.ndim == 1 and lat.ndim == 2:
        # Use 2dim latitude and expand 2dim longitude
        lat2d = lat
        lon2d = np.tile(lon, (lat2d.shape[0], 1))

    #
    # Compute gradients of longitude and latitude
    #
    dlon2d = np.gradient(lon2d, axis=parsed_argv.index_lon)
    dlat2d = np.gradient(lat2d, axis=parsed_argv.index_lat)

    #
    # Check results of computed gradients
    #
    if np.all(dlon2d == 0.):
        print("All longitude distances are zero, try a different direction index\n"+
              "  Do NOT use '--index_lon "+str(parsed_argv.index_lon)+"'")
        if parsed_argv.index_lon == 0:
            print("  => Have you tried '--index_lon 1' ?")
        elif parsed_argv.index_lon == 1:
            print("  => Have you tried '--index_lon 0' ?")

    if np.all(dlat2d == 0.):
        print("All latitude distances are zero, try a different direction index\n"+
              "  Do NOT use '--index_lat "+str(parsed_argv.index_lat)+"'")
        if parsed_argv.index_lat == 0:
            print("  => Have you tried '--index_lat 1' ?")
        elif parsed_argv.index_lat == 1:
            print("  => Have you tried '--index_lat 0' ?")


    #
    # Since we are interested in distances (positive definite), we compute
    # the absolute numbers (in case the direction is inverted)
    #
    dlon2d = np.abs(dlon2d)
    dlat2d = np.abs(dlat2d)

    #
    # Compute the distances
    #
    # Conversion number
    distance_per_degree = 111000.0 # meter per degree latitude

    dy = dlat2d*distance_per_degree
    dx = dlon2d*distance_per_degree * np.cos(lat2d*(np.pi*2.0/360.))

    #
    # Create output file
    #

    # axis for output file
    y_axis = [i for i in range(dx.shape[0])]
    x_axis = [i for i in range(dx.shape[1])]


    # Output data set
    output = xr.Dataset(
        data_vars = {
            parsed_argv.vname_dx_field: (["y_axis", "x_axis"], dx,
                                     {'long_name': 'grid distance east-west (x-direction)',
                                      'comment': 'input field index '+str(parsed_argv.index_lat),
                                      'units': 'm'}),
            parsed_argv.vname_dy_field: (["y_axis", "x_axis"], dy,
                                     {'long_name': 'grid distance north-south (y-direction)',
                                      'comment': 'index '+str(parsed_argv.index_lon),
                                      'units': 'm'}),
            "surface_elevation": (["y_axis", "x_axis"], zs.data,
                                  {'long_name': 'surface elevation',
                                   'units': zs.attrs['units'],
                                   'standard_name': 'surface_altitude'}),
            "y_axis": (["y_axis"], y_axis,
                    {'long_name': "Cartesian y-coordinate",
                     'standard_name': 'projectio_y_coordinate',
                     'units': '1',
                     'axis': 'Y'}
                    ),
            "x_axis": (["x_axis"], x_axis,
                    {'long_name': "Cartesian x-coordinate",
                     'standard_name': 'projectio_x_coordinate',
                     'units': '1',
                     'axis': 'X'}),
            },
        coords = {
            "longitude": (["y_axis", "x_axis"], lon2d,
                      {'long_name': lon.attrs['long_name'],
                       'units': lon.attrs['units'],
                       'standard_name': 'longitude'}),
            "latitude": (["y_axis", "x_axis"], lat2d,
                      {'long_name': lat.attrs['long_name'],
                       'units': lat.attrs['units'],
                       'standard_name': 'latitude'}
                       )
            },
        attrs = {"title": "Grid size distances from longitude and latitude",
                 "source_file": parsed_argv.input,
                 "creation_date": str(datetime.now())}
        )

    #
    # Write the data set
    #
    print("Written output file '"+parsed_argv.output+"' as "+parsed_argv.output_format)
    output.to_netcdf(parsed_argv.output, format=parsed_argv.output_format)

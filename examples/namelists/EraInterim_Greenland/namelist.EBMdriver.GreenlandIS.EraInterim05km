! Git version control id: $Id$
!>
!! @file namelist.EBMdriver
!! @brief Namelist parameter "namelist.EBMdriver" file for the CISSEMBEL program
!! @details This namelist parameter file is the reference file, that
!!  (shall) contain the namelist parameters used by the offline
!!  driver/main proggram `EBMdriver.F08`. The namelist parameters for
!!  the CISSEMBEL model are in a separated namelist list.
!!  If you use the python driver or couple CISSEMBEL directly with an
!!  atmosphere model, for instance, the namelist `namelist.EBMdriver`
!!  is NOT used or evaluated.
!!
!! @note Please note that some of the parameters are only used by the
!!  driving offline model (EBMdriver.F08), such as @c timing.
!!
!

!
! -- Settings needed for the offline mode, where the forcing is read
!    from input  files for all setups. The input files are netCDF
!    files. Depending on the settings in EBMdriver.F08 the model can
!    use classical netCDF-3, netCDF-4 and/or compressed netCDF-4.
!

! ---------------------------------------------------------------------
!
! Data for the offline mode driven by EBMdriver.F08
!

&source_domain_size
 xsize = 305,  ! Lateral grid box size in x-direction
 ysize = 541,  ! Lateral grid box size in y-direction
 mx = 8,     ! Number of MPI slices in x-direction (experimental)
 ny = 1,     ! Number of MPI slices in y-direction (experimental)
/

&target_domain_size
 ! NOTE: 1-based index Fortran standard
 xs_tgt = 1, ! Lateral start index of reduced grid in x-direction
 xe_tgt = 1, ! Lateral end index of reduced grid in x-direction
 ys_tgt = 1, ! Lateral start index of reduced grid in y-direction
 ye_tgt = 1, ! Lateral end index of reduced grid in x-direction
/

&offline_time
 dtime       =   21600., ! time step width in seconds (21600sec=6hr)
 iloop_step  =  1,       ! standard: iloop_step=1
 iloop_start =  1,       ! / Number_of_steps =
 iloop_end   =  1,       ! \      iloop_end-iloop_start+1
 flag_datetime_from_input = T, ! Use the time information from the forcing file (default=T)?
/

&offline_driver_ctrl
  flag_nml_offline_time=T,      ! read time (iloop_start, iloop_end) from namelist
  flag_nml_offline_forceinfo=T, ! read forcing file information from namelist
  flag_nml_offline_domain=T,    ! read domain size from namelist
  flag_disp_minmax_read=T,      ! Report min/max values of read forcing
  flag_read_input=T,            ! read input record, false: No climate forcing from file
  flag_read_input_once=F,       ! read all record once, false: one record per time step (standard)
  flag_cut_forcing_field=F,     ! cut a subdomain of the forcing fields
  flag_trigger_monthly_write=F, ! trigger monthly writing of output files
!  flag_regrid_forcing_field=F, ! regrid the forcing fields (not yet implemented)
/

!
! In the offline_force_info section used by EBMdriver.F08
! Please NO spaces around the '%' sign.
!
&offline_force_info
topo_info%filename   = '../data/merged_yearly_05km/merged_yearly_Interim1979.nc',
 topo_info%varname   = 'z',
 topo_info%flag_read = T,
 topo_info%factor    = 1.0e0,
 topo_info%offset    = 0.0e0,
airpress_info%filename   = '../data/merged_yearly_05km/merged_yearly_Interim1979.nc',
 airpress_info%varname   = 'sp',
 airpress_info%flag_read = T,
temp2m_info%filename   = '../data/merged_yearly_05km/merged_yearly_Interim1979.nc',
 temp2m_info%varname   = 't2m',
 temp2m_info%flag_read = T,
dew2m_info%filename  = '../data/merged_yearly_05km/merged_yearly_Interim1979.nc',
 dew2m_info%varname   = 'd2m',
 dew2m_info%flag_read = T,
wind_info%filename  = '../data/merged_yearly_05km/merged_yearly_Interim1979.nc',
 wind_info%varname   = 'wind10',
 wind_info%flag_read = T,
tprecip_info%filename  = '../data/merged_yearly_05km/merged_yearly_Interim1979.nc',
 tprecip_info%varname   = 'tp',
 tprecip_info%flag_read = T,
 tprecip_info%factor    = 1.0e0,
 tprecip_info%offset    = 0.0e0,
snowfall_info%filename   = 'EBMInput.Month00.nc',
 snowfall_info%varname   = 'snowfall',
 snowfall_info%flag_read = F,
rainfall_info%filename   = 'EBMInput.Month00.nc',
 rainfall_info%varname   = 'rain',
 rainfall_info%flag_read = F,
solardown_info%filename   = '../data/merged_yearly_05km/merged_yearly_Interim1979.nc',
 solardown_info%varname   = 'ssrd',
 solardown_info%flag_read = T,
thermaldown_info%filename   = '../data/merged_yearly_05km/merged_yearly_Interim1979.nc',
 thermaldown_info%varname   = 'strd',
 thermaldown_info%flag_read = T,
latentheat_info%filename   = 'EBMInput.Month00.nc',
 latentheat_info%varname   = 'latent',
 latentheat_info%flag_read = F,
evasubli_info%filename   = 'EBMInput.Month00.nc',
 evasubli_info%varname   = 'evasub',
 evasubli_info%flag_read = F,
sensible_info%filename   = 'EBMInput.Month00.nc',
 sensible_info%varname   = 'sens',
 sensible_info%flag_read = F,
evaporat_info%filename   = 'EBMInput.Month00.nc',
 evaporat_info%varname   = 'evapo',
 evaporat_info%flag_read = F,
sublimat_info%filename   = 'EBMInput.Month00.nc',
 sublimat_info%varname   = 'sublim',
 sublimat_info%flag_read = F,
cloudcover_info%filename   = '../data/merged_yearly_05km/merged_yearly_Interim1979.nc',
 cloudcover_info%varname   = 'tcc',
 cloudcover_info%flag_read = T,
icefrac_info%filename   = 'EBMInput.Month00.nc',
 icefrac_info%varname   = 'icefrac',
 icefrac_info%flag_read = F,
lat_info%filename   = 'EBMInput.Month00.nc',
 lat_info%varname   = 'lon',
 lat_info%flag_read = F,
lon_info%filename   = 'EBMInput.Month00.nc',
 lon_info%varname   = 'lat',
 lon_info%flag_read = F,
mask_info%filename   = 'EBMInput.Month00.nc',
 mask_info%varname   = 'mask',
 mask_info%flag_read = F,
/

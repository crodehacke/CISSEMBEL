#!/usr/bin/env bash
set -e
#set -vx

# (c) Christian Rodehacke, 2023, DMI Copenhagen, Denmark
# -------------------------------------------------------------------
# This file is part of CISSEMBEL, which is the
# == Copenhagen Ice-Snow Surface Energy and Mass Balance modEL ==
#
# CISSEMBEL is free software; you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE  v.1.2
#
# Information about the EUROPEAN UNION PUBLIC LICENCE (*EUPL*) and
# translations into other European languages are available at
# https://eupl.eu (last access: 2021-10-01)](https://eupl.eu/)
#
# We hope that  this  distributed  CISSEMBEL  code, its  tools and
# documentations are usefull. Any improvements of the code, tools,
# and documentations tools are  very welcome.  Please, consider to
# share your thoughts and improvements with the community.
#
# The documentation,  tools,  and the code are provided as it are.
# NO WARRANTY  ABOUT FITNESS  AND USABILITY OF THE CODE, TOOLS, OR
# DOCUMENTATION  ARE  GIVEN.   NO  WARRANTY  IS  GIVEN  ABOUT  THE
# CORRECTNESS  OF COMPUTED RESULTS WITH THE  PROVIDED CODE, TOOLS,
# AND DOCUMENTATION. YOU USE THE CODE, DOCUMENTATION, AND TOOLS AT
# YOUR OWN RISK.           Removing  of  any  copyright  statement
# or  this  disclaimer  is considered  as a  attempt  to defraud.
#
# -------------------------------------------------------------------

#
# Example for a loop across various yearly data sets
#
#: for F in ../Data/EraInterim/EraInterim1979_2017.GrIS/ATMOS_????.GrIS.ISMIP6.nc4
#: for F in ~/work/Data/EraInterim/EraInterim1979_2017.GrIS/ATMOS_????.GrIS.ISMIP6.nc4
#: do
#:     ~/src/CISSEMBEL/util/EraInterim/EraInterim2PROMICE_Baptiste.bash $F
#: done

#
# Input files and thier paths: === CHECK THESE SETTINGS ===
#
# -- (preliminary) Initial file
fname_init_promice_src="PROMICE_Baptiste_initial.nc"

# -- forcing file
fname_forcing_src=${1:-"ATMOS_Month00.1979.nc4"}
density=1000 # Density of fresh water (kg m-3)
dt=21600     # Time step between records (seconds)

# -- depth and height axis
#fname_layer_thickness="$HOME/src/CISSEMBEL/examples/data.templates/depth_axis.levels28.cdl"
fname_layer_thickness="$HOME/src/CISSEMBEL/examples/data.templates/depth_axis.levels35.cdl"
fname_height_classes="$HOME/src/CISSEMBEL/examples/data.templates/height_axis.levels13.cdl"

#
# Variable names: === CHECK THESE SETTINGS ===
#
# -- forcing file
if [[ 1 -eq 1 ]] ; then
    # New data
    vname_lon_forcing="longitude"  # vname_lon_forcing="lon"
    vname_lat_forcing="latitude"   # vname_lat_forcing="lat"
    vname_precipitation="tprec"
    vname_radiation_solar="ssrd"
    vname_radiation_thermal="strd"
    vname_elevation_forcing="zs"
    do_compute_total_precipitation_m2kg=0
    do_compute_radiation_dev_time=1
else
    # Old/former data
    vname_lon_forcing="lon"
    vname_lat_forcing="lat"
    vname_precipitation="pr"
    vname_radiation_solar="sradsd"
    vname_radiation_thermal="tradsd"
    vname_elevation_forcing="zs"
    do_compute_total_precipitation_m2kg=1
    do_compute_radiation_dev_time=0
fi
vname_x_forcing="x"
vname_y_forcing="y"

# -- initial file
vname_lon_initial="lon"
vname_lat_initial="lat"
vname_elevation_initial="usrf"
vname_x_initial="x1"
vname_y_initial="y1"

#
# Requested remapping via CDO (Climate Data Operator)
#
CDO_GENremap=genbil  # alternative: CDO_GENremap=gennn

# --------------------------------------------------------
#
# programs/commands
#
# === CHECK THE PATHs of these CISSEMBEL utilies ===
#
COMPUTE_LONLAT_DX_DY=~cr/src/CISSEMBEL/util/lonlat2dxdy.py
COMPUTE_SURFACE_SLOPE=~cr/src/CISSEMBEL/util/grad_surf.py

#
# Command line programs
#
CDO='cdo -s'
NCATTED=ncatted
NCKS=ncks
NCRENAME=ncrename
NCWA=ncwa

NCDUMP=ncdump
NCGEN=ncgen

# --------------------------------------------------------
#
# Correct the initial PROMICE initial file
#
fname_init_promice0="PROMICE_Baptiste_initial.ncwa.nc"
fname_init_promice1="PROMICE_Baptiste_initial.ncwa.nc3"
fname_init_promice2="PROMICE_Baptiste_initial.ncwa.nc3.ncatted.nc"
fname_init_promice3="PROMICE_Baptiste_initial.ncwa.nc3.ncatted.rename.nc"
fname_init_promice_use=$fname_init_promice3

#
# Strip off the time information
#
#$NCWA -a time $fname_init_promice_src $fname_init_promice0 # FAILS
$NCDUMP $fname_init_promice_src | sed '/time/d' | $NCGEN -o $fname_init_promice0 -

#
# Ensure netcdf-3
#
$NCKS --no_abc -3O $fname_init_promice0 $fname_init_promice1

#
# Add needed attributes to PROMICE initial file
#
$NCATTED -O \
	 -a long_name,$vname_lon_initial,c,c,"longitude" \
	 -a long_name,$vname_lat_initial,c,c,"latitude" \
	 -a units,$vname_lon_initial,c,c,"degreesE" \
	 -a units,$vname_lat_initial,c,c,"degressN" \
	 -a long_name,usrf,c,c,"surface elevation" \
	 -a units,usrf,c,c,"m" \
	 -a coordinates,usrf,c,c,"$vname_lon_initial $vname_lat_initial" \
	 -a long_name,surface_slope,c,c,"surface slope" \
	 -a units,surface_slope,c,c,"m m-1" \
	 -a coordinates,surface_slope,c,c,"$vname_lon_initial $vname_lat_initial" \
	 $fname_init_promice1 \
	 $fname_init_promice2

#
# Rename surface_slope (will be replaced below)
#
vname_surface_slope_init="surface_slope"
if [[ 1 -eq 1 ]] ; then
    vname_surface_slope_init="old_surf_slope"
    $NCRENAME -O -v surface_slope,${vname_surface_slope_init} \
	      $fname_init_promice2 \
	      $fname_init_promice3
else
    cp -f $fname_init_promice2 $fname_init_promice3
fi


# --------------------------------------------------------
#
# Grid description of target and weights for source to target
#
fname_griddes="griddes.$(basename $fname_init_promice_use).dat"
fname_weights="weights.${CDO_GENremap:?'Unknown remapping define CDO_GENremap'}.nc"

$CDO griddes -selvar,${vname_surface_slope_init} $fname_init_promice_use > $fname_griddes
$CDO ${CDO_GENremap},$fname_griddes $fname_forcing_src $fname_weights

# --------------------------------------------------------
#
# Create auxillary fields for the initial data file
#
fname_surface_slope="slope.$(basename $fname_forcing_src)"
fname_lonlat_dxdy_forcing="lonlat2dxdy.$(basename $fname_forcing_src)"

if [[ 1 -eq 1 ]] ; then
    #
    # Determine grid box sizes
    #
    echo "* Create $fname_lonlat_dxdy_forcing"
    echo "  Run $COMPUTE_LONLAT_DX_DY"
    echo "  >>-------------------------------------------------"
    $COMPUTE_LONLAT_DX_DY --input $fname_forcing_src \
			  --output $fname_lonlat_dxdy_forcing \
			  --vname_surface $vname_elevation_forcing \
			  --vname_lon $vname_lon_forcing \
			  --vname_lat $vname_lat_forcing
    echo "  <<-------------------------------------------------"

    #
    # Compute the actual slope
    #
    echo "* Create $fname_surface_slope"
    echo "  Run $COMPUTE_SURFACE_SLOPE"
    echo "  >>-------------------------------------------------"
    $COMPUTE_SURFACE_SLOPE --input $fname_lonlat_dxdy_forcing \
			   --output ${fname_surface_slope}.domain \
			   --vname_input_axis0 x_axis \
			   --vname_input_axis1 y_axis \
			   --vname_input_field_dx delta_x \
			   --vname_input_field_dy delta_y \
			   --vname_input_lon $vname_lon_forcing \
			   --vname_input_lat $vname_lat_forcing \
			   --vname_input_surface surface_elevation
    echo "  <<-------------------------------------------------"
    echo " Remap (interpolate) the slope to target grid"
    $CDO -f nc2 remap,$fname_griddes,$fname_weights \
	 ${fname_surface_slope}.domain \
	 ${fname_surface_slope}
fi

# --------------------------------------------------------
#
# Work on new forcing data
#
fname_output_forcing=Forcing.${fname_init_promice_src}.$(basename $fname_forcing_src) # step1
fname_output_forcing=${fname_output_forcing%%.nc4} # step2
fname_output_forcing=${fname_output_forcing%%.nc}  # step3
fname_output_forcing=${fname_output_forcing}.nc    # step4
fname_output_forcing=$(echo ${fname_output_forcing} | sed 's/\_initial//g')    # step5 (final)

echo "* Create forcing $fname_output_forcing"

#
# Ensure that the attribute "coordinate" is defined
#
fname_output_attr_forcing=$(basename ${fname_output_forcing}.attr_coordinates)
if [[ 1 -eq 1 ]] ; then
    echo -n "  Attribute 'coordinates'"
    $NCATTED -O \
	     -a coordinates,,c,c,"$vname_lon_forcing $vname_lat_forcing" \
	     -a coordinates,time,d,, \
	     -a coordinates,${vname_lon_forcing},d,, \
	     -a coordinates,${vname_lat_forcing},d,, \
	     $fname_forcing_src \
	     $fname_output_attr_forcing
else
    echo -n "  Expect attribute 'coordinates'"
    cp -ip $fname_forcing_src ${fname_output_forcing}.attr_coordinates
fi

#
# Regrid EraInterim to promice locations
#
echo -n " Remap"
$CDO -f nc2 remap,$fname_griddes,$fname_weights \
     $fname_output_attr_forcing \
     $fname_output_forcing \
     && rm $fname_output_attr_forcing


#
# Correct fields and units of remapped/regridded ERAInterim data
#
if [[ $do_compute_total_precipitation_m2kg -gt 0 ]] ; then
    echo -n " compute:percipitation"
    #
    # Precipitation
    #
    # Original unit: Meter (Water equivalent),
    #                with assumed water density of 1000 kg m-3
    #                and a time step of dt
    # Required unit: kg m-2 s-1
    #
    # tprec[kg m-2 s-1] = tprec[m]*density[kg m-3]*(1/dt)[s-1]
    #

    # Calculate corrected precipitation
    $CDO -expr,"$vname_precipitation=${vname_precipitation}*${density}/${dt};" \
	 -selvar,$vname_precipitation \
	 $fname_output_forcing \
	 $fname_output_forcing.$vname_precipitation

    # Adjust the unit of the precipitation
    $NCATTED -a units,${vname_precipitation},o,c,'kg m-2 s-1' \
	     $fname_output_forcing.$vname_precipitation

    # Overwrite/Replace (append) the new precipitation in the output file
    $NCKS --no_abc -A \
	  $fname_output_forcing.$vname_precipitation \
	  $fname_output_forcing &>/dev/null
fi

if [[ $do_compute_radiation_dev_time -gt 0 ]] ; then
    echo -n " compute"
    #
    # Solar and thermal radiation downward at surface
    #
    # Original unit: Joule Meter-2
    #                with a time step of dt
    # Required unit: Watt Meter-2 = Joule Second-1 Meter-2
    #
    # ssrd[W m-2] = ssrd[J m-2]*(1/dt)[s-1]
    # strd[W m-2] = strd[J m-2]*(1/dt)[s-1]
    #

    for vname in \
	${vname_radiation_thermal:?"Missing var name for thermal rad"} \
	${vname_radiation_solar:?"Missing var name for solar rad"}
    do
	echo -n ":${vname}"
	# Calculate corrected radiation
	$CDO -expr,"$vname=${vname}/${dt};" \
	     -selvar,$vname \
	     $fname_output_forcing \
	     $fname_output_forcing.$vname

	# Adjust the unit of the precipitation
	$NCATTED -a units,${vname},o,c,'W m-2' \
		 $fname_output_forcing.$vname

	# Overwrite/Replace (append) the new radiation in the output
	# file
	$NCKS --no_abc -A \
	      $fname_output_forcing.$vname \
	      $fname_output_forcing  &>/dev/null
    done
fi

#
# Correct further units to make the comply common conventions
#
$NCATTED \
    -a units,${vname_radiation_solar},o,c,'W m-2' \
    -a units,${vname_radiation_thermal},o,c,'W m-2' \
    -a units,wind10,o,c,'m s-1' \
    -a units,${vname_elevation_forcing},o,c,'m' \
    $fname_output_forcing
#todo:del:

#
# Add horizontal coordinate variables (horizontal axis)
#
echo -n " Grid size:"
#OK:x_size=$(ncdump -h $fname_output_forcing | grep "$vname_x_forcing = " | head -n 1 | cut -d= -f2 | tr -d \;)
x_size=$(ncdump -h $fname_output_forcing | grep "$vname_x_forcing = " | head -n 1 | tr -d '[:punct:][:alpha:][:space:]' )
y_size=$(ncdump -h $fname_output_forcing | grep "$vname_y_forcing = " | head -n 1 | tr -d '[:punct:][:alpha:][:space:]' )

echo " $vname_x_forcing = $x_size and $vname_y_forcing = $y_size"

fname_axis=axis__${fname_output_forcing}
cat > ${fname_axis}.cdl <<EOF_cdl
netcdf axis {
dimensions:
	${vname_x_forcing} = ${x_size} ;
	${vname_y_forcing} = ${y_size} ;
variables:
	int ${vname_x_forcing}(${vname_x_forcing}) ;
		${vname_x_forcing}:long_name = "Cartesian x-coordinate" ;
		${vname_x_forcing}:standard_name = "projection_x_coordinate" ;
		${vname_x_forcing}:units = "1" ;
		${vname_x_forcing}:axis = "X" ;
	int ${vname_y_forcing}(${vname_y_forcing}) ;
		${vname_y_forcing}:long_name = "Cartesian y-coordinate" ;
		${vname_y_forcing}:standard_name = "projection_y_coordinate" ;
		${vname_y_forcing}:units = "1" ;
		${vname_y_forcing}:axis = "Y" ;
// global attributes:
   :creation_date = "$(date)";
   :source_file = "${fname_output_forcing}" ;
   :history = "Tell me about it" ;

data:
	${vname_x_forcing} = $(seq -s, ${x_size}) ;
	${vname_y_forcing} = $(seq -s, ${y_size}) ;
}
EOF_cdl

$NCGEN -o ${fname_axis}.nc ${fname_axis}.cdl

#
# Add the axis
#
$NCKS --no_abc -A ${fname_axis}.nc ${fname_output_forcing} &>/dev/null

# --------------------------------------------------------
#
# Build an updated initial file
#
fname_output_initial="PROMICE_Baptiste_initial.$(basename $fname_forcing_src)"  # step1
fname_output_initial=${fname_output_initial%%.nc4} # step2
fname_output_initial=${fname_output_initial%%.nc}  # step3
fname_output_initial=${fname_output_initial}.nc    # step4 (final)

echo "* Create new initial $fname_output_initial"
test -f $fname_output_initial && rm -f $fname_output_initial

#
# Start with adjusted field without "surface_slope"
#
if [ $fname_output_initial -nt $fname_surface_slope ] ; then
    # File without variable 'surface_slope', which will be added below
    $NCKS --no_abc -3 -O -xv surface_slope $fname_init_promice_use $fname_output_initial
else
    $NCKS --no_abc -3 -O $fname_init_promice_src $fname_output_initial
fi

#
# Add actual surface elevation
#
echo "  Add surface elevation"
$NCKS --no_abc -A -v $vname_elevation_forcing \
      $fname_output_forcing \
      $fname_output_initial &> /dev/null

#
# Add updated surface slope
#
if [ $fname_output_initial -nt $fname_surface_slope ] ; then
    echo "  Add surface slope"
    #$NCRENAME -d x_axis,x -d y_axis,y $fname_surface_slope
    $NCKS --no_abc -Av surface_slope \
	  $fname_surface_slope $fname_output_initial &>/dev/null
fi

#
# Rename the surface elevation variable if requested
#
if [[ "Xsurface_elevation" != "X${vname_elevation_initial}" ]]
then
    echo "  Rename the surface elevation variable"
    $NCRENAME -v ${vname_elevation_forcing},zs_atm \
	      $fname_output_initial
else
    vname_elevation_initial=${vname_elevation_forcing}
fi

#
# Add layer thickness
#
if [[ 1 -eq 1 ]] ; then
    echo "  Add layer thickness $fname_layer_thickness"
    suffix=${fname_layer_thickness##*.}

    #
    # Get ridge of the kaxis dimension and dependend variables
    #
    $NCWA -a kaxis $fname_output_initial ${fname_output_initial}.tmp
    $NCKS --no_abc -O -C -xv depth,layer_thickness,kaxis,time,times \
	  ${fname_output_initial}.tmp \
	  $fname_output_initial \
	&& rm ${fname_output_initial}.tmp

    #
    # Add the depth dimension `kaxis` and the variable `kaxis`,
    # `layer_thickness`, and `depth`.
    #
    if [[ "${suffix}" == "cdl" ]] ; then
	fname=$(basename ${fname_layer_thickness%%.cdl}).$$.nc
	$NCGEN -o $fname $fname_layer_thickness
	$NCKS --no_abc -A $fname $fname_output_initial &>/dev/null \
	    && rm $fname
    else
	$NCKS --no_abc -A \
	      $fname_layer_thickness $fname_output_initial &>/dev/null
    fi
fi

#
# Add height (class) levels
#
if [[ 1 -eq 1 ]] ; then
    echo "  Add layer thickness $fname_height_classes"
    suffix=${fname_height_classes##*.}

    #
    # Get ridge of the haxis dimension and dependend variables
    #
    $NCWA -a haxis $fname_output_initial ${fname_output_initial}.tmp
    $NCKS --no_abc -O -C -xv height,haxis,time,times \
	  ${fname_output_initial}.tmp \
	  $fname_output_initial \
	&& rm ${fname_output_initial}.tmp

    #
    # Add the height dimension `haxis` and the variable `haxis`,
    # `height`.
    #
    if [[ "${suffix}" == "cdl" ]] ; then
	fname=$(basename ${fname_height_classes%%.cdl}).$$.nc
	$NCGEN -o $fname $fname_height_classes
	$NCKS --no_abc -A $fname $fname_output_initial  &>/dev/null \
	    && rm $fname
    else
	$NCKS --no_abc -A \
	      $fname_height_classes $fname_output_initial &>/dev/null
    fi
fi

#
# Correct attributes
#
echo "  Adjust attributes (1)"
$NCATTED \
    -a long_name,$vname_elevation_initial,c,c,"surface elevation" \
    -a standard_name,$vname_elevation_initial,c,c,"surface_altitude" \
    -a long_name,kaxis,c,c,"z-layer of mesh grid" \
    -a positive,kaxis,c,c,"down" \
    -a axis,kaxis,c,c,"Z" \
    -a standard_name,kaxis,c,c,"model_layer_number" \
    -a long_name,stationID,c,c,"stationID" \
    -a coordinates,stationID,c,c,"$vname_lon_initial $vname_lat_initial" \
    -a long_name,surface_slope,c,c,"surface slope" \
    -a coordinates,surface_slope,c,c,"$vname_lon_initial $vname_lat_initial" \
    -a units,surface_slope,c,c,"m m-1" \
    -a long_name,layer_thickness,c,c,"layer thickness" \
    -a units,layer_thickness,c,c,"m" \
    -a long_name,${vname_x_initial},c,c,"Cartesian x-coordinate" \
    -a standard_name,${vname_x_initial},c,c,"projection_x_coordinate" \
    -a axis,${vname_x_initial},c,c,"X" \
    -a long_name,${vname_y_initial},c,c,"Cartesian y-coordinate" \
    -a standard_name,${vname_y_initial},c,c,"projection_y_coordinate" \
    -a axis,${vname_y_initial},c,c,"Y" \
    $fname_output_initial

echo "  Adjust attributes (2)"
$NCATTED \
    -a long_name,haxis,c,c,"height class elevation levels" \
    -a standard_name,haxis,c,c,"level_number" \
    -a positive,haxis,c,c,"up" \
    $fname_output_initial

#
# Create an classical netcdf file
#
if [[ 1 -eq 0 ]] ; then
    #
    # NOT needed anymore, because we use above classical and 64-bit
    # classical netcdf-files
    #
    fname_output_initial_nc3=${fname_output_initial%%.nc4}.nc
    if [[ ${fname_output_initial%%.nc4} != ${fname_output_initial} ]] ; then
	echo "  Classical NetCDF $out"
	$NCKS --no_abc -O3 ${fname_output_initial} ${fname_output_initial_nc3}
    fi
fi

#
# Rename the coordinate variables (horizontal axes)
#
if [[ 1 -eq 0 ]] ; then
    #
    # NOT needed any more
    #
    echo "  Rename dimension variables"
    for FILE in ${fname_output_initial} ${fname_output_initial_nc3}
    do
	if [[ -f $FILE ]] ; then
	    echo "   - $FILE"
	    mv ${FILE} ${FILE}.tmp
	    $NCRENAME -O -v $vname_x_initial,x -v $vname_y_initial,y \
		      ${FILE}.tmp ${FILE} \
		      && rm ${FILE}.tmp
	fi
    done
    vname_x_initial=x
    vname_y_initial=y
fi

# --------------------------------------------------------
#
# NetCDF-4 versions
#
if [[ 1 -eq 0 ]] ; then
    echo "* NetCDF4 versions"
    for file in  ${fname_output_forcing} ${fname_output_initial}
    do
	if [[ -f $file ]] ; then
	    NC4file=$(dirname $file)/$(basename $file)4
	    echo "   - $NC4file"
	    $NCKS --no_abc -O -7 -L 3 $file $NC4file
	fi
    done
else
    echo "* SKIP NetCDF4 versions"
fi
# --------------------------------------------------------
#
# Clean up
#
if [[ 1 -eq 1 ]] ; then
    echo "* Clean up"
    for file in lon.nc lat.nc \
	$fname_init_promice0 $fname_init_promice1 \
	$fname_init_promice2 $fname_init_promice3 \
	$fname_init_promice_use \
	$fname_griddes $fname_weights \
	$fname_surface_slope ${fname_surface_slope}.domain \
	$fname_lonlat_dxdy_forcing $fname_grid_area \
	$fname_grid_weights ${fname_axis}.nc ${fname_axis}.cdl \
	$fname_output_forcing.$vname_precipitation \
	$fname_output_forcing.$vname_radiation_solar \
	$fname_output_forcing.$vname_radiation_thermal

    do
	if [[ -f $file ]] ; then
	    echo "  - rm $file"
	    rm $file
	fi
    done
fi

# --------------------------------------------------------
#
# Show what has been created
#
echo "* Created final files"
for file in ${fname_output_initial} ${fname_output_initial_nc3} \
				    $fname_output_forcing
do
    if [[ -f ${file} ]] ; then
	echo "  - ${file}"
    fi
        if [[ -f ${file}4 ]] ; then
	echo "  - ${file}4"
    fi
done

echo "Bye bye ..."
exit 0

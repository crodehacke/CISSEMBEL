#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 27 11:10:19 CEST 2021


-- Test and plots for mod_solver.F08

@author: Christian Rodehacke, DMI Copenhagen, Denmark

@copyright: Copyright (C) 2016-2025 Christian Rodehacke
"""
# -------------------------------------------------------------------
# This file is part of CISSEMBEL, which is the
# == Copenhagen Ice-Snow Surface Energy and Mass Balance modEL ==
#
# CISSEMBEL is free software; you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE  v.1.2
#
# Information about the EUROPEAN UNION PUBLIC LICENCE (*EUPL*) and
# translations into other European languages are available at
# https://eupl.eu (last access: 2023-09-18)](https://eupl.eu/)
#
# We hope that  this  distributed  CISSEMBEL  code, its  tools and
# documentations are usefull. Any improvements of the code, tools,
# and documentations tools are  very welcome.  Please, consider to
# share your thoughts and improvements with the community.
#
# The documentation,  tools,  and the code are provided as it are.
# NO WARRANTY  ABOUT FITNESS  AND USABILITY OF THE CODE, TOOLS, OR
# DOCUMENTATION  ARE  GIVEN.   NO  WARRANTY  IS  GIVEN  ABOUT  THE
# CORRECTNESS  OF COMPUTED RESULTS WITH THE  PROVIDED CODE, TOOLS,
# AND DOCUMENTATION. YOU USE THE CODE, DOCUMENTATION, AND TOOLS AT
# YOUR OWN RISK.           Removing  of  any  copyright  statement
# or  this  disclaimer  is considered  as a  attempt  to defraud.
#
# -------------------------------------------------------------------

def func1(x):
    '''
    Test function 1; copy from mod_solver.F08 of mod_solver::func1

    Parameters
    ----------
    x : float or numypy.ndarray
        variable value

    Returns
    -------
    function value at x
    '''
    return np.power(x, 3.0)+2.0*x-5.0

def func2(x):
    '''
    Test function 2; copy from mod_solver.F08 of mod_solver::func2

    Parameters
    ----------
    x : float or numypy.ndarray
        variable value

    Returns
    -------
    function value at x
    '''
    return np.power(x, 3)+5.0*np.power(x, 2.0)+5.0*(x-3.0)+4.0

def func3(x):
    '''
    Test function 3; copy from mod_solver.F08 of mod_solver::func3

    Parameters
    ----------
    x : float or numypy.ndarray
        variable value

    Returns
    -------
    function value at x
    '''
    return 0.75*np.exp(x-7)-np.power(x+9, 1.1)+np.power(x, 1.5)-5.0

def func4(x):
    '''
    Test function 4; copy from mod_solver.F08 of mod_solver::func4

    Parameters
    ----------
    x : float or numypy.ndarray
        variable value

    Returns
    -------
    function value at x
    '''
    return np.cos(0.75*np.exp(x-7))-np.power(x+9, 1.1)+np.power(x, 1.5)-5.0


# -------------------------------------------------------------------------
def plot_funcroot_newton(library_name, plot_suffix=None,
                         plotname='funcroot_newton'):
    '''
    Plot results of the via c-binding call Fortran function funcroot_newton

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'funcroot_newton'.

    Returns
    -------
    None.

    '''
    print('* Newton solver method to find the root')
    root_values_targets = np.zeros(4,)
    root_values_targets[0] = 1.328268855668608
    root_values_targets[1] = 1
    root_values_targets[2] = 8.52477
    root_values_targets[3] = 9.6780

    root_values_newton = np.zeros_like(root_values_targets)

    root_values_newton[0] = mod_solver.funcroot_newton(-10, 10, 25, 1)
    root_values_newton[1] = mod_solver.funcroot_newton(-10, 10, 25, 2)

    root_values_newton[2] = mod_solver.funcroot_newton(0, 15, 25, 3)
    root_values_newton[3] = mod_solver.funcroot_newton(0, 15, 25, 4)

    x_func = np.zeros((1000, 4))
    x_func[:, 0] = np.linspace(-10, 10, 1000)
    x_func[:, 1] = np.linspace(-10, 10, 1000)

    x_func[:, 2] = np.linspace(0, 15, 1000)
    x_func[:, 3] = np.linspace(0, 15, 1000)


    def subplot1(x, y, ax, string):
        xp = np.where(y>0, x, np.NaN)
        yp = np.where(y>0, y, np.NaN)

        ax.plot(x, y*0.0, '--', c='gray')
        ax.plot(x, y, label=string)

        # Positive values
        xp = np.where(y>0, x, np.NaN)
        yp = np.where(y>0, y, np.NaN)
        ax.plot(xp, yp, 'b:')

        # Negative values
        xp = np.where(y<0, x, np.NaN)
        yp = np.where(y<0, y, np.NaN)
        ax.plot(xp, yp, 'c:')

    def subplot_target(target, iter_value, ax):
        # ax.plot(target, 0, 'xr', label='target')
        if ( np.isnan(iter_value) ):
            ax.plot(target, 0, 'xm', label='target (missed)')
        else:
            ax.plot(target, 0, 'xr', label='target')
            ax.plot(iter_value, 0, 'og', label='found') #, facecolors='none')


    #
    # Figure
    #
    fig = plt.figure(dpi=300, facecolor='white')
    ax0 = fig.subplots(2, 2)
    fig.subplots_adjust(wspace=0.14)

    subplot1(x_func[:, 0], func1(x_func[:, 0]), ax0[0, 0], 'funct 1')
    subplot1(x_func[:, 1], func2(x_func[:, 1]), ax0[1, 0], 'funct 2')
    subplot1(x_func[:, 2], func3(x_func[:, 2]), ax0[0, 1], 'funct 3')
    subplot1(x_func[:, 3], func4(x_func[:, 3]), ax0[1, 1], 'funct 4')

    subplot_target(root_values_targets[0], root_values_newton[0], ax0[0, 0])
    subplot_target(root_values_targets[1], root_values_newton[1], ax0[1, 0])
    subplot_target(root_values_targets[2], root_values_newton[2], ax0[0, 1])
    subplot_target(root_values_targets[3], root_values_newton[3], ax0[1, 1])


    ax0[0, 0].legend()
    ax0[1, 0].legend()
    ax0[0, 1].legend()
    ax0[1, 1].legend()

    fig.suptitle("Newton-Raphson Method")

    print(root_values_targets)
    print(root_values_newton)

    if plot_suffix:
        module4plots.plotting(fig, plotname, plot_suffix)


# -------------------------------------------------------------------------
def plot_funcroot_bisection(library_name, plot_suffix=None,
                            plotname='funcroot_bisection'):
    '''
    Plot results of the via c-binding call Fortran function funcroot_bisection

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'funcroot_bisection'.

    Returns
    -------
    None.

    '''
    print('* Bisection solver method to find the root')
    root_values_targets = np.zeros(4,)
    root_values_targets[0] = 1.328268855668608
    root_values_targets[1] = 1
    root_values_targets[2] = 8.52477
    root_values_targets[3] = 9.6780

    root_values_bisection = np.zeros_like(root_values_targets)

    root_values_bisection[0] = mod_solver.funcroot_bisection(-10, 10, 25, 1)
    root_values_bisection[1] = mod_solver.funcroot_bisection(-10, 10, 25, 2)

    root_values_bisection[2] = mod_solver.funcroot_bisection(0, 15, 25, 3)
    root_values_bisection[3] = mod_solver.funcroot_bisection(0, 15, 25, 4)

    x_func = np.zeros((1000, 4))
    x_func[:, 0] = np.linspace(-10, 10, 1000)
    x_func[:, 1] = np.linspace(-10, 10, 1000)

    x_func[:, 2] = np.linspace(0, 15, 1000)
    x_func[:, 3] = np.linspace(0, 15, 1000)


    def subplot1(x, y, ax, string):
        xp = np.where(y>0, x, np.NaN)
        yp = np.where(y>0, y, np.NaN)

        ax.plot(x, y*0.0, '--', c='gray')
        ax.plot(x, y, label=string)

        # Positive values
        xp = np.where(y>0, x, np.NaN)
        yp = np.where(y>0, y, np.NaN)
        ax.plot(xp, yp, 'b:')

        # Negative values
        xp = np.where(y<0, x, np.NaN)
        yp = np.where(y<0, y, np.NaN)
        ax.plot(xp, yp, 'c:')

    def subplot_target(target, iter_value, ax):
        # ax.plot(target, 0, 'xr', label='target')
        if ( np.isnan(iter_value) ):
            ax.plot(target, 0, 'xm', label='target (missed)')
        else:
            ax.plot(target, 0, 'xr', label='target')
            ax.plot(iter_value, 0, 'og', label='found') #, facecolors='none')


    #
    # Figure
    #
    fig = plt.figure(dpi=300, facecolor='white')
    ax0 = fig.subplots(2, 2)
    fig.subplots_adjust(wspace=0.14)

    subplot1(x_func[:, 0], func1(x_func[:, 0]), ax0[0, 0], 'funct 1')
    subplot1(x_func[:, 1], func2(x_func[:, 1]), ax0[1, 0], 'funct 2')
    subplot1(x_func[:, 2], func3(x_func[:, 2]), ax0[0, 1], 'funct 3')
    subplot1(x_func[:, 3], func4(x_func[:, 3]), ax0[1, 1], 'funct 4')

    subplot_target(root_values_targets[0], root_values_bisection[0], ax0[0, 0])
    subplot_target(root_values_targets[1], root_values_bisection[1], ax0[1, 0])
    subplot_target(root_values_targets[2], root_values_bisection[2], ax0[0, 1])
    subplot_target(root_values_targets[3], root_values_bisection[3], ax0[1, 1])


    ax0[0, 0].legend()
    ax0[1, 0].legend()
    ax0[0, 1].legend()
    ax0[1, 1].legend()

    fig.suptitle("Bisection Method")

    print(root_values_targets)
    print(root_values_bisection)

    if plot_suffix:
        module4plots.plotting(fig, plotname, plot_suffix)

# -------------------------------------------------------------------------
def plot_funcroot_secant(library_name, plot_suffix=None,
                         plotname='funcroot_secant'):
    '''
    Plot results of the via c-binding call Fortran function funcroot_secant

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'funcroot_secant'.

    Returns
    -------
    None.

    '''
    print('* Secant solver method to find the root')
    root_values_targets = np.zeros(4,)
    root_values_targets[0] = 1.328268855668608
    root_values_targets[1] = 1
    root_values_targets[2] = 8.52477
    root_values_targets[3] = 9.6780

    root_values_secant = np.zeros_like(root_values_targets)

    root_values_secant[0] = mod_solver.funcroot_secant(-10, 10, 25, 1)
    root_values_secant[1] = mod_solver.funcroot_secant(-10, 10, 25, 2)

    root_values_secant[2] = mod_solver.funcroot_secant(0, 15, 25, 3)
    root_values_secant[3] = mod_solver.funcroot_secant(0, 15, 25, 4)

    x_func = np.zeros((1000, 4))
    x_func[:, 0] = np.linspace(-10, 10, 1000)
    x_func[:, 1] = np.linspace(-10, 10, 1000)

    x_func[:, 2] = np.linspace(0, 15, 1000)
    x_func[:, 3] = np.linspace(0, 15, 1000)


    def subplot1(x, y, ax, string):
        xp = np.where(y>0, x, np.NaN)
        yp = np.where(y>0, y, np.NaN)

        ax.plot(x, y*0.0, '--', c='gray')
        ax.plot(x, y, label=string)

        # Positive values
        xp = np.where(y>0, x, np.NaN)
        yp = np.where(y>0, y, np.NaN)
        ax.plot(xp, yp, 'b:')

        # Negative values
        xp = np.where(y<0, x, np.NaN)
        yp = np.where(y<0, y, np.NaN)
        ax.plot(xp, yp, 'c:')

    def subplot_target(target, iter_value, ax):
        # ax.plot(target, 0, 'xr', label='target')
        if ( np.isnan(iter_value) ):
            ax.plot(target, 0, 'xm', label='target (missed)')
        else:
            ax.plot(target, 0, 'xr', label='target')
            ax.plot(iter_value, 0, 'og', label='found') #, facecolors='none')


    #
    # Figure
    #
    fig = plt.figure(dpi=300, facecolor='white')
    ax0 = fig.subplots(2, 2)
    fig.subplots_adjust(wspace=0.14)

    subplot1(x_func[:, 0], func1(x_func[:, 0]), ax0[0, 0], 'funct 1')
    subplot1(x_func[:, 1], func2(x_func[:, 1]), ax0[1, 0], 'funct 2')
    subplot1(x_func[:, 2], func3(x_func[:, 2]), ax0[0, 1], 'funct 3')
    subplot1(x_func[:, 3], func4(x_func[:, 3]), ax0[1, 1], 'funct 4')

    subplot_target(root_values_targets[0], root_values_secant[0], ax0[0, 0])
    subplot_target(root_values_targets[1], root_values_secant[1], ax0[1, 0])
    subplot_target(root_values_targets[2], root_values_secant[2], ax0[0, 1])
    subplot_target(root_values_targets[3], root_values_secant[3], ax0[1, 1])


    ax0[0, 0].legend()
    ax0[1, 0].legend()
    ax0[0, 1].legend()
    ax0[1, 1].legend()

    fig.suptitle("Secant Method")

    print(root_values_targets)
    print(root_values_secant)

    if plot_suffix:
        module4plots.plotting(fig, plotname, plot_suffix)

# -------------------------------------------------------------------------
def plot_funcroot_brent(library_name, plot_suffix=None,
                        plotname='funcroot_brent'):
    '''
    Plot results of the via c-binding call Fortran function funcroot_brent

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'funcroot_brent'.

    Returns
    -------
    None.

    '''
    print('* brent solver method to find the root')
    root_values_targets = np.zeros(4,)
    root_values_targets[0] = 1.328268855668608
    root_values_targets[1] = 1
    root_values_targets[2] = 8.52477
    root_values_targets[3] = 9.6780

    root_values_brent = np.zeros_like(root_values_targets)

    root_values_brent[0] = mod_solver.funcroot_brent(-10, 10, 25, 1)
    root_values_brent[1] = mod_solver.funcroot_brent(-10, 10, 25, 2)

    root_values_brent[2] = mod_solver.funcroot_brent(0, 15, 25, 3)
    root_values_brent[3] = mod_solver.funcroot_brent(0, 15, 25, 4)

    x_func = np.zeros((1000, 4))
    x_func[:, 0] = np.linspace(-10, 10, 1000)
    x_func[:, 1] = np.linspace(-10, 10, 1000)

    x_func[:, 2] = np.linspace(0, 15, 1000)
    x_func[:, 3] = np.linspace(0, 15, 1000)


    def subplot1(x, y, ax, string):
        xp = np.where(y>0, x, np.NaN)
        yp = np.where(y>0, y, np.NaN)

        ax.plot(x, y*0.0, '--', c='gray')
        ax.plot(x, y, label=string)

        # Positive values
        xp = np.where(y>0, x, np.NaN)
        yp = np.where(y>0, y, np.NaN)
        ax.plot(xp, yp, 'b:')

        # Negative values
        xp = np.where(y<0, x, np.NaN)
        yp = np.where(y<0, y, np.NaN)
        ax.plot(xp, yp, 'c:')

    def subplot_target(target, iter_value, ax):
        # ax.plot(target, 0, 'xr', label='target')
        if ( np.isnan(iter_value) ):
            ax.plot(target, 0, 'xm', label='target (missed)')
        else:
            ax.plot(target, 0, 'xr', label='target')
            ax.plot(iter_value, 0, 'og', label='found') #, facecolors='none')


    #
    # Figure
    #
    fig = plt.figure(dpi=300, facecolor='white')
    ax0 = fig.subplots(2, 2)
    fig.subplots_adjust(wspace=0.14)

    subplot1(x_func[:, 0], func1(x_func[:, 0]), ax0[0, 0], 'funct 1')
    subplot1(x_func[:, 1], func2(x_func[:, 1]), ax0[1, 0], 'funct 2')
    subplot1(x_func[:, 2], func3(x_func[:, 2]), ax0[0, 1], 'funct 3')
    subplot1(x_func[:, 3], func4(x_func[:, 3]), ax0[1, 1], 'funct 4')

    subplot_target(root_values_targets[0], root_values_brent[0], ax0[0, 0])
    subplot_target(root_values_targets[1], root_values_brent[1], ax0[1, 0])
    subplot_target(root_values_targets[2], root_values_brent[2], ax0[0, 1])
    subplot_target(root_values_targets[3], root_values_brent[3], ax0[1, 1])


    ax0[0, 0].legend()
    ax0[1, 0].legend()
    ax0[0, 1].legend()
    ax0[1, 1].legend()

    fig.suptitle("Brent's Method")

    print(root_values_targets)
    print(root_values_brent)

    if plot_suffix:
        module4plots.plotting(fig, plotname, plot_suffix)


# -------------------------------------------------------------------------
#
# Main program
#
if __name__ == '__main__':
    import cbind_mod_solver as mod_solver
    import numpy as np
    import matplotlib.pyplot as plt
    import matplotlib as mpl
    import module4plots

    #
    # Load the shared library
    #
    LIBRARY_NAME = '../src/CISSEMBEL_CBindings.so' # INCLUDING path, e.g., ./
    print('Shared library containing C-bindings of Fortran code "'
          +LIBRARY_NAME+'"')

    PLOT_SUFFIXES = module4plots.parse_arguments() # = None = ['png', 'pdf']

    #
    # Plots for each function
    #
    plot_funcroot_newton(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_funcroot_bisection(LIBRARY_NAME, PLOT_SUFFIXES)
    #plot_funcroot_secant(LIBRARY_NAME, PLOT_SUFFIXES) #TODO:Its is broken
    plot_funcroot_brent(LIBRARY_NAME, PLOT_SUFFIXES)

# -- Last line

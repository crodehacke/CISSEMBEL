#!/bin/bash
# Master CISSEMBEL run script: HIRHAM SMB-type simulations via python driver using HIRHAM data in subdirectories
#
# To run CISSEMBEL simulations driven by preprocessed HIRHAM data via
# a python driver. DMI's preprocessing, which creates a tree of
# subdirectories for years, month, and days. The python driver reads
# the files across all data files in these subdirectories. The
# compilation of the code requires `make CISSEMBEL4py`.
#
# Usage example:
# nohup ./cissembel_hirham_run4py_ctrl_subdirs.bash > hirham2cissemble.simulation.$SCENARIO.log 2>&1 &
#
# To perform SMB simulations in the HIRHAM-SMB style, this run scripts
# - prepares a run directory,
# - copies or links to essentially files
# - performs specific adjustments (if requested)
# - calls the python driver and
# - performs post-processing (if requested)
#
#
# (c) Christian Rodehacke, 2023, DMI Copenhagen, Denmark
#
# -------------------------------------------------------------------
# This file is part of CISSEMBEL, which is the
# == Copenhagen Ice-Snow Surface Energy and Mass Balance modEL ==
#
# CISSEMBEL is free software; you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE  v.1.2
#
# Information about the EUROPEAN UNION PUBLIC LICENCE (*EUPL*) and
# translations into other European languages are available at
# https://eupl.eu (last access: 2021-10-01)](https://eupl.eu/)
#
# We hope that  this  distributed  CISSEMBEL  code, its  tools and
# documentations are usefull. Any improvements of the code, tools,
# and documentations tools are  very welcome.  Please, consider to
# share your thoughts and improvements with the community.
#
# The documentation,  tools,  and the code are provided as it are.
# NO WARRANTY  ABOUT FITNESS  AND USABILITY OF THE CODE, TOOLS, OR
# DOCUMENTATION  ARE  GIVEN.   NO  WARRANTY  IS  GIVEN  ABOUT  THE
# CORRECTNESS  OF COMPUTED RESULTS WITH THE  PROVIDED CODE, TOOLS,
# AND DOCUMENTATION. YOU USE THE CODE, DOCUMENTATION, AND TOOLS AT
# YOUR OWN RISK.           Removing  of  any  copyright  statement
# or  this  disclaimer  is considered  as a  attempt  to defraud.
#
# -------------------------------------------------------------------
set -e

#
# SETTINGS
#
EXPID="CE2" # from /dmidata/projects/nckf/smb_hirham/CE2/CE2_DataALLVAR/
#RUNDIR=/home/data/cr/work/CISSEMBEL.test.tmp/${EXPID} #Laptop for tests
RUNDIR="/data/users/${USER}/work/CISSEMBEL/${EXPID}"

LOGGING_FILE="CISSEMBEL.${EXPID}.log"

PYTHON_DRIVER=cissembel_hirham_subdirs.py
PYTHON_SHARED_LIBRARY=~cr/src/CISSEMBEL/src/CISSEMBEL_CBindAll.so
PYTHON_MODULES=~cr/src/CISSEMBEL/test

#CISSEMBEL_NAMELIST_EBM_SOURCE=~cr/src/CISSEMBEL/examples/namelists/HIRHAM.Greenland_5km/namelist.ebm.HIRHAM.Iceland
#CISSEMBEL_INITIAL_FILE=~cr/src/CISSEMBEL/data/HIRHAM.Greenland_5km/HIRHAM_initial.Iceland.xaxis_282_375_yaxis_108_172.kaxis32.nc
#CISSEMBEL_NAMELIST_EBM_SOURCE=~cr/src/CISSEMBEL/examples/namelists/HIRHAM.Greenland_5km/namelist.ebm.HIRHAM.GrIS
#CISSEMBEL_INITIAL_FILE=~cr/src/CISSEMBEL/data/HIRHAM.Greenland_5km/HIRHAM_initial.GrIS.xaxis_48_327_yaxis_95_510.kaxis32.nc
CISSEMBEL_NAMELIST_EBM_SOURCE=~cr/src/CISSEMBEL/examples/namelists/namelist.ebm.HIRHAM
CISSEMBEL_INITIAL_FILE=~cr/src/CISSEMBEL/data/HIRHAM.Greenland_5km/HIRHAM_initial.kaxis32.nc

# ------------------------------------------------------------------
# ------------------------------------------------------------------
#
# Functions
#
# ------------------------------------------------------------------
openmp_settings(){

    echo "* OpenMP setting"
    #export OMP_DISPLAY_ENV=true # For diagnostic
    export OMP_NUM_THREADS=32 #48 : 75% of ampere
    #export OMP_PROC_BIND=true
    #export OMP_WAIT_POLICY=active
    export OMP_DYNAMIC=false
    export OMP_SCHEDULE=dynamic

    for _item in $(env | grep OMP_ ) ; do
	echo "  - $_item"
    done
}

create_rundir(){
    _dir=${1:?"Missing directory name in create_rundir"}

    echo "* Working directory"
    if [ -d $_dir ] ; then
	echo "  o Reuse existing RUNDIR ${RUNDIR}"
    else
	echo "  + Create RUNDIR ${RUNDIR}"
	mkdir -p $RUNDIR
    fi
}

# ------------------------------------------------------------------
copy(){
    __source=${1}
    __target=${2}

    __source_basename=$(basename $__source)
    [ -d __target ] && __target=${__target}/${__source_basename}

    if [ -f $__target ] ; then
	echo "  o Keep existing file $__source_basename"
    else
	if [ ! -f $__source ] ; then
	    echo "### Missing source file $__source"
	    exit 1
	fi

	echo "  o copy file $__source"
	cp -p $__source $__target
    fi
}

# ------------------------------------------------------------------
link(){
    __source=${1}
    __target=${2}

    __source_basename=$(basename $__source)
    [ -d __target ] && __target=${__target}/${__source_basename}

    if [ -L $__target ] ; then
	echo "  o Keep existing link $__target"
    else
	if [ ! -e $__source ] ; then
	    echo "### Missing source file $__source"
	    exit 1
	fi

	echo "  o link file $__source"
	ln -s $__source $__target
    fi
}

# ------------------------------------------------------------------
copy_essential(){
    _dir=${1:?"Missing directory name in copy_essential"}

    echo "* Copy files to $_dir"
    _filelist="\
    ${PYTHON_DRIVER} \
    ${PYTHON_SHARED_LIBRARY} \
    ${CISSEMBEL_INITIAL_FILE}"

    for _file in $_filelist ; do
	copy $_file $_dir
    done
}

# ------------------------------------------------------------------
create_namelist(){
    _dir=${1:?"Missing directory name in create_namelist"}

    _namelist="namelist.ebm"
    echo "* Create namelist $_namelist"
    copy $CISSEMBEL_NAMELIST_EBM_SOURCE $_dir/$_namelist
}

# ------------------------------------------------------------------
link_essential(){
    for _file in ${PYTHON_MODULES} ; do
	case $_file in
	    ${PYTHON_MODULES})
		_target="python4cissemble"
		;;
	    *)
		_target=$(basename _file)
		;;
	esac

	link $_file $_target
    done

}

# ------------------------------------------------------------------
# ------------------------------------------------------------------
#
# Main body
#
# ------------------------------------------------------------------

create_rundir $RUNDIR
copy_essential $RUNDIR
create_namelist $RUNDIR

echo "* Jump into the RUNDIR $RUNDIR"
cd $RUNDIR

link_essential
openmp_settings

echo " --------------------------- - - -  -  -  -   -   -    -     -"
pwd
ls -l
echo " --------------------------- - - -  -  -  -   -   -    -     -"

#
# Do the jub
#
chmod u+x $PYTHON_DRIVER

command="./$PYTHON_DRIVER >& $LOGGING_FILE"
echo $command
$command

#echo "=== DMESG =================================================================="
#dmesg | tail -n 75
#echo "=== DMESG =================================================================="

#
# Post processing
#

echo '--- Done'
exit 0

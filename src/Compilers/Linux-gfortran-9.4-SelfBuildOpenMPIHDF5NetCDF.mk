#! Git version control id: $Id$

#
# -------------------------------------------------------------------
#
# Copyright (C) 2016-2025 Christian Rodehacke
#
# This file is part of CISSEMBEL, which is the
# == Copenhagen Ice-Snow Surface Energy and Mass Balance modEL ==
#
# CISSEMBEL is free software; you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE  v.1.2
#
# Information about the EUROPEAN UNION PUBLIC LICENCE (*EUPL*) and
# translations into other European languages are available at
# https://eupl.eu (last access: 2023-09-18)](https://eupl.eu/)
#
# We hope that  this  distributed  CISSEMBEL  code, its  tools and
# documentations are usefull. Any improvements of the code, tools,
# and documentations tools are  very welcome.  Please, consider to
# share your thoughts and improvements with the community.
#
# The documentation,  tools,  and the code are provided as it are.
# NO WARRANTY  ABOUT FITNESS  AND USABILITY OF THE CODE, TOOLS, OR
# DOCUMENTATION  ARE  GIVEN.   NO  WARRANTY  IS  GIVEN  ABOUT  THE
# CORRECTNESS  OF COMPUTED RESULTS WITH THE  PROVIDED CODE, TOOLS,
# AND DOCUMENTATION. YOU USE THE CODE, DOCUMENTATION, AND TOOLS AT
# YOUR OWN RISK.           Removing  of  any  copyright  statement
# or  this  disclaimer  is considered  as a  attempt  to defraud.
#
# -------------------------------------------------------------------

## @file Linux-gfortran-9.4-SelfBuildOpenMPIHDF5NetCDF.mk
#  @brief Include makefile that contain specific information for the
#    environment following the filename scheme: $(OS)-$(FORT).mk ,
#    where the compiler environment is storage in $(COMPILER_ENV). To
#    check its value on different computers run the following command:
#    make print-COMPILER_ENV
#    All different compiler specific files are located in the
#    sub-directory $(Compilers), so to ask to the final filename check
#    make print-COMPILER_INCLUDE_FILE
#
#    We set in this file
#    - Program locations for various Unix programs
#    - Path of required libraries
#    - Compiler specific flags
##

#---------------------------------------
#
# Unix programs (compiler or machine specific commands)
#
# XXX   :=

#---------------------------------------
#
# Flags
#
# flag for free format of long lines: LINE_LENGTH
LINE_LENGTH := -ffree-line-length-512
FFLAGS := -cpp
#FFLAGS += -march=native # architecture
ifdef DEBUG_FLAGS
  FFLAGS += -g -O0
  FFLAGS += -Wall -fbacktrace -fcheck=all -pedantic
  FFLAGS += -ffpe-trap=invalid,zero,overflow,underflow
  FFLAGS += -finit-real=snan
  FFLAGS += -DDEBUG
else
  FFLAGS += -O2 -mtune=native -mfpmath=sse
  ifdef PARALLEL_AUTO
    FFLAGS += -floop-parallelize-all
    $(warning "Turn on autoparallelization : set OMP_NUM_THREADS")
  endif
  FFLAGS += -DNDEBUG
endif

ifdef LARGE_MEM_FLAG
  FFLAGS += -mcmodel=medium # for data exceeding 2GB
endif
ifdef GPROFILE_FLAG
  $(warning "***** Add profiling support -pg ************")
  FFLAGS += -pg
  ifdef DEBUG_FLAGS
    FFLAGS += -fcheck-array-temporaries
  endif
endif

ifdef POS_INDEPEN_CODE
  $(warning "***** PIC: partition independent code ******")
  FFLAGS += -fPIC
endif

#
# Libaries
#
CPPFLAGS :=
ifdef OPENMP_PARALLEL
  FFLAGS   += -fopenmp
endif
ifdef MPI_PARALLEL
  # NetCDF incl. HDF5
  FFLAGS   += -I/home/cr/usr/local/netcdf-4.9.0-gcc/include
  #??#FFLAGS   += -I/home/cr/usr/local/netcdf-4.9.0-gcc/include -I/home/cr/usr/local/hdf5-1.12.2-gcc/include -I/home/cr/usr/local/netcdf-4.9.0-gcc/include
  #??#CPPFLAGS += -L/home/cr/usr/local/netcdf-4.9.0-gcc/lib -lnetcdff -Wl,-Bsymbolic-functions -Wl,-z,relro -Wl,-z,now -lnetcdf -lnetcdf -ldl -lm
  CPPFLAGS += -L/home/cr/usr/local/netcdf-4.9.0-gcc/lib -lnetcdff -lnetcdf -ldl -lm -lnetcdf

  # OpenMPI
  FFLAGS   += -DMPI_PARALLEL
  #??#FFLAGS   += -I/home/cr/usr/local/openmpi-4.1.4-gcc/include
  #??##CPPFLAGS += -L/home/cr/usr/local/openmpi-4.1.4-gcc/lib -lmpi
  #??##CPPFLAGS += -L/home/cr/usr/local/openmpi-4.1.4-gcc/lib -lmpi_usempif08
  #??#CPPFLAGS += -L/home/cr/usr/local/openmpi-4.1.4-gcc/lib

  FORT     := mpifort
  ifdef MPI_PARALLEL_OUT
    FFLAGS   += -DPARALLEL_IO
  endif

else
  # NetCDF incl. HDF5 (Ubuntu 20.02, "focal" standards)
  CPPFLAGS += -I/usr/include -I/usr/include/hdf5/serial -L/usr/lib/x86_64-linux-gnu -lnetcdff -Wl,-Bsymbolic-functions -Wl,-z,relro -Wl,-z,now -lnetcdf -lnetcdf -ldl -lm

endif

#
# -- Last lines
#

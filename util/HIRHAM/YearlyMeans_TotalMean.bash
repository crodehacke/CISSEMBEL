#!/usr/bin/env bash
# For a given period, computes for preprocessed data files the yearly mean values and total mean.
#
#   In addition, for given indexes a subdomain of the total mean is
# created. If you would like to adjust the subdomain, please adjust
# towards the end of this script the variables ix_var, ix0, ix1,
# iy_var, iy0, iy1, and tag.
#
#   Please keep in mind that the file name structure is hard-coded
# for the special conditions found in DMI's file pol.
#
#
# Usage
#  ./YearlyMeans_TotalMean.bash StartYear EndYear Scenario
#
#
# Dependence: cdo, ncatted
#
# (c) Christian Rodehacke, 2023, DMI Copenhagen, Denmark
#
# -------------------------------------------------------------------
# This file is part of CISSEMBEL, which is the
# == Copenhagen Ice-Snow Surface Energy and Mass Balance modEL ==
#
# CISSEMBEL is free software; you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE  v.1.2
#
# Information about the EUROPEAN UNION PUBLIC LICENCE (*EUPL*) and
# translations into other European languages are available at
# https://eupl.eu (last access: 2021-10-01)](https://eupl.eu/)
#
# We hope that  this  distributed  CISSEMBEL  code, its  tools and
# documentations are usefull. Any improvements of the code, tools,
# and documentations tools are  very welcome.  Please, consider to
# share your thoughts and improvements with the community.
#
# The documentation,  tools,  and the code are provided as it are.
# NO WARRANTY  ABOUT FITNESS  AND USABILITY OF THE CODE, TOOLS, OR
# DOCUMENTATION  ARE  GIVEN.   NO  WARRANTY  IS  GIVEN  ABOUT  THE
# CORRECTNESS  OF COMPUTED RESULTS WITH THE  PROVIDED CODE, TOOLS,
# AND DOCUMENTATION. YOU USE THE CODE, DOCUMENTATION, AND TOOLS AT
# YOUR OWN RISK.           Removing  of  any  copyright  statement
# or  this  disclaimer  is considered  as a  attempt  to defraud.
#
# -------------------------------------------------------------------
set -e

YR0=${1:?Missing starting year as first input}
YR1=${2:?Missing ending year as second input}
SCENARIO=${3:?Missing scenario as third input}

SUFFIX="nc4"

#
# Commands
#
CDO=cdo
NCATTED=ncatted
NCKS=ncks

#
# Information
#
echo "Info"
echo " Date  $(date)"
echo " PWD   $(pwd)"
echo " Start year YR0      $YR0"
echo " End   year YR1      $YR1"
echo " Scenario   SCENARIO $SCENARIO"
echo " File name  SUFFIX   $SUFFIX"
echo

#
# Do the job
#
echo "Loop"
for yr in $(seq $YR0 $YR1) ; do
    input="DMI-HIRHAM5_${SCENARIO}_${yr}_MERGED.${SUFFIX}"
    output=${input%%.${SUFFIX}}.ym.${SUFFIX}
    if [ -f $input ] ; then
	echo " ymean $input"
	#
	# Time shift ("shifttime") by -1 Second to bring last record
	# at 0:00 of the new year into the year of interest.
	#
	$CDO -s -O yearmean -shifttime,-1sec $input $output
    else
	echo " SKIP missing $input"
    fi
done

echo
echo "TotalMean"
$CDO -O -s timmean -mergetime \
	DMI-HIRHAM5_${SCENARIO}_????_MERGED.ym.${SUFFIX} \
	DMI-HIRHAM5_${SCENARIO}_${YR0}_${YR1}_MERGED.ym.tm.${SUFFIX}
echo

ix_var='x'
ix0='47'
ix1='347'
iy_var='y'
iy0='81'
iy1='519'
tag='Greenland'
echo "Cold-start file restricted to ${tag}"

$NCATTED -a grid_mapping,,d,, \
	 DMI-HIRHAM5_${SCENARIO}_${YR0}_${YR1}_MERGED.ym.tm.${SUFFIX}

$NCKS -O -7 --no-abc -d ${ix_var},${ix0},${ix1} -d ${iy_var},${iy0},${iy1} \
      -xv rotated_pole \
      DMI-HIRHAM5_${SCENARIO}_${YR0}_${YR1}_MERGED.ym.tm.${SUFFIX} \
      DMI-HIRHAM5_${SCENARIO}.${tag}.xaxis_${ix0}_${ix1}_yaxis_${iy0}_${iy1}.nc




echo
echo "Done ($(date))"
exit 0

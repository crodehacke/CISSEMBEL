#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep  9 14:06:53 2022


-- Repair time_bounds

Run:
directly:     ./repair_tbounds.py file.nc
alterntively  python repair_tbounds.py file.nc


@author: Christian Rodehacke, DMI Copenhagen, Denmark

@copyright: Copyright (C) 2022-2023 Christian Rodehacke
"""
# -------------------------------------------------------------------
# This file is part of CISSEMBEL, which is the
# == Copenhagen Ice-Snow Surface Energy and Mass Balance modEL ==
#
# CISSEMBEL is free software; you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE  v.1.2
#
# Information about the EUROPEAN UNION PUBLIC LICENCE (*EUPL*) and
# translations into other European languages are available at
# https://eupl.eu (last access: 2023-09-18)](https://eupl.eu/)
#
# We hope that  this  distributed  CISSEMBEL  code, its  tools and
# documentations are usefull. Any improvements of the code, tools,
# and documentations tools are  very welcome.  Please, consider to
# share your thoughts and improvements with the community.
#
# The documentation,  tools,  and the code are provided as it are.
# NO WARRANTY  ABOUT FITNESS  AND USABILITY OF THE CODE, TOOLS, OR
# DOCUMENTATION  ARE  GIVEN.   NO  WARRANTY  IS  GIVEN  ABOUT  THE
# CORRECTNESS  OF COMPUTED RESULTS WITH THE  PROVIDED CODE, TOOLS,
# AND DOCUMENTATION. YOU USE THE CODE, DOCUMENTATION, AND TOOLS AT
# YOUR OWN RISK.           Removing  of  any  copyright  statement
# or  this  disclaimer  is considered  as a  attempt  to defraud.
#
# -------------------------------------------------------------------


# -------------------------------------------------------------------------
def parse_arguments():
    '''
    Parse the command line arguments

    Returns
    -------
    argparse.Namespace
        Command line arguments.

    '''
    parser = argparse.ArgumentParser(description='Repair corrupt time_bounds',
        add_help=False)

    parser.add_argument('--filename', '-f',
                        nargs='?',
                        required=True,
                        help='filename containing the corrupt time_bounds')

    parser.add_argument('--backname', '-b',
                        nargs='?',
                        required=False,
                        default=parser.parse_args().filename+'.bak',
                        help='filename containing the corrupt time_bounds')

    parser.add_argument('--varname', '-v',
                        nargs='?',
                        required=False,
                        default='time_bounds',
                        help='Variable name of the time_bounds variable')

    # # Report used command line arguments
    # for single_arg in vars(parser.parse_args()): #sorted(vars(args)) :
    #     print('  - {:21s}'.format(single_arg)+
    #           ' = '+str(getattr(parser.parse_args(), single_arg)) )

    filename = parser.parse_args().filename
    varname = parser.parse_args().varname

    return parser.parse_args()


def file_operations(fname, bname):

    try:
        path.exists(fname)
    except FileNotFoundError:
        print("File does not exist "+fname)
    else:
        if not path.isfile(fname):
            print("File is an directory "+fname)


    if not bname is None or bname == 'None':
        try:
            copyfile(fname, bname)
        except IsADirectoryError:
            print("File is an directory "+fname)
        else:
            print("Created from input data "+fname+" a backup "+bname)


def read_tbounds(bname, fname, vname):
    filehandle = xarray.open_dataset(bname)

    idx_lower = 0
    idx_upper = 1

    tbounds = filehandle.get(vname).values
    tbounds[1:, idx_lower] = tbounds[0:-1, idx_upper]

    print("Corrected time boundaries of '"+vname+"' in file "+fname)
    filehandle.to_netcdf(fname)


# -------------------------------------------------------------------------
#
# Main program
#

if __name__ == '__main__':
    import argparse
    from os import path
    from shutil import copyfile
    import numpy
    import xarray

    command_line_args = parse_arguments()

    file_operations(command_line_args.filename,
                    command_line_args.backname)
    read_tbounds(command_line_args.backname,
                 command_line_args.filename,
                 command_line_args.varname)


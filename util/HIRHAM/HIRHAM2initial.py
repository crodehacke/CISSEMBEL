#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed 11 Jan 2023 02:56:17 PM CET

--- Takes prepred HIRHAM data and construct an initial
file for the CISSEMBEL model. Please note: Some provided fields, such as the
`surface_slope`, are filled with standard values that do not necessarily
describe adequately the conditions.

cdo merge SMBmodel_LatLonElev.nc  SMBmodel_ElevGrad.nc \
    ZwallyBasinsGreenlandExtended.nc \
    SMB_preinit.nc

Call example (1): Directly on the command line

python ./HIRHAM2initial.py \
            --input ./SMB_preinit.nc \
            --vname_input_surface_elevation elev \
            --vname_input_longitude longitude \
            --vname_input_latitude latitude \
            --output HIRHAM_initial.kaxis32.nc \
            --vname_output_xaxis xaxis \
            --vname_output_yaxis yaxis \
            --vname_output_surface_elevation surface_elevation \
            --vname_output_longitude lon \
            --vname_output_latitude lat \
            --dname_output_xaxis xaxis \
            --dname_output_yaxis yaxis

Call example (2): On the ipython console

runfile('./HIRHAM2initial.py', \
        args='-i ./ORG/PROMICE_subsurface_temperature_all_depths_hourly.nc \
        -o HIRHAM_initial.kaxis32.nc \
        --vname_output_xaxis x --vname_output_yaxis y \
        --vname_output_surface_elevation usrf \
        --vname_output_longitude lon --vname_output_latitude lat \
        --dname_output_xaxis x1 --dname_output_yaxis y1', \
        wdir='/home/cr/src/CISSEMBEL/data/PROMICE.Baptiste')


@author: Christian Rodehacke, DMI Copenhagen, Denmark

@copyright: Copyright (C) 2023 Christian Rodehacke
"""
# -------------------------------------------------------------------
# This file is part of CISSEMBEL, which is the
# == Copenhagen Ice-Snow Surface Energy and Mass Balance modEL ==
#
# CISSEMBEL is free software; you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE  v.1.2
#
# Information about the EUROPEAN UNION PUBLIC LICENCE (*EUPL*) and
# translations into other European languages are available at
# https://eupl.eu (last access: 2021-10-01)](https://eupl.eu/)
#
# We hope that  this  distributed  CISSEMBEL  code, its  tools and
# documentations are usefull. Any improvements of the code, tools,
# and documentations tools are  very welcome.  Please, consider to
# share your thoughts and improvements with the community.
#
# The documentation,  tools,  and the code are provided as it are.
# NO WARRANTY  ABOUT FITNESS  AND USABILITY OF THE CODE, TOOLS, OR
# DOCUMENTATION  ARE  GIVEN.   NO  WARRANTY  IS  GIVEN  ABOUT  THE
# CORRECTNESS  OF COMPUTED RESULTS WITH THE  PROVIDED CODE, TOOLS,
# AND DOCUMENTATION. YOU USE THE CODE, DOCUMENTATION, AND TOOLS AT
# YOUR OWN RISK.           Removing  of  any  copyright  statement
# or  this  disclaimer  is considered  as a  attempt  to defraud.
#
# -------------------------------------------------------------------

# -------------------------------------------------------------------------
def parse_arguments():
    '''
    Parse the command line arguments

    Returns
    -------
    list
        parsed command line arguments.

    '''
    parser = argparse.ArgumentParser(
        description='Creation of (re)start conditions from average climate conditions',
        add_help=False)

    #
    # Input and output file names
    #
    parser.add_argument('--input', '-i',
                        nargs='?',
                        required=True,
                        help='Input file name containing the input conditions')

    parser.add_argument('--output', '-o',
                        nargs='?',
                        required=False,
                        default='initial_file.nc',
                        help='Output file name of the file')


    #
    # variable names in the input file
    #
    parser.add_argument('--vname_input_surface_elevation',
                        nargs='?',
                        required=False,
                        default='elevation', #!TODO: better 'default'
                        help='variable name of the surface elevation in the input file')

    parser.add_argument('--vname_input_surface_slope',
                        nargs='?',
                        required=False,
                        default='ElevGrad',
                        help='variable name of the surface slope in the input file')

    parser.add_argument('--vname_input_area',
                        nargs='?',
                        required=False,
                        default='cellarea',
                        help='variable name of the grid cell area in the input file')

    parser.add_argument('--vname_input_glacmask',
                        nargs='?',
                        required=False,
                        default='glacmask',
                        help='variable name of the glacier mask in the input file')

    parser.add_argument('--vname_input_basins',
                        nargs='?',
                        required=False,
                        default='BasinsINTExtended',
                        help='variable name of the basins mask in the input file')

    parser.add_argument('--vname_input_longitude',
                        nargs='?',
                        required=False,
                        default='longitude',
                        help='variable name of the longitude in the input file')

    parser.add_argument('--vname_input_latitude',
                        nargs='?',
                        required=False,
                        default='latitude',
                        help='variable name of the latitude in the input file')

    #
    # variable names in the output file
    #
    parser.add_argument('--vname_output_surface_elevation',
                        nargs='?',
                        required=False,
                        default='elevation',
                        help='variable name of the surface elevation in the output file')

    parser.add_argument('--vname_output_surface_slope',
                        nargs='?',
                        required=False,
                        default='surface_slope',
                        help='variable name of the surface slope in the output file')

    parser.add_argument('--vname_output_area',
                        nargs='?',
                        required=False,
                        default='cellarea',
                        help='variable name of the grid cell area in the output file')

    parser.add_argument('--vname_output_glacmask',
                        nargs='?',
                        required=False,
                        default='glacmask',
                        help='variable name of the glacier mask in the output file')

    parser.add_argument('--vname_output_basins',
                        nargs='?',
                        required=False,
                        default='basins',
                        help='variable name of the basins mask in the output file')


    parser.add_argument('--vname_output_longitude',
                        nargs='?',
                        required=False,
                        default='lon',
                        help='variable name of the longitude in the output file')

    parser.add_argument('--vname_output_latitude',
                        nargs='?',
                        required=False,
                        default='lat',
                        help='variable name of the latitude in the output file')

    parser.add_argument('--vname_output_xaxis',
                        nargs='?',
                        required=False,
                        default='iaxis',
                        help='variable name of the xaxis in the output file')

    parser.add_argument('--vname_output_yaxis',
                        nargs='?',
                        required=False,
                        default='jaxis',
                        help='variable name of the yaxis in the output file')

    parser.add_argument('--dname_output_xaxis',
                        nargs='?',
                        required=False,
                        default='iaxis',
                        help='dimension name of the xaxis in the output file')

    parser.add_argument('--dname_output_yaxis',
                        nargs='?',
                        required=False,
                        default='jaxis',
                        help='dimension name of the yaxis in the output file')

    #
    # Further arguments
    #
    parser.add_argument('--output_format',
                        nargs='?',
                        required=False,
                        default='NETCDF3_64BIT',
                        choices=['NETCDF4', 'NETCDF4_CLASSIC', 'NETCDF3_64BIT', 'NETCDF3_CLASSIC'],
                        help='Format of the netcdf output file')


    # # Report used command line arguments
    print('Used settings: read command line arguments or default values')
    for single_arg in vars(parser.parse_args()): #sorted(vars(args)) :
        print(' - %-32s = '% (single_arg)+str(getattr(parser.parse_args(), single_arg)) )
        # print(type(single_arg))
    print()

    return parser.parse_args()


# -------------------------------------------------------------------------
#
# Main program
#
if __name__ == '__main__':
    import argparse           # argparse : Parse command line arguments
    import numpy as np        # Numpy    : Array handling
    # import pandas as pd       # Pandas   :
    import xarray as xr       # Xarrray  : Easy handling of netcdf files
    from datetime import  datetime # Current date/time
    # from pyproj import Proj   # PyProj   : Transformation between coordinates
    # from pathlib import Path
    import os


    print('Start    the job ('+str(datetime.now())[:19]+')')
    # Parse command arguments
    parsed_argv = parse_arguments()

    dname_iaxis = parsed_argv.vname_output_xaxis
    dname_jaxis = parsed_argv.vname_output_yaxis
    dname_kaxis = 'kaxis'
    dname_haxis = 'haxis'

    #
    # Read input data
    #
    #data_in = xr.open_dataset(parsed_argv.input, decode_times=False)
    data_in = xr.open_dataset(parsed_argv.input, decode_times=True)

    #
    # Axis
    #
    y_axis = np.arange(int(data_in.get(parsed_argv.vname_input_surface_elevation).shape[0]))
    if data_in.get(parsed_argv.vname_input_surface_elevation).ndim > 1:
        x_axis = np.arange(int(data_in.get(parsed_argv.vname_input_surface_elevation).shape[1]))
    else:
        x_axis = np.array([0])

    x_axis = xr.DataArray(x_axis, dims=[dname_iaxis],
                          attrs= {'long_name': 'Cartesian x-coordinate',
                                  'units': 'count',
                                  'standard_name': 'projection_x_coordinate'},)
    y_axis = xr.DataArray(y_axis, dims=[dname_jaxis],
                          attrs= {'long_name': 'Cartesian y-coordinate',
                                  'units': 'count',
                                  'standard_name': 'projection_y_coordinate'},)
    k_axis = xr.DataArray(np.arange(32), dims=[dname_kaxis],
                          attrs= {'standard_name': 'model_level_number',
                                  'long_name' : 'z-layer of mesh grid',
                                  'units' : 'count',
                                  'positive' : 'down',})
    h_axis = xr.DataArray(np.arange(13), dims=[dname_haxis],
                          attrs= {'standard_name': 'height',
                                  'long_name' : 'height class elevation levels',
                                  'units' : 'meter',
                                  'positive' : 'up',})

    #
    # Layer thickness, height
    #
    # -- Analytic layer_thickness ...
    layer_thickness = np.array(np.power(0.12*k_axis.data, 1.4)+0.15)

    # -- manually given layer_thickness ...
    if ( k_axis.size == 35 and False ):
        layer_thickness = np.array([0.15, 0.15, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2,
                                    0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2,
                                    0.25, 0.26, 0.26, 0.26, 0.345, 0.35, 0.35,
                                    0.545, 0.56, 0.47, 0.5, 0.7, 0.8, 0.8,
                                    0.9, 1.4, 2.0, 3.0, 5.5])

    layer_thickness = xr.DataArray(layer_thickness, dims=[dname_kaxis],
                                   attrs ={'standard_name' : 'snow_layer_thickness',
                                           'long_name' : 'thickness of each snow layer',
                                           'units' : 'meter',})

    #
    # Transform layer_thickness to depth
    #
    depth = np.zeros_like(layer_thickness)
    depth[0] = layer_thickness[0]*0.5
    for k in range(1, layer_thickness.size):
        depth[k] = depth[k-2]+layer_thickness[k]*0.5

    depth = xr.DataArray(depth, dims=[dname_kaxis],
                         attrs ={'standard_name' : 'depth',
                                 'long_name' : 'depth below surface',
                                 'units' : 'meter',
                                 'positive' : 'down', })

    #
    # height
    #
    height = xr.DataArray(np.array([0., 100., 250., 500., 750., 1000., 1250.,
                                    1500., 2000., 2400., 2800., 3500., 10000.]),
                          dims=[dname_haxis],
                          attrs ={'standard_name' : 'height',
                                  'long_name' : 'height class elevation levels',
                                  'units' : 'meter' ,
                                  'positive' : 'up',})



    #
    # Dummy data
    #
    shape_2dim = data_in.get(parsed_argv.vname_input_longitude).data.shape
    size_2dim = data_in[parsed_argv.vname_input_longitude].data.size

    ones2dim = np.zeros(shape_2dim)
    zeros2dim = np.zeros(shape_2dim)

    #
    # Masks
    #
    glacier_mask = data_in[parsed_argv.vname_input_glacmask].data
    icesheet_mask = np.where(data_in['maskbas'].data > -10, 1, 0)
    basins_icesheet = np.array(np.where(icesheet_mask,
                                       data_in[parsed_argv.vname_input_basins].data,
                                       0),
                              dtype=np.short)

    #
    # Output data construct
    #
    data_out = xr.Dataset(
        coords = {
            parsed_argv.dname_output_xaxis: (x_axis),
            parsed_argv.dname_output_yaxis: (y_axis),
            dname_kaxis: k_axis,
            dname_haxis: h_axis,
            },
        data_vars = {
            parsed_argv.vname_output_longitude :
                ([parsed_argv.vname_output_yaxis,
                  parsed_argv.vname_output_xaxis],
                 data_in[parsed_argv.vname_input_longitude].data,
                 {'long_name': 'longitude',
                  'units': 'degreesE',
                  'standard_name': 'longitude'}
                 ),
            parsed_argv.vname_output_latitude :
                ([parsed_argv.vname_output_yaxis,
                  parsed_argv.vname_output_xaxis],
                 data_in[parsed_argv.vname_input_latitude].data,
                 {'long_name': 'latitude',
                  'units': 'degreesN',
                  'standard_name': 'latitude'}
                 ),
            parsed_argv.vname_output_surface_elevation :
                ([parsed_argv.vname_output_yaxis,
                  parsed_argv.vname_output_xaxis],
                 data_in[parsed_argv.vname_input_surface_elevation].data,
                 {'long_name': 'surface elevation',
                  'units': 'm',
                  'coordinates': parsed_argv.vname_output_longitude+" "+parsed_argv.vname_output_latitude, }
                 ),
            parsed_argv.vname_output_surface_slope :
                ([parsed_argv.vname_output_yaxis,
                  parsed_argv.vname_output_xaxis],
                 data_in[parsed_argv.vname_input_surface_slope].data,
                  {'long_name': 'surface slope',
                  'units': 'm m-1',
                  'coordinates': parsed_argv.vname_output_longitude+" "+parsed_argv.vname_output_latitude, }
                  ),
            parsed_argv.vname_output_area :
                ([parsed_argv.vname_output_yaxis,
                  parsed_argv.vname_output_xaxis],
                 data_in[parsed_argv.vname_input_area].data,
                  {'long_name': 'grid cell area',
                   'standard_name': 'cell_area',
                  'units': 'm2',
                  'coordinates': parsed_argv.vname_output_longitude+" "+parsed_argv.vname_output_latitude, }
                  ),

            parsed_argv.vname_output_glacmask :
                ([parsed_argv.vname_output_yaxis,
                  parsed_argv.vname_output_xaxis],
                 data_in[parsed_argv.vname_input_glacmask].data,
                 {'long_name': 'glacier mask',
                  'coordinates': parsed_argv.vname_output_longitude+" "+parsed_argv.vname_output_latitude, }
                 ),

            'icesheet_mask' :
                ([parsed_argv.vname_output_yaxis,
                  parsed_argv.vname_output_xaxis],
                 icesheet_mask,
                 {'long_name': 'icesheet mask',
                  'coordinates': parsed_argv.vname_output_longitude+" "+parsed_argv.vname_output_latitude, }
                ),

            parsed_argv.vname_output_basins :
                ([parsed_argv.vname_output_yaxis,
                  parsed_argv.vname_output_xaxis],
                 basins_icesheet,
                 {'long_name': 'basins',
                  'coordinates': parsed_argv.vname_output_longitude+" "+parsed_argv.vname_output_latitude, }
                 ),

            parsed_argv.vname_output_basins+'_full' :
                ([parsed_argv.vname_output_yaxis,
                  parsed_argv.vname_output_xaxis],
                 data_in[parsed_argv.vname_input_basins].data,
                 {'long_name': 'basins',
                  'coordinates': parsed_argv.vname_output_longitude+" "+parsed_argv.vname_output_latitude, }
                 ),

            'height' : height,
            'layer_thickness': layer_thickness ,
            'depth' : depth,

            },
        attrs = {
            "title": "Preliminary initial CISSEMBEL data",
            "data_source": str(parsed_argv.input),
            "creation_date": str(datetime.now()),
            },
        )

    print("Written forcing file '"+parsed_argv.output+"' as "+parsed_argv.output_format)
    data_out.to_netcdf(parsed_argv.output, format=parsed_argv.output_format) #, unlimited_dims='time')


    print(' ncatted -a coordinates,lon,d,, -a coordinates,lat,d,, -a coordinates,site_label,d,, '+parsed_argv.output)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 27 11:10:19 CEST 2021

C-binding to Fortran functions and subroutines in mod_solver as part of the
Copenhagen Ice Snow Surface Energy and Mass Balance modEL (CISSEMBEL)

@author: Christian Rodehacke, DMI Copenhagen, Denmark

@copyright: Copyright (C) 2016-2025 Christian Rodehacke
"""
# -------------------------------------------------------------------
# This file is part of CISSEMBEL, which is the
# == Copenhagen Ice-Snow Surface Energy and Mass Balance modEL ==
#
# CISSEMBEL is free software; you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE  v.1.2
#
# Information about the EUROPEAN UNION PUBLIC LICENCE (*EUPL*) and
# translations into other European languages are available at
# https://eupl.eu (last access: 2023-09-18)](https://eupl.eu/)
#
# We hope that  this  distributed  CISSEMBEL  code, its  tools and
# documentations are usefull. Any improvements of the code, tools,
# and documentations tools are  very welcome.  Please, consider to
# share your thoughts and improvements with the community.
#
# The documentation,  tools,  and the code are provided as it are.
# NO WARRANTY  ABOUT FITNESS  AND USABILITY OF THE CODE, TOOLS, OR
# DOCUMENTATION  ARE  GIVEN.   NO  WARRANTY  IS  GIVEN  ABOUT  THE
# CORRECTNESS  OF COMPUTED RESULTS WITH THE  PROVIDED CODE, TOOLS,
# AND DOCUMENTATION. YOU USE THE CODE, DOCUMENTATION, AND TOOLS AT
# YOUR OWN RISK.           Removing  of  any  copyright  statement
# or  this  disclaimer  is considered  as a  attempt  to defraud.
#
# -------------------------------------------------------------------


#-----------------------------------------------------------------
def funcroot_newton(bound_lower, bound_upper, iterations, testfunc_id,
                    library='../src/CISSEMBEL_CBindings.so'):
    """
    C-binding to Fortran function funcroot_newton in mod_solver in CISSEMBEL
    Newton-Raphson method to find the root for the selected test function

    Parameters
    ----------
    bound_lower : float or numpy.ndarray
        Lower bound of the interval to search for the root (zero); Tbnd0.
    bound_upper : float or numpy.ndarray
        Upper bound of the interval to search for the root (zero); Tbnd1.
    iterations : int or numpy.ndarray
        Maximum number of iterations to search for the root (zero); iter_max.
    testfunc_id : int or numpy.ndarray
        Identifer selecting the test function (see variable 'icase' in
        funcroot_newton of section 'Set of simple test function' in mod_solver.F08)
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings. The
        default is '../src/CISSEMBEL_CBindings.so'.

    Returns
    -------
    root_value: float or numpy.ndarray
        Found root value (zero position) for the selected test function

    """
    if isinstance(library, ctypes.CDLL):
        fortlibrary = library
    else:
        fortlibrary = ctypes.cdll.LoadLibrary(library)

    if isinstance(bound_lower, (float, int)):
        inum = 0
        # Single c-value
        c_bound_lower = ctypes.c_double(bound_lower)
        c_bound_upper = ctypes.c_double(bound_upper)
        c_iterations = ctypes.c_int(iterations)
        c_testfunc_id = ctypes.c_int(testfunc_id)
    else:
        inum = len(bound_lower)
        # Number conversion, Fortran-order
        c_bound_lower = numpy.array(bound_lower, order="F", dtype=float)
        c_bound_upper = numpy.array(bound_upper, order="F", dtype=float)
        c_iterations = numpy.array(iterations, order="F", dtype=int)
        c_testfunc_id = numpy.array(iterations, order="F", dtype=int)
        # c-pointer
        c_bound_lower_ptr = c_bound_lower.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_bound_upper_ptr = c_bound_upper.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_iterations_ptr = c_iterations.ctypes.data_as(ctypes.POINTER(ctypes.c_int))
        c_testfunc_id_ptr = c_testfunc_id.ctypes.data_as(ctypes.POINTER(ctypes.c_int))

        # Field sizes
        m4c.compare_field2shapes(bound_lower.shape, bound_upper.shape,
                                 'bound_lower', 'bound_upper')
        m4c.compare_field2shapes(iterations.shape, bound_upper.shape,
                                 'iterations', 'bound_upper')
        m4c.compare_field2shapes(iterations.shape, testfunc_id.shape,
                                 'iterations', 'testfunc_id')
    if inum:
        # array
        root_value = numpy.zeros((inum), order="F", dtype=float)
        root_value_ptr = root_value.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        fortlibrary.c_funcroot_newton.argtypes = [ctypes.c_int,
                                                  ctypes.POINTER(ctypes.c_double),
                                                  ctypes.POINTER(ctypes.c_double),
                                                  ctypes.POINTER(ctypes.c_int),
                                                  ctypes.POINTER(ctypes.c_double)]
        fortlibrary.c_funcroot_newton(ctypes.c_int(inum),
                                      c_bound_lower_ptr,
                                      c_bound_upper_ptr,
                                      c_iterations_ptr,
                                      root_value_ptr)
    else:
        # skalar
        fortlibrary.funcroot_newton.restype = ctypes.c_double
        root_value = fortlibrary.funcroot_newton(ctypes.byref(c_bound_lower),
                                                 ctypes.byref(c_bound_upper),
                                                 ctypes.byref(c_iterations),
                                                 ctypes.byref(c_testfunc_id))
    return root_value

#-----------------------------------------------------------------
def funcroot_bisection(bound_lower, bound_upper, iterations, testfunc_id,
                    library='../src/CISSEMBEL_CBindings.so'):
    """
    C-binding to Fortran function funcroot_bisection in mod_solver in CISSEMBEL
    Bisection method to find the root for the selected test function

    Parameters
    ----------
    bound_lower : float or numpy.ndarray
        Lower bound of the interval to search for the root (zero); Tbnd0.
    bound_upper : float or numpy.ndarray
        Upper bound of the interval to search for the root (zero); Tbnd1.
    iterations : int or numpy.ndarray
        Maximum number of iterations to search for the root (zero); iter_max.
    testfunc_id : int or numpy.ndarray
        Identifer selecting the test function (see variable 'icase' in
        funcroot_bisection of section 'Set of simple test function' in mod_solver.F08)
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings. The
        default is '../src/CISSEMBEL_CBindings.so'.

    Returns
    -------
    root_value: float or numpy.ndarray
        Found root value (zero position) for the selected test function

    """
    if isinstance(library, ctypes.CDLL):
        fortlibrary = library
    else:
        fortlibrary = ctypes.cdll.LoadLibrary(library)

    if isinstance(bound_lower, (float, int)):
        inum = 0
        # Single c-value
        c_bound_lower = ctypes.c_double(bound_lower)
        c_bound_upper = ctypes.c_double(bound_upper)
        c_iterations = ctypes.c_int(iterations)
        c_testfunc_id = ctypes.c_int(testfunc_id)
    else:
        inum = len(bound_lower)
        # Number conversion, Fortran-order
        c_bound_lower = numpy.array(bound_lower, order="F", dtype=float)
        c_bound_upper = numpy.array(bound_upper, order="F", dtype=float)
        c_iterations = numpy.array(iterations, order="F", dtype=int)
        c_testfunc_id = numpy.array(iterations, order="F", dtype=int)
        # c-pointer
        c_bound_lower_ptr = c_bound_lower.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_bound_upper_ptr = c_bound_upper.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_iterations_ptr = c_iterations.ctypes.data_as(ctypes.POINTER(ctypes.c_int))
        c_testfunc_id_ptr = c_testfunc_id.ctypes.data_as(ctypes.POINTER(ctypes.c_int))

        # Field sizes
        m4c.compare_field2shapes(bound_lower.shape, bound_upper.shape,
                                 'bound_lower', 'bound_upper')
        m4c.compare_field2shapes(iterations.shape, bound_upper.shape,
                                 'iterations', 'bound_upper')
        m4c.compare_field2shapes(iterations.shape, testfunc_id.shape,
                                 'iterations', 'testfunc_id')
    if inum:
        # array
        root_value = numpy.zeros((inum), order="F", dtype=float)
        root_value_ptr = root_value.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        fortlibrary.c_funcroot_bisection.argtypes = [ctypes.c_int,
                                                     ctypes.POINTER(ctypes.c_double),
                                                     ctypes.POINTER(ctypes.c_double),
                                                     ctypes.POINTER(ctypes.c_int),
                                                     ctypes.POINTER(ctypes.c_double)]
        fortlibrary.c_funcroot_bisection(ctypes.c_int(inum),
                                         c_bound_lower_ptr,
                                         c_bound_upper_ptr,
                                         c_iterations_ptr,
                                         root_value_ptr)
    else:
        # skalar
        fortlibrary.funcroot_bisection.restype = ctypes.c_double
        root_value = fortlibrary.funcroot_bisection(ctypes.byref(c_bound_lower),
                                                    ctypes.byref(c_bound_upper),
                                                    ctypes.byref(c_iterations),
                                                    ctypes.byref(c_testfunc_id))
    return root_value

#-----------------------------------------------------------------
def funcroot_secant(bound_lower, bound_upper, iterations, testfunc_id,
                    library='../src/CISSEMBEL_CBindings.so'):
    """
    C-binding to Fortran function funcroot_secant in mod_solver in CISSEMBEL
    Secant method to find the root for the selected test function

    Parameters
    ----------
    bound_lower : float or numpy.ndarray
        Lower bound of the interval to search for the root (zero); Tbnd0.
    bound_upper : float or numpy.ndarray
        Upper bound of the interval to search for the root (zero); Tbnd1.
    iterations : int or numpy.ndarray
        Maximum number of iterations to search for the root (zero); iter_max.
    testfunc_id : int or numpy.ndarray
        Identifer selecting the test function (see variable 'icase' in
        funcroot_secant of section 'Set of simple test function' in mod_solver.F08)
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings. The
        default is '../src/CISSEMBEL_CBindings.so'.

    Returns
    -------
    root_value: float or numpy.ndarray
        Found root value (zero position) for the selected test function

    """
    if isinstance(library, ctypes.CDLL):
        fortlibrary = library
    else:
        fortlibrary = ctypes.cdll.LoadLibrary(library)

    if isinstance(bound_lower, (float, int)):
        inum = 0
        # Single c-value
        c_bound_lower = ctypes.c_double(bound_lower)
        c_bound_upper = ctypes.c_double(bound_upper)
        c_iterations = ctypes.c_int(iterations)
        c_testfunc_id = ctypes.c_int(testfunc_id)
    else:
        inum = len(bound_lower)
        # Number conversion, Fortran-order
        c_bound_lower = numpy.array(bound_lower, order="F", dtype=float)
        c_bound_upper = numpy.array(bound_upper, order="F", dtype=float)
        c_iterations = numpy.array(iterations, order="F", dtype=int)
        c_testfunc_id = numpy.array(iterations, order="F", dtype=int)
        # c-pointer
        c_bound_lower_ptr = c_bound_lower.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_bound_upper_ptr = c_bound_upper.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_iterations_ptr = c_iterations.ctypes.data_as(ctypes.POINTER(ctypes.c_int))
        c_testfunc_id_ptr = c_testfunc_id.ctypes.data_as(ctypes.POINTER(ctypes.c_int))

        # Field sizes
        m4c.compare_field2shapes(bound_lower.shape, bound_upper.shape,
                                 'bound_lower', 'bound_upper')
        m4c.compare_field2shapes(iterations.shape, bound_upper.shape,
                                 'iterations', 'bound_upper')
        m4c.compare_field2shapes(iterations.shape, testfunc_id.shape,
                                 'iterations', 'testfunc_id')
    if inum:
        # array
        root_value = numpy.zeros((inum), order="F", dtype=float)
        root_value_ptr = root_value.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        fortlibrary.c_funcroot_secant.argtypes = [ctypes.c_int,
                                                  ctypes.POINTER(ctypes.c_double),
                                                  ctypes.POINTER(ctypes.c_double),
                                                  ctypes.POINTER(ctypes.c_int),
                                                  ctypes.POINTER(ctypes.c_double)]
        fortlibrary.c_funcroot_secant(ctypes.c_int(inum),
                                      c_bound_lower_ptr,
                                      c_bound_upper_ptr,
                                      c_iterations_ptr,
                                      root_value_ptr)
    else:
        # skalar
        fortlibrary.funcroot_secant.restype = ctypes.c_double
        root_value = fortlibrary.funcroot_secant(ctypes.byref(c_bound_lower),
                                                 ctypes.byref(c_bound_upper),
                                                 ctypes.byref(c_iterations),
                                                 ctypes.byref(c_testfunc_id))
    return root_value

#-----------------------------------------------------------------
def funcroot_brent(bound_lower, bound_upper, iterations, testfunc_id,
                    library='../src/CISSEMBEL_CBindings.so'):
    """
    C-binding to Fortran function funcroot_brent in mod_solver in CISSEMBEL
    Brent's method to find the root for the selected test function

    Parameters
    ----------
    bound_lower : float or numpy.ndarray
        Lower bound of the interval to search for the root (zero); Tbnd0.
    bound_upper : float or numpy.ndarray
        Upper bound of the interval to search for the root (zero); Tbnd1.
    iterations : int or numpy.ndarray
        Maximum number of iterations to search for the root (zero); iter_max.
    testfunc_id : int or numpy.ndarray
        Identifer selecting the test function (see variable 'icase' in
        funcroot_brent of section 'Set of simple test function' in mod_solver.F08)
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings. The
        default is '../src/CISSEMBEL_CBindings.so'.

    Returns
    -------
    root_value: float or numpy.ndarray
        Found root value (zero position) for the selected test function

    """
    if isinstance(library, ctypes.CDLL):
        fortlibrary = library
    else:
        fortlibrary = ctypes.cdll.LoadLibrary(library)

    if isinstance(bound_lower, (float, int)):
        inum = 0
        # Single c-value
        c_bound_lower = ctypes.c_double(bound_lower)
        c_bound_upper = ctypes.c_double(bound_upper)
        c_iterations = ctypes.c_int(iterations)
        c_testfunc_id = ctypes.c_int(testfunc_id)
    else:
        inum = len(bound_lower)
        # Number conversion, Fortran-order
        c_bound_lower = numpy.array(bound_lower, order="F", dtype=float)
        c_bound_upper = numpy.array(bound_upper, order="F", dtype=float)
        c_iterations = numpy.array(iterations, order="F", dtype=int)
        c_testfunc_id = numpy.array(iterations, order="F", dtype=int)
        # c-pointer
        c_bound_lower_ptr = c_bound_lower.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_bound_upper_ptr = c_bound_upper.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_iterations_ptr = c_iterations.ctypes.data_as(ctypes.POINTER(ctypes.c_int))
        c_testfunc_id_ptr = c_testfunc_id.ctypes.data_as(ctypes.POINTER(ctypes.c_int))

        # Field sizes
        m4c.compare_field2shapes(bound_lower.shape, bound_upper.shape,
                                 'bound_lower', 'bound_upper')
        m4c.compare_field2shapes(iterations.shape, bound_upper.shape,
                                 'iterations', 'bound_upper')
        m4c.compare_field2shapes(iterations.shape, testfunc_id.shape,
                                 'iterations', 'testfunc_id')
    if inum:
        # array
        root_value = numpy.zeros((inum), order="F", dtype=float)
        root_value_ptr = root_value.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        fortlibrary.c_funcroot_brent.argtypes = [ctypes.c_int,
                                                 ctypes.POINTER(ctypes.c_double),
                                                 ctypes.POINTER(ctypes.c_double),
                                                 ctypes.POINTER(ctypes.c_int),
                                                 ctypes.POINTER(ctypes.c_double)]
        fortlibrary.c_funcroot_brent(ctypes.c_int(inum),
                                     c_bound_lower_ptr,
                                     c_bound_upper_ptr,
                                     c_iterations_ptr,
                                     root_value_ptr)
    else:
        # skalar
        fortlibrary.funcroot_brent.restype = ctypes.c_double
        root_value = fortlibrary.funcroot_brent(ctypes.byref(c_bound_lower),
                                                ctypes.byref(c_bound_upper),
                                                ctypes.byref(c_iterations),
                                                ctypes.byref(c_testfunc_id))
    return root_value

# -----------------------------------------------------------
#
# Main program
#
if __name__ == '__main__':
    import ctypes
    import numpy
    import module4cbind as m4c

    #
    # Load the shared library
    #
    LIBRARY_NAME = '../src/CISSEMBEL_CBindings.so' # INCLUDING path, e.g., ./

    print('= Load shared library containing C-bindings of Fortran code "'
          +LIBRARY_NAME+'"')
    FORTLIBRARY = ctypes.cdll.LoadLibrary(LIBRARY_NAME)
else:
    import ctypes
    import numpy
    import module4cbind as m4c

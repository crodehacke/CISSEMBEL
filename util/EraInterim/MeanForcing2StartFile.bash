#!/bin/bash
# Creates based on long-term mean/climatic atmospheric fields a restart-file.
#
# Create a best-guess state/restart file which also takes the height
#   classes into account. Here we even consider the height class PLUS
#   support.
#
# Call:
# ./MeanForcing2StartFile.bash MeanForcing InitialFile RestartFile
#
# Call examples:
#./MeanForcing2StartFile.bash DMI-HIRHAM5_CE2_1971_2000.tm.Greenland.xaxis_47_347_yaxis_81_519.nc \
#			 $HOME/src/CISSEMBEL/data/HIRHAM.Greenland_5km/HIRHAM_initial.Greenland.xaxis_47_347_yaxis_81_519.kaxis32.nc \
#			 HIRHAM_boot.CE2.Greenland.xaxis_47_347_yaxis_81_519.kaxis32.nc"
#
# Note:
# ncks -O -7 --no-abc -d x,47,347 -d y,81,519 -xv rotated_pole \
#      DMI-HIRHAM5_CE2_1971_2000_MERGED.ym.tm.nc4 \
#      DMI-HIRHAM5_CE2_1971_2000.tm.Greenland.xaxis_47_347_yaxis_81_519.nc
#
#
# (c) Christian Rodehacke, 2023, DMI Copenhagen, Denmark
# -------------------------------------------------------------------
# This file is part of CISSEMBEL, which is the
# == Copenhagen Ice-Snow Surface Energy and Mass Balance modEL ==
#
# CISSEMBEL is free software; you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE  v.1.2
#
# Information about the EUROPEAN UNION PUBLIC LICENCE (*EUPL*) and
# translations into other European languages are available at
# https://eupl.eu (last access: 2021-10-01)](https://eupl.eu/)
#
# We hope that  this  distributed  CISSEMBEL  code, its  tools and
# documentations are usefull. Any improvements of the code, tools,
# and documentations tools are  very welcome.  Please, consider to
# share your thoughts and improvements with the community.
#
# The documentation,  tools,  and the code are provided as it are.
# NO WARRANTY  ABOUT FITNESS  AND USABILITY OF THE CODE, TOOLS, OR
# DOCUMENTATION  ARE  GIVEN.   NO  WARRANTY  IS  GIVEN  ABOUT  THE
# CORRECTNESS  OF COMPUTED RESULTS WITH THE  PROVIDED CODE, TOOLS,
# AND DOCUMENTATION. YOU USE THE CODE, DOCUMENTATION, AND TOOLS AT
# YOUR OWN RISK.           Removing  of  any  copyright  statement
# or  this  disclaimer  is considered  as a  attempt  to defraud.
#
# -------------------------------------------------------------------
#

FORCING_FILE=${1:?Missing input forcing file name as first argument}
INITIAL_FILE=${2:?Missing input initial file name as second argument}
OUTPUT_FILE=${3:?Missing the output file name as third argument}

PYTHON_SCRIPT=../aveclim2start.py

forcing_vname_air_temperature='t2m'
forcing_vname_precipitation='tp'
forcing_vname_elevation='z'

initial_vname_xaxis='x'
initial_vname_yaxis='y'
initial_vname_elevation='orog'
initial_vname_height='height'

lapse_rate=0.005


#
# Do the job
#
set -x
$PYTHON_SCRIPT --input_climate $FORCING_FILE \
               --input_initial $INITIAL_FILE \
               --output $OUTPUT_FILE \
               --vname_climate_airtemperature    $forcing_vname_air_temperature \
               --vname_climate_precipitation     $forcing_vname_precipitation \
               --vname_climate_surface_elevation $forcing_vname_elevation \
               --vname_climate_longitude         lon \
               --vname_climate_latitude          lat \
               --vname_initial_surface_elevation $initial_vname_elevation \
               --vname_initial_longitude         lon \
               --vname_initial_latitude          lat \
               --vname_initial_axis_right        $initial_vname_xaxis \
               --vname_initial_axis_left         $initial_vname_yaxis \
               --vname_initial_height            $initial_vname_height \
               --height_class_plus \
               --lapse_rate                      $lapse_rate
set +x

echo
if [ -f $OUTPUT_FILE ] ; then
    echo "Created output file: $(du -hs $OUTPUT_FILE)"
else
    echo "Missing output file $OUTPUT_FILE"
fi

echo "-- Done"
exit 0

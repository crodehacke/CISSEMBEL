#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov  9 13:40:38 2021

-- Test and plots for mod_albedo.F08

@author: Christian Rodehacke, DMI Copenhagen, Denmark

@copyright: Copyright (C) 2016-2025 Christian Rodehacke
"""
# -------------------------------------------------------------------
# This file is part of CISSEMBEL, which is the
# == Copenhagen Ice-Snow Surface Energy and Mass Balance modEL ==
#
# CISSEMBEL is free software; you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE  v.1.2
#
# Information about the EUROPEAN UNION PUBLIC LICENCE (*EUPL*) and
# translations into other European languages are available at
# https://eupl.eu (last access: 2023-09-18)](https://eupl.eu/)
#
# We hope that  this  distributed  CISSEMBEL  code, its  tools and
# documentations are usefull. Any improvements of the code, tools,
# and documentations tools are  very welcome.  Please, consider to
# share your thoughts and improvements with the community.
#
# The documentation,  tools,  and the code are provided as it are.
# NO WARRANTY  ABOUT FITNESS  AND USABILITY OF THE CODE, TOOLS, OR
# DOCUMENTATION  ARE  GIVEN.   NO  WARRANTY  IS  GIVEN  ABOUT  THE
# CORRECTNESS  OF COMPUTED RESULTS WITH THE  PROVIDED CODE, TOOLS,
# AND DOCUMENTATION. YOU USE THE CODE, DOCUMENTATION, AND TOOLS AT
# YOUR OWN RISK.           Removing  of  any  copyright  statement
# or  this  disclaimer  is considered  as a  attempt  to defraud.
#
# -------------------------------------------------------------------

# -------------------------------------------------------------------------
def plot_albedo_temp(library_name, plot_suffix=None, plotname='albedo_temp'):
    '''
    Plot results of the via c-binding call Fortran function albedo_temp

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'albedo_temp'.

    Returns
    -------
    None.

    '''
    print('* Albedo-Temp')
    tmelt = FREEZING_TEMP
    surface_temperature = np.linspace(-20, 10, 1101, dtype=float) + tmelt
    trans_temperature = 2.0
    tmelt_temperature = np.zeros_like(surface_temperature) + tmelt

    albedo = mod_albedo.albedo_temp(surface_temperature, tmelt_temperature,
                                    trans_temperature, library_name)
    #
    # Plot
    #
    fig0 = plt.figure(dpi=300, facecolor='white')
    ax0 = fig0.subplots()

    ax0.plot(surface_temperature, albedo,'.', color='darkred', zorder=1)
    ax0.set_xlabel('Surface Temperature (K)')
    ax0.set_ylabel('Albedo (1)')
    ax0.set_title('Albedo_Temp')


    ax0.plot([tmelt, tmelt], [max(albedo)*1.01, min(albedo)*0.99],
             linestyle='--', linewidth=2, color='cyan', zorder=0)
    ax0.text(tmelt+0.1, max(albedo),  r'$T_{freeze}$', color='darkcyan')


    ax0.text(surface_temperature[0]+
             (surface_temperature[-1]-surface_temperature[0])*0.5,
             albedo[0]+(albedo[-1]-albedo[0])*0.75,
             "$\\alpha_{{fresh\\_snow}}$ = {:2.3g}\n".format(ALBEDO_FRESHSNOW)+
             "$\\alpha_{{ice}}$ = {:2.3g}\n".format(ALBEDO_ICE)+
             "$\\alpha_{{melting}}$ = {:2.3g}\n".format(ALBEDO_MELTING)+
             "$T_{{trans}}$ = {:2.2g} K".format(TEMPERATURE_TRANSITIONAL),
             ha="right", va="top",bbox=dict(boxstyle="square",
                                            ec='darkgray', fc='lightgray',
                                            alpha=0.5))
    if plot_suffix:
        module4plots.plotting(fig0, plotname, plot_suffix)

# -------------------------------------------------------------------------
def plot_albedo_temp_snowdepth(library_name, plot_suffix=None,
                               plotname='albedo_temp_snowdepth'):
    '''
    Plot results of the via c-binding call Fortran function albedo_temp_snowdepth

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'albedo_temp_snowdepth'.

    Returns
    -------
    None.

    '''
    print('* Albedo-Temp-Snowdepth')

    tmelt = FREEZING_TEMP
    snow_depth_threshold = SNOW_DEPTH_THRESHOLD
    surface_temperature = np.linspace(-20, 10, 1101, dtype=float) + tmelt
    snowdepth = np.array([0.0, 0.25, 0.5, 2.0]) * SNOW_DEPTH_THRESHOLD
    trans_temperature = 2.0
    tmelt_temperature = np.zeros_like(surface_temperature) + tmelt

    albedo_multi = np.zeros((len(surface_temperature), len(snowdepth)))
    for isd, snowd in enumerate(snowdepth):
        snowdepth_array = np.zeros_like(surface_temperature) + snowd
        albedo_multi[:, isd] = mod_albedo.albedo_temp_snowdepth( \
            surface_temperature,
            snowdepth_array,
            tmelt_temperature,
            trans_temperature,
            snow_depth_threshold,
            library_name)
    #
    # Plot
    #
    fig0 = plt.figure(dpi=300, facecolor='white')
    ax0 = fig0.subplots()

    line0 = ax0.plot(surface_temperature, albedo_multi,'.', zorder=1)
    ax0.set_xlabel('Surface Temperature (K)')
    ax0.set_ylabel('Albedo (1)')
    ax0.set_title('Albedo_Temp_Snowdepth')


    ax0.plot([tmelt, tmelt],
             [np.max(albedo_multi)*1.01, np.min(albedo_multi)*0.99],
             linestyle='--', linewidth=2, color='cyan', zorder=0)
    ax0.text(tmelt+0.1, np.max(albedo_multi),  r'$T_{freeze}$', color='darkcyan')

    label = [] #[r'$T_{frez}$']
    for snowd in snowdepth:
        label.append('{:2.2g}'.format(snowd))
    ax0.legend(line0, label, title=r'$d_{{snow}}$ (m)')

    ax0.text(surface_temperature[0]+
             (surface_temperature[-1]-surface_temperature[0])*0.5,
             albedo_multi[0, 0]+(albedo_multi[-1, 0]-albedo_multi[0, 0])*0.4,
             "$\\alpha_{{fresh\\_snow}}$ = {:2.3g}\n".format(ALBEDO_FRESHSNOW)+
             "$\\alpha_{{ice}}$ = {:2.3g}\n".format(ALBEDO_ICE)+
             "$\\alpha_{{melting}}$ = {:2.3g}\n".format(ALBEDO_MELTING)+
             "$T_{{trans}}$ = {:2.2g} K\n".format(TEMPERATURE_TRANSITIONAL)+
             "$d_{{snow}}^{{threshold}}$ = {:2.3g} m".format(SNOW_DEPTH_THRESHOLD),
             ha="right", va="top",bbox=dict(boxstyle="square",
                                            ec='darkgray', fc='lightgray',
                                            alpha=0.5))
    if plot_suffix:
        module4plots.plotting(fig0, plotname, plot_suffix)


# -------------------------------------------------------------------------
def plot_albedo_time(library_name, plot_suffix=None, plotname='albedo_time'):
    '''
    Plot results of the via c-binding call Fortran function albedo_time

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'albedo_time'.

    Returns
    -------
    None.

    '''
    print('* Albedo-Time')
    albedo_light = ALBEDO_FRESHSNOW
    albedo_dark = ALBEDO_ICE
    reci_decaytime = RECIPROCAL_SNOW_DECAYTIME
    snow_age = np.linspace(0, 200, 701, dtype=float) # days

    albedo = mod_albedo.albedo_time(snow_age* 86400., albedo_light,
                                    albedo_dark, reci_decaytime, library_name)
    #
    # Plot
    #
    fig0 = plt.figure(dpi=300, facecolor='white')
    ax0 = fig0.subplots()

    ax0.plot(snow_age, albedo,'.', color='darkred', zorder=1)
    ax0.set_xlabel('Snow age/Time (day)')
    ax0.set_ylabel('Albedo (1)')
    ax0.set_title('albedo_time')

    ax0.plot([1/(reci_decaytime*86400), 1/(reci_decaytime*86400)],
             [max(albedo)*0.99, min(albedo)*0.99],
             linestyle='--', linewidth=2, color='green', zorder=0)
    ax0.text(1/(reci_decaytime*86400), max(albedo),
             r'$\tau_{{decay}}$', color='darkgreen')

    ax0.text(snow_age[0]+(snow_age[-1]-snow_age[0])*0.95,
             albedo[0]+(albedo[-1]-albedo[0])*0.05,
            "$\\alpha_{{light}}$ = {:2.3g}\n".format(albedo_light)+
            "$\\alpha_{{dark}}$ = {:2.3g}\n".format(albedo_dark)+
            "$\\tau_{{decay}}$ = {:2.3g} days\n".format(1./(reci_decaytime*86400.)),
             ha="right", va="top",bbox=dict(boxstyle="square",
                                            ec='darkgray', fc='lightgray',
                                            alpha=0.5))
    if plot_suffix:
        module4plots.plotting(fig0, plotname, plot_suffix)


# -------------------------------------------------------------------------
def plot_albedo_snowd_exp(library_name, plot_suffix=None,
                          plotname='albedo_snowd_exp'):
    '''
    Plot results of the via c-binding call Fortran function albedo_snowd_exp

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'albedo_snowd_exp'.

    Returns
    -------
    None.

    '''
    print('* Albedo-Snowdepth Exponential')

    snow_depth = np.linspace(0., 0.1, 601, dtype=float) # Meter
    albedo_top = np.zeros_like(snow_depth)+ALBEDO_FRESHSNOW
    albedo_below = np.zeros_like(snow_depth)+ALBEDO_ICE
    reci_decaylength = RECIPROCAL_SNOW_DECAYLENGTH

    albedo = mod_albedo.albedo_snowd_exp(snow_depth, albedo_top, albedo_below,
                                         reci_decaylength, library_name)
    #
    # Plot
    #
    fig0 = plt.figure(dpi=300, facecolor='white')
    ax0 = fig0.subplots()

    ax0.plot(snow_depth, albedo,'-', color='darkblue', zorder=1, linewidth=3)
    #ax0.set_xscale('log')
    ax0.set_xlabel('Snow depth (m)')
    ax0.set_ylabel('Albedo (1)')
    ax0.set_title('albedo_snowd_exp')


    ax0.text(snow_depth[0]+(snow_depth[-1]-snow_depth[0])*0.05,
             albedo_below[-1],
             "$\\alpha_{{below}}$ = {:2.3g}".format(albedo_below[0]),
             ha='left')
    ax0.text(snow_depth[0]+(snow_depth[-1]-snow_depth[0])*1.00,
             albedo_top[-1]*0.98,
             "$\\alpha_{{below}}$ = {:2.3g}".format(albedo_top[0]),
             ha='right')

    if plot_suffix:
        module4plots.plotting(fig0, plotname, plot_suffix)

# -------------------------------------------------------------------------
def plot_albedo_time_snowdepth(library_name, plot_suffix=None,
                               plotname='albedo_time_snowdepth'):
    '''
    Plot results of the via c-binding call Fortran function albedo_time_snowdepth

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'albedo_inttime_warn'.

    Returns
    -------
    None.

    '''
    print('* Albedo-Time-Snowdepth')


    snow_age_day = np.linspace(0, 180, 101, dtype=float) # days
    snow_age = snow_age_day*86400.0
    snow_depth = np.linspace(0., 0.05, 201, dtype=float) # Meter

    albedo_freshsnow = ALBEDO_FRESHSNOW
    albedo_firn = ALBEDO_FIRN
    albedo_ice = ALBEDO_ICE
    reciprocal_snow_decaytime = RECIPROCAL_SNOW_DECAYTIME

    albedo_multi = np.zeros((len(snow_age), len(snow_depth)))
    for isd, snowd in enumerate(snow_depth):
        snow_depth_array = np.zeros_like(snow_age) + snowd
        alb = mod_albedo.albedo_time_snowdepth(snow_age, snow_depth_array,
                                               albedo_freshsnow, albedo_firn,
                                               albedo_ice,
                                               reciprocal_snow_decaytime)
        albedo_multi[:, isd] = alb.reshape(snow_age.shape)
    del alb, snow_depth_array


    #
    # Plot
    #
    fig0 = plt.figure(dpi=300, facecolor='white')
    ax0 = fig0.subplots()

    csf = ax0.contourf(snow_depth, snow_age_day, albedo_multi, cmap='BuPu_r')
    ax0.set_xlabel('Snow depth (m)')
    ax0.set_ylabel('Time (day)')
    ax0.set_title('albedo_time_snowdepth')

    cbar = fig0.colorbar(csf, ax=ax0)
    cbar.set_label('Albedo (1)')

    if plot_suffix:
        module4plots.plotting(fig0, plotname, plot_suffix)


# -------------------------------------------------------------------------
def plot_albedo_inttime_warm(library_name, plot_suffix=None,
                             plotname='albedo_inttime_warm'):
    '''
    Plot results of the via c-binding call Fortran function albedo_inttime_warn

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'albedo_inttime_warn'.

    Returns
    -------
    None.

    '''
    print('* Albedo-IntTime-Snowdepth')

    tmelt = FREEZING_TEMP
    surface_temperature = np.linspace(-1.25, 10, 131, dtype=float)+tmelt
    time = np.linspace(0, 150, 121, dtype=float) # days

    stemp2d, time2d = np.meshgrid(surface_temperature, time)
    snowts = np.maximum((stemp2d-tmelt) * time2d, 0.)
    #snowdepth = np.array([0.005, 0.01, 0.02, 0.05]) #([0, 0.001, 0.01, 0.1])
    snowdepth = np.array([0.005, 0.01, 0.02, 0.05])

    albedo_multi = np.zeros_like(snowts)
    albedo_multi = np.zeros((len(snowts[:,0]), len(snowts[0,:]), len(snowdepth)))
    for isd, snowd in enumerate(snowdepth):
        snowts_array = snowts.reshape(-1) * 86400
        snowdepth_array = np.zeros_like(snowts_array) + snowd
        alb = mod_albedo.albedo_inttime_warm(snowts_array, snowdepth_array,
                                             library_name)
        albedo_multi[:, :, isd] = alb.reshape(snowts.shape)
    del alb, snowts_array, snowdepth_array

    #
    # Figure 1
    #
    fig1 = plt.figure(dpi=300, facecolor='white')
    ax0 = fig1.subplots()

    snowts_nan = np.where(snowts>0 , snowts, np.NaN)
    csf = ax0.contourf(surface_temperature, time, snowts_nan, cmap='Reds')
    ax0.set_xlabel('Near-surface air temperature (K)')
    ax0.set_ylabel('Time (day)')

    ax0.set_title('Integrated temperature above freezing over time\n'+
                  'since last snowfall: '+
                  '$snowts=\\int T_{air}|_{{(T_{air}>T_{{threshold}})}} dt$ ')
    cbar = fig1.colorbar(csf, ax=ax0)
    cbar.set_label('$\\int T_{air}|_{{(T_{air}>T_{{threshold}})}} dt\\;$ (Kelvin day)')

    ax0.plot([tmelt, tmelt], [min(time), max(time)], ls='--', lw=2, color='cyan')
    ax0.text(tmelt-0.25, min(max(time), 25),  r'$T_{freeze}$',
             color='darkcyan', horizontalalignment='right', rotation=90)

    if plot_suffix:
        module4plots.plotting(fig1, plotname+'_snowts', plot_suffix)

    #
    # Figure 2
    #
    fig2 = plt.figure(dpi=300, facecolor='white')
    [axul, axur], [axll, axlr] = fig2.subplots(2, 2)
    fig2.subplots_adjust(hspace=0.1, wspace=0.1)

    for iax, ax0 in enumerate([axul, axur, axll, axlr]):
        # Background: filled contours
        cflevels = np.linspace(0.5, 0.90, 9)
        csf = ax0.contourf(surface_temperature, time, albedo_multi[:, :, iax],
                           cmap='Greys_r', levels=cflevels, extend='both')

        # Contour lines
        clevels = [0.5, 0.6, 0.7, 0.8, 0.9]

        cs0_ = ax0.contour(surface_temperature, time, albedo_multi[:, :, iax],
                           levels=clevels, cmap='YlOrRd')
        if ax0 in [axul]:
            cs0 = cs0_


        if ax0 in [axul, axll]:
            # left
            ax0.set_ylabel('Time (days)')
        if ax0 in [axll, axlr]:
            # bottom
            ax0.set_xlabel('Temperature $T_{air}$ (K)')
        if ax0 in [axul, axur]:
            # top
            ax0.set_xticklabels([])
        if ax0 in [axur, axlr]:
            # left
            ax0.set_yticklabels([])
        if ax0 == axul:
            # Legend
            h_cs0,_ = cs0_.legend_elements()
            ax0.legend(h_cs0, [f'{lev}' for lev in clevels],
                       loc='center right', labelspacing=0.2)

        # T_freezing line
        ax0.plot([tmelt, tmelt], [min(time), max(time)], linestyle='--',
                 linewidth=2, color='darkcyan')
        ax0.text(tmelt-0.25, time[-1]*0.5,  r'$T_{freeze}$', color='darkcyan',
                 ha='right', va='center', rotation=90)

        # Text/Label about snow thickness
        color_text = 'white'
        if ax0 == axlr:
            color_text = 'black'
        ax0.text(surface_temperature[0]+(surface_temperature[-1]-surface_temperature[0])*.95,
                 time[-1]*.95,r'$d_{{snow}} =$ {:g}'.format(snowdepth[iax]*100)+
                 ' cm', color=color_text, ha='right',va='top')



    cbar = fig2.colorbar(csf, ax=[axul, axur, axll, axlr])
    cbar.set_label('Albedo $\\alpha$ (1)')
    if cs0:
        cbar.add_lines(cs0)
    fig2.suptitle('albedo_inttime_warm')

    if plot_suffix:
        module4plots.plotting(fig2, plotname, plot_suffix)


# -------------------------------------------------------------------------

def plot_albedo_rodehacke1(library_name, plot_suffix=None,
                           plotname='albedo_rodehacke1'):
    '''
    Plot results of the via c-binding call Fortran function albedo_rodehacke1

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'albedo_rodehacke1'.

    Returns
    -------
    None.

    '''
    print('* Albedo-Rodehacke1')
    plotting_albedo_rodehacke123(library_name, plot_suffix, plotname,
                                 albedo_scheme='albedo_rodehacke1')

# -------------------------------------------------------------------------

def plot_albedo_rodehacke2(library_name, plot_suffix=None,
                           plotname='albedo_rodehacke2'):
    '''
    Plot results of the via c-binding call Fortran function albedo_rodehacke2

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'albedo_rodehacke2'.

    Returns
    -------
    None.

    '''
    print('* Albedo-Rodehacke2')
    plotting_albedo_rodehacke123(library_name, plot_suffix, plotname,
                                 albedo_scheme='albedo_rodehacke2')

# -------------------------------------------------------------------------

def plot_albedo_rodehacke3(library_name, plot_suffix=None,
                           plotname='albedo_rodehacke3'):
    '''
    Plot results of the via c-binding call Fortran function albedo_rodehacke3

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'albedo_rodehacke3'.

    Returns
    -------
    None.

    '''
    print('* Albedo-Rodehacke3')
    plotting_albedo_rodehacke123(library_name, plot_suffix, plotname,
                                 albedo_scheme='albedo_rodehacke3')


# -------------------------------------------------------------------------

def plotting_albedo_rodehacke123(library_name, plot_suffix=None,
                                    plotname='albedo_rodehacke1',
                                    albedo_scheme='albedo_rodehacke1'):
    '''
    Performs the actul module4plots.plotting of one of the three albedo scheme results of
    the via c-binding call Fortran function albedo_rodehacke1,
    albedo_rodehacke2, or albedo_rodehacke3


    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'albedo_rodehacke1'.
    albedo_scheme : TYPE, optional
        DESCRIPTION. The default is 'albedo_rodehacke1'.

    Returns
    -------
    None.

    '''
    # -------------------------------------------------------------------
    #
    # Internal functions
    #

    # -------------------------------------------------------------------
    def snow_age_history(time_array, snowfall_event_index, start_with_snowfall=False):
        '''
        Computes the snow age history based on given time indexes

        Parameters
        time_array : numpy.ndarray
            Time axis.
        snowfall_event_index : int
            DESCRIPTION.
        start_with_snowfall : TYPE, optional
            Shall we assume that we have snowfall in the beginning. The default is False.

        Returns
        -------
        snowage : numpy.ndarray
            Snow age history.

        '''
        if len(time_array)>1:
            dt_time = time_array[1] - time_array[0]

            if isinstance(snowfall_event_index, (np.integer, np.int, np.float)):
                # Case 1: Here we have one snow_fall_event_index
                if start_with_snowfall:
                    snowage = np.ones_like(time_array) * dt_time
                else:
                    snowage = np.zeros_like(time_array)

                if snowfall_event_index == 0:
                    snowage[snowfall_event_index] = dt_time

                # for tindx = range(1, len(time)):
                #     if snowage[tindx:] = dt_time

                if snowfall_event_index < len(time_array):
                    snowage[snowfall_event_index] = 0.
                    snowage[snowfall_event_index+1:] = dt_time



            else:
                # Case 2: Here we have an array of snow_fall_event_index
                if start_with_snowfall:
                    snowage = np.ones((len(time_array), len(snowfall_event_index))) * dt_time
                else:
                    snowage = np.zeros((len(time_array), len(snowfall_event_index)))
                for iindx, tindx in enumerate(snowfall_event_index):
                    if tindx < len(time_array):
                        snowage[tindx, iindx] = 0.
                        snowage[tindx+1:, iindx] = dt_time

            # Cumsum along time axis (axis=0) to compute the actual age
            snowage = np.cumsum(snowage, axis=0)
        else:
            snowage = -9.

        return snowage

    # -------------------------------------------------------------------
    def snow_depth_history(time_array, temperature_array, set_sdepth, slope,
                           set_sdepth_index=[0],
                           sdepth_range=[0., 2.], melt_temp=FREEZING_TEMP):
        '''
        Linear reduction of the snow depth which is set to set_sdepth at the
        provided index list set_sdepth_index


        Parameters
        ----------
        time_array : list or numpy.ndarray
            Array of the time axis.
        temperature_array : list or numpy.ndarray
            Temperature array.
        set_sdepth : float
            Snow depth, Value use in time of set_sdepth_index.
        slope : float
            Linear growth/decline controlled by the temperature_array below/above melt_temp.
        set_sdepth_index : List or numpy.ndarray, optional
            Index during which set_sdepth is applied. The default is [0].
        sdepth_range : list
            Snow depth minimum and maximum, until where the snow depth declines or grows.
        melt_temp : float, optional
            Melting temperature. The default is FREEZING_TEMP.
        Returns
        -------
        snowdepth : list or numpy.ndarray
            Temporal evolution of the snowdepth.

        '''
        if isinstance(time_array, (np.integer, np.float)):
            len_time_array = int(1)
        else:
            len_time_array = len(time_array)

        snowdepth = np.zeros(len_time_array)
        sdepth_min = min(sdepth_range)
        sdepth_max = max(sdepth_range)
        set_sdepth_index = list(set_sdepth_index)

        if set_sdepth_index:
            # Use index of time_array to set thickness to sdepth_set
            for idx in range(len_time_array):
                if idx in set_sdepth_index:
                    snowdepth[idx] = set_sdepth
                else:
                    # snowdepth[idx] = max(sdepth_min,snowdepth[idx-1]+slope)

                    # actual slope depends on the temperature
                    off = slope
                    if temperature_array[idx] >= melt_temp:
                        off = -slope

                    snowdepth[idx] = min(sdepth_max, max(sdepth_min,
                                                          snowdepth[idx-1]+off))

        return snowdepth


    # -------------------------------------------------------------------
    #
    # main body
    #
    # -------------------------------------------------------------------
    print('  - '+str(albedo_scheme))

    #
    # Scenarios
    #
    snowdepth_threshold = SNOW_DEPTH_THRESHOLD
    snowd_inf = 20*snowdepth_threshold
    snowd_thin = 0.1*snowdepth_threshold
    snowd_zero = 0.0

    rho_heavy = RHO_ICE
    rho_light = RHO_SNOW

    temperature_cold = FREEZING_TEMP-2.5
    temperature_warm = FREEZING_TEMP
    temperature_melt = FREEZING_TEMP

    NUMBER_OF_SCENARIOS = 11
    NO_TIME_STEPS = 650

    #
    # Generic secnarios
    #
    # Time-axis: day, second
    days = np.linspace(0, 120, NO_TIME_STEPS, dtype=float) # days
    seconds = days*86400.
    dt_second = seconds[1]-seconds[0]

    # Index of defined events at given days
    days_of_pre_melting_days = 90

    day_of_melting = 30
    idx_day_melt = abs(days-day_of_melting) == min(abs(days-day_of_melting))
    if any(idx_day_melt):
        idx_day_melt = int(np.min(np.where(idx_day_melt)))
        idx_day_melt = min(idx_day_melt, NO_TIME_STEPS-1)

    day_of_refreezing = 20
    idx_day_refrez = abs(days-day_of_refreezing) == min(abs(days-day_of_refreezing))
    if any(idx_day_refrez):
        idx_day_refrez = int(np.min(np.where(idx_day_refrez)))
        idx_day_refrez = min(idx_day_refrez, NO_TIME_STEPS-1)

    day_of_snowfall = 80
    idx_day_snowfall = abs(days-day_of_snowfall) == min(abs(days-day_of_snowfall))
    if any(idx_day_snowfall):
        idx_day_snowfall = int(np.min(np.where(idx_day_snowfall)))
        idx_day_snowfall = min(idx_day_snowfall, NO_TIME_STEPS-1)

    #
    # Time invariant fields
    #
    snow_depth_inf = np.zeros_like(seconds) + snowd_inf
    snow_depth_thin = np.zeros_like(seconds) + snowd_thin
    snow_depth_zero = np.zeros_like(seconds) + snowd_zero

    rho_field_heavy = np.zeros_like(seconds) + rho_heavy
    rho_field_light = np.zeros_like(seconds) + rho_light

    temperature_field_cold = np.zeros_like(seconds) + temperature_cold
    temperature_field_warm = np.zeros_like(seconds) + temperature_warm
    temperature_field_melt = np.zeros_like(seconds) + temperature_melt

    #
    # Temporally evolving fields
    #
    temperature_field_warm2cold_refrz = \
        np.concatenate((temperature_field_warm[:idx_day_refrez],
                        temperature_field_cold[idx_day_refrez:],), axis=0)

    # Snowfall : 3 months ago
    snow_age_sf3monthago = np.cumsum(np.ones_like(seconds)
                                     +dt_second)+days_of_pre_melting_days*86400.
    # Snowfall : at the start (index=0)
    snow_age_sfstart = np.ones_like(seconds)+dt_second
    snow_age_sfstart[0] = 0
    snow_age_sfstart = np.cumsum(snow_age_sfstart)

    # Melting : ongoing since 3 months, refreezing day 10:
    melt_age_3month = np.cumsum(np.ones_like(seconds)
                                +dt_second)+days_of_pre_melting_days*86400.
    melt_mask_3month = np.ones_like(days, dtype=int)

    # No melting
    melt_mask_nomelt = np.zeros_like(days, dtype=int)
    melt_age_nomelt = np.cumsum(np.zeros_like(seconds)+dt_second)

    # Melting : No melt at start (index=1), melting after `day_of_melting`
    melt_mask_afterstart = np.zeros_like(seconds, dtype=int)
    melt_mask_afterstart[idx_day_melt:] = 1
    melt_age_afterstart = np.cumsum(np.zeros_like(seconds)+dt_second)
    melt_age_afterstart[idx_day_melt:] = 0.0

    #
    # Preallocate memory for field driving albedo_rodehacke[1-3]
    #
    albedo_fields = np.zeros((NO_TIME_STEPS, NUMBER_OF_SCENARIOS), dtype=float)
    snow_age_fields = np.zeros_like(albedo_fields)
    melt_age_fields = np.zeros_like(albedo_fields)
    snow_depth_fields = np.zeros_like(albedo_fields)
    rho_fields = np.zeros_like(albedo_fields)
    temp_fields = np.zeros_like(albedo_fields)
    melt_mask_fields = np.zeros_like(albedo_fields, dtype=int)
    day_fields = np.zeros_like(albedo_fields, dtype=int)


    #
    # -- Metamorphose Scenarios
    #
    # Scenario 0: infinite snow layer with snowfall day 1, no melting
    # snowdepth >> SNOW_DEPTH_THRESHOLD
    iscenario = 0
    snow_age_fields[:, iscenario] = snow_age_sfstart
    melt_age_fields[:, iscenario] = melt_age_nomelt
    snow_depth_fields[:, iscenario] = snow_depth_inf
    rho_fields[:, iscenario] = rho_field_light
    temp_fields[:, iscenario] = temperature_field_cold
    melt_mask_fields[:, iscenario] = melt_mask_nomelt
    day_fields[:, iscenario] = days

    # Scenario 1: thin snow layer with snowfall day 1, no melting
    # snowdepth < SNOW_DEPTH_THRESHOLD
    # rho_surface = RHO_SNOW
    iscenario = 1
    snow_age_fields[:, iscenario] = snow_age_sfstart
    melt_age_fields[:, iscenario] = melt_age_nomelt
    snow_depth_fields[:, iscenario] = snow_depth_thin
    rho_fields[:, iscenario] = rho_field_light
    temp_fields[:, iscenario] = temperature_field_cold
    melt_mask_fields[:, iscenario] = melt_mask_nomelt
    day_fields[:, iscenario] = days

    # Scenario 2: thin snow layer with snowfall day 1, no melting
    # snowdepth < SNOW_DEPTH_THRESHOLD
    # rho_surface = RHO_ICE
    iscenario = 2
    snow_age_fields[:, iscenario] = snow_age_sfstart #000_010
    melt_age_fields[:, iscenario] = melt_age_nomelt
    snow_depth_fields[:, iscenario] = snow_depth_thin
    rho_fields[:, iscenario] = rho_field_heavy
    temp_fields[:, iscenario] = temperature_field_cold
    melt_mask_fields[:, iscenario] = melt_mask_nomelt
    day_fields[:, iscenario] = days

    # Scenario 3: ice layer with snowfall day 1, no melting
    # snowdepth < SNOW_DEPTH_THRESHOLD
    # rho_surface = RHO_SNOW
    iscenario = 3
    snow_age_fields[:, iscenario] = snow_age_sfstart #000_010
    melt_age_fields[:, iscenario] = melt_age_nomelt
    snow_depth_fields[:, iscenario] = snow_depth_zero
    rho_fields[:, iscenario] = rho_field_heavy
    temp_fields[:, iscenario] = temperature_field_cold
    day_fields[:, iscenario] = days

    #
    # -- Melting scenarios
    #
    # Scenario 4: infinite snow layer, snowfall step 1, melting step 2:
    # snowdepth >> SNOW_DEPTH_THRESHOLD
    iscenario = 4
    snow_age_fields[:, iscenario] = snow_age_sfstart #  yesterday
    melt_age_fields[:, iscenario] = melt_age_afterstart
    snow_depth_fields[:, iscenario] = snow_depth_inf
    rho_fields[:, iscenario] = rho_field_light
    temp_fields[:, iscenario] = temperature_field_warm
    melt_mask_fields[:, iscenario] = melt_mask_afterstart
    day_fields[:, iscenario] = days


    # Scenario 5: thin snow layer, snowfall step 1, melting step 2:
    # snowdepth < SNOW_DEPTH_THRESHOLD
    iscenario = 5
    snow_age_fields[:, iscenario] = snow_age_sfstart
    melt_age_fields[:, iscenario] = melt_age_afterstart
    snow_depth_fields[:, iscenario] = snow_depth_thin
    rho_fields[:, iscenario] = rho_field_heavy
    temp_fields[:, iscenario] = temperature_field_warm
    melt_mask_fields[:, iscenario] = melt_mask_afterstart
    day_fields[:, iscenario] = days

    # Scenario 6: ice layer, snowfall step 1, melting step 2:
    # snowdepth = 0
    iscenario = 6
    snow_age_fields[:, iscenario] = snow_age_sfstart
    melt_age_fields[:, iscenario] = melt_age_afterstart
    snow_depth_fields[:, iscenario] = snow_depth_zero
    rho_fields[:, iscenario] = rho_field_heavy
    temp_fields[:, iscenario] = temperature_field_warm
    melt_mask_fields[:, iscenario] = - np.abs(melt_mask_afterstart)
    day_fields[:, iscenario] = days

    #
    # -- Refreezing scenarios
    #
    # Scenario 7: extended melting infinite snowlayer no snowfall since 3 months, refreezing day 10
    # snowdepth >> SNOW_DEPTH_THRESHOLD
    iscenario = 7
    snow_age_fields[:, iscenario] = snow_age_sf3monthago
    melt_age_fields[:, iscenario] = melt_age_3month
    snow_depth_fields[:, iscenario] = snow_depth_inf
    rho_fields[:, iscenario] = rho_field_light
    temp_fields[:, iscenario] = temperature_field_warm2cold_refrz
    melt_mask_fields[:, iscenario] = melt_mask_3month
    day_fields[:, iscenario] = days

    # Scenario 8: extended melting thin snowlayer no snowfall since 3 months, refreezing day 10
    # snowdepth < SNOW_DEPTH_THRESHOLD
    iscenario = 8
    snow_age_fields[:, iscenario] = snow_age_sf3monthago
    melt_age_fields[:, iscenario] = melt_age_3month
    snow_depth_fields[:, iscenario] = snow_depth_thin
    rho_fields[:, iscenario] = rho_field_heavy
    temp_fields[:, iscenario] = temperature_field_warm2cold_refrz
    melt_mask_fields[:, iscenario] = melt_mask_3month
    day_fields[:, iscenario] = days

    # Scenario 9: extended melting ice layer no snowfall since 3 months, refreezing day 10
    # snowdepth = 0
    iscenario = 9
    snow_age_fields[:, iscenario] = snow_age_sf3monthago
    melt_age_fields[:, iscenario] = melt_age_3month
    snow_depth_fields[:, iscenario] = snow_depth_zero
    rho_fields[:, iscenario] = rho_field_heavy
    temp_fields[:, iscenario] = temperature_field_warm2cold_refrz
    melt_mask_fields[:, iscenario] = - np.abs(melt_mask_3month)
    day_fields[:, iscenario] = days

    #
    # --- Compound scenario
    #
    # Scenario 10
    iscenario = 10

    snowd_inf2 = 1.5*snowdepth_threshold
    snowd_thin2 = 0.25*snowdepth_threshold

    # Defined events at given days
    days2_of_pre_melting_days = days_of_pre_melting_days

    day2_of_refreezing_ice = 10
    day2_of_snowfall_on_thin = day2_of_refreezing_ice + 30
    day2_of_snowfall_off_thin = day2_of_snowfall_on_thin + 10
    day2_of_decay_thin = day2_of_snowfall_off_thin #!!+ 0
    day2_of_melting_thin = day2_of_decay_thin + 50
    day2_of_refreezing_thin = day2_of_melting_thin + 30
    day2_of_snowfall_on_thick1 = day2_of_refreezing_thin + 25
    day2_of_snowfall_off_thick1 = day2_of_snowfall_on_thick1 +10
    day2_of_decay_thick1 = day2_of_snowfall_off_thick1 #!!+ 0
    day2_of_melting_thick1 = day2_of_decay_thick1 + 50
    day2_of_refreezing_thick1 = day2_of_melting_thick1 + 20
    day2_of_snowfall_on_thick2 = day2_of_refreezing_thick1 + 50
    day2_of_snowfall_off_thick2 = day2_of_snowfall_on_thick2 + 10
    day2_of_decay_thick2 = day2_of_snowfall_off_thick2 + 100


    # Time-axis: day2, seconds2
    days2 = np.linspace(0, day2_of_decay_thick2, NO_TIME_STEPS, dtype=float) # days
    seconds2 = days2*86400.
    dt_second2 = seconds[1]-seconds[0]

    # Index on time axis of defined events at given days
    # idx2_refrez_ice = int(np.where(abs(days2-day2_of_refreezing_ice) ==
    #                                min(abs(days2-day2_of_refreezing_ice)))[0])
    idx2_snowf1_thin = int(np.where(abs(days2-day2_of_snowfall_on_thin) ==
                                    min(abs(days2-day2_of_snowfall_on_thin)))[0])
    idx2_snowf0_thin = int(np.where(abs(days2-day2_of_snowfall_off_thin) ==
                                    min(abs(days2-day2_of_snowfall_off_thin)))[0])
    idx2_decay_thin = int(np.where(abs(days2-day2_of_decay_thin) ==
                                    min(abs(days2-day2_of_decay_thin)))[0])
    idx2_melt1_thin = int(np.where(abs(days2-day2_of_melting_thin) ==
                                   min(abs(days2-day2_of_melting_thin)))[0])
    idx2_refrez_thin = int(np.where(abs(days2-day2_of_refreezing_thin) ==
                                    min(abs(days2-day2_of_refreezing_thin)))[0])
    idx2_melt0_thin = idx2_refrez_thin
    idx2_snowf1_thick1 = int(np.where(abs(days2-day2_of_snowfall_on_thick1) ==
                                      min(abs(days2-day2_of_snowfall_on_thick1)))[0])
    idx2_snowf0_thick1 = int(np.where(abs(days2-day2_of_snowfall_off_thick1) ==
                                      min(abs(days2-day2_of_snowfall_off_thick1)))[0])
    idx2_decay_thick1 = int(np.where(abs(days2-day2_of_decay_thick1) ==
                                      min(abs(days2-day2_of_decay_thick1)))[0])
    idx2_melt1_thick1 = int(np.where(abs(days2-day2_of_melting_thick1) ==
                                     min(abs(days2-day2_of_melting_thick1)))[0])
    idx2_refrez_thick1 = int(np.where(abs(days2-day2_of_refreezing_thick1) ==
                                      min(abs(days2-day2_of_refreezing_thick1)))[0])
    idx2_melt0_thick1 = idx2_refrez_thick1
    idx2_snowf1_thick2 = int(np.where(abs(days2-day2_of_snowfall_on_thick2) ==
                                      min(abs(days2-day2_of_snowfall_on_thick2)))[0])
    idx2_snowf0_thick2 = int(np.where(abs(days2-day2_of_snowfall_off_thick2) ==
                                      min(abs(days2-day2_of_snowfall_off_thick2)))[0])
    idx2_decay_thick2 = int(np.where(abs(days2-day2_of_decay_thick2) ==
                                      min(abs(days2-day2_of_decay_thick2)))[0])

    snow_depth = np.zeros_like(seconds2) + snowd_zero
    # Linear increasing snow depth
    snow_depth[idx2_snowf1_thin:idx2_snowf0_thin] = \
        [snowd_thin2*(val-idx2_snowf1_thin)/(idx2_snowf0_thin-idx2_snowf1_thin)
         for val in range(idx2_snowf1_thin, idx2_snowf0_thin)]
    snow_depth[idx2_snowf0_thin:] = snowd_thin2
    # Linear increasing snow depth
    snow_depth[idx2_snowf1_thick1:idx2_snowf0_thick1] = \
        [snowd_inf2+(snowd_inf2-snow_depth[idx2_snowf1_thick1])*
          (val-idx2_snowf0_thick1)/(idx2_snowf0_thick1-idx2_snowf1_thick1)
          for val in range(idx2_snowf1_thick1, idx2_snowf0_thick1)]
    snow_depth[idx2_snowf0_thick1:] = snowd_inf2
    snow_depth[idx2_snowf1_thick2:idx2_snowf0_thick2] = \
        [snowd_inf2+0.925*
         (val-idx2_snowf1_thick2)/(idx2_snowf0_thick2-idx2_snowf1_thick2)
         for val in range(idx2_snowf1_thick2, idx2_snowf0_thick2)]
    snow_depth[idx2_snowf0_thick2:] = snowd_inf2+0.925

    snow_age = np.cumsum(np.ones_like(seconds2)+dt_second2)+ \
        days2_of_pre_melting_days*86400.
    snow_age[idx2_snowf1_thin:idx2_snowf0_thin] = 0
    snow_age[idx2_snowf0_thin:idx2_snowf1_thick1] = dt_second2
    snow_age[idx2_snowf0_thin:idx2_snowf1_thick1] = \
        np.cumsum(snow_age[idx2_snowf0_thin:idx2_snowf1_thick1])
    snow_age[idx2_snowf1_thick1:idx2_snowf0_thick1] = 0
    snow_age[idx2_snowf0_thick1:idx2_snowf1_thick2] = dt_second2
    snow_age[idx2_snowf0_thick1:idx2_snowf1_thick2] = \
        np.cumsum(snow_age[idx2_snowf0_thick1:idx2_snowf1_thick2])
    snow_age[idx2_snowf1_thick2:idx2_snowf0_thick2] = 0
    snow_age[idx2_snowf0_thick2:] = dt_second2
    snow_age[idx2_snowf0_thick2:] = np.cumsum(snow_age[idx2_snowf0_thick2:])

    snowfall_event = np.zeros_like(seconds2, dtype=int)
    snowfall_event[idx2_snowf1_thin: idx2_snowf0_thin] = 1
    snowfall_event[idx2_snowf1_thick1: idx2_snowf0_thick1] = 1
    snowfall_event[idx2_snowf1_thick2: idx2_snowf0_thick2] = 1

    melting_event = np.zeros_like(seconds2, dtype=int)
    melting_event[idx2_melt1_thin: idx2_melt0_thin] = 1
    melting_event[idx2_melt1_thick1: idx2_melt0_thick1] = 1


    rho_ = np.zeros_like(seconds2) + rho_heavy
    rho_[idx2_snowf1_thick1:] = rho_light
    # Linear increasing
    rho_[idx2_snowf0_thick1: idx2_snowf1_thick2] = \
        [rho_light+(rho_heavy-rho_light)*
         (val-idx2_snowf0_thick1)/(idx2_snowf1_thick2-idx2_snowf0_thick1)
         for val in range(idx2_snowf0_thick1, idx2_snowf1_thick2)]
    # Linear decreasing
    rho_[idx2_snowf1_thick2: idx2_snowf0_thick2] = \
        [rho_heavy+(rho_light-rho_heavy)*
          (val-idx2_snowf1_thick2)/(idx2_snowf0_thick2-idx2_snowf1_thick2)
          for val in range(idx2_snowf1_thick2, idx2_snowf0_thick2)]

    melt_age = np.cumsum(np.ones_like(seconds2)+dt_second2) \
        +days_of_pre_melting_days*86400.
    melt_age[idx2_snowf1_thin:] = 0.
    melt_age[idx2_melt0_thin:] = dt_second2
    melt_age[idx2_melt0_thin:] = np.cumsum(melt_age[idx2_melt0_thin:])
    melt_age[idx2_snowf1_thick1:] = 0.
    melt_age[idx2_melt0_thick1:] = dt_second2
    melt_age[idx2_melt0_thick1:] = np.cumsum(melt_age[idx2_melt0_thick1:])
    melt_age[idx2_snowf1_thick2:] = 0.

    melt_mask = np.ones_like(seconds2, dtype=int)
    melt_mask[idx2_snowf1_thin:] = 0
    melt_mask[idx2_melt1_thin:] = 1
    melt_mask[idx2_snowf1_thick1:] = 0
    melt_mask[idx2_melt1_thick1:] = 1
    melt_mask[idx2_snowf1_thick2:] = 0

    temp_ = np.zeros_like(seconds2) + temperature_cold
    # temp_[idx2_snowf1_thin:idx2_melt1_thin] = \
    #     [(temperature_warm)+(temperature_cold-temperature_warm)*
    #      (val-idx2_snowf1_thin)/(idx2_melt1_thin-idx2_snowf1_thin)
    #      for val in range(idx2_snowf1_thin, idx2_melt1_thin)]
    temp_[idx2_snowf1_thin:idx2_melt1_thin] = \
        [(temperature_warm)+
         (temperature_cold-temperature_warm)* 0.25*
         (val-idx2_snowf1_thin)/(idx2_melt1_thin-idx2_snowf1_thin)
         for val in range(idx2_snowf1_thin, idx2_melt1_thin)]
    temp_[idx2_snowf1_thick1:idx2_snowf0_thick1] = \
        [(temperature_warm)+(temperature_cold-temperature_warm)*
         (val-idx2_snowf1_thick1)/(idx2_snowf0_thick1-idx2_snowf1_thick1)
         for val in range(idx2_snowf1_thick1, idx2_snowf0_thick1)]
    # temp_[melt_mask==1] = temperature_warm
    temp_[idx2_melt1_thin:idx2_melt0_thin] = temperature_warm
    temp_[idx2_melt1_thick1:idx2_melt0_thick1] = temperature_warm

    snow_age_fields[:, iscenario] = snow_age
    melt_age_fields[:, iscenario] = melt_age
    snow_depth_fields[:, iscenario] = snow_depth
    rho_fields[:, iscenario] = rho_
    temp_fields[:, iscenario] = temp_
    melt_mask_fields[:, iscenario] = melt_mask
    day_fields[:, iscenario] = days2

    del snow_age, melt_age, snow_depth, rho_, melt_mask, temp_


    #
    # Compute the albedo for the constructed scenarios
    #

    labels = []
    for iscenario in range(NUMBER_OF_SCENARIOS):
        #
        # Build the label
        #
        if iscenario == 0:
            if albedo_scheme == 'albedo_rodehacke1':
                labels.append('fresh snow over snow ($\\rho=${:g}$\\,kg\\,m^3$)'.
                              format(rho_fields[0, iscenario]))
            else:
                labels.append('fresh snow $d_{{snow}}\\gg${:.2f}$\\,m$'.
                              format(snowdepth_threshold)+
                              ', $\\rho=${:g}$\\,kg\\,m^3$'.format(rho_fields[0, iscenario]))
        elif iscenario == 1:
            labels.append('fresh snow on snow $d_{{snow}}<${:.2f}$\\,m$'.
                          format(snowdepth_threshold)+
                          ', $\\rho=${:g} $kg\\,m^3$'.format(rho_fields[0, iscenario]))
        elif iscenario == 2:
            if albedo_scheme == 'albedo_rodehacke1':
                labels.append('fresh snow over ice ($\\rho=${:g} $kg\\,m^3$)'.
                              format(rho_fields[0, iscenario]))
            else:
                labels.append('fresh snow on ice $d_{{snow}}<${:.2f}$\\,m$'.
                              format(snowdepth_threshold)+
                              ', $\\rho=${:g} $kg\\,m^3$'.format(rho_fields[0, iscenario]))
        elif iscenario == 3:
            labels.append('ice $d_{{snow}}=0\\,m$, $\\rho=${:g} $kg\\,m^3$'.
                          format(rho_fields[0, iscenario]))
        elif iscenario == 4:
            if albedo_scheme == 'albedo_rodehacke1':
                labels.append('melting snow $d_{{snow}}\\gg${:.2f}$\\,m$'.
                              format(snowdepth_threshold))
            else:
                labels.append('melting fresh snow $d_{{snow}}\\gg${:.2f}$\\,m$'.
                              format(snowdepth_threshold))
        elif iscenario == 5:
            labels.append('melting snow $d_{{snow}}<${:.2f}$\\,m$'.
                          format(snowdepth_threshold))
        elif iscenario == 6:
            labels.append('melting ice $d_{{snow}}=0\\,m$')
        elif iscenario == 7:
            labels.append('melting snow $d_{{snow}}\\gg${:.2f}$\\,m$ for '.
                          format(snowdepth_threshold)+
                          str(days_of_pre_melting_days+day_of_refreezing)+' days')
        elif iscenario == 8:
            labels.append('melting snow $d_{{snow}}<${:.2f}$\\,m$ for '.
                          format(snowdepth_threshold)+
                          str(days_of_pre_melting_days+day_of_refreezing)+' days')
        elif iscenario == 9:
            labels.append('melting ice $d_{{snow}}=0\\,m$ for '+
                         str(days_of_pre_melting_days+day_of_refreezing)+' days')
        else:
            labels.append(str(iscenario))


        #
        # Compute
        #
        if albedo_scheme == 'albedo_rodehacke1':
            if iscenario in [-9]: #[0, 1, 2, 3]:
                # Vectorized code works for these cases
                albedo_fields[:, iscenario] = \
                    mod_albedo.albedo_rodehacke1(snow_age_fields[:, iscenario],
                                                 melt_age_fields[:, iscenario],
                                                 snow_depth_fields[:, iscenario],
                                                 rho_fields[:, iscenario],
                                                 temp_fields[:, iscenario],
                                                 melt_mask_fields[:, iscenario],
                                                 temperature_field_melt,
                                                 snowdepth_threshold)
            else:
                for ic in range(NO_TIME_STEPS):
                    albedo_fields[ic, iscenario] = \
                    mod_albedo.albedo_rodehacke1(snow_age_fields[ic, iscenario],
                                                 melt_age_fields[ic, iscenario],
                                                 snow_depth_fields[ic, iscenario],
                                                 rho_fields[ic, iscenario],
                                                 temp_fields[ic, iscenario],
                                                 melt_mask_fields[ic, iscenario],
                                                 temperature_field_melt[ic],
                                                 snowdepth_threshold)
        elif albedo_scheme == 'albedo_rodehacke2':
            if iscenario in [-9]: #[0, 1, 2, 3]:
                # Vectorized code works for these cases
                albedo_fields[:, iscenario] = \
                    mod_albedo.albedo_rodehacke2(snow_age_fields[:, iscenario],
                                                 melt_age_fields[:, iscenario],
                                                 snow_depth_fields[:, iscenario],
                                                 rho_fields[:, iscenario],
                                                 temp_fields[:, iscenario],
                                                 melt_mask_fields[:, iscenario],
                                                 temperature_field_melt,
                                                 snowdepth_threshold)
            else:
                for ic in range(NO_TIME_STEPS):
                    albedo_fields[ic, iscenario] = \
                    mod_albedo.albedo_rodehacke2(snow_age_fields[ic, iscenario],
                                                 melt_age_fields[ic, iscenario],
                                                 snow_depth_fields[ic, iscenario],
                                                 rho_fields[ic, iscenario],
                                                 temp_fields[ic, iscenario],
                                                 melt_mask_fields[ic, iscenario],
                                                 temperature_field_melt[ic],
                                                 snowdepth_threshold)
        elif albedo_scheme == 'albedo_rodehacke3':
            if iscenario in [-9]: # [0, 1, 2, 3]:
                # Vectorized code works for these cases
                albedo_fields[:, iscenario] = \
                    mod_albedo.albedo_rodehacke3(snow_age_fields[:, iscenario],
                                                  melt_age_fields[:, iscenario],
                                                  snow_depth_fields[:, iscenario],
                                                  rho_fields[:, iscenario],
                                                  temp_fields[:, iscenario],
                                                  melt_mask_fields[:, iscenario],
                                                  temperature_field_melt,
                                                  snowdepth_threshold)
            else:
                for ic in range(NO_TIME_STEPS):
                    albedo_fields[ic, iscenario] = \
                    mod_albedo.albedo_rodehacke3(snow_age_fields[ic, iscenario],
                                                 melt_age_fields[ic, iscenario],
                                                 snow_depth_fields[ic, iscenario],
                                                 rho_fields[ic, iscenario],
                                                 temp_fields[ic, iscenario],
                                                 melt_mask_fields[ic, iscenario],
                                                 temperature_field_melt[ic],
                                                 snowdepth_threshold)

    #
    # Figures
    #
    do_figure1 = True
    do_figure2 = True
    do_figure3 = True
    do_figure4 = True

    #
    # Figure 1: Decay
    #
    if do_figure1:
        fig1 = plt.figure(dpi=300, facecolor='white')
        axes1 = fig1.subplots(2, 1, sharex=True)
        fig1.subplots_adjust(hspace=0.1)
        iscenario_list = [0, 1, 2, 3]
        if albedo_scheme == 'albedo_rodehacke1':
            iscenario_list = [0, 2]

        #
        # Upper subplot
        #
        iax = 0
        ilines = axes1[iax].plot(day_fields[:, iscenario],
                                 albedo_fields[:, iscenario_list], lw=2)
        for icount, iline in enumerate(ilines):
            if icount in [0, 2]:
                iline.set_linestyle('-')
            else:
                iline.set_linestyle('--')

        label = [labels[indx] for indx in iscenario_list]
        axes1[iax].legend(label, loc='upper right') #, bbox_to_anchor=(1.2, 1.0))
        axes1[iax].set_title(albedo_scheme+' (metamorphose, no melting)')
        axes1[iax].set_ylabel('Albedo $\\alpha$')

        axes1[iax].plot(-1, ALBEDO_FRESHSNOW,'x')
        axes1[iax].plot(day_fields[-1, iscenario], ALBEDO_FIRN,'x')
        axes1[iax].plot(day_fields[-1, iscenario], ALBEDO_ICE,'x')
        axes1[iax].text(-1, ALBEDO_FRESHSNOW, '$\\alpha_{{fresh\\, snow}}$')
        axes1[iax].text(day_fields[-3, iscenario], ALBEDO_FIRN, '$\\alpha_{{firn}}$')
        axes1[iax].text(day_fields[-3, iscenario], ALBEDO_ICE, '$\\alpha_{{ice}}$')

        #
        # 2nd upper subplot
        #
        iax = 1
        axes1[iax].plot(day_fields[:, iscenario],
                        snow_age_fields[:, iscenario_list]/86400., '-',
                        lw=2, c='mediumblue')
        axes1[iax].set_ylabel('$t_{{snow}}$ (days)', color='mediumblue')
        axes1[iax].tick_params(axis='y', color='mediumblue',
                               labelcolor='mediumblue')
        ax_add = axes1[iax].twinx()
        ax_add.plot(day_fields[:, iscenario],
                    melt_age_fields[:, iscenario_list]/86400., '--',
                    lw=2, c='firebrick')
        ax_add.set_ylabel('$t_{{melt}}$ (days)', c='firebrick')
        ax_add.tick_params(axis='y', color='firebrick', labelcolor='firebrick')

        if len(axes1)>=3:
            iax = 2
            axes1[iax].plot(day_fields[:, iscenario],
                            melt_mask_fields[:, iscenario_list], '-')
            axes1[iax].set_ylabel('$d_{{snow}}$ (m)')

        axes1[-1].set_xlabel('Time (day)')

        if plot_suffix:
            module4plots.plotting(fig1, plotname+'_decay', plot_suffix)

    #
    # Figure 2: Melting
    #
    if do_figure2:
        fig2 = plt.figure(dpi=300, facecolor='white')
        axes1 = fig2.subplots(2, 1, sharex=True)
        fig2.subplots_adjust(hspace=0.1)
        iscenario_list = [4, 5, 6]
        if albedo_scheme == 'albedo_rodehacke1':
            iscenario_list = [4, 5]

        #
        # Upper subplot
        #
        iax = 0
        ilines = axes1[iax].plot(day_fields[:, iscenario],
                                 albedo_fields[:, iscenario_list], lw=2)
        for icount, iline in enumerate(ilines):
            if icount in [1]:
                iline.set_linestyle('-')
            elif icount in [2]:
                iline.set_linestyle('-.')

        label = [labels[indx] for indx in iscenario_list]
        axes1[iax].legend(label, loc='upper right',
                          title='Melting since day '+str(day_of_melting))
        axes1[iax].set_title(albedo_scheme+' (melting)')
        axes1[iax].set_ylabel('Albedo $\\alpha$')


        axes1[iax].plot(-1, ALBEDO_FRESHSNOW,'x')
        # axes1[iax].plot(day_fields[-1, iscenario], ALBEDO_FIRN,'x')
        # axes1[iax].plot(day_fields[-1, iscenario], ALBEDO_ICE,'x')
        axes1[iax].plot(day_fields[-1, iscenario], ALBEDO_SNOW_MELTING ,'x')
        axes1[iax].plot(day_fields[-1, iscenario], ALBEDO_ICE_MELTING,'x')
        axes1[iax].text(-1, ALBEDO_FRESHSNOW, '$\\alpha_{{fresh\\, snow}}$')
        # axes1[iax].text(-1, ALBEDO_FIRN, '$\\alpha_{{firn}}$')
        # axes1[iax].text(-1, ALBEDO_ICE, '$\\alpha_{{ice}}$')
        axes1[iax].text(day_fields[-3, iscenario], ALBEDO_SNOW_MELTING, '$\\alpha_{{snow\\, melt}}$')
        axes1[iax].text(day_fields[-3, iscenario], ALBEDO_ICE_MELTING, '$\\alpha_{{ice\\, melt}}$')

        #
        # 2nd upper subplot
        #
        iax = 1
        axes1[iax].plot(day_fields[:, iscenario],
                        snow_age_fields[:, iscenario_list]/86400., '-',
                        lw=2, c='mediumblue')
        axes1[iax].set_ylabel('$t_{{snow}}$ (days)', color='mediumblue')
        axes1[iax].tick_params(axis='y', color='mediumblue',
                               labelcolor='mediumblue')
        ax_add = axes1[iax].twinx()
        ax_add.plot(day_fields[:, iscenario],
                    melt_age_fields[:, iscenario_list]/86400., '--',
                    lw=2, c='firebrick')
        ax_add.set_ylabel('$t_{{melt}}$ (days)', c='firebrick')
        ax_add.tick_params(axis='y', color='firebrick', labelcolor='firebrick')

        if len(axes1)>=3:
            iax = 2
            axes1[iax].plot(day_fields[:, iscenario],
                            melt_mask_fields[:, iscenario_list], '-')
            axes1[iax].set_ylabel('$d_{{snow}}$ (m)')

        axes1[-1].set_xlabel('Time (day)')

        if plot_suffix:
            module4plots.plotting(fig2, plotname+'_melt', plot_suffix)

    #
    # Figure 3: Refreezing
    #
    if do_figure3:
        fig3 = plt.figure(dpi=300, facecolor='white')
        axes1 = fig3.subplots(2, 1, sharex=True)
        fig3.subplots_adjust(hspace=0.1)
        # Scenario 8 and 9 show the same temporal evolution
        # iscenario_list = [7, 8, 9] #, 10]
        # if albedo_scheme == 'albedo_rodehacke1':
        #     iscenario_list = [7, 8]
        iscenario_list = [7, 8]

        #
        # Upper subplot
        #
        iax = 0
        ilines = axes1[iax].plot(day_fields[:, iscenario],
                                 albedo_fields[:, iscenario_list], lw=2)
        for icount, iline in enumerate(ilines):
            if icount in [1]:
                iline.set_linestyle('-')
            elif icount in [2]:
                iline.set_linestyle('--')

        label = [labels[indx] for indx in iscenario_list]
        axes1[iax].legend(label, loc='center right',
                          title='Refreezing since day '+
                          str(day_of_refreezing))
        axes1[iax].set_title(albedo_scheme+' (refreezing)')
        axes1[iax].set_ylabel('Albedo $\\alpha$')

        # axes1[iax].plot(-1, ALBEDO_FRESHSNOW,'x')
        # axes1[iax].plot(day_fields[-1, iscenario], ALBEDO_FIRN,'x')
        # axes1[iax].plot(day_fields[-1, iscenario], ALBEDO_ICE,'x')
        axes1[iax].plot(-1, ALBEDO_SNOW_MELTING ,'x')
        axes1[iax].plot(-1, ALBEDO_ICE_MELTING,'x')
        axes1[iax].plot(day_fields[-1, iscenario],
                        (2.*ALBEDO_SNOW_REFROZEN+ALBEDO_SNOW_MELTING)/3, 'x')
        axes1[iax].plot(day_fields[-3, iscenario],
                        (2.*ALBEDO_ICE_REFROZEN+ALBEDO_ICE_MELTING)/3, 'x')
        # axes1[iax].text(-1, ALBEDO_FRESHSNOW, '$\\alpha_{{fresh\\, snow}}$')
        # axes1[iax].text(-1, ALBEDO_FIRN, '$\\alpha_{{firn}}$')
        # axes1[iax].text(-1, ALBEDO_ICE, '$\\alpha_{{ice}}$')
        axes1[iax].text(-1, ALBEDO_SNOW_MELTING, '$\\alpha_{{snow\\, melt}}$')
        axes1[iax].text(-1, ALBEDO_ICE_MELTING, '$\\alpha_{{ice\\, melt}}$')
        # axes1[iax].text(-1, ALBEDO_SNOW_REFROZEN, '$\\alpha_{{snow\\, refrz}}$')
        # axes1[iax].text(-1, ALBEDO_ICE_REFROZEN, '$\\alpha_{{ice\\, refrz}}$')
        axes1[iax].text(day_fields[-3, iscenario],
                        (2.*ALBEDO_SNOW_REFROZEN+ALBEDO_SNOW_MELTING)/3,
                        '$\\frac{2}{3}\\alpha_{{snow\\, refrz}} + \\frac{1}{3}\\alpha_{{snow\\, melt}}$',
                        ha='right')
        axes1[iax].text(day_fields[-3, iscenario],
                        (2.*ALBEDO_ICE_REFROZEN+ALBEDO_ICE_MELTING)/3,
                        '$\\frac{2}{3}\\alpha_{{ice\\, refrz}} + \\frac{1}{3}\\alpha_{{ice\\, melt}}$',
                        ha='right' )
        #
        # 2nd upper subplot
        #
        iax = 1
        axes1[iax].plot(day_fields[:, iscenario],
                        snow_age_fields[:, iscenario_list]/86400., '-',
                        lw=2, c='mediumblue')
        axes1[iax].set_ylabel('$t_{{snow}}$ (days)', color='mediumblue')
        axes1[iax].tick_params(axis='y', color='mediumblue',
                               labelcolor='mediumblue')
        ax_add = axes1[iax].twinx()
        ax_add.plot(day_fields[:, iscenario],
                    melt_age_fields[:, iscenario_list]/86400., '--',
                    lw=2, c='firebrick')
        ax_add.set_ylabel('$t_{{melt}}$ (days)', c='firebrick')
        ax_add.tick_params(axis='y', color='firebrick', labelcolor='firebrick')

        if len(axes1)>=3:
            iax = 2
            axes1[iax].plot(day_fields[:, iscenario],
                            melt_mask_fields[:, iscenario_list], '-')
            axes1[iax].set_ylabel('$d_{{snow}}$ (m)')

            ax_add = axes1[iax].twinx()
            ax_add.plot(day_fields[:, iscenario],
                        temp_fields[:, iscenario_list]-FREEZING_TEMP, ':',
                        lw=2, c='firebrick')
            ax_add.set_ylabel('$T$ ($^{{\degree}}\\!C$)', c='firebrick')
            ax_add.tick_params(axis='y', color='firebrick', labelcolor='firebrick')

        axes1[-1].set_xlabel('Time (day)')

        if plot_suffix:
            module4plots.plotting(fig3, plotname+'_refrez', plot_suffix)

    #
    # Figure 4: Combined scenario
    #
    if do_figure4:
        fig4 = plt.figure(dpi=300, facecolor='white', figsize=(4.8, 6.4),
                          tight_layout=True)
        axes1 = fig4.subplots(4, 1, sharex=True)
        fig4.subplots_adjust(hspace=0.1)
        iscenario_list = [10]

        #
        # Upper subplot
        #
        iax = 0
        ilines = axes1[iax].plot(day_fields[:, iscenario],
                                 albedo_fields[:, iscenario_list],
                                 linewidth=2, color='darkslategray')
        axes1[iax].set_title(albedo_scheme+' (scenario)')
        axes1[iax].set_ylabel('Albedo $\\alpha$')

        # axes1[iax].plot(-1, ALBEDO_FRESHSNOW,'x')
        # axes1[iax].plot(day_fields[-1, iscenario], ALBEDO_FIRN,'x')
        # axes1[iax].plot(day_fields[-1, iscenario], ALBEDO_ICE,'x')
        # axes1[iax].plot(-1, ALBEDO_SNOW_MELTING ,'x')
        # axes1[iax].plot(-1, ALBEDO_ICE_MELTING,'x')
        # axes1[iax].plot(day_fields[-1, iscenario],
        #                 (2.*ALBEDO_SNOW_REFROZEN+ALBEDO_SNOW_MELTING)/3, 'x')
        # axes1[iax].plot(day_fields[-1, iscenario],
        #                 (2.*ALBEDO_ICE_REFROZEN+ALBEDO_ICE_MELTING)/3, 'x')
        # axes1[iax].text(-1, ALBEDO_FRESHSNOW, '$\\alpha_{{fresh\\, snow}}$')
        # axes1[iax].text(-1, ALBEDO_FIRN, '$\\alpha_{{firn}}$')
        # axes1[iax].text(-1, ALBEDO_ICE, '$\\alpha_{{ice}}$')
        # axes1[iax].text(-1, ALBEDO_SNOW_MELTING, '$\\alpha_{{snow\\, melt}}$')
        # axes1[iax].text(-1, ALBEDO_ICE_MELTING, '$\\alpha_{{ice\\, melt}}$')
        # axes1[iax].text(-1, ALBEDO_SNOW_REFROZEN, '$\\alpha_{{snow\\, refrz}}$')
        # axes1[iax].text(-1, ALBEDO_ICE_REFROZEN, '$\\alpha_{{ice\\, refrz}}$')
        # axes1[iax].text(day_fields[-3, iscenario],
        #                 (2.*ALBEDO_SNOW_REFROZEN+ALBEDO_SNOW_MELTING)/3,
        #                 '$\\frac{2}{3}\\alpha_{{snow\\, refrz}} + \\frac{1}{3}\\alpha_{{snow\\, melt}}$',
        #                 ha='right')
        # axes1[iax].text(day_fields[-3, iscenario],
        #                 (2.*ALBEDO_ICE_REFROZEN+ALBEDO_ICE_MELTING)/3,
        #                 '$\\frac{2}{3}\\alpha_{{ice\\, refrz}} + \\frac{1}{3}\\alpha_{{ice\\, melt}}$',
        #                 ha='right' )

        # Event axis
        ax_add = axes1[iax].twinx()
        # -- snowfall
        not_zero = (snowfall_event>0)
        ax_add.plot(day_fields[not_zero, iscenario], snowfall_event[not_zero],
                    '*', lw=2, c='navy')
        # -- melting
        not_zero = (melting_event>0)
        ax_add.plot(day_fields[not_zero, iscenario], melting_event[not_zero],
                    '.', lw=2, c='crimson')
        ax_add.set_ylim([0.5, 1.2]) # Moves the Marks with y-values of 1 upward #Down: ([0.95, 1.5])
        ax_add.set_yticks([1])
        ax_add.set_yticklabels(['event'], c='darkgray')
        ax_add.legend(['snowfall', 'melting'], loc='lower right')

        #
        # 2nd upper subplot
        #
        iax = 1
        axes1[iax].plot(day_fields[:, iscenario],
                        temp_fields[:, iscenario_list], '-', lw=2, c='orangered') #c='darkcyan')
        axes1[iax].set_ylabel('$T_{{snow}}$ (K)', color='orangered')
        axes1[iax].tick_params(axis='y', color='orangered',
                               labelcolor='orangered')
        axes1[iax].plot(day_fields[:, iscenario],
                        temp_fields[:, iscenario_list]*0.+FREEZING_TEMP, ':',
                        lw=1, c='orangered')
        axes1[iax].text(day_fields[-1, iscenario], FREEZING_TEMP,
                        '$T_{{freeze}}$',c='orangered', ha='right', va='top')

        ax_add = axes1[iax].twinx()
        ax_add.plot(day_fields[:, iscenario], rho_fields[:, iscenario_list],
                    '--', lw=2, c='purple')
        #ax_add.set_ylabel('$\\rho_{{surface}}$ (kg m$^{{-3}}$)', c='purple')
        ax_add.set_ylabel('$\\rho_{{below}}$ (kg m$^{{-3}}$)', c='purple')
        ax_add.tick_params(axis='y', color='purple', labelcolor='purple')
        ax_add.text(day_fields[0, iscenario], rho_heavy,
                    '$\\rho_{{ice}}=${:g}'.format(rho_heavy), c='purple',
                    ha='left', va='top')
        ax_add.text(day_fields[-1, iscenario], rho_light,
                    '$\\rho_{{snow}}=${:g}'.format(rho_light), c='purple',
                    ha='right', va='bottom')

        #
        # 3rd upper subplot
        #
        iax = 2
        zero = (snow_depth_fields[:, iscenario]<=0)
        axes1[iax].plot(day_fields[zero, iscenario],
                        snow_depth_fields[zero, iscenario], '--', lw=2,
                        c='deepskyblue', label='ice, $d_{{snow}}=0$')

        nearly_zero = np.logical_and(snow_depth_fields[:, iscenario]>0, \
                        snow_depth_fields[:, iscenario]<SNOW_DEPTH_THRESHOLD)
        axes1[iax].plot(day_fields[nearly_zero, iscenario],
                        snow_depth_fields[nearly_zero, iscenario], '--', lw=3,
                        c='limegreen', label='snow, $d_{{snow}} < d_{{thres}}$')
                        #c='lightseagreen', label='snow, $d_{{snow}} < d_{{thres}}$')

        bigger_than_zero = (snow_depth_fields[:, iscenario]>=SNOW_DEPTH_THRESHOLD)
        axes1[iax].plot(day_fields[bigger_than_zero, iscenario],
                        snow_depth_fields[bigger_than_zero, iscenario], '-', lw=3,
                        c='forestgreen', label='snow, $d_{{snow}}\\geq d_{{thres}}$')
        axes1[iax].set_ylabel('$d_{{snow}}$ (m)', c='darkgreen')
        axes1[iax].tick_params(axis='y', color='darkgreen',
                               labelcolor='darkgreen')
        axes1[iax].legend(loc='upper left')


        ax_add = axes1[iax].twinx()
        # -- Refreeze thicker snow
        not_zero = np.logical_and(bigger_than_zero, melt_age_fields[:, iscenario])
        ax_add.plot(day_fields[not_zero, iscenario], not_zero[not_zero], '^',
                    lw=1, c='indigo', label='$d_{{snow}} \\geq d_{{thres}}$')
        # -- Refreeze thin snow
        not_zero = np.logical_and(nearly_zero, melt_age_fields[:, iscenario])
        ax_add.plot(day_fields[not_zero, iscenario], not_zero[not_zero],'v',
                    lw=1, c='dodgerblue', label='$d_{{snow}} < d_{{thres}}$')

        ax_add.legend(loc='center right', title='Refreeze', bbox_to_anchor=(1.2, 0.5))
        ax_add.set_ylim([0.85, 1.5]) # Moves the Marks with y-values of 1 downward
        ax_add.set_yticks([])

        #
        # 4th upper subplot = bottom subplot
        #
        if len(axes1)>=4:
            iax = 3
            axes1[iax].plot(day_fields[:, iscenario],
                            snow_age_fields[:, iscenario_list]/86400., '-',
                            lw=2, c='mediumblue')
            axes1[iax].set_ylabel('$t_{{snow}}$ (days)', color='mediumblue')
            axes1[iax].tick_params(axis='y', color='mediumblue',
                                   labelcolor='mediumblue')

            ax_add = axes1[iax].twinx()
            ax_add.plot(day_fields[:, iscenario],
                        melt_age_fields[:, iscenario_list]/86400., '--',
                        lw=2.1, c='firebrick')
            ax_add.set_ylabel('$t_{{melt}}$ (days)', c='firebrick')
            ax_add.tick_params(axis='y', color='firebrick', labelcolor='firebrick')

        axes1[-1].set_xlabel('Time (day)')

        #
        # axhspan for all subplot indicating certain events
        #
        for axes in axes1:
            highlight_snowfall = True
            highlight_melting = True
            highlight_refreezing = True
            highlight_decay = False


            if highlight_snowfall:
                # Snowfall
                axes.axvspan(days2[idx2_snowf1_thin],
                             days2[idx2_snowf0_thin],
                             facecolor='cyan', alpha=0.50)
                axes.axvspan(days2[idx2_snowf1_thick1],
                             days2[idx2_snowf0_thick1],
                             facecolor='cyan', alpha=0.50)
                axes.axvspan(days2[idx2_snowf1_thick2],
                             days2[idx2_snowf0_thick2],
                             facecolor='cyan', alpha=0.50)

            if highlight_melting:
                # Melting
                axes.axvspan(days2[idx2_melt1_thin],
                             days2[idx2_melt0_thin],
                             facecolor='crimson', alpha=0.33)
                axes.axvspan(days2[idx2_melt1_thick1],
                             days2[idx2_melt0_thick1],
                             facecolor='crimson', alpha=0.33)

            if highlight_refreezing:
                # Refreezing
                axes.axvspan(days2[idx2_refrez_thin],
                             days2[idx2_snowf1_thick1],
                             facecolor='steelblue', alpha=0.33)
                axes.axvspan(days2[idx2_refrez_thick1],
                             days2[idx2_snowf1_thick2],
                             facecolor='steelblue', alpha=0.33)

            if highlight_decay:
                # Decay
                axes.axvspan(days2[idx2_snowf0_thin],
                             days2[idx2_melt1_thin],
                             facecolor='yellow', alpha=0.15)
                axes.axvspan(days2[idx2_snowf0_thick1],
                             days2[idx2_melt1_thick1],
                             facecolor='yellow', alpha=0.15)
                axes.axvspan(days2[idx2_snowf0_thick2],
                             days2[idx2_decay_thick2],
                             facecolor='gold', alpha=0.15)

        if plot_suffix:
            module4plots.plotting(fig4, plotname+'_scenario', plot_suffix)



# -------------------------------------------------------------------------
#plot_albedo_ecearth1
def plot_albedo_ecearth1(library_name, plot_suffix=None,
                                 plotname='albedo_ecearth1'):
    '''
    Plot results of the via c-binding call Fortran function
    albedo_ecearth1

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'albedo_ecearth1'.

    Returns
    -------
    None.

    '''
    print('* Albedo ecearth1')

    steps = 512
    dtime = np.ones((steps))*21600  # Seconds
    recip_decaytime = np.zeros_like(dtime)+RECIPROCAL_SNOW_DECAYTIME
    trans_temperature = np.zeros_like(dtime)+TEMPERATURE_TRANSITIONAL
    melt_temperature = np.zeros_like(dtime)+FREEZING_TEMP

    snowfall = np.zeros_like(dtime)
    snowfall[0:10] = 1.0
    melt_mask = np.zeros_like(dtime, dtype=int)
    surface_temperature = np.zeros_like(dtime)+ \
        np.floor(FREEZING_TEMP-(TEMPERATURE_TRANSITIONAL+1.0))

    #
    # Melt event
    step_melt_start = int(steps*0.33)
    step_melt_end1 = step_melt_start+21
    step_melt_end2 = step_melt_start+61


    surface_temperature[step_melt_start:step_melt_end1] = FREEZING_TEMP+0.1
    melt_mask[step_melt_start:step_melt_end2] = 1
    snowfall[step_melt_end2:step_melt_end2+5] = 1.0

    albedo_start = ALBEDO_FRESHSNOW
    albedo = np.ones_like(dtime)


    for itime in range(len(dtime)):
        if itime <=0:
            albedo_old = albedo_start
        else:
            albedo_old = albedo[itime-1]
        albedo[itime] = mod_albedo.albedo_ecearth1(surface_temperature[itime],
                                                   melt_mask[itime],
                                                   snowfall[itime],
                                                   melt_temperature[itime],
                                                   recip_decaytime[itime],
                                                   dtime[itime],
                                                   albedo_old,
                                                   trans_temperature[itime])
    days = (np.cumsum(dtime)-dtime[0])/(86400.)

    #
    # Plot the results
    #
    fig0 = plt.figure(dpi=300, facecolor='white', figsize=(4.8, 4.2),
                      tight_layout=True)
    axes1 = fig0.subplots(2, 1, sharex=True)
    fig0.subplots_adjust(hspace=0.05, wspace=0.1)

    #
    # Upper subplot
    #
    iax = 0
    axes1[iax].plot(days, albedo, '-', linewidth=2, color='darkslategray')
    axes1[iax].set_title('albedo_ecearth (scenario)')
    axes1[iax].set_ylabel('Albedo $\\alpha$')

    axes1[iax].set_xlim(days[0], days[-1])


    # Event axis
    ax_add = axes1[iax].twinx()
    # -- snowfall
    snow_event = (snowfall>0)
    ax_add.plot(days[snow_event], (snowfall[snow_event]>0)+0.04, '*', lw=2, c='navy')
    # -- melting
    melt_event = np.logical_and(melt_mask>0, surface_temperature>FREEZING_TEMP)
    ax_add.plot(days[melt_event], melt_mask[melt_event]+0.00, '.', lw=2, c='crimson')
    # -- refreezing
    refreeze_event = np.logical_and(melt_mask, (surface_temperature<FREEZING_TEMP))
    ax_add.plot(days[refreeze_event], melt_mask[refreeze_event]-0.04, '.', lw=2, c='green')

    ax_add.set_ylim([0.5, 1.2]) # Moves the Marks with y-values of 1 upward #Down: ([0.95, 1.5])
    ax_add.set_yticks([1])
    ax_add.set_yticklabels(['event'], c='darkgray', rotation=90, va='center')
    ax_add.legend(['snowfall', 'melting', 'refreezing'], loc='upper right')

    #
    # 2nd (middle) subplot
    #
    iax = 1
    color = 'darkred' #'orangered'
    axes1[iax].plot(days, surface_temperature, '-', lw=2, c=color) #c='darkcyan')
    axes1[iax].set_ylabel('$T_{{snow}}$ (K)', color=color)
    axes1[iax].tick_params(axis='y', color=color, labelcolor=color)
    axes1[iax].plot(days, surface_temperature*0.+FREEZING_TEMP, ':', lw=1.5, c=color)
    axes1[iax].text(days[-1], FREEZING_TEMP, '$T_{{freeze}}$', c=color,
                    ha='right', va='top')

    ax_add = axes1[iax].twinx()
    color = 'midnightblue' # 'purple'
    ax_add.plot(days, melt_mask, '--', lw=2, c=color)
    ax_add.set_ylabel('mask', c=color, va='bottom')
    ax_add.tick_params(axis='y', color=color, labelcolor=color)
    ax_add.text(days[int(0.5*len(days))], 1, 'melting or refrozen', c=color,
                ha='center', va='top')
    ax_add.text(days[int(0.5*len(days))], 0, 'not-melted',c=color,
                ha='center', va='bottom')
    ax_add.set_yticks([0, 1])
    ax_add.set_ylim([-0.2, 1.2]) # By this command we move the right y-axis
                                 # ticks and labels towards the center, which
                                 # reduces the distance to the subfigure above
                                 # and below.

    ax_add.set_yticklabels(['snow\nfirn', 'melting\nrefrozen'], c=color,
                           rotation=90, va='center', ha='left')

    axes1[-1].set_xlabel('Time (day)')

    if plot_suffix:
        module4plots.plotting(fig0, plotname+'_scenario', plot_suffix)


# -------------------------------------------------------------------------
def plot_albedo_time_melt_freeze(library_name, plot_suffix=None,
                                 plotname='albedo_time_melt_freeze'):
    '''
    Plot results of the via c-binding call Fortran function
    albedo_time_melt_freeze

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'albedo_time_melt_freeze'.

    Returns
    -------
    None.

    '''
    print('* Albedo time-melt-freeze')


    def plot_axvspan_event(event_array, time_array, color):
        event = event_array.copy()
        time = time_array.copy()

        # icc = 0
        while np.any(event):
            # icc = icc + 1
            # print('##  '+str(icc))
            count = np.arange(0, len(event))

            count_event = count[event]
            idx0 = count_event[0]
            idx1 = idx0
            for idx in count_event[1:]:
                if idx-idx1 <= 1:
                    idx1 = idx
                else:
                    # We have detected a discontinous gap, hence we have found
                    # the current upper index of this chain of events
                    break


            axes.axvspan(time[idx0], time[idx1], facecolor=color, alpha=0.50)

            if np.any(event[idx1+1:]):
                event = event[idx1+1:]
                time = time[idx1+1:]
            else:
                # Nothing to do, leave the while construction
                break


    days = np.linspace(0, 150, 301)
    #days = np.linspace(0, 140, 281)
    #days = np.linspace(0, 50, 101)
    delta_time_day = days[1]-days[0]

    seconds_per_day = 86400. # seconds day-1

    # flag_scalar_calls = False: MAY DELIVER WRONG RESULTS!
    flag_scalar_calls = True
    if flag_scalar_calls:
        # Parameters as scalars
        alb_light = ALBEDO_FRESHSNOW
        alb_dark = ALBEDO_FIRN
        alb_refrez = ALBEDO_REFROZEN
        alb_melt = ALBEDO_MELTING
        rec_decaytime = RECIPROCAL_SNOW_DECAYTIME
    else:
        # Parameter as arrays
        alb_light = np.full_like(days, ALBEDO_FRESHSNOW)
        alb_dark = np.full_like(days, ALBEDO_FIRN)
        alb_refrez = np.full_like(days, ALBEDO_REFROZEN)
        alb_melt = np.full_like(days, ALBEDO_MELTING)
        rec_decaytime = np.full_like(days, RECIPROCAL_SNOW_DECAYTIME)

    # Forcing
    temp_melt = np.full_like(days, FREEZING_TEMP)
    temp_surf = temp_melt.copy()
    snow_age = np.zeros_like(days)
    melt_mask = np.zeros_like(days, dtype=int)
    snowfall = np.zeros_like(days)

    #
    # Scenario
    #
    temp_scalar = FREEZING_TEMP+5.25
    snow_scalar = 0.0
    age = 40-delta_time_day

    for iday, day in enumerate(days):
        if day <= 15.0:
            temp_scalar = max(temp_scalar-0.25, FREEZING_TEMP)
        elif day > 15 and day <= 17 :
            temp_scalar = FREEZING_TEMP+2.5
        elif day == 18:
            temp_scalar = FREEZING_TEMP-0.1
        elif day > 20.0 and day <= 40.0:
            temp_scalar = temp_scalar-0.10
        elif day > 95 and day <= 105:
            temp_scalar = temp_scalar+0.195
        elif day > 120 and day <= 125:
            temp_scalar = temp_scalar+0.1
        elif day > 145 and day < 160:
            temp_scalar = temp_scalar-0.47

        snow_scalar = 0.0
        if day >= 8 and day <= 12:
            snow_scalar = 2.0
        elif day >= 30 and day <= 35:
            snow_scalar = 2.0
        elif day >= 52.0 and day <= 55.0:
            snow_scalar = 2.0
        elif day >= 115 and day <= 125:
            snow_scalar = 2.0
        # Add some small noise to the snowfall
        #snow_scalar = max(snow_scalar-np.random.rand()*0.05, 0.0)

        age = age+delta_time_day
        if snow_scalar > 0 :
            age = 0.0

        if age <= 0.0 and temp_scalar <= FREEZING_TEMP:
            melt_mask[iday] = 0
        elif temp_scalar > FREEZING_TEMP:
            melt_mask[iday] = 1
            #age = 0
        elif iday > 0:
            melt_mask[iday] = melt_mask[iday-1]

        temp_surf[iday] = temp_scalar
        snow_age[iday] = age*seconds_per_day
        snowfall[iday] = snow_scalar

    #
    # Call the CISSEMBEL function
    #
    if flag_scalar_calls:
        albedo = np.zeros_like(days)
        for idx in range(len(days)):
            albedo[idx] = mod_albedo.albedo_time_melt_freeze(snow_age[idx],
                                                             alb_light,
                                                             alb_dark,
                                                             alb_refrez,
                                                             alb_melt,
                                                             rec_decaytime,
                                                             temp_surf[idx],
                                                             temp_melt[idx],
                                                             melt_mask[idx])
    else:
        # Array version my deliver errorous results
        albedo = np.zeros_like(days)
        albedo = mod_albedo.albedo_time_melt_freeze(snow_age,
                                                    alb_light,
                                                    alb_dark,
                                                    alb_refrez,
                                                    alb_melt,
                                                    rec_decaytime,
                                                    temp_surf,
                                                    temp_melt,
                                                    melt_mask)

    #
    # Plot the results
    #
    fig0 = plt.figure(dpi=300, facecolor='white', figsize=(4.8, 6.4),
                      tight_layout=True)
    axes1 = fig0.subplots(3, 1, sharex=True)
    fig0.subplots_adjust(hspace=0.05, wspace=0.1)

    #
    # Upper subplot
    #
    iax = 0
    axes1[iax].plot(days, albedo, '-', linewidth=2, color='darkslategray')
    axes1[iax].set_title('albedo_time_melt_freeze (scenario)')
    axes1[iax].set_ylabel('Albedo $\\alpha$')

    axes1[iax].set_xlim(days[0], days[-1])

    if flag_scalar_calls:
        axes1[iax].text(days[ -2], alb_light, '$\\alpha_{{fresh\\: snow}}$',
                        ha='right')
        axes1[iax].text(days[ -2], alb_dark, '$\\alpha_{{firn}}$', ha='right')
        axes1[iax].text(days[-11], alb_refrez, '$\\alpha_{{refreeze}}$', ha='right')
        axes1[iax].text(days[ -1]+delta_time_day, alb_melt, '$\\alpha_{{melt}}$')
    else:
        axes1[iax].text(days[ -2], alb_light[0], '$\\alpha_{{fresh\\: snow}}$',
                        ha='right')
        axes1[iax].text(days[ -2], alb_dark[0], '$\\alpha_{{firn}}$', ha='right')
        axes1[iax].text(days[-11], alb_refrez[0], '$\\alpha_{{refreeze}}$',
                        ha='right')

    # Event axis
    ax_add = axes1[iax].twinx()
    # -- snowfall
    snow_event = (snowfall>0)
    ax_add.plot(days[snow_event], (snowfall[snow_event]>0)+0.04, '*', lw=2, c='navy')
    # -- melting
    melt_event = np.logical_and(melt_mask>0, temp_surf>FREEZING_TEMP)
    ax_add.plot(days[melt_event], melt_mask[melt_event]+0.00, '.', lw=2, c='crimson')
    # -- refreezing
    refreeze_event = np.logical_and(melt_mask, (temp_surf<temp_melt))
    ax_add.plot(days[refreeze_event], melt_mask[refreeze_event]-0.04, '.', lw=2, c='green')

    ax_add.set_ylim([0.5, 1.2]) # Moves the Marks with y-values of 1 upward #Down: ([0.95, 1.5])
    ax_add.set_yticks([1])
    ax_add.set_yticklabels(['event'], c='darkgray', rotation=90, va='center')
    ax_add.legend(['snowfall', 'melting', 'refreezing'], loc='lower center')

    #
    # 2nd (middle) subplot
    #
    iax = 1
    color = 'darkred' #'orangered'
    axes1[iax].plot(days, temp_surf, '-', lw=2, c=color) #c='darkcyan')
    axes1[iax].set_ylabel('$T_{{snow}}$ (K)', color=color)
    axes1[iax].tick_params(axis='y', color=color, labelcolor=color)
    axes1[iax].plot(days, temp_surf*0.+FREEZING_TEMP, ':', lw=1.5, c=color)
    axes1[iax].text(days[-1], FREEZING_TEMP, '$T_{{freeze}}$', c=color,
                    ha='right', va='top')

    ax_add = axes1[iax].twinx()
    color = 'midnightblue' # 'purple'
    ax_add.plot(days, melt_mask, '--', lw=2, c=color)
    ax_add.set_ylabel('mask', c=color, va='bottom')
    ax_add.tick_params(axis='y', color=color, labelcolor=color)
    ax_add.text(days[int(0.5*len(days))], 1, 'melting or refrozen', c=color,
                ha='center', va='top')
    ax_add.text(days[int(0.5*len(days))], 0, 'not-melted',c=color,
                ha='center', va='bottom')
    ax_add.set_yticks([0, 1])
    ax_add.set_ylim([-0.2, 1.2]) # By this command we move the right y-axis
                                 # ticks and labels towards the center, which
                                 # reduces the distance to the subfigure above
                                 # and below.

    ax_add.set_yticklabels(['snow\nfirn', 'melting\nrefrozen'], c=color,
                           rotation=90, va='center', ha='left')

    #
    # 3th upper subplot = bottom subplot
    #
    if len(axes1)>=3:
        iax = 2
        color = 'darkgreen'
        color = 'forestgreen'
        axes1[iax].plot(days, snow_age/86400., '--', lw=2, c=color)
        axes1[iax].set_ylabel('$t_{{snow}}$ (days)', color=color)
        axes1[iax].tick_params(axis='y', color=color, labelcolor=color)

        color = 'navy'
        ax_add = axes1[iax].twinx()
        ax_add.plot(days, snowfall , ':', lw=1.5, c=color)
        ax_add.set_ylabel('snowfall (off-on)', c=color)
        ax_add.tick_params(axis='y', color=color, labelcolor=color)
        ax_add.set_yticks([0, 1, 2])

    axes1[-1].set_xlabel('Time (day)')

    #
    # axhspan for all subplot indicating certain events
    #
    for axes in axes1:
        highlight_snowfall = True
        highlight_melting = True
        highlight_refreezing = True
        # highlight_decay = False

        if highlight_snowfall:
            # Snowfall
            plot_axvspan_event(snow_event.copy(), days.copy(), 'cyan')

        if highlight_melting:
            # Melting
            plot_axvspan_event(melt_event.copy(), days.copy(), 'crimson')

        if highlight_refreezing:
            # Refreezing: #'steelblue'
            plot_axvspan_event(refreeze_event, days.copy(), 'palegreen')

        # if highlight_decay:
        #     # Decay
        #     axes.axvspan(days2[idx2_snowf0_thin],
        #                  days2[idx2_melt1_thin],
        #                  facecolor='yellow', alpha=0.15)

    if plot_suffix:
        module4plots.plotting(fig0, plotname+'_scenario', plot_suffix)


# -------------------------------------------------------------------------
def plot_albedo_density(library_name, plot_suffix=None,
                        plotname='albedo_density'):

    '''
    Plot results of the via c-binding
    call Fortran function albedo_density

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is
    'albedo_density'.

    Returns
    -------
    None.

    '''
    print('* Albedo density')
    snow_density = np.linspace(200, 950, 25)
    albedo_upper_threshold = np.ones_like(snow_density)
    q1 = np.zeros_like(snow_density)-4.0e-4
    q2 = np.zeros_like(snow_density)+0.97

    albedo_density = mod_albedo.albedo_density(snow_density,
                                               albedo_upper_threshold,
                                               q1,
                                               q2,
                                               library_name)
    #
    # Figure
    #
    fig0 = plt.figure(dpi=300, facecolor='white')
    ax0 = fig0.subplots()

    ax0.plot(snow_density, albedo_density, lw=2, linestyle='-',
             label="density dependent")

    ax0.text(snow_density[4], albedo_density[0],
             "Upper albedo threshold {:2.2f} \n".format(albedo_upper_threshold[-2])+
             r"q1 = {:4.4g} m$^3$ kg".format(q1[0])+"\n"+
             "q2 = {:2.2f}".format(q2[0]),
             ha='left', va='top')
    ax0.set_xlabel(r"Snow density, $\rho$ (kg m$^{-3}$)")
    ax0.set_ylabel("Albedo, $\\alpha$")
    ax0.set_title(r"Snow-density dependent albedo, $\alpha = q_{{1}}\; \rho + q_{{2}}$") # $\\alpha = q_{{1}} \\rho + q_{{2}} $")

    if plot_suffix:
        module4plots.plotting(fig0, plotname, plot_suffix)


# -------------------------------------------------------------------------
def plot_albedo_cloud_shift(library_name, plot_suffix=None, plotname='albedo_cloud_shift'):
    '''
    Plot results of the via c-binding call Fortran function albedo_cloud_shift

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'albedo_cloud_shift'.

    Returns
    -------
    None.

    '''
    print('* Albedo cloud shift')
    albedo_in = np.zeros(500, dtype=float) + 0.5
    cloudcover = np.linspace(0,1,len(albedo_in))

    albedo_cloud_shift = mod_albedo.albedo_cloud_shift(albedo_in, cloudcover,
                                                       library_name)
    #
    # Figure
    #
    fig0 = plt.figure(dpi=300, facecolor='white')
    ax0 = fig0.subplots()

    ax0.plot(cloudcover, albedo_cloud_shift, lw=2, linestyle='-', label='Cloud-influenced')
    ax0.plot(cloudcover, albedo_in, lw=2, linestyle='--', label='Original')

    ax0.text(cloudcover[-10], albedo_in[-1],'Pivot point 0.5\n'+
             'Max change 0.05\n', ha='right', va='bottom')
    ax0.set_xlabel('Cloud cover fraction')
    ax0.set_ylabel('Albedo')
    ax0.legend(title='Cloud-cover influence')

    if plot_suffix:
        module4plots.plotting(fig0, plotname, plot_suffix)



# -------------------------------------------------------------------------

# -------------------------------------------------------------------------
#
# Main program
#
if __name__ == '__main__':
    import values_mod_param as physical
    import values_mod_albedo as albedos
    import cbind_mod_albedo as mod_albedo
    import numpy as np
    import matplotlib.pyplot as plt
    import module4plots

    print('Load physical constant')
    physical.constant()
    # Values
    RHO_SNOW = physical.constant.rho_snow
    RHO_ICE = physical.constant.rho_ice
    FREEZING_TEMP = physical.constant.Tmelt_fw

    print('Load albedos constant')
    albedos.constant()
    # Values
    ALBEDO_FRESHSNOW = albedos.constant.albedo_freshsnow
    ALBEDO_MELTING = albedos.constant.albedo_melting
    ALBEDO_FIRN = albedos.constant.albedo_firn
    ALBEDO_REFROZEN = albedos.constant.albedo_refrozen
    ALBEDO_ICE = albedos.constant.albedo_ice
    ALBEDO_SNOW = albedos.constant.albedo_snow
    ALBEDO_SNOW_MELTING = albedos.constant.albedo_snow_melting
    ALBEDO_SNOW_REFROZEN = albedos.constant.albedo_snow_refrozen
    ALBEDO_ICE_MELTING = albedos.constant.albedo_ice_melting
    ALBEDO_ICE_REFROZEN = albedos.constant.albedo_ice_refrozen
    RECIPROCAL_SNOW_DECAYTIME = albedos.constant.reciprocal_snow_decaytime
    RECIPROCAL_SNOW_DECAYLENGTH = albedos.constant.reciprocal_snow_depth_decaylength
    SNOW_DEPTH_THRESHOLD = albedos.constant.default_snow_depth_threshold
    SNOWFALL_THRESHOLD = albedos.constant.default_snowfall_threshold
    TEMPERATURE_TRANSITIONAL = albedos.constant.default_temperature_transitional
    TRANS_TEMPERATURE = albedos.constant.trans_temperature

    #
    # Load the shared library
    #
    LIBRARY_NAME = '../src/CISSEMBEL_CBindings.so' # INCLUDING path, e.g., ./
    print('Shared library containing C-bindings of Fortran code "'
          +LIBRARY_NAME+'"')

    PLOT_SUFFIXES = module4plots.parse_arguments() # = None = ['png', 'pdf']

    #
    # Plots for each function
    #
    plot_albedo_temp(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_albedo_temp_snowdepth(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_albedo_time(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_albedo_snowd_exp(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_albedo_time_snowdepth(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_albedo_inttime_warm(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_albedo_rodehacke1(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_albedo_rodehacke2(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_albedo_rodehacke3(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_albedo_ecearth1(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_albedo_time_melt_freeze(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_albedo_density(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_albedo_cloud_shift(LIBRARY_NAME, PLOT_SUFFIXES)
# -- last line

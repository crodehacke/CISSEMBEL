#!/bin/bash
# Script setting up CISSEMBEL simulations using HIRHAM data nearly directly
#
# Usage example:
# SCENARIO="CE2";YEAR0=1971;YEAR1=2100 ; nohup ./cissembel_hirham_part1_data.bash $SCENARIO $YEAR0 $YEAR1 > hirham2cissemble.preprocessing.$SCENARIO.log 2>&1 &
#
# (c) Christian Rodehacke, 2023, DMI Copenhagen, Denmark
#
# -------------------------------------------------------------------
# This file is part of CISSEMBEL, which is the
# == Copenhagen Ice-Snow Surface Energy and Mass Balance modEL ==
#
# CISSEMBEL is free software; you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE  v.1.2
#
# Information about the EUROPEAN UNION PUBLIC LICENCE (*EUPL*) and
# translations into other European languages are available at
# https://eupl.eu (last access: 2021-10-01)](https://eupl.eu/)
#
# We hope that  this  distributed  CISSEMBEL  code, its  tools and
# documentations are usefull. Any improvements of the code, tools,
# and documentations tools are  very welcome.  Please, consider to
# share your thoughts and improvements with the community.
#
# The documentation,  tools,  and the code are provided as it are.
# NO WARRANTY  ABOUT FITNESS  AND USABILITY OF THE CODE, TOOLS, OR
# DOCUMENTATION  ARE  GIVEN.   NO  WARRANTY  IS  GIVEN  ABOUT  THE
# CORRECTNESS  OF COMPUTED RESULTS WITH THE  PROVIDED CODE, TOOLS,
# AND DOCUMENTATION. YOU USE THE CODE, DOCUMENTATION, AND TOOLS AT
# YOUR OWN RISK.           Removing  of  any  copyright  statement
# or  this  disclaimer  is considered  as a  attempt  to defraud.
#
# -------------------------------------------------------------------
set -e

#
# Activate/Uncomment HDF5_USE_FILE_LOCKING, if you have file system issues
#
export HDF5_USE_FILE_LOCKING=FALSE


#
# Main parameters, also controlled via command line arguments
#
SCENARIO="${1:-CE2}"
YEAR_SCENRIO_START=${2:-1971}
YEAR_SCENRIO_END=${3:-2100}

#
# Source data root directory (FORCING_ORG_ROOT_DIR) and
# Subdirectories of the source forcing (FORCING_ORG_SUBDIRS)
#
FORCING_ORG_ROOT_DIR="/net/isilon/ifs/arch/home/fbo/HIRHAM"
FORCING_ORG_SUBDIRS="${SCENARIO}/PP"

#
# near-surface air temperature : tas
# solar radiation (down)       : rsds
# thermal radioation (down)    : rlds
# [near-surface wind speed      : sfcWind ; alternative =(uas^2+vas^2)^0.5]
# sea level pressure (surface) : psl
# latent heat flux (up)        : hfls
# senible heat flux (up)       : hfss
# total precipitation          : prc + prls = (convective + large scale) precpipitation
# snowfall                     : prsn (Daily values not 3hourly!)
# evaporation                  : evspsbl
# cloud cover                  : clt  (value range 0-1, daily values not 3hourly!)
#
# TIP: List the variables first that are modified or corrected before
#      merging. In the loop wait for the first $icount_needed_var
#      variables in the list $VARIABLES before parallel excecution of
#      modifications and corrections are started.
icount_needed_var=5 # if snow is included: icount_needed_var=6
VARIABLES=( \
  'prls' \
  'prc' \
  'psl' \
  'clt' \
  'evspsbl' \
#  'prsn' \ # Snow
##  'sfcWind' \
  'rlds' \
  'rsds' \
  'hfss' \
  'hfls' \
  'tas' \
)

#
# Variables to combine
#
VARNAME_ConvectivePrecipitation="prc"
VARNAME_LargeScaelPrecipitation="prls"
VARNAME_TotalPrecipitation="precip"

VARNAME_Snowfall='prsn'
VARNAME_Rainfall='rain'

#
# Variable to correct
#
VARNAME_SeaLevelPressure="psl"
VARNAME_SurfacePressure="ps"

VARNAME_CloudCover='clt'
VARNAME_EvaporationOld='evspsbl'
VARNAME_EvaporationNew='evspo'

#
# Variable as reference for number of records
#
VARNAME_AirTemperature='tas'
VARNAME_ReferenceLength=${VARNAME_AirTemperature:?Missing variable}

#
# Invariant surface elevation file
#
SURFACE_ELEVATION_FILE="$HOME/work/Data/data4Models/HIRHAM/topo_geog.nc"
VARNAME_elevation_src="var6"     # variable name in SURFACE_ELEVATION_FILE
VARNAME_elevation="orog"         # variable name in the output

GLACIER_MASK_FILE="${HOME}/work/Data/data4Models/HIRHAM/glmask_geog.nc"
VARNAME_GlacierMask_src="var232" # variable name in GLACIER_MASK_FILE
VARNAME_GlacierMask="glacmask"   # variable name in the output

LAND_MASK_FILE="${HOME}/work/Data/data4Models/HIRHAM/landsea_geog.nc"
VARNAME_LandMask_src="var81"     # variable name in LAND_MASK_FILE
VARNAME_LandMask="landmask"      # variable name in the output

#
# Where shall we storage/save the processed data?
#
WORKING_ROOT_DIR=${HOME}/work/HIRHAM_CISSEMBEL

CISSEMBEL_SUBDIR=cissembel
DATA_SUBDIR=data

# ========================================================================
#
# Commands: Use full path to use a specific version
#
CDO=cdo
NCATTED=ncatted
NCKS=ncks
NCWA=ncwa


# ========================================================================
#
# Subroutines, functions
#

#
# Prepare output directories
#
prepare_working_directory(){
    wrdir=${1:?Missing working root directory: prepare_working_directory}
    sce=${2:?Missing scenario string: prepare_working_directory}
    year0=${3:?Missing starting year: prepare_working_directory}
    year1=${4:?Missing ending year: prepare_working_directory}
    if [ $# -lt 4 ] ; then
	echo " Missing arguments: prepare_working_directory"
	exit 1
    fi

    workdir="${wrdir}/${sce}"
    datadir="${workdir}/${DATA_SUBDIR}"
    rundir="${workdir}/${CISSEMBEL_SUBDIR}"

    echo "Create data directory"
    directory=$datadir
    if [ -d $directory ] ; then
	echo " - keep existing $directory"
    else
	echo " - create new dir $directory"
	mkdir -p $directory
    fi

    echo "Create CISSEMBEL working directory ${CISSEMBEL_SUBDIR}"
    if [ -d $rundir ] ; then
	echo " - keeping existing $rundir"
    else
	echo " - create new dir $rundir"
	mkdir -p ${rundir}
    fi
    echo

    # Export created directory/path variables
    export WORKDIR=$workdir
    export DATADIR=${WORKDIR}/${DATA_SUBDIR}
    export RUNDIR=${WORKDIR}/${CISSEMBEL_SUBDIR}
}

#
# Clean invariant input files
#
invariant_field4add(){
    filein=${1:?Missing input file name:invariant_field4add}
    fileout=${2:?Missing output file name:invariant_field4add}
    varin=${3:?Missing input variable name:invariant_field4add}
    varout=${4:?Missing output variable name:invariant_field4add}

    if [ -f $filein ] ; then
	echo -n "   + invariant field (${varin}->${varout}) from ${filein}"
	if [ ! -f  $fileout ] ; then
	    echo " [New:clean]"
	    $NCWA -a height,alt,time \
		  $filein \
		  ${fileout}.tmp

	    $CDO -s -f nc \
		 -maxc,0 \
		 -setmisstoc,0 \
		 -setvar,${varout} -selvar,${varin} \
		 ${fileout}.tmp \
		 $fileout && rm ${fileout}.tmp
	else
	    echo " [reuse]"
	fi
    else
	echo "   - Skip missing invariant file $filein"
    fi
}

#
# Reports and checks of required input
#
checks(){
    echo "Report and check input"

    #
    # Reports
    #
    echo "Report"
    echo "  hostname   ${HOST:-$(hostname)}"
    echo "  date-time  $(date)"
    echo "  pwd        $(pwd)"
    echo "  User       ${USER:-Unknown}"
    echo "  SCENARIO           ${SCENARIO}"
    echo "  YEAR_SCENRIO_START ${YEAR_SCENRIO_START}"
    echo "  YEAR_SCENRIO_END   ${YEAR_SCENRIO_END}"
    echo
    echo "  working directory $WORKDIR"
    echo "  data directory    $DATADIR"
    echo "  run directory     $RUNDIR"
    echo
    echo "Checks"
    #
    # Check input forcing directory
    #
    ierror=0

    for directory in $FORCING_ORG_ROOT_DIR ${FORCING_ORG_ROOT_DIR}/$FORCING_ORG_SUBDIRS
    do
	if [ -d $directory ] ; then
	    echo "  OK   forcing directory $directory"
	else
	    echo "  MISS forcing directory $directory"
	    ierror=$(( ierror+1 ))
	fi
    done

    #
    # Input files
    # Remark: If you do not want to stop for a missing file,
    #         deactivate the command ierror=$(( ierror+1 )).
    if [ -f $SURFACE_ELEVATION_FILE ] ; then
	echo "  OK   topography file $SURFACE_ELEVATION_FILE"
    else
	echo "  MISS topography file $SURFACE_ELEVATION_FILE"
	ierror=$(( ierror+1 ))
    fi

    if [ -f $GLACIER_MASK_FILE ] ; then
	echo "  OK   glac. mask file $GLACIER_MASK_FILE"
    else
	echo "  MISS glac. mask file $GLACIER_MASK_FILE"
	ierror=$(( ierror+1 ))
    fi

    if [ -f $LAND_MASK_FILE ] ; then
	echo "  OK   land mask  file $LAND_MASK_FILE"
    else
	echo "  MISS land mask  file $LAND_MASK_FILE"
	ierror=$(( ierror+1 ))
    fi

    #
    # Report number of errours missing files
    #
    if [ $ierror -gt 0 ] ; then
	echo "MISSING input (see above)"
	echo " S T O P"
	exit 1
    fi

    echo
    }

# ========================================================================
# ------------------------------------------------------------------------
#
# Main
#
prepare_working_directory $WORKING_ROOT_DIR $SCENARIO $YEAR_SCENRIO_START $YEAR_SCENRIO_END

#
# Checks
#
checks

#
# Essential settings
#
SOURCE_DIR=${FORCING_ORG_ROOT_DIR}/${FORCING_ORG_SUBDIRS}
if [ ! -d $SOURCE_DIR ] ; then
    echo "**** Missing SOURCE_DIR=$SOURCE_DIR"
    echo "S T O P"
    exit 1
fi
SOURCE_DATA_FILE_HEAD="DMI-HIRHAM5_${SCENARIO}"
SOURCE_DATA_FILE_TAIL=".nc"
TARGET_DATA_FILE_TAIL=".nc4"

# ------------------------------------------------------------------------
#
# Fetch and unpack the data, prepare output
#
# T H E   L O O P
#
echo "Fetch source data, create unpacked files, and combine data fields"
for year in $(seq $YEAR_SCENRIO_START $YEAR_SCENRIO_END) ; do
    echo " * YEAR $year ($(date '+%H:%M:%S'))"

    # ------------------------------------------------------------
    # Merged data
    zip_rate_merged_file=1
    MERGED_TARGET_FILE="${DATADIR}/${SOURCE_DATA_FILE_HEAD}_${year}_MERGED${TARGET_DATA_FILE_TAIL}"

    DATADIR_YEAR=${DATADIR}/${year} # Date subdirectory for each year and property
    [ ! -d $DATADIR_YEAR ] && mkdir -p $DATADIR_YEAR

    # ------------------------------------------------------------
    # List of background processes. All background processes must be added
    # to one of these lists.
    list1_pid="" # List of processes that must be completed before computing
                 # deduced, corrected, and combined variables.
    list2_pid="" # List of all remaining processes that must completed before
                 # merging individual files into one combined one.

    # ------------------------------------------------------------
    #
    # Create temporal invariant fields
    # (place it here to allow for changing fields if needed)
    #
    echo "   Create cleaned temporal invariant fields"
    CLEAN_SURFACE_ELEVATION_FILE=${DATADIR}/$(basename $SURFACE_ELEVATION_FILE)
    CLEAN_GLACIER_MASK_FILE=${DATADIR}/$(basename $GLACIER_MASK_FILE)
    CLEAN_LAND_MASK_FILE=${DATADIR}/$(basename $LAND_MASK_FILE)

    # Add flag 'clean'
    CLEAN_SURFACE_ELEVATION_FILE=${CLEAN_SURFACE_ELEVATION_FILE%%.nc}.clean.nc
    CLEAN_GLACIER_MASK_FILE=${CLEAN_GLACIER_MASK_FILE%%.nc}.clean.nc
    CLEAN_LAND_MASK_FILE=${CLEAN_LAND_MASK_FILE%%.nc}.clean.nc

    invariant_field4add $SURFACE_ELEVATION_FILE \
			$CLEAN_SURFACE_ELEVATION_FILE \
			$VARNAME_elevation_src \
			$VARNAME_elevation
    $NCATTED -ha units,$VARNAME_elevation,c,c,'m' \
	     $CLEAN_SURFACE_ELEVATION_FILE

    invariant_field4add $GLACIER_MASK_FILE \
			$CLEAN_GLACIER_MASK_FILE \
			$VARNAME_GlacierMask_src \
			$VARNAME_GlacierMask
    $NCATTED -ha units,$VARNAME_GlacierMask,c,c,'1' \
	     $CLEAN_GLACIER_MASK_FILE

    invariant_field4add $LAND_MASK_FILE \
			$CLEAN_LAND_MASK_FILE \
			$VARNAME_LandMask_src \
			$VARNAME_LandMask
    $NCATTED -ha units,$VARNAME_LandMask,c,c,'1' \
	     $CLEAN_LAND_MASK_FILE

    # ------------------------------------------------------------
    #
    # Loop of all files/variables
    #
    echo -n "   - vars:"
    icount_var=0 # internal variable counter

    for var in ${VARIABLES[*]} ; do
	icount_var=$(( icount_var+1 ))
	PACKED_SOURCE_FILE="${SOURCE_DIR}/${SOURCE_DATA_FILE_HEAD}_${year}_${var}${SOURCE_DATA_FILE_TAIL}.gz"
	TARGET_TMP0="${DATADIR_YEAR}/$(basename ${PACKED_SOURCE_FILE%%.gz})"
	TARGET_FILE="${TARGET_TMP0%%${SOURCE_DATA_FILE_TAIL}}${TARGET_DATA_FILE_TAIL}"

	points=0
	[[ -f $PACKED_SOURCE_FILE ]] && points=$(( points+1 ))
	[[ -f $TARGET_FILE ]] && points=$(( points+1 ))

	case $points in
	    0) echo -n "[MISS:$var]"
	       echo -n
	       ;;
	    1) if [ $PACKED_SOURCE_FILE -nt $TARGET_FILE ] ; then
		   echo -n "[New:$var"
		   [ ! -f $TARGET_TMP0 ] && gunzip -kc $PACKED_SOURCE_FILE > $TARGET_TMP0

		   # === TODO: Move this part below into a procedure
		   #           and use it also in "invariant_field4add".
		   dim2clean=''
		   # Check for those "$dim" that should be elimated
		   # Please, place them in the for-loop
		   for dim in height alt ; do
		       # Check the dimension header until "variable" |
		       #   select lines with '=' | take the strings before '='
		       # Use "$NCKS -M" or "ncdump -h"
		       for line in x$($NCKS -M $TARGET_TMP0 | sed '/variable/q' | grep '=' | cut -d= -f1) ; do
			   if [[ "x${line}" =~ "x${dim}" ]] ; then
			       if [ "x${dim2clean}" == "x" ] ; then
				   dim2clean="${dim}"
			       else
				   dim2clean="${dim2clean},${dim}"
			       fi
			   fi
		       done
		   done
		   if [ "x${dim2clean}" == "x" ] ; then
		       # Create gzipped NetCDF-4 file
		       echo -n "+ncks(nc4z)"
		       ( $NCKS -O -7 -L 3  $TARGET_TMP0 $TARGET_FILE && rm -f $TARGET_TMP0 ) &
		   else
		       # Filter unneeded dimensions and create gzipped NetCDF-4 file
		       echo -n "+ncwa(nc4z)"
		       ( $NCWA -O -a ${dim2clean} -7 -L 3 $TARGET_TMP0 $TARGET_FILE && rm -f $TARGET_TMP0 ) &
		   fi
		   processID=$!
		   if [ $icount_var -le $icount_needed_var ] ; then
		       list1_pid="${list1_pid} ${processID}"
		   else
		       list2_pid="${list2_pid} ${processID}"
		   fi

		   echo -n "]"
	       else
		   echo -n "[KeepE:$var]"
	       fi
	       ;;
	    2) echo -n "[Keep:$var]"
	       ;;
	    *) echo -n "[?:${var}:?]"
	esac
    done  # for var in ...

    echo -n "[wait"
    #wait            # Waits for all background processes
    #wait $processID # Waits only for the last background process
    for processID in ${list1_pid[*]} ; do
	# Waits for listed background processes
	echo -n ":pid=${processID}"
	wait $processID
    done
    echo -n ":continue]"
    echo ":"

    # ------------------------------------------------------------
    #
    # Combine variables
    #

    #
    # NOTE: Original mass flux unit "kg m-3 s-1", which is wrong and it
    #       should be "kg m-3 3hour-1", where 3 hours = 10800 seconds.
    #       To correct it, we allow for this special cdo command:
    #
    ##SPECIAL_CDO_MASS_FLUX_COMMAND="-divc,$(echo '3600.0*3.0' | bc -l)"
    #SPECIAL_CDO_MASS_FLUX_COMMAND="-divc,10800.0"
    ##SPECIAL_CDO_MASS_FLUX_COMMAND="-mulc,$(echo '1.0/(3600.0*3.0)' | bc -l)"
    SPECIAL_CDO_MASS_FLUX_COMMAND="-mulc,0.00009259259" #=0.00009259259259259259


    echo -n "   - combine vars:"
    #
    # convective and large scale precipitation to total precipitation
    #
    var=${VARNAME_TotalPrecipitation:-TotalPrecip}
    FILEout="${DATADIR_YEAR}/${SOURCE_DATA_FILE_HEAD}_${year}_${var}${TARGET_DATA_FILE_TAIL}"
    FILE1in="${DATADIR_YEAR}/${SOURCE_DATA_FILE_HEAD}_${year}_${VARNAME_ConvectivePrecipitation}${TARGET_DATA_FILE_TAIL}"
    FILE2in="${DATADIR_YEAR}/${SOURCE_DATA_FILE_HEAD}_${year}_${VARNAME_LargeScaelPrecipitation}${TARGET_DATA_FILE_TAIL}"

    points=0
    if [ -f $FILEout ] ; then
	echo -n "[Keep:${var}]"
    else
	[ -f $FILE1in ] && points=$(( points+1 ))
	[ -f $FILE2in ] && points=$(( points+1 ))

	if [ $points -eq 2 ] ; then
	    echo -n "[New:$var]"
	    ( $CDO -s -f nc4c -z zip6 \
		   -setattribute,${var}@long_name="total precpitation",${var}@long_name="precpitation_flux",${var}@code=59 \
		   ${SPECIAL_CDO_MASS_FLUX_COMMAND} \
		   -setvar,${var} \
		   -add $FILE1in $FILE2in $FILEout ) &  ##IF-NOT-SNOW## && rm $FILE1in $FILE2in ) &
	    processID=$!
	    list2_pid="${list2_pid} ${processID}"
	else
	    echo -n "[MISS:$var]"
	fi
    fi

    ##
    ## Obtain rainfall by subtracting snowfall from total precipitation
    ## NOTE: Require temporal interpolation of snowfall because
    ##       snowfall is available as daily and not sub-daily fields.
    ## TODO - temporal interpolation ....
    ##
    #var=${VARNAME_Rainfall:-Rainfall}
    #FILEout="${DATADIR_YEAR}/${SOURCE_DATA_FILE_HEAD}_${year}_${var}${TARGET_DATA_FILE_TAIL}"
    #FILE1in="${DATADIR_YEAR}/${SOURCE_DATA_FILE_HEAD}_${year}_${VARNAME_ConvectivePrecipitation}${TARGET_DATA_FILE_TAIL}"
    #FILE2in="${DATADIR_YEAR}/${SOURCE_DATA_FILE_HEAD}_${year}_${VARNAME_LargeScaelPrecipitation}${TARGET_DATA_FILE_TAIL}"
    #FILE3in="${DATADIR_YEAR}/${SOURCE_DATA_FILE_HEAD}_${year}_${VARNAME_Snowfall}${TARGET_DATA_FILE_TAIL}"
    #
    #points=0
    #if [ -f $FILEout ] ; then
    #	echo -n "[Keep:${var}]"
    #else
    #	[ -f $FILE1in ] && points=$(( points+1 ))
    #	[ -f $FILE2in ] && points=$(( points+1 ))
    #	[ -f $FILE3in ] && points=$(( points+1 ))
    #
    #	if [ $points -eq 3 ] ; then
    #	    nstep_precip=$($CDO -s ntime $FILE1in)
    #	    nstep_snowf=$($CDO -s ntime $FILE3in)
    #	    # Ratio of time steps
    #	    INTNTIME_SnowFall=$(( nstep_precip/nstep_snowf ))
    #
    #	    echo -n "[New:$var]"
    #	    ( $CDO -s -f nc4c -z zip6 \
    #		 -setattribute,${var}@long_name="rainfall" \
    #		 ${SPECIAL_CDO_MASS_FLUX_COMMAND} \
    #		 -setvar,${var} \
    #		 -sub -add $FILE1in $FILE2in \
    #		      $FILE3in \
    #		 $FILEout && rm $FILE1in $FILE2in ) &
    #		 processID=$!
    #		 list2_pid="${list2_pid} ${processID}"
    #	else
    #	    echo -n "[MISS:$var]"
    #	fi
    #fi

    echo ":"

    # ------------------------------------------------------------
    #
    # Correct data fields
    #
    echo -n "   - correct vars:"

    #
    # Sea level pressure to surface pressure
    #
    #press       ! air pressure (Pascal)
    #zs_force= 0 ! reference/original height of forcing (Meter); sea level pressure
    #zs_target   ! target/new height (Meter)
    #
    # hcpressure = press*((1.0_wp-2.255e-5_wp*zs_target) / &
    #            & (1.0_wp-2.255e-5_wp*zs_force))**5.256_wp

    var="${VARNAME_SurfacePressure:-SurfacePressue}"
    FILEout="${DATADIR_YEAR}/${SOURCE_DATA_FILE_HEAD}_${year}_${var}${TARGET_DATA_FILE_TAIL}"
    FILE1in="${DATADIR_YEAR}/${SOURCE_DATA_FILE_HEAD}_${year}_${VARNAME_SeaLevelPressure}${TARGET_DATA_FILE_TAIL}"

    if [ $FILE1in -nt $FILEout ] ; then
       echo -n "[New:$var]"
       ( $CDO -s -f nc4c -z zip6 \
	      -setattribute,${var}@long_name="surface pressure" \
	      -setvar,${var} \
	      -mul -selvar,${VARNAME_SeaLevelPressure} $FILE1in \
              -expr,"${var}=((1.0-2.255e-5*${VARNAME_elevation})^5.256)" -selvar,${VARNAME_elevation} $CLEAN_SURFACE_ELEVATION_FILE \
	      $FILEout && rm $FILE1in ) &
       processID=$!
       list2_pid="${list2_pid} ${processID}"
    else
	echo -n "[Keep:$var]"
    fi

    #
    # Correct mass flux of evaporation
    #
    var="${VARNAME_EvaporationOld:-evspsbl}"
    FILE1in="${DATADIR_YEAR}/${SOURCE_DATA_FILE_HEAD}_${year}_${var}${TARGET_DATA_FILE_TAIL}"
    FILEout="${DATADIR_YEAR}/${SOURCE_DATA_FILE_HEAD}_${year}_${VARNAME_EvaporationNew}${TARGET_DATA_FILE_TAIL}"

    if [ $FILE1in -nt $FILEout ] ; then
	echo -n "[New:$var]"
	( $CDO -s -f nc4c -z zip6 \
	       -setvar,${VARNAME_EvaporationNew} \
	       ${SPECIAL_CDO_MASS_FLUX_COMMAND:-copy} \
	       $FILE1in \
	       $FILEout && rm $FILE1in ) &
	processID=$!
	list2_pid="${list2_pid} ${processID}"
    else
	echo -n "[Keep:$var]"
    fi

    #
    # Interpolate missing time steps in total cloud cover
    #
    FILEcount_ref="${DATADIR_YEAR}/${SOURCE_DATA_FILE_HEAD}_${year}_${VARNAME_ReferenceLength}${TARGET_DATA_FILE_TAIL}"
    var=$VARNAME_CloudCover
    FILEout="${DATADIR_YEAR}/${SOURCE_DATA_FILE_HEAD}_${year}_${var}${TARGET_DATA_FILE_TAIL}"
    FILE1in="${DATADIR_YEAR}/${SOURCE_DATA_FILE_HEAD}_${year}_${var}_daily${TARGET_DATA_FILE_TAIL}"
    FILE2in="${DATADIR_YEAR}/${SOURCE_DATA_FILE_HEAD}_${year}_${var}_time0${TARGET_DATA_FILE_TAIL}"

    # Number of time steps
    nstep_clt=$($CDO -s ntime $FILEout)
    nstep_former=$($CDO -s ntime $FILEcount_ref)
    # Ratio of time steps
    INTNTIME_CloudCover=$(( nstep_former/nstep_clt ))

    if [ ${nstep_clt:-0} -ne $nstep_former ] ; then
	echo -n "[New:$var+intntime${INTNTIME_CloudCover}]"

	mv $FILEout $FILE1in
	# Use the first time step as the time step
	$CDO -s -shifttime,-24hour -seltimestep,1 $FILE1in $FILE2in
	timestring=$($CDO -s showdate $FILE2in | tr -d '[:space:]')
	datestring=$($CDO -s showtime $FILE2in | tr -d '[:space:]')

	( $CDO -s -f nc4c -z zip6 \
	       -setattribute,${var}@units='1',${var}@long_name='Total Cloud Fraction; interpolated in time' \
	       -seltimestep,2/99999 \
	       -intntime,${INTNTIME_CloudCover} \
	       -mergetime $FILE1in $FILE2in $FILEout 2> /dev/null \
	      && rm $FILE1in $FILE2in ) #&
	#processID=$!
	#list2_pid="${list2_pid} ${processID}"
    else
	echo -n "[Keep:$var]"
    fi

    echo -n "[wait"
    for processID in ${list2_pid[*]} ; do
	# Waits for listed background processes
	echo -n ":pid=${processID}"
	wait $processID
    done
    echo -n ":continue]"

    echo ":"

    # ------------------------------------------------------------
    #
    # Prepare for combining data fields; select the right fields
    #
    echo -n "   + forcing files to combined:"
    var2combine=""
    for var in \
	${VARIABLES[*]} \
	${VARNAME_TotalPrecipitation} \
	${VARNAME_SurfacePressure} \
	${VARNAME_EvaporationNew}
	#${VARNAME_Rainfall} # Todo: If we have sub-daily snowfall
    do
	icount=0
	#
	# Here list all variables, which should
	# NOT go into the merged file below
	#
	for var2skip in \
	    $VARNAME_ConvectivePrecipitation \
	    $VARNAME_LargeScaelPrecipitation \
	    $VARNAME_SeaLevelPressure \
	    $VARNAME_EvaporationOld
	do
	    if [ "x${var}" == "x${var2skip}" ] ; then
		echo -n "[-$var]"
		icount=1
		break
	    fi
	done
	if [ $icount -le 0 ] ; then
	    echo -n "[+$var]"
	    var2combine="${var2combine} ${var}"
	fi
    done

    #
    # Create list of forcing files to merge
    #
    files2combine=""
    for var in ${var2combine[*]} ; do
	SOURCE_FILE="${DATADIR_YEAR}/${SOURCE_DATA_FILE_HEAD}_${year}_${var}${TARGET_DATA_FILE_TAIL}"
	if [ -f $SOURCE_FILE ] ; then
	    files2combine="$files2combine ${SOURCE_FILE}"
	else
	    echo -n "[MissFile:$(basename $SOURCE_FILE)]"
	fi
    done
    echo ":"

    # ------------------------------------------------------------
    #
    # Add topography/surface elevation, glacier mask, and land mask
    #
    echo -n "   + Add these temporal invariant files:"
    for file in \
	$CLEAN_SURFACE_ELEVATION_FILE \
	$CLEAN_GLACIER_MASK_FILE \
	$CLEAN_LAND_MASK_FILE
    do
	if [ -f $file ] ; then
	    echo -n "[Add:$(basename ${file})]"
	    files2combine="$files2combine ${file}"
	else
	    echo -n "[Miss:$(basename ${file})]"
	fi
    done
    echo ":"

    # ------------------------------------------------------------
    #
    # Finally combine data files
    #
    if [ -f $MERGED_TARGET_FILE ] ; then
	echo "   o Keep existing $MERGED_TARGET_FILE"
    else
	if [ $zip_rate_merged_file -gt 0 ] ; then
	    echo "   = Create new (zip $zip_rate_merged_file) $MERGED_TARGET_FILE"
	    ( $CDO -s -f nc4c -z zip${zip_rate_merged_file} \
		   merge ${files2combine} $MERGED_TARGET_FILE \
		  && rm ${DATADIR}/${year}/*.nc4 && rmdir ${DATADIR}/${year} ) &
	else
	    echo "   = Create new $MERGED_TARGET_FILE"
	    ( $CDO -s -f nc4c \
		   merge ${files2combine} $MERGED_TARGET_FILE \
		  && rm ${DATADIR}/${year}/*.nc4 && rmdir ${DATADIR}/${year} ) &
	fi
    fi
done  # year

echo -n " * Waiting for last background processes ..($(date '+%H:%M:%S')).."
wait
echo ".. ($(date '+%H:%M:%S')).. Continue"

# ------------------------------------------------------------------------
#
# Report created data
#
echo
echo "Prepared data in directory ${DATADIR}"
for file in ${DATADIR}/* ; do
    if [ -d $file ] ; then
	echo "  +[d] $(basename $file)"
    elif [ -f $file ] ; then
	echo "  +[f] $(basename $file)"
    else
	echo "  +[ ] $(basename $file)"
    fi
done

# ------------------------------------------------------------------------
#
# Clean up
#
unset WORKDIR
unset DATADIR
unset RUNDIR

echo "Done ($(date))"
exit 0
# --- last line

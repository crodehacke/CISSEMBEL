#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 25 12:59:15 2022

--- Takes the by Baptiste provided PROMICE data set and construct an initial
file for the CISSEMBEL model. Please note: Some provided fields, such as the
`surface_slope`, are filled with standard values that do not necessarily
describe adequately the conditions.

Call example (1): Directly on the command line

python ./PROMICE_baptiste2initial.py \
            --input ./ORG/PROMICE_subsurface_temperature_all_depths_hourly.nc \
            --output PROMICE_Baptiste_initial.nc \
            --vname_output_xaxis x \
            --vname_output_yaxis y \
            --vname_output_surface_elevation usrf \
            --vname_output_longitude lon \
            --vname_output_latitude lat \
            --dname_output_xaxis x1 \
            --dname_output_yaxis y1

Call example (2): On the ipython console

runfile('./PROMICE_baptiste2initial.py', \
        args='-i ./ORG/PROMICE_subsurface_temperature_all_depths_hourly.nc \
        -o PROMICE_Baptiste_initial.nc \
        --vname_output_xaxis x --vname_output_yaxis y \
        --vname_output_surface_elevation usrf \
        --vname_output_longitude lon --vname_output_latitude lat \
        --dname_output_xaxis x1 --dname_output_yaxis y1', \
        wdir='/home/cr/src/CISSEMBEL/data/PROMICE.Baptiste')


@author: Christian Rodehacke, DMI Copenhagen, Denmark

@copyright: Copyright (C) 2022-2025 Christian Rodehacke
"""
# -------------------------------------------------------------------
# This file is part of CISSEMBEL, which is the
# == Copenhagen Ice-Snow Surface Energy and Mass Balance modEL ==
#
# CISSEMBEL is free software; you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE  v.1.2
#
# Information about the EUROPEAN UNION PUBLIC LICENCE (*EUPL*) and
# translations into other European languages are available at
# https://eupl.eu (last access: 2021-10-01)](https://eupl.eu/)
#
# We hope that  this  distributed  CISSEMBEL  code, its  tools and
# documentations are usefull. Any improvements of the code, tools,
# and documentations tools are  very welcome.  Please, consider to
# share your thoughts and improvements with the community.
#
# The documentation,  tools,  and the code are provided as it are.
# NO WARRANTY  ABOUT FITNESS  AND USABILITY OF THE CODE, TOOLS, OR
# DOCUMENTATION  ARE  GIVEN.   NO  WARRANTY  IS  GIVEN  ABOUT  THE
# CORRECTNESS  OF COMPUTED RESULTS WITH THE  PROVIDED CODE, TOOLS,
# AND DOCUMENTATION. YOU USE THE CODE, DOCUMENTATION, AND TOOLS AT
# YOUR OWN RISK.           Removing  of  any  copyright  statement
# or  this  disclaimer  is considered  as a  attempt  to defraud.
#
# -------------------------------------------------------------------

# -------------------------------------------------------------------------
def parse_arguments():
    '''
    Parse the command line arguments

    Returns
    -------
    list
        parsed command line arguments.

    '''
    parser = argparse.ArgumentParser(
        description='Creation of (re)start conditions from average climate conditions',
        add_help=False)

    #
    # Input and output file names
    #
    parser.add_argument('--input', '-i',
                        nargs='?',
                        required=True,
                        help='Input file name containing the input conditions')

    parser.add_argument('--output', '-o',
                        nargs='?',
                        required=False,
                        default='initial_file.nc',
                        help='Output file name of the file')


    #
    # variable names in the input file
    #
    parser.add_argument('--vname_input_surface_elevation',
                            nargs='?',
                        required=False,
                        default='elevation', #!TODO: better 'default'
                        help='variable name of the surface elevation in the input file')

    parser.add_argument('--vname_input_longitude',
                        nargs='?',
                        required=False,
                        default='longitude',
                        help='variable name of the longitude in the input file')

    parser.add_argument('--vname_input_latitude',
                        nargs='?',
                        required=False,
                        default='latitude',
                        help='variable name of the latitude in the input file')

    #
    # variable names in the output file
    #
    parser.add_argument('--vname_output_surface_elevation',
                        nargs='?',
                        required=False,
                        default='elevation',
                        help='variable name of the surface elevation in the output file')
    parser.add_argument('--vname_output_surface_slope',
                        nargs='?',
                        required=False,
                        default='surface_slope',
                        help='variable name of the surface slope in the output file')

    parser.add_argument('--vname_output_longitude',
                        nargs='?',
                        required=False,
                        default='longitude',
                        help='variable name of the longitude in the output file')

    parser.add_argument('--vname_output_latitude',
                        nargs='?',
                        required=False,
                        default='latitude',
                        help='variable name of the latitude in the output file')

    parser.add_argument('--vname_output_xaxis',
                        nargs='?',
                        required=False,
                        default='iaxis',
                        help='variable name of the xaxis in the output file')

    parser.add_argument('--vname_output_yaxis',
                        nargs='?',
                        required=False,
                        default='jaxis',
                        help='variable name of the yaxis in the output file')

    parser.add_argument('--dname_output_xaxis',
                        nargs='?',
                        required=False,
                        default='iaxis',
                        help='dimension name of the xaxis in the output file')

    parser.add_argument('--dname_output_yaxis',
                        nargs='?',
                        required=False,
                        default='jaxis',
                        help='dimension name of the yaxis in the output file')

    #
    # Further arguments
    #
    parser.add_argument('--output_format',
                        nargs='?',
                        required=False,
                        default='NETCDF3_64BIT',
                        choices=['NETCDF4', 'NETCDF4_CLASSIC', 'NETCDF3_64BIT', 'NETCDF3_CLASSIC'],
                        help='Format of the netcdf output file')


    # # Report used command line arguments
    print('Used settings: read command line arguments or default values')
    for single_arg in vars(parser.parse_args()): #sorted(vars(args)) :
        print(' - %-32s = '% (single_arg)+str(getattr(parser.parse_args(), single_arg)) )
        # print(type(single_arg))
    print()

    return parser.parse_args()


# -------------------------------------------------------------------------
#
# Main program
#
if __name__ == '__main__':
    import argparse           # argparse : Parse command line arguments
    import numpy as np        # Numpy    : Array handling
    # import pandas as pd       # Pandas   :
    import xarray as xr       # Xarrray  : Easy handling of netcdf files
    from datetime import  datetime # Current date/time
    # from pyproj import Proj   # PyProj   : Transformation between coordinates
    # from pathlib import Path
    import os


    print('Start    the job ('+str(datetime.now())[:19]+')')
    # Parse command arguments
    parsed_argv = parse_arguments()

    dname_iaxis = parsed_argv.vname_output_xaxis
    dname_jaxis = parsed_argv.vname_output_yaxis
    dname_kaxis = 'kaxis'
    dname_haxis = 'haxis'

    #
    # Read input data
    #
    #data_in = xr.open_dataset(parsed_argv.input, decode_times=False)
    data_in = xr.open_dataset(parsed_argv.input, decode_times=True)

    #
    # Axis
    #
    x_axis = np.arange(int(data_in.site.shape[0]))
    if data_in.site.ndim > 1:
        y_axis = np.arange(int(data_in.site.shape[1]))
    else:
        y_axis = np.array([0])

    x_axis = xr.DataArray(x_axis, dims=[dname_iaxis],
                          attrs= {'long_name': 'Cartesian x-coordinate',
                                  'units': 'count',
                                  'standard_name': 'projection_x_coordinate'},)
    y_axis = xr.DataArray(y_axis, dims=[dname_jaxis],
                          attrs= {'long_name': 'Cartesian y-coordinate',
                                  'units': 'count',
                                  'standard_name': 'projection_y_coordinate'},)
    k_axis = xr.DataArray(np.arange(15), dims=[dname_kaxis],
                          attrs= {'standard_name': 'model_level_number',
                                  'long_name' : 'z-layer of mesh grid',
                                  'units' : 'count',
                                  'positive' : 'down',})
    h_axis = xr.DataArray(np.arange(13), dims=[dname_haxis],
                          attrs= {'standard_name': 'height',
                                  'long_name' : 'height class elevation levels',
                                  'units' : 'meter',
                                  'positive' : 'up',})

    #
    # Dummy data
    #
    shape_2dim = np.expand_dims(data_in[parsed_argv.vname_input_longitude].data,
                                        axis=0).shape
    size_2dim = np.expand_dims(data_in[parsed_argv.vname_input_longitude].data,
                                        axis=0).size
    ones2dim = np.zeros(shape_2dim)
    zeros2dim = np.zeros(shape_2dim)
    increment = np.expand_dims(np.arange(size_2dim), axis=0)


    #
    # Site labels
    #
    site_label = xr.DataArray(np.expand_dims(data_in.site.data, axis=0),
                              dims=[parsed_argv.dname_output_yaxis,
                                    parsed_argv.dname_output_xaxis],
                              attrs={'long_name': 'Label of PROMICE site'})
    site_label = xr.DataArray(np.expand_dims(data_in.site.data, axis=0),
                              dims=[y_axis.dims[0], x_axis.dims[0]],
                              attrs={'long_name': 'Label of PROMICE site'})

    #
    # Output data construct
    #
    data_out = xr.Dataset(
        coords = {
            parsed_argv.dname_output_xaxis: (x_axis),
            parsed_argv.dname_output_yaxis: (y_axis),
            dname_kaxis: k_axis,
            dname_haxis: h_axis,
            },
        data_vars = {
            parsed_argv.vname_output_longitude :
                ([parsed_argv.vname_output_yaxis,
                  parsed_argv.vname_output_xaxis],
                 np.expand_dims(data_in[parsed_argv.vname_input_longitude].data, axis=0),
                 {'long_name': 'longitude',
                  'units': 'degreesE',
                  'standard_name': 'longitude'}
                 ),
            parsed_argv.vname_output_latitude :
                ([parsed_argv.vname_output_yaxis,
                  parsed_argv.vname_output_xaxis],
                 np.expand_dims(data_in[parsed_argv.vname_input_latitude].data, axis=0),
                 {'long_name': 'latitude',
                  'units': 'degreesN',
                  'standard_name': 'latitude'}
                 ),
            parsed_argv.vname_output_surface_elevation :
                ([parsed_argv.vname_output_yaxis,
                  parsed_argv.vname_output_xaxis],
                 np.expand_dims(data_in[parsed_argv.vname_input_surface_elevation].data, axis=0),
                 {'long_name': 'surface elevation',
                  'units': 'm',
                  'coordinates': parsed_argv.vname_output_longitude+" "+parsed_argv.vname_output_latitude, }
                 ),
            parsed_argv.vname_output_surface_slope :
                ([parsed_argv.vname_output_yaxis,
                  parsed_argv.vname_output_xaxis],
                 zeros2dim+0.01,
                 {'long_name': 'surface slope',
                  'units': 'm m-1',
                  'coordinates': parsed_argv.vname_output_longitude+" "+parsed_argv.vname_output_latitude, }
                 ),
            'stationID' : ([parsed_argv.vname_output_yaxis,
                            parsed_argv.vname_output_xaxis],
                            increment,
                            {'long_name': 'station id',
                             'coordinates': parsed_argv.vname_output_longitude+" "+parsed_argv.vname_output_latitude, }
                            ),
            'layer_thickness': ([dname_kaxis], 0.5*k_axis.data+0.33,
                                {'standard_name' : 'snow_layer_thickness' ,
                                 'long_name' : 'thickness of each snow layer' ,
                                 'units' : 'meter' ,}),
            'site_label': site_label ,
            'lon_promice_means' :
                ([parsed_argv.vname_output_yaxis,
                  parsed_argv.vname_output_xaxis],
                 np.expand_dims(data_in[parsed_argv.vname_input_longitude].data, axis=0),
                 {'long_name': 'longitude',
                  'units': 'degreesE',
                  'coordinates': parsed_argv.vname_output_longitude+" "+parsed_argv.vname_output_latitude, }
                 ),
            'lat_promice_means' :
                ([parsed_argv.vname_output_yaxis,
                  parsed_argv.vname_output_xaxis],
                 np.expand_dims(data_in[parsed_argv.vname_input_latitude].data, axis=0),
                 {'long_name': 'latitude',
                  'units': 'degreesN',
                  'coordinates': parsed_argv.vname_output_longitude+" "+parsed_argv.vname_output_latitude, }
                 ),
            },
        attrs = {
            "title": "Preliminary initial CISSEMBEL data",
            "data_source": str(parsed_argv.input),
            "creation_date": str(datetime.now()),
            },
        )

    print("Written forcing file '"+parsed_argv.output+"' as "+parsed_argv.output_format)
    data_out.to_netcdf(parsed_argv.output, format=parsed_argv.output_format) #, unlimited_dims='time')


    print(' ncatted -a coordinates,lon,d,, -a coordinates,lat,d,, -a coordinates,site_label,d,, '+parsed_argv.output)


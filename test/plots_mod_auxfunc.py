#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 21 20:00:17 CET 2022


-- Test and plots for mod_auxfunc.F08

@author: Christian Rodehacke, DMI Copenhagen, Denmark

@copyright: Copyright (C) 2016-2025 Christian Rodehacke
"""
# -------------------------------------------------------------------
# This file is part of CISSEMBEL, which is the
# == Copenhagen Ice-Snow Surface Energy and Mass Balance modEL ==
#
# CISSEMBEL is free software; you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE  v.1.2
#
# Information about the EUROPEAN UNION PUBLIC LICENCE (*EUPL*) and
# translations into other European languages are available at
# https://eupl.eu (last access: 2023-09-18)](https://eupl.eu/)
#
# We hope that  this  distributed  CISSEMBEL  code, its  tools and
# documentations are usefull. Any improvements of the code, tools,
# and documentations tools are  very welcome.  Please, consider to
# share your thoughts and improvements with the community.
#
# The documentation,  tools,  and the code are provided as it are.
# NO WARRANTY  ABOUT FITNESS  AND USABILITY OF THE CODE, TOOLS, OR
# DOCUMENTATION  ARE  GIVEN.   NO  WARRANTY  IS  GIVEN  ABOUT  THE
# CORRECTNESS  OF COMPUTED RESULTS WITH THE  PROVIDED CODE, TOOLS,
# AND DOCUMENTATION. YOU USE THE CODE, DOCUMENTATION, AND TOOLS AT
# YOUR OWN RISK.           Removing  of  any  copyright  statement
# or  this  disclaimer  is considered  as a  attempt  to defraud.
#
# -------------------------------------------------------------------

# -------------------------------------------------------------------------
def plot_diffusion_implicit_central(library_name, plot_suffix=None,
                                    plotname='diffusion_implicit_central'):
    '''
    Plot results of the via c-binding call Fortran function subsurface_field4d

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'rescale_field'.

    Returns
    -------
    None.

    '''
    print('* Diffusion implicit central')


    def distance2depth(delta_z):
        depth = np.zeros_like(delta_z)
        iz = 0
        depth[iz] = delta_z[iz]*0.5
        for iz in range(1,len(delta_z)):
            depth[iz] = depth[iz-1]+delta_z[iz]*0.5
        return depth

    def depthaxis2_low_mid_high(depth):
        f_low = 2./5.
        f_mid = 0.5
        f_high = 3./5.
        i_low = int(np.argwhere(abs(depth[-1]*f_low-depth)
                                  == min(abs(depth[-1]*f_low-depth))))
        i_mid = int(np.argwhere(abs(depth[-1]*f_mid-depth)
                                  == min(abs(depth[-1]*f_mid-depth))))
        i_high = int(np.argwhere(abs(depth[-1]*f_high-depth)
                                  == min(abs(depth[-1]*f_high-depth))))
        return i_low, i_mid, i_high

    def depth4val2_low_mid_high(depth, depth_low, depth_mid, depth_high):
        i_low = int(np.argwhere(abs(depth_low-depth)
                                  == min(abs(depth_low-depth))))
        i_mid = int(np.argwhere(abs(depth_mid-depth)
                                  == min(abs(depth_mid-depth))))
        i_high = int(np.argwhere(abs(depth_high-depth)
                                  == min(abs(depth_high-depth))))
        return i_low, i_mid, i_high


    def fig_fine_and_coarse_lines_time(figure,
                                       temp01_ini, temp01_step, depth01,
                                       temp02_ini, temp02_step, depth02,
                                       alpha01, _tsteps, bbottom_temp=None,
                                       mid_title='Diffusivity',
                                       mid_xlabel=r' $\alpha$ ($m^{{2}}\; s^{{-1}}$)',
                                       timesteps_in_plot=7):

        _axes = figure.subplots(1, 3, sharey=True,
                                gridspec_kw={'width_ratios': [4, 1, 4]})

        iax_equi=0
        iax_linin=2
        iax_alpha=1

        #
        # Equidistant solutions
        #
        _axes[iax_equi].plot(temp01_ini, depth01,
                             'x', color='black', alpha=0.4,  # linewidth=3.0, linestyle='--',
                             label=None)
                             # label='initial')
                          # label='Starting\ncondition')
        for it in range(0, _tsteps, max(1, int(_tsteps/timesteps_in_plot))):
        # for it in range(_tsteps-1, 0, -max(1, int(_tsteps0x/timesteps_in_plot))):
            _axes[iax_equi].plot(temp01_step[:, it], depth01,
                                 linewidth=1.5,
                                 label='{:.2f}'.format(time_axis0x[it]/86400.0))
            # label='$t$ ={:-6.1f} hr'.format(time_axis0x[it]/3600.0))
            # label='$t$ ={:.1f} min'.format(time_axis0x[it]/60.0))
            # label='$t$ ={:6.2f} hr'.format(time_axis0x[it]/3600.0))
            # label='Step t={:d}'.format(it))

        if bbottom_temp:
            _axes[iax_equi].plot(bbottom_temp,
                                 max(depth01)+0.02*(max(depth01)-min(depth01)),
                                 '^',
                                 color='r',
                                 label='Below')

        #
        # Linear increasing solutions
        #
        _axes[iax_linin].plot(temp02_ini, depth02,
                      'x', color='black', alpha=0.4, #  linewidth=3.0, linestyle='--',
                      label='Start')
        for it in range(0, _tsteps, max(1, int(_tsteps/timesteps_in_plot))):
            _axes[iax_linin].plot(temp02_step[:, it], depth02,
                                  linewidth=1.5,
                                  label='$t$ ={:-6.1f} hr'.format(time_axis0x[it]/3600.0))

        if bbottom_temp:
            _axes[iax_linin].plot(bbottom_temp,
                                  max(depth02)+0.02*(max(depth02)-min(depth02)),
                                  '^',
                                  color='r',
                                  label='Below')


        #
        # Middel panel, e.g., diffusivity
        #
        _axes[iax_alpha].plot(alpha01, depth01, '--.')

        #
        # Axis adjustments
        #
        for iax, axes in enumerate(_axes):
            if iax == iax_equi:
                axes.invert_yaxis() # Only once, since "fig2.subplots(1, 2, sharey=True)"
                axes.set_xlabel('Temperature ($^{\circ}C$)')
                axes.set_ylabel('Depth (m)')
                axes.set_title('Equidistant grid')
                axes.legend(loc='upper left', title='Time (day)')
                            # title=r'$\alpha$ = {:.2e} $m^2 s^{{-1}}$'.format(alpha_const01[iax_equi]))
            elif iax == iax_linin:
                axes.set_xlabel('Temperature ($^{\circ}C$)')
                axes.set_title('Increasing grid')
            elif iax == iax_alpha:
                axes.set_xlabel(mid_xlabel)
                axes.set_title(mid_title)
            axes.annotate(text='('+str(iax+1)+')',
                          xy=(0.95, 0.05), xytext=(0.97, 0.05),
                          textcoords='axes fraction', ha='right', va='top')

    # We shall check the following cases
    # - Equidistance depth grid/constant distances, constant alpha
    #   - Delta peak/column in the center/mid depth
    #   - Step in the center/mid depth
    # - Equidistance depth grid/constant distances, alpha step at center/mid depth
    #   - Delta peak/column in the center/mid depth
    #   - Step in the center/mid depth

    # Constant
    # ice diffusivity at 0 degC approx 1.02 mm2 s-1
    alpha_low = 1.02*1.0e-6   # 1.02*1.0e-6
    alpha_mid = 2.75*1.0e-6
    alpha_high = 5.5*1.0e-6

    temp_degC2Kelvin = 273.15
    temp_low = -40.0+temp_degC2Kelvin # Kelvin
    temp_mid = -20.0+temp_degC2Kelvin
    temp_high = -0.0+temp_degC2Kelvin

    grid_dz = 0.1 # Meter

    second_per_day = 86400.

    #
    # Grid2 : increasing: depth_max = 10.05
    #
    grid_size02 = 25
    distance02 = np.linspace(grid_dz, 1.508, grid_size02) # Meter
    depth02 = distance2depth(distance02) # Meter

    idx_low02, idx_mid02, idx_high02 = depthaxis2_low_mid_high(depth02)

    #
    # Grid1 : linear: depth_max = 10.05
    #
    grid_size01 = 201
    distance01 = np.full((grid_size01), grid_dz) # Meter
    depth01 = distance2depth(distance01) # Meter

    # grid_size01 = 21
    idx_low01, idx_mid01, idx_high01 = depth4val2_low_mid_high(depth01,
                                                               depth02[idx_low02],
                                                               depth02[idx_mid02],
                                                               depth02[idx_high02])

    #
    # Case 1: Centered temperature peak/hat, constant alpha
    #
    # Grid 01
    temperature_peak01 = np.zeros_like(distance01) + temp_low # Kelvin
    temperature_peak01[idx_low01:idx_high01] = temp_high
    bottom_temp_noflux = temperature_peak01[-1]

    alpha_const01 = np.zeros_like(distance01)+ alpha_mid # m2 s-1
    source_zero01 = np.zeros_like(distance01)  # K s-1

    # Grid 02
    temperature_peak02 = np.zeros_like(distance02) + temp_low # Kelvin
    temperature_peak02[idx_low02:idx_high02] = temp_high
    # bottom_temp_noflux = temperature_peak01[-1]

    alpha_const02 = np.zeros_like(distance02) + alpha_mid # m2 s-1
    source_zero02 = np.zeros_like(distance02)  # K s-1

    #
    # Case 2: Centered temperature peak/hat, step alpha
    #
    # Grid 01
    alpha_step01 = np.zeros_like(distance01) + alpha_low # m2 s-1
    alpha_step01[idx_mid01:] = alpha_high # m2 s-1

    # Grid 02
    alpha_step02 = np.zeros_like(distance02) + alpha_low # m2 s-1
    alpha_step02[idx_mid02:] = alpha_high # m2 s-1

    #
    # Case 3: Temperature Step, step alpha, cold below bottom temperature
    #
    temperature_step01 = np.zeros_like(distance01) + temp_high # Kelvin
    temperature_step01[idx_mid01:] = temp_mid
    bottom_temp_cold = temp_low

    # Grid 02
    temperature_step02 = np.zeros_like(distance02) + temp_high # Kelvin
    temperature_step02[idx_mid02:] = temp_mid
    # bottom_temp_cold = temp_low

    #
    # Case 4: Constant temperature, contant alpha, step source
    #
    # Grid 01
    temperature_const01 = np.zeros_like(distance01) + temp_mid
    alpha_src01 = np.zeros_like(distance01) + alpha_mid # m2 s-1
    source_peak01 = np.zeros_like(distance01)
    source_peak01[idx_mid01:] = 2.424242/second_per_day

    # Grid 02
    temperature_const02 = np.zeros_like(distance02) + temp_mid
    alpha_src02 = np.zeros_like(distance02) + alpha_mid # m2 s-1
    source_peak02 = np.zeros_like(distance02)
    source_peak02[idx_mid02:] = 2.424242/second_per_day



    #
    # Time
    #
    time_steps0x = 99
    delta_time0x = 7200.0 # Seconds
    time_axis0x = np.linspace(0.0, time_steps0x*delta_time0x, time_steps0x)

    #
    # Computation in steps
    #
    # Allocated fields
    # Case 1
    temp_peak01_tserie = np.zeros([temperature_peak01.size, time_axis0x.size])
    temp_peak02_tserie = np.zeros([temperature_peak02.size, time_axis0x.size])
    # Case 2
    temp_as_peak01_tserie = np.zeros([temperature_peak01.size, time_axis0x.size])
    temp_as_peak02_tserie = np.zeros([temperature_peak02.size, time_axis0x.size])
    # Case 3
    temp_step01_tserie = np.zeros([temperature_step01.size, time_axis0x.size])
    temp_step02_tserie = np.zeros([temperature_step02.size, time_axis0x.size])
    # Case 4
    temp_const01_src_tserie = np.zeros([temperature_const01.size, time_axis0x.size])
    temp_const02_src_tserie = np.zeros([temperature_const02.size, time_axis0x.size])



    #
    # First/initial state
    it = 0
    # Case 1
    temp_peak01_tserie[:, it] = \
        mod_auxfunc.diffusion_implicit_central(time_axis0x[0],
                                               temperature_peak01,
                                               distance01,
                                               alpha_const01,
                                               source_zero01,
                                               bottom_temp_noflux)
    temp_peak02_tserie[:, it] = \
        mod_auxfunc.diffusion_implicit_central(time_axis0x[0],
                                               temperature_peak02,
                                               distance02,
                                               alpha_const02,
                                               source_zero02,
                                               bottom_temp_noflux)
    # Case 2
    temp_as_peak01_tserie[:, it] = \
        mod_auxfunc.diffusion_implicit_central(time_axis0x[0],
                                               temperature_peak01,
                                               distance01,
                                               alpha_step01,
                                               source_zero01,
                                               bottom_temp_noflux)
    temp_as_peak02_tserie[:, it] = \
        mod_auxfunc.diffusion_implicit_central(time_axis0x[0],
                                               temperature_peak02,
                                               distance02,
                                               alpha_step02,
                                               source_zero02,
                                               bottom_temp_noflux)
    # Case 3
    temp_step01_tserie[:, it] = \
        mod_auxfunc.diffusion_implicit_central(time_axis0x[0],
                                               temperature_step01,
                                               distance01,
                                               alpha_step01,
                                               source_zero01,
                                               bottom_temp_cold)
    temp_step02_tserie[:, it] = \
        mod_auxfunc.diffusion_implicit_central(time_axis0x[0],
                                               temperature_step02,
                                               distance02,
                                               alpha_step02,
                                               source_zero02,
                                               bottom_temp_cold)
    # Case 4
    temp_const01_src_tserie[:, it] = \
        mod_auxfunc.diffusion_implicit_central(time_axis0x[0],
                                               temperature_const01,
                                               distance01,
                                               alpha_src01,
                                               source_peak01,
                                               temperature_const01[-1])
    temp_const02_src_tserie[:, it] = \
        mod_auxfunc.diffusion_implicit_central(time_axis0x[0],
                                               temperature_const02,
                                               distance02,
                                               alpha_src02,
                                               source_peak02,
                                               temperature_const02[-1])

    #
    # Temporal evolution
    for it, dt in enumerate(time_axis0x[1:]):
        # Case 1
        temp_peak01_tserie[:, it+1] = \
            mod_auxfunc.diffusion_implicit_central(delta_time0x,
                                                   temp_peak01_tserie[:, it],
                                                   distance01,
                                                   alpha_const01,
                                                   source_zero01,
                                                   bottom_temp_noflux)
        temp_peak02_tserie[:, it+1] = \
            mod_auxfunc.diffusion_implicit_central(delta_time0x,
                                                   temp_peak02_tserie[:, it],
                                                   distance02,
                                                   alpha_const02,
                                                   source_zero02,
                                                   bottom_temp_noflux)
        # Case 3
        temp_as_peak01_tserie[:, it+1] = \
            mod_auxfunc.diffusion_implicit_central(delta_time0x,
                                                   temp_as_peak01_tserie[:, it],
                                                   distance01,
                                                   alpha_step01,
                                                   source_zero01,
                                                   bottom_temp_noflux)
        temp_as_peak02_tserie[:, it+1] = \
            mod_auxfunc.diffusion_implicit_central(delta_time0x,
                                                   temp_as_peak02_tserie[:, it],
                                                   distance02,
                                                   alpha_step02,
                                                   source_zero02,
                                                   bottom_temp_noflux)
        # Case 3
        temp_step01_tserie[:, it+1] = \
            mod_auxfunc.diffusion_implicit_central(delta_time0x,
                                                   temp_step01_tserie[:, it],
                                                   distance01,
                                                   alpha_step01,
                                                   source_zero01,
                                                   bottom_temp_cold)
        temp_step02_tserie[:, it+1] = \
            mod_auxfunc.diffusion_implicit_central(delta_time0x,
                                                   temp_step02_tserie[:, it],
                                                   distance02,
                                                   alpha_step02,
                                                   source_zero02,
                                                   bottom_temp_cold)
        # Case 4
        temp_const01_src_tserie[:, it+1] = \
            mod_auxfunc.diffusion_implicit_central(delta_time0x,
                                                   temp_const01_src_tserie[:, it],
                                                   distance01,
                                                   alpha_src01,
                                                   source_peak01,
                                                   temperature_const01[-1])
        temp_const02_src_tserie[:, it+1] = \
            mod_auxfunc.diffusion_implicit_central(delta_time0x,
                                                   temp_const02_src_tserie[:, it],
                                                   distance02,
                                                   alpha_src02,
                                                   source_peak02,
                                                   temperature_const02[-1])

    print(temp_const01_src_tserie[:,-1].max())


    #
    # Figure 0
    #
    fig0 = plt.figure(dpi=300, facecolor='white')
    axes0 = fig0.subplots()

    axes0.plot(distance01, depth01,'.-',
               linewidth=2.0,
               label='equidistant, {:d} nodes'.format(grid_size01))
    axes0.plot(distance02, depth02, 'o--',
               linewidth=2.0,
               label='linear increasing\ndistances, {:d} nodes'.format(grid_size02))

    axes0.set_xlabel('Distance (m)')
    axes0.set_ylabel('Depth (m)')
    axes0.set_title('Grid distance $\Delta x$ versus depth $d$')
    axes0.invert_yaxis()

    axes0.legend()


    if plot_suffix:
        module4plots.plotting(fig0, plotname+'_grid', plot_suffix)

    #
    # Figure 1
    #
    fig1 = plt.figure(dpi=300, facecolor='white')
    fig_fine_and_coarse_lines_time(fig1,
                                   temperature_const01-temp_degC2Kelvin,
                                   temp_const01_src_tserie-temp_degC2Kelvin,
                                   depth01,
                                   temperature_const02-temp_degC2Kelvin,
                                   temp_const02_src_tserie-temp_degC2Kelvin,
                                   depth02,
                                   source_peak01*second_per_day,
                                   time_steps0x,
                                   temperature_const01[-1]-temp_degC2Kelvin,
                                   mid_title='Source',
                                   mid_xlabel=r'$T_{flx}$ ($K\; day^{{-1}}$)')
    if plot_suffix:
        module4plots.plotting(fig1, plotname+'_tempConst_SrcHat_profile_time', plot_suffix)

    #
    # Figure 2
    #
    fig2 = plt.figure(dpi=300, facecolor='white')
    fig_fine_and_coarse_lines_time(fig2,
                                   temperature_peak01-temp_degC2Kelvin,
                                   temp_peak01_tserie-temp_degC2Kelvin,
                                   depth01,
                                   temperature_peak02-temp_degC2Kelvin,
                                   temp_peak02_tserie-temp_degC2Kelvin,
                                   depth02,
                                   alpha_const01*1.0e6,
                                   time_steps0x,
                                   bottom_temp_noflux-temp_degC2Kelvin,
                                   mid_title='Diffusivity',
                                   mid_xlabel=r'$\alpha$ ($mm^{{2}}\; s^{{-1}}$)')
    if plot_suffix:
        module4plots.plotting(fig2, plotname+'_tempHat_profile_time', plot_suffix)

    #
    # Figure 3
    #
    fig3 = plt.figure(dpi=300, facecolor='white')
    fig_fine_and_coarse_lines_time(fig3,
                                   temperature_peak01-temp_degC2Kelvin,
                                   temp_as_peak01_tserie-temp_degC2Kelvin,
                                   depth01,
                                   temperature_peak02-temp_degC2Kelvin,
                                   temp_as_peak02_tserie-temp_degC2Kelvin,
                                   depth02,
                                   alpha_step01*1.0e6,
                                   time_steps0x,
                                   bottom_temp_noflux-temp_degC2Kelvin,
                                   mid_title='Diffusivity',
                                   mid_xlabel=r'$\alpha$ ($mm^{{2}}\; s^{{-1}}$)')
    if plot_suffix:
        module4plots.plotting(fig3, plotname+'_tempHat_alphaStep_profile_time', plot_suffix)


    #
    # Figure 4
    #
    fig4 = plt.figure(dpi=300, facecolor='white')
    fig_fine_and_coarse_lines_time(fig4,
                                   temperature_step01-temp_degC2Kelvin,
                                   temp_step01_tserie-temp_degC2Kelvin,
                                   depth01,
                                   temperature_step02-temp_degC2Kelvin,
                                   temp_step02_tserie-temp_degC2Kelvin,
                                   depth02,
                                   alpha_step01*1.0e6,
                                   time_steps0x,
                                   bottom_temp_cold-temp_degC2Kelvin,
                                   mid_title='Diffusivity',
                                   mid_xlabel=r'$\alpha$ ($mm^{{2}}\; s^{{-1}}$)')
    if plot_suffix:
        module4plots.plotting(fig4, plotname+'_tempStep_profile_ColdBottom_time', plot_suffix)


    #
    # Figure 5
    #
    fig5 = plt.figure(dpi=300, facecolor='white')
    axes2 = fig5.subplots(1, 2, sharey=True,
                          gridspec_kw={'width_ratios': [1, 4]})

    iax_pcolor = 1
    iax_alpha = 0

    contour_filled_levels=np.arange(temp_low+5, temp_high, 5)-temp_degC2Kelvin
    contour_levels=[-35, -25, -15, -5] # Line contour lines

    [XX, YY] = np.meshgrid(time_axis0x/second_per_day, depth01)
    handle0 = axes2[iax_pcolor].contourf(XX, YY,
                                         temp_peak01_tserie-temp_degC2Kelvin,
                                         levels=contour_filled_levels,
                                         extend='both',
                                         cmap='coolwarm',
                                         alpha=0.66)

    handleCS2 = axes2[iax_pcolor].contour(XX, YY,
                                          temp_as_peak01_tserie-temp_degC2Kelvin,
                                          levels=contour_levels,
                                          linestyles='solid',
                                          colors='green')
    handleCS1 = axes2[iax_pcolor].contour(XX, YY,
                                          temp_peak01_tserie-temp_degC2Kelvin,
                                          levels=contour_levels,
                                          linestyles='dotted',
                                          colors='k')

    axes2[iax_alpha].plot(alpha_const01, depth01, ':',
                          linewidth=2,
                          color='k',
                          label='constant $\\alpha$')
    axes2[iax_alpha].plot(alpha_step01, depth01, '-',  # linestyle='solid',
                          linewidth=2,
                          color='green',
                          label='varying $\\alpha$')

    #
    # Adjust
    #
    axes2[iax_alpha].set_ylabel('Depth (m)')
    axes2[iax_alpha].invert_yaxis()
    axes2[iax_alpha].set_xlabel(r'$\alpha$ ($m^{{2}} s^{{-1}}$)')
    axes2[iax_alpha].xaxis.tick_top()

    axes2[iax_pcolor].set_xlabel('days')

    handle1, dummy = handleCS1.legend_elements()
    handle2, dummy = handleCS2.legend_elements()
    axes2[iax_pcolor].legend([handle1[0], handle2[0]],
                  ['constant $\\alpha$', 'varying $\\alpha$'])

    cbar=fig5.colorbar(handle0, ax=axes2[iax_pcolor],
                       location='right',
                       label='Temperature ($^{{\circ}}C$); Const. $\\alpha$={:g} $m{{m^2}} s^{{-1}}$'.format(alpha_const01[0]))
    cbar.add_lines(handleCS1)

    if plot_suffix:
        module4plots.plotting(fig5, plotname+'_temp_evolution', plot_suffix)

# -------------------------------------------------------------------------
def plot_rescale_field(library_name, plot_suffix=None, plotname='rescale_field'):
    '''
    Plot results of the via c-binding call Fortran function subsurface_field4d

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'rescale_field'.

    Returns
    -------
    None.

    '''
    print('* Rescale precipitation')

    # Inclined plane, shall represent an ice sheet ()
    x = np.minimum(np.linspace(-2000, 4500, 2500), 3500)
    y = np.zeros((1000))
    X, Y = np.meshgrid(x, y)
    del Y

    # Nunatak, dom
    x_nunatak = np.linspace(0, len(x), len(x))
    y_nunatak = np.linspace(0, len(y), len(y))
    X_nunatak, Y_nunatak = np.meshgrid(x_nunatak, y_nunatak)

    # Nunatak x-position left of the center
    x0_nunatak = np.max(x_nunatak)*2./3.+np.min(x_nunatak)*1./3.
    y0_nunatak = np.mean(y_nunatak) # Nunatak y-position at the center

    nunatak = 5000 * \
        np.exp(-np.square(4.0*(X_nunatak-x0_nunatak)/len(x_nunatak))) * \
        np.exp(-np.square(4.0*(Y_nunatak-y0_nunatak)/len(y_nunatak))) - 500.

    # Combined elevation: Inclined plane plus nunatak
    elevation0 = np.maximum(nunatak, np.maximum(X, 0.0))
    elevation_growth = np.maximum(nunatak, np.maximum(X+1000.0, 0.0))
    elevation_decay = np.maximum(nunatak, np.maximum(X-1000.0, 0.0))

    # Build glaciered mask, where
    threshold_elevation2ice = 10
    mask_ice0 = np.where(elevation0 > threshold_elevation2ice, 1, 0)
    mask_ice_growth = np.where(elevation_growth > threshold_elevation2ice, 1, 0)
    mask_ice_decay = np.where(elevation_decay > threshold_elevation2ice, 1, 0)

    # Rainband
    rainband_contour = 1000
    rain_background = np.zeros_like(elevation0) + 5.
    rainband = np.maximum(0., 15.-np.square((elevation0-rainband_contour)*10/len(x)))

    rain = rain_background + rainband

    # Preallocate result fields
    rain_decay = np.zeros_like(rain)
    rain_growth = np.zeros_like(rain)
    rain_decay_rescaled = rain_decay
    rain_growth_rescaled = rain_growth
    mask_decay_rescaled = np.zeros_like(mask_ice0, dtype=int)
    mask_growth_rescaled = np.zeros_like(mask_ice0, dtype=int)

    #
    # Call CISSEMBEL's functions
    #
    use_2dfields = True

    if use_2dfields:
        #
        # Use calls of 2d-fields
        #

        #
        # Height correction of the precipitation
        #
        # Lowered/decayed surface
        rain_decay = mod_physic.hcprecip_high_desert(rain, elevation0,
                                                     elevation_decay,
                                                     library_name)
        # Uplifted/grown surface
        rain_growth = mod_physic.hcprecip_high_desert(rain, elevation0,
                                                      elevation_growth,
                                                      library_name)
        #
        # Rescale the precipitation
        #
        # Rescale lowered/decayed surface

        mask_decay_rescaled  = np.maximum(mask_ice0, mask_ice_decay)
        factor = mod_auxfunc.rescale_field(rain, rain_decay, mask_decay_rescaled)
        rain_decay_rescaled = np.where(mask_decay_rescaled,
                                       rain_decay*factor, rain_decay)

        # Rescale uplifted/grown surface
        mask_growth_rescaled = np.maximum(mask_ice0, mask_ice_growth)
        factor = mod_auxfunc.rescale_field(rain, rain_growth, mask_growth_rescaled)

        rain_growth_rescaled = np.where(mask_growth_rescaled,
                                        rain_growth*factor, rain_growth)
    else:
        #
        # Use calls of 1d-fields/arrays
        #

        #
        # Height correction of the precipitation
        #
        for idx in range(rain.shape[0]):
            # Auxillary arrays
            array_rain = rain[idx, :]
            array_elev0 = elevation0[idx, :]
            array_elev_d = elevation_decay[idx, :]
            array_elev_g = elevation_growth[idx, :]

            # Height corrections for precipitation
            # Lowered/decayed surface
            rain_decay[idx, :] = mod_physic.hcprecip_high_desert(array_rain,
                                                                  array_elev0,
                                                                  array_elev_d,
                                                                  library_name)
            # Uplifted/grown surface
            rain_growth[idx, :] = mod_physic.hcprecip_high_desert(array_rain,
                                                                  array_elev0,
                                                                  array_elev_g,
                                                                  library_name)
        del array_elev0, array_elev_d, array_elev_g

        #
        # Rescale the precipitation
        #
        shape_of_elements = rain.shape
        number_of_elements = shape_of_elements[0]*shape_of_elements[1]

        array_rain = rain.reshape(number_of_elements)


        # Rescale lowered/decayed surface
        array_rain_d = rain_decay.reshape(number_of_elements)
        array_mask = np.maximum(mask_ice0.reshape(number_of_elements),
                                mask_ice_decay.reshape(number_of_elements))
        factor = mod_auxfunc.rescale_field(array_rain,
                                           array_rain_d,
                                           array_mask)

        array_rain_decay_rescaled = np.where(array_mask,
                                             array_rain_d*factor,
                                             array_rain_d)

        rain_decay_rescaled = array_rain_decay_rescaled.reshape(shape_of_elements)
        mask_decay_rescaled = array_mask.reshape(shape_of_elements)

        del array_rain_decay_rescaled, array_rain_d

        # Rescale uplifted/grown surface
        array_rain_g = rain_growth.reshape(number_of_elements)
        array_mask = np.maximum(mask_ice0.reshape(number_of_elements),
                                mask_ice_growth.reshape(number_of_elements))

        factor = mod_auxfunc.rescale_field(array_rain,
                                          array_rain_g,
                                          array_mask)

        array_rain_growth_rescaled = np.where(array_mask,
                                              array_rain_g*factor,
                                              array_rain_g)

        rain_growth_rescaled = array_rain_growth_rescaled.reshape(shape_of_elements)
        mask_growth_rescaled = array_mask.reshape(shape_of_elements)

        del array_rain_growth_rescaled, array_rain_g, array_rain, array_mask

    #
    # Figures
    #
    cmap_elevation = 'Greens_r' # 'ocean'
    cmap_rainfall = 'Blues'
    cmap_rainfall_anomaly = 'BrBG'

    color_bandelevation = 'fuchsia'

    linestyle_bandelevation = '-.'

    levels_elevation_contour = [10, 500, 1000, 2000, 3000, 4000]
    levels_elevation_fill = np.arange(0, 5000, 500)
    levels_rainfall = np.arange(4, 24, 2)
    levels_rainfall_anomaly =[-7, -5, -3, -2, -1, 0, 1, 2, 3, 5, 7]

    hatch_no_ice0 = '-'
    hatch_rescaled ='.'

    flag_plot_elevation0 = True
    flag_plot_rainfall0 = True
    flag_plot_elevation_rainfall0 = True
    flag_plot_elevations = True
    flag_plot_rescaled = True
    flag_plot_rescaled_anomaly = True

    def label_nunatak(iaxes, xpos, ypos, print_label=True):
        # Mark Nunatak' peak
        iaxes.plot(xpos, ypos, 'Xk')
        if print_label:
            iaxes.text(xpos, ypos,'  Nunatak', ha='left', va='center')

    def as_si(x, ndp=2):
        # https://stackoverflow.com/questions/31453422/displaying-numbers-with-x-instead-of-e-scientific-notation-in-matplotlib/31453961
        #
        # Call example
        # a=1.92e-7
        # plt.text(0.01, 0.23, r"$a = {0:s}$".format(as_si(a,2)), size=20)

        s = '{x:0.{ndp:d}e}'.format(x=x, ndp=ndp)
        m, e = s.split('e')
        return r'{m:s}\cdot 10^{{{e:d}}}'.format(m=m, e=int(e))


    #
    # Figure 1
    #
    if flag_plot_elevation0:
        fig1 = plt.figure(dpi=300, facecolor='white')
        axes1 = fig1.subplots()
        #axes1.set_aspect('equal')

        # Elevation
        pmesh1 = axes1.contourf(elevation0, cmap=cmap_elevation,
                                levels=levels_elevation_fill)
        cont1 = axes1.contour(elevation0, levels=levels_elevation_contour,
                              colors='dimgray', alpha=0.66, linewidths=2,
                              linestyles='--')

        cont2 = axes1.contour(elevation0, levels=[-999999., rainband_contour],
                              colors='red', alpha=0.66, linewidths=2,
                              linestyles='-')


        # Mask NO ice sheet region
        if True:
            # New code
            axes1.contourf(mask_ice0, levels=[0, 0.5],
                           hatches=[hatch_no_ice0], alpha=0)
        else:
            # Old code: deprecated: error in matplotlib 3.7
            axes1.contourf(mask_ice0, levels=[0, 0.5, 1],
                           hatches=[hatch_no_ice0, ' '], alpha=0)
        # Mark Nunatak's peak
        label_nunatak(axes1, x0_nunatak, y0_nunatak)

        cbar = fig1.colorbar(pmesh1, ax=axes1)
        cbar.set_label('Elevation (m)')
        if cont1:
            cbar.add_lines(cont1)
        if cont2:
            cbar.add_lines(cont2)

        axes1.xaxis.set_ticks([])
        axes1.yaxis.set_ticks([])
        axes1.text(50, len(y)/2, 'Strips:\nNo ice',
                   color='silver', fontsize='large', va='center')

        axes1.set_title('Reference topography')

        if plot_suffix:
            module4plots.plotting(fig1, plotname+'_elevation0', plot_suffix)

    #
    # Figure 2
    #
    if flag_plot_rainfall0:
        fig2 = plt.figure(dpi=300, facecolor='white')
        axes1 = fig2.subplots()
        # axes1.set_aspect('equal')

        # Rainfall
        pmesh1 = axes1.contourf(rain, cmap=cmap_rainfall,
                                levels=levels_rainfall, extend='max')
        cont1 = axes1.contour(elevation0, levels=levels_elevation_contour,
                              colors='dimgray', alpha=0.66, linewidths=1,
                              linestyles='--')
        cont2 = axes1.contour(elevation0, levels=[-999999., rainband_contour],
                              colors='red', alpha=0.66, linewidths=2,
                              linestyles='-')

        # Mask NO ice sheet region
        if True:
            # New code
            axes1.contourf(mask_ice0, levels=[0, 0.5],
                           hatches=[hatch_no_ice0], alpha=0)
        else:
            # Old code: deprecated: error in matplotlib 3.7
            axes1.contourf(mask_ice0, levels=[0, 0.5, 1],
                           hatches=[hatch_no_ice0, ' '], alpha=0)
        # Mark Nunatak's peak
        label_nunatak(axes1, x0_nunatak, y0_nunatak)

        cbar = fig2.colorbar(pmesh1, ax=axes1)
        # cbar.set_label('Rainfall, $\\int p\\; dA=${:4.4g}'.format(rain.sum()))
        cbar.set_label('Rainfall, $\\int p\\; dA={:s}$'.format(as_si(rain.sum())))

        axes1.xaxis.set_ticks([])
        axes1.yaxis.set_ticks([])
        axes1.text(50, len(y)/2, 'Strips:\nNo ice',
                   color='gray', fontsize='large', va='center')

        if plot_suffix:
            module4plots.plotting(fig2, plotname+'_rainfall0', plot_suffix)


    #
    # Figure 3
    #
    if flag_plot_elevation_rainfall0:
        fig3 = plt.figure(dpi=300, facecolor='white')
        axes1 = fig3.subplots()
        # axes1.set_aspect('equal')

        # Elevation + Rainfall
        # pmesh1 = axes1.pcolormesh(elevation0, cmap=cmap_elevation, vmin=0)
        pmesh1 =axes1.contourf(elevation0, cmap=cmap_elevation,
                               levels=levels_elevation_fill)
        pmesh2 = axes1.pcolormesh(np.where(rain>rain_background, rain, np.NaN),
                                  cmap=cmap_rainfall, vmin=0, alpha=0.22)
        cont1 = axes1.contour(elevation0, levels=levels_elevation_contour,
                              colors='dimgray', alpha=0.66, linewidths=1,
                              linestyles='--')
        cont2 = axes1.contour(elevation0, levels=[-999999., rainband_contour],
                              colors='red', alpha=0.66, linewidths=2,
                              linestyles='-')

        # Mask NO ice sheet region
        if True:
            # New code
            axes1.contourf(mask_ice0, levels=[0, 0.5],
                           hatches=[hatch_no_ice0], alpha=0)
        else:
            # Old code: deprecated: error in matplotlib 3.7
            axes1.contourf(mask_ice0, levels=[0, 0.5, 1],
                           hatches=[hatch_no_ice0, ' '], alpha=0)
        # Mark Nunatak's peak
        label_nunatak(axes1, x0_nunatak, y0_nunatak)

        cbar = fig3.colorbar(pmesh1, ax=axes1)
        cbar.set_label('Elevation (m)')
        if cont1:
            cbar.add_lines(cont1)
        if cont2:
            cbar.add_lines(cont2)

        axes1.xaxis.set_ticks([])
        axes1.yaxis.set_ticks([])
        axes1.text(50, len(y)/2, 'Strips:\nNo ice',
                   color='silver', fontsize='large', va='center')

        if plot_suffix:
            module4plots.plotting(fig3, plotname+'elevation0_rain0', plot_suffix)


    #
    # Figure 4
    #
    if flag_plot_elevations:
        fig4 = plt.figure(dpi=300, facecolor='white')
        axes1 = fig4.subplots(3, 1, sharex=True)
        fig4.subplots_adjust(hspace=0.1)

        levels = np.arange(0, 5000, 500)
        # Elevation + Rainfall
        for iax in axes1:
            if iax == axes1[0]:
                pmesh1 = iax.contourf(elevation0, cmap=cmap_elevation,
                                      levels=levels, extend='both')
                cont1 = iax.contour(elevation0,
                                    levels=levels_elevation_contour,
                                    colors='dimgray', alpha=0.66, linewidths=1,
                                    linestyles='--')
                if True:
                    # New code
                    iax.contourf(mask_ice0, levels=[0, 0.5],
                                 hatches=[hatch_no_ice0], alpha=0)
                else:
                    # Old code: deprecated: error in matplotlib 3.7
                    iax.contourf(mask_ice0, levels=[0, 0.5, 1],
                                 hatches=[hatch_no_ice0, ' '], alpha=0)
                iax.set_ylabel('Reference')
            elif iax == axes1[1]:
                pmesh2 = iax.contourf(elevation_decay, cmap=cmap_elevation,
                                      levels=levels, extend='both')
                cont1 = iax.contour(elevation_decay,
                                    levels=levels_elevation_contour,
                                    colors='dimgray', alpha=0.66, linewidths=1,
                                    linestyles='--')
                if True:
                    # New code
                    iax.contourf(mask_ice_decay, levels=[0, 0.5],
                                   hatches=[hatch_no_ice0], alpha=0)
                else:
                    # Old code: deprecated: error in matplotlib 3.7
                    iax.contourf(mask_ice_decay, levels=[0, 0.5, 1],
                                 hatches=[hatch_no_ice0, ' '], alpha=0)
                iax.set_ylabel('Shrinking: $\\downarrow$')
            elif iax == axes1[2]:
                pmesh2 = iax.contourf(elevation_growth, cmap=cmap_elevation,
                                      levels=levels, extend='both')
                cont1 = iax.contour(elevation_growth,
                                    levels=levels_elevation_contour,
                                    colors='dimgray', alpha=0.66, linewidths=1,
                                    linestyles='--')
                if True:
                    # New code
                    iax.contourf(mask_ice_growth, levels=[0, 0.5],
                                 hatches=[hatch_no_ice0], alpha=0)
                else:
                    # Old code: deprecated: error in matplotlib 3.7
                    iax.contourf(mask_ice_growth, levels=[0, 0.5, 1],
                                 hatches=[hatch_no_ice0, ' '], alpha=0)
                iax.set_ylabel('Growth: $\\uparrow$')

            cont1.collections[2].set_linestyle(linestyle_bandelevation)
            cont1.collections[2].set_color(color_bandelevation)
            cont1.collections[2].set_linewidth(2)


            cont2 = iax.contour(elevation0,
                                levels=[-999999., rainband_contour],
                                colors='red', alpha=0.66, linewidths=1,
                                linestyles='-')

            # Mark Nunatak's peak
            label_nunatak(iax, x0_nunatak, y0_nunatak)

            iax.xaxis.set_ticks([])
            iax.yaxis.set_ticks([])
            iax.text(50, len(y)/2, 'Strips:\nNo ice',
                     color='silver', fontsize='large', va='center')

        cbar = fig4.colorbar(pmesh1, ax=axes1, location='right')
        cbar.set_label('Elevation (m)')
        if cont1:
            cbar.add_lines(cont1)
        # if cont2:
        #     cbar.add_lines(cont2)

        if plot_suffix:
            module4plots.plotting(fig4, plotname+'_elevations', plot_suffix)

    #
    # Figure 5
    #
    if flag_plot_rescaled:
        fig5 = plt.figure(dpi=300, facecolor='white')
        axes1 = fig5.subplots(2, 2, sharex=True, sharey= True)
        fig5.subplots_adjust(wspace=0.1)

        for iax in axes1.flatten():
            if iax == axes1[0, 0]:
                pmesh2 = iax.contourf(rain_decay,
                                      cmap=cmap_rainfall,
                                      levels=levels_rainfall, extend='max')
                title = '$\\downarrow\\;\\int p\\, dA={:s}$'.format(as_si(rain_decay.sum()))
                cont1 = iax.contour(elevation_decay,
                                    levels=levels_elevation_contour,
                                    colors='dimgray', alpha=0.66, linewidths=1,
                                    linestyles='--')
            if iax == axes1[0, 1]:
                pmesh2 = iax.contourf(rain_decay_rescaled,
                                      cmap=cmap_rainfall,
                                      levels=levels_rainfall, extend='max')
                title = '$\\downarrow\\Re\\;\\int p\\, dA={:s}$'.format(as_si(rain_decay_rescaled.sum()))
                if True:
                    # New code
                    iax.contourf(mask_decay_rescaled, levels=[0.5, 1.0],
                                 hatches=[hatch_rescaled], alpha=0)
                else:
                    # Old code: deprecated: error in matplotlib 3.7
                    iax.contourf(mask_decay_rescaled, levels=[0, 0.5, 1],
                                 hatches=[' ', hatch_rescaled], alpha=0)
                cont1 = iax.contour(elevation_decay,
                                    levels=levels_elevation_contour,
                                    colors='dimgray', alpha=0.66, linewidths=1,
                                    linestyles='--')

            if iax == axes1[1, 0]:
                pmesh2 = iax.contourf(rain_growth,
                                      cmap=cmap_rainfall,
                                      levels=levels_rainfall, extend='max')
                title = '$\\uparrow\\;\\int p\\, dA={:s}$'.format(as_si(rain_growth.sum()))
                cont1 = iax.contour(elevation_growth,
                                    levels=levels_elevation_contour,
                                    colors='dimgray', alpha=0.66, linewidths=1,
                                    linestyles='--')
            if iax == axes1[1, 1]:
                pmesh2 = iax.contourf(rain_growth_rescaled,
                                      cmap=cmap_rainfall,
                                      levels=levels_rainfall, extend='max')
                title = '$\\uparrow\\Re\\;\\int p\\, dA={:s}$'.format(as_si(rain_growth_rescaled.sum()))
                if True:
                    # New code
                    iax.contourf(mask_growth_rescaled, levels=[0.5, 1.0],
                                 hatches=[hatch_rescaled], alpha=0)
                else:
                    # Old code: deprecated: error in matplotlib 3.7
                    iax.contourf(mask_growth_rescaled, levels=[0, 0.5, 1],
                                  hatches=[' ', hatch_rescaled], alpha=0)
                cont1 = iax.contour(elevation_growth,
                                    levels=levels_elevation_contour,
                                    colors='dimgray', alpha=0.66,
                                    linewidths=2,
                                    linestyles='--')
            if iax == axes1[0, 0]:
                iax.set_ylabel('Shrinking: $\\downarrow$')
            if iax == axes1[1, 0]:
                iax.set_ylabel('Growth $\\uparrow$')
            if iax == axes1[1, 0]:
                iax.set_xlabel('Height correction (HC)')
            if iax == axes1[1, 1]:
                iax.set_xlabel('HC + $\\Re$escaled')

            cont1.collections[2].set_linestyle(linestyle_bandelevation)
            cont1.collections[2].set_color(color_bandelevation)

            cont2 = iax.contour(elevation0,
                                levels=[-999999., rainband_contour],
                                colors='red', alpha=0.66, linewidths=1,
                                linestyles='-')

            iax.set_title(title)

            # Mask NO ice sheet region
            if True:
                # New code
                conthatch = iax.contourf(mask_ice0, levels=[0, 0.5],
                                         hatches=[hatch_no_ice0], alpha=0)
                # conthatch.collections[1].set_edgecolor('gray')
            else:
                # Old code: deprecated: error in matplotlib 3.7
                conthatch = iax.contourf(mask_ice0, levels=[0, 0.5, 1],
                             hatches=[hatch_no_ice0, ' '], alpha=0)
                conthatch.collections[1].set_edgecolor('gray')
            # Mark Nunatak's peak
            if iax == axes1[0, 0]:
                label_nunatak(iax, x0_nunatak, y0_nunatak)
            else:
                label_nunatak(iax, x0_nunatak, y0_nunatak, False)

            iax.xaxis.set_ticks([])
            iax.yaxis.set_ticks([])


        cbar = fig5.colorbar(pmesh2, ax=axes1)
        cbar.set_label('Rainfall'+
                       ', Ref: $\\int p\\; dA={:s}$'.format(as_si(rain.sum())))
        # if cont1:
        #     cbar.add_lines(cont1)

        if plot_suffix:
            module4plots.plotting(fig5, plotname+'_precipitation_rescaled', plot_suffix)


    #
    # Figure 6
    #
    if flag_plot_rescaled_anomaly:
        fig6 = plt.figure(dpi=300, facecolor='white')
        axes1 = fig6.subplots(2, 2, sharex=True, sharey= True)
        fig6.subplots_adjust(wspace=0.1)

        for iax in axes1.flatten():
            if iax == axes1[0, 0]:
                pmesh2 = iax.contourf(rain_decay-rain,
                                      cmap=cmap_rainfall_anomaly,
                                      levels=levels_rainfall_anomaly)
                title = '$\\downarrow\\;\\int p\\, dA={:s}$'.format(as_si(rain_decay.sum()))
                cont1 = iax.contour(elevation_decay,
                                    levels=levels_elevation_contour,
                                    colors='dimgray', alpha=0.66, linewidths=1,
                                    linestyles='--')
            if iax == axes1[0, 1]:
                pmesh2 = iax.contourf(rain_decay_rescaled-rain,
                                      cmap=cmap_rainfall_anomaly,
                                      levels=levels_rainfall_anomaly)
                title = '$\\downarrow\\Re\\;\\int p\\, dA={:s}$'.format(as_si(rain_decay_rescaled.sum()))

                if True:
                    # New code
                    iax.contourf(mask_decay_rescaled, levels=[0.5, 1.0],
                                 hatches=[hatch_rescaled], alpha=0)
                else:
                    # Old code: deprecated: error in matplotlib 3.7
                    iax.contourf(mask_decay_rescaled, levels=[0, 0.5, 1],
                                  hatches=[' ', hatch_rescaled], alpha=0)
                cont1 = iax.contour(elevation_decay,
                                    levels=levels_elevation_contour,
                                    colors='dimgray', alpha=0.66, linewidths=1,
                                    linestyles='--')


            if iax == axes1[1, 0]:
                pmesh2 = iax.contourf(rain_growth-rain,
                                      cmap=cmap_rainfall_anomaly,
                                      levels=levels_rainfall_anomaly)
                title = '$\\uparrow\\;\\int p\\, dA={:s}$'.format(as_si(rain_growth.sum()))
                cont1 = iax.contour(elevation_growth,
                                    levels=levels_elevation_contour,
                                    colors='dimgray', alpha=0.66, linewidths=1,
                                    linestyles='--')
            if iax == axes1[1, 1]:
                pmesh2 = iax.contourf(rain_growth_rescaled-rain,
                                      cmap=cmap_rainfall_anomaly,
                                      levels=levels_rainfall_anomaly)
                title = '$\\uparrow\\Re\\;\\int p\\, dA={:s}$'.format(as_si(rain_growth_rescaled.sum()))


                if True:
                    # New code
                    iax.contourf(mask_growth_rescaled, levels=[0.5, 1],
                                 hatches=[hatch_rescaled], alpha=0)
                else:
                    # Old code: deprecated: error in matplotlib 3.7
                    iax.contourf(mask_growth_rescaled, levels=[0, 0.5, 1],
                                 hatches=[' ', hatch_rescaled], alpha=0)
                cont1 = iax.contour(elevation_growth,
                                    levels=levels_elevation_contour,
                                    colors='dimgray', alpha=0.66,
                                    linewidths=2,
                                    linestyles='--')
            if iax == axes1[0, 0]:
                iax.set_ylabel('Shrinking: $\\downarrow$')
            if iax == axes1[1, 0]:
                iax.set_ylabel('Growth $\\uparrow$')
            if iax == axes1[1, 0]:
                iax.set_xlabel('Height correction (HC)')
            if iax == axes1[1, 1]:
                iax.set_xlabel('HC + $\\Re$escaled')

            cont1.collections[2].set_linestyle(linestyle_bandelevation)
            cont1.collections[2].set_color(color_bandelevation)

            cont2 = iax.contour(elevation0,
                                levels=[-999999., rainband_contour],
                                colors='red', alpha=0.66, linewidths=1,
                                linestyles='-')

            iax.set_title(title)

            # Mask NO ice sheet region
            if True:
                # New code
                conthatch = iax.contourf(mask_ice0, levels=[0, 0.5],
                                         hatches=[hatch_no_ice0], alpha=0)
                # conthatch.collections[1].set_edgecolor('gray')
            else:
                # Old code: deprecated: error in matplotlib 3.7
                conthatch = iax.contourf(mask_ice0, levels=[0, 0.5, 1],
                             hatches=[hatch_no_ice0, ' '], alpha=0)
                conthatch.collections[1].set_edgecolor('gray')
            # Mark Nunatak's peak
            if iax == axes1[0, 0]:
                label_nunatak(iax, x0_nunatak, y0_nunatak)
            else:
                label_nunatak(iax, x0_nunatak, y0_nunatak, False)

            iax.xaxis.set_ticks([])
            iax.yaxis.set_ticks([])

        cbar = fig6.colorbar(pmesh2, ax=axes1)
        cbar.set_label('Rainfall anomaly'+
                       ', Ref: $\\int p\\; dA={:s}$'.format(as_si(rain.sum())))
        # if cont1:
        #     cbar.add_lines(cont1)

        if plot_suffix:
            module4plots.plotting(fig6, plotname+'_precipitation_rescaled_ano', plot_suffix)






# -------------------------------------------------------------------------
def plot_values_at_depth_level(library_name, plot_suffix=None,
                               plotname='values_at_depth_level'):
    '''
    Plot results of the via c-binding call Fortran function values_at_depth_level

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'values_at_depth_level'.

    Returns
    -------
    None.

    '''
    print('* Extract values at requested depth')

    depth0 = 0
    depth1 = 50
    depth = np.linspace(depth0, depth1, 11)   # Generic depth profile
    field1 = np.exp(depth/(0.25*max(depth)))  # Generic field1: exponential function
    field2 = np.sin(2*np.pi*depth/max(depth)) # Generic field2: Sin function

    # Higher resolved subsurface depth field which shall reproduce the fields
    # field1 and field2
    depth_subsurface = np.linspace(depth0, depth1, len(depth)*3+2)

    # Preallocte output
    subsurface_values1 = np.zeros_like(depth_subsurface)
    subsurface_values2 = np.zeros_like(depth_subsurface)

    for idepth, sdepth in enumerate(depth_subsurface):
        subsurface_values1[idepth] = \
            mod_auxfunc.values_at_depth_level(field1, depth, sdepth,
                                              library_name)
        subsurface_values2[idepth] = \
            mod_auxfunc.values_at_depth_level(field2, depth, sdepth,
                                             library_name)
    #
    # Figure
    #
    fig = plt.figure(dpi=300, facecolor='white')
    ax_left, ax_right = fig.subplots(1, 2, sharey=True)
    fig.subplots_adjust(wspace=0.025)

    ax_left.plot(field1, depth, '--o', c='cornflowerblue')
    ax_left.plot(subsurface_values1, depth_subsurface, '.', c='darkred')
    ax_left.set_xlabel('Value')
    ax_left.set_title('Exponential', fontsize='small')
    ax_left.legend(['Reference', 'Interpolated'])

    ax_right.plot(field2, depth, '--o', c='cornflowerblue')
    ax_right.plot(subsurface_values2, depth_subsurface, '.', c='darkred')
    ax_right.set_xlabel('Value')
    ax_right.set_title('Sinus', fontsize='small')

    ax_left.set_ylabel('Depth (m)')
    ax_left.invert_yaxis()

    fig.suptitle('Test of interpolation for generic profiles')

    if plot_suffix:
        module4plots.plotting(fig, plotname, plot_suffix)


# -------------------------------------------------------------------------
def plot_adiff_0to1_cto0(library_name, plot_suffix=None,
                         plotname='adiff_0to1_cto0'):
    '''
    Plot results of the via c-binding call Fortran function adiff_0to1_cto0

    Parameters
    ----------
    library : str or ctypes.CDLL
        Reference to imported shared library containing the C-bindings.
    plot_suffix : str, optional
        Suffix of the figure output file. The default is None.
    plotname : TYPE, optional
        Filename prefix. The default is 'adiff_0to1_cto0'.

    Returns
    -------
    None.

    '''
    print('* adiff_0to1_cto0')

    diff = np.linspace(-20, 20, 81, dtype=float)
    adiff = np.abs(diff)
    critical_diffs = [7.5, 15.0]

    criterion0 = mod_auxfunc.adiff_0to1_cto0(adiff, critical_diffs[0], library_name)
    criterion1 = mod_auxfunc.adiff_0to1_cto0(adiff, critical_diffs[1], library_name)

    #
    # Figure
    #
    fig = plt.figure(dpi=300, facecolor='white')
    ax_all = fig.subplots()

    ax_all.plot(diff, criterion0, '-', color='navy', linewidth=2,
                 label='{:.1f}'.format(critical_diffs[0]))
    ax_all.plot(diff, criterion1, '--', color='darkorange', linewidth=2,
                 label='{:.1f}'.format(critical_diffs[1]))


    ax_all.axvspan(-critical_diffs[1], critical_diffs[1],
                   facecolor='darkorange', alpha=0.1)
    ax_all.axvspan(-critical_diffs[0], critical_diffs[0],
                   facecolor='navy', alpha=0.15)


    ax_all.set_xlabel('Difference (including sign)')
    ax_all.set_ylabel('criterion value')

    ax_all.legend(title='Critical difference')

    if plot_suffix:
        module4plots.plotting(fig, plotname, plot_suffix)

# -------------------------------------------------------------------------
#
# Main program
#
if __name__ == '__main__':
    import values_mod_param as physical
    import cbind_mod_physic as mod_physic
    import cbind_mod_auxfunc as mod_auxfunc
    import numpy as np
    import matplotlib.pyplot as plt
    import module4plots

    print('Load physical constant')
    physical.constant()
    # Values
    GRADLW = physical.constant.gradlw
    LAPSE_RATE = physical.constant.lapse_rate
    PATM = physical.constant.patm_surf
    RHO_ICE = physical.constant.rho_ice
    RHO_SNOW = physical.constant.rho_snow
    RHO_FWATER = physical.constant.rho_fwater
    RHO_SEAWATER = physical.constant.rho_seawater
    CDENSITY = physical.constant.Cdens
    FREEZING_TEMP = physical.constant.Tmelt_fw

    #
    # Load the shared library
    #
    LIBRARY_NAME = '../src/CISSEMBEL_CBindings.so' # INCLUDING path, e.g., ./
    print('Shared library containing C-bindings of Fortran code "'
          +LIBRARY_NAME+'"')

    PLOT_SUFFIXES = module4plots.parse_arguments() # = None = ['png', 'pdf']

    #
    # Plots for each function
    #
    plot_diffusion_implicit_central(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_rescale_field(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_values_at_depth_level(LIBRARY_NAME, PLOT_SUFFIXES)
    plot_adiff_0to1_cto0(LIBRARY_NAME, PLOT_SUFFIXES)
# -- Last line

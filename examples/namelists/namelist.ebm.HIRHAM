! Git version control id: $Id$
!>
!! @file namelist.ebm
!! @brief Namelist parameter file controlling the behavior of CISSEMBEL
!! @details This namelist parameter file contains all current
!!  available namelist parameter that control the internal behavior of
!!  CISSEMBEL, such the name of the initial (input) field, names of
!!  output files, flag determining considered processes, and various
!!  numerical and physical parameters. However, settings determining
!!  the reading of atmospheric forcing file(s) are **not** controlled
!!  by this namelist file `namelist.ebm`.
!!
!! @note Prameters controlling the offline driver are compiled in an
!!  independent namelist file named `namelist.EBMdriver`. This other
!!  namelist file includes information about the reading of forcing
!!  input files.
!!  If you use a Python driver or couple CISSEMBEL directly with an
!!  atmosphere model, for instance, the namelist `namelist.EBMdriver`
!!  is NOT used and evaluated.
!!
!!  HIRHAM setup
!

! ------------------------------------------------------------------
!
! General control of CISSEMBEL
!
&run_ctrl
 do_restart = F, ! Shall I do a restart?
 do_region_timeseries = F, ! Current default: F; Do regional timeseries (region definitions from initial file)
 do_region_area_weight = F ! Shall we read and write the grid point area field?
 do_stations = F,          ! Current default: F; Do timeseries at selected station (potentially high frequent)
 do_compute_surface_slope = F ! Compute surface slope from elevation, If 'F' provide field in INItial file
 do_height_classes = F,    ! Compute the SMB at different heights instead of the the reference height
 do_height_classes_plus = F, ! Do height classes and SMB at reference height, which is saved at index=0
 do_age_snow = F,          ! Compute the snow age (top=0)
 do_biology = F,           ! Future, default=F
 do_datetime_from_YYMMDDHHMM = F, !
 do_albedo_cloudcover = T, ! Shall the cloud cover modifies the surface albedo?
 verbose_level = 3, ! Level of verboseness of output
 dx_grid = 100000. , ! Grid size in x/i-direction for surface slope computation (requires do_compute_surface_slope=T)
 dy_grid = 100000. , ! Grid size in x/i-direction for surface slope computation (requires do_compute_surface_slope=T)
 inum_omp_thread_nml = 4, ! Number of OMP threads if defined pre-processor LIMIT_OMP_NUM_THREADS_NML
/

&write_frequency
 ! Zero deactivates writing except for automatic writing at the end.
 nstep_restart = 8036,     !
 nstep_output = 4,         !
 nstep_diagnost = 220,     !
 nstep_station = 4,        !
 nstep_forcing = 16072,    !
 !ORG nstep_timeserie2d = 8   , !
 !ORG nstep_timeserie3d = 2035, !
 nstep_timeserie2d = 1   , !
 nstep_timeserie3d = 56,   !
 nstep_biology = 16072,    !
 nstep_coupling = 16072,   !
/

&processes_world
  do_height_correction_airpress	= T,
/

&processes_column
 ! do_rain_temp_warms_snow = T,
 ! do_top_massflux2move = T,
 ! do_percolation_in_column = T,
 ! do_refreeze_in_column = T,
 ! do_form_superimposed_ice = T,
 ! do_densification = T,
 ! do_grainsize_evolution = T,
 ! do_heat_diffusion = T,
 ! iselect_freshsnow_density = 1,
/

&processes_bulk_fluxes
/

! ------------------------------------------------------------------
!
! Labels used to describe the model run, which are storaged in the output
!
&label_institute
 ! Information about the insitution running CISSEMBEL
 institution = "Danish Meteorological Institute" ,
 institution_id = "DMI" ,
 department = "Rigsfaellesskabets Klima" , ! = "Rigsfællesskabets Klima" ,
 address = "Lyngbyvej 100, DK-2100 Copenhagen, Denmark" ,
 contact = "Christian Rodehacke, http://research.dmi.dk/staff/all-staff/cr" ,
 web_page = "https://www.dmi.dk" ,
 keywords = "Surface Mass Balance, numerical simulation, cryosphere, CISSEMBEL",
 !license = "standard",
/

&label_run
 ! Information about the specific CISSEMBEL simulation
 model_domain = "HIRHAM Greenland domain" ,
 experiment = "Simulation by EraInterim 2007-2017",
 experiment_id = "EraInterim2007_2017",
 parent_experiment_id = "spinup2007_2017",
 forcing_data_source = "HIRHAM",
 project = "PROTECT (European Union), NCKF (National DK)" ,
/

! ------------------------------------------------------------------
!
! Filenames and names of its dimensions and variables
!
! NOTE: Atmospheric forcing files are NOT defined here
!
&io_put
 do_long_message = F,
 do_allmpi_world_msg = F,
/

&CISSEMBEL_nml_files
 !Optional: fname_nml4rst = 'namelist.ebm',  ! Namelist filename of ReSTart data file information
 !Optional: fname_nml4ini = 'namelist.ebm',  ! Namelist filename of INItial data file information
 !Optional: fname_nml4write = 'namelist.ebm', ! Namelist filename of write/output data file information
 !Optional: fname_nml4label = 'namelist.ebm', ! Namelist filename of label information about run and institute
 !Optional: fname_nml4bio = 'namelist.ebm' , ! Namelist filename of biology component information
/

&ini_file
 fname_ini_data = 'HIRHAM_initial.nc',
 dimname_iaxis = 'xaxis', ! Dimension name of i-/x-dimension
 dimname_jaxis = 'yaxis', ! Dimension name of j-/y-dimension
 dimname_kaxis = 'kaxis', ! Dimension name of k-/z-dimension (depth)
 dimname_haxis = 'haxis', ! Dimension name of h-dimension (height)
 dimname_raxis = 'raxis', ! Dimension name of r-dimension (regional basins)
 dimname_saxis = 'saxis', ! Dimension name of s-dimension (stations)
 dimname_caxis = 'nv',    ! Dimension name of c-dimension (corner of the grid boxes)
 dimname_1axis = 'one',   ! Dimension name of 1-dimension (degenerated dim with length 1)
 dimname_taxis = 'time',  ! Dimension name of t-dimension (time)
 varname_area = 'cell_area', ! Field/Variable name of the cell area (m2)
 varname_depth = 'depth', ! depth of snow layer (either depth in meters
 varname_height ='height', ! Heights (needed if height classes are used)
 varname_lat = 'lat',     ! Latitude
 varname_layer_thickness = 'layer_thickness', ! thickness of snow/ice layers
 varname_lon = 'lon',     ! Longitude
 varname_region_mask = 'basins', ! Mask for integer value defining various basins
 varname_station_mask = 'station_mask', ! Mask where stations are located
 varname_station_number = 'stationID',  ! Numeric values as identifiers for stations
 varname_surface_slope = 'surface_slope' ! Surface slope (needed for time-delayed runoff; m m-1)
 varname_target_elevation = 'elevation', ! Target surface elevation (of ice sheet model)
/

&restart_file
 fname_rst_input = 'restart_output_spinup.nc',
 fname_rst_output = 'restart_output_final.nc',
 irecord_restart = -1, ! Record to read from restart file (-1=last record)
/

&write2files
 netcdf_create_mode = 'NF90_64BIT_OFFSET', ! Mode of created files: NF90_64BIT_OFFSET, NF90_NETCDF4, NF90_NETCDF4_CLASSIC , ...
 do_ncvar_compression = F, ! Compression of variables in output files; requires netcdf4 output
 do_write_extended_forcing = T, !Standard: F
 fname_dia_data = 'diagnost.nc',    ! filename of diagnostic output (DIA)
 fname_frc_data = 'forcing_out.nc', ! filename of written forcing fields (FRC)
 fname_sta_data = 'station.nc',     ! filename of the station file (STA)
 fname_cpl_data = 'coupling4ism.nc', ! filename of the coupling file (CPL) to drive ice sheet models
 dimname_haxis = 'haxis',  ! Dimension name of h-dimension (height)
 dimname_iaxis = 'iaxis',  ! Dimension name of i-/x-dimension
 dimname_jaxis = 'jaxis',  ! Dimension name of j-/y-dimension
 dimname_kaxis = 'kaxis',  ! Dimension name of k-/z-dimension (depth)
 dimname_raxis = 'raxis',  ! Dimension name of r-dimension (regional basins)
 dimname_saxis = 'saxis',  ! Dimension name of s-dimension (stations)
 dimname_caxis = 'nv',     ! Dimension name of c-dimension (corner of the grid boxes)
 dimname_baxis = 'tbounds',! Dimension name of b-dimension (time bounds, length 2)
 dimname_1axis = 'one',    ! Dimension name of 1-dimension (degenerated dim, length 1)
 dimname_taxis = 'time',   ! Dimension name of t-dimension (time)
 units_haxis = 'meter',    ! Units of the h-dimension (height)
 units_taxis = 'seconds since 2007-09-23 16:00:00', ! Units of the t-dimension (time)
 varname_lat = 'lat', ! Latitude
 varname_lon = 'lon', ! Longitude
/

&biology
 fname_bio_input = 'biology_input.nc',   ! filename of the biology input file (BIO); restart and continuation
 fname_bio_output = 'biology_output.nc', ! filename of the biology output file (BIO)
 irecord_biology = -1,   ! Record to read from biology input file (-1=last record)
/

&tracer
 fname_tracer_input = 'tracer_input.nc',   ! filename of the tracer input file (TRC); restart and continuation
 fname_tracer_output = 'tracer_output.nc', ! filename of the tracer output file (TRC)
 irecord_tracer = -1,   ! Record to read from tracer input file (-1=last record)
/

&CISSEMBEL_para_files
 do_read_para4albedo = T, ! Read parameters for mod_albedo?
 do_read_para4param  = T, ! Read parameters for mod_param?
 do_read_para4physic = T, ! Read parameters for mod_physic?
 do_read_para4phy_col = T, ! Read parameters for mod_physic_colum?
 do_read_para4dyn_col = T, ! Read parameters for mod_dynamic_colum?
 do_read_para4sta_col = F, ! Read parameters for mod_static_colum?
 fname_para4albedo = 'namelist.ebm',
 !Optional: fname_para4param  = 'namelist.ebm',
 !Optional: fname_para4physic = 'namelist.ebm',
 !Optional: fname_para4phy_col = 'namelist.ebm',
 !Optional: fname_para4dyn_col = 'namelist.ebm',
 !Optional: fname_para4sta_col = 'namelist.ebm',
/

! ------------------------------------------------------------------
!
! Note: A exclamation point ('!') marks an comment. If an '!' occurs
!       at the beginning/before a defined value, its standard value
!       is used, which is defined in the code.
!

! ------------------------------------------------------------------
!
! Parameters for albedo parameterization (mod_albedo.F08)
!
! iselect_albedo : It selects the albedo parameterization
!
&albedo_parameters
 iselect_albedo = 5 ,
 do_restrict_albedo_allowed_min_max = T,
 ! albedo_allowed_minimum = 0.10 ,
 ! albedo_allowed_maximum = 0.95 ,
 ! albedo_ice = 0.65 ,
 ! albedo_ice_melting = 0.40 ,
 ! albedo_ice_refrozen = 0.45 ,
 ! albedo_firn = 0.70 ,
 ! albedo_freshsnow = 0.85 ,
 ! albedo_melting = 0.55 ,
 ! albedo_q1 = -4.0e-4 ,
 ! albedo_q2 = 0.97 ,
 ! albedo_refrozen =  0.65 ,
 ! albedo_snow = 0.76 ,
 ! albedo_snow_melting = 0.50 ,
 ! albedo_snow_refrozen = 0.65 ,
 ! alb_delta_cloud = 0.05 ,
 ! cloudc0 = 0.5 ,
 ! default_reciprocal_refrozen_decaytime = 2.572e-7 ,
 ! default_reciprocal_snow_decaytime = 3.858e-7 ,
 ! default_snowfall_threshold = 7.23e-7 ,
 ! default_snow_depth_decaylength = 2.4e-3 ,
 ! default_snow_depth_threshold =  0.05 ,
 ! default_temperature_transitional = 2.0 ,
 ! flag_albedo_refrozen = T ,
/
! ------------------------------------------------------------------

! ------------------------------------------------------------------
!
! Parameters listed in mod_param.F08
!
&param_parameters
 lapse_rate = -6.5e-3,          ! atmospheric lapse rate (K m-1)
 latent_heat_melting = 3.36e5,  ! (J kg-1)
 latent_heat_sublimation = 2.834e6, ! (J kg-1)
 snowfall_threshold = 7.23e-7,  ! (kg m-2 s-1) !7.23e-10 m/s for rho=1000kg/m3
 refrezfrac  = 1.0,             ! (1) Fraction of melted ice that refreeze
 refrez_cutoff_density = 830.0, ! (kg m-3) pore close-off density
 graind_upper_threshold = 11.0e-3, ! (Meter) Upper grain size threshold.
 snowd_required_thickness = 0.0025,! (Meter) Required thickness to consider a snow layer
 snowd_max_thickness = 2.0,     ! (Meter)
 snowd_init = 0.10,             ! (Meter) initial snowdepth for new runs
 emissivity_snow = 0.98,        ! (1) Warren (2019)
/
! ------------------------------------------------------------------

! ------------------------------------------------------------------
!
! Parameters listed in mod_physic.F08
!
&physic_parameters
 iselect_precip2snow = 1,
 omega_tpre2snow = 0.5,          ! (Kelvin)
 superimp_ice_form_c1 = 28512,   ! (Second)
 superimp_ice_form_c2 = 2160000, ! (Second)
 superimp_ice_form_c3 = 140, ! (unitless)
 Tmin_wvp = -138.0,  ! (degreeC)
 Tmax_wvp =  +50.0,  ! (degreeC)
/
! ------------------------------------------------------------------

! ------------------------------------------------------------------
!
! Parameters listed in mod_physic_column.F08
!
&phy_col_parameters
 bottom_age_limit_min = 86400,         ! (Second)
 bottom_age_limit_max = 3944700000000. ! (Second)
 bottom_graind_limit_min =  0.0004,    ! (Meter)
 bottom_graind_limit_max =  0.05,      ! (Meter)
 bottom_temp_limit_max = 273.14,       ! (Kelvin)
 bottom_temp_limit_min = 173.15,       ! (Kelvin)
 do_compute_maximum_liquid_water = T,
 do_dedicated_darcy_flow = T,
 do_refreeze_normed_length = T,
 do_refreeze_normed_period = F,
 do_refreeze_fixed_factor = F,
 default_liquid_water_fraction_max = 0.07,  ! (1 = fraction)
 default_new_grain_diameter = 0.0005,  ! (Meter)
 melt_fraction_limit_max = 1.0,        ! (1)
 permeability_icelens = 0.0,
 refreeze_length = 0.1,           ! (Meter)
 refreeze_inv_etime = 1.3889e-4,  ! (Second-1)
 wh2wice = 1.0,
/
! ------------------------------------------------------------------

! ------------------------------------------------------------------
!
! Parameters listed in mod_physic_column.F08
!
&auxfunc_parameters
 iter_max_diff = 19,               ! (1)
 mismatch_tolerance_diff = 1.0e-8, ! (UNIT)
/

! ------------------------------------------------------------------
!
! Parameters listed in mod_dynamic_column.F08
!
&dyn_col_parameters
 thickness_maximum = 15.,
 ! default_new_layer_minimum_thickness = 0.30, !0.065
/
! ------------------------------------------------------------------

! ------------------------------------------------------------------
!
! Parameters listed in mod_static_column.F08
!
&sta_col_parameters
/
! ------------------------------------------------------------------
